/*
    @Description: handlers all DML events on Attachment
    @Author: Jesfer Baculod - Positive Group 
    @History:
        09/07/17 - Updated, restructured AttachmentTrigger
*/
trigger AttachmentTrigger on Attachment (after insert, before delete) {

	Integer trigger_insert_loopCount = 0;

    Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());

    if (trigset.Enable_Attachment_Trigger__c){

            if (trigger.isInsert){
            	if(trigger_insert_loopCount == 0){ //Execute Attachment Trigger just once
            		if (trigger.isAfter){
            			//call after insert event triggers 
                    	AttachmentTriggerHandler.onAfterInsert(trigger.newMap);
            		}
            	}
            }
            else if (trigger.isDelete){
            	if (trigger.isBefore){
        			//call before delete event triggers 
                	AttachmentTriggerHandler.onBeforeDelete(trigger.oldMap);
        		}
            }
    }

}
/*
    @Description: handlers all DML events on Scenarios
    @Author: Jesfer Baculod - Positive Group 
    @History:
        11/13/17 - Created
        11/14/17 - Updated
*/
trigger ScenarioTrigger on Complex_Scenario__c (before insert, before update) {

	Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());
    
    if (trigset.Enable_Scenario_Trigger__c){
    	if (trigger.isInsert){
    		if (trigger.isBefore){
    			//call before insert event triggers
    			ScenarioTriggerHandler.onBeforeInsert(trigger.new);
    		}
    	}
    	else if (trigger.isUpdate){
    		if (trigger.isBefore){
    			//call before update event triggers
    			if (ScenarioTriggerHandler.firstRun){
    				ScenarioTriggerHandler.onBeforeUpdate(trigger.oldMap,trigger.newMap);
    				if (!Test.isRunningTest()) ScenarioTriggerHandler.firstRun = false;
    			}
    		}
    	}
    }
}
trigger AgentWorkTrigger on AgentWork (before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {
	
	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_AgentWork_Trigger__c){
			TriggerFactory.createHandler(AgentWork.sObjectType);
		}
	}
}
// trigger AgentWorkTrigger on AgentWork (after update) {
//     System.debug('trigger new '+trigger.new);
//     if(Trigger.isUpdate){
//         if(trigger.new[0].Status == 'Opened'){
//             Support_Request__c sp = new Support_Request__c(Id = trigger.new[0].WorkItemId, Status__c ='In Progress', Stage__c = 'In Progress');
//             Database.update(sp);       
//         }
//     }
// }
trigger RemoveNullReference on Reference__c (after insert, after update) {
    Reference__c m = [select Phone__c from Reference__c where id =:trigger.new[0].id];
    if(m.Phone__c == null){
        delete m;
    }
}
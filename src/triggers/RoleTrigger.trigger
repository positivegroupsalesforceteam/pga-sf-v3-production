/** 
* @FileName: RoleTrigger
* @Description: Main Trigger for Role__c object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 2/15 RDAVID Created Trigger
* 1.1 2/16 JBACULOD Added Trigger toggle for object
**/ 
trigger RoleTrigger on Role__c (	before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {
	
	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Role_Trigger__c){
			TriggerFactory.createHandler(Role__c.sObjectType);
		}
	}
}
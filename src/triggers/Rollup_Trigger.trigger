trigger Rollup_Trigger on Income__c (after insert, after update, after delete) {

// Non bulkified
id contactid = trigger.new[0].Contact__c;

decimal MonthlyIncome = 0.0;
decimal MonthlyovertimeIncome = 0.0;
decimal FamilySupport = 0.0;
decimal investmentincome = 0.0;
decimal centralinkincome = 0.0;
decimal selftemploymentincome = 0.0;

List<Income__c > incomelist = new List<Income__c >();
incomelist = [select id,Employer_Net_Monthly_Standard_Income__c,Employer_Net_Monthly_Overtime__c,Family_Support_Income__c,
              Investment_Income__c,Centrelink_Income__c,Self_Employed_Annual_Net_Income__c 
              from Income__c where Contact__c =: contactid AND Is_Active__c= true];
if(incomelist.size() > 0){             
for(Income__c inc : incomelist ){

    if(inc.Employer_Net_Monthly_Standard_Income__c != null) MonthlyIncome  += inc.Employer_Net_Monthly_Standard_Income__c ;
    if(inc.Employer_Net_Monthly_Overtime__c != null) MonthlyovertimeIncome += inc.Employer_Net_Monthly_Overtime__c;
    if(inc.Family_Support_Income__c!= null) FamilySupport += inc.Family_Support_Income__c;
    if(inc.Investment_Income__c!= null) investmentincome += inc.Investment_Income__c;
    if(inc.Centrelink_Income__c!= null) centralinkincome += inc.Centrelink_Income__c;
    if(inc.Self_Employed_Annual_Net_Income__c != null) selftemploymentincome +=  inc.Self_Employed_Annual_Net_Income__c ;
}


Contact cnt = new Contact();
cnt.Id = contactId;
cnt.Combined_Regular_Income__c = MonthlyIncome  ;
cnt.Combined_Child_Support_Income__c= FamilySupport ;
cnt.Combined_Centrelink_Income__c = centralinkincome ;
cnt.Combined_Investment_Income__c = investmentincome ;
cnt.Combined_Overtime_Income__c = MonthlyovertimeIncome ;
cnt.Self_Employed_Combined_Income__c = selftemploymentincome ;
update cnt;
}
}
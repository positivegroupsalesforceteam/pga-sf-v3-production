/** 
* @FileName: ContactTrigger1
* @Description: Main Trigger for Contact object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 20/12/18 RDAVID - extra/PartnerAndDealerDirectory - Created Trigger
**/ 
trigger ContactTrigger1 on Contact (before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {
    if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Contact_Trigger__c){
			TriggerFactory.createHandler(Contact.sObjectType);
		}
	}
}
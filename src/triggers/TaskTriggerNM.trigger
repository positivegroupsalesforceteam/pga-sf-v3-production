trigger TaskTriggerNM on Task (before insert, before update, before delete, after update, after insert, after delete, after undelete) {

    if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Task_Trigger__c){
			TriggerFactory.createHandler(Task.sObjectType);
		}
	}
}
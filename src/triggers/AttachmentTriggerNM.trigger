/** 
* @FileName: AttachmentTriggerNM
* @Description: Main Trigger for Attachment object used in New Model
* @Copyright: Positive (c) 2018
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 9/03/18 JBACULOD Created
**/ 
trigger AttachmentTriggerNM on Attachment (before insert, before update, 
                                        before delete, after insert, 
                                        after update, after delete, after undelete) {
    
    if (TriggerFactory.trigset.Enable_Triggers__c){
        if (TriggerFactory.trigset.Enable_Attachment_Trigger__c){
            TriggerFactory.createHandler(Attachment.sObjectType);
        }
    } 

}
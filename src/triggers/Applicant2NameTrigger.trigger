trigger Applicant2NameTrigger on Applicant_2__c (before insert, before update) {

    Set<Id> accIds = new Set<Id>(); 
    Map<Id,Account> accMap;
    
    for(Applicant_2__c objApp : trigger.new)
     {
         accIds.add(objApp.Account__c);
         
         if(objApp.name != null)
         {
             List<String> nameArray = objApp.name.split(' ');
             if(nameArray.size() == 1) {
                 objApp.First_Name__c = nameArray[0];
                 objApp.Middle_Name__c = '';
                 objApp.Last_Name__c = '';
                 
             }
             if(nameArray.size() == 2) 
             {
                 objApp.First_Name__c = nameArray[0];
                 objApp.Last_Name__c = nameArray[1];
                 objApp.Middle_Name__c = '';
             }
             if(nameArray.size() > 2  ) 
             {
                 objApp.First_Name__c = nameArray[0];
                 objApp.Middle_Name__c = nameArray[1];
                 objApp.Last_Name__c = objApp.name.remove(nameArray[0] + ' '+ nameArray[1]);
             }
         }
     }
    accMap = new Map<Id,Account>([SELECT Id, Applicant2_Mobile__c FROM Account WHERE Id IN: accIds]);
    for(Applicant_2__c app2:Trigger.New){
        if(accMap != null && accMap.size()>0){
            if(app2 != null){
                if(app2.Mobile__c != null){
                Account myAcc = accMap.get(app2.Account__c);
                myAcc.Applicant2_Mobile__c = app2.Mobile__c;
                    }
                }
            }
        }        
    if(accMap != null){
    	Database.update(accMap.values());    
    }   
}
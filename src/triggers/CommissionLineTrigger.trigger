/** 
* @FileName: FrontEndSubmissionTrigger
* @Description: Main Trigger for CommissionLineTrigger object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 4/8/18 RDAVID Created Trigger
**/ 
trigger CommissionLineTrigger on Commission_Line_Items__c (	before insert, after insert, before update, after update, before delete, after delete) {
	
	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Commission_Line_Trigger__c){
			TriggerFactory.createHandler(Commission_Line_Items__c.sObjectType);
		}
	}
}
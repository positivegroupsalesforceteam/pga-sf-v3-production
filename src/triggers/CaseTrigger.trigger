/** 
* @FileName: CaseTrigger
* @Description: Main Trigger for Case object used in New Model
* @Copyright: Ativa, Positive (c) 2017
* @author: Raoul Lake
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 4/02/17 RLAKE Created
* 1.1 8/14/17 JBACULOD Updated, restructured case trigger 
* 2.0 2/19/18 JBACULOD Moved old case trigger to CaseTrigger0 to follow NM new trigger framework
**/ 
trigger CaseTrigger on Case (before insert, before update, 
                                        before delete, after insert, 
                                        after update, after delete, after undelete) {

    if (TriggerFactory.trigset.Enable_Triggers__c){
        if (TriggerFactory.trigset.Enable_Case_Trigger__c){
            TriggerFactory.createHandler(Case.sObjectType);
        }
    }
    
}
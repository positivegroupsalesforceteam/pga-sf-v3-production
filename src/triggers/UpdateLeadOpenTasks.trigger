Trigger UpdateLeadOpenTasks on Task (after delete, after insert, after undelete, after update) {
 
    System.Debug('Runnning UpdateLeadOpenTasks');
 
// Declare the variables
 
    public set<Id> LeadIDs = new Set<Id>();
    public list<Lead> LeadsToUpdate = new List<Lead>();
    public set<Id> AccountIDs = new Set<Id>();
    public list<Account> AccountsToUpdate = new List<Account>();
    public set<Id> OpportunityIDs = new Set<Id>();
    public list<Opportunity> OpportunityToUpdate= new List<Opportunity>();
    public List<Outbound_SMS_History__c> createSms = new List<Outbound_SMS_History__c>();
    public List<Outbound_SMS_History__c> createSmsOpps = new List<Outbound_SMS_History__c>();
    public List<Outbound_SMS_History__c> createSmsAcc = new List<Outbound_SMS_History__c>();
    public List<Inbound_SMS_History__c> incomingSms = new List<Inbound_SMS_History__c>();
    public List<Inbound_SMS_History__c> incomingSmsOpps = new List<Inbound_SMS_History__c>();
    public List<Inbound_SMS_History__c> incomingSmsAcc = new List<Inbound_SMS_History__c>();
    private Map<Id, lead> mapLead;
    
// Build the list of Leads and Accounts to update
 
    if(Trigger.isInsert || Trigger.isUpdate || Trigger.isUnDelete){

    System.Debug('Creating a list of IDs to update');
 
    for(Task t: Trigger.new){       
        System.Debug(t.WhoId);
  
        if(t.WhoId !=null) {
        //We need to put something here to handle a fail if the user doesn't link an activity to a Account or Lead

            if(string.valueOf(t.WhoId).startsWith('00Q')) {
                LeadIDs.add(t.WhoId);
            } else {
                System.Debug('Not lead');
            }
        }
        if (t.WhatId !=null) {
        System.debug('@@@Line38whatId' + t.whatId);
            if(string.valueOf(t.WhatId).startsWith('001')) {
                AccountIDs.add(t.WhatId);
            } else if(string.valueOf(t.WhatId).startsWith('006')) {
                OpportunityIDs.add(t.WhatId);
            } else {
                System.Debug('Not account or opportunity');
            }
        }
        
    } //end for

// Inserting sms tasks in to Outbound Sms History Object for Lead.    

    mapLead = new Map<Id, Lead>([SELECT Id, First_Call_Made_Time__c  from Lead where Id IN:LeadIds]); 
    for(Task t:Trigger.New){
    
        if(t.whoId != null ){
        if(t.subject == 'SMS: Sent'){
        if(mapLead != null && mapLead.containsKey(t.whoId) && t.whoId == mapLead.get(t.whoId).id){
            createSms.add(new Outbound_SMS_History__c (OutboundSms_Lead__c = t.whoId, 
            OutboundSms_SMSText__c = t.Description, OutboundSms_StatusMessage__c = t.status, 
            OutboundSms_Sent_By__c = t.createdBy.Name, OutboundSms_SentStatus__c = t.Status, OutboundSms_Name__c = t.who.Name ));
            } 
        }
    }    

    }
                  
    Try {
       if(createSms != null&& createSms.size()>0){
    insert createSms;
    }
    } catch(exception Ex) {
    System.debug('@@@Exception:'+Ex);
    }
 
  
// Inserting sms tasks in to Inbound Sms History Object for Lead.
    
    for(Task t:Trigger.New){
        System.debug('@@@whoId:' + t.whoId);
        if(t.whoId != null ){
        System.debug('@@@subject:' + t.subject);
        if(t.subject=='SMS: Received' && t.Description != null){
        if(mapLead != null && mapLead.containsKey(t.whoId) && t.whoId == mapLead.get(t.whoId).Id){
            
        string descriptionVal;
        string desTrimVal;
            if(t.Description != null && t.Description.length()>19){
            descriptionVal = t.Description.substring(6,19);
        }
        if(t.Description != null && t.Description.length()>27){
            desTrimVal= t.Description.substring(27);
        }
            incomingSms.add(new Inbound_SMS_History__c (InboundSms_Lead__c = t.whoId, 
            InboundSms_Sent_By__c = t.CreatedBy.Name, InboundSms_Mobile_Number__c = descriptionVal, 
            InboundSms_SMS_Text__c = desTrimVal, InboundSms_SentStatus__c = t.status));
       }   
        }
    } 
    }
        
    Try{
    insert incomingSms;
    } catch(exception Ex){
    System.debug('@@@exception:'+Ex);       
    }

// Inserting Sms task in to Outbound Sms History Object for Opportunity.

    Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>([SELECT Id from Opportunity where Id IN:OpportunityIds]);
    for(Task t:Trigger.New){
        
        System.debug('@@@mapOpp' + mapOpp);
        System.debug('@@@mapSize' + mapOpp.size());
        System.debug('@@@whatId'+ t.whatId);
        System.debug('@@@Subject' + t.subject);
        if(t.whatId != null ){
        if(t.Subject == 'SMS: Sent'){
        if(mapOpp != null && mapOpp.containsKey(t.whatId) && t.whatId==mapOpp.get(t.whatId).Id){
            createSmsOpps.add(new Outbound_SMS_History__c ( OutboundSms_Opportunity__c = t.whatId, 
            OutboundSms_SMSText__c = t.Description, OutboundSms_StatusMessage__c = t.status, 
            OutboundSms_Sent_By__c = t.createdBy.Name, OutboundSms_SentStatus__c = t.Status, OutboundSms_Name__c = t.what.Name ));       
        }
        }
    }
    } 
    
    Try{
    system.debug('@@@Create Sms' + createSms.size());
    if(createSmsOpps != null&& createSmsOpps.size()>0){
    insert createSmsOpps;
    }
    } Catch (Exception Ex){
            System.debug('@@@Exception:' + Ex );
    }
 
// Inserting Sms task in to Inbound sms History Object for Opportunity.

    set<id> whatIds=new set<id>();
    for(Task t:Trigger.New){
        if(t.accountId!=null){
        
            whatIds.add(t.accountId);
        
        }
        if(t.whatId != null ){
            
        system.debug('@@@t' + t);
        system.debug('@@@whatId' + t.whatId);
        system.debug('@@@subject' + t.subject);
        if(t.subject=='Sms: Received')  {
        if(t.whatId != null && mapOpp.containsKey(t.whatId) && t.whatId==mapOpp.get(t.whatId).Id){

        }       
        }
    }
    }
    
    system.debug('@@@whatIds'+whatIds);
    if(whatIds.size()>0){

        Map<id,opportunity> oppMap=new Map<id,opportunity>([select id,name, 
        accountId from opportunity where accountId IN:whatIds]);
        system.debug('@@@oppMap'+oppMap);
        for(Task t:Trigger.New){
            
            for(opportunity opp:oppMap.values()){
                if(t.accountId==opp.accountId){
                
        string descriptionVal;
        string desTrimVal;
            if(t.Description != null && t.Description.length()>19){
            descriptionVal = t.Description.substring(6,19);
        }
            if(t.Description != null && t.Description.length()>27){
            desTrimVal= t.Description.substring(27);
        }
            if(t.Subject == 'SMS: Received') {        
            incomingSmsOpps.add(new Inbound_SMS_History__c (InboundSms_Opportunity__c = opp.id, 
            InboundSms_Sent_By__c = t.CreatedBy.Name, InboundSms_Mobile_Number__c = descriptionVal, 
            InboundSms_SMS_Text__c = desTrimVal, InboundSms_SentStatus__c = t.status));
                        }
                    } 
                }
            }
        }
    
        Try{
            insert incomingSmsOpps; 
        }Catch(Exception Ex){
            System.debug('@@@Exception:' + Ex);
        }

// Inserting SMS task in to Outbound SMS History for Account
        
        Map<Id,Account> accMap = new Map<Id,Account>([SELECT Id, Name FROM Account WHERE Id IN: AccountIDs]);
            for(task t:Trigger.New){
                if(t.whatId != null){
                if(accMap != null && accMap.containsKey(t.whatId) && t.WhatId == accMap.get(t.whatId).Id){
                if(t.Subject == 'SMS: Sent'){ 
                    createSmsAcc.add(new Outbound_SMS_History__c(Account__c = t.WhatId, OutboundSms_SMSText__c = t.Description,
                    OutboundSms_StatusMessage__c = t.status, OutboundSms_Sent_By__c = t.createdBy.Name, 
                    OutboundSms_SentStatus__c = t.Status, OutboundSms_Name__c = t.what.Name  ));
                    }    
                }
            }
            }    
        try{
            insert createSmsAcc;
           }
        catch(Exception ex){
            System.debug('Exception' + ex);
        }

// Inserting SMS task in to Inbound SMS History for Account
    
        for(task t:Trigger.New){
            if(t.WhatId != null){
            if(accMap != null && accMap.containsKey(t.WhatId) && t.WhatId == accMap.get(t.WhatId).Id){
                if(t.Subject == 'SMS: Received' && t.Description != null){
                    String descriptionVal;
                    String desTrimVal;
                    if(t.Description != null && t.Description.length()>19){
                        descriptionVal = t.Description.substring(6,19);
                    }
                    if(t.Description != null && t.Description.length()>27){
                        desTrimVal = t.Description.substring(27);
                    }
                    incomingSmsAcc.add(new Inbound_SMS_History__c (Account__c = t.whatId, 
                    InboundSms_Sent_By__c = t.CreatedBy.Name, InboundSms_Mobile_Number__c = descriptionVal, 
                    InboundSms_SMS_Text__c = desTrimVal, InboundSms_SentStatus__c = t.status));
                }    
            }
            }    
        }
            try {
                insert incomingSmsAcc;
            }catch(Exception ex){
                System.debug('Exception ' + ex);
            }
            
    }

    if(Trigger.isDelete){
        for(Task t: Trigger.old){
        if(t == null){
            if(string.valueOf(t.WhoId).startsWith('00Q')){
            // leads
            LeadIDs.add(t.WhoId);
            }else if(string.valueOf(t.WhatId).startsWith('001')){
            // account
            AccountIDs.add(t.WhatId);
            } else if(string.valueof(t.WhatId).startsWith('006')) {
            // oportunity
            OpportunityIDs.add(t.WhatId);
            } else {
            System.Debug('Not account, lead or opportunity');
            }
        }
    }
    }
 
    /*
// Update the Lead
 
    if(LeadIDs.size()>0){
    System.Debug('Lead has at least 1 record - doing something');
        for(Lead l: [Select l.Id, l.Calls_Made__c, 
        (Select Id From Tasks where CallType='Outbound' and Status='Completed') //Change select statement to just be NVM Tasks
        From Lead l where Id in :LeadIDs])
    LeadsToUpdate.add(new Lead(Id=l.Id, Calls_Made__c = l.Tasks.size()));
    try{
    
        update LeadsToUpdate;
    
    }catch(exception e){
    
    
    }
 
    }
 
// Update the Account
    if(AccountIDs.size()>0){
    System.Debug('Account has at least 1 record - doing something');
//We need to put in code to handle it if we don't know who the Account is
 
        for(Account c: [Select c.Id, c.Calls_Made__c,
        (Select Id From Tasks where CallType='Outbound' and Status='Completed')//Change select statement to just be NVM Tasks
        From Account c where Id in :AccountIDs])
    AccountsToUpdate.add(new Account(Id=c.Id, Calls_Made__c = c.Tasks.size()));
    try{
    update AccountsToUpdate;
    }catch(exception e){
    
    }
    }
 
 // Update the Opportunity
    if(OpportunityIDs.size()>0){
    System.Debug('Opportunity has at least 1 record - doing something');
//We need to put in code to handle it if we don't know who the Opportunity is
 
        for(Opportunity o: [Select o.Id, o.Calls_Made__c,
        (Select Id From Tasks where CallType='Outbound' and Status='Completed')//Change select statement to just be NVM Tasks
            From Opportunity o where Id in :OpportunityIDs])
        OpportunityToUpdate.add(new Opportunity(Id=o.Id, Calls_Made__c = o.Tasks.size()));
        try{
        update OpportunityToUpdate;
        }catch(exception e){
               
            }
        }
    
    

    List<Task> listOfTasks = [SELECT Id, CreatedDate, CallType, whoId from Task where whoId IN: LeadIds AND CallType = 'Outbound' ORDER BY CreatedDate ASC Limit 1 ];
     
    if(Trigger.isInsert){
        if(listOfTasks.size()>0 && listOfTasks != null){
            for(Task t:listOfTasks){
                if(t.whoId != null){
                    if(string.valueOf(t.whoId).startsWith('00Q')){
                         if(t.CallType == 'Outbound'){
                            System.debug('CallType ===> ' + t.CallType);
                                lead myLead = mapLead.get(t.whoId);
                            myLead.First_Call_Made_Time__c = t.CreatedDate;
                            System.debug('First Call Made Time ===> ' + myLead.First_Call_Made_Time__c );
                        } 
                    }
                }    
            }
        }else{
            for(task tObj:trigger.new){
                if(tObj.whoId != null){
                    if(string.valueOf(tObj.whoId).startsWith('00Q')){
                         if(tObj.CallType == 'Outbound'){
                            lead myLead = mapLead.get(tObj.whoId);
                            myLead.First_Call_Made_Time__c = tObj.CreatedDate;
                        } 
                    } 
                }
            }
        }
    }
    if(mapLead != null){
    update mapLead.values();    
    } */
}
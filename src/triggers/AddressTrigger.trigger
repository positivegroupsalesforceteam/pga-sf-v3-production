/** 
* @FileName: AddressTrigger
* @Description: Main Trigger for Address__c object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 15/06/2018 RDAVID Created Trigger
**/ 
trigger AddressTrigger on Address__c (	before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {
	
	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Address_Trigger__c){
			TriggerFactory.createHandler(Address__c.sObjectType);
		}
	}
}
/** 
* @FileName: ReferralCompanyTrigger
* @Description: Main Trigger for Referral_Company__c object used in New Model
* @Copyright: Positive (c) 2019 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 7/01/19 RDAVID - extra/PartnerAndDealerDirectory - Created Trigger
**/ 
trigger ReferralCompanyTrigger on Referral_Company__c (before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {
    if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Referral_Company_Trigger__c){
			TriggerFactory.createHandler(Referral_Company__c.sObjectType);
		}
	}                                           
}
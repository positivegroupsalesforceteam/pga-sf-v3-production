trigger Rollup_ContactTrigger on Contact (after insert, after update, after delete) {
/*  14 May 2017 (RL comments)
*   Applies to Key Person rt (Connective only as at May 2017). 
*   In future if extended beyond Connective, check for appropriate account record type matches in rollups.
*   On Delete - no action taken at this stage. For future, consider any logic for delete.
*/

    // Non bulkified
    Id rectypeid = [select id from Recordtype where Name = 'Key Person' and sObjectType = 'Contact'].Id;

    if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){

        if(trigger.new[0].Recordtypeid == rectypeid && trigger.new[0].Accountid != null){
            id Accountid = trigger.new[0].Accountid ;
            decimal MonthlyIncome = 0.0;
            decimal MonthlyovertimeIncome = 0.0;
            decimal FamilySupport = 0.0;
            decimal investmentincome = 0.0;
            decimal centralinkincome = 0.0;
            decimal selftemploymentincome = 0.0;
            decimal rent = 0.0;
            decimal creditcard = 0.0;
            decimal loan = 0.0;  
            decimal other = 0.0;

            List<Contact> ContactList = new List<Contact>();
            ContactList = [select id,Combined_Regular_Income__c ,Combined_Overtime_Income__c ,Combined_Child_Support_Income__c,
                          Combined_Centrelink_Income__c,Combined_Investment_Income__c ,Self_Employed_Combined_Income__c,
                           Combined_Rent_Mortgage_Board__c,Combined_Credit_Card_Payments__c,Combined_Loan_Payments__c,
                           Combined_Other_Liabilities__c from Contact where AccountId =: Accountid ];
            if(ContactList .size() > 0){             
                for(Contact inc : ContactList ){

                    if(inc.Combined_Regular_Income__c != null) MonthlyIncome  += inc.Combined_Regular_Income__c ;
                    if(inc.Combined_Overtime_Income__c != null) MonthlyovertimeIncome += inc.Combined_Overtime_Income__c ;
                    if(inc.Combined_Child_Support_Income__c != null) FamilySupport += inc.Combined_Child_Support_Income__c;
                    if(inc.Combined_Investment_Income__c != null) investmentincome += inc.Combined_Investment_Income__c ;
                    if(inc.Combined_Centrelink_Income__c != null) centralinkincome += inc.Combined_Centrelink_Income__c ;
                    if(inc.Self_Employed_Combined_Income__c != null) selftemploymentincome +=  inc.Self_Employed_Combined_Income__c ;
                    
                      if(inc.Combined_Rent_Mortgage_Board__c != null)  rent += inc.Combined_Rent_Mortgage_Board__c;
                     if(inc.Combined_Credit_Card_Payments__c != null)   creditcard  += inc.Combined_Credit_Card_Payments__c;
                     if(inc.Combined_Loan_Payments__c != null)  loan  += inc.Combined_Loan_Payments__c;
                     if(inc.Combined_Other_Liabilities__c != null) other += inc.Combined_Other_Liabilities__c;
                }   

                Account cnt = new Account();
                cnt.Id = AccountId;
                cnt.Combined_Regular_Income__c = MonthlyIncome  ;
                cnt.Combined_Child_Support_Income__c = FamilySupport ;
                cnt.Combined_Centrelink_Income__c = centralinkincome ;
                cnt.Combined_Investment_Income__c = investmentincome ;
                cnt.Combined_Overtime_Income__c = MonthlyovertimeIncome ;
                cnt.Self_Employed_Combined_Income__c = selftemploymentincome ;
                cnt.Combined_Rent_Mortgage_Board__c = rent;
                cnt.Combined_Credit_Card_Payments__c = creditcard;
                cnt.Combined_Loan_Payments__c = loan;
                cnt.Combined_Other_Liabilities__c = other;
            
                update cnt;
            }

        }
    }
    If (trigger.isAfter && trigger.isDelete) {
        //at this stage do nothing. For future consider what remedial action is needed.
    }
}
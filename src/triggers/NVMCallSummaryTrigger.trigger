/** 
* @FileName: CaseTrigger
* @Description: Main Trigger for NVM Call Summary
* @Copyright: Positive (c) 2019
* @author: Raoul Lake
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 5/20/19 JBACULOD Created
**/ 
trigger NVMCallSummaryTrigger on NVMStatsSF__NVM_Call_Summary__c (before insert, before update, 
                                        before delete, after insert, 
                                        after update, after delete, after undelete) {

    if (TriggerFactory.trigset.Enable_Triggers__c){
        if (TriggerFactory.trigset.Enable_NVM_Call_Summary_Trigger__c){
            TriggerFactory.createHandler(NVMStatsSF__NVM_Call_Summary__c.sObjectType);
        }
    }                                     

}
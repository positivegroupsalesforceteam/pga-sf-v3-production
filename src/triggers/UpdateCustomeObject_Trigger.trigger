trigger UpdateCustomeObject_Trigger on Lead (before Update ) 
{
// Add applicant2
    List<Applicant_2__c> appentries = new List<Applicant_2__c>();
  
    for (Applicant_2__c Qualification_Criteria : [   select  Account__c, Lead__c
                                                                from    Applicant_2__c 
                                                                where   Lead__c in :Trigger.newMap.keySet()]) 
    {

        Lead lead = Trigger.newMap.get(Qualification_Criteria.Lead__c);
        if (lead != null && lead.isConverted == true && Trigger.oldMap.get(lead.Id).isConverted == false) 
        {
            Qualification_Criteria.account__c = lead.ConvertedAccountId;
            appentries.add(Qualification_Criteria);
        }
    }


    update appentries;
// Add assets
    List<Assets__c> assentries = new List<Assets__c>();

    for (Assets__c Qualification_Criteria : [   select  Account__c, Lead__c
                                                                from    Assets__c 
                                                                where   Lead__c in :Trigger.newMap.keySet()]) 
    {
        Lead lead = Trigger.newMap.get(Qualification_Criteria.Lead__c);
        if (lead != null && lead.isConverted == true && Trigger.oldMap.get(lead.Id).isConverted == false) 
        {
            Qualification_Criteria.account__c = lead.ConvertedAccountId;
            assentries.add(Qualification_Criteria);
        }
    }
    update assentries;
// Add employers
    List<Employee__c> empentries = new List<Employee__c>();

    for (Employee__c Qualification_Criteria : [   select  Account__c, Lead__c
                                                                from    Employee__c 
                                                                where   Lead__c in :Trigger.newMap.keySet()]) 
    {
        Lead lead = Trigger.newMap.get(Qualification_Criteria.Lead__c);
        if (lead != null && lead.isConverted == true && Trigger.oldMap.get(lead.Id).isConverted == false) 
        {
            Qualification_Criteria.account__c = lead.ConvertedAccountId;
            empentries.add(Qualification_Criteria);
        }
    }
    update empentries;
// Add liabilities
    List<Liability__c> liaentries = new List<Liability__c>();

    for (Liability__c Qualification_Criteria : [   select  Account__c, Lead__c
                                                                from    Liability__c 
                                                                where   Lead__c in :Trigger.newMap.keySet()]) 
    {
        Lead lead = Trigger.newMap.get(Qualification_Criteria.Lead__c);
        if (lead != null && lead.isConverted == true && Trigger.oldMap.get(lead.Id).isConverted == false) 
        {
            Qualification_Criteria.account__c = lead.ConvertedAccountId;
            liaentries.add(Qualification_Criteria);
        }
    }
    update liaentries;
// Add references
    List<Reference__c> refentries = new List<Reference__c>();

    for (Reference__c Qualification_Criteria : [   select  Account__c, Lead__c
                                                                from    Reference__c 
                                                                where   Lead__c in : Trigger.newMap.keySet()]) 
    {
        Lead lead = Trigger.newMap.get(Qualification_Criteria.Lead__c);
        if (lead != null && lead.isConverted == true && Trigger.oldMap.get(lead.Id).isConverted == false) 
        {
            Qualification_Criteria.account__c = lead.ConvertedAccountId;
            refentries.add(Qualification_Criteria);
        }
    }
    update refentries;
// Add Form Submissions
    List<Form_Submissions__c> formEntries = new List<Form_Submissions__c>();
    
    for (Form_Submissions__c Qualification_Criteria : [ select Opportunity__c, Lead__c from Form_Submissions__c
                                                        where Lead__c in : Trigger.newMap.KeySet()])
    {
        Lead lead = Trigger.newMap.get(Qualification_Criteria.Lead__c);
        if (lead != null && lead.isConverted == true && Trigger.oldMap.get(lead.Id).isConverted == false)
        {
            Qualification_Criteria.Opportunity__c = lead.ConvertedOpportunityId;
            formEntries.add(Qualification_Criteria );    
        }
    }
    update formEntries;                                                            
 
// Living Expenses calculation 
    
    for(Lead ld:Trigger.New){
       
        if(ld.CreatedDate_Time__c != null){
        ld.CreatedTime__c = ld.CreatedDate_Time__c.format('KK:mm a');
        }
      
        if(ld.Lender_and_Dependant_Expenses__c > ld.Living_Expenses_Customer__c){
            ld.Expenses_Considered__c= ld.Lender_and_Dependant_Expenses__c ;
        }else
    {
    ld.Expenses_Considered__c= ld.Living_Expenses_Customer__c;
        }
    }
    
    for(Lead l:Trigger.New){
        if(l.CreatedDate_Time__c != null){
            l.CreatedTime__c = l.CreatedDate_Time__c.format('KK:mm a');
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('07') && l.CreatedTime__c.contains('AM')){
            l.CreatedTime24__c = '07:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('08') && l.CreatedTime__c.contains('AM')){
            l.CreatedTime24__c = '08:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('09') && l.CreatedTime__c.contains('AM')){
            l.CreatedTime24__c = '09:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('10') && l.CreatedTime__c.contains('AM')){
            l.CreatedTime24__c = '10:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('11') && l.CreatedTime__c.contains('AM')){
            l.CreatedTime24__c = '11:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('00') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '12:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('01') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '13:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('02') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '14:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('03') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '15:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('04') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '16:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('05') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '17:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('06') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '18:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('07') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '19:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('08') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '20:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('09') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '21:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('10') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '22:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('11') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '23:00';
        }
        if(string.valueOf(l.CreatedTime__c).startsWith('12') && l.CreatedTime__c.contains('PM')){
            l.CreatedTime24__c = '24:00';
        }
    }
     
}
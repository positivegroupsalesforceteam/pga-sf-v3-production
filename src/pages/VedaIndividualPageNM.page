<!--
Author: Steven Herod, Feb 2014
Purpose: Combination search screen and results display
History:	09/08/2017 - Moved PDF conversion to controller (Jesfer Baculod - Positive Group)
			09/11/2017 - Added setTabTitle for opening Veda Credit Check in Service Console (Jesfer Baculod - Positive Group)
-->

<apex:page showHeader="true" sidebar="true" standardController="Role__c" extensions="VedaIndividualCC_NM" tabStyle="Role__c">

<apex:sectionHeader title="VedaScore Financial Commercial + Consumer 1.1" subtitle="{!acctObj.lastName}, {!acctObj.firstName}" />
<apex:includeScript value="/support/console/36.0/integration.js"/>

<apex:form >	
<apex:pageBlock >

	<apex:pageMessage summary="{!errorTextMsg}"
				rendered="{!errorMsg}"	 severity="error" strength="3" />
    <apex:pageMessages />

	<apex:pageBlockButtons >
		<apex:commandButton value="Perform Credit Check" action="{!generateCreditReport}" />
		<apex:commandButton value="Back" action="{!back}" />
	</apex:pageBlockButtons>
	
		<apex:pageBlockSection title="Search Criteria" columns="3">
		 <apex:pageBlockSectionItem ><apex:outputLabel value="Firstname"/><apex:inputText value="{!request.individual.firstGivenName}"/></apex:pageBlockSectionItem>
		 <apex:pageBlockSectionItem ><apex:outputLabel value="Family Name"/><apex:inputText value="{!request.individual.familyName}"/></apex:pageBlockSectionItem>
		 <apex:pageBlockSectionItem >
		 <apex:outputLabel value="Date of Birth"/><apex:inputText value="{!request.individual.dateOfBirth}"/></apex:pageBlockSectionItem>
		 <apex:pageBlockSectionItem >
		 <apex:outputLabel value="Street Number"/><apex:inputText value="{!request.individual.currentAddress.streetNumber}"/></apex:pageBlockSectionItem>
		 <apex:pageBlockSectionItem >
		<apex:outputLabel value="Street Name"/><apex:inputText value="{!request.individual.currentAddress.streetName}"/></apex:pageBlockSectionItem>
		 <apex:pageBlockSectionItem >
		<apex:outputLabel value="Street Type"/><apex:inputText value="{!request.individual.currentAddress.streetType}"/></apex:pageBlockSectionItem>
		 <apex:pageBlockSectionItem >
			<apex:outputLabel value="Postcode"/><apex:inputText value="{!request.individual.currentAddress.postcode}"/></apex:pageBlockSectionItem>
		 <apex:pageBlockSectionItem >
		<apex:outputLabel value="Suburb"/><apex:inputText value="{!request.individual.currentAddress.suburb}"/></apex:pageBlockSectionItem>
		 <apex:pageBlockSectionItem >
		<apex:outputLabel value="State"/><apex:inputText value="{!request.individual.currentAddress.state}"/></apex:pageBlockSectionItem>
		 <apex:pageBlockSectionItem >
		<apex:outputLabel value="Country"/><apex:inputText value="{!request.individual.currentAddress.country}"/></apex:pageBlockSectionItem>
		 <apex:pageBlockSectionItem >
		<apex:outputLabel value="Drivers License Number"/><apex:inputText value="{!request.individual.driversLicenseNumber}"/></apex:pageBlockSectionItem>
		 <apex:pageBlockSectionItem >
			<apex:outputLabel value="Gender"/><apex:inputText value="{!request.individual.gender}"/></apex:pageBlockSectionItem>

<!-- 		 <apex:pageBlockSectionItem > -->
<!-- 		<apex:outputLabel value="Save result to contact?"/><apex:inputCheckbox value="{!saveResultToContact}"/> -->
<!-- 		</apex:pageBlockSectionItem> -->
		</apex:pageBlockSection>

</apex:pageBlock>
	</apex:form>

	
	<apex:pageBlock id="detailList" rendered="{!(response != null)}">
			
			<apex:pageBlockSection title="General Messages" columns="1" rendered="{!response.generalMessages.size>0}">				
				<apex:dataTable value="{!response.generalMessages}" var="cch" width="65%" >
					<apex:column value="{!cch}">
						<apex:facet name="header">Messages</apex:facet>
					</apex:column>										
				</apex:dataTable>					
			</apex:pageBlockSection>		
			
			<apex:pageBlockSection title="Individual Identity" columns="1">
		
				<table width="75%">
					<tr>
						<td>Name:</td>
						<td>{!response.individual.firstGivenName} {!response.individual.familyName} {!response.individual.otherNames}</td>
					</tr>
					<tr>
						<td>Date of Birth:</td>
						<td>{!response.individual.dateOfBirth}</td>
					</tr>
					<tr>
						<td>Gender:</td>
						<td>{!response.individual.gender}</td>
					</tr>		
					<tr>
						<td>Drivers License Number:</td>
						<td>{!response.individual.driversLicenseNumber}</td>
					</tr>
					<tr>
						<td>Veda Advantage File No:</td>
						<td>{!response.individual.creditFileNumber}</td>
					</tr>
		
					<tr>
						<td>First Reported Date:</td>
						<td>{!response.individual.firstReportedDate}</td>
					</tr>										
				</table>
		
				</apex:pageBlockSection>
		
				<apex:pageBlockSection title="VedaScore Details: This application has been scored using VedaScore 1.1 " columns="1">
		
				<table width="45%">
					<tr>
						<td rowspan="4"><h1 style="font-size:30pt">{!response.score.vedaScore}</h1></td>
						<td>VedaScore:</td>
						<td>{!response.score.vedaScore}</td>
					</tr>
					<tr>
						<td>Relative Risk:</td>
						<td>{!response.score.relativeRisk}</td>
					</tr>
					<tr>
						<td>Applicant Odds:</td>
						<td>{!response.score.applicantOdds}</td>
					</tr>
					<tr>
						<td>Population Odds:</td>
						<td>{!response.score.populationScore}</td>
					</tr>						
				</table>		
				
				
				<apex:pageBlock title="" rendered="{!response.score.contributingFactors.size>0}">					
					<apex:dataTable value="{!response.score.contributingFactors}" var="cch" width="50%" >
						<apex:column value="{!cch.value}">
							<apex:facet name="header">Key Contributing Factors impacting this assessment</apex:facet>
						</apex:column>
						<apex:column value="{!cch.impact}">
							<apex:facet name="header">Impact on Risk</apex:facet>
						</apex:column>
					</apex:dataTable>
				</apex:pageBlock>	
				
		
				</apex:pageBlockSection>
		
				<apex:pageBlockSection title="Summary Characteristics" columns="1">					
					<table width="75%" border="1" >
						<tr>
							<td>Known Identities:</td>
							<td>{!response.summaryDetail.knownIdentities}</td>	
							
							<td>Age of credit file:</td>
							<td>{!response.summaryDetail.ageOfCreditFile}</td>						
						</tr>
						
						<tr>
							<td>Adverse on file:</td>
							<td>{!response.summaryDetail.adverseOnFile}</td>
						
							<td>Current Directorships:</td>
							<td>{!response.summaryDetail.currentDirectorships}</td>
						</tr>	
						<tr>
							<td>Credit Enquiries - Last 12 months:</td>
							<td>{!response.summaryDetail.creditEnqLast12Months}</td>
						
							<td>Credit Enquiries - last 5 years:</td>
							<td>{!response.summaryDetail.creditEnqLast5yrs}</td>
						</tr>
						<tr>
							<td>Previous Directorships:</td>
							<td>{!response.summaryDetail.previousDirectorships}</td>
						
							<td>Proprietorships:</td>
							<td>{!response.summaryDetail.proprietorships}</td>
						</tr>
						<tr>
							<td>Total Value of Outstanding Defaults:</td>
							<td>{!response.summaryDetail.totalValueOfOutstandingDefaults}</td>
						
							<td>File notes:</td>
							<td>{!response.summaryDetail.fileNotes}</td>
						</tr>
						<tr>
							<td>Authorised Agents - Last 12 months:</td>
							<td>{!response.summaryDetail.authorisedAgentLast12Months}</td>
						
							<td>Authorised Agents - last 5 years:</td>
							<td>{!response.summaryDetail.authorisedAgentLast5yrs}</td>
						</tr>				
					</table>
				</apex:pageBlockSection>
		
				<apex:pageBlockSection title="Other Identifying Details" columns="1">
					<apex:pageBlock title="Current Address" >
						<apex:dataTable value="{!response.individual.currentAddress}" var="cch" width="50%" >
							<apex:column value="{!cch.addressToString}">
								<apex:facet name="header">Address</apex:facet>
							</apex:column>
							<apex:column value="{!cch.createdDate}">
								<apex:facet name="header">First Reported</apex:facet>
							</apex:column>
						</apex:dataTable>						
					</apex:pageBlock>
					<hr/>
					<apex:pageBlock title="Previous Addresses" rendered="{!response.individual.previousAddresses.size>0}">					
						<apex:dataTable value="{!response.individual.previousAddresses}" var="cch" width="50%" >
							<apex:column value="{!cch.addressToString}">
								<apex:facet name="header">Address</apex:facet>
							</apex:column>
							<apex:column value="{!cch.createdDate}">
								<apex:facet name="header">First Reported</apex:facet>
							</apex:column>
						</apex:dataTable>
					</apex:pageBlock>
					<apex:pageBlock title="Employments" rendered="{!response.employments.size>0}">					
						<apex:dataTable value="{!response.employments}" var="cch" width="50%" >
							<apex:column value="{!cch.name}">
								<apex:facet name="header">Employer Name</apex:facet>
							</apex:column>
							<apex:column value="{!cch.empDate}">
								<apex:facet name="header">First Reported</apex:facet>
							</apex:column>
						</apex:dataTable>
					</apex:pageBlock>
					<apex:pageBlock title="Occupations" rendered="{!response.occupations.size>0}">					
						<apex:dataTable value="{!response.occupations}" var="cch" width="50%" >
							<apex:column value="{!cch.name}">
								<apex:facet name="header">Occupation</apex:facet>
							</apex:column>
							<apex:column value="{!cch.occpDate}">
								<apex:facet name="header">First Reported</apex:facet>
							</apex:column>
						</apex:dataTable>
					</apex:pageBlock>
					
					</apex:pageBlockSection>
					
			
		
				<apex:pageBlockSection title="Commercial Credit History" columns="1">
				
					<apex:pageBlock title="Commercial Payment Defaults" rendered="{!response.commercialCreditDefaults.size>0}">		
						<apex:repeat value="{!response.commercialCreditDefaults}" var="def">
							<table width="80%" border="1" >
								<tr>
									<td>Account Number:</td>
									<td>{!def.acctNumber}</td>	
									
									<td>Account Type:</td>
									<td>{!def.acctType}</td>						
								</tr>
								
								<tr>
									<td>Co-Borrower:</td>
									<td>{!def.coBorrower}</td>	
									
									<td>Default Status Date:</td>
									<td>{!def.defaultStatusDate}</td>						
								</tr>
								
								<tr>
									<td>Default Status Code:</td>
									<td>{!def.defaultStatusCode}</td>
									
									<td>Latest Reason To Report:</td>
									<td>{!def.latestReasonToReport}</td>		
														
								</tr>
								<tr>
									<td>Latest Credit Provider:</td>
									<td>{!def.latestCreditProvider}</td>
								
									<td>Latest Date:</td>
									<td>{!def.latestDate}</td>
								</tr>	
								<tr>
									<td>Latest Amount:</td>
									<td>{!def.latestAmount}</td>
								
									<td>Role in Payment Default:</td>
									<td>{!def.roleInPaymentDefault}</td>
								</tr>
								<tr>
									<td>Original Credit Provider:</td>
									<td>{!def.originalCreditProvider}</td>
								
									<td>Original Date:</td>
									<td>{!def.originalDate}</td>
								</tr>
								<tr>
									<td>Original Amount:</td>
									<td>{!def.originalAmount}</td>									
										
									<td>Original Reason To Report:</td>
									<td>{!def.originalReasonToReport}</td>
								</tr>												
							</table>
							<br/>
						</apex:repeat>
					</apex:pageBlock>
				
					<apex:pageBlock title="Commercial Credit Enquiries" rendered="{!response.commercialCreditEnquiries.size>0}">		
						<!--  <apex:dataTable value="{!response.commercialCreditEnquiries}" var="cch" width="75%" >
							<apex:column value="{!cch.dateOfEnquiry}">
								<apex:facet name="header">Date of Enquiry</apex:facet>
							</apex:column>
							<apex:column value="{!cch.creditEnquirer}">
								<apex:facet name="header">Credit Enquirer</apex:facet>
							</apex:column>
							<apex:column value="{!cch.accountType}">
								<apex:facet name="header">Account Type</apex:facet>
							</apex:column>
							<apex:column value="${!cch.amount}">
								<apex:facet name="header">Amount</apex:facet>
							</apex:column>
							<apex:column value="{!cch.role}">
								<apex:facet name="header">Role</apex:facet>
							</apex:column>
						</apex:dataTable> -->
						
						<apex:repeat value="{!response.commercialCreditEnquiries}" var="def">
							<table width="80%" border="1" >
								<tr>
									<td>Date of Enquiry:</td>
									<td>{!def.dateOfEnquiry}</td>	
									
									<td>Credit Enquirer:</td>
									<td>{!def.creditEnquirer}</td>						
								</tr>
								
								<tr>
									<td>Account Type:</td>
									<td>{!def.accountType}</td>	
									
									<td>Amount:</td>
									<td>{!def.amount}</td>						
								</tr>
								
								<tr>
									<td>Role:</td>
									<td>{!def.role}</td>
									
									<td>Client Reference:</td>
									<td>{!def.referenceNumber}</td>		
														
								</tr>													
							</table>
							<br/>
						</apex:repeat>
						
					</apex:pageBlock>
					
					<apex:pageBlock title="Commercial Credit Applications" rendered="{!response.commercialCreditApplications.size>0}">		
						<!-- <apex:dataTable value="{!response.commercialCreditApplications}" var="cch" width="75%" >
							<apex:column value="{!cch.dateOfEnquiry}">
								<apex:facet name="header">Date of Enquiry</apex:facet>
							</apex:column>
							<apex:column value="{!cch.creditEnquirer}">
								<apex:facet name="header">Credit Enquirer</apex:facet>
							</apex:column>
							<apex:column value="{!cch.accountType}">
								<apex:facet name="header">Account Type</apex:facet>
							</apex:column>
							<apex:column value="${!cch.amount}">
								<apex:facet name="header">Amount</apex:facet>
							</apex:column>
							<apex:column value="{!cch.role}">
								<apex:facet name="header">Role</apex:facet>
							</apex:column>
						</apex:dataTable> -->
						
						<apex:repeat value="{!response.commercialCreditApplications}" var="def">
							<table width="80%" border="1" >
								<tr>
									<td>Date of Enquiry:</td>
									<td>{!def.dateOfEnquiry}</td>	
									
									<td>Credit Enquirer:</td>
									<td>{!def.creditEnquirer}</td>						
								</tr>
								
								<tr>
									<td>Account Type:</td>
									<td>{!def.accountType}</td>	
									
									<td>Amount:</td>
									<td>{!def.amount}</td>						
								</tr>
								
								<tr>
									<td>Role:</td>
									<td>{!def.role}</td>
									
									<td>Client Reference:</td>
									<td>{!def.referenceNumber}</td>		
														
								</tr>													
							</table>
							<br/>
						</apex:repeat>
					</apex:pageBlock>
					
					<apex:pageBlock title="Commercial Authorised Agent Enquiries" rendered="{!response.commercialAuthorisedAgentEnquiries.size>0}">		
						<!-- <apex:dataTable value="{!response.commercialAuthorisedAgentEnquiries}" var="cch" width="75%" >
							<apex:column value="{!cch.dateOfEnquiry}">
								<apex:facet name="header">Date of Enquiry</apex:facet>
							</apex:column>
							<apex:column value="{!cch.creditEnquirer}">
								<apex:facet name="header">Credit Enquirer</apex:facet>
							</apex:column>
							<apex:column value="{!cch.accountType}">
								<apex:facet name="header">Account Type</apex:facet>
							</apex:column>				
							<apex:column value="${!cch.amount}">
								<apex:facet name="header">Amount</apex:facet>
							</apex:column>
							<apex:column value="{!cch.role}">
								<apex:facet name="header">Role</apex:facet>
							</apex:column>
						</apex:dataTable> -->
						
						<apex:repeat value="{!response.commercialAuthorisedAgentEnquiries}" var="def">
							<table width="80%" border="1" >
								<tr>
									<td>Date of Enquiry:</td>
									<td>{!def.dateOfEnquiry}</td>	
									
									<td>Credit Enquirer:</td>
									<td>{!def.creditEnquirer}</td>						
								</tr>
								
								<tr>
									<td>Account Type:</td>
									<td>{!def.accountType}</td>	
									
									<td>Amount:</td>
									<td>{!def.amount}</td>						
								</tr>
								
								<tr>
									<td>Role:</td>
									<td>{!def.role}</td>
									
									<td>Client Reference:</td>
									<td>{!def.referenceNumber}</td>		
														
								</tr>													
							</table>
							<br/>
						</apex:repeat>
						
					</apex:pageBlock>
					
					<apex:pageBlock title="Alerts CCLI-Lite" rendered="{!response.commercialCurrentProviders.size>0}">		
						<apex:dataTable value="{!response.commercialCurrentProviders}" var="cch" width="50%" >
							<apex:column value="{!cch.name}">
								<apex:facet name="header">Credit Provider</apex:facet>
							</apex:column>
							<apex:column value="{!cch.providedDate}">
								<apex:facet name="header">Date Credit Provided</apex:facet>
							</apex:column>							
						</apex:dataTable>
					</apex:pageBlock>
					
					<apex:pageBlock title="Commerical File Messages" rendered="{!response.commericalFileMessages.size>0}">		
						<apex:dataTable value="{!response.commericalFileMessages}" var="cch" width="40%" >
							<apex:column value="{!cch}">
								<apex:facet name="header">File Messages</apex:facet>
							</apex:column>													
						</apex:dataTable>
					</apex:pageBlock>
					
				</apex:pageBlockSection>
		
				<apex:pageBlockSection title="Consumer Credit History" columns="1">
				
					<apex:pageBlock title="Consumer Payment Defaults" rendered="{!response.consumerCreditDefaults.size>0}">		
						<apex:repeat value="{!response.consumerCreditDefaults}" var="def">
							<table width="80%" border="1" >
								<tr>
									<td>Account Number:</td>
									<td>{!def.acctNumber}</td>	
									
									<td>Account Type:</td>
									<td>{!def.acctType}</td>						
								</tr>
								
								<tr>
									<td>Co-Borrower:</td>
									<td>{!def.coBorrower}</td>	
									
									<td>Default Status Date:</td>
									<td>{!def.defaultStatusDate}</td>						
								</tr>
								
								<tr>
									<td>Default Status Code:</td>
									<td>{!def.defaultStatusCode}</td>
									
									<td>Latest Reason To Report:</td>
									<td>{!def.latestReasonToReport}</td>		
														
								</tr>
								<tr>
									<td>Latest Credit Provider:</td>
									<td>{!def.latestCreditProvider}</td>
								
									<td>Latest Date:</td>
									<td>{!def.latestDate}</td>
								</tr>	
								<tr>
									<td>Latest Amount:</td>
									<td>{!def.latestAmount}</td>
								
									<td>Role in Payment Default:</td>
									<td>{!def.roleInPaymentDefault}</td>
								</tr>
								<tr>
									<td>Original Credit Provider:</td>
									<td>{!def.originalCreditProvider}</td>
								
									<td>Original Date:</td>
									<td>{!def.originalDate}</td>
								</tr>
								<tr>
									<td>Original Amount:</td>
									<td>{!def.originalAmount}</td>									
										
									<td>Original Reason To Report:</td>
									<td>{!def.originalReasonToReport}</td>
								</tr>												
							</table>
							<br/>
						</apex:repeat>
					</apex:pageBlock>
					
					<apex:pageBlock title="Consumer Credit Enquiries" rendered="{!response.consumerCreditEnquiries.size>0}">		
						<!-- <apex:dataTable value="{!response.consumerCreditEnquiries}" var="cch" width="75%" >
							<apex:column value="{!cch.dateOfEnquiry}">
								<apex:facet name="header">Date of Enquiry</apex:facet>
							</apex:column>
							<apex:column value="{!cch.creditEnquirer}">
								<apex:facet name="header">Credit Enquirer</apex:facet>
							</apex:column>
							<apex:column value="{!cch.accountType}">
								<apex:facet name="header">Account Type</apex:facet>
							</apex:column>				
							<apex:column value="${!cch.amount}">
								<apex:facet name="header">Amount</apex:facet>
							</apex:column>
							<apex:column value="{!cch.role}">
								<apex:facet name="header">Role</apex:facet>
							</apex:column>
						</apex:dataTable> -->
						
						<apex:repeat value="{!response.consumerCreditEnquiries}" var="def">
							<table width="80%" border="1" >
								<tr>
									<td>Date of Enquiry:</td>
									<td>{!def.dateOfEnquiry}</td>	
									
									<td>Credit Enquirer:</td>
									<td>{!def.creditEnquirer}</td>						
								</tr>
								
								<tr>
									<td>Account Type:</td>
									<td>{!def.accountType}</td>	
									
									<td>Amount:</td>
									<td>{!def.amount}</td>						
								</tr>
								
								<tr>
									<td>Role:</td>
									<td>{!def.role}</td>
									
									<td>Client Reference:</td>
									<td>{!def.referenceNumber}</td>		
														
								</tr>													
							</table>
							<br/>
						</apex:repeat>
						
					</apex:pageBlock>
					
					<apex:pageBlock title="Consumer Credit Applications" rendered="{!response.consumerCreditApplications.size>0}">		
						<!-- <apex:dataTable value="{!response.consumerCreditApplications}" var="cch" width="75%" >
							<apex:column value="{!cch.dateOfEnquiry}">
								<apex:facet name="header">Date of Enquiry</apex:facet>
							</apex:column>
							<apex:column value="{!cch.creditEnquirer}">
								<apex:facet name="header">Credit Enquirer</apex:facet>
							</apex:column>
							<apex:column value="{!cch.accountType}">
								<apex:facet name="header">Account Type</apex:facet>
							</apex:column>
							<apex:column value="${!cch.amount}">
								<apex:facet name="header">Amount</apex:facet>
							</apex:column>
							<apex:column value="{!cch.role}">
								<apex:facet name="header">Role</apex:facet>
							</apex:column>
						</apex:dataTable> -->
						
						<apex:repeat value="{!response.consumerCreditApplications}" var="def">
							<table width="80%" border="1" >
								<tr>
									<td>Date of Enquiry:</td>
									<td>{!def.dateOfEnquiry}</td>	
									
									<td>Credit Enquirer:</td>
									<td>{!def.creditEnquirer}</td>						
								</tr>
								
								<tr>
									<td>Account Type:</td>
									<td>{!def.accountType}</td>	
									
									<td>Amount:</td>
									<td>{!def.amount}</td>						
								</tr>
								
								<tr>
									<td>Role:</td>
									<td>{!def.role}</td>
									
									<td>Client Reference:</td>
									<td>{!def.referenceNumber}</td>		
														
								</tr>													
							</table>
							<br/>
						</apex:repeat>
					</apex:pageBlock>
					
					<apex:pageBlock title="Consumer Authorised Agent Enquiries" rendered="{!response.consumerAuthorisedAgentEnquiries.size>0}">		
						<!--  <apex:dataTable value="{!response.consumerAuthorisedAgentEnquiries}" var="cch" width="75%" >
							<apex:column value="{!cch.dateOfEnquiry}">
								<apex:facet name="header">Date of Enquiry</apex:facet>
							</apex:column>
							<apex:column value="{!cch.creditEnquirer}">
								<apex:facet name="header">Credit Enquirer</apex:facet>
							</apex:column>
							<apex:column value="{!cch.accountType}">
								<apex:facet name="header">Account Type</apex:facet>
							</apex:column>				
							<apex:column value="${!cch.amount}">
								<apex:facet name="header">Amount</apex:facet>
							</apex:column>
							<apex:column value="{!cch.role}">
								<apex:facet name="header">Role</apex:facet>
							</apex:column>
						</apex:dataTable> -->
						
						<apex:repeat value="{!response.consumerAuthorisedAgentEnquiries}" var="def">
							<table width="80%" border="1" >
								<tr>
									<td>Date of Enquiry:</td>
									<td>{!def.dateOfEnquiry}</td>	
									
									<td>Credit Enquirer:</td>
									<td>{!def.creditEnquirer}</td>						
								</tr>
								
								<tr>
									<td>Account Type:</td>
									<td>{!def.accountType}</td>	
									
									<td>Amount:</td>
									<td>{!def.amount}</td>						
								</tr>
								
								<tr>
									<td>Role:</td>
									<td>{!def.role}</td>
									
									<td>Client Reference:</td>
									<td>{!def.referenceNumber}</td>		
														
								</tr>													
							</table>
							<br/>
						</apex:repeat>
						
					</apex:pageBlock>
					
					<apex:pageBlock title="Alerts CCLI-Lite" rendered="{!response.consumerCurrentProviders.size>0}">		
						<apex:dataTable value="{!response.consumerCurrentProviders}" var="cch" width="50%" >
							<apex:column value="{!cch.name}">
								<apex:facet name="header">Credit Provider</apex:facet>
							</apex:column>
							<apex:column value="{!cch.providedDate}">
								<apex:facet name="header">Date Credit Provided</apex:facet>
							</apex:column>							
						</apex:dataTable>
					</apex:pageBlock>
					
					<apex:pageBlock title="Consumer File Messages" rendered="{!response.consumerFileMessages.size>0}">		
						<apex:dataTable value="{!response.consumerFileMessages}" var="cch" width="40%" >
							<apex:column value="{!cch}">
								<apex:facet name="header">File Messages</apex:facet>
							</apex:column>													
						</apex:dataTable>
					</apex:pageBlock>		
					
					
				</apex:pageBlockSection>
		
				<apex:pageBlockSection title="Public Record Information" columns="1">
				
					<apex:pageBlock title="Court Action Details" rendered="{!response.courtActions.size>0}">		
						<apex:repeat value="{!response.courtActions}" var="def">
							<table width="80%" border="1" >
								<tr>
									<td>Court Action Type:</td>
									<td>{!def.courtActionType}</td>	
									
									<td>Creditor:</td>
									<td>{!def.creditor}</td>						
								</tr>
								
								<tr>
									<td>Court Action Date:</td>
									<td>{!def.actionDate}</td>
								
									<td>Court Action Amount:</td>
									<td>{!def.courtActionAmount}</td>
								</tr>	
								<tr>
									<td>Plaint Number:</td>
									<td>{!def.plaintNumber}</td>
								
									<td>Role Type:</td>
									<td>{!def.roleType}</td>
								</tr>
								<tr>
									<td>Court Type:</td>
									<td>{!def.courtType}</td>
								
									<td>Co-Borrower:</td>
									<td>{!def.coBorrower}</td>
								</tr>
								<tr>
									<td>Court Action Status Date:</td>
									<td>{!def.courtActionStatusDate}</td>
									
									<td>Court Action Status Code:</td>
									<td>{!def.courtActionStatusCode}</td>
								</tr>												
							</table>
							<br/>
						</apex:repeat>
					</apex:pageBlock>
					
					<apex:pageBlock title="Directorship Details" rendered="{!response.directorships.size>0}">		
						<apex:repeat value="{!response.directorships}" var="def">
							<table width="75%" border="1" >
								<tr>
									<td>Directorship Type:</td>
									<td>{!def.directorshipType}</td>	
									
									<td>Date Appointed:</td>
									<td>{!def.dateAppointed}</td>						
								</tr>
								
								<tr>
									<td>Date Ceased:</td>
									<td>{!def.dateCeased}</td>
								
									<td>Date Last KnowAs Director:</td>
									<td>{!def.dateLastKnownAsDirector}</td>
								</tr>	
								<tr>
									<td>Org. Bureau Reference:</td>
									<td>{!def.orgBureauReference}</td>
								
									<td>Org. Name:</td>
									<td>{!def.orgName}</td>
								</tr>
								<tr>
									<td>Org. Type Code:</td>
									<td>{!def.orgTypeCode}</td>
								
									<td>Org. Status Code:</td>
									<td>{!def.orgStatusCode}</td>
								</tr>
								<tr>
									<td>Org. Number:</td>
									<td>{!def.orgNumber}</td>
									
									<td>Org. ABN:</td>
									<td>{!def.ABN}</td>
								</tr>												
							</table>
							<br/>
						</apex:repeat>
					</apex:pageBlock>
				
					<apex:pageBlock title="Bankruptcy Details" rendered="{!response.bankruptcies.size>0}">		
						<apex:dataTable value="{!response.bankruptcies}" var="cch" width="75%" >
							<apex:column value="{!cch.bankruptcyType}">
								<apex:facet name="header">Type</apex:facet>
							</apex:column>
							<apex:column value="{!cch.dateDeclared}">
								<apex:facet name="header">Date Declared</apex:facet>
							</apex:column>
							<apex:column value="{!cch.narrative}">
								<apex:facet name="header">Narrative</apex:facet>
							</apex:column>				
							<apex:column value="${!cch.dischargeStatus}">
								<apex:facet name="header">Discharge Status</apex:facet>
							</apex:column>
							<apex:column value="{!cch.role}">
								<apex:facet name="header">Role</apex:facet>
							</apex:column>
							<apex:column value="{!cch.coborrower}">
								<apex:facet name="header">Co-Borrower</apex:facet>
							</apex:column>
						</apex:dataTable>
					</apex:pageBlock>
					
					<apex:pageBlock title="Proprietorship Details" rendered="{!response.proprietorships.size>0}">		
						<apex:dataTable value="{!response.proprietorships}" var="cch" width="75%" >
							<apex:column value="{!cch.dateAppointed}">
								<apex:facet name="header">Date Appointed</apex:facet>
							</apex:column>
							<apex:column value="{!cch.businessBureauReference}">
								<apex:facet name="header">Bureau Reference</apex:facet>
							</apex:column>
							<apex:column value="{!cch.businessName}">
								<apex:facet name="header">Business Name</apex:facet>
							</apex:column>				
							<apex:column value="${!cch.businessAbn}">
								<apex:facet name="header">Business ABN</apex:facet>
							</apex:column>
							<apex:column value="{!cch.businessRegState}">
								<apex:facet name="header">Business Reg. State</apex:facet>
							</apex:column>
							<apex:column value="{!cch.businessRegNumber}">
								<apex:facet name="header">Business Reg. Number</apex:facet>
							</apex:column>
						</apex:dataTable>
					</apex:pageBlock>
					
					<apex:pageBlock title="Disqualified Directorship Details" rendered="{!response.disqualifiedDirectorships.size>0}">		
						<apex:dataTable value="{!response.disqualifiedDirectorships}" var="cch" width="50%" >
							<apex:column value="{!cch.dateDisqualified}">
								<apex:facet name="header">Date Disqualified</apex:facet>
							</apex:column>
							<apex:column value="{!cch.dateDisqualifiedUntil}">
								<apex:facet name="header">Date Disqualified Until</apex:facet>
							</apex:column>							
						</apex:dataTable>
					</apex:pageBlock>
					
					<apex:pageBlock title="Public File Messages" rendered="{!response.publicFileMessages.size>0}">		
						<apex:dataTable value="{!response.publicFileMessages}" var="cch" width="40%" >
							<apex:column value="{!cch}">
								<apex:facet name="header">File Messages</apex:facet>
							</apex:column>													
						</apex:dataTable>
					</apex:pageBlock>
					
				</apex:pageBlockSection>
		
					
		
				<apex:pageBlockSection title="File Notes" columns="1" rendered="{!response.notes.size>0}">
				
					<apex:dataTable value="{!response.notes}" var="cch" width="50%" >
						<apex:column value="{!cch.dateRecorded}">
							<apex:facet name="header">Date Recorded</apex:facet>
						</apex:column>
						<apex:column value="{!cch.fileNote}">
							<apex:facet name="header">File Note</apex:facet>
						</apex:column>							
					</apex:dataTable>					
				</apex:pageBlockSection>	
		
				<apex:pageBlockSection title="Cross References to Other Files" columns="1">
					
					<apex:pageBlock title="Consumer Reference Files" rendered="{!response.consumerIndividualCrossReference.size>0}">
						<apex:dataTable value="{!response.consumerIndividualCrossReference}" var="cch" width="65%" >
							<apex:column value="{!cch.fullName}">
								<apex:facet name="header">Name</apex:facet>
							</apex:column>
							<apex:column value="{!cch.bureauReference}">
								<apex:facet name="header">Veda File</apex:facet>
							</apex:column>
							<apex:column value="{!cch.createdDate}">
								<apex:facet name="header">Cross Reference Create Date</apex:facet>
							</apex:column>
						</apex:dataTable>						
					</apex:pageBlock>
					
					<apex:pageBlock title="Commercial Reference Files" rendered="{!response.commercialIndividualCrossReference.size>0}">
						<apex:dataTable value="{!response.commercialIndividualCrossReference}" var="cch" width="65%" >
							<apex:column value="{!cch.fullName}">
								<apex:facet name="header">Name</apex:facet>
							</apex:column>
							<apex:column value="{!cch.bureauReference}">
								<apex:facet name="header">Veda File</apex:facet>
							</apex:column>
							<apex:column value="{!cch.createdDate}">
								<apex:facet name="header">Cross Reference Create Date</apex:facet>
							</apex:column>
						</apex:dataTable>						
					</apex:pageBlock>
		
				</apex:pageBlockSection>	
		
				<apex:pageBlockSection title="Other Possible Matching File" columns="1">
					<apex:pageBlock title="Possible Match Details" rendered="{!response.possibleMatch.familyName!=null}">
						<table width="75%" border="1" >
							<tr>
								<td>Bureau Reference:</td>
								<td>{!response.possibleMatch.bureauReference}</td>	
								
								<td>Gender:</td>
								<td>{!response.possibleMatch.gender}</td>						
							</tr>
							
							<tr>
								<td>Date of Birth:</td>
								<td>{!response.possibleMatch.dateOfBirth}</td>
							
								<td>Drivers Licence Number:</td>
								<td>{!response.possibleMatch.driversLicenseNumber}</td>
							</tr>	
							<tr>
								<td>Name:</td>
								<td>{!response.possibleMatch.firstGivenName} {!response.possibleMatch.familyName} {!response.possibleMatch.otherNames}</td>
																						
								<td>Enquiry Amount:</td>
								<td>{!response.possibleMatch.enquiryAmount}</td>
							</tr>
							<tr>
								<td>Credit File Number:</td>
								<td>{!response.possibleMatch.creditFileNumber}</td>
								
								<td>First Reported Date:</td>
								<td>{!response.possibleMatch.firstReportedDate}</td>
							</tr>												
						</table>
					</apex:pageBlock>
					
					<apex:pageBlock title="Current Address" rendered="{!response.possibleMatch.currentAddress.createdDate!=null}">
						<apex:dataTable value="{!response.possibleMatch.currentAddress}" var="cch" width="50%" >
							<apex:column value="{!cch.addressToString}">
								<apex:facet name="header">Address</apex:facet>
							</apex:column>
							<apex:column value="{!cch.createdDate}">
								<apex:facet name="header">First Reported</apex:facet>
							</apex:column>
						</apex:dataTable>						
					</apex:pageBlock>
					
					<apex:pageBlock title="Previous Addresses" rendered="{!response.possibleMatch.previousAddresses.size>0}">					
						<apex:dataTable value="{!response.possibleMatch.previousAddresses}" var="cch" width="50%" >
							<apex:column value="{!cch.addressToString}">
								<apex:facet name="header">Address</apex:facet>
							</apex:column>
							<apex:column value="{!cch.createdDate}">
								<apex:facet name="header">First Reported</apex:facet>
							</apex:column>
						</apex:dataTable>
					</apex:pageBlock>
					<apex:pageBlock title="Employments" rendered="{!response.possibleMatch.employments.size>0}">					
						<apex:dataTable value="{!response.possibleMatch.employments}" var="cch" width="50%" >
							<apex:column value="{!cch.name}">
								<apex:facet name="header">Employer Name</apex:facet>
							</apex:column>
							<apex:column value="{!cch.empDate}">
								<apex:facet name="header">First Reported</apex:facet>
							</apex:column>
						</apex:dataTable>
					</apex:pageBlock>
					<apex:pageBlock title="Occupations" rendered="{!response.possibleMatch.occupations.size>0}">					
						<apex:dataTable value="{!response.possibleMatch.occupations}" var="cch" width="50%" >
							<apex:column value="{!cch.name}">
								<apex:facet name="header">Occupation</apex:facet>
							</apex:column>
							<apex:column value="{!cch.occpDate}">
								<apex:facet name="header">First Reported</apex:facet>
							</apex:column>
						</apex:dataTable>
					</apex:pageBlock>
		
				</apex:pageBlockSection>
		
	    <!-- <script>
        
           	ifrm = document.createElement("IFRAME"); 
			ifrm.setAttribute("src", "/apex/VedaIndividualPagePrintable?attachID={!attachmentId}&objAttId={!idOfObjToAttach}&objType={!sObjectType}"); 
			ifrm.style.width = 0+"px";  
 			ifrm.style.height = 0+"px";  
			document.body.appendChild(ifrm); 
      
    	</script> -->					 

	</apex:pageBlock>

	<script>

       function setTabTitle() {
            console.log('IsinConsole:'+sforce.console.isInConsole());
            if (sforce.console.isInConsole()){
                sforce.console.setTabTitle('Veda Credit Check');
            }
       }       
      
       var previousOnload = window.onload;        
       window.onload = function() { 
            if (previousOnload) { 
                previousOnload();
            }                
            setTimeout('setTabTitle()', '500'); 
       }
       console.log('Veda Credit Check loaded');

    </script>

</apex:page>
<!--
//@author: Jesfer Baculod - Positive Group
//@history: 08/24/17 - Created
			11/6/17 - Updated hiding Notes in Sidebar on Create record
            11/9/17 - Added refreshing current tab on created Note
			14/9/19 - Added support for hiding Notes sidebar on VF pages
//@description: VF page for creating Notes on a specific object  - (page should be included as a console component on page layouts)
-->
<apex:page showHeader="false" controller="CreateNotesCC" >
	<head>
		<meta charset="utf-8" />
		  <meta http-equiv="x-ua-compatible" content="ie=edge" />
		  <title>Notes</title>
		  <meta name="viewport" content="width=device-width, initial-scale=1" />
		  <!-- Import the Design System style sheet -->
		  <apex:stylesheet value="{!URLFOR($Resource.SLDS, 'assets/styles/salesforce-lightning-design-system-vf.min.css')}" />
	</head>

	 <apex:includeScript value="/support/console/39.0/integration.js"/>
	 <apex:includeScript value="/soap/ajax/39.0/connection.js" />
	 <apex:includeScript value="/soap/ajax/39.0/apex.js" />

	 <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" lang="en">
	 <div class="PLS">
		<div class="slds-page-header">
		  <div class="slds-media">
		    <div class="slds-media__figure">
		      <span class="slds-icon_container slds-icon-standard-{!LOWER(objType)}" title="{!objType}">
		        <svg class="slds-icon" aria-hidden="true">
		          <use xlink:href="{!URLFOR($Resource.SLDS,'/assets/icons/standard-sprite/svg/symbols.svg#' & LOWER(objType))}"></use>
		        </svg>
		      </span>
		    </div>
		    <div class="slds-media__body">
		      <h1 class="slds-page-header__title slds-truncate slds-align-middle" style="position:relative; top: 5px;" title="{!ObjType} Notes">Notes</h1>
		    </div>
		  </div>
		</div>
	 </div>

	<apex:form id="mainFrm" rendered="{!displaycnotes}">
		<apex:actionFunction name="refreshNotes" action="{!refreshNotes}" reRender="mainFrm" />
        <apex:actionFunction name="reloadNote" action="{!reloadNote}" reRender="mainFrm, opscripts" >
            <apex:param value="" name="objID" assignTo="{!curID}" />
        </apex:actionFunction>

		<apex:pageBlock mode="detail">
			<apex:pageMessages />
            <apex:pageMessage summary="Note added on {!ObjType}. Please wait for a moment." severity="confirm" strength="2" rendered="{!saveSuccess}" />
			<apex:pageBlockButtons location="bottom">
				<table>
                    <tr>
                        <td>
                            <apex:commandButton value="Create" status="status" action="{!saveNote}" id="btnCreate" reRender="mainFrm, opscripts" oncomplete="if('{!withErrors}' == 'false') refreshcurrentTab();" disabled="{!saveSuccess}"/>
                        </td>
                        <td>
                            <apex:actionStatus id="status" onstart="document.getElementById('divstatus').style.display = 'block';" onstop="document.getElementById('divstatus').style.display = {!IF(saveSuccess,'block;','none;')}" />
                            <div id="divstatus" style="display:{!IF(saveSuccess,'block;','none;')} position: relative; left: -75px;">
                                    <img src="/img/loading.gif" />
                            </div>
                        </td>
                    </tr>
                </table>
			</apex:pageBlockButtons>
			<apex:pageBlockSection columns="1">
				<apex:pageBlockSectionItem >
					<apex:inputText value="{!note.Title}" id="ittitle" html-placeholder="Untitled Note" disabled="{!saveSuccess}"/>
				</apex:pageBlockSectionItem>
				<apex:pageBlockSectionItem >
					<apex:inputTextArea value="{!notebody}" id="itbody" rows="8" style="width: 100%;" disabled="{!saveSuccess}"/>
				</apex:pageBlockSectionItem>
			</apex:pageBlockSection>
		</apex:pageBlock>

		<apex:pageBlock title="Notes" id="pbnotes">
				<apex:outputPanel rendered="{!IF(cdllist.empty,false,true)}">
					<apex:pageBlockTable value="{!cdllist}" var="cnwr" style="max-width: 300px;">
						<apex:column headerValue="{!$ObjectType.ContentVersion.fields.Title.label}">
                            <apex:commandLink title="{!LEFT(cnwr.cv.TextPreview,250)}" onclick="openNote('{!cnwr.cdl.ContentDocumentId}','{!JSENCODE(cnwr.cdl.ContentDocument.Title)}');" rerender="mainFrm">{!cnwr.cdl.ContentDocument.Title}</apex:commandLink>
                        </apex:column>
                        <apex:column headerValue="Text Preview" >
                        	<apex:outputText value="{!LEFT(cnwr.cv.TextPreview,10)}..." />
                        </apex:column>
						<apex:column value="{!cnwr.cdl.ContentDocument.LastModifiedDate}" />
					</apex:pageBlockTable>
				</apex:outputPanel>
				<apex:outputPanel rendered="{!IF(cdllist.empty,true,false)}">
					No records to display
				</apex:outputPanel>
		</apex:pageBlock>
	</apex:form>

	<apex:outputpanel id="opscripts">
    <script type="text/javascript">

		var noteID;
		var noteURL;
		var noteTitle;
		function openNote(recID,recName){
			noteID = recID;
			noteTitle = recName;
			noteURL = '/' + noteID + '?isdtp=vw';
			sforce.console.getEnclosingPrimaryTabId(openSubtab);
		}


      	var callback = function (result) {};
      	var curTabID; var objID;
      	var curID = '{!curID}';
      	var onRefresh = false;

      	getFocusedPrimaryTabId();
      	//getEnclosingPrimaryTabObjectId();
      	getFocusedSubtabId();
        if ('{!displaycnotes}' == 'true') setSidebarVisible(true);
        else{ 
            setSidebarVisible(false);
            //console.log(typeof reloadNote);
            //if (typeof reloadNote != 'undefined') 
            reloadNote(objID);
        }
     
    
     	function setSidebarVisible(show) {
       		sforce.console.setSidebarVisible(show,curTabID,sforce.console.Region.RIGHT,callback);
  		}

        function refreshcurrentTab(){
            getFocusedSubtabId();
            getFocusedPrimaryTabId();
        }

  		function getFocusedSubtabId(){
  			sforce.console.getFocusedSubtabId(showTabId);
  		}

      	function getFocusedPrimaryTabId() {
        		sforce.console.getFocusedPrimaryTabId(showTabId);
    	}

    	var showTabId = function showTabId(result) {
		    //Display the tab ID
		    console.log('Result'+result);
        	console.log('Tab ID: ' + result.id);
        	curTabID = result.id;
        	console.log('Primary Tab:'+curTabID);
            if ('{!saveSuccess}' == 'true'){ 
                sforce.console.refreshSubtabById(curTabID, true, function(result){
                        //sforce.console.onEnclosingTabRefresh(onRefreshEvent);
                        console.log(result);
                        if (result.success == false){
                            location.href = '/apex/CreateNotes?id=' + curID; //Open Notes Sidebar
                        }
                });
            }
    	};

    	function getEnclosingPrimaryTabObjectId(){
    		sforce.console.getEnclosingPrimaryTabObjectId(showObjectId);
    	}

    	function getFocusedPrimaryTabObjectId(){
    		sforce.console.getFocusedPrimaryTabObjectId(showObjectId);
    	}

    	function getFocusedSubtabObjectId() {
            sforce.console.getFocusedSubtabObjectId(showObjectId);
        }

        var getObjectId = function getObjectId(result){
        	var resID = result.Id;
		}
		
		var showtablinkcallback = function(result){
			var tlink = result.tabLink;
			console.log('tabLink: '+tlink);
			//identify if current tab is a custom VF page / Lightning App / Component
			if (tlink.includes('apex') || tlink.includes('.app')) setSidebarVisible(false);
			else setSidebarVisible(true);
			console.log('objID: '+ objID);
			if (objID.startsWith('500')){ //SubTab is a Case
        		if (!onRefresh){
        			location.href = '/apex/CreateCaseCommentsLC?id='+objID; //Open Case Comments Sidebar
	        		setSidebarVisible(true);
        		}
        	}
        	else if( objID.startsWith('006') || objID.startsWith('00Q') || objID.startsWith('a48')){ //Focused SabTab is an Opportunity / Lead / Partner
        		if (!onRefresh || '{!saveSuccess}' == 'true'){
        			console.log('refresh');
        			location.href = '/apex/CreateNotes?id=' + objID; //Open Notes Sidebar
	        		setSidebarVisible(true);
        		}
        	}
        	else{
        		console.log('redirected to parent');
        		location.href = '/apex/ConsoleSideBarParentVF';
        		setSidebarVisible(false);
        	}
		}

        var showObjectId = function showObjectId(result) {
            console.log('showObjectID start');
        	console.log('Result'+result);
        	console.log('Primary Tab:'+curTabID);
            // Display the object ID
        	console.log('Object ID: ' + result.id);
        	var resID = result.id;
			objID = result.id;
			sforce.console.getTabLink(sforce.console.TabLink.TAB_ONLY, null, showtablinkcallback);
    	};

		var openSubtab = function openSubtab(result) {
            var primaryTabId = result.id;
            console.log(noteURL);
            var s = sforce.console.openSubtab(primaryTabId , noteURL, false, noteTitle, null,openSuccess);
        };

        var openSuccess = function openSuccess(result){
        	var subtabId = result.id;
        	console.log(subtabId);
        	sforce.console.focusSubtabById(subtabId);
        }

        var onFocusEvent = function (result){
                console.log('Note in focus');
                refreshNotes();
                onRefresh = false;
                getFocusedSubtabObjectId();
           };

        var onRefreshEvent = function (result){
                console.log('Record Tab refresh');
                //refreshNotes();
                //getEnclosingPrimaryTabObjectId();
                getFocusedSubtabObjectId();
                onRefresh = true;
                //getFocusedPrimaryTabObjectId();
           };

        sforce.console.onFocusedSubtab(onFocusEvent);
        sforce.console.onEnclosingTabRefresh(onRefreshEvent);
        
        console.log(curID);
        

	</script>
    </apex:outputpanel>


	</html>
</apex:page>
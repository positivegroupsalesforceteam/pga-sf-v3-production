<!--
//@author: Rexie David - Positive Group
//@history: 12/07/2018- Updated
//@description: VF page for Submitting Requests on Case
-->
<apex:page tabstyle="Submission_Request_CT__c" controller="SubmissionRequest_CaseNM" sidebar="false">
    <apex:includeScript value="{!URLFOR($Resource.jquery_3_1_1,'jquery-3.1.1.min.js')}"/>
    <apex:includeScript value="/support/console/39.0/integration.js"/>
    <apex:includeScript value="/soap/ajax/39.0/connection.js" />
    <apex:includeScript value="/soap/ajax/39.0/apex.js" />

    <style>
        #addressOverlay{
            position: fixed; /* Sit on top of the page content */
            display: none; /* Hidden by default */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0; 
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.2); /*Black background with opacity */
            z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
            cursor: pointer; /* Add a pointer on hover */
        }

        #overlay{
            position: fixed; /* Sit on top of the page content */
            display: none; /* Hidden by default */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0; 
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255,255,255,0.5); /* rgba(0,0,0,0.2); Black background with opacity */
            z-index: 3; /* Specify a stack order in case you're using a different order for other elements */
            cursor: pointer; /* Add a pointer on hover */
        }

        #ovdiv{
            position: absolute;
            top: 50%;
            left: 50%;
            font-size: 50px;
            color: white;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
        }

        .ovdiv2{
            position: absolute;
            top: 50%;
            left: 50%;
            width: 40%;
            background-color: #EEE5E3;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
        }
    </style>

    
    <!-- oncomplete="if ('{!saveSuccess}' == 'true'){ backtoCase('save'); location.href = '/{!subReqID}?isdtp=vw';} " -->
    <apex:sectionHeader title="Submission Request" subtitle="{!cse.CaseNumber}" />
    <apex:form id="mainFrm">

        <apex:actionFunction name="af_BackToCase" action="{!caseid}" />
        <apex:actionFunction name="af_save_case" action="{!submit}" oncomplete="if ('{!saveSuccess}' == 'true'){ backtoCase('save'); location.href = '/{!subReq.Id}?isdtp=vw';}" status="status1" reRender="mainFrm"/>

        <div id="overlay">
            <div id="ovdiv"><img src="/img/loading.gif" /></div>
        </div>
        <apex:actionStatus id="status1" onstart="document.getElementById('overlay').style.display = 'block';" onstop="document.getElementById('overlay').style.display = 'none'; " style="align:left;" />
        
        <apex:pageBlock mode="edit" title="">
            <apex:pageMessage severity="INFO" rendered="{!!showSection}" strength="1" summary="Application not valid for submission. Please make sure the Case has an Application and Lender." />
            <apex:pageMessages />
            <apex:pageBlockButtons location="both">
                <!-- <apex:commandButton action="{!saveUpdates}" value="Save"/> -->
                <!-- <apex:commandButton action="{!readyforSub}" value="Validate Submission" reRender="thePageBlock,opscripts"/> -->
                <!-- <apex:commandButton action="{!submit}" value="Submit" />-->
                <apex:outputPanel rendered="{!showSection}">
                	<button type="button" id="btnSave" style="padding:5px;" onclick="af_save_case();">Submit</button>
                </apex:outputPanel>
    
                <button type="button" id="btnCancel" style="padding:5px;" onclick="backtoCase('Cancel')">Cancel</button>
            </apex:pageBlockButtons>

            <apex:pageBlockSection title="Submission Checklist" columns="2" rendered="{!showSection}">
                <apex:inputField required="true" value="{!subReq.Have_signed_privacy_consent__c}" />
                <apex:inputField value="{!subReq.Latitude_Privacy_Accepted__c}" />
                <apex:inputField value="{!subReq.Partner_Pay_Slip_Obtained__c}" />
                <apex:inputField required="true" value="{!subReq.Have_checked_income_on_payslips__c}" />
                <apex:inputField value="{!subReq.Have_checked_3_months_of_statements__c}" />
                <apex:inputField required="true" value="{!subReq.Support_Docs_Combined__c}" />
                <apex:inputField required="true" label="I confirm Salesforce record is accurate" value="{!subReq.Salesforce_Record_Accurate__c}" />
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Application Details" columns="2" rendered="{!showSection}">
                <apex:outputField value="{!cse.CaseNumber}" />
                <apex:pageBlockSectionItem />
                <apex:outputField value="{!subReq.Application_Type1__c}" />
                <apex:pageBlockSectionItem />
                <apex:outputField value="{!subReq.Lender1__c}" />
                <apex:pageBlockSectionItem />
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Ready For Submission Notes" for="mId"/>
                    <apex:outputPanel styleClass="requiredInput" layout="block">
                        <apex:outputPanel styleClass="requiredBlock" layout="block"/>
                        <apex:inputTextArea value="{!subReq.Ready_For_Submission_Notes1__c}" id="mId" rows="10" style="width: 300px;"/>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Notes For Lender" for="mId2"/>
                    <apex:outputPanel styleClass="requiredInput" layout="block">
                        <apex:outputPanel styleClass="requiredBlock" layout="block"/>
                        <apex:inputTextArea value="{!subReq.Notes_For_Lender1__c}" id="mId2" rows="10" style="width: 300px;"/>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    
    </apex:form>
    <apex:outputpanel id="opscripts">
        <script type="text/javascript">

            function backtoCase(event){
                console.log('IsinConsole:'+sforce.console.isInConsole());
                if (sforce.console.isInConsole()){
                    if (event == 'save'){
                        //Refresh Opp Detail tab
                        sforce.console.getEnclosingPrimaryTabId(showTabId);
                    }
                    else{
                        //Close current tab and open Primary Tab (Opp Detail)
                        console.log(sforce.console.getEnclosingTabId(closeSubtab));
                        sforce.console.getEnclosingTabId(closeSubtab);
                    }
                }
                else af_BackToCase();
           }

           var showTabId = function showTabId(result){
                var tabId = result.id;
                console.log(tabId);
                sforce.console.refreshPrimaryTabById(tabId , true);
           };

           var closeSubtab = function closeSubtab(result) {
                var tabId = result.id;
                console.log(tabId);
                sforce.console.closeTab(tabId);
            };

            function setTabTitle() {
                console.log('IsinConsole:'+sforce.console.isInConsole());
                if (sforce.console.isInConsole()){
                    sforce.console.setTabTitle('Submission Request');
                }
            }       
          
            var previousOnload = window.onload;        
            window.onload = function() { 
                if (previousOnload) { 
                    previousOnload();
                }                
                setTimeout('setTabTitle()', '500'); 
            }
           
            console.log('loaded');
        </script>
    </apex:outputpanel>
</apex:page>
<!--
//@author: Srikanth Vivaram, Jesfer Baculod - Positive Group
//@history: 07/19/17 - Updated, Added LPI Amount
            07/20/17 - Updated, Added Product field
            07/28/17 - Updated, changed how LDR gets accessed and closed
            07/31/17 - Updated, modified Vendor Details display
            09/12/17 - Removed rerender Attribute on buttons due to Approval Condition Rich Text field
//@description: VF page for creating Loan Document Request Form records
-->
<apex:page standardController="case" extensions="LoanDocumentRequestFormCase" sidebar="false" showHeader="false">
    <apex:sectionHeader title="Loan Document Request Form" subtitle="{!cseObj.CaseNumber}"/>
<style>
    
    
    .back-opp {
        margin:7px;
        margin-top:40px;
    }
    .back-opp a{
        text-decoration:none;
    }
    .back-opp a:hover{
        text-decoration:none;
    }
    .btn-primary {
        background:#0099cc;
        color:#ffffff;
        border:none;
        padding:7px;
        font-weight:700;
        letter-spacing:1px;
    }

    .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary {
        background: #33a6cc;
    }

    .btn-primary:active, .btn-primary.active {
        background: #007299;
        box-shadow: none;
    }

    img.pageTitleIncon {
        display:none;
    }
    div.ptBody.secondaryPalette.brandSecondaryBrd {
        background: #0099cc;
        color:#ffffff;
        padding:7px;
        border: 0 none;
        font-weight:700;
        letter-spacing: 1px;
    } 
    div.bPageTitle div.content h1.pageType {
        color: white;
        margin-left: 5px;
        font-size: 2.8em;
    }
    div.bPageTitle div.content h2.pageDescription {
        font-size:1.4em;
    }
    div.ptBody.secondaryPalette.brandSecondaryBrd{
        background: #0099cc;
        color: #ffffff;
        padding: 7px;
        border: 0 none;
        font-weight: 700;
        letter-spacing: 1px;
    }
    .pbBody div.pbSubheader.brandTertiaryBgr.tertiaryPalette{
        background-color: #4da6ff;
    }
    table.exceptionText td {
        font-style:italic;
        font-size:16px;
        font-weight:bold;
        text-align:left;
        color:#F75D59;
    }

    .greenColor{
        background-color:#ecffe6;
    }
    .redColor{
        backGround-color:#F75D59;
    }

    div.message.errorM3{
        background-color: #f7e6ff;
    }

</style>

    <apex:includeScript value="/support/console/36.0/integration.js"/>

    <apex:form id="mainFrm">
         <apex:actionFunction name="refreshOppVD" action="{!refreshCaseDetails}" reRender="pbsVD" />
         <apex:actionFunction name="af_BackToCase" action="{!caseId}" />
         <div class="back-opp"> 
            <a href="#" onclick="backtoCase()"><span class="btn-primary">&larr; Back To case</span></a>
        </div>  
        <apex:pageBlock >
            <apex:pageMessages escape="false"  />
            <apex:pageblockButtons location="top">
                <!-- rerender attribute removed due to Approval Condition rich text field -->
                <apex:commandButton value="Save" status="loadingStatus" action="{!saveUpdates}" />
                <apex:commandButton value="Validate LDR" status="loadingStatus" action="{!validateLdr}" />
                <apex:commandButton value="Submit" action="{!submit}" status="loadingStatus" />
                <apex:outputpanel >
                    <apex:actionStatus id="loadingStatus" onstart="document.getElementById('divstatus').style.visibility = 'visible';" onstop="document.getElementById('divstatus').style.visibility = 'hidden'; " />
                    <div id="divstatus" style="visibility: hidden; position:relative; top: -20px; left: -30px;">
                        <img src="/img/loading.gif" /> <strong></strong>
                    </div>
                </apex:outputpanel>    
            </apex:pageblockButtons>
            <apex:pageblockSection >
            <apex:outputText value="{!cseObj.CaseNumber}"/>
            <apex:pageblockSectionItem ></apex:pageblockSectionItem>
            <apex:inputfield value="{!cseObj.Application_Type__c}"/>
            <apex:pageBlockSectionItem />
            <apex:inputfield value="{!cseObj.Internal_Customer_Rating__c}" rendered="{!IF(cseObj.Loan_Type__c != 'Personal Loan',true,false)}" />
            </apex:pageblockSection>
            <apex:pageBlockSection title="Docs Request Checklist">
                <apex:inputField value="{!cseObj.Approval_Conditions__c}" style="width:330px; height:100px;"/>
                <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$ObjectType.Loan_Document_Request_Form__c.fields.Payout_Valid_min_5_days__c.label}"/><apex:inputCheckbox value="{!payoutValidMin5Days}"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$ObjectType.Loan_Document_Request_Form__c.fields.Notes_to_Lender__c.label}"/><apex:inputTextarea value="{!notesToLender}" style="width:330px;height:100px;"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$ObjectType.Loan_Document_Request_Form__c.fields.Payout_Letter_in_File__c.label}"/><apex:inputCheckbox value="{!payoutLetterInFile}"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.Loan_Document_Request_Form__c.fields.Notes_To_Support__c.label}"/><apex:inputTextarea value="{!notesToSupport}" style="width:330px;height:100px;"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$ObjectType.Loan_Document_Request_Form__c.fields.Received_Insurance_Waiver__c.label}"/><apex:inputCheckbox value="{!receivedInsuranceWaiver}"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:selectList id="insurancePackage" value="{!insurancePackage}" size="1" style="float:left;" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.Insurance_Package__c.label}">
                    <apex:selectOptions value="{!insurancePackageSp}"/>
                </apex:selectList>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$ObjectType.Loan_Document_Request_Form__c.fields.Insurance_Package_Notes_COMPULSORY__c.label}"/><apex:inputTextarea value="{!insurancePackageNotesCOMPULSORY}" style="width:330px;height:100px;"/>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection title="Finance Details">                    
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputField value="{!cseObj.Vehicle_Purchase_Price__c}"/>
                <apex:inputField value="{!cseObj.Lender__c}" html-disabled="true"/>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputField value="{!cseObj.Loan_Type__c}" html-disabled="true"/>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputField value="{!cseObj.Loan_Term__c}"/>
                <apex:inputText value="{!cseObj.Cash_Deposit__c}"/>
                <apex:inputField value="{!cseObj.Loan_Rate__c}"/>
                <apex:inputField value="{!cseObj.Balloon__c}"/>
                <apex:inputField value="{!cseObj.Base_Rate__c}" /> 
                <apex:pageBlockSectionItem rendered="{!IF(cseObj.Loan_Type__c != 'Personal Loan',false,true)}" />
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!IF(cseObj.Lender__c == 'ANZ',false,true)}" /> 
                <apex:pageBlockSectionItem rendered="{!IF(cseObj.Lender__c == 'ANZ',true,false)}">
                    <apex:outputLabel value="{!$ObjectType.Loan_Document_Request_Form__c.fields.Brokerage__c.label}"/>
                    <apex:inputField value="{!loanObj.Brokerage__c}"/>
                </apex:pageBlockSectionItem>
                <apex:inputField value="{!cseObj.Trade_in_Amount__c}"/>
                <apex:inputField value="{!cseObj.Application_Fee__c}"/>
                <apex:inputField value="{!cseObj.Trade_In_Description__c}"/>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.Loan_Document_Request_Form__c.fields.Trail_N__c.label}"/>
                    <apex:inputText value="{!cseObj.Trail__c}" maxlength="8"/>
                </apex:pageBlockSectionItem>
                <apex:inputField value="{!cseObj.Payout_Amount__c}"/>                
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputText value="{!cseObj.Payout_To__c}"/>                
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputField value="{!cseObj.Required_Loan_Amount__c}"/>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>                
                <apex:pageblockSectionItem >
                <apex:outputLabel value="Figures Are Correct"/><apex:inputCheckbox value="{!figuresAreCorrect}"/> 
                </apex:pageblockSectionItem>
            </apex:pageBlockSection> 
            
            <apex:pageBlockSection title="Insurance Details" >
                    <apex:selectList id="gapProvider" value="{!gapProvider}" size="1" style="float:left;width:200px;" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.GAP_Provider__c.label}">
                        <apex:selectOptions value="{!gapProviderSp}"/>
                    </apex:selectList>
                <apex:inputText value="{!gapPremium}" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.GAP_Premium__c.label}" style="width:200px;" maxlength="16"/>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputField value="{!LoanObj.Product__c}" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.Product__c.label}" />
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:SelectList id="lpiCoverOption" value="{!lpiCoverOption}" size="1" style="float:left;width:200px;" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.LPI_Cover_Option__c.label}">
                        <apex:selectOptions value="{!lipCoverOptionSp}"/>
                    </apex:SelectList>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.Loan_Document_Request_Form__c.fields.LPI_Amount__c.label}" for="itlpiamount" />
                    <apex:inputText id="itlpiamount" value="{!lpiamount}" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.LPI_Amount__c.label}" maxlength="16" style="width:200px;"/>
                </apex:pageBlockSectionItem>
                    <apex:selectList id="lpiCoverType" value="{!lpiCoverType}" size="1" style="float:left;width:200px;" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.LPI_Cover_Type__c.label}">
                        <apex:SelectOptions value="{!lpiCoverTypeSp}"/>                        
                    </apex:selectList>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:SelectList id="warrantyProvider" value="{!warrantyProvider}" size="1" style="float:left;width:200px;" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.Warranty_Provider__c.label}">
                        <apex:selectOptions value="{!warrantyProviderSp}"/>
                    </apex:SelectList>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputText value="{!warrantyProductName}" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.Warranty_Product_Name__c.label}" style="width:200px;" maxlength="40"/>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:selectList id="warrantyTerm" value="{!warrantyTerm}" size="1" style="float:left;width:200px;" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.Warranty_Term__c.label}">
                    <apex:SelectOptions value="{!warrantyTermSp}"/>
                </apex:selectList>
                <apex:inputText value="{!warrantyPremium}" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.Warranty_Premium__c.label}" style="width:200px;" maxlength="16"/>
                
            </apex:pageBlockSection>
                
                <apex:pageBlockSection title="Vendor Details" id="pbsVD">
                    <apex:inputField value="{!cseObj.Sale_Type__c}" html-disabled="true">
                        <apex:actionSupport event="onchange" rerender="pbsVD" status="VDStatus" />
                    </apex:inputField>
                        <apex:pageBlockSectionItem />
                        <apex:pageBlockSectionItem rendered="{!IF(cseObj.Sale_Type__c == 'Dealer',true,false)}">
                            <apex:outputLabel value="Vendor Name" />
                            <apex:outputpanel >
                                <apex:outputText value="{!cseObj.Dealer_Vendor__r.Name}" /> 
                                &nbsp;<apex:outputLink value="#{!$Component.pbsVD}" onclick="openEditVendorDetailsLink();" title="Edit Vendor Details">[Edit Vendor Details]</apex:outputLink>
                            </apex:outputpanel>
                        </apex:pageBlockSectionItem>
                        <apex:pageBlockSectionItem rendered="{!IF(cseObj.Sale_Type__c == 'Dealer',true,false)}">
                            <apex:outputLabel value="Dealer Contact Person" />
                            <apex:outputText value="{!cseObj.Dealer_Contact__r.Name}" rendered="{!IF(cseObj.Sale_Type__c == 'Dealer',true,false)}" />
                        </apex:pageBlockSectionItem>
                        <apex:pageBlockSectionItem rendered="{!IF(cseObj.Sale_Type__c == 'Dealer',true,false)}">
                            <apex:outputLabel value="Vendor Phone" />
                            <apex:outputText value="{!cseObj.Dealer_Vendor__r.Business_Phone_Number__c}" />
                        </apex:pageBlockSectionItem>
                        <apex:pageBlockSectionItem rendered="{!IF(cseObj.Sale_Type__c == 'Dealer',true,false)}">
                            <apex:outputLabel value="Dealer Contact Mobile" />
                            <apex:outputText value="{!cseObj.Dealer_Contact__r.MobilePhone}" rendered="{!IF(OR(cseObj.Sale_Type__c == 'Dealer',cseObj.Sale_Type__c == 'Private'),true,false)}" />
                        </apex:pageBlockSectionItem>
                        <apex:pageBlockSectionItem rendered="{!IF(cseObj.Sale_Type__c == 'Dealer',true,false)}">
                            <apex:outputLabel value="Vendor Address" />
                            <apex:outputText value="{!cseObj.Dealer_Vendor__r.Address_Line_1__c} {!cseObj.Dealer_Vendor__r.Suburb__c} {!cseObj.Dealer_Vendor__r.Postcode__c} {!cseObj.Dealer_Vendor__r.State__c}" rendered="{!IF(cseObj.Sale_Type__c == 'Dealer',true,false)}" />
                        </apex:pageBlockSectionItem>
                        <apex:pageBlockSectionItem rendered="{!IF(cseObj.Sale_Type__c == 'Dealer',true,false)}">
                            <apex:outputLabel value="Dealer Email" />
                            <apex:outputText value="{!cseObj.Dealer_Contact__r.Email}" rendered="{!IF(cseObj.Sale_Type__c == 'Dealer',true,false)}" />
                        </apex:pageBlockSectionItem>
                        <apex:pageBlockSectionItem rendered="{!IF(cseObj.Sale_Type__c == 'Dealer',true,false)}" />
                        <apex:pageBlockSectionItem rendered="{!IF(cseObj.Sale_Type__c == 'Private',true,false)}" >
                            <apex:outputlabel value="Private Sale Vendor Contact" />
                            <apex:outputpanel >
                                <apex:outputText value="{!cseObj.Vendor_Contact__r.Name}" /> 
                                &nbsp;<apex:outputLink value="#{!$Component.pbsVD}" onclick="openEditVendorDetailsLink();" title="Edit Vendor Details">[Edit Vendor Details]</apex:outputLink>
                            </apex:outputpanel>

                        </apex:pageBlockSectionItem>
                        <apex:pageBlockSectionItem >
                            <apex:outputLabel value="Vendor Details Are Correct" />
                            <apex:inputCheckbox value="{!vendorDetailsCorrect}" >
                            </apex:inputCheckbox>
                        </apex:pageBlockSectionItem>
                </apex:pageBlockSection>                           
            
            <apex:pageBlockSection title="Primary Asset Details">    
                <apex:inputField value="{!cseObj.Asset_Type__c}"/>
                <apex:inputField value="{!cseObj.Asset_Sub_Type__c}"/>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputText value="{!cseObj.Vehicle_Transmission__c}" maxlength="15"/>
                <apex:inputText value="{!cseObj.Vehicle_Year__c}" maxlength="4"/>
                <apex:inputText value="{!cseObj.Vehicle_Body_Type__c}" maxlength="15" />
                <apex:inputText value="{!cseObj.Vehicle_Make__c}" maxlength="255" />
                <apex:inputText value="{!cseObj.Vehicle_Colour__c}" maxlength="30" />
                <apex:inputText value="{!cseObj.Vehicle_Model__c}" maxlength="20" />
                <apex:inputText value="{!cseObj.Vehicle_Fuel_Type__c}" maxlength="30"/>
                <apex:inputText value="{!cseObj.Varient__c}" maxlength="50" />
                <apex:inputText value="{!cseObj.Vehicle_Odometer_Reading__c}" maxlength="10"/>
                <apex:inputText value="{!cseObj.Vehicle_VIN__c}" maxlength="17"/>
                <apex:inputfield value="{!cseObj.Vehicle_Date_First_Registered__c}" />
                <apex:inputfield value="{!cseObj.Engine_No__c}"/>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputText value="{!cseObj.Vehicle_Registration_Number__c}" maxlength="20" />                
                <apex:inputText value="{!cseObj.Gross_Vehicle_Mass__c}" label="{!$ObjectType.Loan_Document_Request_Form__c.fields.Gross_Vehicle_Mass__c.label}" maxlength="10"/>
                <apex:inputField value="{!cseObj.Vehicle_Registration_State__c}"/>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputText value="{!cseObj.Accessories_Modifications_Values__c}" maxlength="10" />
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputField value="{!cseObj.Is_Vehicle_Under_Statutory_Warranty__c}"/>
                <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                <apex:inputField value="{!cseObj.Statutory_Warranty_End_Date__c}"/>
            </apex:pageBlockSection>    
            
        </apex:pageBlock>
    </apex:form>
    <script>

          function openEditVendorDetailsLink(){
                if (sforce.console.isInConsole()) { 
                    sforce.console.getEnclosingPrimaryTabId(openSubtab);

                } else { 
                    window.open(url,'_target'); 
                } 
           }

           function backtoCase(){
                console.log('IsinConsole:'+sforce.console.isInConsole());
                if (sforce.console.isInConsole()){

                    console.log(sforce.console.getEnclosingTabId(closeSubtab));
                    sforce.console.getEnclosingTabId(closeSubtab);
                }
                else af_BackToCase();
           }

           var openSubtab = function openSubtab(result) {
                //Now that we have the primary tab ID, we can open a new subtab in it
                var primaryTabId = result.id;
                var url = '/apex/Case_EditVendorDetails?id={!case.Id}'; 
                var s = sforce.console.openSubtab(primaryTabId , url, false, 'Edit Vendor Details', null,openSuccess);
            };

            var openSuccess = function openSuccess(result){
                var subtabId = result.id;
                console.log(subtabId);
                sforce.console.focusSubtabById(subtabId);
            }

           var closeSubtab = function closeSubtab(result) {
                var tabId = result.id;
                console.log(tabId);
                sforce.console.closeTab(tabId);
            };

           function setTabTitle() {
                console.log('IsinConsole:'+sforce.console.isInConsole());
                if (sforce.console.isInConsole()){
                    sforce.console.setTabTitle('Loan Document');
                }
           }       
          
           var previousOnload = window.onload;        
           window.onload = function() { 
                if (previousOnload) { 
                    previousOnload();
                }                
                setTimeout('setTabTitle()', '500'); 
           }
           
           console.log('loaded');

           var onFocusEvent = function (result){
                console.log('LDR tab in focus');
                refreshOppVD();
           };

           sforce.console.onFocusedSubtab(onFocusEvent); 

    </script>
</apex:page>
<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object is used to record visits to Referral Companies. The most recent record will update the Last Contact Date on the Referral Company record with the Created Date of the Site Visit record.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Comments__c</fullName>
        <description>This 500-character text field is used to record some Comments about the Site Visit. This content will be copied to the &apos;Recent Comments&apos; section on the Referral record</description>
        <externalId>false</externalId>
        <inlineHelpText>This 500-character text field is used to record some Comments about the Site Visit. This content will be copied to the &apos;Recent Comments&apos; section on the Referral record</inlineHelpText>
        <label>Comments</label>
        <length>500</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Referral_Company_Name__c</fullName>
        <description>This Master-Detail lookup field is used to select a Referral Company record for each Site Visit</description>
        <externalId>false</externalId>
        <inlineHelpText>This Master-Detail lookup field is used to select a Referral Company record for each Site Visit</inlineHelpText>
        <label>Referral Company Name</label>
        <referenceTo>Referral_Company__c</referenceTo>
        <relationshipName>Site_Visits</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Visit_Date__c</fullName>
        <defaultValue>TODAY()</defaultValue>
        <description>This date field is used to record the date of the site visit to the Referral Company. This date will be used to update the Last Contact Date on the Referral Company record</description>
        <externalId>false</externalId>
        <inlineHelpText>This date field is used to record the date of the site visit to the Referral Company. This date will be used to update the Last Contact Date on the Referral Company record</inlineHelpText>
        <label>Visit Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>OLD MODEL Site Visit</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Referral_Company_Name__c</columns>
        <columns>Visit_Date__c</columns>
        <columns>Comments__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>RefCoSiteVisit-{000000}</displayFormat>
        <label>Site Visit ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>OLD MODEL Site Visits</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Comments__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Referral_Company_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Visit_Date__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>

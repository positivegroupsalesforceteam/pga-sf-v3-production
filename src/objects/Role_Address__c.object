<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>junction object between role and address (so addresses can be reused multiple times)</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>lookup to Account, optional for now as can be derived from Role.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select Account (optional)</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Role Addresses</relationshipLabel>
        <relationshipName>Role_Addresses</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Active_Address__c</fullName>
        <defaultValue>&quot;Current&quot;</defaultValue>
        <description>whether current or previous address</description>
        <externalId>false</externalId>
        <inlineHelpText>select if current or previous address</inlineHelpText>
        <label>Active Address?</label>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Current</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Previous</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Address__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup to Address</description>
        <externalId>false</externalId>
        <inlineHelpText>Select address for Role</inlineHelpText>
        <label>Address</label>
        <referenceTo>Address__c</referenceTo>
        <relationshipLabel>Role Addresses</relationshipLabel>
        <relationshipName>Role_Addresses</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Application_Number__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>RDAVID 17/07/2019 tasks/25349502: Chris Sims</description>
        <externalId>false</externalId>
        <label>Application Number</label>
        <referenceTo>Application__c</referenceTo>
        <relationshipLabel>Role Addresses</relationshipLabel>
        <relationshipName>Role_Addresses</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Application__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK((&quot;/&quot;+Role__r.Application__r.ID18__c) ,Role__r.Application__r.Name)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Application</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Role__r.Case__r.CaseNumber</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Case Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_Owner_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Role__r.Case__r.Owner_Full_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Case Owner Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_Stage__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Role__r.Case__r.Stage__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Case Stage</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_Status__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Role__r.Case__r.Status )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Case Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK((&quot;/&quot;+Role__r.Case__r.ID18__c) ,Role__r.Case__r.CaseNumber)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Case</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <description>End date for Role at that Address</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter end date for Role at this address</inlineHelpText>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>FE_Address_Duration_months__c</fullName>
        <description>from frontend system - time at address in months</description>
        <externalId>false</externalId>
        <label>FE Address Duration (months)</label>
        <precision>5</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Full_Address__c</fullName>
        <externalId>false</externalId>
        <formula>Address__r.Full_Address__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Full Address</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Joint_Address__c</fullName>
        <defaultValue>false</defaultValue>
        <description>NM Field.</description>
        <externalId>false</externalId>
        <label>Joint Address</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Location</label>
        <referenceTo>Location__c</referenceTo>
        <relationshipLabel>Role Addresses</relationshipLabel>
        <relationshipName>Role_Addresses</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Mercury_GUID__c</fullName>
        <description>GUID for person address in Mercury (Connective)</description>
        <externalId>false</externalId>
        <label>Mercury GUID</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Residential_Situation__c</fullName>
        <description>used in both current and previous address</description>
        <externalId>false</externalId>
        <label>Residential Situation</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Residential_Situation</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Role_Account_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Role__r.Account_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Role Account Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Role_Master_Role_Address_Count__c</fullName>
        <externalId>false</externalId>
        <formula>Role__r.Number_of_Role_Addresses__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Role Master - Role Address Count</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Role__c</fullName>
        <description>lookup to Role</description>
        <externalId>false</externalId>
        <inlineHelpText>select Role</inlineHelpText>
        <label>Role</label>
        <referenceTo>Role__c</referenceTo>
        <relationshipLabel>Role Addresses</relationshipLabel>
        <relationshipName>Role_Addresses</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <description>Start date for Role at that address</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter start date for Role at this address</inlineHelpText>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Time_at_Address_years__c</fullName>
        <externalId>false</externalId>
        <formula>Year(  End_Date__c  )- Year(  Start_Date__c  )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Time at Address (years)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Validated__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Validated</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>m_Account_SFID__c</fullName>
        <description>data migration - linking field to source of address</description>
        <externalId>false</externalId>
        <label>m_Account_SFID</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>m_Applicant2_SFID__c</fullName>
        <description>data migration - linking field to Applicant 2 as address origination</description>
        <externalId>false</externalId>
        <label>m_Applicant2_SFID</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>m_Case_SFID__c</fullName>
        <description>data migration - reference to source object</description>
        <externalId>false</externalId>
        <label>m_Case_SFID</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>m_Combo_Address__c</fullName>
        <description>unique key to link with Address. Also reference copy for Address at migration.</description>
        <externalId>false</externalId>
        <label>m_Combo_Address</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>m_Lead_ID18__c</fullName>
        <description>data migration - reference to source object</description>
        <externalId>false</externalId>
        <label>m_Lead_ID18</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>m_Lead_SF18__c</fullName>
        <description>data migration - reference to source object</description>
        <externalId>false</externalId>
        <label>m_Lead_SF18</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>m_Lead_SFID__c</fullName>
        <description>data migration - reference to source object</description>
        <externalId>false</externalId>
        <label>m_Lead_SFID</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>m_Opportunity_SFID__c</fullName>
        <description>data migration - reference to source object</description>
        <externalId>false</externalId>
        <label>m_Opportunity_SFID</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>m_Source_Object_SFID__c</fullName>
        <description>SFID for source object for Role Address &amp; Address at migration</description>
        <externalId>false</externalId>
        <label>m_Source_SFID</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>m_Source_Object__c</fullName>
        <description>object name for source Role Address &amp; Address</description>
        <externalId>false</externalId>
        <label>m_Source_Object</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>t_Role_Address_Sequence__c</fullName>
        <description>temp field using sequence t manage links between Address and Role Address</description>
        <externalId>false</externalId>
        <label>t_Role_Address_Sequence</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>z_Full_Address__c</fullName>
        <externalId>false</externalId>
        <label>Full Address</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Role Address</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>z_Full_Address__c</columns>
        <columns>Active_Address__c</columns>
        <columns>Account__c</columns>
        <columns>Role__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All Role Addresses</label>
    </listViews>
    <nameField>
        <displayFormat>RA-{0000000}</displayFormat>
        <label>Role Address ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Role Addresses</pluralLabel>
    <recordTypeTrackHistory>true</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Full_Address</fullName>
        <active>true</active>
        <description>current or previous address for Role - links to Address object</description>
        <label>Full Address</label>
        <picklistValues>
            <picklist>Active_Address__c</picklist>
            <values>
                <fullName>Current</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Previous</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Residential_Situation</picklist>
            <values>
                <fullName>Boarder</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Employer Provided</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Living With Parents</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Mortgage - Owner Occupied</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Own Outright</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Renter - Agent</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Renter - Private</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Postcode_Only</fullName>
        <active>true</active>
        <description>Location used at Lead qualification</description>
        <label>Postcode Only</label>
        <picklistValues>
            <picklist>Active_Address__c</picklist>
            <values>
                <fullName>Current</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Previous</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Residential_Situation</picklist>
            <values>
                <fullName>Boarder</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Employer Provided</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Living With Parents</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Mortgage - Owner Occupied</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Own Outright</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Renter - Agent</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Renter - Private</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>zLead</fullName>
        <active>true</active>
        <description>postcode location, used at Lead qualification</description>
        <label>zLead</label>
        <picklistValues>
            <picklist>Active_Address__c</picklist>
            <values>
                <fullName>Current</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Previous</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Residential_Situation</picklist>
            <values>
                <fullName>Boarder</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Employer Provided</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Living With Parents</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Mortgage</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Mortgage - Owner Occupied</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Own Outright</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Renter - Agent</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Renter - Private</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Renter Agent</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Renter Private</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>New_Role_Address</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>New Role Address (Full)</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/36.0/connection.js&quot;)} 
{!REQUIRESCRIPT(&quot;/support/console/36.0/integration.js&quot;)} 
var url = &apos;/apex/NewRoleAddress?scontrolCaching=1&amp;id={!Role__c.Id}&apos;; 

if (sforce.console.isInConsole()) { 
srcUp(url + &apos;&amp;isdtp=vw&apos;); 
} else { 
window.open(url,&apos;_target&apos;); 
}</url>
    </webLinks>
</CustomObject>

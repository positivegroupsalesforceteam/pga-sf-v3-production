<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Address_Notify_UNKNOWN_Location</fullName>
        <description>Address - Notify UNKNOWN Location</description>
        <protected>false</protected>
        <recipients>
            <recipient>jesfer.baculod@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rexie.david@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Address_Notify_UNKNOWN_Location</template>
    </alerts>
    <rules>
        <fullName>Address - Notify UNKNOWN Location</fullName>
        <actions>
            <name>Address_Notify_UNKNOWN_Location</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(Location__c), Location__r.Name == &apos;UNKNOWN&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

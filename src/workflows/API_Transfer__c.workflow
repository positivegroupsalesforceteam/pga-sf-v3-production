<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>API_Transfer_Equifax_Alert</fullName>
        <description>API Transfer - Equifax Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>rexie.david@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/API_Transfer_Equifax_Template</template>
    </alerts>
    <outboundMessages>
        <fullName>API_Transfer_Create_Case_From_FB_Leads</fullName>
        <apiVersion>45.0</apiVersion>
        <description>Trigger Jitterbit operation for processing Facebook Lead Ads</description>
        <endpointUrl>https://Positive187318.jitterbit.eu/CloudTest/1.0/facebookLeadAdsIntegration</endpointUrl>
        <fields>FB_Transaction_ID__c</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>API Transfer - Create Case From FB Leads</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>API_Transfer_Email_to_SF</fullName>
        <apiVersion>45.0</apiVersion>
        <description>Creates SF records for NM from email through Zap and Jitterbit</description>
        <endpointUrl>https://Positive187318.jitterbit.eu/CloudTest/1/emailToSf</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>jesfer.baculod@positivelendingsolutions.com.au</integrationUser>
        <name>API Transfer - Email to SF</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>API_Transfer_Equifax</fullName>
        <apiVersion>47.0</apiVersion>
        <description>SFBAU-98 RDAVID</description>
        <endpointUrl>https://Positive187318.jitterbit.eu/CloudTest/equifax</endpointUrl>
        <fields>Case__c</fields>
        <fields>EQ_Account_Type_Code__c</fields>
        <fields>EQ_Bureau_Reference__c</fields>
        <fields>EQ_C_Country_Code__c</fields>
        <fields>EQ_C_Full_Address__c</fields>
        <fields>EQ_C_Postcode__c</fields>
        <fields>EQ_C_State_Code__c</fields>
        <fields>EQ_C_Street_Name__c</fields>
        <fields>EQ_C_Street_Number__c</fields>
        <fields>EQ_C_Street_Type__c</fields>
        <fields>EQ_C_Suburb__c</fields>
        <fields>EQ_C_Unit_Number__c</fields>
        <fields>EQ_Client_Reference__c</fields>
        <fields>EQ_Current_Address__c</fields>
        <fields>EQ_Date_of_Birth__c</fields>
        <fields>EQ_Driver_Licence_Number__c</fields>
        <fields>EQ_Employer_Name__c</fields>
        <fields>EQ_Employment_Type__c</fields>
        <fields>EQ_Enquiry_Amount__c</fields>
        <fields>EQ_Enquiry_Client_Reference__c</fields>
        <fields>EQ_First_Name__c</fields>
        <fields>EQ_Gender_Code__c</fields>
        <fields>EQ_Include_Individual_Trading_History__c</fields>
        <fields>EQ_Last_Name__c</fields>
        <fields>EQ_Middle_Name__c</fields>
        <fields>EQ_Operator_ID__c</fields>
        <fields>EQ_Operator_Name__c</fields>
        <fields>EQ_P_Country_Code__c</fields>
        <fields>EQ_P_Full_Address__c</fields>
        <fields>EQ_P_Postcode__c</fields>
        <fields>EQ_P_State_Code__c</fields>
        <fields>EQ_P_Street_Name__c</fields>
        <fields>EQ_P_Street_Number__c</fields>
        <fields>EQ_P_Street_Type__c</fields>
        <fields>EQ_P_Suburb__c</fields>
        <fields>EQ_P_Unit_Number__c</fields>
        <fields>EQ_Permission_Type_Code__c</fields>
        <fields>EQ_Previous_Address__c</fields>
        <fields>EQ_Product_Data_Level_Code__c</fields>
        <fields>EQ_Relationship_Code__c</fields>
        <fields>EQ_Request_Status__c</fields>
        <fields>EQ_Role__c</fields>
        <fields>EQ_Scorecard_ID__c</fields>
        <fields>EQuifax_Enquiry_Id__c</fields>
        <fields>Equifax_Request_Type__c</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>rexie.david@positivelendingsolutions.com.au</integrationUser>
        <name>API Transfer - Equifax</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>API_Transfer_LeadaConnect_to_SF</fullName>
        <apiVersion>47.0</apiVersion>
        <description>Outbound Workflow for LeadaConnect</description>
        <endpointUrl>https://Positive187318.jitterbit.eu/CloudTest/leadaConnect</endpointUrl>
        <fields>Id</fields>
        <fields>Integration_Log__c</fields>
        <fields>LC_Email__c</fields>
        <fields>LC_Finance_for_Business_Purposes__c</fields>
        <fields>LC_First_Name__c</fields>
        <fields>LC_Last_Name__c</fields>
        <fields>LC_Loan_Amount__c</fields>
        <fields>LC_Middle_Name__c</fields>
        <fields>LC_Phone__c</fields>
        <fields>LC_Postcode__c</fields>
        <fields>LC_Suburb__c</fields>
        <fields>Manual_Push_Count__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>API Transfer - LeadaConnect to SF</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>API_Transfer_Manual_Push_to_Overflow</fullName>
        <apiVersion>45.0</apiVersion>
        <endpointUrl>https://positive187318.jitterbit.eu/CloudTest/1.0/pushSfToOverflow</endpointUrl>
        <fields>Id</fields>
        <fields>OF_Transaction_ID__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>API Transfer - Manual Push to Overflow</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>API Transfer - Create Case from FB Lead Ads</fullName>
        <actions>
            <name>API_Transfer_Create_Case_From_FB_Leads</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Trigger Jitterbit project for creating data structure of FB Lead Ads. 

(1) On FB Lead Ad API Transfer create 
(2) On Repush</description>
        <formula>AND(  OR(  AND(  NOT(ISNEW()),  ISCHANGED(Manual_Push_Count__c),  Manual_Push_Count__c &gt; 0  ),  ISNEW()  ),  RecordType.DeveloperName = &apos;FB_Lead_Ad&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>API Transfer - Email to SF</fullName>
        <actions>
            <name>API_Transfer_Email_to_SF</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Trigger Jitterbit project for creating data structure of DLA Email 

(1) On Email API Transfer create 
(2) On Repush</description>
        <formula>AND(  RecordType.DeveloperName = &apos;Email&apos;,  OR(  ISNEW(),  AND(  ISCHANGED(Manual_Push_Count__c),  Manual_Push_Count__c &gt; 0  )  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>API Transfer - Equifax</fullName>
        <actions>
            <name>API_Transfer_Equifax</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>API Transfer - Equifax	

Trigger Jitterbit project for creating/retrieval of Equifax report

(1) On Equifax API Transfer create</description>
        <formula>AND( 				RecordType.DeveloperName = &apos;Equifax&apos;, 				OR(    								ISNEW(),    								AND(      												ISCHANGED(Manual_Push_Count__c),       												Manual_Push_Count__c &gt; 0     								)     				)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>API Transfer - LeadaConnect to SF</fullName>
        <actions>
            <name>API_Transfer_LeadaConnect_to_SF</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Trigger Jitterbit project for creating data structure of LeadaConnect

(1) On LeadaConnect API Transfer create 
(2) On Repush</description>
        <formula>AND(    RecordType.DeveloperName = &apos;LeadaConnect&apos;,    OR(   ISNEW(),   AND(     ISCHANGED(Manual_Push_Count__c),      Manual_Push_Count__c &gt; 0    )    ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>API Transfer - Manual Push to Overflow</fullName>
        <actions>
            <name>API_Transfer_Manual_Push_to_Overflow</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>AND(  RecordType.DeveloperName = &apos;Overflow&apos;,  ISCHANGED(Manual_Push_Count__c),  Manual_Push_Count__c &gt; 0  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

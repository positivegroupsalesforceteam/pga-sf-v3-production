<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Mortgage_Tax_Invoice_Requested_Notification</fullName>
        <description>Account - Mortgage - Tax Invoice Requested Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>clint.sjoberg@positivehomeloans.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rob@positivehomeloans.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Account_Mortgage_Tax_Invoice_Requested_Notification</template>
    </alerts>
    <alerts>
        <fullName>Lead_Generated_notify_assigned_user</fullName>
        <description>Lead Generated - notify assigned user</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Lead_Template/Lead_account_Assignment</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Email_Index</fullName>
        <field>Email_Index__c</field>
        <formula>PersonEmail</formula>
        <name>Account Email Index</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Lead_change_back_to_false</fullName>
        <description>When task created, reset new lead filed to false</description>
        <field>New_Lead__c</field>
        <literalValue>false</literalValue>
        <name>New Lead - change back to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_spaces_mobile_field</fullName>
        <field>PersonMobilePhone</field>
        <formula>SUBSTITUTE(PersonMobilePhone,&quot; &quot;, &quot;&quot;)</formula>
        <name>Remove spaces mobile field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_spaces_sms_mobile_field</fullName>
        <field>SMS_Mobile__c</field>
        <formula>SUBSTITUTE(SMS_Mobile__c,&quot; &quot;,&quot;&quot;)</formula>
        <name>Remove spaces sms mobile field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SMS_Mobile</fullName>
        <field>SMS_Mobile__c</field>
        <formula>IF(LEFT(PersonMobilePhone , 2) = &quot;04&quot;,TRIM( &quot;61&quot; &amp; TRIM(RIGHT(PersonMobilePhone , LEN(PersonMobilePhone )-1 ))),PersonMobilePhone)</formula>
        <name>SMS Mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Acc_Record_Type_as_Individual</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Individual</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Acc Record Type as Individual</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Acc_Record_Type_as_Property</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Property</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Acc Record Type as Property</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account - Mortgage TIR Task</fullName>
        <actions>
            <name>Account_Mortgage_Tax_Invoice_Requested_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Mortgage_Tax_Invoice_Requested</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( Opp_Stage__c == &apos;Tax Invoice Requested&apos;, ISPICKVAL(Residential_Status__c, &apos;Mortgage&apos;), RecordType.Name = &apos;Individual&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send SMS Privacy for Account</fullName>
        <actions>
            <name>Sending_SMS_Privacy_to_Account</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.PersonMobilePhone</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SMS_Privacy__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Acc as Individual when Lead is Individual or Car</fullName>
        <actions>
            <name>Update_Acc_Record_Type_as_Individual</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Record_Type__c</field>
            <operation>equals</operation>
            <value>Individual</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Record_Type__c</field>
            <operation>equals</operation>
            <value>Car</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Acc as Property when Lead is Property</fullName>
        <actions>
            <name>Update_Acc_Record_Type_as_Property</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Record_Type__c</field>
            <operation>equals</operation>
            <value>Property</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update SMS Mobile number with country code</fullName>
        <actions>
            <name>Remove_spaces_mobile_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_spaces_sms_mobile_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SMS_Mobile</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.PersonMobilePhone</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>account mob to opp</fullName>
        <active>false</active>
        <formula>ISCHANGED(PersonMobilePhone)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>update email index</fullName>
        <actions>
            <name>Account_Email_Index</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>PersonEmail!=null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Lead_Generated</fullName>
        <assignedToType>owner</assignedToType>
        <description>Lead has completed a form and requires a follow up.</description>
        <dueDateOffset>-1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Lead Generated</subject>
    </tasks>
    <tasks>
        <fullName>Mortgage_Tax_Invoice_Requested</fullName>
        <assignedTo>elliot@positivehomeloans.com.au</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Mortgage - Tax Invoice Requested</subject>
    </tasks>
    <tasks>
        <fullName>Sending_SMS_Privacy_to_Account</fullName>
        <assignedToType>owner</assignedToType>
        <description>{&quot;template&quot;:&quot;a096F00001xxH1W&quot;}</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Sending SMS Privacy to Account</subject>
    </tasks>
</Workflow>

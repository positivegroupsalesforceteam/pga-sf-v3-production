<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Loan_Case_Settled_c_Field</fullName>
        <field>Loan_Case_Settled__c</field>
        <literalValue>1</literalValue>
        <name>Update Loan_Case_Settled__c Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Settled Checkbox When Case Settled</fullName>
        <actions>
            <name>Update_Loan_Case_Settled_c_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>CommissionCT__c.Case_Stage__c</field>
            <operation>equals</operation>
            <value>Settled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Lead_Generated_notify_assigned_user</fullName>
        <description>Lead Generated - notify assigned user</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Lead_Template/Lead_account_Assignment</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_to_open_lead_from_task</fullName>
        <description>SF automation assigmeNt - set to open lead if new lead = true</description>
        <field>New_Lead__c</field>
        <name>Change to open lead from task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Lead_change_back_to_false</fullName>
        <description>When task created, reset new lead filed to NULL</description>
        <field>New_Lead__c</field>
        <name>New Lead - change back to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pivacy_SMS_Sent_set_to_True</fullName>
        <description>set flag to true once Privacy SMS is sent</description>
        <field>Privacy_SMS_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Pivacy SMS Sent set to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Privacy_SMS_Sent_set_to_True</fullName>
        <description>set flag on Contact that Privacy SMS has been sent</description>
        <field>Privacy_SMS_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Privacy SMS Sent set to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Testing_phone_update</fullName>
        <field>HomePhone</field>
        <formula>MobilePhone</formula>
        <name>Testing phone update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Referral_Company_Contact_To_Financeapp</fullName>
        <apiVersion>40.0</apiVersion>
        <endpointUrl>https://pls.finance-app.com.au/api/salesforce/fetchReferralContact</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>Referral Company Contact To Financeapp</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>test_referral_company_user</fullName>
        <apiVersion>39.0</apiVersion>
        <endpointUrl>http://pls.dev-lightning-platform.com.au/api/salesforce/fetchReferralContact</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>Referral Company Contact To Lightning</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Connective Privacy SMS</fullName>
        <actions>
            <name>Pivacy_SMS_Sent_set_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>send Privacy SMS to Connective applicant on Contact creation (from FinanceApp or manually, Key Person rt)</description>
        <formula>AND( RecordType.Name == &apos;Key Person&apos;, NOT(ISNULL(MobilePhone)), NOT(ISBLANK(MobilePhone)), NOT(ISNULL(Lightning_Privacy_Link__c)), NOT(ISBLANK(Lightning_Privacy_Link__c)), CreatedBy.Alias == &apos;Api&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Create Referral Company User On  Finance-app</fullName>
        <actions>
            <name>Referral_Company_Contact_To_Financeapp</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Using formula as rule: (1) Referral_Company__c must not be BLANK, (2) IF PLSa_Access__c or PLSb_Access__c is changed, OR newly created contact (e.g. createdDate == LastModifiedDate)</description>
        <formula>IF(ISBLANK(Referral_Company__c),false,IF(ISCHANGED(PLSa_Access__c) || ISCHANGED(PLSb_Access__c), true, IF(DATEVALUE(CreatedDate) = DATEVALUE(LastModifiedDate),true,false)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create Referral Company User On  Lightning</fullName>
        <actions>
            <name>test_referral_company_user</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>Using formula as rule: (1) Referral_Company__c must not be BLANK, (2) IF Lightning_Access__c is changed, 3.) IF TicknFlick_Access__c is changed OR newly created contact (e.g. createdDate == LastModifiedDate)</description>
        <formula>IF(ISBLANK(Referral_Company__c),false,IF(ISCHANGED(Lightning_Access__c) || ISCHANGED(Nodifi_Access__c) || ISCHANGED(TicknFlick_Access__c) || ISCHANGED(Email), true, IF(DATEVALUE(CreatedDate) = DATEVALUE(LastModifiedDate),true,false)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create task on new lead form entry</fullName>
        <actions>
            <name>Lead_Generated_notify_assigned_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New_Lead_change_back_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Generated</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.New_Lead__c</field>
            <operation>equals</operation>
            <value>true</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send SMS Privacy for Contact</fullName>
        <actions>
            <name>Sending_SMS_Privacy_to_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.MobilePhone</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.SMS_Privacy__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Lead_Generated</fullName>
        <assignedToType>accountOwner</assignedToType>
        <description>Lead has completed a form and requires a follow up.</description>
        <dueDateOffset>-1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Lead Generated</subject>
    </tasks>
    <tasks>
        <fullName>Sending_SMS_Privacy_to_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <description>{&quot;template&quot;:&quot;a096F00001xxEVo&quot;}</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sending SMS Privacy to Contact</subject>
    </tasks>
</Workflow>

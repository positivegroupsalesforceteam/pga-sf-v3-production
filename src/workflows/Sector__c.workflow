<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Sector_Send_Case_SLA_Sector_Warning_Notification</fullName>
        <description>Sector - Send Case SLA Sector Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Alert_Recipient_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email_Warning_Recipient_POC__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Email_Warning_Recipient__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Sector_Case_SLA_Sector</template>
    </alerts>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Referral_Company_Comments</fullName>
        <description>This workflow Rule and Field update trigger will copy the comments and Created Date to the Referral Company record whenever a new record is created or edited.</description>
        <field>Last_Visit_Comments__c</field>
        <formula>Comments__c</formula>
        <name>Update Referral Company Comments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Referral_Company_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Referral_Company_Visit_Date</fullName>
        <description>This workflow Rule and Field update trigger will copy the comments and Created Date to the Referral Company record whenever a new record is created or edited.</description>
        <field>Last_Contact_Date__c</field>
        <formula>Visit_Date__c</formula>
        <name>Update Referral Company Visit Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Referral_Company_Name__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>New Site Visit Update Referral Content</fullName>
        <actions>
            <name>Update_Referral_Company_Comments</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Referral_Company_Visit_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow Rule and Field update trigger will copy the comments and Created Date to the Referral Company record whenever a new record is created or edited.</description>
        <formula>OR(ISNEW(),ISCHANGED(LastModifiedDate))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

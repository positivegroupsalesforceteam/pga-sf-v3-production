<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Relationship_Nodifi_Partner_Sync</fullName>
        <ccEmails>mary.tapar@positivelendingsolutions.com.au</ccEmails>
        <description>Relationship - Nodifi Partner Sync</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Relationship_Nodifi_Partner_Sync_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Populate_Account_Name</fullName>
        <description>NM field update to populate Account_Name__c text field used in Lookup search.</description>
        <field>Account_Name__c</field>
        <formula>Associated_Account__r.Name</formula>
        <name>Populate Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Relationship_Nodifi_Partner_Sync</fullName>
        <apiVersion>45.0</apiVersion>
        <description>This outboung message is created to support the New Model creation of Referral Company Contact.
Set up login to Nodifi, Refactor existing setup process to run from creation of the Relationship record {previously ran ib create Contact, Business RT}</description>
        <endpointUrl>http://nodifi.dev-lightning-platform.com.au/api/salesforce/fetchReferralContact</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>Relationship - Nodifi Partner Sync</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Relationship - Nodifi Partner Sync</fullName>
        <actions>
            <name>Relationship_Nodifi_Partner_Sync</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Relationship_Nodifi_Partner_Sync</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>RDAVID 25/07/2019 tasks/25446073</description>
        <formula>AND(     $Setup.Workflow_Rule_Settings1__c.Enable_Relationship_Nodifi_Partner_Sync__c == TRUE,      OR(         AND(             ISNEW(),              NOT(ISBLANK(TEXT(Nodifi_Tenancy__c))),              NOT(ISBLANK(TEXT(Nodifi_User_Role__c))),              NOT(ISBLANK(Contact_Email1__c)), Active__c         ),          AND(             NOT(ISNEW()),              Partner_Parent__c != &apos;Connective&apos;,              NOT(ISPICKVAL(Nodifi_Tenancy__c , &apos;No Access&apos;)),             OR( ISCHANGED(Nodifi_Tenancy__c),                  ISCHANGED(Nodifi_User_Role__c),                  ISCHANGED(Contact_Mobile1__c),                  ISCHANGED(Contact_Email1__c)             )          ),         AND(             NOT(ISNEW()),              OR( ISCHANGED(Nodifi_Tenancy__c),                  ISCHANGED(Nodifi_User_Role__c),                  AND(ISBLANK(PRIORVALUE(End_Date__c)),                      ISCHANGED(End_Date__c),                      NOT(ISBLANK(End_Date__c))                 )             )         )     ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Relationship - Populate Account Name Text</fullName>
        <actions>
            <name>Populate_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>NM workflow to populate Account_Name__c text field used in lookup search.</description>
        <formula>AND ( $Setup.Workflow_Rule_Settings1__c.Enable_Relationship_Workflows__c ,       NOT(ISNULL( Associated_Account__c )) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

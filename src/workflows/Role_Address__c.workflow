<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Full_Address</fullName>
        <description>NM field update to populate Full Address text field based on selected Address. (Role Address)</description>
        <field>z_Full_Address__c</field>
        <formula>Address__r.Full_Address__c</formula>
        <name>Populate Full Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Role Address - Populate Full Address</fullName>
        <actions>
            <name>Populate_Full_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>NM workflow to populate Full Address field based on selected Address. (Role Address)</description>
        <formula>AND ( $Setup.Workflow_Rule_Settings1__c.Enable_Role_Address_Workflows__c ,  NOT(ISBLANK(Address__r.Full_Address__c))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

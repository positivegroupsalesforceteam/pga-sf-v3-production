<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>User_Send_Email_When_Support_Profile_is_switched</fullName>
        <description>User - Send Email When Support Profile is switched</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/User_Send_Email_When_Support_Profile_is_switched</template>
    </alerts>
    <fieldUpdates>
        <fullName>Remove_61_from_User_mobile</fullName>
        <field>MobilePhone</field>
        <formula>SUBSTITUTE(MobilePhone,&apos;61&apos;,&apos;0&apos;)</formula>
        <name>Remove 61 from User mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_country_code_from_User_mobile</fullName>
        <field>MobilePhone</field>
        <formula>SUBSTITUTE(MobilePhone,&apos;+61&apos;,&apos;&apos;)</formula>
        <name>Remove country code from User mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_spaces_from_User_mobile_field</fullName>
        <field>MobilePhone</field>
        <formula>SUBSTITUTE(MobilePhone,&quot; &quot;,&quot;&quot;)</formula>
        <name>Remove spaces from User mobile field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Search_Role</fullName>
        <field>Search_Role__c</field>
        <formula>UserRole.Name</formula>
        <name>Update Search Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>User_Update_On_Current_Team_Since</fullName>
        <field>On_Current_Team_Since__c</field>
        <formula>IF(NOT(ISPICKVAL(Team__c, &apos;&apos;)),
NOW(), null)</formula>
        <name>User - Update On Current Team Since</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>User_Update_Previous_Team</fullName>
        <field>Previous_Team__c</field>
        <formula>IF( NOT(ISNULL(PRIORVALUE(Team__c))),
TEXT(PRIORVALUE(Team__c)), &apos;&apos;
)</formula>
        <name>User - Update Previous Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Remove country code from User mobile</fullName>
        <actions>
            <name>Remove_spaces_from_User_mobile_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND ( ISCHANGED (MobilePhone) || ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Search Role</fullName>
        <actions>
            <name>Update_Search_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Search Field Role with User&apos;s Role Name</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User - Send Email When Support Profile is switched</fullName>
        <actions>
            <name>User_Send_Email_When_Support_Profile_is_switched</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>RDAVID 10/05/2019: tasks/24199281 Add Email Notifications in Profile Switcher</description>
        <formula>AND(  OR(Profile.Name == &apos;NM - Nodifi Support&apos; , Profile.Name == &apos;NM - PLS Support&apos;),  NOT(PRIORVALUE(ProfileId) == ProfileId)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User - Update Team Info</fullName>
        <actions>
            <name>User_Update_On_Current_Team_Since</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>User_Update_Previous_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Team__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

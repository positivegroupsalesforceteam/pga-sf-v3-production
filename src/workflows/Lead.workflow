<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Automatic_email_to_customer_with_fair_go_finance_details</fullName>
        <description>Automatic email to customer with fair go finance details</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tom_s_templates/Personal_Loan_Application_with_Signature</template>
    </alerts>
    <alerts>
        <fullName>Cash_Train_Loan_Template</fullName>
        <description>Cash Train Loan Template</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tom_s_templates/Cash_Train_Loan_Application_with_Signature</template>
    </alerts>
    <alerts>
        <fullName>Email_BDM_Support_on_Lead_Assignment</fullName>
        <description>Email BDM Support on Lead Assignment</description>
        <protected>false</protected>
        <recipients>
            <recipient>justin@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>steve@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Template/Lead_Assignment_Referral_Compaany</template>
    </alerts>
    <alerts>
        <fullName>Email_BDM_on_Lead_Assignment</fullName>
        <description>Email BDM on Lead Assignment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Template/Lead_Assignment_Referral_Compaany</template>
    </alerts>
    <alerts>
        <fullName>Email_owner_on_assignment_from_round_robin_app</fullName>
        <description>Email owner on assignment from round robin (app)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Lead_Template/Lead_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Fair_Go_Finance_Personal_Loan_Template</fullName>
        <description>Fair Go Finance Personal Loan Template</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tom_s_templates/Personal_Loan_Application_with_Signature</template>
    </alerts>
    <alerts>
        <fullName>Lead_Generated_notify_assigned_user</fullName>
        <description>Lead Generated - notify assigned user</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>jordan@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Template/Lead_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Lead_Notify_for_Unqualified_Leads</fullName>
        <description>Lead - Notify for PLS Unqualified Leads</description>
        <protected>false</protected>
        <recipients>
            <recipient>jordan@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Template/Lead_Unqualified</template>
    </alerts>
    <alerts>
        <fullName>Lead_PHL_New_Lead_Notification</fullName>
        <description>Lead - PHL New Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>rob@positivehomeloans.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Template/Lead_Assignment_Mortgage</template>
    </alerts>
    <alerts>
        <fullName>Send_GRJ_Forklifts_an_email_of_finance_lead_from_them</fullName>
        <ccEmails>greg@grjforklifts.com.au</ccEmails>
        <description>Send GRJ Forklifts an email of finance lead from them</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Lead_Template/Client_Referal_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_to_Cash_Train_about_LFPBC</fullName>
        <description>Send an email to Cash Train about LFPBC</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Tom_s_templates/LFPWBC_Cash_Train_Referral</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_to_Client_For_Veda_Check_from_LFPBC</fullName>
        <description>Send an email to Client For Veda Check from LFPBC</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Tom_s_templates/LFPWBC_Veda_Referral</template>
    </alerts>
    <alerts>
        <fullName>UCA_Car_Attempt_1_No_Info</fullName>
        <description>UCA Car Attempt 1 - No Info</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UCA_Attempteds/UCA_Car_Attempted_1</template>
    </alerts>
    <alerts>
        <fullName>UCA_Car_Attempt_3</fullName>
        <description>UCA Car Attempt 3</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UCA_Attempteds/UCA_Car_Attempted_3</template>
    </alerts>
    <alerts>
        <fullName>UCA_Car_Attempt_5</fullName>
        <description>UCA Car Attempt 5</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UCA_Attempteds/UCA_Car_Attempted_5</template>
    </alerts>
    <alerts>
        <fullName>UCA_Car_Attempt_7</fullName>
        <description>UCA Car Attempt 7</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UCA_Attempteds/UCA_Car_Attempted_7</template>
    </alerts>
    <alerts>
        <fullName>UCA_Finance_Attempt_1_No_Info</fullName>
        <description>UCA Finance Attempt 1 - No Info</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UCA_Attempteds/UCA_Finance_Attempt_1</template>
    </alerts>
    <alerts>
        <fullName>UCA_Finance_Attempt_3</fullName>
        <description>UCA Finance Attempt 3</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UCA_Attempteds/UCA_Finance_Attempt_3</template>
    </alerts>
    <alerts>
        <fullName>UCA_Finance_Attempt_5</fullName>
        <description>UCA Finance Attempt 5</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UCA_Attempteds/UCA_Finance_Attempt_5</template>
    </alerts>
    <alerts>
        <fullName>UCA_Finance_Attempt_7</fullName>
        <description>UCA Finance Attempt 7</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UCA_Attempteds/UCA_Finance_Attempt_7</template>
    </alerts>
    <alerts>
        <fullName>UCA_Owned_Leads_Notification</fullName>
        <description>UCA Owned Leads Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>daniel@usedcarsadelaide.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>luke.caesar@nodifi.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Template/Notify_Luke_on_UCA_Leads</template>
    </alerts>
    <fieldUpdates>
        <fullName>Application_Form_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Application_Forms_Received</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Application Form Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_according_to_Consultant_ID_AD</fullName>
        <field>OwnerId</field>
        <lookupValue>amy@positivelendingsolutions.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign according to Consultant ID - AD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_according_to_Consultant_ID_CS</fullName>
        <field>OwnerId</field>
        <lookupValue>chris.sims@positivegroup.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign according to Consultant ID - CS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_according_to_Consultant_ID_CT</fullName>
        <field>OwnerId</field>
        <lookupValue>christ@positivelendingsolutions.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign according to Consultant ID - CT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_according_to_Consultant_ID_DZ</fullName>
        <field>OwnerId</field>
        <lookupValue>daniel@usedcarsadelaide.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign according to Consultant ID - DZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_according_to_Consultant_ID_JM</fullName>
        <field>OwnerId</field>
        <lookupValue>jordan@positivelendingsolutions.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign according to Consultant ID - JM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_according_to_Consultant_ID_LC</fullName>
        <field>OwnerId</field>
        <lookupValue>luke.caesar@nodifi.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign according to Consultant ID - LC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_according_to_Consultant_ID_LS</fullName>
        <field>OwnerId</field>
        <lookupValue>liam.sutcliffe@nodifi.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign according to Consultant ID - LS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_according_to_Consultant_ID_SL</fullName>
        <field>OwnerId</field>
        <lookupValue>steve@positivelendingsolutions.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign according to Consultant ID - SL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_according_to_Consultant_ID_TC</fullName>
        <field>OwnerId</field>
        <lookupValue>tom@positivelendingsolutions.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign according to Consultant ID - TC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_according_to_Consultant_ID_TW</fullName>
        <field>OwnerId</field>
        <lookupValue>tim@positivelendingsolutions.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign according to Consultant ID - TW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Car_Campaign_Update</fullName>
        <field>Car_Campaign_Code__c</field>
        <formula>&quot;Yes&quot;</formula>
        <name>Car Campaign Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_to_open_lead_from_task</fullName>
        <description>SF automation assigmnet - set to open lead if new lead = true</description>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Change to open lead from task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_From_Unqualified_Mortgage</fullName>
        <field>From_Unqualified_Mortgage__c</field>
        <literalValue>1</literalValue>
        <name>Check From Unqualified Mortgage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Unqualified_Reason</fullName>
        <field>Unqualified_Reason__c</field>
        <name>Clear Unqualified Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_CRM_UID</fullName>
        <field>CRM_UID__c</field>
        <formula>Id</formula>
        <name>Copy CRM UID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Forms_Completed_information</fullName>
        <field>Forms_Completed2__c</field>
        <formula>LEFT(Forms_Completed__c,255)</formula>
        <name>Copy Forms Completed information</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Erase_from_company</fullName>
        <field>Company</field>
        <name>Erase &apos;-&apos; from company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Living_Exp_calculation_for_Customer</fullName>
        <field>Living_Expenses_Customer__c</field>
        <formula>Living_Expenses2_Customer__c</formula>
        <name>Lead Living Exp calculation for Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Set_Owner_to_Unassigned</fullName>
        <field>OwnerId</field>
        <lookupValue>Unassigned</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Lead - Set Owner to Unassigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Delacon_Caller_Phone_No</fullName>
        <description>Replace +61, 61 with 0 on MobilePhone</description>
        <field>DELAPLA__PLA_Caller_Phone_Number__c</field>
        <formula>IF( LEFT(DELAPLA__PLA_Caller_Phone_Number__c,2) == &apos;61&apos;, SUBSTITUTE(DELAPLA__PLA_Caller_Phone_Number__c, &apos;61&apos;, &apos;0&apos;), 
IF(LEFT(DELAPLA__PLA_Caller_Phone_Number__c,3) == &apos;+61&apos;, SUBSTITUTE(DELAPLA__PLA_Caller_Phone_Number__c, &apos;+61&apos;, &apos;0&apos;), 
DELAPLA__PLA_Caller_Phone_Number__c))</formula>
        <name>Lead_Update_Delacon_Caller_Phone_No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Inactive_Owner_to_Unassign</fullName>
        <field>OwnerId</field>
        <lookupValue>Unassigned</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Lead - Update Inactive Owner to Unassign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Last_Attempted_Contact_Dat</fullName>
        <field>Last_Attempted_Contact_Date__c</field>
        <formula>NOW()</formula>
        <name>Lead - Update Last Attempted Contact Dat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Loan_Type_as_Property</fullName>
        <field>Loan_Type__c</field>
        <literalValue>Property</literalValue>
        <name>Lead - Update Loan Type as Property</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Open_Attempted_Lead_Since</fullName>
        <field>Open_Attempted_Lead_Until__c</field>
        <formula>IF(
AND(
OR( ISPICKVAL(PRIORVALUE(Status),&apos;Open&apos;),
	ISPICKVAL(PRIORVALUE(Status),&apos;Attempted Contact&apos;)
),
ISPICKVAL(Status,&apos;Unqualified&apos;)
),
NOW(),null)</formula>
        <name>Lead - Update Open/Attempted Lead Since</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_of_Attempts</fullName>
        <field>X_of_Attempts_Done_Today__c</field>
        <formula>IF( 
AND( 
NOT(ISNULL(X_of_Attempts_Done_Today__c)), 
DATEVALUE(Last_Attempted_Contact_Date__c) == DATEVALUE(PRIORVALUE(Last_Attempted_Contact_Date__c)) 
), 
X_of_Attempts_Done_Today__c + 1, 
1)</formula>
        <name>Lead - Update # of Attempts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mobile_field_update</fullName>
        <field>SMS_Mobile__c</field>
        <formula>IF(LEFT(MobilePhone , 2) = &quot;04&quot;,TRIM( &quot;61&quot; &amp; TRIM(RIGHT(MobilePhone , LEN(MobilePhone)-1 ))),MobilePhone )</formula>
        <name>Mobile field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Lead_change_back_to_false</fullName>
        <description>When task created, reset new lead filed to NULL</description>
        <field>New_Lead__c</field>
        <name>New Lead - change back to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Next_Contact_Time_Update</fullName>
        <field>NVMConnect__NextContactTime__c</field>
        <formula>NOW()</formula>
        <name>Next Contact Time Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_country_code_from_Lead_mobile</fullName>
        <field>MobilePhone</field>
        <formula>TRIM(RIGHT(MobilePhone, 9) )</formula>
        <name>Remove country code from Lead mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_spaces_mobile_field</fullName>
        <field>MobilePhone</field>
        <formula>SUBSTITUTE(MobilePhone,&quot; &quot;,&quot;&quot;)</formula>
        <name>Remove spaces mobile field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_spaces_sms_mobile_field</fullName>
        <field>SMS_Mobile__c</field>
        <formula>SUBSTITUTE(SMS_Mobile__c,&quot; &quot;, &quot;&quot;)</formula>
        <name>Remove spaces sms mobile field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Seminar_lead_status_update</fullName>
        <field>Status</field>
        <literalValue>First Home Buyers Seminar - Adelaide</literalValue>
        <name>Seminar lead status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Status_to_Open</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Set Lead Status to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Two_week_callback_status_change_to_open</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Two week callback status change to open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UCA_Change_owner_to_Luke</fullName>
        <field>OwnerId</field>
        <lookupValue>luke.caesar@nodifi.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>UCA - Change owner to Luke</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Latest_Comment_w_Timestamp</fullName>
        <description>Update every change on DST</description>
        <field>Latest_Comment__c</field>
        <formula>LEFT(TEXT(DAY(DATEVALUE(NOW())))+&quot;/&quot; +TEXT(MONTH(DATEVALUE(NOW())))+&quot;/&quot; +TEXT(YEAR(DATEVALUE(NOW()))) &amp; &quot; &quot; 
&amp; 
TEXT(IF( 
OR( 
VALUE( MID( TEXT( NOW()+ ($Setup.General_Settings__c.Timestamp_Time_Zone__c) ), 12, 2 ) ) = 0, 
VALUE( MID( TEXT( NOW()+ ($Setup.General_Settings__c.Timestamp_Time_Zone__c) ), 12, 2 ) ) = 12 
), 
12, 
VALUE( MID( TEXT( NOW()+ ($Setup.General_Settings__c.Timestamp_Time_Zone__c)), 12, 2 ) ) 
- 
IF( 
VALUE( MID( TEXT( NOW()+ ($Setup.General_Settings__c.Timestamp_Time_Zone__c)), 12, 2 ) ) &lt; 12, 
0, 
12 
) 
)) 
&amp; &quot;:&quot; &amp; 
MID( TEXT( NOW()+ ($Setup.General_Settings__c.Timestamp_Time_Zone__c)), 15, 2 ) 
&amp; 
IF( 
VALUE( MID( TEXT( NOW()+ ($Setup.General_Settings__c.Timestamp_Time_Zone__c) ), 12, 2 ) ) &lt; 12, 
&quot; AM&quot;, 
&quot; PM&quot; 
) 
&amp; &apos; : &apos; &amp; Latest_Comment__c, 250)</formula>
        <name>Update Latest Comment w/ Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Owner_to_PHL_Team</fullName>
        <field>OwnerId</field>
        <lookupValue>Mortgage_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Lead Owner to PHL Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Record_type_as_Property</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Property</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Lead Record type as Property</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Status_Inv_Sem_Adl_2015</fullName>
        <description>Update Lead Status When Lead Source = Investment Seminar - Adelaide</description>
        <field>Status</field>
        <literalValue>Investment Seminar - Aug 15</literalValue>
        <name>Update Lead Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Unqualified_Reason</fullName>
        <field>Unqualified_Reason__c</field>
        <literalValue>Unable to Contact</literalValue>
        <name>Update Lead Unqualified Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_status_as_Unqualified</fullName>
        <field>Status</field>
        <literalValue>Unqualified</literalValue>
        <name>Update Lead status as Unqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_as_Indivudual</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Individual</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type as Individual</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Property</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Property</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Property</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_To_FHB_Seminar_Melbournme</fullName>
        <field>Status</field>
        <literalValue>First Home Buyers Seminar - Melbourne</literalValue>
        <name>Update Status To FHB Seminar Melbournme</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_referred_to_veda</fullName>
        <description>Leads not after finance looking for a free credit check</description>
        <field>Status</field>
        <literalValue>Referred to Veda</literalValue>
        <name>Update Status to referred to veda</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_field_to_Refered_to_Cash_Train</fullName>
        <field>Status</field>
        <literalValue>Referred to Cash Train</literalValue>
        <name>Update field to Refered to Cash Train</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_owner_for_round_robin</fullName>
        <description>change lead owner to &apos;unassigned&apos; for round robin ready</description>
        <field>OwnerId</field>
        <lookupValue>Unassigned</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update owner for round robin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_zero_in_front_of_Lead_mobile_num</fullName>
        <field>MobilePhone</field>
        <formula>IF(BEGINS(MobilePhone,&quot;4&quot;),&apos;0&apos;+MobilePhone, MobilePhone)</formula>
        <name>Update zero in front of Lead mobile num</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X4WD_and_Adventure_Show</fullName>
        <field>Status</field>
        <literalValue>4WD and Adventure Show</literalValue>
        <name>4WD and Adventure Show</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_first_name_capitals</fullName>
        <field>FirstName</field>
        <formula>UPPER(LEFT(FirstName, 1)) &amp;
MID(FirstName, 2, LEN(FirstName))</formula>
        <name>update first name capitals</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_form_location</fullName>
        <field>Form_Location__c</field>
        <formula>&quot;Call In&quot;</formula>
        <name>update form location</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_form_type</fullName>
        <field>Form__c</field>
        <formula>&quot;Call In&quot;</formula>
        <name>update form type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_lead_new</fullName>
        <field>New_Lead__c</field>
        <name>update lead = new</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_lead_source_from_old_database</fullName>
        <description>update lead source from old database</description>
        <field>LeadSource</field>
        <literalValue>Positive Lending Solutions - Website</literalValue>
        <name>update lead source from old database</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_lead_status_old_database</fullName>
        <field>Status</field>
        <literalValue>old database</literalValue>
        <name>update lead status old database</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_old_database_to_open</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>update old database to open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>upper_case_lastname</fullName>
        <field>LastName</field>
        <formula>UPPER(LEFT(LastName, 1)) &amp;
MID(LastName, 2, LEN(LastName))</formula>
        <name>upper case lastname</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Display_owner_to_new_lead</fullName>
        <apiVersion>33.0</apiVersion>
        <endpointUrl>http://www.positivelendingsolutions.com.au/round-robin/</endpointUrl>
        <fields>Id</fields>
        <fields>OwnerId</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>sri@positivelendingsolutions.com.au</integrationUser>
        <name>Display owner to new lead</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>%28NEW%29 Email on round robin assignment</fullName>
        <actions>
            <name>Email_owner_on_assignment_from_round_robin_app</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Generated3</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <formula>PRIORVALUE(OwnerID__c) = &quot;00G90000002DzFIEA0&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>4WD and Adventure Show</fullName>
        <actions>
            <name>X4WD_and_Adventure_Show</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>4WD and Adventure Show</value>
        </criteriaItems>
        <description>Will update lead status if lead source equals 4WD and Adventure Show</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Add activity for form entry</fullName>
        <active>false</active>
        <description>Add an activity record for each form submission made by the lead</description>
        <formula>AND( ISCHANGED( Form_Location__c ), ISCHANGED( Form__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Application Forms Received</fullName>
        <actions>
            <name>Application_Form_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Application_Form_Received</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  OR(  Application_Forms__c == 0,  Application_Forms__c == 1  ),  Form__c == &apos;Application Form&apos;,  NOT(ISPICKVAL(Consultant_ID__c,&apos;AA&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;CS&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;DZ&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;GG&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;JM&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;LS&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;LC&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;RM&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;SL&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;TW&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;TC&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;LM&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;TNC&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;ML&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;SE&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;AL&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;CT&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;JS&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;KP&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;AD&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;CZ&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;JD&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;MCO&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;AG&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;AH&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;SP&apos;)),  NOT(ISPICKVAL(Consultant_ID__c,&apos;SW&apos;))  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign SA BDM to Sam</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>SA BDM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Unassigned</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign UCA to Luke</fullName>
        <actions>
            <name>UCA_Change_owner_to_Luke</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>2 AND (1 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Used Cars Adelaide - Website,Used Cars Adelaide - Call In</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Unassigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>contains</operation>
            <value>UCA</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - AA</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>AA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - AD</fullName>
        <actions>
            <name>Assign_according_to_Consultant_ID_AD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>AD</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - AG</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>AG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - AH</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>AH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - CK</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>CK</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - CS</fullName>
        <actions>
            <name>Email_BDM_on_Lead_Assignment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Generated</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Finance App - Website</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Chris Sims</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - CT</fullName>
        <actions>
            <name>Assign_according_to_Consultant_ID_CT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>CT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - CZ</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>CZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - DZ</fullName>
        <actions>
            <name>Assign_according_to_Consultant_ID_DZ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>DZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - GG</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>GG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - GR</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>GR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - JB</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>JB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - JD</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>JD</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - JK</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>JK</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - JM</fullName>
        <actions>
            <name>Assign_according_to_Consultant_ID_JM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>JM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - JS</fullName>
        <actions>
            <name>Email_BDM_on_Lead_Assignment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Generated</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>John Schlobohm</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Finance App - Website</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - LC</fullName>
        <actions>
            <name>Assign_according_to_Consultant_ID_LC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>LC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - LS</fullName>
        <actions>
            <name>Assign_according_to_Consultant_ID_LS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>LS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - MCO</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>MCO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - RM</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>RM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>PHL Application Form</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - RP</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>RP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - SE</fullName>
        <actions>
            <name>Email_BDM_Support_on_Lead_Assignment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_BDM_on_Lead_Assignment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Generated</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Sam Elliott</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Finance App - Website</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - SK</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>SK</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - SL</fullName>
        <actions>
            <name>Assign_according_to_Consultant_ID_SL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>SL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - SM</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>SM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>PHL Application Form</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - SW</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>SW</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>PHL Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - TC</fullName>
        <actions>
            <name>Assign_according_to_Consultant_ID_TC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>TC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - TNC</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>TNC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - TW</fullName>
        <actions>
            <name>Assign_according_to_Consultant_ID_TW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>TW</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign according to Consultant ID - WS</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Consultant_ID__c</field>
            <operation>equals</operation>
            <value>WS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>Application Form</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Attempted contact email and sms</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Attempted Contact</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Call Lead Task</fullName>
        <actions>
            <name>Lead_Generated_notify_assigned_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Contact_New_Lead</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Calls_Made__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>A new task is created when lead comes in to contact person</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Car Campaign Assignment</fullName>
        <actions>
            <name>Car_Campaign_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2  OR 3  OR 4  OR 5  OR 6  OR 7  OR 8  OR 9  OR 10  OR 11  OR 12</booleanFilter>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>hilux</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>ranger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>dmax</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>colorado</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>yaris</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>accent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>focus</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>x-trail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>outlander</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>rav4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Customer_comments__c</field>
            <operation>contains</operation>
            <value>tucson</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cash Train After Veda Email - LFPWBC</fullName>
        <actions>
            <name>Send_an_email_to_Cash_Train_about_LFPBC</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_field_to_Refered_to_Cash_Train</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>LFPWBC_Cash_Train_Referral_email_sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>LoansForPeopleWithBadCredit - Website</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>LFPBC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Referred To Veda</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Forms_Completed__c</field>
            <operation>contains</operation>
            <value>Quick Quote</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Forms_Completed__c</field>
            <operation>contains</operation>
            <value>Approval</value>
        </criteriaItems>
        <description>Send a cash train email to clients who complete credit check first, then qq or pa form - ONLY on LFPWBC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cash Train Finance Email</fullName>
        <actions>
            <name>Cash_Train_Loan_Template</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Cash_Train_email_sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Referred To Cash Train</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>notEqual</operation>
            <value>LoansForPeopleWithBadCredit - Website</value>
        </criteriaItems>
        <description>Email template will automatically go out when the lead status is referred to Cash Train</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Forms Completed information</fullName>
        <actions>
            <name>Copy_Forms_Completed_information</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  NOT(ISNULL(Forms_Completed__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create task on new lead form entry</fullName>
        <actions>
            <name>Lead_Generated_notify_assigned_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New_Lead_change_back_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Generated2</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>This workflow will trigger when the record leaves the &apos;Unassigned&apos; queue.

Look to change this in due course to not use hard-coded ID&apos;s to move records</description>
        <formula>BEGINS(PRIORVALUE(OwnerId),&quot;00G90000002DzFI&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Erase %27-%27 company on creation</fullName>
        <actions>
            <name>Erase_from_company</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  Company == &apos;-&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FHB Confirmation SMS 13th</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>April 12th-14th 2016 Fhb Sem Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Seminar_date__c</field>
            <operation>equals</operation>
            <value>13/04/16</value>
        </criteriaItems>
        <description>Send confirmation SMS on FHB registration</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FHB Confirmation SMS 14th</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>April 12th-14th 2016 Fhb Sem Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Seminar_date__c</field>
            <operation>equals</operation>
            <value>14/04/16</value>
        </criteriaItems>
        <description>Send confirmation SMS on FHB registration</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FHB Confirmation SMS 5%2F10%2F17</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Positive Home Loans - Website</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Seminar_date__c</field>
            <operation>equals</operation>
            <value>5/10/17</value>
        </criteriaItems>
        <description>Send confirmation SMS on FHB registration</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Fair Go Finance Email</fullName>
        <actions>
            <name>Fair_Go_Finance_Personal_Loan_Template</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Personal_Loan_Application_emailed_to_Lead</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Referred to Fair Go Finance</value>
        </criteriaItems>
        <description>Email template will automatically go out when the lead status is referred to fair go finance</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>In Active -%3E Unassigned</fullName>
        <actions>
            <name>Update_owner_for_round_robin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assign new lead enquiry from In Active queue to Unassigned queue for round robin</description>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  ISPICKVAL(Status,&apos;Open&apos;),  OR(  Owner:Queue.QueueName == &apos;Car Loans - Clean Credit&apos;,  Owner:Queue.QueueName == &apos;In Active Users&apos;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Change to Property - Mortgage Team</fullName>
        <actions>
            <name>Update_Record_Type_to_Property</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  CASESAFEID(OwnerId) == $Label.Queue_Mortgage_Team_ID )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Loan Type as Property</fullName>
        <actions>
            <name>Lead_Update_Loan_Type_as_Property</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c, RecordType.DeveloperName = &apos;Property&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Notify Luke on UCA Owned Leads</fullName>
        <actions>
            <name>UCA_Owned_Leads_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c, CONTAINS(TEXT(LeadSource), &apos;Used Cars Adelaide&apos;), Owner:User.Alias = &apos;dzand&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Owner Dashcord to Unassigned</fullName>
        <actions>
            <name>Lead_Set_Owner_to_Unassigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  CASESAFEID(OwnerId) ==  CASESAFEID( $Label.User_Dashcord_Site_Guest_User_ID) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - PHL New Lead Notification</fullName>
        <actions>
            <name>Lead_PHL_New_Lead_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  OR(  AND(  ISNEW(),  ISPICKVAL(LeadSource, &apos;Positive Home Loans - Website&apos;)  ),  AND(  ISCHANGED(LeadSource),  ISPICKVAL(LeadSource, &apos;Positive Home Loans - Website&apos;)  ),  AND(  ISCHANGED(OwnerId),  Owner:User.Profile.Name == &apos;Mortgage&apos;  )  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - PLS Unqualified</fullName>
        <actions>
            <name>Lead_Notify_for_Unqualified_Leads</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c, ISPICKVAL(Status,&apos;Unqualified&apos;), RecordType.Name == &apos;Individual&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Reopen Unqualified Mortgage Leads</fullName>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  ISBLANK(Referral_Company__c),  ISPICKVAL(Status,&apos;Unqualified&apos;), ISPICKVAL(Residential_Status__c, &apos;Mortgage&apos;), OR( RecordType.Name == &apos;Individual&apos;, RecordType.Name == &apos;Property&apos; ), !From_Unqualified_Mortgage__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_From_Unqualified_Mortgage</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Clear_Unqualified_Reason</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_Lead_Status_to_Open</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Lead_Owner_to_PHL_Team</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Record_Type_to_Property</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead - Update Delacon Caller Phone Number</fullName>
        <actions>
            <name>Lead_Update_Delacon_Caller_Phone_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>NM. Updates Caller Phone Number&apos;s Area Code to match with Mobile phone of Person Account</description>
        <formula>AND(  ISPICKVAL(LeadSource,&apos;delacon&apos;),  OR(  LEFT(DELAPLA__PLA_Caller_Phone_Number__c, 3) == &apos;+61&apos;,  LEFT(DELAPLA__PLA_Caller_Phone_Number__c, 2) == &apos;61&apos;  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Update Last Attempted Contact Date</fullName>
        <actions>
            <name>Lead_Update_Last_Attempted_Contact_Dat</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Update_of_Attempts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  ISCHANGED(Attempted_Contact_Status__c), ISPICKVAL(Status,&apos;Attempted Contact&apos;),  OR(X_of_Attempts_Done_Today__c &lt;=  $Setup.General_Settings__c.Lead_Attempted_Contact_Limit_per_Day__c, ISNULL(X_of_Attempts_Done_Today__c), ISBLANK(X_of_Attempts_Done_Today__c)  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Update Open%2FAttempted Lead Until</fullName>
        <actions>
            <name>Lead_Update_Open_Attempted_Lead_Since</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( ISCHANGED(Status), OR( ISPICKVAL(Status,&apos;Unqualified&apos;), ISPICKVAL(Status,&apos;Open&apos;), ISPICKVAL(Status,&apos;Attempted Contact&apos;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Update Owner to Unassigned</fullName>
        <actions>
            <name>Lead_Update_Inactive_Owner_to_Unassign</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update Lead Owner to Unassigned when Lead is reopened</description>
        <formula>OR( 	AND( 		ISPICKVAL( Status, &quot;Open&quot;), 		CONTAINS( TEXT(LeadSource), &quot;website&quot;) 	), 	AND(  		ISPICKVAL(Status, &apos;Open&apos;),  		OR(  		Owner:User.IsActive == false,  		Lead_Owner_Full_Name__c == &apos;Tom Caesar&apos;,  		Lead_Owner_Full_Name__c == &apos;Jordan Mutton&apos;,  		Lead_Owner_Full_Name__c == &apos;Liam Sutcliffe&apos;,  		Lead_Owner_Full_Name__c == &apos;Daniel Adams&apos;,  		Lead_Owner_Full_Name__c == &apos;Alex Hall&apos;,  		Lead_Owner_Full_Name__c == &apos;Danika Mattaboni&apos;,  		Lead_Owner_Full_Name__c == &apos;Justin Beck&apos;,  		Lead_Owner_Full_Name__c == &apos;Melissa Caruso&apos;,  		Lead_Owner_Full_Name__c == &apos;Bree Bock&apos;,  		Lead_Owner_Full_Name__c == &apos;Sam Elliott&apos;,  		Lead_Owner_Full_Name__c == &apos;John Scholobohm&apos;,  		Lead_Owner_Full_Name__c == &apos;Graeme Galletly&apos;  		),  		NOT(BEGINS(OwnerId, &apos;00G&apos;))  	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Living Expenses calculation for Customer</fullName>
        <actions>
            <name>Lead_Living_Exp_calculation_for_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Living Expenses Customer is updated from Living Expenses2 Customer field.</description>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  NOT(ISNULL(Living_Expenses2_Customer__c)),   RecordType.Name != &apos;Property&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Loan Type as Car</fullName>
        <actions>
            <name>Update_Record_Type_as_Indivudual</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Loan_Type__c</field>
            <operation>equals</operation>
            <value>Car</value>
        </criteriaItems>
        <description>Update record type as Individual for leads</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Loan Type as Property</fullName>
        <actions>
            <name>Update_Lead_Record_type_as_Property</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Loan_Type__c</field>
            <operation>equals</operation>
            <value>Property</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead UID Copy</fullName>
        <actions>
            <name>Copy_CRM_UID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  Id != &quot;&quot;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Loan Type Update</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Loan_Type__c</field>
            <operation>equals</operation>
            <value>Car</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Magain Leads To Sarah</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Forms_Completed__c</field>
            <operation>contains</operation>
            <value>Magain</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notContain</operation>
            <value>Sarah</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Lead SMS Notification</fullName>
        <actions>
            <name>SMS_Notification</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>This workflow trigger will load the SMS Notification template in SMS Magic and send a welcome message to the new lead as soon as the Lead is assigned to a Lending Manager</description>
        <formula>AND( LastName =&quot;DO NOT ASSIGN MANUALLY&quot;, AND( PRIORVALUE( Consultant_Identifier__c )=&quot;&quot;, Consultant_Identifier__c !=&quot;&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Next Contact Time</fullName>
        <actions>
            <name>Next_Contact_Time_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.NVMConnect__NextContactTime__c</field>
            <operation>greaterThan</operation>
            <value>1/1/3000</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify GRJ Forklifts of finance lead</fullName>
        <actions>
            <name>Send_GRJ_Forklifts_an_email_of_finance_lead_from_them</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Client_Referer__c</field>
            <operation>equals</operation>
            <value>GRJ Forklifts</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Old Database workflow</fullName>
        <actions>
            <name>update_lead_status_old_database</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>old database</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Old database -%3E open lead</fullName>
        <actions>
            <name>Update_owner_for_round_robin</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_lead_source_from_old_database</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_old_database_to_open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>change from old database import to open lead when client contacts again</description>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  ISPICKVAL(Status,&apos;Old Database&apos;),  NOT(ISNULL(Forms_Completed__c)),  Owner:Queue.QueueName == &apos;Car Loans - Clean Credit&apos;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Re-opened leads to unassigned</fullName>
        <actions>
            <name>Update_owner_for_round_robin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  ISPICKVAL( Status, &quot;Open&quot;), CONTAINS( TEXT(LeadSource), &quot;website&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Remove country code from Lead mobile</fullName>
        <actions>
            <name>Remove_country_code_from_Lead_mobile</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_zero_in_front_of_Lead_mobile_num</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  OR( BEGINS(MobilePhone , &apos;+61&apos;), BEGINS(MobilePhone , &apos;61&apos;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Cash Train Email to LFPBC</fullName>
        <actions>
            <name>Send_an_email_to_Cash_Train_about_LFPBC</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_field_to_Refered_to_Cash_Train</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>LFPWBC_Cash_Train_Referral_email_sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 4 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>LoansForPeopleWithBadCredit - Website</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>LFPBC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Forms_Completed__c</field>
            <operation>notContain</operation>
            <value>credit check</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send SMS Privacy for Lead</fullName>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  ISPICKVAL(SMS_Privacy__c,&apos;1&apos;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send SMS when PLS SMS App completed on Finance App Website</fullName>
        <active>false</active>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  Form__c == &apos;SMS App Form&apos;,  ISPICKVAL(Status,&apos;Open&apos;),  NOT(ISNULL(MobilePhone)), Lead_Owner_Full_Name__c == &apos;Chris Sims&apos;, RecordType.Name != &apos;Property&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send SMS when PLS SMS App completed on Finance App Website Generic</fullName>
        <active>false</active>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  Form__c == &apos;SMS App Form&apos;,  ISPICKVAL(Status,&apos;Open&apos;),  Form_Location__c == &apos;SMS App - PLS - Car Finance&apos;  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send SMS when PLS SMS App completed on Finance App Website Generic %28B%29</fullName>
        <active>false</active>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  Form__c == &apos;SMS App Form&apos;, ISPICKVAL(Status,&apos;Open&apos;), Form_Location__c == &apos;SMS App - PLS - Personal Finance&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send SMS when SMS App completed on Climat Finance App Website</fullName>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c, Form__c   == &apos;SMS App Form&apos;, Referral_Company_Finance_App__c == &apos;https://climat.finance-app.com.au/&apos;, RecordType.Name != &apos;Property&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send SMS when SMS App completed on Finance App Website</fullName>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c, Form__c   == &apos;SMS App Form&apos;, NOT(ISNULL(MobilePhone)), Referral_Company_Finance_App__c == &apos;https://climat.finance-app.com.au/&apos;, RecordType.Name != &apos;Property&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send an Veda Email to LFPBC</fullName>
        <actions>
            <name>Update_Status_to_referred_to_veda</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Veda_Credit_Check_Email_Sent_to_Client</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Lead.Forms_Completed__c</field>
            <operation>contains</operation>
            <value>Credit Check</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>LoansForPeopleWithBadCredit - Website</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>LFPBC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Two week callback status change to open</fullName>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c, ISPICKVAL(Status, &apos;2 week callback&apos;), RecordType.Name != &apos;Property&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Two_week_callback_status_change_to_open</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Unqualified Asset w%2FMortgage</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Unqualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Individual</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Residential_Status__c</field>
            <operation>equals</operation>
            <value>Mortgage,Own Outright,Own Home With Mortgage,Own Home</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Unqualified Mortgage Trigger</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Unqualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Residential_Status__c</field>
            <operation>equals</operation>
            <value>Mortgage,Own Home With Mortgage</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Latest Comment w%2F Timestamp</fullName>
        <actions>
            <name>Update_Latest_Comment_w_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update Latest Comment including its timestamp</description>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c, ISCHANGED(Latest_Comment__c),  NOT( CONTAINS(Latest_Comment__c, &apos;Attempted&apos;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Status to</fullName>
        <actions>
            <name>Update_Lead_Status_Inv_Sem_Adl_2015</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Investment Seminar - Adelaide</value>
        </criteriaItems>
        <description>If Lead Source = Investment Seminar - Adelaide Update Lead Status - Investment Seminar - Aug 15</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update SMS Mobile field with country code</fullName>
        <actions>
            <name>Mobile_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_spaces_mobile_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_spaces_sms_mobile_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_zero_in_front_of_Lead_mobile_num</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule will update SMS Mobile field with country code required for incoming SMS loo-up functionality</description>
        <formula>AND (  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  ISCHANGED( MobilePhone ) || ISNEW()  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Status to FHB Seminar</fullName>
        <actions>
            <name>Update_Status_To_FHB_Seminar_Melbournme</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
            <value>March 9th Fhb Sem Registration</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update form if call in lead</fullName>
        <actions>
            <name>update_form_location</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_form_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>update form type, form location to call in if call in selected.</description>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  ISPICKVAL(LeadSource,&apos;Positive Lending Solutions - Call in&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update lead status to FHB Sem - ADL - Nov 15</fullName>
        <actions>
            <name>Seminar_lead_status_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Fhb Sem - Adelaide</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>When Lead Attempted Contact 10 update status field</fullName>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  ISPICKVAL(Attempted_Contact_Status__c,&apos;Attempted Contact 10&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Lead_Unqualified_Reason</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Lead_status_as_Unqualified</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>email BDM %26 BDM Support on BDM lead assignment</fullName>
        <actions>
            <name>Email_BDM_Support_on_Lead_Assignment</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>email BDM &amp; BDM Support on BDM lead assignment</description>
        <formula>Owner:User.Alias=(&quot;selli&quot;) || Owner:User.Alias=(&quot;bbock&quot;) || Owner:User.Alias=(&quot;jschl&quot;) &amp;&amp; RecordType.Name != &apos;Property&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>email on round robin assignment</fullName>
        <actions>
            <name>Lead_Generated_notify_assigned_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Generated3</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Deactivated. new process has been made for Process Builder</description>
        <formula>OR( BEGINS( PRIORVALUE( OwnerId ) , &quot;00G90000002DzFI&quot;), ISNEW(),  ISCHANGED(OwnerId) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>upper case fname</fullName>
        <actions>
            <name>update_first_name_capitals</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>upper_case_lastname</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Upper case first letter of name</description>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Lead_Workflows__c,  NOT(ISBLANK(FirstName)),  OR(  ISNEW(),  ISCHANGED(FirstName),  ISCHANGED(LastName),  ISCHANGED(LastModifiedDate)  ),  LEFT(FirstName, 1) &lt;&gt; UPPER(LEFT(FirstName, 1)),  LEFT(LastName, 1) &lt;&gt; UPPER(LEFT(LastName, 1))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Application_Form_Received</fullName>
        <assignedToType>owner</assignedToType>
        <description>Online Application form received.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Application Form Received</subject>
    </tasks>
    <tasks>
        <fullName>Cash_Train_email_sent</fullName>
        <assignedToType>owner</assignedToType>
        <description>Cash Train Loan Application has been automatically emailed to Lead</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Cash Train email sent</subject>
    </tasks>
    <tasks>
        <fullName>Contact_New_Lead</fullName>
        <assignedToType>owner</assignedToType>
        <description>CALL THIS LEAD ASAP</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Contact New Lead</subject>
    </tasks>
    <tasks>
        <fullName>LFPWBC_Cash_Train_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <description>Email sent to client referring to cash train</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>LFPWBC Cash Train Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>LFPWBC_Cash_Train_Referral_email_sent</fullName>
        <assignedToType>owner</assignedToType>
        <description>LFPWBC Cash Train Referral email sent.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>LFPWBC Cash Train Referral email sent.</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Generated</fullName>
        <assignedToType>owner</assignedToType>
        <description>Lead has completed a form and requires a follow up.</description>
        <dueDateOffset>-1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Lead Generated</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Generated2</fullName>
        <assignedToType>owner</assignedToType>
        <description>New lead generated and email sent to notify lead owner</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Lead Generated</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Generated3</fullName>
        <assignedToType>owner</assignedToType>
        <description>Round Robin Assigned</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email Sent to Lead Owner - Round Robin activity</subject>
    </tasks>
    <tasks>
        <fullName>Personal_Loan_Application_emailed_to_Lead</fullName>
        <assignedToType>owner</assignedToType>
        <description>Personal Loan Application has been automatically emailed to Lead</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Fairgo Finance email sent</subject>
    </tasks>
    <tasks>
        <fullName>SMS_Notification</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Lead-638a</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>SMS Notification</subject>
    </tasks>
    <tasks>
        <fullName>Thank_you_mail_sent_to_Lead</fullName>
        <assignedToType>owner</assignedToType>
        <description>Thank you mail has been sent to Lead.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Thank you mail sent to Lead</subject>
    </tasks>
    <tasks>
        <fullName>Veda_Credit_Check_Email_Sent_to_Client</fullName>
        <assignedToType>owner</assignedToType>
        <description>Veda credit check email sent to client with link to 

https://forms.mycreditfile.com.au/Apply/Personal/Credit/Step1.aspx?form=FreeCreditFile</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Veda Credit Check Email Sent to Client</subject>
    </tasks>
</Workflow>

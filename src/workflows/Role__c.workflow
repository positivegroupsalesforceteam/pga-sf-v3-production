<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Role_SMS_Sent</fullName>
        <description>Role SMS Sent</description>
        <protected>false</protected>
        <recipients>
            <recipient>jesfer.baculod@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rexie.david@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Role_SMS_Sent</template>
    </alerts>
</Workflow>

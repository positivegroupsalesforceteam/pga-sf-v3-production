<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ANZ_Credit_Proposal_Email_Template_Send</fullName>
        <description>ANZ Credit Proposal Email Template Send</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Credit_Proposals_compliance/ANZ_Credit_Proposal_Template_with_Signature</template>
    </alerts>
    <alerts>
        <fullName>Approval_conditions_email_notification</fullName>
        <description>Approval conditions email notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Approval_Conditions_mail_to_rep</template>
    </alerts>
    <alerts>
        <fullName>Approved_Insurance_Email</fullName>
        <ccEmails>info@positivelendingsolutions.com.au</ccEmails>
        <description>Approved Insurance Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Emails/t1464178953496</template>
    </alerts>
    <alerts>
        <fullName>Approved_Insurance_Email_Liberty</fullName>
        <ccEmails>info@positivelendingsolutions.com.au</ccEmails>
        <description>Approved Insurance Email - Liberty</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Emails/New_Insurance_Approved_Email_Liberty</template>
    </alerts>
    <alerts>
        <fullName>Approved_and_vehicle_source_equals_New_Car_Notification</fullName>
        <description>Approved and vehicle source equals New Car Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>chris.sims@positivegroup.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Templates/Opp_stage_equals_approved_and_vehicle_source_Equals_New_Car</template>
    </alerts>
    <alerts>
        <fullName>Capital_Credit_Proposal_Email_Template_Send</fullName>
        <description>Capital Credit Proposal Email Template Send</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Credit_Proposals_compliance/Credit_Proposal_Capital_Finance</template>
    </alerts>
    <alerts>
        <fullName>Email_BDM_Sup_on_BDL_Loan_Docs_Sent</fullName>
        <description>Email BDM Sup on BDL Loan Docs Sent</description>
        <protected>false</protected>
        <recipients>
            <recipient>justin@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>steve@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Old_Emails_Storage/Notify_Loan_Docs_Sent</template>
    </alerts>
    <alerts>
        <fullName>Email_Chris_and_Graeme_about_Ready_for_Submission</fullName>
        <description>Email Chris and Graeme about Ready for Submission</description>
        <protected>false</protected>
        <recipients>
            <recipient>tim@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Unfiled_Public_Email_Templates_2/Notify_TL_when_Opp_Stage_Ready_for_Submission_more_than_once</template>
    </alerts>
    <alerts>
        <fullName>Email_Insurance_FSGs_Boat</fullName>
        <description>Email Insurance FSGs Boat</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Insurance_Emails/Insurance_Email_Boat</template>
    </alerts>
    <alerts>
        <fullName>Email_Insurance_FSGs_Car_Commercial</fullName>
        <description>Email Insurance FSGs Car Commercial</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Insurance_Emails/Insurance_Email_Car_Commercial</template>
    </alerts>
    <alerts>
        <fullName>Email_Insurance_FSGs_Car_Consumer</fullName>
        <description>Email Insurance FSGs Car Consumer</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Insurance_Emails/Insurance_Email_Car_Consumer</template>
    </alerts>
    <alerts>
        <fullName>Email_Insurance_FSGs_Caravan</fullName>
        <description>Email Insurance FSGs Caravan</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Insurance_Emails/Insurance_Email_Caravan</template>
    </alerts>
    <alerts>
        <fullName>Email_Insurance_FSGs_Motorcycle</fullName>
        <description>Email Insurance FSGs Motorcycle</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Insurance_Emails/Insurance_Email_Motorcycle</template>
    </alerts>
    <alerts>
        <fullName>Email_Insurance_FSGs_Truck</fullName>
        <description>Email Insurance FSGs Truck</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Insurance_Emails/Insurance_Email_Truck</template>
    </alerts>
    <alerts>
        <fullName>Email_Insurance_FSGs_Watercraft</fullName>
        <description>Email Insurance FSGs Watercraft</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Insurance_Emails/Insurance_Email_Watercraft</template>
    </alerts>
    <alerts>
        <fullName>Email_Luke_Dz_when_SA_Deal_approved</fullName>
        <description>Email Luke/Dz when SA Deal approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>luke.caesar@nodifi.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UCA_Attempteds/SA_VIC_Deal_Approval_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Owner_When_Deal_is_Suspended</fullName>
        <description>Email Owner When Deal is Suspended</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Old_Emails_Storage/Deal_Suspended_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Support_When_Stage_Loan_Docs_Recieved</fullName>
        <description>Email Support When Stage = Loan Docs Recieved</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Support</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Old_Emails_Storage/Notify_Loan_Docs_Recieved</template>
    </alerts>
    <alerts>
        <fullName>Email_to_remind_sales_guys_to_call_customer_to_let_them_know</fullName>
        <ccEmails>rob@positivelendingsolutions.com.au</ccEmails>
        <description>Email to remind sales guys to call customer to let them know</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Sales_Templates/Home_Loan_Follow_up</template>
    </alerts>
    <alerts>
        <fullName>Finance_1_Credit_Proposal_Email_Template_Send</fullName>
        <description>Finance 1 Credit Proposal Email Template Send</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Credit_Proposals_compliance/Finance_1_Credit_Proposal_Template_with_Signature</template>
    </alerts>
    <alerts>
        <fullName>GE_Credit_Proposal_Email_Template_Send</fullName>
        <description>GE Credit Proposal Email Template Send</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Credit_Proposals_compliance/GE_Money_Credit_Proposal_Template_with_Signature</template>
    </alerts>
    <alerts>
        <fullName>Liberty_Credit_Proposal_Email_Template_Send</fullName>
        <description>Liberty Credit Proposal Email Template Send</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Credit_Proposals_compliance/Liberty_Financial_Credit_Proposal_Template_with_Signature</template>
    </alerts>
    <alerts>
        <fullName>Loan_Docs_Requested_Email_Alert</fullName>
        <description>Loan Docs Requested Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Support</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Loan_Docs_Requested_Notification</template>
    </alerts>
    <alerts>
        <fullName>Macquarie_Credit_Proposal_Email_Template_Send</fullName>
        <description>Macquarie Credit Proposal Email Template Send</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Credit_Proposals_compliance/Macquarie_Credit_Proposal_Template_with_Signature</template>
    </alerts>
    <alerts>
        <fullName>Money_3_Credit_Proposal_Email_Template_Send</fullName>
        <description>Money 3 Credit Proposal Email Template Send</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Credit_Proposals_compliance/Money_3_Credit_Proposal_Template_with_Signature</template>
    </alerts>
    <alerts>
        <fullName>NOW_Finance_Credit_Proposal_Email_Template_Send</fullName>
        <description>NOW Finance Credit Proposal Email Template Send</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Credit_Proposals_compliance/Credit_Proposal_Now_Finance</template>
    </alerts>
    <alerts>
        <fullName>Notify_BDM_Sup_of_deal_suspended</fullName>
        <description>Notify BDM Sup of deal suspended</description>
        <protected>false</protected>
        <recipients>
            <recipient>justin@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>steve@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Old_Emails_Storage/Deal_Suspended_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_Of_Loan_Docs_Sent</fullName>
        <description>Notify Owner Of Loan Docs Sent</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Old_Emails_Storage/Notify_Loan_Docs_Sent</template>
    </alerts>
    <alerts>
        <fullName>Notify_submission_processors_of_opportunity_ready_for_submission</fullName>
        <description>Notify submission processors of opportunity ready for submission</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Support</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Old_Emails_Storage/Notify_Ready_for_Submission</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Closed_Lost_Notification</fullName>
        <description>Opportunity - Notify for PLS Closed Lost Opps</description>
        <protected>false</protected>
        <recipients>
            <recipient>jordan@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Opportunity_Templates/Opportunity_Closed_Lost_Notification</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Mortgage_Settled_Notification</fullName>
        <description>Opportunity - Mortgage - Settled Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>clint.sjoberg@positivehomeloans.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rob@positivehomeloans.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Opportunity_Templates/Opportunity_Mortgage_Settled_Notification</template>
    </alerts>
    <alerts>
        <fullName>Pepper_Finance_Credit_Proposal_Email_Template_Send</fullName>
        <description>Pepper Finance Credit Proposal Email Template Send</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Credit_Proposals_compliance/Credit_Proposal_Pepper_Finance</template>
    </alerts>
    <alerts>
        <fullName>Ready_for_submission_alert</fullName>
        <description>Ready for submission alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>alex@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>liam.sutcliffe@nodifi.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Old_Emails_Storage/Notify_Ready_for_Submission</template>
    </alerts>
    <alerts>
        <fullName>Send_a_mail_to_Luke_when_vehicle_source_is_UCA</fullName>
        <description>Send a mail to Luke when vehicle source is UCA</description>
        <protected>false</protected>
        <recipients>
            <recipient>luke.caesar@nodifi.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Template/Vehicle_source_is_UCA</template>
    </alerts>
    <alerts>
        <fullName>Sent_for_Settlement_Notification</fullName>
        <description>Sent for Settlement Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Sent_for_settlement_Notification</template>
    </alerts>
    <alerts>
        <fullName>St_George_Credit_Proposal_Email_Template_Send</fullName>
        <description>St.George Credit Proposal Email Template Send</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Credit_Proposals_compliance/Credit_Proposal_St_George</template>
    </alerts>
    <alerts>
        <fullName>Submitted_to_Lender_Notification</fullName>
        <description>Submitted to Lender Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Submitted_to_Lender_Notification</template>
    </alerts>
    <alerts>
        <fullName>Yamaha_Credit_Proposal_Email_Template_Send</fullName>
        <description>Yamaha Credit Proposal Email Template Send</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Credit_Proposals_compliance/Credit_Proposal_Yamaha</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_first_name_from_account_to_opportuni</fullName>
        <field>First_name_opportunity_hidden__c</field>
        <formula>Account.FirstName</formula>
        <name>Add first name from account to opportuni</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bizible_Opp_Amount</fullName>
        <field>bizible2__Bizible_Opportunity_Amount__c</field>
        <formula>Total_Commision__c</formula>
        <name>Bizible Opp Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Opp_StageName_to_Account</fullName>
        <field>Opp_Stage__c</field>
        <formula>Opp_Stage__c</formula>
        <name>Copy Opp StageName to Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_first_name_from_account_to_opportun</fullName>
        <field>First_name_opportunity_hidden__c</field>
        <formula>Account.FirstName</formula>
        <name>Copy first name from account to opportun</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_of_Person_Account_Email</fullName>
        <field>Email__c</field>
        <formula>Account.PersonEmail</formula>
        <name>Copy of Person Account Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lender_Flag_field_update_as_one</fullName>
        <field>Lender_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Lender Flag field update as one</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lender_Flag_field_update_as_zero</fullName>
        <field>Lender_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Lender Flag field update as zero</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Living_Expenses_calculation_for_Cust</fullName>
        <field>Living_Expenses_Customer__c</field>
        <formula>Living_Expenses2_Customer__c</formula>
        <name>Opp Living Expenses calculation for Cust</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_App_Forms_Taken_Stage</fullName>
        <field>StageName</field>
        <literalValue>Application Forms Taken</literalValue>
        <name>Opportunity - App Forms Taken Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Name_Update</fullName>
        <description>Updates Opportunity name to Owner - Product - Amount</description>
        <field>Name</field>
        <formula>Account.FirstName&amp;&quot; &quot;&amp; Account.LastName &amp; &quot; - &quot; &amp; TEXT(CloseDate)  &amp; &quot; - &quot; &amp; Loan_Reference__c</formula>
        <name>Opportunity Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_TrustPilot_Link_Generated</fullName>
        <field>TrustPilot_Link_Generated__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity - TrustPilot Link Generated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ready_for_Submission_upd_Increment_Stage</fullName>
        <field>Increment_StageName__c</field>
        <formula>IF ( ISBLANK(Increment_StageName__c ), 1, Increment_StageName__c  + 1)</formula>
        <name>Ready for Submission upd Increment Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_spaces_mobile_field</fullName>
        <field>Mobile__c</field>
        <formula>SUBSTITUTE(Mobile__c,&quot; &quot;,&quot;&quot;)</formula>
        <name>Remove spaces mobile field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_spaces_sms_mobile_field</fullName>
        <field>SMS_Mobile__c</field>
        <formula>SUBSTITUTE(SMS_Mobile__c,&quot; &quot;,&quot;&quot;)</formula>
        <name>Remove spaces sms mobile field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Settled_Date_updated_with_today_s_date</fullName>
        <description>Settled date = closed date (standard field)</description>
        <field>CloseDate</field>
        <formula>Today()</formula>
        <name>Settled Date updated with today&apos;s date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_set_to_null</fullName>
        <description>If the owner of the Opportunity is a member of the PLS Team default value of Status should be null</description>
        <field>Status__c</field>
        <name>Status set to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Upd_Opp_as_Car_when_Acc_is_Car</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Car</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Upd Opp as Car when Acc is Car</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Upd_Opp_as_Individual_when_Acc_is_Indivi</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Individual</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Upd Opp as Individual when Acc is Indivi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Upd_Opp_as_Propety_when_Acc_is_Property</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Property</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Upd Opp as Propety when Acc is Property</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Email_fr</fullName>
        <field>PersonEmail</field>
        <formula>Email__c</formula>
        <name>Update Account Email fr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Email_from_Opportunity</fullName>
        <field>PersonEmail</field>
        <formula>Email__c</formula>
        <name>Update Account Email from Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Mobile_from_Opportunity</fullName>
        <field>PersonMobilePhone</field>
        <formula>Mobile__c</formula>
        <name>Update Account Mobile from Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Application_Date_on_Approved</fullName>
        <field>Approval_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Application Date on Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Employment_DetailsPreviousHistory</fullName>
        <field>Previous_employment_history__pc</field>
        <formula>Previous_employment_history__c</formula>
        <name>Update Employment DetailsPreviousHistory</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Employment_Details_CurrentStatus</fullName>
        <description>Whenever a new opportunity record is created for an Account, it will update the Account with the latest employment details as recorded on the most recent Opportunity record</description>
        <field>Employment_Status__pc</field>
        <formula>Employment_Status__c</formula>
        <name>Update Employment Details-CurrentStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Employment_Details_Gap</fullName>
        <field>How_long_was_the_gap_between_jobs__c</field>
        <formula>How_long_was_the_gap_between_jobs__c</formula>
        <name>Update Employment Details-Gap</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Employment_Details_LengthPrevious</fullName>
        <field>length_of_time_in_previous_employment__pc</field>
        <formula>Length_of_time_in_Previous_Employment__c</formula>
        <name>Update Employment Details-LengthPrevious</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Employment_Details_Length_Current</fullName>
        <description>Whenever a new opportunity record is created for an Account, it will update the Account with the latest employment details as recorded on the most recent Opportunity record</description>
        <field>Length_of_time_in_employment__pc</field>
        <formula>Length_of_time_in_employment__c</formula>
        <name>Update Employment Details-Length Current</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Latest_Comment_Date_Time</fullName>
        <field>Latest_Comment_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Latest Comment (Date/Time)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Latest_Comment_w_Timestamp</fullName>
        <field>Latest_Comment__c</field>
        <formula>LEFT(TEXT(DAY(DATEVALUE(Latest_Comment_Date_Time__c )))+&quot;/&quot; +TEXT(MONTH(DATEVALUE(Latest_Comment_Date_Time__c )))+&quot;/&quot; +TEXT(YEAR(DATEVALUE(Latest_Comment_Date_Time__c ))) &amp; &quot; &quot; 
&amp; 
TEXT(IF( 
OR( 
VALUE( MID( TEXT( Latest_Comment_Date_Time__c + ($Setup.General_Settings__c.Timestamp_Time_Zone__c) ), 12, 2 ) ) = 0, 
VALUE( MID( TEXT( Latest_Comment_Date_Time__c + ($Setup.General_Settings__c.Timestamp_Time_Zone__c) ), 12, 2 ) ) = 12 
), 
12, 
VALUE( MID( TEXT( Latest_Comment_Date_Time__c + ($Setup.General_Settings__c.Timestamp_Time_Zone__c)), 12, 2 ) ) 
- 
IF( 
VALUE( MID( TEXT( Latest_Comment_Date_Time__c + ($Setup.General_Settings__c.Timestamp_Time_Zone__c)), 12, 2 ) ) &lt; 12, 
0, 
12 
) 
)) 
&amp; &quot;:&quot; &amp; 
MID( TEXT( Latest_Comment_Date_Time__c + ($Setup.General_Settings__c.Timestamp_Time_Zone__c)), 15, 2 ) 
&amp; 
IF( 
VALUE( MID( TEXT( Latest_Comment_Date_Time__c + ($Setup.General_Settings__c.Timestamp_Time_Zone__c) ), 12, 2 ) ) &lt; 12, 
&quot; AM&quot;, 
&quot; PM&quot; 
) 
&amp; &apos; : &apos; &amp; Latest_Comment__c , 250)</formula>
        <name>Update Latest Comment w/ Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Priority</fullName>
        <field>Priority_Deal__c</field>
        <literalValue>1</literalValue>
        <name>Update Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Property</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Property</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Property</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Support_Comment_w_Timestam</fullName>
        <field>Sales_Support_Comment__c</field>
        <formula>TEXT(DAY(TODAY())) + &quot;/&quot; + TEXT(MONTH(TODAY())) + &quot;/&quot; + TEXT(YEAR(TODAY())) + &quot; &quot; + MID(TEXT(NOW() + $Setup.General_Settings__c.Timestamp_Time_Zone__c), 12,5 ) + &quot; &quot; + &quot; : &quot; + TEXT(StageName)</formula>
        <name>Update Sales Support Comment w/ Timestam</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Loan_Docs_Sent</fullName>
        <field>StageName</field>
        <literalValue>Loan Documents Sent</literalValue>
        <name>Update Stage to Loan Docs Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_opportunity_Mobile_from_account</fullName>
        <field>Mobile__c</field>
        <formula>Account.PersonContact.MobilePhone</formula>
        <name>Update opportunity Mobile from account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Mobile_field</fullName>
        <field>SMS_Mobile__c</field>
        <formula>IF(LEFT(Mobile__c, 2) = &quot;04&quot;,TRIM( &quot;61&quot; &amp; TRIM(RIGHT(Mobile__c, LEN(Mobile__c)-1 ))),Mobile__c)</formula>
        <name>update Mobile field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_amount</fullName>
        <field>Amount</field>
        <formula>Total_Commision__c - 0</formula>
        <name>update amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_mobile_from_account_to_opportunit</fullName>
        <field>Mobile__c</field>
        <formula>Account.PersonContact.MobilePhone</formula>
        <name>update mobile from account to opportunit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_opp_mobile_from_account</fullName>
        <field>Mobile__c</field>
        <formula>Account.PersonContact.MobilePhone</formula>
        <name>update opp mobile from account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ANZ Credit Proposal Contact Send</fullName>
        <actions>
            <name>ANZ_Credit_Proposal_Email_Template_Send</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>ANZ_Credit_Proposal</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>ANZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends ANZ Credit Proposal to Contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Add account name and stage  to pipeline field</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Submitted to Lender,Approved,Tax Invoice Requested,Loan Documents Sent,Sent for Settlement</value>
        </criteriaItems>
        <description>Will add account name and stage to field to use in pipeline dashboard</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Form SMS Notification</fullName>
        <actions>
            <name>SMS_Notification</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Application Forms Sent</value>
        </criteriaItems>
        <description>SMS notification when stage has been changed to Application Form Sent within an Opportunity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Form SMS Notification to Mobile Field</fullName>
        <actions>
            <name>SMS_Notification_to_Mobile</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Application Forms Sent</value>
        </criteriaItems>
        <description>SMS notification when stage has been changed to &quot;Application Form Sent&quot; within an Opportunity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Approval Conditions Notification</fullName>
        <actions>
            <name>Approval_conditions_email_notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>This workflow rule is triggered when one of the Sales Support Team members changes the Stage value on an Opportunity to &apos;Approved&apos;. This will fire an email notification to the Opportunity Owner.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Approved Insurance Email - Liberty</fullName>
        <actions>
            <name>Approved_Insurance_Email_Liberty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Insurance_FSGs_Emailed</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Type2__c</field>
            <operation>notContain</operation>
            <value>Personal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>Liberty</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Approved Insurance Email - Non Liberty</fullName>
        <actions>
            <name>Approved_Insurance_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Insurance_FSGs_Emailed</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Type2__c</field>
            <operation>notContain</operation>
            <value>Personal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>notEqual</operation>
            <value>Liberty</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Approved and vehicle source Equals New Car Notification</fullName>
        <actions>
            <name>Approved_and_vehicle_source_equals_New_Car_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Vehicle_Source__c</field>
            <operation>equals</operation>
            <value>New Car</value>
        </criteriaItems>
        <description>This workflow rule is triggered when one of the Sales Support Team members changes the Stage value on an Opportunity to &apos;Approved&apos; and vehicle source is &quot;New Car&quot;. This will fire an email notification to the Opportunity Owner.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Capital Credit Proposal Contact Send</fullName>
        <actions>
            <name>Capital_Credit_Proposal_Email_Template_Send</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Capital_Credit_Proposal_emailed_to_Opportunity_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>Capital</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends Capital Credit Proposal to Contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Centre 1 Credit Proposal Contact Send</fullName>
        <actions>
            <name>Centre_1_Credit_Proposal_emailed_to_Opportunity_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>Centre 1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends Centre 1 Credit Proposal to Contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Email to Opportunity</fullName>
        <actions>
            <name>Copy_of_Person_Account_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Email is copied once from Account to Opportunity</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Copy Mobile to Opportunity</fullName>
        <actions>
            <name>Update_Account_Mobile_from_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_opp_mobile_from_account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Mobile__c</field>
            <operation>equals</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Copy Opp StageName to Account</fullName>
        <actions>
            <name>Copy_Opp_StageName_to_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission,Tax Invoice Requested</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy first name from account to opportunity</fullName>
        <actions>
            <name>Add_first_name_from_account_to_opportuni</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.First_name_opportunity_hidden__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create Morgage Task for home loans team after Car Loan has Settled</fullName>
        <actions>
            <name>Opportunity_Mortgage_Settled_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Settle_Car_Loan_Morgage_Lead</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Residential_Status__c</field>
            <operation>equals</operation>
            <value>Mortgage</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Settled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_Referer__c</field>
            <operation>notEqual</operation>
            <value>Assured Home Loans</value>
        </criteriaItems>
        <description>Creates a follow up task for home loans team to chase Morgage Lead from Settled Car Loan</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email BDM when Opportunity stage is submitted to lender</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Submitted to Lender</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Referral_Company_Text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Sam Elliott</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>John Schlobohm</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Gary Rawlings</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Finance 1 Credit Proposal Contact Send</fullName>
        <actions>
            <name>Finance_1_Credit_Proposal_Email_Template_Send</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Finance_1_Credit_Proposal_emailed_to_Opportunity_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>Finance1,Finance 1 Economy</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends Finance 1 Credit Proposal to Contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GE Credit Proposal Contact Send</fullName>
        <actions>
            <name>GE_Credit_Proposal_Email_Template_Send</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>GE_Credit_Proposal_emailed_to_Opportunity_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>GE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends GE Credit Proposal to Contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Home Loan follow up call</fullName>
        <actions>
            <name>Email_to_remind_sales_guys_to_call_customer_to_let_them_know</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Residential_Status__c</field>
            <operation>equals</operation>
            <value>Mortgage</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Loan Documents Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_Referer__c</field>
            <operation>notEqual</operation>
            <value>Assured Home Loans</value>
        </criteriaItems>
        <description>Reminder to have the sales guys ring their clients with a home loan to expect a call from Rob</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Insurance FSG Docs - Boat</fullName>
        <actions>
            <name>Email_Insurance_FSGs_Boat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Insurance_FSGs_Emails_Boat</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Loan_Type2__c</field>
            <operation>contains</operation>
            <value>Boat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Tax Invoice Requested</value>
        </criteriaItems>
        <description>Insurance FSG&apos;s going to client when deal is at tax invoice requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Insurance FSG Docs - Car Commercial</fullName>
        <actions>
            <name>Email_Insurance_FSGs_Car_Commercial</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Insurance_FSGs_Emails_Car_Commercial</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Loan_Type2__c</field>
            <operation>contains</operation>
            <value>Car</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Tax Invoice Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Chattel Mortgage</value>
        </criteriaItems>
        <description>Insurance FSG&apos;s going to client when deal is at tax invoice requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Insurance FSG Docs - Car Consumer</fullName>
        <actions>
            <name>Email_Insurance_FSGs_Car_Consumer</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Insurance_FSGs_Emails_Car_Consumer</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Loan_Type2__c</field>
            <operation>contains</operation>
            <value>Car</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Tax Invoice Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Insurance FSG&apos;s going to client when deal is at tax invoice requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Insurance FSG Docs - Caravan</fullName>
        <actions>
            <name>Email_Insurance_FSGs_Caravan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Insurance_FSGs_Emails_Caravan</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Loan_Type2__c</field>
            <operation>equals</operation>
            <value>Caravan,Camper Trailer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Tax Invoice Requested</value>
        </criteriaItems>
        <description>Insurance FSG&apos;s going to client when deal is at tax invoice requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Insurance FSG Docs - Motorcycle</fullName>
        <actions>
            <name>Email_Insurance_FSGs_Motorcycle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Insurance_FSGs_Emails_Motorcycle</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Loan_Type2__c</field>
            <operation>contains</operation>
            <value>Bike</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Tax Invoice Requested</value>
        </criteriaItems>
        <description>Insurance FSG&apos;s going to client when deal is at tax invoice requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Insurance FSG Docs - Truck</fullName>
        <actions>
            <name>Email_Insurance_FSGs_Truck</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Insurance_FSGs_Emails_Truck</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Loan_Type2__c</field>
            <operation>contains</operation>
            <value>Truck</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Tax Invoice Requested</value>
        </criteriaItems>
        <description>Insurance FSG&apos;s going to client when deal is at tax invoice requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Insurance FSG Docs - Watercraft</fullName>
        <actions>
            <name>Email_Insurance_FSGs_Watercraft</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Insurance_FSGs_Emails_Watercraft</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Loan_Type2__c</field>
            <operation>contains</operation>
            <value>Jet Ski</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Tax Invoice Requested</value>
        </criteriaItems>
        <description>Insurance FSG&apos;s going to client when deal is at tax invoice requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lender Flag field update as one</fullName>
        <actions>
            <name>Lender_Flag_field_update_as_one</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>ANZ,BOQ,Capital,Esanda,Finance1,GE,Liberty,Macquarie,Money 3,NOW Finance,Pepper,Yamaha Finance,Centre 1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>RateSetter,Finance 1 Economy,Micro Money 3,U Me Loans</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>contains</operation>
            <value>George</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lender Flag field update as zero</fullName>
        <actions>
            <name>Lender_Flag_field_update_as_zero</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>Adelaide Bank,AFM – Australian First Mortgage,AMP,Auswide,Bank of Sydney,Bank SA,Bankwest,CBA,Citi Bank,Go Getta,Heritage Bank,Homeloans LTD,ING,La Trobe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>Morris Finance,Nationalcorp,Redzed,Resimac,Suncorp,Teachers Mutual Bank,The Rock,Westpac,Direct Money</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Liberty Credit Proposal Contact Send</fullName>
        <actions>
            <name>Liberty_Credit_Proposal_Email_Template_Send</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Liberty_Credit_Proposal_emailed_to_Opportunity_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>contains</operation>
            <value>Liberty</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends Liberty Credit Proposal to Contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Loan Documents Requested Notification</fullName>
        <actions>
            <name>Loan_Docs_Requested_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Loan Docs Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <description>This workflow rule is triggered when one of the Sales Team members changes the Stage value on an Opportunity to &apos;Loan Docs Requested&apos;. This will fire an email notification to Sales support team.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Macquarie Credit Proposal Contact Send</fullName>
        <actions>
            <name>Macquarie_Credit_Proposal_Email_Template_Send</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Macquarie_Credit_Proposal_emailed_to_Opportunity_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>Macquarie</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends Macquarie Credit Proposal to Contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Money 3 Credit Proposal Contact Send</fullName>
        <actions>
            <name>Money_3_Credit_Proposal_Email_Template_Send</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Money_3_Credit_Proposal_emailed_to_Opportunity_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>Money 3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends Money 3 Credit Proposal to Contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NOW Finance Credit Proposal  Contact Send</fullName>
        <actions>
            <name>NOW_Finance_Credit_Proposal_Email_Template_Send</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NOW_Finance_Credit_Proposal_emailed_to_Opportunity_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>NOW Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends NOW Finance proposal to contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Alex when Gary Deals Hit Ready For Submission</fullName>
        <actions>
            <name>Ready_for_submission_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Gary Rawlings</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify BDM Sup of BDM deal loan docs sent</fullName>
        <actions>
            <name>Email_BDM_Sup_on_BDL_Loan_Docs_Sent</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Loan Documents Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Deal_Suspended__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Bree Bock</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Sam Elliott</value>
        </criteriaItems>
        <description>When opportunity is set to &quot;loan docs sent&quot; notify owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify BDM Sup of deal suspension</fullName>
        <actions>
            <name>Notify_BDM_Sup_of_deal_suspended</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Stage_to_Loan_Docs_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Deal_Suspended__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Sam Elliott</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Bree Bock</value>
        </criteriaItems>
        <description>When opportunity has been suspended</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Chris%2FGraeme when Opp is Ready for Sub more than once</fullName>
        <actions>
            <name>Email_Chris_and_Graeme_about_Ready_for_Submission</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>PRIORVALUE( Increment_StageName__c ) = 1 &amp;&amp; Increment_StageName__c = 2 || PRIORVALUE( Increment_StageName__c ) = 2 &amp;&amp; Increment_StageName__c = 3 || PRIORVALUE( Increment_StageName__c ) = 3 &amp;&amp; Increment_StageName__c = 4 || PRIORVALUE( Increment_StageName__c ) = 4 &amp;&amp; Increment_StageName__c = 5 || PRIORVALUE( Increment_StageName__c ) = 5 &amp;&amp; Increment_StageName__c = 6</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Owner of deal suspension</fullName>
        <actions>
            <name>Email_Owner_When_Deal_is_Suspended</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Stage_to_Loan_Docs_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Deal_Suspended__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>notEqual</operation>
            <value>Sam Elliott</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>notEqual</operation>
            <value>Bree Bock</value>
        </criteriaItems>
        <description>When opportunity has been suspended</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Owner of loan docs sent</fullName>
        <actions>
            <name>Notify_Owner_Of_Loan_Docs_Sent</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Loan Documents Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Deal_Suspended__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>notEqual</operation>
            <value>Bree Bock</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>notEqual</operation>
            <value>Sam Elliott</value>
        </criteriaItems>
        <description>When opportunity is set to &quot;loan docs sent&quot; notify owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Support team of loan docs recieved</fullName>
        <actions>
            <name>Email_Support_When_Stage_Loan_Docs_Recieved</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Loan Docs Recieved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <description>When opportunity is set to &quot;loan docs recieved&quot; notify Support team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Support team of submission ready</fullName>
        <actions>
            <name>Notify_submission_processors_of_opportunity_ready_for_submission</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <description>When opportunity is set to &quot;ready for submission&quot; notify Support team.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp Living Expenses calculation for customer</fullName>
        <actions>
            <name>Opp_Living_Expenses_calculation_for_Cust</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Living_Expenses2_Customer__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <description>Living Expenses Customer is updated from Living Expenses2 Customer field.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Change to Property - Mortgage Team</fullName>
        <actions>
            <name>Update_Record_Type_to_Property</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Owner_Profile_Name__c == $Label.Profile_Name_Mortgage</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - PLS Closed Lost</fullName>
        <actions>
            <name>Opportunity_Closed_Lost_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Individual</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Set to App Forms Taken From Callback</fullName>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Opp_Workflows__c, ISPICKVAL(StageName,&apos;Callback&apos;), NOT(ISNULL(Set_Call_Back_Date_Time__c)), NOT(ISBLANK(Set_Call_Back_Date_Time__c)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Opportunity_App_Forms_Taken_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Set_Call_Back_Date_Time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Opportunity - Update Bizible Opp Amount</fullName>
        <actions>
            <name>Bizible_Opp_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Total_Commision__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PHL Notification - New Opp From Closed Lost Mortgage</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.For_Home_Loan_Review__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.From_Closed_Lost_Mortgage__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PHL Notification - New Opp From Qualified %2F App Forms Taken</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.For_Home_Loan_Review__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.From_Qualified_App_Forms_Taken_Opps__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Pepper Finance Credit Proposal Contact Send</fullName>
        <actions>
            <name>Pepper_Finance_Credit_Proposal_Email_Template_Send</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Pepper_Finance_Credit_Proposal_emailed_to_Opportunity_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>Pepper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends Pepper Finance Credit Proposal to Contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ready for Submission upd Increment StageName by one</fullName>
        <actions>
            <name>Ready_for_Submission_upd_Increment_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  ISCHANGED(StageName),  NOT(ISPICKVAL(PRIORVALUE(StageName), &apos;Ready for Submission&apos;)),  ISPICKVAL(StageName, &apos;Ready for Submission&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send SMS Privacy for Opportunity</fullName>
        <actions>
            <name>Sending_SMS_Privacy_to_Opportunity</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Mobile__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.SMS_Privacy__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sent for settlement Notification</fullName>
        <actions>
            <name>Sent_for_Settlement_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Sent for Settlement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <description>This workflow rule is triggered when one of the Sales Support Team members changes the Stage value on an Opportunity to &apos;Sent for settlement&apos;. This will fire an email notification to the Opportunity Owner.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>St%2EGeorge Credit Proposal Contact Send %28consumer%29</fullName>
        <actions>
            <name>St_George_Credit_Proposal_Email_Template_Send</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>St_George_Credit_Proposal_emailed_to_Opportunity_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>St.George</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends St.George Credit Proposal to Contact (consumer deals only)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status field mapping exception from Lead conversion</fullName>
        <actions>
            <name>Status_set_to_null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>To bypass lead status to opportunity status mapping when the current user is a member of PLS Team(Sales Team, Sales Team WA and Sales Support).</description>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Opp_Workflows__c , OR( Owner.Profile.Name == &apos;Sales Team&apos;, Owner.Profile.Name == &apos;Sales Team WA&apos;, Owner.Profile.Name == &apos;Sales Support&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Submitted to Lender Notification</fullName>
        <actions>
            <name>Submitted_to_Lender_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Submitted to Lender</value>
        </criteriaItems>
        <description>This workflow rule is triggered when one of the Sales Support Team members changes the Stage value on an Opportunity to &apos;Submitted to Lender&apos;. This will fire an email notification to the Opportunity Owner.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Upd Opp as Car when Acc is Car</fullName>
        <actions>
            <name>Upd_Opp_as_Car_when_Acc_is_Car</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Record_Type__c</field>
            <operation>equals</operation>
            <value>Car</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Upd Opp as Individual when Acc is Individual</fullName>
        <actions>
            <name>Upd_Opp_as_Individual_when_Acc_is_Indivi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Record_Type__c</field>
            <operation>equals</operation>
            <value>Individual</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Upd Opp as Property when Acc is Property</fullName>
        <actions>
            <name>Upd_Opp_as_Propety_when_Acc_is_Property</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Record_Type__c</field>
            <operation>equals</operation>
            <value>Property</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Email from Opportunity</fullName>
        <actions>
            <name>Update_Account_Email_from_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Opportunity Email is changed then email is copied to account</description>
        <formula>ISCHANGED(Email__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Mobile from Opportunity</fullName>
        <actions>
            <name>Update_Account_Mobile_from_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Opportunity Mobile is changed then mobile is copied to account</description>
        <formula>ISCHANGED( Mobile__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Approval Date when Application is Approved</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Updates Approval Date to Today when Tiggered from Stage changing to Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Application_Date_on_Approved</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Employment Details on Account</fullName>
        <actions>
            <name>Update_Employment_DetailsPreviousHistory</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Employment_Details_CurrentStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Employment_Details_Gap</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Employment_Details_LengthPrevious</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Employment_Details_Length_Current</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Whenever a new opportunity record is created for an Account, it will update the Account with the latest employment details as recorded on the most recent Opportunity record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Latest Comment %28Date%2FTime%29</fullName>
        <actions>
            <name>Update_Latest_Comment_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Latest_Comment__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Latest Comment w%2F Timestamp</fullName>
        <actions>
            <name>Update_Latest_Comment_w_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update Latest Comment including its timestamp</description>
        <formula>ISCHANGED(Latest_Comment_Date_Time__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Name to Date and Loan Ref</fullName>
        <actions>
            <name>Opportunity_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Settled,Declined By Lender,No Longer Proceeding</value>
        </criteriaItems>
        <description>Updates Opportunity when Settled, Declined By Lender, No Longer Proceeding to Contact Name and Settled Date (Closed Date)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Priority BDM Deals</fullName>
        <actions>
            <name>Update_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Referral_Company_Text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update SMS Mobile number with country code</fullName>
        <actions>
            <name>Remove_spaces_mobile_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_spaces_sms_mobile_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_Mobile_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Mobile__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Support Comment w%2F Timestamp</fullName>
        <actions>
            <name>Update_Sales_Support_Comment_w_Timestam</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(StageName)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Settled Date to today if stage has been changed to Settled%2C Declined By Lender%2C No Longer Proceeding</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Settled,Declined By Lender,No Longer Proceeding</value>
        </criteriaItems>
        <description>Updates the settled date (Standard Field - Closed date ) to today if Stage is changed to Settled, Declined By Lender, No Longer Proceeding</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Opportunity_Name_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Settled_Date_updated_with_today_s_date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Vehicle Source UCA</fullName>
        <actions>
            <name>Send_a_mail_to_Luke_when_vehicle_source_is_UCA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Vehicle_Source__c</field>
            <operation>equals</operation>
            <value>Used Cars Adelaide</value>
        </criteriaItems>
        <description>Notify Luke when vehicle source is Used Cars Adelaide</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>When Opp Approved Trigger</fullName>
        <actions>
            <name>Email_Luke_Dz_when_SA_Deal_approved</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3) AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Current_State__c</field>
            <operation>equals</operation>
            <value>South Australia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Current_State__c</field>
            <operation>equals</operation>
            <value>Victoria</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Referral_Company_Text__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.New_or_Used__c</field>
            <operation>equals</operation>
            <value>Used</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Type2__c</field>
            <operation>equals</operation>
            <value>Car Loan</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Yamaha Finance Credit Proposal Contact Send</fullName>
        <actions>
            <name>Yamaha_Credit_Proposal_Email_Template_Send</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Yamaha_Credit_Proposal_emailed_to_Opportunity_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Ready for Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lender__c</field>
            <operation>equals</operation>
            <value>Yamaha Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product__c</field>
            <operation>equals</operation>
            <value>Consumer Loan</value>
        </criteriaItems>
        <description>Sends YamahaFinance Credit Proposal to Contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>blank opp mob from account</fullName>
        <actions>
            <name>update_mobile_from_account_to_opportunit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISBLANK( Mobile__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>update amount for pardot</fullName>
        <actions>
            <name>update_amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Total_Commision__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Property</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>updated acc mob to oppt</fullName>
        <actions>
            <name>update_mobile_from_account_to_opportunit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR (  ISBLANK( Mobile__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>ANZ_Credit_Proposal</fullName>
        <assignedToType>owner</assignedToType>
        <description>ANZ Credit Proposal has been automatically emailed to Opportunity Contact</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>ANZ Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
    <tasks>
        <fullName>Capital_Credit_Proposal_emailed_to_Opportunity_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <description>Capital Credit Proposal has been automatically emailed to Opportunity Contact</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Capital Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
    <tasks>
        <fullName>Centre_1_Credit_Proposal_emailed_to_Opportunity_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <description>Centre 1 Credit Proposal has been automatically emailed to Opportunity Contact</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Centre 1 Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
    <tasks>
        <fullName>Finance_1_Credit_Proposal_emailed_to_Opportunity_Contact</fullName>
        <assignedToType>creator</assignedToType>
        <description>Finance 1 Credit Proposal has been automatically emailed to Opportunity Contact</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Finance 1 Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
    <tasks>
        <fullName>GE_Credit_Proposal_emailed_to_Opportunity_Contact</fullName>
        <assignedToType>creator</assignedToType>
        <description>GE Credit Proposal has been automatically emailed to Opportunity Contact</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>GE Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
    <tasks>
        <fullName>Insurance_FSGs_Emailed</fullName>
        <assignedToType>owner</assignedToType>
        <description>Insurance FSG&apos;s have been sent to client.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Insurance FSGs Emailed</subject>
    </tasks>
    <tasks>
        <fullName>Insurance_FSGs_Emails_Boat</fullName>
        <assignedToType>owner</assignedToType>
        <description>Insurance Email - Boat has been sent to client</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Insurance FSGs Emails - Boat</subject>
    </tasks>
    <tasks>
        <fullName>Insurance_FSGs_Emails_Car_Commercial</fullName>
        <assignedToType>owner</assignedToType>
        <description>Insurance Email - Car Commercial has been sent to client</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Insurance FSGs Emails - Car Commercial</subject>
    </tasks>
    <tasks>
        <fullName>Insurance_FSGs_Emails_Car_Consumer</fullName>
        <assignedToType>owner</assignedToType>
        <description>Insurance Email - Car Consumer has been sent to client</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Insurance FSGs Emails - Car Consumer</subject>
    </tasks>
    <tasks>
        <fullName>Insurance_FSGs_Emails_Caravan</fullName>
        <assignedToType>owner</assignedToType>
        <description>Insurance Email - Caravan has been sent to client</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Insurance FSGs Emails - Caravan</subject>
    </tasks>
    <tasks>
        <fullName>Insurance_FSGs_Emails_Motorcycle</fullName>
        <assignedToType>owner</assignedToType>
        <description>Insurance Email - Motorcycle has been sent to client</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Insurance FSGs Emails - Motorcycle</subject>
    </tasks>
    <tasks>
        <fullName>Insurance_FSGs_Emails_Truck</fullName>
        <assignedToType>owner</assignedToType>
        <description>Insurance Email - Truck has been sent to client</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Insurance FSGs Emails - Truck</subject>
    </tasks>
    <tasks>
        <fullName>Insurance_FSGs_Emails_Watercraft</fullName>
        <assignedToType>owner</assignedToType>
        <description>Insurance Email - Watercraft has been sent to client</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Approval_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Insurance FSGs Emails - Watercraft</subject>
    </tasks>
    <tasks>
        <fullName>Liberty_Credit_Proposal_emailed_to_Opportunity_Contact</fullName>
        <assignedToType>creator</assignedToType>
        <description>Liberty Credit Proposal has been automatically emailed to Opportunity Contact</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Liberty Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
    <tasks>
        <fullName>Macquarie_Credit_Proposal_emailed_to_Opportunity_Contact</fullName>
        <assignedToType>creator</assignedToType>
        <description>Macquarie Credit Proposal has been automatically emailed to Opportunity Contact</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Macquarie Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
    <tasks>
        <fullName>Money_3_Credit_Proposal_emailed_to_Opportunity_Contact</fullName>
        <assignedToType>creator</assignedToType>
        <description>Money 3 Credit Proposal has been automatically emailed to Opportunity Contact</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Money 3 Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
    <tasks>
        <fullName>NOW_Finance_Credit_Proposal_emailed_to_Opportunity_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>NOW Finance Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
    <tasks>
        <fullName>Pepper_Finance_Credit_Proposal_emailed_to_Opportunity_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <description>Pepper Finance Credit Proposal has been automatically emailed to Opportunity Contact</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Pepper Finance Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
    <tasks>
        <fullName>SMS_Notification</fullName>
        <assignedToType>owner</assignedToType>
        <description>Opportunity - b926</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
    <tasks>
        <fullName>SMS_Notification_to_Mobile</fullName>
        <assignedToType>owner</assignedToType>
        <description>Opportunity - 33a0</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>SMS Notification</subject>
    </tasks>
    <tasks>
        <fullName>Sending_SMS_Privacy_to_Opportunity</fullName>
        <assignedToType>owner</assignedToType>
        <description>{&quot;template&quot;:&quot;a096F00001rdQdk&quot;}</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Sending SMS Privacy to Opportunity</subject>
    </tasks>
    <tasks>
        <fullName>Settle_Car_Loan_Morgage_Lead</fullName>
        <assignedTo>elliot@positivehomeloans.com.au</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Settle Car Loan Mortgage Lead</subject>
    </tasks>
    <tasks>
        <fullName>St_George_Credit_Proposal_emailed_to_Opportunity_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <description>St.George Credit Proposal has been automatically emailed to Opportunity Contact</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>St.George Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
    <tasks>
        <fullName>Yamaha_Credit_Proposal_emailed_to_Opportunity_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <description>Yamaha Credit Proposal has been automatically emailed to Opportunity Contact</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Yamaha Credit Proposal emailed to Opportunity Contact</subject>
    </tasks>
</Workflow>

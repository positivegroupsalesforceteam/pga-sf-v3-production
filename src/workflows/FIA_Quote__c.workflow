<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FIA_Quote_Notify_Case_Owner_with_FIA_Quote_Response</fullName>
        <description>FIA Quote - Notify Case Owner with FIA Quote Response</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/FIA_Quote_Notify_Case_Owner_with_FIA_Quote_Response</template>
    </alerts>
    <alerts>
        <fullName>FIA_Quote_Send_to_Client</fullName>
        <description>FIA Quote - Send to Client</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/FIA_Quote_Send_to_Client</template>
    </alerts>
    <alerts>
        <fullName>Notify_FE_Team_for_Sent_FIA_Quote</fullName>
        <ccEmails>mary.tapar@positivelendingsolutions.com.au</ccEmails>
        <ccEmails>vianca.aquino@positivegroup.com.au</ccEmails>
        <description>Notify FE Team for Sent FIA Quote</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/FIA_Quote_to_Send</template>
    </alerts>
    <fieldUpdates>
        <fullName>FIA_Quote_Set_Sent_to_Client_to_NOW</fullName>
        <field>Date_Time_Sent_to_Client__c</field>
        <formula>NOW()</formula>
        <name>FIA Quote - Set Sent to Client to NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FIA_Quote_Set_Stage_to_With_Client</fullName>
        <field>Stage__c</field>
        <literalValue>With Client</literalValue>
        <name>FIA Quote - Set Stage to With Client</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FIA_Quote_Set_Status_to_Pending</fullName>
        <field>Status__c</field>
        <literalValue>Pending</literalValue>
        <name>FIA Quote - Set Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_FIA_Quote_to_FE</fullName>
        <apiVersion>47.0</apiVersion>
        <description>Send FIA Quote data to Frontend via Jitterbit</description>
        <endpointUrl>https://Positive187318.jitterbit.eu/CloudTest/1.0/financeInsuranceApp</endpointUrl>
        <fields>Id</fields>
        <fields>Partition__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>Send FIA Quote to FE</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>FIA Quote - Receive FIA Quote Updates From Client</fullName>
        <actions>
            <name>FIA_Quote_Notify_Case_Owner_with_FIA_Quote_Response</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Workflow_Rule_Settings1__c.Enable_FIA_Quote_Workflows__c, ISCHANGED(Date_Time_Response_Received__c), NOT(ISBLANK(Date_Time_Response_Received__c)), ISPICKVAL(Stage__c, &apos;With PGA&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FIA Quote - Send FIA Quote to Client</fullName>
        <actions>
            <name>FIA_Quote_Send_to_Client</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FIA_Quote_Set_Sent_to_Client_to_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FIA_Quote_Set_Stage_to_With_Client</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FIA_Quote_Set_Status_to_Pending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Workflow_Rule_Settings1__c.Enable_FIA_Quote_Workflows__c, ISCHANGED(FE_Form_URL__c), NOT(ISBLANK(FE_Form_URL__c)), TEXT(Preferred_Notification__c) == &apos;Email&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FIA Quote - Send FIA Quote to FE</fullName>
        <actions>
            <name>Notify_FE_Team_for_Sent_FIA_Quote</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_FIA_Quote_to_FE</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Triggered only when clicking Send button on FIA Quote record</description>
        <formula>AND( $Setup.Workflow_Rule_Settings1__c.Enable_FIA_Quote_Workflows__c, ISCHANGED(Push_Count__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

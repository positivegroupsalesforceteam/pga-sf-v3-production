<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Manager_of_Personal_Loan_closed_case</fullName>
        <description>Alert Manager of Personal Loan closed case</description>
        <protected>false</protected>
        <recipients>
            <recipient>daniel.adams@positivegroup.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NM_PLS_Templates/CLOSED_CASE_Personal_Loan</template>
    </alerts>
    <alerts>
        <fullName>Approved_Manager_Email</fullName>
        <description>Submitted - Manager Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>tim@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>NM_PLS_Templates/Submitted_Manager_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Asset_Not_Ready_Reopened_Notification</fullName>
        <description>Case - Asset Not Ready - Reopened Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Asset_Not_Ready_ReOpened_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Closed_Nodifi</fullName>
        <description>Case - Closed Nodifi</description>
        <protected>false</protected>
        <recipients>
            <recipient>liam.sutcliffe@nodifi.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Closed_Nodifi_Send_Email_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Connective_Broker_Notification</fullName>
        <description>Case - Connective Broker Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Connective_Broker_lookup__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Connective_Broker_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Deactivate_Client_access_to_Full_Application</fullName>
        <description>Case - Deactivate Client access to Full Application</description>
        <protected>false</protected>
        <recipients>
            <recipient>api@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Deactivate_Client_access_to_Full_Application</template>
    </alerts>
    <alerts>
        <fullName>Case_Mortgage_Settlement</fullName>
        <description>Case - Mortgage Settlement</description>
        <protected>false</protected>
        <recipients>
            <recipient>PLS_Manager_Group</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>NM_PLS_Templates/Case_Mortgage_Settlement_Template</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Assigned_to_Settlements_FULL_APP</fullName>
        <description>Case - NODIFI - Assigned to Settlements - FULL APP</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Introducer1_Support_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Assigned_to_Settlements_FULL_APP</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Assigned_to_Settlements_LEAD_to_CLIENT</fullName>
        <description>Case - NODIFI - Assigned to Settlements - LEAD to CLIENT</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact_Email_MC__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Assigned_to_Settlements_LEAD_to_CLIENT</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Assigned_to_Settlements_LEAD_to_INTRODUCER</fullName>
        <description>Case - NODIFI - Assigned to Settlements - LEAD to INTRODUCER</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Introducer1_Support_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Assigned_to_Settlements_LEAD_to_INTRODUCER</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Docs_Back_Not_Checked_FULL_APP</fullName>
        <description>Case - NODIFI - Docs Back Not Checked - FULL APP</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Introducer1_Support_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Docs_Back_Not_Checked_FULL_APP</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Docs_Back_Not_Checked_LEAD_to_CLIENT</fullName>
        <description>Case - NODIFI - Docs Back Not Checked - LEAD to CLIENT</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact_Email_MC__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Docs_Back_Not_Checked_LEAD_to_CLIENT</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Part_Docs_Received_FULL_APP</fullName>
        <description>Case - NODIFI - Part Docs Received - FULL APP</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Introducer1_Support_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Part_Docs_Received_FULL_APP</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Part_Docs_Received_LEAD_to_CLIENT</fullName>
        <description>Case - NODIFI - Part Docs Received - LEAD to CLIENT</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact_Email_MC__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Part_Docs_Received_LEAD_to_CLIENT</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Part_Docs_Received_LEAD_to_INTRODUCER</fullName>
        <description>Case - NODIFI - Part Docs Received - LEAD to INTRODUCER</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Introducer1_Support_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Part_Docs_Received_LEAD_to_INTRODUCER</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Submitted_to_Lender_FULL_APP</fullName>
        <description>Case - NODIFI - Submitted to Lender - FULL APP</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Introducer1_Support_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Submitted_to_Lender_FULL_APP</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Submitted_to_Lender_LEAD_to_CLIENT</fullName>
        <description>Case - NODIFI - Submitted to Lender - LEAD to CLIENT</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact_Email_MC__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Submitted_to_Lender_LEAD_to_CLIENT</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Submitted_to_Lender_LEAD_to_INTRODUCER</fullName>
        <description>Case - NODIFI - Submitted to Lender - LEAD to INTRODUCER</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Introducer1_Support_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Submitted_to_Lender_LEAD_to_INTRODUCER</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Withdrawn_FULL_APP</fullName>
        <description>Case - NODIFI - Withdrawn - FULL APP</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Introducer1_Support_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Withdrawn_FULL_APP</template>
    </alerts>
    <alerts>
        <fullName>Case_NODIFI_Withdrawn_LEAD_to_INTRODUCER</fullName>
        <description>Case - NODIFI - Withdrawn - LEAD to INTRODUCER</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Introducer1_Support_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NODIFI_Withdrawn_LEAD_to_INTRODUCER</template>
    </alerts>
    <alerts>
        <fullName>Case_New_Commercial_Scenario_Notification</fullName>
        <description>Case - New Commercial Scenario Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ryan.mccallum@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stephen.hahn@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Case_New_Commercial_Scenario_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_New_Complex_Scenario_Notification</fullName>
        <ccEmails>caf@positivelendingsolutions.com.au</ccEmails>
        <description>Case - New Complex Scenario Notification</description>
        <protected>false</protected>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Case_New_Complex_Scenario_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_New_NOD_Case_1_15_19</fullName>
        <description>Case - New NOD Case (1/15/19)</description>
        <protected>false</protected>
        <recipients>
            <recipient>jesfer.baculod@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rexie.david@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rl-pos@rl.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Email_New_NOD_Cases</template>
    </alerts>
    <alerts>
        <fullName>Case_New_PWM_Notification</fullName>
        <description>Case - New PWM Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>tim@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tom@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>NM_PLS_Templates/Case_New_PWM_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_New_Submission_Request_Email_Alert</fullName>
        <ccEmails>support@positivelendingsolutions.com.au</ccEmails>
        <description>Case - New Submission Request Email Alert</description>
        <protected>false</protected>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_New_Submission_Request_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Case_Notify_Child_Case_on_Won_Parent_Case</fullName>
        <ccEmails>jesfer.baculod@positivelendingsolutions.com.au,</ccEmails>
        <ccEmails>chris.sims@positivegroup.com.au,</ccEmails>
        <ccEmails>rexie.david@positivelendingsolutions.com.au,</ccEmails>
        <ccEmails>tim.wells@positivelendingsolutions.com.au</ccEmails>
        <description>Case - Notify Child Case on Won Parent Case</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Notify_Child_Case_Owner_of_Won_Parent_Case</template>
    </alerts>
    <alerts>
        <fullName>Case_Notify_Introducer_on_New_Case_Comments</fullName>
        <description>Case - Notify Introducer on New Case Comments</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Introducer1_Support_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>credit@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NOD_Public_Case_Comment</template>
    </alerts>
    <alerts>
        <fullName>Case_Notify_Karlon_Owner_on_Scheduled_Call</fullName>
        <description>Case - Notify Karlon Owner on Scheduled Call</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>rexie.david@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Notify_Karlon_Owner_on_Scheduled_Call</template>
    </alerts>
    <alerts>
        <fullName>Case_Notify_Owner_New_Case_Comment</fullName>
        <description>Case - Notify Owner New Case Comment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>chris@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_New_Case_Comment_Not_Owner</template>
    </alerts>
    <alerts>
        <fullName>Case_Notify_Settlement_Officer_New_Case_Comment</fullName>
        <description>Case - Notify Settlement Officer New Case Comment</description>
        <protected>false</protected>
        <recipients>
            <field>Settlement_Officer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>chris@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_New_Case_Comment_Not_Owner</template>
    </alerts>
    <alerts>
        <fullName>Case_Notify_Settlements_officer_and_Manager_of_lost_deal</fullName>
        <description>Case - Notify Settlements officer and Manager of lost deal</description>
        <protected>false</protected>
        <recipients>
            <recipient>tim@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Settlement_Officer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>NM_PLS_Templates/Case</template>
    </alerts>
    <alerts>
        <fullName>Case_Notify_Team_on_Introducer_Comments</fullName>
        <description>Case - Notify Team on Introducer Comments</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_New_Introducer_Case_Comment</template>
    </alerts>
    <alerts>
        <fullName>Case_Notify_Team_on_Introducer_Comments_Settlement_Officer</fullName>
        <description>Case - Notify Team on Introducer Comments (Settlement Officer)</description>
        <protected>false</protected>
        <recipients>
            <field>Settlement_Officer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_New_Introducer_Case_Comment</template>
    </alerts>
    <alerts>
        <fullName>Case_Owner_Assigned_Changed_To_Attempted</fullName>
        <description>Case - Owner Assigned Changed To Attempted</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Case_on_Owner_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Case_PHL_PLS_CASE_is_Submitted_to_lender</fullName>
        <description>Case - PHL - PLS CASE is Submitted to lender</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>chris.murray@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>elliot@positivehomeloans.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>justin@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nathan@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PHL_Opp_FUP_General/Case_PHL_PLS_case_submitted_to_lender</template>
    </alerts>
    <alerts>
        <fullName>Case_Part_Doc_s_Required</fullName>
        <description>Case - Part Doc&apos;s Required</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>NM_PLS_Templates/Case_Part_Doc_s_Required_Template</template>
    </alerts>
    <alerts>
        <fullName>Case_Ready_For_Submission_Notification</fullName>
        <ccEmails>caf@positivelendingsolutions.com.au</ccEmails>
        <description>Case - Ready For Submission Notification</description>
        <protected>false</protected>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Ready_For_Submission_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Scenario_Lost_Notification</fullName>
        <description>Case - Scenario Lost Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Case_Scenario_Lost_and_Went_Cold_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Send_Notification_KAR_Sales_Opportunity_Case</fullName>
        <ccEmails>rexie.david@positivelendingsolutions.com.au</ccEmails>
        <ccEmails>jesfer.baculod@positivelendingsolutions.com.au</ccEmails>
        <description>Case - Send Notification - KAR Sales Opportunity Case</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_KAR_Sales_Opp_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Send_Notification_Non_Connective</fullName>
        <description>Case - Send Notification - Non Connective</description>
        <protected>false</protected>
        <recipients>
            <field>Referral_Partner_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>liam.sutcliffe@nodifi.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>credit@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_New_Case_From_Non_Connective</template>
    </alerts>
    <alerts>
        <fullName>Case_Send_Notification_Sourcing_Case</fullName>
        <description>Case - Send Notification - Sourcing Case</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Sourcing_Case_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Send_SLA_Scenario_Actioned_Warning_Notification</fullName>
        <description>Case - Send SLA Scenario Actioned Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Case_SLA_Scenario_Actioned_Warning</template>
    </alerts>
    <alerts>
        <fullName>Case_Send_SLA_Scenario_With_Introducer_Warning_Notification</fullName>
        <description>Case - Send SLA Scenario With Introducer Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Case_SLA_With_Introducer_Warning</template>
    </alerts>
    <alerts>
        <fullName>Case_Send_SLA_Scenario_With_Lender_Warning_Notification</fullName>
        <description>Case - Send SLA Scenario With Lender Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Case_SLA_With_Lender_Warning</template>
    </alerts>
    <alerts>
        <fullName>Case_Sent_for_Settlement</fullName>
        <description>Case - Sent for Settlement</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>NM_PLS_Templates/Case_Sent_for_Settlement_Template</template>
    </alerts>
    <alerts>
        <fullName>Case_Submitted_To_Lender_Notification</fullName>
        <description>Case - Submitted To Lender Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Submitted_To_Lender_Notification</template>
    </alerts>
    <alerts>
        <fullName>Child_Case_Owner_Parent_case_has_been_closed</fullName>
        <ccEmails>tim.wells@positivelendingsolutions.com.au</ccEmails>
        <description>Child Case Owner - Parent case has been closed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NM_PLS_Templates/Child_Case_PHL_Owner</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Assignment</fullName>
        <description>New Case Assignment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/New_Case_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_sent_to_Jordan_on_Case_creation</fullName>
        <description>Notification sent to Jordan on Case creation</description>
        <protected>false</protected>
        <recipients>
            <recipient>jordan@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Unfiled_Public_Email_Templates_2/Support_Email_for_new_Case_created</template>
    </alerts>
    <alerts>
        <fullName>Send_SLA_Application_Review_Warning_Notification</fullName>
        <description>Send SLA Application Review Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Case_SLA_Application_Review_Warning</template>
    </alerts>
    <alerts>
        <fullName>Send_SLA_Docs_Out_Warning_Notification</fullName>
        <description>Send SLA Docs Out Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Settlement_SLA_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Case_SLA_Docs_Out_Warning</template>
    </alerts>
    <alerts>
        <fullName>Send_SLA_Initial_Review_Warning_Notification</fullName>
        <ccEmails>jesfer.baculod@positivelendingsolutions.com.au</ccEmails>
        <description>Send SLA Initial Review Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Reviewer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Case_SLA_Initial_Review_Warning</template>
    </alerts>
    <alerts>
        <fullName>Send_SLA_Sent_for_Settlement_Warning_Notification</fullName>
        <description>Send SLA Sent for Settlement Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Settlement_SLA_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Case_SLA_Sent_for_Settlement_Warning</template>
    </alerts>
    <alerts>
        <fullName>Send_SLA_Submitted_to_Lender_Warning_Notification</fullName>
        <description>Send SLA Submitted to Lender Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>danika@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Submitter__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Case_SLA_Submitted_to_Lender_Warning</template>
    </alerts>
    <alerts>
        <fullName>Survey_Monkey_NOD_NPS_Lost</fullName>
        <description>Survey Monkey - NOD - NPS - Lost</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Introducer1_Support_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>credit@nodifi.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NOD_Survey_NPS_LOST</template>
    </alerts>
    <alerts>
        <fullName>Survey_Monkey_PLS_NPS_Lost</fullName>
        <description>Survey Monkey - PLS - NPS - Lost</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact_Email_MC__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Survey_Monkey_NPS_LOST</template>
    </alerts>
    <alerts>
        <fullName>Survey_Monkey_PLS_NPS_Settled</fullName>
        <description>Survey Monkey - PLS - NPS - Settled</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact_Email_MC__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Survey_Monkey_NPS_SETTLED</template>
    </alerts>
    <alerts>
        <fullName>Survey_Monkey_PLS_NPS_Submitted</fullName>
        <description>Survey Monkey - PLS - NPS - Submitted</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact_Email_MC__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Survey_Monkey_NPS_SUBMITTED</template>
    </alerts>
    <alerts>
        <fullName>Test_Email1</fullName>
        <description>Test Email1</description>
        <protected>false</protected>
        <recipients>
            <recipient>jesfer.baculod@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rexie.david@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tim@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Test_Email1</template>
    </alerts>
    <alerts>
        <fullName>Test_Email2</fullName>
        <description>Test Email2</description>
        <protected>false</protected>
        <recipients>
            <recipient>rexie.david@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tim@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Test_Email2</template>
    </alerts>
    <alerts>
        <fullName>Test_Email3</fullName>
        <description>Test Email3</description>
        <protected>false</protected>
        <recipients>
            <recipient>rexie.david@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tim@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Test_Email3</template>
    </alerts>
    <alerts>
        <fullName>Test_Trigger</fullName>
        <description>Test Trigger</description>
        <protected>false</protected>
        <recipients>
            <recipient>jesfer.baculod@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Test_Trigger_Email_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Attempt_Set_to_Blank</fullName>
        <field>Attempted_Contact_Status__c</field>
        <name>Case - Attempt - Set to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Close_Reason_No_Response</fullName>
        <field>Closed_Reason__c</field>
        <literalValue>No Response from Client</literalValue>
        <name>Case - Close Reason - No Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Copy_Latest_Comment</fullName>
        <description>Copy Latest Comment from saved Latest Comment (copy)</description>
        <field>Latest_Comment__c</field>
        <formula>Latest_Comment1__c</formula>
        <name>Case - Copy Latest Comment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_On_Hold</fullName>
        <description>Update Case - On Hold to &quot;Callback&quot;</description>
        <field>On_Hold__c</field>
        <literalValue>Callback</literalValue>
        <name>Case - On Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PB_Sched_Action_Remove_Callback</fullName>
        <field>PB_Sched_Action__c</field>
        <literalValue>Remove Callback - Set Status/Stage</literalValue>
        <name>Case - PB Sched Action - Remove Callback</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PWM_Close_Status</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Case - PWM Close Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PWM_Closed_Reason_No_Response</fullName>
        <field>Closed_Reason__c</field>
        <literalValue>No Response from Client</literalValue>
        <name>Case - PWM Closed Reason No Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_PWM_Stage_to_Lost</fullName>
        <field>Stage__c</field>
        <literalValue>Lost</literalValue>
        <name>Case - PWM Stage to Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Populate_Assigned_Team_At_Closed</fullName>
        <field>Assigned_Team_At_Closed__c</field>
        <formula>IF((BEGINS(OwnerId, &quot;005&quot;)), TEXT(Owner:User.Team__c), IF((BEGINS(OwnerId, &quot;00G&quot;)), Owner:Queue.QueueName, &apos; &apos;))</formula>
        <name>Case - Populate Assigned Team - At Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Populate_Sent_To_NVM_Date_Time</fullName>
        <field>Sent_To_NVM_Date_Time__c</field>
        <formula>now()</formula>
        <name>Case - Populate Sent To NVM Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Remove_Submitter</fullName>
        <field>Submitter__c</field>
        <name>Case - Remove Submitter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Routable</fullName>
        <description>Case routable through NVM</description>
        <field>NVMContactWorld__NVMRoutable__c</field>
        <literalValue>1</literalValue>
        <name>Case Routable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Prior_Stage</fullName>
        <field>Stage_Prior__c</field>
        <formula>IF( 
AND(
NOT(ISPICKVAL(PRIORVALUE(Stage__c),&apos;Lost&apos;)),
NOT(ISPICKVAL(PRIORVALUE(Stage__c),&apos;Asset Not Ready&apos;)),
NOT(ISPICKVAL(PRIORVALUE(Stage__c),&apos;Deal With Introducer&apos;))
),
TEXT(PRIORVALUE(Stage__c)), Stage_Prior__c)</formula>
        <name>Case - Set Prior Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Prior_Stage_NM</fullName>
        <description>IF( 
AND( 
NOT(ISPICKVAL(PRIORVALUE(Stage__c),&apos;Lost&apos;)), 
NOT(ISPICKVAL(PRIORVALUE(Stage__c),&apos;Asset Not Ready&apos;)), 
NOT(ISPICKVAL(PRIORVALUE(Stage__c),&apos;Deal With Introducer&apos;)) 
), 
TEXT(PRIORVALUE(Stage__c)), Stage_Prior__c)</description>
        <field>Stage_Prior__c</field>
        <formula>IF( 
AND( 
NOT(ISPICKVAL(PRIORVALUE(Stage__c),&apos;Lost&apos;)), 
NOT(ISPICKVAL(PRIORVALUE(Stage__c),&apos;Asset Not Ready&apos;)), 
NOT(ISPICKVAL(PRIORVALUE(Stage__c),&apos;Deal With Introducer&apos;)) 
), 
TEXT(PRIORVALUE(Stage__c)), Stage_Prior__c)</formula>
        <name>Case - Set Prior Stage (NM)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Prior_Status</fullName>
        <field>Prior_Status__c</field>
        <formula>IF(
AND(
NOT(ISPICKVAL(PRIORVALUE(Status),&apos;Closed&apos;)), 
NOT(ISPICKVAL(PRIORVALUE(Status),&apos;Lost&apos;)), 
NOT(ISPICKVAL(PRIORVALUE(Status),&apos;On Hold&apos;))
),
TEXT(PRIORVALUE(Status)),Prior_Status__c)</formula>
        <name>Case - Set Prior Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Prior_Status_NM</fullName>
        <description>NM Version.</description>
        <field>Prior_Status__c</field>
        <formula>IF(
AND(
NOT(ISPICKVAL(PRIORVALUE(Status),&apos;Lost&apos;)), 
NOT(ISPICKVAL(PRIORVALUE(Status),&apos;On Hold&apos;))
),
TEXT(PRIORVALUE(Status)),Prior_Status__c)</formula>
        <name>Case - Set Prior Status (NM)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Stage_to_Lost_Identifier_F</fullName>
        <description>Set Stage to Lost Identifier to FALSE</description>
        <field>Set_Stage_to_Lost__c</field>
        <literalValue>0</literalValue>
        <name>Case - Set Stage to Lost Identifier - F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Stage_to_Lost_Identifier_T</fullName>
        <description>Set Stage to Lost Identifier to TRUE</description>
        <field>Set_Stage_to_Lost__c</field>
        <literalValue>1</literalValue>
        <name>Case - Set Stage to Lost Identifier - T</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Stage_to_Owner_Assigned</fullName>
        <field>Stage__c</field>
        <literalValue>Owner Assigned</literalValue>
        <name>Case - Set Stage to Owner Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Stage_Lost</fullName>
        <field>Stage__c</field>
        <literalValue>Lost</literalValue>
        <name>Case - Stage Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Close</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Case - Status Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Uncheck_Validated_Flag</fullName>
        <description>Uncheck Validated Flag</description>
        <field>Validated__c</field>
        <literalValue>0</literalValue>
        <name>Case - Uncheck Validated Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Untick_NVMRoutable</fullName>
        <field>NVMContactWorld__NVMRoutable__c</field>
        <literalValue>0</literalValue>
        <name>Case - Untick NVMRoutable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Last_Attempted_Contact_Dat</fullName>
        <field>Last_Attempted_Contact_Date__c</field>
        <formula>NOW()</formula>
        <name>Case - Update Last Attempted Contact Dat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_NVM_Next_Contact_Date</fullName>
        <field>NVMConnect__NextContactTime__c</field>
        <formula>NOW()+(3/24)</formula>
        <name>Case - Update NVM Next Contact Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Stage_to_Deal_with_Introdu</fullName>
        <field>Stage__c</field>
        <literalValue>Deal With Introducer</literalValue>
        <name>Case - Update Stage to Deal with Introdu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_to_On_Hold</fullName>
        <field>Status</field>
        <literalValue>On Hold</literalValue>
        <name>Case - Update Status to On Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_WOD_Latest_Comment</fullName>
        <field>WOD_Latest_Comments__c</field>
        <formula>Latest_Comment__c</formula>
        <name>Case - Update WOD Latest Comment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_of_Attempts</fullName>
        <field>X_of_Attempts_Done_Today__c</field>
        <formula>IF( 
AND( 
NOT(ISNULL(X_of_Attempts_Done_Today__c)), 
DATEVALUE(Last_Attempted_Contact_Date__c) == DATEVALUE(PRIORVALUE(Last_Attempted_Contact_Date__c)) 
), 
X_of_Attempts_Done_Today__c + 1, 
1)</formula>
        <name>Case - Update # of Attempts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hold_ATT_4</fullName>
        <description>Change &quot;On hold&quot; to callback - to stop us calling the case for attempted 5.</description>
        <field>On_Hold__c</field>
        <literalValue>Callback</literalValue>
        <name>Hold ATT#4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LoggedinTimeout</fullName>
        <description>Sets the time to wait for a logged in agent.</description>
        <field>NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedIn__c</field>
        <formula>30</formula>
        <name>LoggedinTimeout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Loggedouttimeout</fullName>
        <description>Logged Out Timeout</description>
        <field>NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedOut__c</field>
        <formula>30</formula>
        <name>Loggedouttimeout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NVMAccount</fullName>
        <description>NVM Account Name - PLS</description>
        <field>NVMContactWorld__NVMAccountOverride__c</field>
        <formula>&quot;PLS&quot;</formula>
        <name>NVMAccount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NVM_Route_Plan</fullName>
        <description>Sets the NVM Call Flow Name</description>
        <field>NVMContactWorld__RoutePlanIdentifier__c</field>
        <formula>&quot;Web Form Callback&quot;</formula>
        <name>NVM Route Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Approval_Date</fullName>
        <field>Approval_Date__c</field>
        <formula>Today()</formula>
        <name>Populate Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Callback</fullName>
        <description>REmove callback to allow us to Call for attempted #5</description>
        <field>On_Hold__c</field>
        <name>Remove Callback</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Callback_1</fullName>
        <field>On_Hold__c</field>
        <name>Remove Callback and Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Callback_Set</fullName>
        <field>Call_Back_Set__c</field>
        <name>Remove Callback Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_On_Hold_status</fullName>
        <description>Remove on-hold status to re-call at attempted #7</description>
        <field>On_Hold__c</field>
        <name>Remove - On Hold status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Settled_Date_updated_with_today_s_date</fullName>
        <description>Settled date = closed date (standard field)</description>
        <field>Settled_Closed_Date__c</field>
        <formula>Today()</formula>
        <name>Settled Date updated with today&apos;s date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Test_field_update</fullName>
        <description>Update</description>
        <field>Primary_Contact_Preferred_Call_Time__c</field>
        <literalValue>12:00 pm</literalValue>
        <name>Test field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_NVM_routable</fullName>
        <field>NVMContactWorld__NVMRoutable__c</field>
        <literalValue>1</literalValue>
        <name>Tick NVM routable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Attempted_to_5</fullName>
        <field>Attempted_Contact_Status__c</field>
        <literalValue>Attempted 5</literalValue>
        <name>Update Attempted to #5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Lost_Reason</fullName>
        <field>Lost_Reason__c</field>
        <literalValue>Withdrawn by Positive - With Introducer</literalValue>
        <name>Update Case Lost Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_to_Jordan</fullName>
        <field>OwnerId</field>
        <lookupValue>jordan@positivelendingsolutions.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Case Owner to Jordan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Stage_to_Lost</fullName>
        <field>Stage__c</field>
        <literalValue>Lost</literalValue>
        <name>Update Case Stage to Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_Lost</fullName>
        <field>Status</field>
        <literalValue>Lost</literalValue>
        <name>Update Case Status to Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Deal_Suspended_Field</fullName>
        <field>Deal_Suspended__c</field>
        <literalValue>1</literalValue>
        <name>Update Deal Suspended Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Introducer_Phone</fullName>
        <field>Introducer_Phone__c</field>
        <formula>IF( 
NOT(ISBLANK(Connective_Broker_lookup__r.Phone)), 
Connective_Broker_lookup__r.Phone, 
Connective_Broker_lookup__r.MobilePhone 
)</formula>
        <name>Update Introducer Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Case_Deactivate_Client_Access_to_Full</fullName>
        <apiVersion>46.0</apiVersion>
        <description>RDAVID 4/07/2019 task25140782 Case - Deactivate Client Access to Full App</description>
        <endpointUrl>http://nodifi.dev-lightning-platform.com.au/api/salesforce/deleteApplicantPermission</endpointUrl>
        <fields>Application_GUID__c</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>rexie.david@positivelendingsolutions.com.au</integrationUser>
        <name>Case - Deactivate Client Access to Full</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Case_NOD_Invite_Applicant</fullName>
        <apiVersion>47.0</apiVersion>
        <endpointUrl>http://nodifi.dev-lightning-platform.com.au/api/salesforce/invite-applicant</endpointUrl>
        <fields>Application_GUID__c</fields>
        <fields>Id</fields>
        <fields>Primary_Contact_Email_MC__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>Case - NOD - Invite Applicant</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Case_Push_to_Overflow</fullName>
        <apiVersion>45.0</apiVersion>
        <endpointUrl>https://positive187318.jitterbit.eu/CloudTest/1.0/pushSfToOverflow</endpointUrl>
        <fields>Id</fields>
        <fields>Primary_Contact_Name_MC__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>Case - Push to Overflow</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Case_Update_Message_To_Lightning</fullName>
        <apiVersion>39.0</apiVersion>
        <endpointUrl>http://nodifi.dev-lightning-platform.com.au/api/salesforce/fetchCase</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>Case Update Message To Lightning</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Stage_Change_to_Connective</fullName>
        <apiVersion>39.0</apiVersion>
        <endpointUrl>https://positive187318.jitterbit.io/cloud/1.0/ConnectiveOutboundMessage</endpointUrl>
        <fields>Bank_Fees__c</fields>
        <fields>Comments_for_Broker_Jitterbit__c</fields>
        <fields>Commission_AppFee__c</fields>
        <fields>Commission_Trail__c</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>chris.sims@positivegroup.com.au</integrationUser>
        <name>Stage Change to Connective</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Survey_Monkey_NPS_Closed_Lost</fullName>
        <apiVersion>45.0</apiVersion>
        <endpointUrl>https://www.surveymonkey.com/apps/salesforce/230444/trigger/RbiYvQkpA4C_2FKr6oNCu6V9jy_2FQhMAL6clzlu_2FNWoqxYno2FeWPlzQij6gCUd23yt/</endpointUrl>
        <fields>Id</fields>
        <fields>Primary_Contact_Email_MC__c</fields>
        <fields>Primary_Contact_Name_MC__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>Survey Monkey - NPS Closed Lost</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>CS TEST SMS Workflow</fullName>
        <actions>
            <name>sms_notification_cstest</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Attempted_Contact_Status__c</field>
            <operation>equals</operation>
            <value>Attempted 1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CaseNumber</field>
            <operation>equals</operation>
            <value>00012136</value>
        </criteriaItems>
        <description>deactivated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case -  Close Attempted Contact</fullName>
        <active>true</active>
        <description>NM Workflow to close a Case after 1 hour when Attempted Contact Status is set to &quot;Attempted Contact 8&quot;.</description>
        <formula>AND( $Setup.Workflow_Rule_Settings1__c.Enable_Case_Workflows__c = true,  ISPICKVAL(Partition__c, &apos;Positive&apos;), ISPICKVAL(Status, &apos;New&apos;), ISPICKVAL(Stage__c, &apos;Attempted Contact&apos;), ISPICKVAL(Attempted_Contact_Status__c, &quot;Attempted 8&quot;),  NOT(ISPICKVAL(Channel__c,&apos;DLA&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Attempt_Set_to_Blank</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_Close_Reason_No_Response</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_Stage_Lost</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_Status_Close</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Clear Validated Flag</fullName>
        <actions>
            <name>Case_Uncheck_Validated_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Unchecks Validated Flag if coming from a specific Stage</description>
        <formula>AND( $Setup.Workflow_Rule_Settings1__c.Enable_Case_Workflows__c, Validated__c, TEXT(PRIORVALUE(Stage__c)) == &apos;Ready For Submission&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Closed Nodifi Send Email Notification</fullName>
        <actions>
            <name>Case_Closed_Nodifi</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(     CONTAINS(RecordType.Name, &apos;NOD&apos;), 				PRIORVALUE(Status) != &apos;Closed&apos;, 				PRIORVALUE(Stage__c) != &apos;Lost&apos;, 				ISPICKVAL(Status, &apos;Closed&apos;), 				ISPICKVAL(Stage__c, &apos;Lost&apos;), 				ISPICKVAL(Channel__c, &apos;NODIFI&apos;), 				$User.FirstName != &apos;Rexie&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Email Raoul for new NOD Cases %281%2F15%2F19%29</fullName>
        <actions>
            <name>Case_New_NOD_Case_1_15_19</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( ISNEW(), ISPICKVAL(Partition__c,&apos;Nodifi&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Hold Attempted %236</fullName>
        <actions>
            <name>Case_On_Hold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Hold Att #6 for 2 weeks, then schedule another call</description>
        <formula>AND( $Setup.Workflow_Rule_Settings1__c.Enable_Case_Workflows__c = true,  ISPICKVAL(Partition__c, &apos;Positive&apos;),  ISPICKVAL(Status, &apos;New&apos;),  ISPICKVAL(Stage__c, &apos;Attempted Contact&apos;),  ISPICKVAL(Attempted_Contact_Status__c, &apos;Attempted 6&apos;), NOT(ISPICKVAL(On_Hold__c,&apos;Callback&apos;))   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Remove_On_Hold_status</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Tick_NVM_routable</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Invoke LFP Outbound API</fullName>
        <actions>
            <name>Case_Deactivate_Client_access_to_Full_Application</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Deactivate_Client_Access_to_Full</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>RDAVID 4/07/2019 task25140782 - This workflow will invoke Outbound in Fronted to deactivate client access to full app.</description>
        <formula>AND(      $Setup.Workflow_Rule_Settings1__c.Enable_Case_Workflows__c,      Deactivate_Client_Access_to_Full_App__c  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Manager review</fullName>
        <actions>
            <name>Alert_Manager_of_Personal_Loan_closed_case</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 and 5</booleanFilter>
        <criteriaItems>
            <field>Case.Status_Funnel_Submitted__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Channel__c</field>
            <operation>equals</operation>
            <value>PLS,LFPWBC,DLA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Partition__c</field>
            <operation>equals</operation>
            <value>Positive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>POS: Consumer - Personal Loan</value>
        </criteriaItems>
        <description>Alert Dan Adams that a Personal Loan has not been submitted and lost</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - NVM Next Contact Date</fullName>
        <actions>
            <name>Case_Update_NVM_Next_Contact_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.NVMConnect__NextContactTime__c</field>
            <operation>greaterThan</operation>
            <value>1/1/3000</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - New Submission Request Submitted</fullName>
        <actions>
            <name>Case_New_Submission_Request_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>NM Workflow email alert to notify support@positivelendingsolutions.com.au</description>
        <formula>AND(  ISCHANGED(Stage__c),  TEXT(Stage__c) = &apos;Ready for Submission&apos;, TEXT(Partition__c) = &apos;Positive&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Notify New Complex Scenario Created</fullName>
        <actions>
            <name>Case_New_Complex_Scenario_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>NM workflow version of &quot;Notify New Complex Scenario Created&quot;</description>
        <formula>AND( $Setup.Workflow_Rule_Settings1__c.Enable_Case_Scenario_Workflows__c,  Id != null,  OR(RecordType.Name = $Label.Case_RT_N_Commercial_Scenario,     RecordType.Name = $Label.Case_RT_N_Consumer_Scenario) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Notify Owner Of Owner Assigned Stage</fullName>
        <actions>
            <name>New_Case_Assignment</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Case_Workflows__c, ISPICKVAL(Stage__c, &apos;Owner Assigned&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Notify Settlements officer and Manager of lost deal</fullName>
        <actions>
            <name>Case_Notify_Settlements_officer_and_Manager_of_lost_deal</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Partition__c</field>
            <operation>equals</operation>
            <value>Positive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status_Funnel_Committed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Lost</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - PHL - PLS Case submitted to lender</fullName>
        <actions>
            <name>Case_PHL_PLS_CASE_is_Submitted_to_lender</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3) AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.Partition__c</field>
            <operation>equals</operation>
            <value>Positive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Has_Investment_Property__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Has_Owner_Occupied__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Channel__c</field>
            <operation>equals</operation>
            <value>PLS,LFPWBC,DLA</value>
        </criteriaItems>
        <description>Sends and email to Elliot when a PLS CASE with mortgage or investment property has been submitted to lender</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - PWM Close Case</fullName>
        <active>true</active>
        <formula>AND(      $Setup.Workflow_Rule_Settings1__c.Enable_Case_Workflows__c,     RecordType.Name == &apos;PWM: Insurance App&apos;,      ISPICKVAL(Stage__c,&apos;Attempted Contact&apos;),     ISPICKVAL(Attempted_Contact_Status__c,&apos;Attempted 6&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_PWM_Close_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_PWM_Closed_Reason_No_Response</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_PWM_Stage_to_Lost</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Populate Assigned Team at Closed</fullName>
        <actions>
            <name>Case_Populate_Assigned_Team_At_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(     $Setup.Workflow_Rule_Settings1__c.Enable_Case_Workflows__c,     ISPICKVAL(Status, &apos;Closed&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Push To Overflow</fullName>
        <actions>
            <name>Case_Push_to_Overflow</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>(Positive Partition) - LFPWBC

Check on Closed Reason update if Waiting on TP
Check on FE Submission Complete if Case is already referred Out</description>
        <formula>AND( Owner:Queue.QueueName == &apos;Overflow&apos;, ISPICKVAL(Lead_Bucket__c,&apos;Overflow&apos;), ISPICKVAL(Closed_Reason__c,&apos;Referred to Overflow&apos;), FE_Submission_Complete__c, NOT(ISBLANK(TEXT(Closed_Reason__c))), OR( AND( ISCHANGED(Closed_Reason__c), NOT(ISCHANGED(FE_Submission_Complete__c)) ), AND( ISCHANGED(FE_Submission_Complete__c), NOT(ISCHANGED(Closed_Reason__c)) )  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Ready For Submission Notification</fullName>
        <actions>
            <name>Case_Ready_For_Submission_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Case_Workflows__c,  ISPICKVAL(Stage__c, &apos;Ready For Submission&apos;), ISPICKVAL(Status, &apos; Review&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Remove Submitter on Succeeding Submissions</fullName>
        <actions>
            <name>Case_Remove_Submitter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>RDAVID 15/05/2019: extra/task24564956-OLDSLACleanUp Deactivated as per Chris.</description>
        <formula>AND(  $Setup.Workflow_Rule_Settings__c.Enable_Case_Workflows__c,  NOT(ISNULL(Submitter__c)),  ISPICKVAL(Stage__c, &apos;Ready for Submission&apos;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Scenario - Stage Lost</fullName>
        <actions>
            <name>Case_Scenario_Lost_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>NM workflow rule for Case Scenario Stage Lost.</description>
        <formula>AND( $Setup.Workflow_Rule_Settings1__c.Enable_Case_Scenario_Workflows__c ,  OR(RecordType.Name == $Label.Case_RT_N_Commercial_Scenario,    RecordType.Name ==  $Label.Case_RT_N_Consumer_Scenario), ISCHANGED(Stage__c),     ISPICKVAL(Status, &apos;Lost&apos;),   ISPICKVAL(Stage__c, &apos;Lost&apos;),   ISPICKVAL(Lost_Reason__c, &apos;Went Cold&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Send NPS Closed Lost Survey</fullName>
        <actions>
            <name>Survey_Monkey_NPS_Closed_Lost</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <formula>AND( NOT(ISNEW()), CaseNumber == &apos;00012136&apos;, ISCHANGED(Subject) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Send text after close</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Partition__c</field>
            <operation>equals</operation>
            <value>Positive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Attempted_Contact_Status__c</field>
            <operation>equals</operation>
            <value>Attempted 10</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Closed_Reason__c</field>
            <operation>equals</operation>
            <value>No Response from Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Case.ClosedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Set Approval Date</fullName>
        <actions>
            <name>Populate_Approval_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Approval Date when Case</description>
        <formula>AND( $Setup.Workflow_Rule_Settings1__c.Enable_Case_Workflows__c,  ISCHANGED(Status), ISNULL(PRIORVALUE(Approval_Date__c)), ISPICKVAL(Status, &apos;Approved&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Set Callback To Open Leads</fullName>
        <active>true</active>
        <description>Downgraded from Process Builder to WFR due to PB Scheduled Action limitation/issue for Inactive Users</description>
        <formula>AND( $Setup.Process_Flow_Definition_Settings1__c.Enable_Case_Flow_Definitions__c, ISPICKVAL(Partition__c, &apos;Positive&apos;), ISPICKVAL(On_Hold__c, &apos;Callback&apos;), OR( ISPICKVAL(Stage__c, &apos;Attempted Contact&apos;), ISPICKVAL(Stage__c, &apos;Open&apos;) ), NOT(ISNULL(Call_Back_Set__c)), Run_PB_Sched_Action_Through_WFR__c	 )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_PB_Sched_Action_Remove_Callback</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Call_Back_Set__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Set Stage to Deal With Introducer after 8 Days on Waiting on Docs</fullName>
        <active>false</active>
        <formula>AND( ISPICKVAL(Stage__c, &apos;Waiting on Docs&apos;),  OR(CASESAFEID(RecordTypeId) ==  $Setup.RecordType_And_Profile_IDs__c.Case_Consumer_Loan_RT_ID__c, CASESAFEID(RecordTypeId) ==  $Setup.RecordType_And_Profile_IDs__c.Case_Commercial_Loan_RT_ID__c), $Setup.Workflow_Rule_Settings__c.Enable_Case_Workflows__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Update_Stage_to_Deal_with_Introdu</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_Update_Status_to_On_Hold</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Set Stage to Lost Identifier - FALSE</fullName>
        <actions>
            <name>Case_Set_Stage_to_Lost_Identifier_F</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  WOD_Latest_Comments__c != Latest_Comment__c ,  NOT(ISPICKVAL(Stage__c, &apos;Waiting on Docs&apos;)),  NOT(ISPICKVAL(Status,&apos;New&apos;)),  NOT(ISPICKVAL(Status,&apos;Review&apos;))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Set Stage to Lost Identifier - TRUE</fullName>
        <actions>
            <name>Case_Set_Stage_to_Lost_Identifier_T</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL(Stage__c, &apos;Waiting on Docs&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Set Stage to Lost after 14 Days on Deal with Introducer</fullName>
        <active>false</active>
        <formula>AND( ISPICKVAL(Stage__c, &apos;Deal With Introducer&apos;), $Setup.Workflow_Rule_Settings__c.Enable_Case_Workflows__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Connective_Broker_Notification</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Set_Prior_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_Set_Prior_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Case_Lost_Reason</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Case_Stage_to_Lost</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Case_Status_to_Lost</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Set Stage to Lost after 8 Days on Waiting on Docs</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Set_Stage_to_Lost__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Set_Prior_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Case_Lost_Reason</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Case_Stage_to_Lost</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Case_Status_to_Lost</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Set Stage to Owner Assigned</fullName>
        <actions>
            <name>Case_Set_Stage_to_Owner_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3) AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>Api Admin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Service</value>
        </criteriaItems>
        <description>Set Case Stage to Owner when Case is Open and Owner is not API Admin</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Store Stage Before Update</fullName>
        <actions>
            <name>Case_Set_Prior_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Stage__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Store Status%2FStage Before Update</fullName>
        <actions>
            <name>Case_Set_Prior_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Set_Prior_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR( $Setup.Workflow_Rule_Settings1__c.Enable_Case_Workflows__c, $Setup.Workflow_Rule_Settings__c.Enable_Case_Workflows__c),   OR( ISCHANGED(Stage__c), ISCHANGED(Status) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Store Status%2FStage Before Update %28NM%29</fullName>
        <actions>
            <name>Case_Set_Prior_Stage_NM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Set_Prior_Status_NM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>NM Version of the flow.</description>
        <formula>AND( $Setup.Workflow_Rule_Settings1__c.Enable_Case_Workflow_for_Support_Request__c, OR( ISCHANGED(Stage__c), ISCHANGED(Status) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Submitted To Lender Notification</fullName>
        <actions>
            <name>Case_Submitted_To_Lender_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Case_Workflows__c,  ISPICKVAL(Status,&apos;Submitted&apos;), ISPICKVAL(Stage__c,&apos;Submitted to Lender&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Untick NVMRoutable</fullName>
        <actions>
            <name>Case_Populate_Sent_To_NVM_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Untick_NVMRoutable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND($Setup.Workflow_Rule_Settings1__c.Enable_Case_Workflows__c, NVMContactWorld__NVMRoutable__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Update Deal Suspended When Stage Supended Settlement</fullName>
        <actions>
            <name>Update_Deal_Suspended_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Case_Workflows__c, ISPICKVAL(Stage__c, &apos;Suspended Settlement&apos;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Update Last Attempted Contact Date</fullName>
        <actions>
            <name>Case_Update_Last_Attempted_Contact_Dat</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_of_Attempts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Workflow_Rule_Settings1__c.Enable_Case_Workflows__c, ISCHANGED(Attempted_Contact_Status__c), ISPICKVAL(Stage__c,&apos;Attempted Contact&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Update Latest Comment</fullName>
        <actions>
            <name>Case_Copy_Latest_Comment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>NM. Update Latest Comment with Latest Comment (Copy)</description>
        <formula>ISCHANGED( Latest_Comment__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Update WOD Latest Comment</fullName>
        <actions>
            <name>Case_Update_WOD_Latest_Comment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Store Latest Comment on Waiting on Docs Stage</description>
        <formula>AND( $Setup.Workflow_Rule_Settings__c.Enable_Case_Workflows__c,  ISPICKVAL( Stage__c , &apos;Waiting on Docs&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Update to ATT %235</fullName>
        <actions>
            <name>Hold_ATT_4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Partition__c</field>
            <operation>equals</operation>
            <value>Positive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.On_Hold__c</field>
            <operation>notEqual</operation>
            <value>Callback</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Attempted_Contact_Status__c</field>
            <operation>equals</operation>
            <value>Attempted 4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Attempted_to_5</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Remove_Callback</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>36</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Child Case - Alert owner when Parent case is closed</fullName>
        <actions>
            <name>Child_Case_Owner_Parent_case_has_been_closed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Parent_Case_Stage__c</field>
            <operation>equals</operation>
            <value>Won,Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>POS: PHL Opportunity</value>
        </criteriaItems>
        <description>Sends an alert to he child case owner to notify them that the Parent case has been closed and we can refer deal to Pepper</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Notification when a case is created</fullName>
        <actions>
            <name>Notification_sent_to_Jordan_on_Case_creation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Case_Owner_to_Jordan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NVM Web Callback</fullName>
        <actions>
            <name>Case_Routable</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>LoggedinTimeout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Loggedouttimeout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NVMAccount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NVM_Route_Plan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lead_Owner_Team__c</field>
            <operation>equals</operation>
            <value>Team 1,Team 2,Team 3</value>
        </criteriaItems>
        <description>Initiate a callback for new cases created where status is new and stage is open</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Outbound Message to Connective on Stage change</fullName>
        <actions>
            <name>Stage_Change_to_Connective</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Approved with Conditions,Approved - No Conditions,Final Modifications - WOAA,Documents Sent to Client,Part Docs Received,Sent for Settlement,Suspended Settlement,Settled,Submitted to Lender,Tax Invoice Requested,Tax Invoice Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Connective_Opportunity_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.JWT_Token__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.JWT_Expiry__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remove Callback</fullName>
        <actions>
            <name>Remove_Callback_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_Callback_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Partition__c</field>
            <operation>equals</operation>
            <value>Positive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.On_Hold__c</field>
            <operation>equals</operation>
            <value>Callback,Asset Not Ready</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Call_Back_Set__c</field>
            <operation>lessOrEqual</operation>
            <value>YESTERDAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New,Review,Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Case Update to Lightning</fullName>
        <actions>
            <name>Case_Update_Message_To_Lightning</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR(RecordType.Name == &apos;Commercial Loan&apos;, RecordType.Name == &apos;Consumer Loan&apos;, RecordType.Name == &apos;Vehicle Sourcing&apos;,  RecordType.Name == &apos;NOD: Consumer - Asset Finance&apos;, RecordType.Name == &apos;NOD: Consumer - Personal Loan&apos;,  RecordType.Name == &apos;NOD: Commercial - Asset Finance&apos;,  RecordType.Name == &apos;NOD: Commercial - Business Funding&apos;  ),  NOT(ISBLANK(Application_GUID__c)),  OR(ISCHANGED(Stage__c),  ISCHANGED(AccountId),  ISCHANGED(Application_Type__c),  ISCHANGED(Required_Loan_Amount__c),  ISCHANGED(Lender1__c),  ISCHANGED(OwnerId),  ISCHANGED(Loan_Product__c),  ISCHANGED(Approval_Date__c),  ISCHANGED(Loan_Rate__c),  ISCHANGED(Base_Rate__c),  ISCHANGED(Regular_Monthly_Payment__c),  ISCHANGED(Loan_Reference__c),  ISCHANGED(Approval_Conditions__c),  ISCHANGED(Settled_Closed_Date__c),  ISCHANGED(Sale_Type__c),  ISCHANGED(Asset_Type__c),  ISCHANGED(Asset_Sub_Type__c),  ISCHANGED(Loan_Term__c),  ISCHANGED(Requested_Payment_Frequency__c),  ISCHANGED(Brokerage_inc_gst__c),  ISCHANGED(Application_Fee__c),  ISCHANGED(Total_Amount_Financed__c),  ISCHANGED(Status)  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Commission CT Object When Case Settled</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Settled</value>
        </criteriaItems>
        <description>Update Commission CT Object When Case Settled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Introducer Phone</fullName>
        <actions>
            <name>Update_Introducer_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISCHANGED(Connective_Broker_lookup__c),  NOT(ISBLANK(Connective_Broker_lookup__c))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Settled Date to today if stage has been changed to Settled or Lost</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Settled,Lost</value>
        </criteriaItems>
        <description>Updates the settled date (Standard Field - Closed date ) to today if Stage is changed to Settled or Lost</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Settled_Date_updated_with_today_s_date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Test_SMS_Notification_2</fullName>
        <assignedToType>owner</assignedToType>
        <description>LFP Attempted 1 SMS</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Test SMS Notification 2</subject>
    </tasks>
    <tasks>
        <fullName>sms_notification_cstest</fullName>
        <assignedToType>owner</assignedToType>
        <description>PA-000002</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>sms notification</subject>
    </tasks>
</Workflow>

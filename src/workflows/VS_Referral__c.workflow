<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_VS_Referral</fullName>
        <description>New VS Referral</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Template/New_VS_Referral_Email</template>
    </alerts>
    <alerts>
        <fullName>Used_Cars_Adelaide_Referral</fullName>
        <description>Used Cars Adelaide Referral Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>luke.caesar@nodifi.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UCA_Attempteds/Used_Cars_Adelaide_Referral_Email_for_Luke</template>
    </alerts>
    <alerts>
        <fullName>WF003_Send_notification_to_Opp_or_Case_owner_on_update</fullName>
        <description>WF003-Send notification to Opp or Case owner on update</description>
        <protected>false</protected>
        <recipients>
            <field>Opp_or_Case_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notify_CaseOpp_owner_of_change_on_VSR</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Owner_to_Daniel_Zandona</fullName>
        <field>OwnerId</field>
        <lookupValue>luke.caesar@nodifi.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Owner to Luke Caesar</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF001_Update_Client_FirstName</fullName>
        <field>Client_First_Name__c</field>
        <formula>Opportunity__r.Contact__r.FirstName</formula>
        <name>WF001 Update Client FirstName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF001_Update_Client_LastName</fullName>
        <field>Client_Last_Name__c</field>
        <formula>Opportunity__r.Contact__r.LastName</formula>
        <name>WF001 Update Client LastName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF002_Update_ClientFirstName_Case</fullName>
        <field>Client_First_Name__c</field>
        <formula>Case__r.Contact.FirstName</formula>
        <name>WF002_Update_ClientFirstName_Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF002_Update_ClientLastName_Case</fullName>
        <field>Client_Last_Name__c</field>
        <formula>Case__r.Contact.LastName</formula>
        <name>WF002_Update_ClientLastName_Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF004_Update_opp_case_email_field</fullName>
        <description>Update from the opportunity email field</description>
        <field>Opp_or_Case_Owner_Email__c</field>
        <formula>Opportunity__r.Owner.Email</formula>
        <name>WF004-Update opp case email field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF005_Update_opp_case_email_field</fullName>
        <field>Opp_or_Case_Owner_Email__c</field>
        <formula>Case__r.Owner:User.Email</formula>
        <name>WF005-Update opp case email field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF006_Update_Body_Type_on_VSR</fullName>
        <field>Body_Type__c</field>
        <formula>Body_Type_opp__c</formula>
        <name>WF006-Update Body Type on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF006_Update_Colour_on_VSR</fullName>
        <field>Colour__c</field>
        <formula>Colour_opp_case__c</formula>
        <name>WF006-Update Colour on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF006_Update_Fuel_Type_on_VSR</fullName>
        <field>Fuel_Tyoe__c</field>
        <formula>Fuel_Type_opp_case__c</formula>
        <name>WF006-Update Fuel Type on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF006_Update_Model_on_VSR</fullName>
        <field>Model__c</field>
        <formula>Model_opp_case__c</formula>
        <name>WF006-Update Model on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF006_Update_Transmission_on_VSR</fullName>
        <field>Transmission_Hidden__c</field>
        <formula>Transmission_opp_case__c</formula>
        <name>WF006-Update Transmission on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF006_Update_make_on_VSR</fullName>
        <field>Car_Make__c</field>
        <formula>Make_opp_case__c</formula>
        <name>WF006-Update make on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF006_Update_variant_on_VSR</fullName>
        <field>Varient__c</field>
        <formula>Varient_opp_case__c</formula>
        <name>WF006-Update variant on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF006_Update_year_on_VSR</fullName>
        <field>Make_Year__c</field>
        <formula>Year_opp_case__c</formula>
        <name>WF006-Update year on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF007_Update_country_on_VSR</fullName>
        <description>Update country on VSR from opportunity, from accounts</description>
        <field>Country__c</field>
        <formula>CASE(Opportunity__r.Account.RecordType.DeveloperName, &apos;Business&apos;, text(Opportunity__r.Account.Country__c), 
&apos;Corporate Trust&apos;, text(Opportunity__r.Account.Trust_Country__c), 
&apos;Individual&apos;, Opportunity__r.Account.Current_Country__c, 
&apos;Individual/Joint Trust&apos;, text(Opportunity__r.Account.Trust_Country__c), 
&apos;Property&apos;, Opportunity__r.Account.Current_Country__c, &apos;not provided&apos;)</formula>
        <name>WF007-Update country on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF007_Update_postcode_on_VSR</fullName>
        <description>from opp</description>
        <field>Postcode__c</field>
        <formula>CASE(Opportunity__r.Account.RecordType.DeveloperName, &apos;Business&apos;, value(Opportunity__r.Account.Postcode_2__c), 
&apos;Corporate Trust&apos;, value(Opportunity__r.Account.Trust_Postcode_2__c), 
&apos;Individual&apos;, Opportunity__r.Account.Current_Postcode__c, 
&apos;Individual/Joint Trust&apos;, value(Opportunity__r.Account.Trust_Postcode_2__c), 
&apos;Property&apos;, Opportunity__r.Account.Current_Postcode__c, 0000)</formula>
        <name>WF007-Update postcode on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF007_Update_state_on_VSR</fullName>
        <field>State__c</field>
        <formula>CASE(Opportunity__r.Account.RecordType.DeveloperName, &apos;Business&apos;, text(Opportunity__r.Account.State1__c), 
&apos;Corporate Trust&apos;, text(Opportunity__r.Account.Trust_State__c), 
&apos;Individual&apos;, text(Opportunity__r.Account.Current_State__c), 
&apos;Individual/Joint Trust&apos;, text(Opportunity__r.Account.Trust_State__c), 
&apos;Property&apos;, text(Opportunity__r.Account.Current_State__c), &apos;not provided&apos;)</formula>
        <name>WF007-Update state on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF007_Update_street_no_on_VSR</fullName>
        <field>Street_Number__c</field>
        <formula>CASE(Opportunity__r.Account.RecordType.DeveloperName, &apos;Business&apos;, Opportunity__r.Account.Street_Number__c, 
	&apos;Corporate Trust&apos;, Opportunity__r.Account.Trust_Street_Number__c,
	&apos;Individual&apos;, Opportunity__r.Account.Current_Street_Number__c,
	&apos;Individual/Joint Trust&apos;, Opportunity__r.Account.Trust_Street_Number__c,
	&apos;Property&apos;, Opportunity__r.Account.Current_Street_Number__c, &apos;not provided&apos;)</formula>
        <name>WF007-Update street no on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF007_Update_street_on_VSR</fullName>
        <field>Street_Name__c</field>
        <formula>CASE(Opportunity__r.Account.RecordType.DeveloperName, &apos;Business&apos;, Opportunity__r.Account.Street_Name__c, 
&apos;Corporate Trust&apos;, Opportunity__r.Account.Trust_Street_Name__c, 
&apos;Individual&apos;, Opportunity__r.Account.Current_Street_Name__c, 
&apos;Individual/Joint Trust&apos;, Opportunity__r.Account.Trust_Street_Name__c, 
&apos;Property&apos;, Opportunity__r.Account.Current_Street_Name__c, &apos;not provided&apos;)</formula>
        <name>WF007-Update street on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF007_Update_street_type_on_VSR</fullName>
        <field>Street_Type__c</field>
        <formula>CASE(Opportunity__r.Account.RecordType.DeveloperName, &apos;Business&apos;, Opportunity__r.Account.Street_Type__c, 
&apos;Corporate Trust&apos;, Opportunity__r.Account.Trust_Street_Type__c, 
&apos;Individual&apos;, text(Opportunity__r.Account.Current_Street_Type__c), 
&apos;Individual/Joint Trust&apos;, Opportunity__r.Account.Trust_Street_Type__c, 
&apos;Property&apos;, text(Opportunity__r.Account.Current_Street_Type__c), &apos;not provided&apos;)</formula>
        <name>WF007-Update street type on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF007_Update_suburb_on_VSR</fullName>
        <description>on opp</description>
        <field>Suburb__c</field>
        <formula>CASE(Opportunity__r.Account.RecordType.DeveloperName, &apos;Business&apos;, Opportunity__r.Account.Suburb__c, 
&apos;Corporate Trust&apos;, Opportunity__r.Account.Trust_Suburb__c, 
&apos;Individual&apos;, Opportunity__r.Account.Current_Suburb__c, 
&apos;Individual/Joint Trust&apos;, Opportunity__r.Account.Trust_Suburb__c, 
&apos;Property&apos;, Opportunity__r.Account.Current_Suburb__c, &apos;not provided&apos;)</formula>
        <name>WF007-Update suburb on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF008_Update_Street_on_VSR</fullName>
        <description>from case, from account</description>
        <field>Street_Name__c</field>
        <formula>CASE(Case__r.Account.RecordType.DeveloperName, &apos;Business&apos;, Case__r.Account.Street_Name__c, 
	&apos;Corporate Trust&apos;, Case__r.Account.Trust_Street_Name__c,
	&apos;Individual&apos;, Case__r.Account.Current_Street_Name__c,
	&apos;Individual/Joint Trust&apos;, Case__r.Account.Trust_Street_Name__c,
	&apos;Property&apos;, Case__r.Account.Current_Street_Name__c, &apos;not provided&apos;)</formula>
        <name>WF008-Update Street on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF008_Update_country_on_VSR</fullName>
        <description>from case, then account</description>
        <field>Country__c</field>
        <formula>CASE(Case__r.Account.RecordType.DeveloperName, &apos;Business&apos;, text(Case__r.Account.Country__c), 
	&apos;Corporate Trust&apos;, text(Case__r.Account.Trust_Country__c),
	&apos;Individual&apos;, Case__r.Account.Current_Country__c,
	&apos;Individual/Joint Trust&apos;, text(Case__r.Account.Trust_Country__c),
	&apos;Property&apos;, Case__r.Account.Current_Country__c, &apos;not provided&apos;)</formula>
        <name>WF008-Update country on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF008_Update_postcode_on_VSR</fullName>
        <description>from case, from account</description>
        <field>Postcode__c</field>
        <formula>CASE(Case__r.Account.RecordType.DeveloperName, &apos;Business&apos;, Case__r.Account.Postcode__c, 
	&apos;Corporate Trust&apos;, value(Case__r.Account.Trust_Postcode_2__c),
	&apos;Individual&apos;, Case__r.Account.Current_Postcode__c,
	&apos;Individual/Joint Trust&apos;, value(Case__r.Account.Trust_Postcode_2__c),
	&apos;Property&apos;, Case__r.Account.Current_Postcode__c, 0000)</formula>
        <name>WF008-Update postcode on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF008_Update_state_on_VSR</fullName>
        <description>from case, from account</description>
        <field>State__c</field>
        <formula>CASE(Case__r.Account.RecordType.DeveloperName, &apos;Business&apos;, text(Case__r.Account.State__c), 
	&apos;Corporate Trust&apos;, text(Case__r.Account.Trust_State__c),
	&apos;Individual&apos;, text(Case__r.Account.Current_State__c),
	&apos;Individual/Joint Trust&apos;, text(Case__r.Account.Trust_State__c),
	&apos;Property&apos;, text(Case__r.Account.Current_State__c), &apos;not provided&apos;)</formula>
        <name>WF008-Update state on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF008_Update_street_number_on_VSR</fullName>
        <description>from case, from account</description>
        <field>Street_Number__c</field>
        <formula>CASE(Case__r.Account.RecordType.DeveloperName, &apos;Business&apos;, Case__r.Account.Street_Number__c, 
	&apos;Corporate Trust&apos;, Case__r.Account.Trust_Street_Number__c,
	&apos;Individual&apos;, Case__r.Account.Current_Street_Number__c,
	&apos;Individual/Joint Trust&apos;, Case__r.Account.Trust_Street_Number__c,
	&apos;Property&apos;, Case__r.Account.Current_Street_Number__c, &apos;not provided&apos;)</formula>
        <name>WF008-Update street number on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF008_Update_street_type_on_VSR</fullName>
        <description>from case, from account</description>
        <field>Street_Type__c</field>
        <formula>CASE(Case__r.Account.RecordType.DeveloperName, &apos;Business&apos;, Case__r.Account.Street_Type__c, 
	&apos;Corporate Trust&apos;, Case__r.Account.Trust_Street_Type__c,
	&apos;Individual&apos;, text(Case__r.Account.Current_Street_Type__c),
	&apos;Individual/Joint Trust&apos;, Case__r.Account.Trust_Street_Type__c,
	&apos;Property&apos;, text(Case__r.Account.Current_Street_Type__c), &apos;not provided&apos;)</formula>
        <name>WF008-Update street type on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF008_Update_suburb_on_VSR</fullName>
        <description>from case, from account</description>
        <field>Suburb__c</field>
        <formula>CASE(Case__r.Account.RecordType.DeveloperName, &apos;Business&apos;, Case__r.Account.Suburb__c, 
	&apos;Corporate Trust&apos;, Case__r.Account.Trust_Suburb__c,
	&apos;Individual&apos;, Case__r.Account.Current_Suburb__c,
	&apos;Individual/Joint Trust&apos;, Case__r.Account.Trust_Suburb__c,
	&apos;Property&apos;, Case__r.Account.Current_Suburb__c, &apos;not provided&apos;)</formula>
        <name>WF008-Update suburb on VSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF009_Update_email_from_opp</fullName>
        <field>Email__c</field>
        <formula>Opportunity__r.Email__c</formula>
        <name>WF009-Update email from opp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF010_Update_email_on_VSR_from_case</fullName>
        <field>Email__c</field>
        <formula>Case__r.Email__c</formula>
        <name>WF010-Update email on VSR from case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF011_Update_VSR_Outcome_Detail</fullName>
        <field>Outcome_Reason__c</field>
        <literalValue>VSR Sourced - External Dealer</literalValue>
        <name>WF011-Update VSR Outcome Detail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF011_Update_VSR_to_Won</fullName>
        <field>Stage__c</field>
        <literalValue>Won</literalValue>
        <name>WF011-Update VSR to Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF012_Update_VSR_Outcome_Detai_for_Lost</fullName>
        <field>Outcome_Reason__c</field>
        <literalValue>Customer Not Proceeding - Not Invoiced</literalValue>
        <name>WF012-Update VSR Outcome Detai for Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF012_Update_VSR_to_Lost</fullName>
        <field>Stage__c</field>
        <literalValue>Lost</literalValue>
        <name>WF012-Update VSR to Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Assign to Luke Caesar</fullName>
        <actions>
            <name>New_VS_Referral</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Owner_to_Daniel_Zandona</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VS_Referral__c.Opportunity_Owner__c</field>
            <operation>notEqual</operation>
            <value>Luke Caesar</value>
        </criteriaItems>
        <description>Formerly &quot;Assign to Daniel Zandona&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Used Cars Adelaide Referral Email to Luke</fullName>
        <actions>
            <name>Used_Cars_Adelaide_Referral</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>VS_Referral__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WF001_Update_ClientName_Opp</fullName>
        <actions>
            <name>WF001_Update_Client_FirstName</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF001_Update_Client_LastName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Opportunity__c &lt;&gt; &apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF002_Update_ClientName_Case</fullName>
        <actions>
            <name>WF002_Update_ClientFirstName_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF002_Update_ClientLastName_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks to see if the case is populated on the VSR record.</description>
        <formula>Case__c &lt;&gt; &apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF003-Send notification to Opp or Case owner on update</fullName>
        <actions>
            <name>WF003_Send_notification_to_Opp_or_Case_owner_on_update</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(ISCHANGED( Activity_History__c), ISCHANGED(Stage_History__c), ISCHANGED(PCS_Comments__c)) ,(RecordType.Name = &apos;VS Referral - Locked&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF004-Update opp case email field</fullName>
        <actions>
            <name>WF004_Update_opp_case_email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update with opp email</description>
        <formula>not(ISNULL(Opportunity__c) &amp;&amp; ISNULL(VSR_Owner_First_Name__c ) &amp;&amp; isnull( VSR_Owner_Last_Name__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF005-Update opp case email field</fullName>
        <actions>
            <name>WF005_Update_opp_case_email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update with case email</description>
        <formula>not(ISNULL(Case__c) &amp;&amp; ISNULL(VSR_Owner_First_Name__c ) &amp;&amp; isnull( VSR_Owner_Last_Name__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF006-Update car details on VSR from opp</fullName>
        <actions>
            <name>WF006_Update_Body_Type_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF006_Update_Colour_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF006_Update_Fuel_Type_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF006_Update_Model_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF006_Update_Transmission_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF006_Update_make_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF006_Update_variant_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF006_Update_year_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Opportunity__c &lt;&gt; &apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF007-Update address details on VSR from opp</fullName>
        <actions>
            <name>WF007_Update_country_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF007_Update_postcode_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF007_Update_state_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF007_Update_street_no_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF007_Update_street_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF007_Update_street_type_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF007_Update_suburb_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNULL(Opportunity__c)), NOT(ISBLANK(Opportunity__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF008-Update address details on VSR from case</fullName>
        <actions>
            <name>WF008_Update_Street_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF008_Update_country_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF008_Update_postcode_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF008_Update_state_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF008_Update_street_number_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF008_Update_street_type_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF008_Update_suburb_on_VSR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISNULL(Case__c)), NOT(ISBLANK(Case__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF009-Update email from opp</fullName>
        <actions>
            <name>WF009_Update_email_from_opp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>update email from opp</description>
        <formula>Opportunity__c &lt;&gt;&apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF010-Update email on VSR from case</fullName>
        <actions>
            <name>WF010_Update_email_on_VSR_from_case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Case__c &lt;&gt; &apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF011-Update VSR to Won</fullName>
        <actions>
            <name>WF011_Update_VSR_Outcome_Detail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF011_Update_VSR_to_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VS_Referral__c.Stage_History__c</field>
            <operation>contains</operation>
            <value>Settled</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF012-Update VSR to Lost</fullName>
        <actions>
            <name>WF012_Update_VSR_Outcome_Detai_for_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WF012_Update_VSR_to_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VS_Referral__c.Stage_History__c</field>
            <operation>contains</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

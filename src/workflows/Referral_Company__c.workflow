<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Touch base with Referral Company</fullName>
        <active>false</active>
        <description>If today is greater than 30 days since the BDM has last contacted the Referral Company, send them an email reminding them to touch base</description>
        <formula>(TODAY()- Last_Contact_Date__c ) &gt;=30</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

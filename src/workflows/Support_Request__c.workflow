<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Support_Request_Assigned_to_Agent</fullName>
        <description>Support Request - Assigned to Agent</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>NM_PLS_Templates/Support_Request_Assigned_to_Agent</template>
    </alerts>
    <alerts>
        <fullName>Support_Request_Call_Request_Assigned</fullName>
        <ccEmails>rexie.david@positivelendingsolutions.com.au</ccEmails>
        <description>Support Request - Call Request Assigned</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Support_Request_Call_Request_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Support_Request_Closed_CAR</fullName>
        <description>Support Request - Closed CAR</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Support_Request_Closed_CAR</template>
    </alerts>
    <alerts>
        <fullName>Support_Request_Closed_Notification</fullName>
        <description>Support Request - Closed Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NM_PLS_Templates/Support_Request_Closed_Notification_Template</template>
    </alerts>
    <alerts>
        <fullName>Support_Request_Failed_Support_Request</fullName>
        <description>Support Request - Failed Support Request</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Support_Request_Failed_Support_Request</template>
    </alerts>
    <alerts>
        <fullName>Support_Request_New_CAR</fullName>
        <description>Support Request - New CAR</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Support_Request_New_CAR</template>
    </alerts>
    <alerts>
        <fullName>Test_Email</fullName>
        <description>Test Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>rexie.david@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Test_Email1</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Refresh_Flag_to_True</fullName>
        <field>Refresh_Case__c</field>
        <literalValue>1</literalValue>
        <name>Set Refresh Flag to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Close_Date</fullName>
        <field>Close_Date__c</field>
        <formula>now()</formula>
        <name>Update Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Support_Request_Move_File_Attachments</fullName>
        <apiVersion>43.0</apiVersion>
        <endpointUrl>https://www.workato.com/webhooks/notify/salesforce?sobject=Support_Request__c&amp;org_id=00D5O00000092rJ</endpointUrl>
        <fields>Case_Box_Folder_ID__c</fields>
        <fields>Case_Number__c</fields>
        <fields>Files_Moved_to_Box__c</fields>
        <fields>Id</fields>
        <fields>Transfer_Files_via_Workato__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>Support Request - Move File Attachments</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Support Request - Move File Attachments</fullName>
        <actions>
            <name>Support_Request_Move_File_Attachments</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Move File Attachments to Box</description>
        <formula>AND(  $Setup.Workflow_Rule_Settings1__c.Enable_Support_Request_Workflows__c,  ISCHANGED(Transfer_Files_via_Workato__c),  Transfer_Files_via_Workato__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support Request - Set Refresh Flag</fullName>
        <actions>
            <name>Set_Refresh_Flag_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( TEXT(Status__c) == &apos;In Progress&apos;, TEXT(PRIORVALUE(Status__c)) != &apos;In Progress&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support Request - Update Close Date</fullName>
        <actions>
            <name>Update_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( TEXT(PRIORVALUE( Status__c )) != &apos;Closed&apos;,TEXT(Status__c) == &apos;Closed&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

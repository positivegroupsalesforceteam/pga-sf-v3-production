<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Complex_Scenario_Notification</fullName>
        <ccEmails>caf@positivelendingsolutions.com.au.invalid</ccEmails>
        <description>New Complex Scenario Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Complex_Scenario_Notification</template>
    </alerts>
    <alerts>
        <fullName>Scenario_Lost_Notification</fullName>
        <description>Scenario - Lost Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Introducer_Support__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Scenario_Lost_and_Went_Cold_Notification</template>
    </alerts>
    <alerts>
        <fullName>Scenario_New_Commercial_Notification</fullName>
        <description>Scenario - New Commercial Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ryan.mccallum@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stephen.hahn@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Scenario_New_Commercial_Scenario_Notification</template>
    </alerts>
    <alerts>
        <fullName>Scenario_Send_SLA_Scenario_Actioned_Warning_Notification</fullName>
        <description>Scenario - Send SLA Scenario Actioned Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Scenario_SLA_Scenario_Actioned_Warning</template>
    </alerts>
    <alerts>
        <fullName>Scenario_Send_SLA_With_Introducer_Warning_Notification</fullName>
        <description>Scenario - Send SLA With Introducer Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Scenario_SLA_With_Introducer_Warning</template>
    </alerts>
    <alerts>
        <fullName>Scenario_Send_SLA_With_Lender_Warning_Notification</fullName>
        <description>Scenario - Send SLA With Lender Warning Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SLA_Folder/Scenario_SLA_With_Lender_Warning</template>
    </alerts>
    <rules>
        <fullName>Notify New Complex Scenario Created</fullName>
        <actions>
            <name>New_Complex_Scenario_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Id != null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Scenario - Stage Lost</fullName>
        <actions>
            <name>Scenario_Lost_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISPICKVAL(Stage__c, &apos;Lost&apos;), ISPICKVAL(Lost_Reason__c, &apos;Went Cold&apos;),  $Setup.Workflow_Rule_Settings__c.Enable_Scenario_Workflows__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

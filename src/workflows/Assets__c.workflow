<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Address</fullName>
        <description>concatenate street, number, suburb to update address</description>
        <field>Address__c</field>
        <formula>IF(Street_Number__c == null || ISBLANK(Street_Number__c), Street_Name__c +&apos; &apos;+ Suburb__c, Street_Name__c +&apos; &apos;+ Street_Number__c +&apos; &apos;+ Suburb__c)</formula>
        <name>Update Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>update address</fullName>
        <actions>
            <name>Update_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Assets__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>fill address with street name, number and suburb.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

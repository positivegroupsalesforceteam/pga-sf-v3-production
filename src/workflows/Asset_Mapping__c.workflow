<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Asset_Name</fullName>
        <field>Name</field>
        <formula>Text(Asset_Type__c)</formula>
        <name>Update Asset Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Custome_Asset_Name</fullName>
        <field>Asset_Name__c</field>
        <formula>Assets__r.Name</formula>
        <name>Update Custome Asset Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Asset Fields</fullName>
        <actions>
            <name>Update_Asset_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Custome_Asset_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset_Mapping__c.Asset_Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Frontend_Submission_Send_to_Applicant_Notification</fullName>
        <ccEmails>ralph.sison@positivegroup.com.au</ccEmails>
        <ccEmails>mary.tapar@positivelendingsolutions.com.au</ccEmails>
        <description>Frontend Submission - Send to Applicant Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>rexie.david@positivelendingsolutions.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>info@positivelendingsolutions.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Frontend_Submission_Send_to_Applicant_Notification_Template</template>
    </alerts>
    <outboundMessages>
        <fullName>Frontend_Submission_Send_App</fullName>
        <apiVersion>47.0</apiVersion>
        <description>RDAVID tasks/26127403</description>
        <endpointUrl>http://dev-lightning-platform.com.au/api/salesforce/import-fe-submission</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api@positivelendingsolutions.com.au</integrationUser>
        <name>Frontend Submission - Send App</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Frontend Submission - Send App</fullName>
        <actions>
            <name>Frontend_Submission_Send_to_Applicant_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Frontend_Submission_Send_App</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>RDAVID tasks/26127403</description>
        <formula>AND (     $Setup.Workflow_Rule_Settings1__c.Enable_Frontend_Submission_Workflows__c,     ISCHANGED(Last_Send_App_Date_Time__c), 				NOT(ISBLANK(Last_Send_App_Date_Time__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

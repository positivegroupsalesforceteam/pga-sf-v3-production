@isTest
public class VSReferralTriggerTest {

    public static testMethod void testVsRef() {
    test.starttest();
    Opportunity opp = new Opportunity();
    opp.Name = 'test';
    opp.StageName = 'open';
    opp.CloseDate = date.Today();
    opp.VSR_Latest_Comment__c ='test';
    
    insert opp;
    
    VS_Referral__c vs = new VS_Referral__c();
    vs.Opportunity__c = opp.Id;
    vs.VSR_Latest_Comment__c = 'test';
    
    insert vs;
    test.stoptest();


    }
}
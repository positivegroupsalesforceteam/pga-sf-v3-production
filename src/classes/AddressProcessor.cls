public class AddressProcessor extends DefaultProcessor {
    String xmlFileName = 'Loankit_SF_Mapping';
    Map<String,String> abbreviations = new Map<String,String>{'Australian Capital Territory'=>'ACT','New South Wales'=>'NSW','Northern Territory'=>'NT','Queensland'=>'QLD','South Australia'=>'SA','Tasmania'=>'TAS',
                                        'Victoria'=>'VIC','Western Australia'=>'WA','Other (Outside Australia)'=>'Other'};
    public override boolean process() {
        if(!super.process()){
            return false;
        }
        List<LkRecord> lKRecordsList= XMLMappingAdapter.lkRecordsMap.get(LkConstant.Address);
        Integer i = 0;
        List<LkRecord> tempLKRecord = new List<LkRecord>(); 
        for(LKRecord rec : lKRecordsList) {
            i = i + 1;
            if(!rec.isProcessed) {  
               String postSettleCity = (String)rec.lkValuesMap.get('Post_Settle_Suburb__c');
               if(null != postSettleCity){
                   LKRecord postSettleLKRec= new LKRecord();
                   postSettleLKRec.sfObjectId = rec.sfObjectId ;
                   postSettleLKRec.sObjectRef = rec.sObjectRef;
                   postSettleLKRec.setMappingObject(rec.getMappingObject()) ;
                   Map<String,Object> lkValuesMap =  new Map<String,Object>();
                   String streetName = (String)rec.lkValuesMap.get('Post_Settle_Street_Name__c');
                   String streetNum = (String)rec.lkValuesMap.get('Post_Settle_Street_Number__c');
                   String nonStandardAdd = '';
                   if(streetName != null) {
                       nonStandardAdd = nonStandardAdd + streetName ;
                   }
                   if(streetNum != null) {
                       nonStandardAdd = nonStandardAdd+' , '+streetNum ;
                   }
                   lkValuesMap.put('street',streetName );   
                   lkValuesMap.put('street_number',streetNum );
                   String stateVal = (String)rec.lkValuesMap.get('Post_Settle_State__c');
                   if(stateVal != null && stateVal!='null'){
                       lkValuesMap.put('state',abbreviations.get(stateVal));
                   }
                   lkValuesMap.put('postcode',rec.lkValuesMap.get('Post_Settle_Postcode__c'));
                   lkValuesMap.put('country',rec.lkValuesMap.get('Post_Settle_Country__c'));
                   lkValuesMap.put('non_standard_address_line',nonStandardAdd);  
                   String addStandard = (String)rec.lkValuesMap.get('Post_Settle_Address_Type__c');
                   System.debug('post address type ========= '+addStandard);
                   if(addStandard!= null) {
                       addStandard= addStandard.replaceAll( '\\s+', '');
                       if(addStandard.equalsIgnoreCase('Non-Standard'))
                           addStandard = 'NonStandard';
                   }
                   System.debug('post address type ========= '+addStandard);
                   lkValuesMap.put('address_standard',addStandard);
                   lkValuesMap.put('address_type','PostSettlement Residential');
                   lkValuesMap.put('street_type',rec.lkValuesMap.get('Post_Settle_Street_Type__c'));
                   lkValuesMap.put('city',rec.lkValuesMap.get('Post_Settle_Suburb__c'));
                   postSettleLKRec.lkValuesMap  = lkValuesMap;
                   postSettleLKRec.setFieldToUpdate('LK_PostSettle_Address_Id__c');
                   tempLKRecord.add(postSettleLKRec);
               }
               
               String previousCity = (String)rec.lkValuesMap.get('Previous_City__c');
               if(null != previousCity){
                   LKRecord previousLKRec = new LKRecord();
                   previousLKRec.sfObjectId = rec.sfObjectId ;               
                   previousLKRec.sObjectRef = rec.sObjectRef;
                   previousLKRec.setMappingObject(rec.getMappingObject()) ;          
                   Map<String,Object> lkValuesMap =  new Map<String,Object>();                 
                   lkValuesMap.put('building_name',rec.lkValuesMap.get('Previous_Address__c'));
                   String streetName = (String)rec.lkValuesMap.get('Previous_Street_Name__c');
                   String streetNum = (String)rec.lkValuesMap.get('Previous_Street_Number__c');
                   String nonStandardAdd = '';
                   if(streetName != null) {
                       nonStandardAdd = nonStandardAdd + streetName ;
                   }
                   if(streetNum != null) {
                       nonStandardAdd = nonStandardAdd+' , '+streetNum ;
                   }
                   lkValuesMap.put('street',streetName );   
                   lkValuesMap.put('street_number',streetNum );
                   lkValuesMap.put('non_standard_address_line',nonStandardAdd); 
                    lkValuesMap.put('city',rec.lkValuesMap.get('Previous_City__c'));   
                    String stateVal = (String)rec.lkValuesMap.get('Previous_State__c');
                    if(stateVal != null && stateVal!='null'){
                        lkValuesMap.put('state',abbreviations.get(stateVal));
                    }
                    lkValuesMap.put('postcode',rec.lkValuesMap.get('Previous_Postcode__c'));
                    lkValuesMap.put('country',rec.lkValuesMap.get('Previous_Country__c'));  
                    lkValuesMap.put('start_date',rec.lkValuesMap.get('Previous_Address_Start_Date__c'));
                    lkValuesMap.put('end_date',rec.lkValuesMap.get('Previous_Address_End_Date__c'));
                    lkValuesMap.put('address_type','Previous Residential');
                    String addStandard = (String)rec.lkValuesMap.get('Previous_Address_Type__c');
                    System.debug('prev address type ========= '+addStandard);
                    if(addStandard!= null) {
                        addStandard= addStandard.replaceAll( '\\s+', '');
                        if(addStandard.equalsIgnoreCase('Non-Standard'))
                           addStandard = 'NonStandard';
                    }
                    System.debug('prev address type ========= '+addStandard);
                    lkValuesMap.put('address_standard',addStandard);  
                   previousLKRec.lkValuesMap  = lkValuesMap  ;
                   previousLKRec.setFieldToUpdate('LK_Previous_Address_Id__c');
                   
                  tempLKRecord.add(previousLKRec);
               }
               
               String currentCity = (String)rec.lkValuesMap.get('Current_Suburb__c');
               if(null != currentCity){
                   LKRecord currentLKRec = new LKRecord();
                   currentLKRec.sfObjectId = rec.sfObjectId ;                  
                   currentLKRec.sObjectRef = rec.sObjectRef;
                   currentLKRec.setMappingObject(rec.getMappingObject()) ;                 
                   Map<String,Object> lkValuesMap =  new Map<String,Object>();                 
                    lkValuesMap.put('street',rec.lkValuesMap.get('Current_Street_Name__c'));
                    lkValuesMap.put('street_number',rec.lkValuesMap.get('Current_Street_Number__c'));   
                    lkValuesMap.put('city',rec.lkValuesMap.get('Current_Suburb__c'));  
                    String stateVal = (String)rec.lkValuesMap.get('Current_State__c');
                    if(stateVal != null && stateVal!='null'){
                        lkValuesMap.put('state',abbreviations.get(stateVal));
                    } 
                    lkValuesMap.put('postcode',rec.lkValuesMap.get('Current_Postcode__c'));
                    lkValuesMap.put('country',rec.lkValuesMap.get('Current_Country__c'));   
                    String addStandard = (String)rec.lkValuesMap.get('Current_Address_Type__c');
                    System.debug('curr address type ========= '+addStandard);
                    if(addStandard!= null) {
                        addStandard= addStandard.replaceAll( '\\s+', '');
                        if(addStandard.equalsIgnoreCase('Non-Standard'))
                           addStandard = 'NonStandard';
                    }
                    System.debug('curr address type ========= '+addStandard);
                    lkValuesMap.put('address_standard',addStandard);
                    String address = rec.lkValuesMap.get('Current_Street_Name__c') + ',' +rec.lkValuesMap.get('Current_Street_Number__c') ;             
                    lkValuesMap.put('non_standard_address_line',address);   
                    lkValuesMap.put('start_date',rec.lkValuesMap.get('Current_Address_Start_Date__c'));
                    lkValuesMap.put('address_type','PreSettlement Residential');   
                    lkValuesMap.put('street_type',rec.lkValuesMap.get('Current_Street_Type__c'));      
                    lkValuesMap.put('street_number',rec.lkValuesMap.get('Current_Street_Number__c')); 
                    currentLKRec.lkValuesMap  = lkValuesMap  ;
                    currentLKRec.setFieldToUpdate('LK_Current_Address_Id__c');
                    tempLKRecord.add(currentLKRec);
               }       
            }
        }
        
         if(i > 0){
          lKRecordsList.remove(i-1);
         }
         lKRecordsList.addAll(tempLKRecord);
         return true;
    }
}
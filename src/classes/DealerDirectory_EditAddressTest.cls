@isTest
private class DealerDirectory_EditAddressTest {

    @testSetup static void setupData() {
		        
        //Create test data for Location
        Location__c loc = new Location__c(
            Name = 'Location test',
            Country__c = 'Australia',
            Postcode__c = '132455',
            State__c = 'Australian Capital Territory',
            State_Acr__c = 'ACT'
        );
        insert loc;
        
        //Create test data for Address
        Address__c address = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
        address.Location__c = loc.Id;
        Database.insert(address);

        //Create test data for Dealer Directory
        Dealer_Directory__c dd = new Dealer_Directory__c(
            Name = 'Test Dealer Directory',
            Address1__c = address.Id,
            Location__c = loc.Id
        );
        insert dd;
        
	}
    
    static testMethod void testDDAddress() {
    	Test.startTest();

            Dealer_Directory__c dd = [Select iD, Name From Dealer_Directory__c limit 1];
            PageReference pref = Page.DealerDirectory_EditAddress;
            Test.setCurrentPage(pref);
        	Test.setCurrentPageReference(pref); 
			System.currentPageReference().getParameters().put('id', dd.Id);
        
            ApexPages.StandardController st = new ApexPages.StandardController(dd);
            DealerDirectory_EditAddressCC cc = new DealerDirectory_EditAddressCC(st);
            cc.saveReferralCompany(); //Fail saving due to validation
            cc.resetAddressLocation();

    	Test.stopTest();
    }

}
public class NewRelationshipController {

    public Role__c role {get;set;}
    public Role__c creRole {get;set;}
    public Relationship__c rel {get;set;}
    public Case cse {get;set;}

    //public list <string> relationshipTypes {get;set;}
    public list <relationshipTypeWrapper> relTypeWrlist {get;set;}
    public ID curID {get;set;} //Role ID
    public ID cseID {get;set;} //Case ID
    public boolean hasErrors {get;set;}
    public boolean saveRelationshipSuccess {get;set;}
    public boolean validationSuccess {get;set;}

    public list <SelectOption> RoleRTs {get;set;} // Role RecordTypes
    //public list <SelectOption> RelAllowedRTs {get;set;} //Relationship RecordTypes
    public list <accWrapper> accOfApplist {get;set;}
    public static Map<String,Schema.RecordTypeInfo> relRTMapByName = Schema.SObjectType.Relationship__c.getRecordTypeInfosByName();
    public Map <string, string> relRTNameByIdMap {get;set;}

    public NewRelationshipController(){
        curID = Apexpages.currentpage().getparameters().get('id');
        if (curID != null){
            role = [Select Id, Name, Account__c, Account__r.Name, Case__c From Role__c Where Id = : curID];
            cseID = role.Case__c;
            retrieveCase();
        }
        validationSuccess = hasErrors = saveRelationshipSuccess = hasErrors = false;

        setBusinessRelationshipTypes();
        //Retrieve Relationship Record Type Names
        relRTNameByIdMap = new map <string,string>();
        for (String keyname : relRTMapByName.keySet()){
            string rtkey = relRTMapByName.get(keyname).getRecordTypeId();
            if (!relRTNameByIdMap.containskey(rtkey)){
                relRTNameByIdMap.put(rtKey, keyname);
            }
        }
        rel = new Relationship__c(
            RecordTypeId = relRTMapByName.get('Business Relationship').getRecordTypeId()
        );
    }

    private void retrieveCase(){
        string cseQuery = 'Select Id, CaseNumber, RecordTypeId, RecordType.Name, Application_Name__c, ';
        cseQuery+= 'Application_Name__r.Name, Application_Name__r.RecordTypeId, Application_Name__r.RecordType.Name, ';
        string roleQUery = 'Select Id, Name, RecordTypeId, RecordType.Name, Case__c, Account__c, Account__r.Name, Account__r.FirstName, Account__r.LastName, Account__r.PersonEmail, Account__r.PersonBirthDate, Account__r.PersonMobilePhone, Account__r.isPersonAccount, Account__r.ABN__c, Account__r.Full_Address__c , Application__c ';
        roleQUery+= ' From Roles__r Order By Name ASC'; //Related Roles
        roleQUery= '(' + roleQUery + ')';
        cseQuery+= roleQUery + ' From Case Where Id = : cseID limit 1';
        cse = Database.query(cseQuery);

        accOfApplist = new list <accWRapper>();
        set <Id> roleAccId = new set <Id>();
        //Retrieving Accounts on existing Roles of Case/Application
        if (!cse.Roles__r.isEmpty()){
            for (Role__c crole : cse.Roles__r){
                if (crole.Account__c != null){
                    if (!roleAccId.contains(crole.Account__c)){ //Check for duplicate Accounts assigned in Roles
                        if (crole.Account__r.isPersonAccount){
                            accWrapper acWr = new accWrapper(
                                new Account(Id = crole.Account__c,
                                    PersonEmail = crole.Account__r.PersonEmail,
                                    PersonBirthDate = crole.Account__r.PersonBirthDate,
                                    PersonMobilePhone = crole.Account__r.PersonMobilePhone,
                                    FirstName = crole.Account__r.FirstName,
                                    LastName = crole.Account__r.LastName
                                )
                            );
                            acWr.accName = crole.Account__r.Name;
                            acWr.isPerson = true;
                            acWr.recTypeName = crole.Account__r.Name;
                            acWr.fullAddress = crole.Account__r.Full_Address__c;
                            accOfApplist.add(acWr);
                        }
                        else{
                            accWrapper acWr = new accWrapper(
                                new Account(Id = crole.Account__c, 
                                    Name = crole.Account__r.Name,
                                    ABN__c = crole.Account__r.ABN__c
                                )
                            );  
                            acWr.accName = crole.Account__r.Name;
                            acWr.recTypeName = crole.Account__r.Name;
                            acWr.fullAddress = crole.Account__r.Full_Address__c;
                            accOfApplist.add(acWr);
                        }
                        roleAccId.add(crole.Account__c);
                    }
                }
            }
        }
        if (accOfApplist.size() == 1){
            accOfApplist[0].isSelected = true;
        }
    }

    private void setBusinessRelationshipTypes(){
        relTypeWrlist = new list <relationshipTypeWrapper>();
        relTypeWrlist.add(new relationshipTypeWrapper('Primary Beneficiary'));
        relTypeWrlist.add(new relationshipTypeWrapper('Director'));
        relTypeWrlist.add(new relationshipTypeWrapper('Major Shareholder'));
        relTypeWrlist.add(new relationshipTypeWrapper('Partner'));
        relTypeWrlist.add(new relationshipTypeWrapper('Trustee'));
        relTypeWrlist.add(new relationshipTypeWrapper('Unit Holder'));
    }

    public list <SelectOption> getRelAllowedRTs(){
        list <SelectOption> options = new list <SelectOption>();
        options.add(new SelectOption(
                        relRTMapByName.get('Business Relationship').getRecordTypeId(),
                        'Business Relationship'
                    ));
        options.add(new SelectOption(
                        relRTMapByName.get('Spouse').getRecordTypeId(),
                        'Spouse'
                    ));        
        return options;
    }

    public pageReference defaultRelationshipType(){
        if (rel.RecordTypeId == relRTMapByName.get('Spouse').getRecordTypeId()){
            rel.Relationship_Type__c = 'Spouse';
        }
        else rel.Relationship_Type__c = null;
        rel.Start_Date__c = null;
        return null;
    }

    public pageReference validateAddRelationship(){
        integer roleselCtr = 0;
        validationSuccess = true;
        for (accWrapper acWR : accOfApplist){
            if (acWr.isSelected) roleselCtr++;
        }
        if (roleselCtr == 0){
            validationSuccess = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select Account to relate'));
            hasErrors = true;
        }
        if (rel.RecordTypeId != relRTMapByName.get('Spouse').getRecordTypeId()){
            if (rel.Start_Date__c == null){
                validationSuccess = false;
                rel.Start_Date__c.addError('Start Date is required');
                hasErrors = true;
            }
            integer relTypeCtr = 0;
            for (relationshipTypeWrapper relTypeWr : relTypeWrlist){
                if (relTypeWR.isSelected) relTypeCtr++;
            }
            if (relTypeCtr == 0){
                validationSuccess = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select Relationship Type'));
                hasErrors = true;
            }
        }
        if (validationSuccess) hasErrors = false;
        return null;
    }

    public pageReference saveRelationships(){
        Savepoint sp = Database.setSavepoint();
        try{
            //system.debug('@@relationshipTypes:'+relationshipTypes);
            list <Relationship__c> rellist = new list <Relationship__c>();
            for (accWrapper accWR : accOfApplist){
                if (accWr.isSelected){
                    //Set Master Account for Relationship
                    rel.Master_Account__c = accWr.acc.Id;
                    break;
                }
            }
            rel.Associated_Account__c = role.Account__c;
            if (rel.RecordTypeId == relRTMapByName.get('Spouse').getRecordTypeId()){
                rellist.add(rel);
            }
            else{ //Business Relationship RT
                //Create multiple Relationships base on selected Relationship Type
                for (relationshipTypeWrapper reltypeWR : relTypeWrlist){
                    if (reltypeWR.isSelected){
                        Relationship__c tempRel = new Relationship__c(
                            Master_Account__c = rel.Master_Account__c,
                            Associated_Account__c = role.Account__c,
                            RecordTypeId = rel.RecordTypeId,
                            Start_Date__c = rel.Start_Date__c
                        );
                        tempRel.Relationship_Type__c = reltypeWR.relType;
                        rellist.add(tempRel);
                    }
                }
            }
            upsert rellist;
            saveRelationshipSuccess = true;
        }
        catch(Exception e){
            saveRelationshipSuccess = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
            hasErrors = true;
            Database.rollback( sp );
        }
        return null;
    }

    public class relationshipTypeWrapper{
        public boolean isSelected {get;set;}
        public string relType {get;set;}
        public relationshipTypeWrapper(String relType1){
            relType = relType1;
            isSelected = false;
        }
    }


    public class accWrapper{
        public boolean isPerson {get;set;}
        public string accName {get;set;}
        public string recTypeName {get;set;}
        public string fullAddress {get;set;}
        public boolean isSelected {get;set;}
        public Account acc {get;set;}
        public accWrapper(Account acc1){
            acc = acc1;
            isPerson = isSelected = false;
            accName = fullAddress = recTypeName = '';
        }
    }


}
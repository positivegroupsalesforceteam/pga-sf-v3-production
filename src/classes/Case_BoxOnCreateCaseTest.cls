/*
	@Description: Test class of Box Creation on Case Create
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	9/14/18 JBACULOD Created
*/
@isTest
private class Case_BoxOnCreateCaseTest {

    @testsetup static void setup(){

        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = Trigger_Settings1__c.getOrgDefaults();
        trigSet.Enable_Triggers__c = false;
        trigSet.Enable_Case_Box_Partition_Auto_Create__c = true;
        upsert trigSet Trigger_Settings1__c.Id;

    }

    static testmethod void testcaseBoxFolderCreate(){

        Trigger_Settings1__c trigSet = [Select Id, Enable_Triggers__c, Enable_Case_Box_Partition_Auto_Create__c From Trigger_Settings1__c];

        Test.startTest();
            Id cseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_P_CONS_AF).getRecordTypeId(); //POS: Consumer - Asset Finance
            list <Case> cselistToCreate = new list <Case>();
            for (Integer i=0; i<1; i++){
                Case cse = new Case(
                    Status = 'New',
                    Stage__c = 'Open',
                    Channel__c = 'PLS',
                    Partition__c = 'Positive',
                    RecordTypeId = cseRTId
                );
                cselistToCreate.add(cse);
            }
            insert cselistToCreate;

            list <ID> cseListIDs = new list <ID>();
            for (Case cse : cselistToCreate){
                cseListIDs.add(cse.Id);
            }

            Case_BoxOnCreateCase.test_objRecordFolderID = '12334555';
            Case_BoxOnCreateCase.caseBoxFolderCreate(cseListIDs);
            Case_BoxOnCreateCase cbox = new Case_BoxOnCreateCase(cseListIDs);
            cbox.createBoxFolder(cseListIDs);
            ID jobID = System.enqueueJob(cbox);
            system.debug('@@JobID:'+jobID);


            for (Case cse : [Select Id, Box_Folder_ID__c From Case Where Id in : cseListIDs]){
                system.assert(cse.Box_Folder_ID__c != null);
                system.assertEquals(cse.Box_Folder_ID__c, Case_BoxOnCreateCase.test_objRecordFolderID);
            }

        Test.stopTest();

    }

}
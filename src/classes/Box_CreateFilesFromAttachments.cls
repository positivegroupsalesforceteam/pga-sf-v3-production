/** 
* @FileName: Box_CreateFilesFromAttachments
* @Description: allows creation of Box File from Attachments of a specific record
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 9/18/18 JBACULOD Created Class, createFileInBox
* 1.1 9/19/18 JBACULOD Updated
* 1.2 9/24/18 JBACULOD Added Doc Upload History, Existing File in BOx handling
**/ 
global class Box_CreateFilesFromAttachments {

    private static map <Id, Attachment> delAttMap = new map <Id, Attachment>();
    private static string retmessage; //returning message
    private static list <Attachment> attlist = new list <Attachment>();
    private static Support_Request__c supReq;
    /*private string cseID;
    private string sobjID;
    public Box_CreateFilesFromAttachments(String caseID, String sobjectID){
        cseID = caseID;
        sobjID = sobjectID;
    } 

    public void execute(QueueableContext context){
        system.debug('@@execute');
        system.debug('@@cseID:'+cseID);
        system.debug('@@sobjID:'+sobjID);
        Box_CreateFilesFromAttachments.createBoxFiles(cseID, sobjID);
        system.debug('@@deleteAtts:'+deleteAtts);
        if (deleteAtts) delete attlist; //Delete Attachments in SF 
    } */

    //@InvocableMethod(label='Create Box Files from Attachments' description='create Box Files from Attachments')
    webservice static string createBoxFilesFromAttachment(string caseID, string sobjectID){
        /*system.debug('@@caseID:'+caseID);
        system.debug('@@sobjectID:'+sobjectID);
        Box_CreateFilesFromAttachments boxcfilefromAtt = new Box_CreateFilesFromAttachments(caseID, sobjectID);
        ID jobID = System.enqueueJob(boxcfilefromAtt); */
        Box_CreateFilesFromAttachments.createBoxFiles(caseID, sobjectID);
        system.debug('@@delAttMap.size()'+delAttMap.size());
        system.debug('@@attlist.size()'+attlist.size());
        if (delAttMap.size() > 0){
            delete delAttMap.values(); //Delete Attachments succesffuly transferred to Bxo from SF 
            update supReq; //Update current Parent Sobject Record (currently Support Request)
            retmessage = 'SUCCESS';
        }
        else retmessage = 'File Transfer Failed';
        return retmessage;
    }

    //sobjID : ParentId of SF Attachments
    public static void createBoxFiles(String cseID, String sobjID){
        system.debug('@@cseID:'+cseID);
        system.debug('@@sobjID:'+sobjID);
        system.debug('@@createBox');
        if (TriggerFactory.trigset.Enable_Attachment_Create_File_in_Box__c){

            if (ID.valueof(sobjID).getSObjectType() == Schema.Support_Request__c.SobjectType){
                supReq = [Select Id, Name, Case_Number__r.CaseNumber, Document_Upload_History__c, Files_Moved_to_Box__c From Support_Request__c Where Id = : sobjID];   
                system.debug('@@supReq:'+supReq);
            }

            //Retrieve all attachments of sobjID
            attlist = [Select Id, ParentId, Body, Name From Attachment Where ParentId = : sobjID];
            if (attlist.size() > 0){

                box.Toolkit boxToolkit = new box.Toolkit();
                String boxCseFolderID = boxToolkit.getFolderIdByRecordId(cseID);
                if (Test.isRunningTest()) boxCseFolderID = '123456';
                system.debug('@@boxCseFolderID:'+boxCseFolderID);

                if (boxCseFolderID != null && boxCseFolderID != ''){
                    DateTime dnow = system.now();
		            string strDnow = dnow.format();
                    for (Attachment att : attlist){
                        boolean withBoxErrors = false;
                        //Create attachment file as Box file in Case Box folder
                        String boxfile = boxtoolkit.createFileFromAttachment(att,att.Name,boxCseFolderID,null);
                        system.debug('@@boxFile:'+boxFile);
                        string fileHistory = '';
                        if (boxFile == null || boxFile == ''){ //With Errors
                            system.debug('@@boxToolkit.mostRecentError:'+boxToolkit.mostRecentError);
                            //break;
                            fileHistory = '- ' + att.Name + ' file failed to upload to Case ' + supReq.Case_Number__r.CaseNumber + ' at ' + strDnow;
                            if (String.valueof(boxToolkit.mostRecentError).contains('Conflict')){ //File already exists in Box, create another file w/ _1 in filename
                                string attName = String.valueof(att.Name).substringBeforeLast('.');
                                string attFileExt = String.valueof(att.Name).substringAfterLast('.');
                                string newAttName = attName + '_1.' + attFileExt;
                                boxfile = boxtoolkit.createFileFromAttachment(att,attName + '_1.'+attFileExt,boxCseFolderID,null);
                                fileHistory = '- ' + newAttName + ' file uploaded to Case ' + supReq.Case_Number__r.CaseNumber + ' successfully at ' + strDnow;
                            }
                        }
                        else{
                            fileHistory = '- ' + att.Name + ' file uploaded to Case ' + supReq.Case_Number__r.CaseNumber + ' successfully at ' + strDnow;
                        }
                        if (supReq.Document_Upload_History__c != '' && supReq.Document_Upload_History__c != null) supReq.Document_Upload_History__c+= '\n' + fileHistory;
                        else supReq.Document_Upload_History__c = fileHistory;
                        delAttMap.put(att.Id, att); //Identify which Attachments to be deleted
                    }
                }

                if (delAttMap.size() > 0) supReq.Files_Moved_to_Box__c = true;

                if(!Test.IsRunningTest()) boxToolkit.commitChanges();

            }
            else retmessage = 'No attachments found';

        }

    }

}
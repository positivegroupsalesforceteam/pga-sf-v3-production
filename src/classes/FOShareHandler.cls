/** 
* @FileName: FOShareHandler
* @Description: Trigger Handler for the FO_Share__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/23/2018 RDAVID [SD-77] Created Classes for Rollup Mapping Updates
* 1.1 22/10/18 RDAVID: Optimization
* 1.2 11/27/18 JBACULOD: Fixed To Be Paid Out issue
* 1.3 03/13/19 JBACULOD: Added retry due to Unable_to_lock_row issue
* 1.4 22/07/19 RDAVID: extra/task25350275-SeparateCommercialConsumerIncomeOnApplicationRole
**/ 

public without sharing class FOShareHandler implements ITrigger {	

	private Set<Id> roleIds = new Set<Id>(); //Parent Role Ids

	//SD-77 SF - NM - FOShare Rollup in Role/Application
	private Map<String,List<String>> foshareToRoleMapping = new Map<String,List<String>>(); //FOShare to Role Field Mapping
	// private Map<String,Asset_to_Role_Rollup_Field_Mapping__c> foshareToRoleCS = Asset_to_Role_Rollup_Field_Mapping__c.getAll();
	private Map<Id,SObject> roleToUpdateMap = new Map<Id,SObject>(); //Role to update rollups container

	private List<FO_Share__c> newFOShareList = new List<FO_Share__c>();

	private Schema.DescribeSObjectResult roleObjectDescribe =  Schema.getGlobalDescribe().get('Role__c').getDescribe();
	private Schema.DescribeSObjectResult foshareObjectDescribe = Schema.getGlobalDescribe().get('FO_Share__c').getDescribe();

	private Map<Id,Schema.RecordTypeInfo> roleShareRTIDMap = roleObjectDescribe.getRecordTypeInfosById();
	private Map<Id,Schema.RecordTypeInfo> foShareRTIDMap = foshareObjectDescribe.getRecordTypeInfosById();
	private List<Role__c> parentRoleList = new List<Role__c>();

	private Set<String> roleObjectFields;
	private Set<String> foshareObjectFields;

	// private static List<FO_Rollup_Mapping__mdt> foRollupMapList = new List<FO_Rollup_Mapping__mdt>();

	// Constructor
	public FOShareHandler(){
		//Initialize value of newRoleList
		newFOShareList = (Trigger.IsInsert || Trigger.isUpdate) ? (List<FO_Share__c>) Trigger.new : (Trigger.IsDelete) ? (List<FO_Share__c>) Trigger.old : null;

		//Schema Describes 
		if(TriggerFactory.trigset.Enable_FOShare_Rollup_to_Role__c && roleObjectFields == NULL){
			
			roleObjectFields = roleObjectDescribe.fields.getMap().keySet();
			foshareObjectFields = foshareObjectDescribe.fields.getMap().keySet();
			
			System.debug('roleObjectFields - '+roleObjectFields); System.debug('foshareObjectFields - '+foshareObjectFields);
			// if(foRollupMapList.size() == 0)
			// foRollupMapList = [SELECT Id,Label, Target_RecordType__c, Active__c, IsLead__c, FO_Share_RecordType__c, Source_RecordType__c,Source_Type__c,Source_Field_API_Name__c,Target_Field_API_Name__c,Lead_Source_Type__c FROM FO_Rollup_Mapping__mdt WHERE Active__c = TRUE];
		}
	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
		if(!Trigger.isDelete){
			//1.4 22/07/19 RDAVID: extra/task25350275-SeparateCommercialConsumerIncomeOnApplicationRole
			FOShareGateway.setRoleLookup(newFOShareList,foShareRTIDMap);
		}
	}
	
	public void bulkAfter(){
		
		Set<String> newFOsRTString = new Set<String>();
		Set<String> newFORTString = new Set<String>();
		Set<String> newFOTypeString = new Set<String>();
		
		for(FO_Share__c foShare : newFOShareList){
			if(foShare.Role__c != NULL)
			roleIds.add(FOShare.Role__c);

			if(TriggerFactory.trigset.Enable_FOShare_Rollup_to_Role__c){
				String foRecordTypeName = foShareRTIDMap.get(foShare.RecordTypeId).getname();
				System.debug('foRecordTypeName = '+foRecordTypeName);
				if(!newFOsRTString.contains(foRecordTypeName)){
					newFOsRTString.add(foRecordTypeName);
				}
			}
			// if(foShare.FO_Record_Type__c != NULL)
			// newFORTString.add(foShare.FO_Record_Type__c);
		}
		
		System.debug('newFOsRTString = ' + newFOsRTString);
		
		if(roleIds.size() > 0){
			String selectRole = ' SELECT Id, RecordTypeId, RecordType.Name'; //Id, RecordTypeId, 
			/*Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Role__c.fields.getMap();
			for (String fieldName: schemaFieldMap.keySet()) {
				selectRole+= ', ' + fieldName;
			} */
			String innerSelectFOShare = ', ( SELECT Id, RecordTypeId, RecordType.Name, Type__c, FO_Record_Type__c, h_Employment_Situation__c , h_Monthly_Additional_Income__c , h_Monthly_Base_Income__c ,  Role_Share_Amount__c, Clearing_from_Loan__c ';
			//Additional innerQuery here
			String innerWhere = ' WHERE RecordType.Name IN: newFOsRTString '; //'AND Clearing_from_Loan__c = FALSE AND Being_Refinanced__c = FALSE ';
			String innerFrom = ' FROM FO_Share__r '+innerWhere+')';
			String fromRole = ' FROM Role__c WHERE Id IN: roleIds ';
			System.debug('QUERY  = ' + selectRole+innerSelectFOShare+innerFrom+fromRole);
			//Main Query for Parent Role, As much as possible maintain one query to Role, used inner query
			if(parentRoleList.size() == 0) parentRoleList = Database.query(selectRole+innerSelectFOShare+innerFrom+fromRole);

			for(SObject roleSobject : parentRoleList){
				System.debug('roleSobject = > '+roleSobject);
				//Loop through inner Query
				for(sObject childrec : roleSobject.getSObjects('FO_Share__r')){
					String foRT = (childrec.get('FO_Record_Type__c')!=NULL) ? String.valueOf(childrec.get('FO_Record_Type__c')):NULL;
					if(foRT != NULL)
					newFORTString.add(foRT);
				}
			}
			System.debug('newFORTString = ' + newFORTString);
		}		
		
		if(!newFOsRTString.isEmpty()) 
		foshareToRoleMapping = RollupUtility.getChildToParentMapping(newFOsRTString, newFORTString, roleObjectFields, foshareObjectFields);
		System.debug('@@@@foshareToRoleMappingkeyset = '+foshareToRoleMapping.keyset()); 
		System.debug('@@@@foshareToRoleMappingvalues = '+foshareToRoleMapping.values());
		if(!foshareToRoleMapping.isEmpty())
		roleToUpdateMap = RollupUtility.rollupAutomation(parentRoleList, foshareToRoleMapping, foShareRTIDMap);
	}
		
	public void beforeInsert(SObject so){
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){

	}
	
	public void afterUpdate(SObject oldSo, SObject so){

	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		// Update parent Role Rollup Fields
		if (!roleToUpdateMap.isEmpty()){
			if (TriggerFactory.trigset.Asynchronous_Rollup__c){
				FOShareGateway fosgate = new FOShareGateway(roleToUpdateMap.values());
				ID jobID = System.enqueueJob(fosgate);
			}
			else{ 
				try{
					Database.update(roleToUpdateMap.values());
				}
				catch (Exception e){
					Database.update(roleToUpdateMap.values()); //Retry due to Unable_to_lock_Row issue
				}
			}
		}
		// Update parent Role Rollup Fields
		/*if (!roleToUpdateMap.isEmpty()){
			system.debug('@@roleToUpdateMap:'+roleToUpdateMap);
			List<Role__c> roleToUpdateList = (List<Role__c>) roleToUpdateMap.values();
			Database.update(roleToUpdateList);
		} */
	}
}
/** 
* @FileName: ExpenseGateway
* @Description: Provides finder methods for accessing data in the Expense object.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/14/18 RDAVID Created class, Added getRoleIds
**/ 

public without sharing class ExpenseGateway{

    /**
    * @Description: Returns a set of Parent Role Id associated to Expense record. 
    * @Arguments:   List<Expense1__c> newExpenseList - trigger.new
    * @Returns:     Set<Id> getRoleIds - parent Role ids
    */
    
    public static Set<Id> getRoleIds(List<Expense1__c> newExpenseList){
        Set<Id> roleIdsSet = new Set<Id>();

        for(Expense1__c expense : newExpenseList){
            //CHANGE123 roleIdsSet.add(expense.Role__c);
        }
        return roleIdsSet;
    }
}
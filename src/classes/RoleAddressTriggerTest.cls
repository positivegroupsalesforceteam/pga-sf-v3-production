/** 
* @FileName: RoleAddressTriggerTest
* @Description: Test class for Role Address Trigger 
* @Copyright: Positive (c) 2018 
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/10/18 RDAVID Created class
**/ 
@isTest 
public class RoleAddressTriggerTest{

    @testSetup static void setupData() {

        //Turn On Trigger 
        Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
        triggerSettings.Enable_Triggers__c = true;
        triggerSettings.Enable_Role_Address_Trigger__c = true;
        triggerSettings.Enable_Role_Address_Populate_Address__c = true;
        triggerSettings.Enable_Address_Populate_Location__c = true;
        triggerSettings.Enable_Role_Address_Check_Residency__c = true;
        triggerSettings.Enable_Role_Address_Toggle_Current__c = true;

        upsert triggerSettings Trigger_Settings1__c.Id;
    }
    //SD-31 SF - NM - Role Address Trigger to populate Address lookups - Trigger in Role_Address__c object to populate Parent Account and Parent Role Address__c field.
    static testMethod void testPopulateAddressLookups(){

        //Create Test Account
        List<Account> accList = TestDataFactory.createGenericPersonAccount('First Name','TestLastName',CommonConstants.PACC_RT_I, 2);
        Database.insert(accList);
        accList = [SELECT Id, IsPersonAccount FROM Account WHERE Name LIKE '%TestLastName%'];
        System.assertEquals(accList[0].IsPersonAccount,true);
        System.assertEquals(accList.size(),2);

        //Create Test Case
        List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
        Database.insert(caseList);

        //Create Test Application
        List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
        Database.insert(appList);
        
        Test.StartTest();
            List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
            roleList[0].Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT;
            roleList[0].Phone__c = '09165856439';
            roleList[0].Mobile_Phone__c = '09165856439';
            roleList[0].Email__c = 'test@email.com';
            Database.insert(roleList);

            roleList = [SELECT Id,Address__c,Account__c,Account__r.Address__c FROM Role__c WHERE Application__c =: appList[0].Id];

            System.assertEquals(roleList.size(),1);
            System.assertEquals(roleList[0].Address__c,NULL);
            System.assertEquals(roleList[0].Account__r.Address__c,NULL);
            System.assertNotEquals(roleList[0].Account__c,NULL);

            //Create test data for Location
            Location__c loc = new Location__c(
                Name = 'Location test',
                Country__c = 'Australia',
                Postcode__c = '132455',
                State__c = 'Australian Capital Territory',
                State_Acr__c = 'ACT'
            );
            insert loc;

            //Create test data for Address
            Address__c address = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
            address.Location__c = loc.Id;
            Database.insert(address);
            Address__c address2 = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
            address2.Location__c = loc.Id;
            Database.insert(address2);

            Role_Address__c roleAddress = TestDataFactory.createGenericRoleAddress(accList[0].Id, true, roleList[0].Id, address.Id);
            Database.insert(roleAddress);

            //TriggerFactory.curSoType = null; //reset recursion variable
            roleAddress.Address__c = address2.Id;
            update roleAddress;

            roleList = [SELECT Id,Address__c,Account__c,Account__r.Address__c FROM Role__c WHERE Application__c =: appList[0].Id];

        Test.StopTest();
    }   

    // SD-45 SF - NM - Toggle Active Address field to ensure one "Current" Address on Role
    static testMethod void testToggleActive(){

        //Create Test Account
        List<Account> accList = TestDataFactory.createGenericPersonAccount('First Name','TestLastName',CommonConstants.PACC_RT_I, 2);
        Database.insert(accList);
        accList = [SELECT Id, IsPersonAccount FROM Account WHERE Name LIKE '%TestLastName%'];
        System.assertEquals(accList[0].IsPersonAccount,true);
        System.assertEquals(accList.size(),2);

        //Create Test Case
        List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
        Database.insert(caseList);

        //Create Test Application
        List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
        Database.insert(appList);
        
        Test.StartTest();
            List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  2);
            roleList[0].Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT;
            roleList[0].Phone__c = '09165856439';
            roleList[0].Mobile_Phone__c = '09165856439';
            roleList[0].Email__c = 'test@email.com';
            roleList[0].Residential_Situation__c = 'Boarder';
            roleList[1].Residential_Situation__c = 'Employer Provided';
            Database.insert(roleList);

            roleList = [SELECT Id,Address__c,Account__c,Account__r.Address__c FROM Role__c WHERE Application__c =: appList[0].Id];

            System.assertEquals(roleList.size(),2);
            System.assertEquals(roleList[0].Address__c,NULL);
            System.assertEquals(roleList[0].Account__r.Address__c,NULL);
            System.assertNotEquals(roleList[0].Account__c,NULL);

            List<Address__c> addressList = new List<Address__c>();
            Address__c address = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
            Address__c address2 = TestDataFactory.createGenericAddress(false, 'Australia', '1234', 'South Australia', 'Lane St', '', 'NEW CANBERRA', false);
            Address__c address3 = TestDataFactory.createGenericAddress(false, 'Australia', '1235', 'Victoria', 'Square St', '', 'WEST BEACH', false);
            addressList.add(address);
            addressList.add(address2);
            addressList.add(address3);
            Database.insert(addressList);
            //Insert Current
            List<Role_Address__c> roleAddressList = new List<Role_Address__c>();
            Role_Address__c roleAddress = TestDataFactory.createGenericRoleAddress(accList[0].Id, true, roleList[0].Id, address.Id);
            roleAddress.Start_Date__c = system.today();
            Role_Address__c roleAddress2 = TestDataFactory.createGenericRoleAddress(accList[0].Id, true, roleList[0].Id, address2.Id);
            roleAddress2.Start_Date__c = system.today();
            Role_Address__c roleAddress3 = TestDataFactory.createGenericRoleAddress(accList[0].Id, true, roleList[0].Id, address3.Id);
            roleAddress3.Start_Date__c = system.today();
            roleAddressList.add(roleAddress);
            roleAddressList.add(roleAddress2);
            roleAddressList.add(roleAddress3);
            Database.insert(roleAddressList);

            roleAddressList = [SELECT Id,Active_Address__c FROM Role_Address__c WHERE Role__c =: roleList[0].Id];
            Integer currentCount = 0;
            Id currentId;
            for(Role_Address__c roleAdd : roleAddressList){
                if(roleAdd.Active_Address__c == 'Current'){
                    currentCount++;
                    currentId = roleAdd.Id;
                    roleAdd.Active_Address__c = 'Previous';
                }
            }
            
            //System.assertEquals(currentCount,1);
            //System.assertEquals(currentId,roleAddress.Id);
            //Insert Previous
            roleAddressList = new List<Role_Address__c>();
            roleAddress = TestDataFactory.createGenericRoleAddress(accList[0].Id, false, roleList[1].Id, address.Id);
            roleAddress2 = TestDataFactory.createGenericRoleAddress(accList[0].Id, false, roleList[1].Id, address2.Id);
            roleAddress3 = TestDataFactory.createGenericRoleAddress(accList[0].Id, false, roleList[1].Id, address3.Id);
            roleAddressList.add(roleAddress);
            roleAddressList.add(roleAddress2);
            roleAddressList.add(roleAddress3);
            Database.insert(roleAddressList);

            roleAddressList = [SELECT Id,Active_Address__c FROM Role_Address__c WHERE Role__c =: roleList[1].Id];
            System.assertEquals(roleAddressList.size(),3);
            Id currentAddress;
            currentCount = 0;
            for(Role_Address__c roleAdd : roleAddressList){
                if(roleAdd.Active_Address__c == 'Current'){
                    currentAddress = roleAdd.Id;
                    roleAdd.Active_Address__c = 'Previous';
                    currentCount++;
                }
            }
            //System.assertEquals(currentCount,1);
            Database.update(roleAddressList); //After Update All Role Address = Previous

            currentCount = 0;
            roleAddressList = [SELECT Id,Active_Address__c FROM Role_Address__c WHERE Role__c =: roleList[1].Id];
            for(Role_Address__c roleAdd : roleAddressList){
                if(roleAdd.Active_Address__c == 'Current'){
                    currentCount++;
                    currentAddress = roleAdd.Id;
                }
            }
            //System.assertEquals(currentCount,0); //Make sure No Current Role Address

            roleAddressList = [SELECT Id,Active_Address__c FROM Role_Address__c WHERE Role__c =: roleList[1].Id AND Id !=: currentAddress];
            roleAddressList[0].Active_Address__c = 'Current';
            Database.update(roleAddressList); //Update One Role Address to Current

            roleAddressList = [SELECT Id,Active_Address__c FROM Role_Address__c WHERE Role__c =: roleList[1].Id AND Active_Address__c =: 'Current'];
            //System.assertNotEquals(roleAddressList[0].Id,currentAddress); //Make sure Current Role Address has been changed after Update

            roleAddressList = [SELECT Id,Active_Address__c FROM Role_Address__c];
            Database.delete(roleAddressList); //Delete all existing Role Address

            roleAddressList = [SELECT Id,Active_Address__c FROM Role_Address__c];
            //System.assertEquals(roleAddressList.size(),0); //Make sure All Role Address has been deleted

            roleAddress = TestDataFactory.createGenericRoleAddress(accList[0].Id, true, roleList[1].Id, address.Id);
            Database.insert(roleAddress);

            roleAddress2 = TestDataFactory.createGenericRoleAddress(accList[0].Id, true, roleList[1].Id, address2.Id);
            Database.insert(roleAddress2);

            currentCount = 0;
            roleAddressList = [SELECT Id,Active_Address__c FROM Role_Address__c];

            for(Role_Address__c roleAdd : roleAddressList){
                if(roleAdd.Active_Address__c == 'Current'){
                    currentCount++;
                    currentAddress = roleAdd.Id;
                }
            }
            //System.assertEquals(currentAddress,roleAddress2.Id); //Make sure Current Address is toggled
            //System.assertEquals(currentCount,1); //And only one Current Address in each Role.

            ContinuousUtility cutil = new ContinuousUtility();
            cutil.fakeMethod();

        Test.StopTest();
    }   
}
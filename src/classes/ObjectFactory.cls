/***** Factory class which gives an instance of NodeProcessor *****/
/***** Based on Factory Pattern and Singleton Pattern concepts *****/
public class ObjectFactory {
    private static Map<String,NodeProcessor> instanceMap = new Map<String,NodeProcessor>();
    
    /*
    ** To get the instance of a class(child of NodeProcessor) at runtime
    ** Returns the existing instance if available, create a new one otherwise
    */
    public static NodeProcessor getInstance(string className) {
        NodeProcessor np = instanceMap.get(className);
        if(np!=null) {
            return np;
        }
        Type t = Type.forName(className);
        if(t == null && className != null) {
            // Attempt to get the type again with the namespace explicitly set to blank
            t = Type.forName('', className);
        }
        if(t == null) {
            System.debug(LoggingLevel.Error, 'Failed to find type for ['+className+']');
            return null;
        }
        // Create an instance to confirm the type
        object testInstance = t.newInstance();
        if(!(testInstance instanceOf NodeProcessor)) {
            return null;
        }
        NodeProcessor classInstance = (NodeProcessor)testInstance;
        instanceMap.put(className,classInstance);
        return classInstance;
    }
}
@isTest
public class LoanDocumentRequestFormCaseTest {
	
	@testsetup static void setup(){

		//create test data for Account
        Account acc = new Account(
                Name = 'test'
            );
        insert acc;

        //create test data for Case
        Case cse = new Case(
            Status = 'New',
            Stage__c = 'Open',
            Lender__c = 'ANZ',
            AccountId = acc.Id
        );
        insert cse;

	}

	static testmethod void testLDRvalidate(){

		Account acc = [Select Id, Name From Account];
		Case cse = [Select Id, CaseNumber From Case];

		Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(cse);
            LoanDocumentRequestFormCase ldcse  = new LoanDocumentRequestFormCase(sc);

            //getter methods
            ldcse.getinsurancePackageSP();
            ldcse.getgapProviderSp();
            ldcse.getlipCoverOptionSp();
            ldcse.getlpiCoverTypeSP();
            ldcse.getWarrantyProvidersp();
            ldcse.getWarrantyTermSp();

            //validate Docs Request Checklist
            ldcse.notesToLender = '';
            ldcse.notesToSupport = '';
            ldcse.receivedInsuranceWaiver = false;
            ldcse.insurancePackageNotesCOMPULSORY = '';
            //validate Finance Details
            ldcse.cseObj.Payout_Amount__c = 3;
            ldcse.cseObj.Payout_to__c = '';
            ldcse.payoutValidMin5Days = false;
            ldcse.payoutLetterInFile = false;
            ldcse.figuresAreCorrect = false;
            ldcse.cseObj.Asset_Type__c = 'Heavy Vehicles';
            ldcse.cseObj.Asset_Sub_Type__c = 'Rigid Truck > 4.5t';
            ldcse.cseObj.Gross_Vehicle_Mass__c = 0;
            ldcse.cseObj.Loan_Type__c = 'Car Loan';
            ldcse.cseObj.Vehicle_Purchase_Price__c = 10;
            //validate Insurance Details
            ldcse.gapProvider = 'PLS ERIC';
            ldcse.gapPremium = 0;
            ldcse.lpiCoverOption = '-- no Cover --';
            ldcse.lpiCoverType = null;
            ldcse.validateLdr();

            ldcse.lpiCoverOption = 'PLS ERIC';
            ldcse.lpiCoverType = 'Individual Consumer';
            ldcse.warrantyProvider = 'PLS ERIC';
            ldcse.warrantyPremium = 0;
            ldcse.warrantyProductName = '';
            //Validate Vendor Details
            ldcse.vendorDetailsCorrect = false;
            //Primary Asset Details Validations
            ldcse.cseObj.Vehicle_Vin__c = '';
            ldcse.cseObj.Vehicle_Year__c = '';
            ldcse.cseObj.Vehicle_Make__c = '';
            ldcse.cseObj.Vehicle_Model__c = '';
            
        	ldcse.validateLdr();

            ldcse.gapProvider = null;
            ldcse.lpiCoverOption = null;
            ldcse.lpiCoverType = null;
            ldcse.warrantyProvider = null;
            ldcse.cseObj.Lender__c = 'Liberty';
            ldcse.cseObj.Trail__c = 0;
            ldcse.cseObj.Vehicle_Type__c = 'Motor Vehicle';
            ldcse.cseObj.Vehicle_Vin__c = '12345';
            ldcse.validateLdr();

            //Final Check
            ldcse.notesToSupport = 'test';
            ldcse.notesToLender = 'test';
            ldcse.receivedInsuranceWaiver = true;
            ldcse.insurancePackageNotesCOMPULSORY = 'test';
            ldcse.cseObj.Payout_Amount__c = 5;
            ldcse.cseObj.Payout_to__c = 'test';
            ldcse.payoutLetterInFile = true;
            ldcse.payoutValidMin5Days = true;
            ldcse.figuresAreCorrect = true;
            ldcse.cseObj.Gross_Vehicle_Mass__c = 5;
            ldcse.gapPremium = 10;
            ldcse.warrantyPremium = 10;
            ldcse.warrantyProductName = 'test';
            ldcse.vendorDetailsCorrect = true;
            ldcse.cseObj.Vehicle_Vin__c = '12345678901234567';
            ldcse.cseObj.Vehicle_Year__c = '2017';
            ldcse.cseObj.Vehicle_Make__c = 'make';
            ldcse.cseObj.Vehicle_Model__c = 'model';
            ldcse.gapProvider = 'PLS ERIC';
            ldcse.lpiCoverOption = 'Life Only Cover';
            ldcse.lpiCoverType = 'Individual Consumer';
            ldcse.warrantyProvider = 'PLS ERIC';
            ldcse.cseObj.Trail__c = 10;
            ldcse.validateLdr();

            ldcse.caseID();
            ldcse.refreshCaseDetails();

            ldcse.saveUpdates();
            ldcse.submit();


        Test.stopTest();


	} 

    static testmethod void testLDRSubmit(){

        Account acc = [Select Id, Name From Account];
        Case cse = [Select Id, CaseNumber From Case];

        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(cse);
            LoanDocumentRequestFormCase ldcse  = new LoanDocumentRequestFormCase(sc);

            //getter methods
            ldcse.getinsurancePackageSP();
            ldcse.getgapProviderSp();
            ldcse.getlipCoverOptionSp();
            ldcse.getlpiCoverTypeSP();
            ldcse.getWarrantyProvidersp();
            ldcse.getWarrantyTermSp();

            //Final Check
            ldcse.insurancepackage = '10';
            ldcse.cseObj.Internal_Customer_Rating__c = 'Very happy - Send review request email';
            ldcse.cseObj.Base_Rate__c = 50;
            ldcse.cseObj.Residential_Reference_Completed__c = true;
            ldcse.cseObj.Employment_Reference_Completed__c = true;
            //ldcse.cseObj.Approval_Conditions__c = 'test';
            ldcse.loanObj.Brokerage__c = 10;
            ldcse.cseObj.Loan_Rate__c = 10;
            ldcse.cseObj.Application_Fee__c = 10;
            ldcse.notesToSupport = 'test';
            ldcse.notesToLender = 'test';
            ldcse.receivedInsuranceWaiver = true;
            ldcse.insurancePackageNotesCOMPULSORY = 'test';
            ldcse.cseObj.Payout_Amount__c = 5;
            ldcse.cseObj.Payout_to__c = 'test';
            ldcse.payoutLetterInFile = true;
            ldcse.payoutValidMin5Days = true;
            ldcse.figuresAreCorrect = true;
            ldcse.cseObj.Gross_Vehicle_Mass__c = 5;
            ldcse.gapPremium = 10;
            ldcse.warrantyPremium = 10;
            ldcse.warrantyProductName = 'test';
            ldcse.warrantyTerm = '1 Year';
            ldcse.vendorDetailsCorrect = true;
            ldcse.cseObj.Loan_Type__c = 'Personal Loan';
            ldcse.cseObj.Vehicle_Vin__c = '12345678901234567';
            ldcse.cseObj.Vehicle_Year__c = '2017';
            ldcse.cseObj.Vehicle_Make__c = 'make';
            ldcse.cseObj.Vehicle_Model__c = 'model';
            ldcse.gapProvider = 'PLS ERIC';
            ldcse.lpiCoverOption = 'Life Only Cover';
            ldcse.lpiCoverType = 'Individual Consumer';
            ldcse.warrantyProvider = 'PLS ERIC';
            ldcse.cseObj.Trail__c = 10;
            ldcse.cseObj.Sale_Type__c  = 'Private';
            ldcse.validateLdr();

            ldcse.saveUpdates();
            ldcse.submit();

            //Retrieve created Loan Doc
            //list <Loan_Document_Request_Form_CT__c> loandocs = [Select Id, Client_Name__c From Loan_Document_Request_Form_CT__c Where Client_Name__c = : cse.Id];
            //system.assertEquals(loanDocs.size(),1);

        Test.stopTest();

        

    }


}
/** 
* @FileName: FOShareTriggerTest
* @Description: Test class for FOShare Trigger 
* @Copyright: Positive (c) 2018 
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/23/18 RDAVID Created class
* 1.1 5/07/18 JBACULOD Updated testFOToRoleRollup
**/ 
@isTest
private class FOShareTriggerTest {
	
	@testsetup static void setup(){
        //Turn On Trigger 
		Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
		triggerSettings.Enable_Triggers__c = true;
		triggerSettings.Enable_Application_Sync_App_2_Case__c = true;
		triggerSettings.Enable_Application_Trigger__c= true;
		triggerSettings.Enable_Asset_Trigger__c= true;
		triggerSettings.Enable_Case_Auto_Create_Application__c= true;
		triggerSettings.Enable_Case_Scenario_Trigger__c= true;
		triggerSettings.Enable_Case_SCN_calculateSLATime__c= true;
		triggerSettings.Enable_Case_SCN_recordStageTimestamps__c= true;
		triggerSettings.Enable_Case_Trigger__c= true;
		triggerSettings.Enable_Expense_Trigger__c= true;
		triggerSettings.Enable_FO_Auto_Create_FO_Share__c= true;
		triggerSettings.Enable_FOShare_Rollup_to_Role__c= true;
		triggerSettings.Enable_FOShare_Trigger__c= true;
		triggerSettings.Enable_Income_Trigger__c= true;
		triggerSettings.Enable_Lender_Automation_Call_Lender__c= true;
		triggerSettings.Enable_Liability_Trigger__c= true;
		triggerSettings.Enable_Role_Address_Populate_Address__c= true;
		triggerSettings.Enable_Role_Address_Toggle_Current__c= true;
		triggerSettings.Enable_Role_Address_Trigger__c= true;
		triggerSettings.Enable_Role_Auto_Populate_Case_App__c= true;
		triggerSettings.Enable_Role_Rollup_to_Application__c= true;
		triggerSettings.Enable_Role_To_Account_Propagation__c= true;
		triggerSettings.Enable_Role_Toggle_Primary_Applicant__c= true;
		triggerSettings.Enable_Role_Trigger__c= true;
		triggerSettings.Enable_Triggers__c= true;
		upsert triggerSettings Trigger_Settings1__c.Id;
        
		TestDataFactory.Case2FOSetupTemplate();
        //Create Test Account
		List<Account> accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		//caseList[0].Lead_Type_Set_to_Convert__c = CommonConstants.CASE_RT_P_CONS_AF; //;'p: Consumer - Asset Finance';
		//caseList[0].Lead_Application_Type_Set_to_Convert__c = CommonConstants.APP_RT_CONS_I; //'Consumer - Individual';
		//caseList[0].Application_Record_Type__c = CommonConstants.APP_RT_CONS_I; //'Consumer - Individual';

		Database.insert(caseList);

		Application__c app = [SELECT Id FROM Application__c WHERE Case__c =: caseList[0].Id];
        
		//Create Test Role 
		List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, app.Id,  1);
		roleList[0].Primary_Contact_for_Application__c = true;
		roleList[0].Preferred_Call_Time__c = '8:30 am';
		Database.insert(roleList);
	}

    static testmethod void testFOToRoleRollup(){
        
        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
            Enable_Triggers__c = true,
            Enable_FOShare_Trigger__c = true,
            Enable_FOShare_Rollup_to_Role__c = true
            //Enable_FO_Auto_Create_FO_Share__c = true
        );
        insert trigSet;
        
        //Retrieve Role Test data from Setup
		list <Role__c> rolelist = [Select Id, Case__c, Application__c From Role__c];
		
		Test.startTest();
        
        	Id curFOSRTId = Schema.SObjectType.FO_Share__c.getRecordTypeInfosByName().get(CommonConstants.FO_ASSET).getRecordTypeId();	
        	list <FO_Share__c> foslist = new list <FO_Share__c>();
            for (Role__c role : rolelist){
            	FO_Share__c fos = new FO_Share__c(
                    RecordTypeId = curFOSRTId,
                    Role__c = role.Id,
                    Role_Share__c = 100,
                    Role_Share_Amount__c = 5000,
                    Case__c = role.Case__c,
                    Application_Asset__c = role.Application__c
                );
                foslist.add(fos);
            }
        	insert foslist;
        
        	//Cover FO Share update, delete trigger events
			update foslist;
			delete foslist;
        
        Test.stopTest();
	}
    
	static testmethod void testFOToRoleRollupAsync(){
        
        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
            Enable_Triggers__c = true,
            Enable_FOShare_Trigger__c = true,
            Enable_FOShare_Rollup_to_Role__c = true,
            Asynchronous_Rollup__c = true
            //Enable_FO_Auto_Create_FO_Share__c = true
        );
        insert trigSet;
        
        //Retrieve Role Test data from Setup
		list <Role__c> rolelist = [Select Id, Case__c, Application__c From Role__c];
		
		Test.startTest();
        
        	Id curFOSRTId = Schema.SObjectType.FO_Share__c.getRecordTypeInfosByName().get(CommonConstants.FO_ASSET).getRecordTypeId();	
        	list <FO_Share__c> foslist = new list <FO_Share__c>();
            for (Role__c role : rolelist){
            	FO_Share__c fos = new FO_Share__c(
                    RecordTypeId = curFOSRTId,
                    Role__c = role.Id,
                    Role_Share__c = 100,
                    Role_Share_Amount__c = 5000,
                    Case__c = role.Case__c,
                    Application_Asset__c = role.Application__c
                );
                foslist.add(fos);
            }
        	insert foslist;
        	//Cover FO Share update, delete trigger events
			update foslist;
			delete foslist;
            FOShareGateway fosgate = new FOShareGateway(rolelist);
			ID jobID = System.enqueueJob(fosgate);
        
        Test.stopTest();
	}
    
    static testMethod void testRollup() {
		Test.StartTest();
            Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Income1__c.getRecordTypeInfosByName();
         	Map<String,Schema.RecordTypeInfo> expensertMapByName = Schema.SObjectType.Expense1__c.getRecordTypeInfosByName();
        	Map<String,Schema.RecordTypeInfo> assetrtMapByName = Schema.SObjectType.Asset1__c.getRecordTypeInfosByName();
        	Map<String,Schema.RecordTypeInfo> liartMapByName = Schema.SObjectType.Liability1__c.getRecordTypeInfosByName();
        	Role__c roleRecord = [SELECT Id,Application__c FROM Role__c WHERE RecordType.Name =: CommonConstants.ROLE_RT_APP_INDI LIMIT 1];
			//zLEAD
        	List<Income1__c> incomeList = new List<Income1__c>();
            List<String> leadType = new List<String>{'Centrelink','Child Support','Investment Income','Other Income','PAYG Employment','Self-Employed','Non-Borrowing Spouse'};
            for(Integer i = 0; i < 7; i++){
                Income1__c inc1 = new Income1__c();
                inc1.RecordTypeId = rtMapByName.get(CommonConstants.INCOME_RT_ZL).getRecordTypeId(); //zLead Consumer
                inc1.Lead_Income_Type__c = leadType[i];
                inc1.Net_Monthly_Amount__c = 100;
                inc1.application1__c = roleRecord.Application__c;
                inc1.role__c = roleRecord.Id;
                incomeList.add(inc1);
            }
            database.insert(incomeList,true);
        
        	//Base_Income__c
            List<Income1__c> incomeList1 = new List<Income1__c>();
            List<String> baseRType = new List<String>{rtMapByName.get(CommonConstants.INCOME_RT_PE).getRecordTypeId()}; //PAYG Employment
            List<String> baseSourceType = new List<String>{CommonConstants.INCOME_RT_PE};
            for(Integer i = 0; i < 2; i++){
                Income1__c inc1 = new Income1__c();
                inc1.RecordTypeID = baseRType[0];
                inc1.Income_Type__c = baseSourceType[0];
                if(inc1.Income_Type__c==CommonConstants.INCOME_RT_PE){
                    inc1.Income_Situation__c = CommonConstants.INCOME_IS_SI; //Secondary Income; '2nd Job';
                    inc1.Net_Standard_Pay__c = 100;
                }
                inc1.Application1__c = roleRecord.Application__c;
                inc1.role__c = roleRecord.Id;
                incomeList1.add(inc1);
            }
            database.insert(incomeList1,true);
        
            List<Income1__c> incomeList2 = new List<Income1__c>();
            List<String> baseRType2 = new List<String>{rtMapByName.get(CommonConstants.INCOME_RT_PE).getRecordTypeId(),rtMapByName.get(CommonConstants.INCOME_RT_SE).getRecordTypeId()};
            List<String> baseSourceType2 = new List<String>{CommonConstants.INCOME_RT_PE,CommonConstants.INCOME_RT_SE};
            for(Integer i = 0; i < 2; i++){
                Income1__c inc1 = new Income1__c();
                inc1.RecordTypeID = baseRType2[i];
                inc1.Income_Type__c = baseSourceType2[i];
                if(inc1.Income_Type__c==CommonConstants.INCOME_RT_PE){
                    inc1.Income_Situation__c = CommonConstants.INCOME_IS_SI; //Secondary Income; '2nd Job';
                    inc1.Net_Standard_Pay__c = 100;
                }
                
                if(inc1.Income_Type__c==CommonConstants.INCOME_RT_SE){
                    //inc1.Gross_Income__c = 101;
                }
                
                inc1.Application1__c = roleRecord.Application__c;
                inc1.role__c = roleRecord.Id;
                incomeList2.add(inc1);
            }
            database.insert(incomeList2,true);
               
        	roleRecord = [SELECT Id, Net_Business_Income__c, Child_Support_Income__c, Centrelink_Income__c, Investment_Income__c, Other_Income__c, Base_Income__c, Additional_Income__c, Second_Job_Income__c FROM Role__c WHERE RecordType.Name =: CommonConstants.ROLE_RT_APP_INDI LIMIT 1];
        	//System.assertEquals(roleRecord.Net_Business_Income__c, 100);
        
		Test.StopTest();
	}
    static testMethod void testRollup2() {
		Test.StartTest();
        	Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Income1__c.getRecordTypeInfosByName();
        	Role__c roleRecord = [SELECT Id,Application__c FROM Role__c WHERE RecordType.Name =: CommonConstants.ROLE_RT_APP_INDI LIMIT 1];
        	List<Income1__c> incomeList3 = new List<Income1__c>();
            List<String> baseRType3 = new List<String>{rtMapByName.get(CommonConstants.INCOME_RT_PE).getRecordTypeId(),rtMapByName.get(CommonConstants.INCOME_RT_SE).getRecordTypeId()};
            List<String> baseSourceType3 = new List<String>{CommonConstants.INCOME_RT_PE,CommonConstants.INCOME_RT_SE};
            for(Integer i = 0; i < 2; i++){
                Income1__c inc1 = new Income1__c();
                inc1.RecordTypeID = baseRType3[0];
                inc1.Income_Type__c = baseSourceType3[0];
                if(inc1.Income_Type__c==CommonConstants.INCOME_RT_PE){
                    inc1.Income_Situation__c = CommonConstants.INCOME_IS_CI; //Current Income ; 'Current';
                    inc1.Net_Standard_Pay__c = 100;
           			inc1.Overtime_Pay_monthly_net__c = 100;         
                }
                inc1.Application1__c = roleRecord.Application__c;
                inc1.role__c = roleRecord.Id;
                incomeList3.add(inc1);
            }
            database.insert(incomeList3,true);
            
        	Map<String,Schema.RecordTypeInfo> assetrtMapByName = Schema.SObjectType.Asset1__c.getRecordTypeInfosByName();
        	Map<String,Schema.RecordTypeInfo> liartMapByName = Schema.SObjectType.Liability1__c.getRecordTypeInfosByName();
        	
			
			// Cash_in_Bank__c
            List<Asset1__c> assetList = new List<Asset1__c>();
            List<String> cashInBankSourceType = new List<String>{'Business Account','Personal Account','Term Deposit'};
            for(Integer i = 0; i < 3; i++){
                Asset1__c asset1 = new Asset1__c();
                asset1.RecordTypeID = assetrtMapByName.get(CommonConstants.ASSET_RT_CIB).getRecordTypeId(); //Cash in Bank
                asset1.Asset_Type__c = cashInBankSourceType[i];
                asset1.Asset_Value__c = 100;
                asset1.Application1__c = roleRecord.Application__c;
                assetList.add(asset1);
            }
            database.insert(assetList,true);
        
            // Loans_Investment_Property__c
            List<Liability1__c> liaList = new List<Liability1__c>();
            List<String> type = new List<String>{'Mortgage - Investment'};
            for(Integer i = 0; i < 5; i++){
                Liability1__c lia1 = new Liability1__c();
                lia1.RecordTypeID = liartMapByName.get(CommonConstants.LIABILITY_RT_LRE).getRecordTypeId(); //Loan - Real Estate
                lia1.Liability_Type__c = type[0];
                lia1.Amount_Owing__c = 200;
                lia1.Application1__c = roleRecord.Application__c;
                liaList.add(lia1);
            }
            database.insert(liaList,true);
        
		Test.StopTest();
	}

    static testmethod void testProcessedMapPB(){

        //Prepopulate TriggerFactory.isProcessedMap
        Case cse = [Select Id ,CaseNumber From Case limit 1];
        map <Id,boolean> isProcessedMap = new map <Id,Boolean>();
        isProcessedMap.put(cse.Id, true);
        TriggerFactory.isProcessedMap = isProcessedMap;

        Test.startTest();

            list <ID> sobjIDs = new list <ID>();
            sobjIDs.add(cse.Id);
            ProcessContinueTriggerNM.continueProcessofRecord(sobjIDs);

        Test.stopTest();
    }
}
/*
	@Description: class for calculating SLA Age of Scenario Actioned (SLA-1) on Scenario
	@Author: Jesfer Baculod - Positive Group
	@History:
		- 11/14/2017 - Created
*/
public class Scenario_UpdateSLAWarning_SLA1 {
	
	@InvocableMethod(label='SLA Next Warning Time - Scenario Actioned' description='updates next Warning Time of an SLA whenever an SLA is not yet completed')
	public static void scenarioSLAWarningTimeSA (list <ID> scenIDs) {

		//Retrieve default Business Hours
		BusinessHours bh = [select Id from BusinessHours where IsDefault=true]; 

		//Retrieve Scenarios to update
		list <Complex_Scenario__c> scenlist = [Select Id, 
								SLA_Time_1_Start__c, SLA_Warning_Time_1__c, SLA_Started_Count_1__c, SLA_Completed_Count_1__c, SLA_Active_1__c, SLA_Time_1_mm__c
								From Complex_Scenario__c Where Id in : scenIds];
        
        Datetime warningdate; 
		for (Complex_Scenario__c scen : scenlist){
			warningdate = scen.SLA_Warning_Time_1__c;
			scen.SLA_Warning_Time_1__c = null;
			scen.SLA_Active_1__c = null;
		}
		update scenlist; //force update to retrigger SLA Warning
		system.debug('@@warningdate:'+warningdate);

		for (Complex_Scenario__c scen : scenlist){
            scen.SLA_Active_1__c = 'Yes';
            if (scen.SLA_Started_Count_1__c != scen.SLA_Completed_Count_1__c){ 
                scen.SLA_Warning_Time_1__c = BusinessHours.add(bh.Id, warningdate, 900000); //Set succeeding warning of current SLA (add 15 minutes)
                DateTime warningtimeAR = scen.SLA_Warning_Time_1__c;
				scen.SLA_Warning_Time_1__c = Datetime.newInstance(warningtimeAR.year(), warningtimeAR.month(), warningtimeAR.day(), warningtimeAR.hour(), warningtimeAR.minute(), 0);
                scen.SLA_Time_1_mm__c = Decimal.valueof(( BusinessHours.diff(bh.Id, scen.SLA_Time_1_Start__c, warningdate ) / 1000) / 60 );  //returns SLA Age in minutes
                if (scen.SLA_Time_1_mm__c == 89) scen.SLA_Time_1_mm__c = 90; //workaround for business hours difference of SLA
				if (scen.SLA_Time_1_mm__c == 104) scen.SLA_Time_1_mm__c = 105; //workaround for business hours difference of SLA
				if (scen.SLA_Time_1_mm__c == 119) scen.SLA_Time_1_mm__c = 120; //workaround for business hours difference of SLA
                system.debug('@@slaWarningB');
            }
		}

		update scenlist;

	}


}
/** 
* @FileName: FrontEndSubmissionHandler
* @Description: Trigger Handler for the Front_End_Submission__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/8/18 RDAVID Created Class
* 2.0 RDAVID 6/09/18 extra/DialerAutomationUpdates
* 2.1 RDAVID 12/09/18: Added logic to reset Case to New and Open
* 2.2 RDAVID 17/09/18: Fix issue NVM firing over the weekend, Set Case to Open when Status is New
* 2.3 RDAVID 20/09/18: Only do update to Case when Status and Stage if no further than New > Attempted Contact, moved logic to After event
* 2.4 9/28/18 RDAVID - extra/DialerAutomationUpdates - Added logic for NVM Routing based on Timezone 
* 2.5 JBACULOD 12/02/19: Added updating of Case on Frontend Submission load completion for Overflow
* 2.6 JBACULOD 27/02/19: Added updating of Status/Stage/Closed Reason on FE Complete Load
* 3.0 JBACULOD 19/06/19: Added bypass for processCaseOutsideBusinessHours when Queue is LFP Flow
* 3.1 RDAVID 26/06/19: extra/task25040196-BugFixNVMCaseRoutingFESubTrigger - Added logic to Exclude the Positive_Form_Type__c = 'mini-app' in the Frontend Submission 
* 3.2 RDAVID 11/09/19: extra/task25953355-NODFlowV3SFAutomations - Added Logic to convert Preferred_Call_Time__c to local time and populate Preferred_Call_Date_Time_Local__c
* 3.3 RDAVID 17/09/2019: extra/task25953355-NODFlowV3SFAutomations - Temporarily Disable NVM Call Routing in FE Triggger per Tim Wells. 
* 3.4 JBACULOD 8/10/2019: issuefix/tasks26326007-OverflowNewPositiveFormTypes - Updated criteria for updating Case's FE Submission Completed
**/ 

public with sharing class FrontEndSubmissionHandler implements ITrigger {	
	
	private Map<Id,Case> caseMap = new Map<Id,Case>(); 
	private List<Case> caseToUpList = new List<Case>();
	Process_Flow_Definition_Settings1__c processFlowSettings = Process_Flow_Definition_Settings1__c.getInstance(UserInfo.getUserId());
	public static final String NOD_REQUEST_CALL = 'nod-request-call';
	private Map<String,BusinessHours> stateBusinessHoursMap = new Map<String,BusinessHours>();
	// Constructor
	public FrontEndSubmissionHandler(){
		if(stateBusinessHoursMap.isEmpty()) stateBusinessHoursMap = TimeUtils.getStateBusinessHoursMap();
	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
	}
	
	public void bulkAfter(){
		List<Frontend_Submission__c> newFEList = (List<Frontend_Submission__c>)trigger.new;
		
		Set<Id> cseIdSet = new Set<Id>();
		if(Trigger.isInsert){
			for(Frontend_Submission__c fe : newFEList){
				if(fe.Case__c != NULL){
					cseIdSet.add(fe.Case__c);
				}
			}
		}
		if( ( TriggerFactory.trigset.Enable_Frontend_Sub_SetNVMContact__c || TriggerFactory.trigset.Enable_Frontend_Sub_Complete_Load__c) && cseIdSet.size()>0){
			caseMap = new Map<Id,Case>([SELECT Id, FE_Submission_Complete__c, Owner.Name, Closed_Reason__c, Lead_Bucket__c, Status,Stage__c,State_Code__c,BusinessHoursId,Partition__c,Ready_For_Call__c, (SELECT Id FROM Frontend_Submissions__r) FROM Case WHERE Id IN: cseIdSet]);
		} 
	}

	public void beforeInsert(SObject so){
		Frontend_Submission__c frontEndSub = (Frontend_Submission__c)so;
		// 3.2 RDAVID 11/09/19: extra/task25953355-NODFlowV3SFAutomations
		if(frontEndSub.Positive_Form_Type__c == NOD_REQUEST_CALL && frontEndSub.Preferred_Call_Time__c != NULL 
		&& frontEndSub.Case_State_Code__c != NULL && stateBusinessHoursMap.containsKey(frontEndSub.Case_State_Code__c)){
			Datetime preferredCallTime = TimeUtils.getLocalClientPreferredTime(stateBusinessHoursMap.get(frontEndSub.Case_State_Code__c).TimeZoneSidKey, frontEndSub.Preferred_Call_Time__c);
			System.debug('preferredCallTime == ' + preferredCallTime.format() + ' System.now() == ' +System.now().format());
			if(preferredCallTime >= System.now()) frontEndSub.Preferred_Call_Date_Time_Local__c = preferredCallTime;
			else if (preferredCallTime < System.now()) frontEndSub.Preferred_Call_Date_Time_Local__c = preferredCallTime.addDays(1);
		}
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		Frontend_Submission__c frontEndSub = (Frontend_Submission__c)so;
		Frontend_Submission__c oldFrontEndSub = (Frontend_Submission__c)oldSo;
		if(frontEndSub.Positive_Form_Type__c == NOD_REQUEST_CALL && frontEndSub.Preferred_Call_Time__c != NULL && oldFrontEndSub.Preferred_Call_Time__c != frontEndSub.Preferred_Call_Time__c
		&& frontEndSub.Case_State_Code__c != NULL && stateBusinessHoursMap.containsKey(frontEndSub.Case_State_Code__c)){
			Datetime preferredCallTime = TimeUtils.getLocalClientPreferredTime(stateBusinessHoursMap.get(frontEndSub.Case_State_Code__c).TimeZoneSidKey, frontEndSub.Preferred_Call_Time__c);
			System.debug('preferredCallTime == ' + preferredCallTime.format() + ' System.now() == ' +System.now().format());
			if(preferredCallTime >= System.now()) frontEndSub.Preferred_Call_Date_Time_Local__c = preferredCallTime;
			else if (preferredCallTime < System.now()) frontEndSub.Preferred_Call_Date_Time_Local__c = preferredCallTime.addDays(1);
		}
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){
		Frontend_Submission__c frontEndSub = (Frontend_Submission__c)so;
		
		if(!caseMap.isEmpty() && caseMap.containsKey(frontEndSub.Case__c)){
			Case cse = caseMap.get(frontEndSub.Case__c);//new Case(Id=frontEndSub.Case__c); 
			Integer numOfCurrFEs = (caseMap.get(frontEndSub.Case__c).Frontend_Submissions__r != NULL && caseMap.get(frontEndSub.Case__c).Frontend_Submissions__r.size() > 0) ? caseMap.get(frontEndSub.Case__c).Frontend_Submissions__r.size() : 0;
			Boolean updateCase = false;

			/*RDAVID 17/09/2019: extra/task25953355-NODFlowV3SFAutomations - Temporarily Disable this feature per Tim Wells. 
			if (TriggerFactory.trigset.Enable_Frontend_Sub_SetNVMContact__c){
				if(numOfCurrFEs > 1){
					if(caseMap.get(frontEndSub.Case__c).Status == 'New' && (caseMap.get(frontEndSub.Case__c).Stage__c == 'Open' || caseMap.get(frontEndSub.Case__c).Stage__c == 'Attempted Contact' || caseMap.get(frontEndSub.Case__c).Stage__c == 'WOPA')
					&& frontEndSub.Positive_Form_Type__c != 'mini-app'//3.1 RDAVID 26/06/19: extra/task25040196-BugFixNVMCaseRoutingFESubTrigger
					&& frontEndSub.Positive_Form_Type__c != 'lfp-request-call'){ //3.2 RDAVID 11/09/19: extra/task25953355-NODFlowV3SFAutomations
						updateCase = true;
						cse.Stage__c = 'Open';
						//2.4 9/28/18 RDAVID
						Boolean readyForCall = cse.Ready_For_Call__c;
						CaseGateway.initBusinessHoursVariables(); 
						if(cse.State_Code__c != NULL) CaseGateway.processCaseOutsideBusinessHours(cse);
						if(readyForCall && cse.Ready_For_Call__c && cse.Partition__c == 'Positive' && processFlowSettings.Enable_Case_NVM_Flow_Definitions__c && cse.Lead_Bucket__c != 'LFP Flow'){ //JBACU 19/06/19
							cse.NVMContactWorld__NVMRoutable__c = true;
						}
						//2.4 9/28/18 RDAVID
					}
				}  
			}*/

			if (TriggerFactory.trigset.Enable_Frontend_Sub_Complete_Load__c){ //02/12/2019 JBACULOD
				if (caseMap.get(frontEndSub.Case__c).Owner.Name == 'Overflow' && caseMap.get(frontEndSub.Case__c).Lead_Bucket__c == 'Overflow'){
					if (frontEndSub.Positive_Form_Type__c == 'Quick Quote Part 2' || frontEndSub.Positive_Form_Type__c == 'lfp-credit-check' || frontEndSub.Positive_Form_Type__c == 'lfp-preapproval'){
						cse.FE_Submission_Complete__c = true;
						//cse.Status = 'Closed'; //JBACU 05/07/19 Moved as scheduled action on Case Assignment
						//cse.Stage__c = 'Referred Out';
						//cse.Closed_Reason__c = 'Referred to Overflow';
						updateCase = true;
					}
				}
			}
			
			//if Case is for update
			if(updateCase) caseToUpList.add(cse);
		}
	}
	
	public void afterUpdate(SObject oldSo, SObject so){
		
	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		if(caseToUpList.size()>0) Database.update(caseToUpList);
	}
}
/*
	@Description: testing auto create of Box Folders on Partner records
	@Author: Rexie David - Positive Group
	@History:
		-	4/04/19 - Created
        -   7/10/19 - JBACULOD - updated due to added renaming/linking of Folder from Lead to Partner
*/
@isTest
public class Partner_BoxFolderOnCreateTest {
    @testsetup static void setup(){
        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = Trigger_Settings1__c.getOrgDefaults();
        trigSet.Enable_Triggers__c = false;
        trigSet.Enable_Partner_Box_Auto_Create__c = true;
        upsert trigSet Trigger_Settings1__c.Id;
	}

    static testmethod void testInvocablePartnerBoxFolderCreate(){

        Test.startTest();
            Id rtId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId();
			List<Partner__c> partnerList = new List<Partner__c>();
            Partner__c partner = new Partner__c(Name = 'Test Partner Box', Partner_Type__c = 'Mortgage Aggregator', m_Rating__c = 'Committed', m_Company_Type__c = 'Mortgage Aggregator', RecordTypeId = rtId);
            partnerList.add(partner);
            insert partnerList;

            list <ID> partnerListIDs = new list <ID>();
            for (Partner__c part : partnerList){
                partnerListIDs.add(part.Id);
            }

            Partner_BoxFolderOnCreate.test_objRecordFolderID = '12334555';
            Partner_BoxFolderOnCreate.partnerBoxFolderCreate(partnerListIDs);

        Test.stopTest();
    }

    static testmethod void testPartnerBoxFolderCreate(){

        Test.startTest();
            Id rtId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId();
			List<Partner__c> partnerList = new List<Partner__c>();
            Partner__c partner = new Partner__c(Name = 'Test Partner Box', Partner_Type__c = 'Mortgage Aggregator', m_Rating__c = 'Committed', m_Company_Type__c = 'Mortgage Aggregator', RecordTypeId = rtId);
            partnerList.add(partner);
            insert partnerList;

            list <ID> partnerListIDs = new list <ID>();
            for (Partner__c part : partnerList){
                partnerListIDs.add(part.Id);
            }

            Partner_BoxFolderOnCreate.test_objRecordFolderID = '12334555';
            Partner_BoxFolderOnCreate cbox = new Partner_BoxFolderOnCreate(partnerListIDs);
            cbox.createBoxFolder(partnerListIDs);
            ID jobID = System.enqueueJob(cbox);
            system.debug('@@JobID:'+jobID); 


            for (Partner__c part : [Select Id, Box_Folder_ID__c From Partner__c Where Id in : partnerListIDs]){
                system.assert(part.Box_Folder_ID__c != null);
                system.assertEquals(part.Box_Folder_ID__c, Partner_BoxFolderOnCreate.test_objRecordFolderID);
            } 

        Test.stopTest();

    }

    static testmethod void testPartnerBoxFolderCreateLenderDivision(){

        Test.startTest();
            Id rtId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Lender Division').getRecordTypeId();
			List<Partner__c> partnerList = new List<Partner__c>();
            Partner__c partner = new Partner__c(Name = 'Test Partner Box', Partner_Type__c = 'Personal Loan', m_Rating__c = 'Committed', RecordTypeId = rtId);
            partnerList.add(partner);
            insert partnerList;

            list <ID> partnerListIDs = new list <ID>();
            for (Partner__c part : partnerList){
                partnerListIDs.add(part.Id);
            }

            Partner_BoxFolderOnCreate.test_objRecordFolderID = '12334555';
            Partner_BoxFolderOnCreate cbox = new Partner_BoxFolderOnCreate(partnerListIDs);
            cbox.createBoxFolder(partnerListIDs);
            ID jobID = System.enqueueJob(cbox);
            system.debug('@@JobID:'+jobID);

            for (Partner__c part : [Select Id, Box_Folder_ID__c From Partner__c Where Id in : partnerListIDs]){
                system.assert(part.Box_Folder_ID__c != null);
                system.assertEquals(part.Box_Folder_ID__c, Partner_BoxFolderOnCreate.test_objRecordFolderID);
            }

        Test.stopTest();

    } 
}
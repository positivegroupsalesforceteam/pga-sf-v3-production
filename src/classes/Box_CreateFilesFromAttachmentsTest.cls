/** 
* @FileName: Box_CreateFilesFromAttachmentsTest
* @Description: test class of Boz_CreateFilesFromAttachments
* @Copyright: Positive (c) 2018 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 9/19/18 JBACULOD Created
**/
@isTest
private class Box_CreateFilesFromAttachmentsTest {

    @testsetup static void setup(){

        //Create test data for (Case, Application, Role, Account)
        TestDataFactory.Case2FOSetupTemplate();

        //Create test data for Support Request
        Id rtSRID = Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Document Upload').getRecordTypeId();
        list <Support_Request__c> srlist = new list <Support_Request__c>();
        for (integer i=0; i<1;i++){
            Support_Request__c sr = new Support_Request__c(
                RecordTypeId = rtSRID
            );
            srlist.add(sr);
        }
        insert srlist;

    }

    static testmethod void testCreateFilesFromAttachments(){

        //Retrieve test data fpr Case
        Case parentCse = [Select Id, CaseNumber From Case limit 1];
        //Retrieve Support Request test data
        list <Support_Request__c> srlist = [Select Id, Name, Case_Number__c From Support_Request__c];

        Test.starttest();

            Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Attachment_Create_File_in_Box__c = true
            );
            insert trigSet;

            //Create attachments in Support Request
            list <Attachment> attlist = new list <Attachment>();
            for (Support_Request__c sr : srlist){
                Attachment att = new Attachment(
                    ParentId = sr.Id,
                    Body = Blob.valueof('Body of' + sr.Name),
                    Name = 'Test File' + sr.Name + '.txt'
                );
                attlist.add(att);
            }
            insert attlist;

            //Update Support Request w/ Case
            for (Support_Request__c sr : srlist){
                sr.Case_Number__c = parentCse.Id;
            }
            update srlist;

            String response = Box_CreateFilesFromAttachments.createBoxFilesFromAttachment(srlist[0].Case_Number__c, srlist[0].Id);

        Test.stopTest();

    }

}
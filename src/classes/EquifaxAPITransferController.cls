public class EquifaxAPITransferController {

    public static Map<String,String> streetTypeCodeMap = new Map<String,String>(); //Street x ST
    public static Map<String,String> streetCodeMap = new Map<String,String>(); //ST x ST
    public static String equifaxEnquiryId; //ST x ST

    //called in Lightning component
    @AuraEnabled
    public static wrapperClass initMethod(Id recId){
        System.debug('APPLICATION ID = '+recId);
        // create a wrapper class object and set the wrapper class @AuraEnabled properties and return it to the lightning component.
        wrapperClass returnwrapperClass = new  wrapperClass ();        
        returnwrapperClass.valid = true;//by default;
        returnwrapperClass.permissionTypes = getPicklistValues(API_Transfer__c.EQ_Permission_Type_Code__c.getDescribe()); 
        returnwrapperClass.genderCodes = getPicklistValues(API_Transfer__c.EQ_Gender_Code__c.getDescribe()); 
        returnwrapperClass.equiAccTypeCodes = getPicklistValues(API_Transfer__c.EQ_Account_Type_Code__c.getDescribe()); 
        returnwrapperClass.equiRelationshipCodes = getPicklistValues(API_Transfer__c.EQ_Relationship_Code__c.getDescribe()); 
        returnwrapperClass.equiEmploymentTypes = getPicklistValues(API_Transfer__c.EQ_Employment_Type__c.getDescribe()); 
        returnwrapperClass.equiStateCodes = getPicklistValues(API_Transfer__c.EQ_C_State_Code__c.getDescribe()); 
        returnwrapperClass.equiCountryCodes = getPicklistValues(API_Transfer__c.EQ_C_Country_Code__c.getDescribe()); 
        returnwrapperClass.mainRec = queryRecord(recId,returnwrapperClass); 
        return returnwrapperClass;    
    }

    //called in this class' initMethod method
    private static Application__c queryRecord (Id recordId, wrapperClass returnwrapperClass){
        Application__c record;
        System.debug('recordId == '+recordId);
        if(!String.isBlank(recordId)){
            String sobjectType = recordId.getSObjectType().getDescribe().getName();
            if(sobjectType == 'Application__c'){
                String queryStr = 'SELECT Id,( SELECT Id,Drivers_Licence_Number__c,Gender__c,Date_of_Birth__c,Account_Name__c,Account__r.FirstName,Account__r.MiddleName,Account__r.LastName, Account__r.Drivers_Licence_Number__c, Account__r.Date_of_Birth__c,Role_Type__c FROM Roles__r WHERE Account__r.IsPersonAccount = TRUE ORDER BY Role_Type__c ASC),Recordtype.Name,Case__r.Recordtype.Name,Case__r.Required_Loan_Amount__c FROM Application__c WHERE Id = \''+recordId+'\'';
                try{
                    record = Database.query(queryStr);
                    System.debug('record == '+record);
                    if(record.Roles__r.size() > 0){
                        if(record.Roles__r.size() > 1){
                            returnwrapperClass.showRoleSelectionOnInit = true;
                        }
                        else if(record.Roles__r.size() == 1){
                            returnwrapperClass.showRoleSelectionOnInit = false;
                            returnwrapperClass.selectedRoleId = record.Roles__r[0].Id;//Set the Selected Role;
                            loadData(returnwrapperClass);
                        }
                    }
                    else{
                         returnwrapperClass.errMsg = 'The application has no valid applicants.';
                        returnwrapperClass.valid = false;
                    }
                }
                catch(Exception e){
                    returnwrapperClass.errMsg = 'Error in Retrieving data. Please contact System Admin.' + e.getMessage();
                    returnwrapperClass.valid = false;
                }
            }
        }
        return record;
    }

    //called in this class' queryRecord method
    @AuraEnabled
    public static wrapperClass loadData(wrapperClass returnwrapperClass){
        System.debug('returnwrapperClass ID = '+returnwrapperClass.selectedRoleId);
        returnwrapperClass.apiTransferData = buildFormData(returnwrapperClass.selectedRoleId);
        returnwrapperClass.equifaxEnquiryId = equifaxEnquiryId;
        System.debug('equifaxEnquiryId == '+equifaxEnquiryId);
        return returnwrapperClass;    
    }
    
    //called in this class' loadData method
    public static API_Transfer__c buildFormData (Id recordId){
        System.debug('buildFormData recordId = '+recordId);
        API_Transfer__c apitransfer = new API_Transfer__c(RecordtypeId = Schema.SObjectType.API_Transfer__c.getRecordTypeInfosByName().get('Equifax').getRecordTypeId(), EQ_Permission_Type_Code__c = 'Consumer');
        String roleQuery = 'SELECT Id,Case__c,Equifax_Enquiry_Id__c,Drivers_Licence_Number__c,Gender__c,Date_of_Birth__c,RecordType.Name,Account_Name__c,Account__r.FirstName,Account__r.MiddleName,Account__r.LastName, Account__r.Drivers_Licence_Number__c, Account__r.PersonBirthdate,Role_Type__c, Case__r.Recordtype.Name,Case__r.Required_Loan_Amount__c, (SELECT Id, Address__c, Address__r.Street_Number__c, Address__r.Street_Name__c, Address__r.Street_Type__c, Address__r.Suburb__c, Address__r.State__c, Address__r.Postcode__c, Address__r.Country__c, Address__r.State_Acr__c, Address__r.Full_Address__c, Active_Address__c FROM Role_Addresses__r WHERE Address__c != NULL AND Recordtype.Name = \'Full Address\' AND Active_Address__c != NULL ORDER BY CreatedDate DESC), (SELECT Id,Income_Type__c,Employer_Name__c,Income_Situation__c,Business_Name__c FROM Incomes1__r WHERE Income_Situation__c =	\'Current Income\' AND (Income_Type__c = \'PAYG Employment\' OR Income_Type__c = \'Self-Employed\') ORDER BY CreatedDate DESC) FROM Role__c WHERE Id = \''+ recordId +'\'';
        
        Role__c roleRecord = Database.query(roleQuery);

        if(roleRecord != new Role__c()){
            apitransfer.EQ_First_Name__c = roleRecord.Account__r.FirstName;
            apitransfer.EQ_Middle_Name__c = roleRecord.Account__r.MiddleName;
            apitransfer.EQ_Last_Name__c = roleRecord.Account__r.LastName;
            apitransfer.EQ_Gender_Code__c = (String.isEmpty(roleRecord.Gender__c)) ? 'U' : (roleRecord.Gender__c == 'Male') ? 'M' : (roleRecord.Gender__c == 'Female') ? 'F' : '';
            apitransfer.EQ_Driver_Licence_Number__c =  (String.isEmpty(roleRecord.Drivers_Licence_Number__c)) ? '' : (roleRecord.Drivers_Licence_Number__c.replaceAll( '\\s+', '').length() >= 9 ?  roleRecord.Drivers_Licence_Number__c.substring(0,9) : roleRecord.Drivers_Licence_Number__c.substring(0,roleRecord.Drivers_Licence_Number__c.length())) ;
            apitransfer.EQ_Date_of_Birth__c = roleRecord.Account__r.PersonBirthdate;
            apitransfer.EQ_Account_Type_Code__c = (roleRecord.Case__r.Recordtype.Name.containsIgnoreCase('Asset')) ? 'AL' : (roleRecord.Case__r.Recordtype.Name.containsIgnoreCase('Personal')) ? 'PF' : (roleRecord.Case__r.Recordtype.Name.containsIgnoreCase('Property')) ? 'RM' : '';
            apitransfer.EQ_Enquiry_Amount__c = roleRecord.Case__r.Required_Loan_Amount__c;
            apitransfer.EQ_Relationship_Code__c = setRelationshipCode(roleRecord.Recordtype.Name);
            apitransfer.Case__c = roleRecord.Case__c;
            setEmploymentFields(apitransfer,roleRecord.Incomes1__r);
            setAddress(apitransfer,roleRecord);
            if(equifaxEnquiryId == null) equifaxEnquiryId = roleRecord.Equifax_Enquiry_Id__c;
        }

        return apitransfer;
    }

    //called in this class' setEmploymentFields method
    public static void setEmploymentFields(API_Transfer__c apitransfer, List<Income1__c> employmentIncomeList){
        if(employmentIncomeList.size() > 0){
            Boolean hasCurrent = false;
            for(Income1__c inc : employmentIncomeList){
                if(!hasCurrent){
                    if(inc.Income_Situation__c.containsIgnoreCase('Current')) apitransfer.EQ_Employment_Type__c = 'C';
                    if(inc.Income_Type__c.containsIgnoreCase('PAYG')){
                        apitransfer.EQ_Employment_Type__c = 'C';
                        apitransfer.EQ_Employer_Name__c = inc.Employer_Name__c;
                        hasCurrent = true;
                    }
                    else if(inc.Income_Type__c.containsIgnoreCase('Self')){
                        apitransfer.EQ_Employment_Type__c = 'C';
                        apitransfer.EQ_Employer_Name__c = 'SELF';
                        hasCurrent = true;
                    }
                }
            }
        }
    } 

    //called in this class' buildFormData method
    private static void setAddress(API_Transfer__c apitransfer, Role__c roleRecord){
        if(roleRecord.Role_Addresses__r.size() == 0) return;
        else{
            Boolean hasCurrent = false;
            Boolean hasPrevious = false;
        
            for(Role_Address__c roleAddress : roleRecord.Role_Addresses__r){
                if(roleAddress.Active_Address__c == 'Current' && !hasCurrent){
                    hasCurrent = true;
                    apitransfer.EQ_Current_Address__c = true;
                    apitransfer.EQ_C_Unit_Number__c = getValidAddressFieldValues('unitnum', roleAddress);
                    apitransfer.EQ_C_Street_Number__c = getValidAddressFieldValues('strnum',roleAddress);
                    apitransfer.EQ_C_Street_Name__c = getValidAddressFieldValues('strname',roleAddress);
                    apitransfer.EQ_C_Street_Type__c = getValidAddressFieldValues('strtype',roleAddress);
                    apitransfer.EQ_C_Suburb__c = getValidAddressFieldValues('sub',roleAddress);
                    apitransfer.EQ_C_State_Code__c = getValidAddressFieldValues('statecode',roleAddress);
                    apitransfer.EQ_C_Postcode__c = getValidAddressFieldValues('pcode',roleAddress);
                    apitransfer.EQ_C_Country_Code__c = getValidAddressFieldValues('country',roleAddress);
                }   
                else if(roleAddress.Active_Address__c == 'Previous' && !hasPrevious){
                    hasPrevious = true;
                    apitransfer.EQ_Previous_Address__c = true;
                    apitransfer.EQ_P_Unit_Number__c = getValidAddressFieldValues('unitnum', roleAddress);
                    apitransfer.EQ_P_Street_Number__c = getValidAddressFieldValues('strnum',roleAddress);
                    apitransfer.EQ_P_Street_Name__c = getValidAddressFieldValues('strname',roleAddress);
                    apitransfer.EQ_P_Street_Type__c = getValidAddressFieldValues('strtype',roleAddress);
                    apitransfer.EQ_P_Suburb__c = getValidAddressFieldValues('sub',roleAddress);
                    apitransfer.EQ_P_State_Code__c = getValidAddressFieldValues('statecode',roleAddress);
                    apitransfer.EQ_P_Postcode__c = getValidAddressFieldValues('pcode',roleAddress);
                    apitransfer.EQ_P_Country_Code__c = getValidAddressFieldValues('country',roleAddress);
                }
            }
        }
    }

    // The rules covering address elements may be summarised as follows:
    // • A street type cannot be entered without a street name.
    // • If a street number consists of a range of numbers (e.g., 1-8), enter only the first number.
    // • If a street name consists of a street type (such as “The Avenue” or “The Boulevard”), enter the whole
    // string into the <street-name> element and leave out the <street-type> element. 

    // Character XML Reference
    // Left angle bracket < &lt;
    // Right angle bracket > &gt;
    // Ampersand & &amp;
    // Apostrophe ' &apos;
    // Quotation mark " &quot;

    // called in this class' setAddress method
    public static String getValidAddressFieldValues(String type, Role_Address__c radd){
        String returnVal = '';
        //String unitNum = '';//Suite, Level, Unit, Lot, Floor, Shop, Apt, Apartment, Villa, Studio, Flat, No.
        String strnum = radd.Address__r.Street_Number__c;
        String strname = radd.Address__r.Street_Name__c;
        String strtype = radd.Address__r.Street_Type__c;
        String sub = radd.Address__r.Suburb__c;
        String statecode = radd.Address__r.State_Acr__c;
        String pcode = radd.Address__r.Postcode__c;
        String country = radd.Address__r.Country__c;
        String fuladd = radd.Address__r.Full_Address__c;
        
        if(type == 'unitnum'){
            if(!String.isBlank(strnum)){
                if(strnum.containsIgnoreCase('/')){
                    strnum = strnum.substringBefore('/').replaceAll('\\D','');//get only the numbers
                    if(!String.isBlank(strnum)) return strnum.trim();
                    else{
                        return '';
                    }
                }
                else{
                    return '';
                }
            }
        }
        if(type == 'strnum'){
            if(!String.isBlank(strname) && String.isBlank(strnum)){
                return 'N/A';
            }
            else if(String.isBlank(strname) && String.isBlank(strnum)){
                return '';
            }
            else if(!String.isBlank(strnum)){ //strnum NOT BLANK
                if(strnum.containsIgnoreCase('/')){ // If strnum contains "/" Split it
                    strnum = strnum.substringAfter('/');
                    if(strnum.containsIgnoreCase('-')){ //If a street number consists of a range of numbers (e.g., 1-8), enter only the first number.
                        return strnum.substringBefore('-').trim(); 
                    }
                    else{ //If a street number DOES NOT have '-'.
                        return strnum.trim(); 
                    }
                }
                else if(!strnum.containsIgnoreCase('/')){ // If strnum DOES NOT contains "/"
                    if(strnum.containsIgnoreCase('-')){ //If a street number consists of a range of numbers (e.g., 1-8), enter only the first number.
                        return strnum.substringBefore('-').trim(); 
                    }
                    else{ //If a street number DOES NOT have '-'.
                        return strnum.trim(); 
                    }
                }
            }
        }
        if(type == 'strname'){
            return strname.trim(); 
        }
        if(type == 'strtype'){
            System.debug('strtype = '+ strtype);
            if(String.isBlank(strtype)) return '';
            else{
                strtype = strtype.replaceAll('[^a-zA-Z0-9\\s+]', ''); //remove all special characters in the street type
                System.debug('strtype != blank or null = '+ strtype);
                if(streetTypeCodeMap.isEmpty() && streetCodeMap.isEmpty()) getStreetTypeCodeMap();
                System.debug('streetTypeCodeMap values = '+ streetTypeCodeMap.values());
                if(streetTypeCodeMap.containsKey(strtype.toLowerCase())){ //Street
                    System.debug('streetTypeCodeMap.containsKey(strtype.toLowerCase())');
                    return streetTypeCodeMap.get(strtype.toLowerCase());
                }
                else if(streetCodeMap.containsKey(strtype.toLowerCase())){ //St.
                    System.debug('streetTypeCodeMap.containsKey(strtype.toLowerCase())');
                    return streetCodeMap.get(strtype.toLowerCase());
                }
                else{
                    System.debug('NOT streetTypeCodeMap.containsKey(strtype.toLowerCase())');
                    return strtype.trim()+'(error)';
                }
            }
        }
        if(type == 'sub'){
            return sub.trim(); 
        }
        if(type == 'statecode'){
            if(String.isBlank(statecode) && country.containsIgnoreCase('unlisted country')) return 'OS';
            else if(!String.isBlank(statecode)){
                if(!String.isBlank(country)){
                    if(country.containsIgnoreCase('australia')) return statecode.trim();
                    else if(country.containsIgnoreCase('unlisted country')) return 'OS';
                    else if(country.containsIgnoreCase('new zealand')) return 'NZ';
                    else return 'OS';
                }
            }
            else{
                return '';
            }
        }
        if(type == 'pcode'){
            if(String.isBlank(statecode)) return '';
            else if(!country.containsIgnoreCase('australia')) return '';
            else{
                return pcode.trim(); 
            }
        }
        if(type == 'country'){
            if(String.isBlank(country)) return '';
            else if(country.containsIgnoreCase('australia')) return 'AUS';
            else if(country.containsIgnoreCase('new zealand')) return 'NZ';
            else {
                return '';
            }
        }
        return null;
    }     

    //returns map key as street type , value as code
    //called in this class' getValidAddressFieldValues method
    public static void getStreetTypeCodeMap(){
        if(streetTypeCodeMap.isEmpty() && streetCodeMap.isEmpty()){
            for(Street_Type_Code_Table__mdt streetTypeCode : [SELECT Id,Street_Type__c,Street_Type_Code__c FROM Street_Type_Code_Table__mdt ORDER BY Street_Type__c ASC]){
                // streetTypeCodeMap.put(streetTypeCode.Street_Type_Code__c,streetTypeCode.Street_Type__c);
                streetTypeCodeMap.put(streetTypeCode.Street_Type__c.toLowerCase(),streetTypeCode.Street_Type_Code__c);
                streetCodeMap.put(streetTypeCode.Street_Type_Code__c.toLowerCase(),streetTypeCode.Street_Type_Code__c);
                //System.debug('streetTypeCode - '+streetTypeCode.Street_Type_Code__c + ' -- '+streetTypeCode.Street_Type__c);
            }
        }
        //return streetTypeCodeMap;
    }

    //called in this class' buildFormData method
    private static String setRelationshipCode(String roleRT){
        // - ROLE RT = Company Director, then Relationship Code = 4
        // - ROLE RT "contains" Guarantor, then Relationship Code = 3
        // ELSE
        // - Relationship Code = 1
        System.debug('roleRT == ' + roleRT);
        if(roleRT.containsIgnoreCase('Company Director')){
            // set to 4
            return '4';

        }
        else if(roleRT.containsIgnoreCase('Guarantor')){
            return '3';
        }
        else {
            return '1';
        }
    }

    //called in this lightning component to save the API Transfer record in SF
    @AuraEnabled
    public static API_Transfer__c saveAPITransfer(API_Transfer__c apitransfer){
        API_Transfer__c apiTransferToInsert = apitransfer;
        System.debug('saveAPITransfer -> apitransfer = '+apitransfer);
        try {
            if(apiTransferToInsert.Id != NULL) {
                API_Transfer__c dummyApiTransfer = [SELECT Id, Manual_Push_Count__c FROM API_Transfer__c WHERE Id =: apiTransferToInsert.Id];
                Integer manualPushCount;
                if(dummyApiTransfer.Manual_Push_Count__c == NULL){
                    manualPushCount = 0;
                }
                else{
                    manualPushCount = Integer.valueOf(dummyApiTransfer.Manual_Push_Count__c);
                } 
                apiTransferToInsert.Manual_Push_Count__c = manualPushCount+1;
                apiTransferToInsert.EQ_Request_Status__c = 'Created';
            }
            // Integer manualpushCount = .Manual_Push_Count__c;
            apiTransferToInsert.EQ_Client_Reference__c = apitransfer.EQ_Role__c+string.valueOf(System.now().getTime());
            Database.upsert(apiTransferToInsert);
        }
        catch (Exception e) {
            System.assertEquals('Script-thrown exception', e.getMessage());
            // throw new AuraHandledException('Darn it! Something went wrong: '+ e.getMessage());    
        }
        return apiTransferToInsert;
    }  

    //called in this class' initMethod method.
    private static List<String> getPicklistValues(Schema.DescribeFieldResult fieldSchema){
        List<String> pickListValuesList = new List<String>();
        Schema.DescribeFieldResult fieldResult = fieldSchema;
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList;
    }
    
    // wrapper or Inner class with @AuraEnabled {get;set;} properties*    
    public class wrapperClass{
        @AuraEnabled public List<String> permissionTypes{get;set;}
        @AuraEnabled public List<String> genderCodes{get;set;}
        @AuraEnabled public List<String> equiAccTypeCodes{get;set;}
        @AuraEnabled public List<String> equiRelationshipCodes{get;set;}
        @AuraEnabled public List<String> equiEmploymentTypes{get;set;}
        @AuraEnabled public List<String> equiStateCodes{get;set;}
        @AuraEnabled public List<String> equiCountryCodes{get;set;}
        @AuraEnabled public Application__c mainRec{get;set;} //Application  Rec
        @AuraEnabled public Boolean showRoleSelectionOnInit{get;set;}
        @AuraEnabled public Id selectedRoleId{get;set;} 
        @AuraEnabled public API_Transfer__c apiTransferData{get;set;} 
        @AuraEnabled public String equifaxEnquiryId {get;set;}
        @AuraEnabled public Boolean valid{get;set;} 
        @AuraEnabled public String errMsg{get;set;} 
    }
}
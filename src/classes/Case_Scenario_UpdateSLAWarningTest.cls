/** 
* @FileName: Case_Scenario_UpdateSLAWarningTest
* @Description: Test Class for SLA Warning Classes.
* @Copyright: Positive (c) 2018 
* @author: Rex David
* @Modification Log =============================================================== 
* 1.0 3/28/18 RDAVID Created unit test for Scenario SLA Warning
**/ 
@isTest
public class Case_Scenario_UpdateSLAWarningTest {

    @testsetup static void setup(){
        
    }

    static testmethod void testSLA1Warning(){
        // User sysAdUser = TestDataFactory.create_User_SysAd();
        // System.runAs(sysAdUser) {
            Test.startTest();
                Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                    Enable_Case_Scenario_Trigger__c = true,
                    Enable_Triggers__c = true,
                    Enable_Case_Trigger__c = true,
                    Enable_Case_SCN_recordStageTimestamps__c = true,
                    Enable_Case_SCN_calculateSLATime__c = true
                );
                insert trigset;

                Process_Flow_Definition_Settings1__c processFlowSet = new Process_Flow_Definition_Settings1__c(
                    Enable_Case_Scenario_Flow_Definitions__c = false
                );
                insert processFlowSet;

                General_Settings1__c genSettings = new General_Settings1__c(
                    CASE_SCENARIO_SLA1_DURATION__C = 2, //hours
                    CASE_SCENARIO_SLA2_DURATION__C = 76,	
                    CASE_SCENARIO_SLA3_DURATION__C = 4,
                    CASE_SCENARIO_SLA1_FIRST_WARNING__C = 120000, //milliseconds 2 minutes
                    CASE_SCENARIO_SLA2_FIRST_WARNING__C = 120000,
                    CASE_SCENARIO_SLA3_FIRST_WARNING__C = 120000,
                    CASE_SCENARIO_SLA_1_INTERVAL__C = 120000,
                    CASE_SCENARIO_SLA_2_INTERVAL__C = 120000,
                    CASE_SCENARIO_SLA_3_INTERVAL__C = 120000
                );
                insert genSettings;

                //Create Case - Commercial Scenerio
                List <Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_COMMERCIAL_SCENARIO,5); //n: Commercial Scenario
                for(Case caseIns : caseList){
                    caseIns.Status = 'New';
                    caseIns.Stage__c = 'Open';
                    caseList[0].Stage__c = 'Open';
                }
                insert caseList;

                caseList[0].Stage__c = 'Open';
                update caseList;
                //If "Case - Scenario Assignment" Process Builder is Active :
                //Stage = 'New Scenario'
                //Owner = Round Robin
                //"Case_New_Commercial_Scenario_Notification" Email Alert should be received 
                caseList = [Select Id,Stage__c, RecordType.Name FROM Case WHERE RecordType.Name =: CommonConstants.CASE_RT_N_COMMERCIAL_SCENARIO];
                List<Id> caseScenIDs = new List<Id>();
                for(Case caseIns : caseList){
                    caseScenIDs.add(caseIns.Id);
                }
                Case_Scenario_UpdateSLA1Warning.scenarioSLAWarningTimeSA(caseScenIDs);
                System.assertEquals(caseList.size(),5);
                for(Case caseIns : caseList){
                    System.assertEquals(caseIns.Stage__c,CommonConstants.SCEN_STAGE_OPEN);
                    caseIns.Stage__c = CommonConstants.SCEN_STAGE_WINTRODUCER;
                    caseList[0].Stage__c = CommonConstants.SCEN_STAGE_COMPLETE;
                }
                update caseList;
                caseList = [Select Id,Stage__c, RecordType.Name FROM Case WHERE Stage__c =: CommonConstants.SCEN_STAGE_WINTRODUCER];
                System.assertEquals(caseList.size(),4);

                caseScenIDs = new List<Id>();
                for(Case caseIns : caseList){
                    caseScenIDs.add(caseIns.Id);
                    caseIns.Stage__c = CommonConstants.SCEN_STAGE_WLENDER;
                }
                Case_Scenario_UpdateSLA2Warning.scenarioSLAWarningTimeWI(caseScenIDs);
                update caseList;
                caseList = [Select Id,Stage__c, RecordType.Name FROM Case WHERE Stage__c =: CommonConstants.SCEN_STAGE_WLENDER];
                caseScenIDs = new List<Id>();
                for(Case caseIns : caseList){
                    caseScenIDs.add(caseIns.Id);
                }
                Case_Scenario_UpdateSLA3Warning.scenarioSLAWarningTimeWL(caseScenIDs);
            Test.stopTest();
        // }
    }
}
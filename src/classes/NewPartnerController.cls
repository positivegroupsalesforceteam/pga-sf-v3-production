/** 
* @FileName: NewPartnerController
* @Description: Main controller for NewPartnerCmp Lightning component
* @Copyright: Positive (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 31/05/19 RDAVID - extra/task24746570-NewPartnerPageLightning - Initial version
* 1.1 13/09/19 JBACULOD extra/tasks25900826-LeadToPartner - Added getNodifiPartnerLead for converting Lead to Partner
* 1.2 04/10/19 JBACULOD extra/tasks25900826-LeadToPartner - Added Partition, Channel field mapping for Lead to Partner
* 1.3 07/10/19 JBACULOD extra/tasks26291984 - Added Box Folder ID field mapping for Lead to Partner
* 1.4 16/10/19 JBACULOD extra/tasks26423931-LeadtoPartner-BusiSummary - Added Business Summary field mapping for Lead to Partner
* 1.5 08/01/2020 JBACULOD SFBAU-161 - Populate Latest Comment
* 1.6 22/01/2020 ICRUZ SFBAU-37 - Added linkRelatedToPartner for updating either Legal Entity Name and Full Address field of Partner__c Custom Object.
**/ 
public with sharing class NewPartnerController {

    @AuraEnabled
    public static Lead updateNodifiPartnerLeadToWon(Lead leadObj, ID partnerID){
        TriggerFactory.fromNewPartnerPage = true;
        system.debug('@@partnerID: '+partnerID);
        //Copy notes from Nodifi Partner Lead
        if (partnerID != null){
            //Retrieve Notes from Lead
            set <Id> conDocsIds = new set <Id>();
            list <ContentDocumentLink> cdlinks = [Select Id, ContentDocumentId, LinkedEntityId From ContentDocumentLink Where LinkedEntityId = : leadObj.Id];
            for (ContentDocumentLink cdlink : cdlinks){
                conDocsIds.add(cdlink.ContentDocumentId);
            }
            if (conDocsIds.size() > 0){
                list <ContentVersion> snotes = [Select Id, ContentDocumentId, IsLatest, Title, PathOnClient, VersionData, FileType From ContentVersion Where IsLatest = true AND FileType = 'SNOTE' AND ContentDocumentId in : conDocsIds Order By CreatedDate ASC];
                system.debug('@@Lead snotes: '+snotes);
                if (snotes.size() > 0){
                    //Create sharing of copied Notes for Partner
                    list <ContentDocumentLink> pcdlinks  = new list <ContentDocumentLink>();
                    for (ContentVersion csnote : snotes){
                        ContentDocumentLink cdlink = new ContentDocumentLink(
                            ContentDocumentId = csnote.ContentDocumentId,
                            LinkedEntityId = partnerID,
                            ShareType = 'I',
                            Visibility = 'AllUsers'
                        );
                        pcdlinks.add(cdlink);
                    }
                    insert pcdlinks;
                }
            }
        }
        update leadObj;
        return leadObj;
    }

    @AuraEnabled(cacheable=true)
    public static Lead getNodifiPartnerLead(Id recID){
        lead ld;
        string leadQuery = 'Select Id, FirstName, LastName, Email, Phone, LeadSource, Status, Stage__c, Attempted_Contact_Status__c, Partition__c, Channel__c, Box_Folder_ID__c, Owner.Name, OwnerId, Event__c, Latest_Comment__c, ';
        leadQuery+= 'On_Hold__c, Callback_Set_Date_Time__c, Business_Name__c, Business_Summary__c, ABN__c, ABN__r.Name, Current_Aggregator__c, Current_Aggregator__r.Name ';
        leadQuery+= ' From Lead Where Id = : recID';
        ld = database.query(leadQuery);
        system.debug('@@ld: '+ld);
        return ld;
    }

    @AuraEnabled(cacheable=true)
    public static Map<String,String> getNewPartnerValidRTMap(){
        Map<String,String> newPartnerValidRTMap = new Map<String,String>();

        User currUser = [SELECT Id, Profile.Name FROM User WHERE Id =: userinfo.getuserid()];
        Boolean isSysAd = (currUser.Profile.Name.containsIgnoreCase('Admin')) ? true : false;
        
        for(New_Partner_Lightning_Settings__mdt nplsMDT : Database.query(getNewPartnerLightningSettingQuery(''))){
           if((nplsMDT.For_Sys_Ad_Only__c && isSysAd) || (!nplsMDT.For_Sys_Ad_Only__c && (isSysAd || !isSysAd))){
               newPartnerValidRTMap.put(nplsMDT.Partner_RT__c,nplsMDT.Partner_RT_Id__c);
           }
        }
        System.debug('newPartnerValidRTMap keyset : '+ isSysAd  + ' - ' + newPartnerValidRTMap.keyset());
        return newPartnerValidRTMap;
    }

    @AuraEnabled(cacheable=true)
    public static Map<String, List<FieldSetForm>> getForm(Id recordId, String objectName, String rtName, String branchParentId) {
        
        Map<String,New_Partner_Lightning_Settings__mdt> npCustomMetaMap = new Map<String,New_Partner_Lightning_Settings__mdt>();
        Map<String, List<FieldSetForm>> rtFormMapInstance = new Map<String, List<FieldSetForm>>();
        User currUser = [SELECT Id, Profile.Name FROM User WHERE Id =: userinfo.getuserid()];
        for(New_Partner_Lightning_Settings__mdt nplsMDT : Database.query(getNewPartnerLightningSettingQuery(' AND Partner_RT__c = \''+ rtName +'\''))){

            // [SELECT Id, Is_Active__c, MasterLabel, Partner_RT__c, Partner_RT_Id__c, Field_Set_API_Names1__c, Partner_Branch_RT__c, Partner_Branch_RT_Id__c,Field_Set_API_Names_Branch__c FROM New_Partner_Lightning_Settings__mdt WHERE Is_Active__c = TRUE AND Partner_RT__c =: rtName]){
            
            // npCustomMetaMap.put(nplsMDT.Partner_RT__c,nplsMDT);
            
            System.debug('nplsMDT.Partner_RT__c --> '+nplsMDT.Partner_RT__c);
            List<String> fieldsetsAPINameList = (String.isEmpty(branchParentId)) ? nplsMDT.Field_Set_API_Names1__c.split('\r\n') : nplsMDT.Field_Set_API_Names_Branch__c.split('\r\n');
            
            if(fieldsetsAPINameList.size() > 0){
                
                for(String fieldSetAPI : fieldsetsAPINameList){
                    System.debug('fieldSetAPI --> '+fieldSetAPI);
                    FieldSetForm form = new FieldSetForm();
                    form.Fields = getFields(recordId, objectName, fieldSetAPI.substringAfter(':'));
                    form.SectionLabel = fieldSetAPI.substringBefore(':');
                    form.SectionId = fieldSetAPI.substringAfter(':');
                    form.BranchRTName = nplsMDT.Partner_Branch_RT__c;
                    form.BranchRTId = nplsMDT.Partner_Branch_RT_Id__c;
                    form.currentUser = currUser;
                    if(!rtFormMapInstance.containsKey(nplsMDT.Partner_RT__c)){
                        rtFormMapInstance.put(nplsMDT.Partner_RT__c,new List<FieldSetForm> {form});
                    }
                    else{
                        List<FieldSetForm> existingFieldSetFormList = rtFormMapInstance.get(nplsMDT.Partner_RT__c);
                        existingFieldSetFormList.add(form);
                        rtFormMapInstance.put(nplsMDT.Partner_RT__c,existingFieldSetFormList);
                    }

                }
            }
        }
        return rtFormMapInstance;
    }

    @AuraEnabled(cacheable=true)
    public static User getCurrentUser(){
        return [SELECT Id, Profile.Name FROM User WHERE Id =: userinfo.getuserid()];
    }
    
    private static List<Field> getFields(Id recordId, String objectName, String fieldSetName) {
        Schema.SObjectType objectType = null;
        
        if (recordId != null) {
            objectType = recordId.getSobjectType();
        }
        else if (String.isNotBlank(objectName)) {
            objectType = Schema.getGlobalDescribe().get(objectName);
        }
        
        Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();
        Map<String, Schema.FieldSet> fieldSetMap = objectDescribe.fieldSets.getMap();
        Schema.FieldSet fieldSet = fieldSetMap.get(fieldSetName);
        List<Schema.FieldSetMember> fieldSetMembers = fieldSet.getFields();

        List<Field> fields = new List<Field>();
        for (Schema.FieldSetMember fsm : fieldSetMembers) {
            Field f = new Field(fsm);

            fields.add(f);
        }

        return fields;
    }

    @AuraEnabled(cacheable=true)
    public static List<Address__c> getAddress(String nameFilterString) {
        System.debug('nameFilterString == '+nameFilterString);

        String composedFilter = sanitizeQueryString(nameFilterString);

        // List<Address__c> address =[SELECT Id, Name, Full_Address__c FROM Address__c WHERE Full_Address__c LIKE: composedFilter  ORDER BY Name DESC LIMIT :pageSize OFFSET :(pageSize*pageNumber)];
        List<Address__c> address =[SELECT Id, Name, Full_Address__c FROM Address__c WHERE Full_Address__c LIKE: composedFilter  ORDER BY Name DESC LIMIT 2000];
        System.debug('getAddress **************** - ' + address.size());
        //Add isAccessible() check
        return address;
    }

    @AuraEnabled(cacheable=true)
    public static List<ABN__c> getABNs(String nameFilterString) {
        System.debug('nameFilterString == '+nameFilterString);

        String composedFilter = sanitizeQueryString(nameFilterString);

        // List<Address__c> address =[SELECT Id, Name, Full_Address__c FROM Address__c WHERE Full_Address__c LIKE: composedFilter  ORDER BY Name DESC LIMIT :pageSize OFFSET :(pageSize*pageNumber)];
        List<ABN__c> abnList =[SELECT Id, Name, Registered_ABN__c FROM ABN__c WHERE Name LIKE: composedFilter OR Registered_ABN__c LIKE: composedFilter  ORDER BY CreatedDate DESC LIMIT 2000];
        System.debug('getABNs **************** - ' + abnList.size());
        //Add isAccessible() check
        return abnList;
    }

    /*
        * Method Name: linkRelatedToPartner
        * Date Created: January 22, 2020
        * Description: Used to update Partner record and update either
                       Legal Entity Name (ABN) or Partner Adderess Field.
        * Parameters Used:
            * partnerID     = ID of the record (Partner__c Custom Object) 
                              that needs to be updated.
            * relatedID     = ID of either ABN Record (ABN__c Custom Object) or
                              Address Record (Address__c Custom Object).
            * typeOfSidebar = Name of Navigation in the Sidebar.
                              (See NewPartnerSidebarMenu for list of Navigation)
        * JIRA: SFBAU-37
    */
    
    @AuraEnabled
    public static void linkRelatedToPartner(String partnerID, String relatedID, String typeOfSidebar) {

        System.debug('>>> PARTNER ID <<< ' + partnerID);
        System.debug('>>> RELATED ID <<< ' + relatedID);
        System.debug('>>> TYPE OF SIDEBAR <<< ' + typeOfSidebar);

        if (String.isNotBlank(partnerID) && String.isNotBlank(relatedID) && String.isNotBlank(typeOfSidebar)) {

            System.debug('>>> PARTNER ID, RELATED ID AND TYPE OF SIDEBAR HAVE VALUE <<<');

            List<Partner__c> partnerList = [SELECT Id,
                                                    Legal_Entity_Name__c,
                                                    Partner_Address__c
                                            FROM Partner__c
                                            WHERE (Id = :partnerID)
                                           ];

            System.debug('>>> LIST OF PARTNER SIZE <<< ' + partnerList.size());
            System.debug('>>> LIST OF PARTNER RECORD <<< ' + partnerList);            

            if (partnerList.size() > 0) {
                if (typeOfSidebar.equals('legal')) {
                    partnerList[0].Legal_Entity_Name__c = relatedID;
                } else if (typeOfSidebar.equals('address')) {
                    partnerList[0].Partner_Address__c = relatedID;
                }
                update partnerList;
            }

        }
        
    }

    static String sanitizeQueryString(String aQuery) {
        if (aQuery == null) return '%';
        
        String trimmedQuery = aQuery.trim();
        if (trimmedQuery.length() == 0) return '%';
        return '%' + trimmedQuery.replaceAll('\\W+', '%') + '%';
    }

    static String getNewPartnerLightningSettingQuery(String andString){
        String selectStr = 'SELECT Id, Is_Active__c, MasterLabel, Partner_RT__c, Partner_RT_Id__c, Field_Set_API_Names1__c, Partner_Branch_RT__c, Partner_Branch_RT_Id__c,Field_Set_API_Names_Branch__c,For_Sys_Ad_Only__c ';
        String fromStr = 'FROM New_Partner_Lightning_Settings__mdt ';
        String whereStr = 'WHERE Is_Active__c = TRUE ';
        String andStr = andString;
        String orderByStr = 'ORDER BY Partner_RT__c ASC';
        return selectStr+fromStr+whereStr+andStr+orderByStr;
    }
    
    public class FieldSetForm {
        @AuraEnabled
        public List<Field> Fields { get; set; }
        @AuraEnabled
        public String SectionLabel { get; set; }
        @AuraEnabled
        public String SectionId { get; set; }
        @AuraEnabled
        public String BranchRTName { get; set; }
        @AuraEnabled
        public String BranchRTId { get; set; }
        @AuraEnabled
        public User currentUser { get; set; }
        
        public FieldSetForm() {
            Fields = new List<Field>();
            SectionLabel = '';
            SectionId = '';
            BranchRTName = '';
            BranchRTId = '';
            currentUser = new User();
        }
    }
    
}
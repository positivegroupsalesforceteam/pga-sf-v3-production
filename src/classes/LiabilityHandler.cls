/** 
* @FileName: LiabilityHandler
* @Description: Trigger Handler for the Liability1__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/15 RDAVID Created Class
* 1.1 2/16 JBACULOD Added Method Trigger toggle for object
* 1.2 4/24/18 JBACULOD Added AutoCreaate FO Share
* 1.3 6/26/18 JBACULOD Added deleteFOShareOnFODelete
* 1.4 19/10/18 RDAVID - Optimization
* 1.5 11/03/19 JBACULOD - Added retry on final due to UNABLE_TO_LOCK_ROW issue
**/ 

public without sharing class LiabilityHandler implements ITrigger {	

	private Set<Id> roleIds = new Set<Id>(); //Parent Role Ids
	//SD-81 SF - NM - Auto Create FO Share in Liability
	private Map <string, FO_Share__c> FOShareToCreateMap = new map <string, FO_Share__c>();
	//SD-XX
	private Map <string, FO_Share__c> FOShareToDeleteMap = new map <string, FO_Share__c>();

	// Constructor
	public LiabilityHandler(){

	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
		//Delete FO Share on FO Delete
		if(Trigger.isDelete){
			if (TriggerFactory.trigSet.Enable_FO_Delete_FO_Share_on_FO_Delete__c){
					system.debug('@@test');
					FOShareToDeleteMap = RollupUtility.deleteFOShareOnFODelete('Liability1__c', Trigger.old);
			}
		}
	}
	
	public void bulkAfter(){
		
		if(Trigger.IsInsert || Trigger.isUpdate){
			//Get Parent Role Ids
			roleIds = LiabilityGateway.getRoleIds(Trigger.new);
			//Auto Create FO Share
			if (TriggerFactory.trigSet.Enable_FO_Auto_Create_FO_Share__c){
				if (Trigger.IsInsert){
					FOShareToCreateMap = RollupUtility.autocreateFOShare('Liability1__c', Trigger.new, true);
				}
                if(Trigger.isUpdate){
					FOShareToCreateMap = RollupUtility.autocreateFOShare('Liability1__c', Trigger.new, false);
					//Delete FO Share on removed Roles
					FOShareToDeleteMap = RollupUtility.deleteFOSHareOnRemovedRoles('Liability1__c', Trigger.new);
				}
 			}
		}
	}
		
	public void beforeInsert(SObject so){
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		Liability1__c oldlia = (Liability1__c) oldSo;
        Liability1__c lia = (Liability1__c) so;
		if (!lia.h_Auto_Create_FO_Share__c) lia.h_Auto_Create_FO_Share__c = TRUE; //Updated due to manual Fo Shares being created for Nodifi
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){

	}
	
	public void afterUpdate(SObject oldSo, SObject so){

	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		//Auto Create FO Share for Assets
		if (!FOShareToCreateMap.isEmpty()){
			try{
				Database.upsert(FOShareToCreateMap.values());
			}
			catch (Exception e){ //Retry due to UNABLE_LOCK_ROW issue
				Database.upsert(FOShareToCreateMap.values());
			}
		}
		//Delete created FO Share when Asset got deleted
		if (FOShareToDeleteMap.size() > 0){
			Database.delete(FOSHareToDeleteMap.values());
		}
	}
}
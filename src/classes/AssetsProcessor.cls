public class AssetsProcessor extends DefaultProcessor {
    Map<String,String> abbreviations = new Map<String,String>{'Australian Capital Territory'=>'ACT','New South Wales'=>'NSW','Northern Territory'=>'NT','Queensland'=>'QLD','South Australia'=>'SA','Tasmania'=>'TAS',
                                        'Victoria'=>'VIC','Western Australia'=>'WA','Other (Outside Australia)'=>'Other'};
    CustomStringBuilder cStringBuilder = LKIntegration.cStringBuilder;
    public override boolean process() {
        if(!super.process()) {
            return false;
        }
        List<LKRecord> lstLKContact = new List<LKRecord>();
        lstLKContact.addAll(XMLMappingAdapter.lkRecordsMap.get('PersonAccount'));
        List<LKRecord> applicant2List = XMLMappingAdapter.lkRecordsMap.get('Applicant2');
        XMLMapping mappObj = XMLMappingAdapter.xmlNodesMap.get('Address');
        if(applicant2List!=null && applicant2List.size()>0) {
            lstLKContact.addAll(applicant2List);
        }
        List<LkRecord> lKRecordsList= XMLMappingAdapter.lkRecordsMap.get(getNodeName());//single item map       
        for(Integer i=0 ; i < lKRecordsList.size();i++) {
            List<String> ownersList;
            List<String> shareList;
            String loankitOwnerId;
            LKRecord objLkRec = lKRecordsList.get(i);
            if(objLKRec.isProcessed) {
                continue;
            }
            objLKRec.checkForUpdate = true;
            String ownerId = String.valueOf(objLkRec.lkValuesMap.get('Account__c'));
            if(ownerId == null) {
                ownerId = String.valueOf(objLkRec.lkValuesMap.get('Applicant_2_Mapping__c'));
            }
            for(LKRecord lkCon : lstLKContact) {
                if(ownerId != null && !ownerId.equalsIgnoreCase('null')) {
                    if(lkCon.sfObjectId.equalsIgnoreCase(ownerId)) {
                        loankitOwnerId = lkCon.lkObjectId;
                    }
                }
            }
            for(Integer j=0; j<i; j++) {
                LKRecord lkRecInner = lKRecordsList.get(j);
                if(objLkRec.sfobjectId.equalsIgnoreCase(lkRecInner.sfObjectId)){               
                    List<String> responseLKOwnerIdList = new List<String>();
                    List<String> percentageOwnedLKList = new List<String>();
                    Object o = lkRecInner.lkValuesMap.get('owners');
                    if(o instanceOf List<String>) {
                        responseLKOwnerIdList.addAll((List<String>)o);
                        percentageOwnedLKList.addAll((List<String>)lkRecInner.lkValuesMap.get('percentage_owned'));
                    }else if(o instanceOf String){
                        responseLKOwnerIdList.add(String.valueOf(o));
                        percentageOwnedLKList.add(String.valueOf(lkRecInner.lkValuesMap.get('percentage_owned')));
                    }
                    ownersList = responseLKOwnerIdList;
                    shareList = percentageOwnedLKList;
                }
            }
            if(!objLkRec.isProcessed) {
                Map<String,Object> valMap = objLkRec.lkValuesMap; 
                if(ownersList != null) { 
                    ownersList.add(loankitOwnerId);
                    shareList.add((String)valMap.get('percentage_owned'));
                    valMap.put('owners',ownersList);
                    valMap.put('percentage_owned',shareList); 
                }else {
                    valMap.put('owners',loankitOwnerId);
                }
                valMap.put('other_asset_type','Yes');                 
                String fValue = (String)valMap.get('asset_type');
                if(fValue !=null && fValue!=''){
                    if(fValue.equals('Deposit Account')) {
                        valMap.put('asset_type','DepositAccount');
                        String depAccType = (String)valMap.get('deposit_account_type');//).replaceAll(' ',''));
                        if(depAccType != null) {
                            if(depAccType.equalsIgnoreCase('Other Account')) {
                                depAccType = 'OtherDepositAccount';
                            }
                            depAccType = depAccType.replace(' ','');
                            valMap.put('deposit_account_type',depAccType);
                        }
                        valMap.remove('vehicle_make');
                        valMap.remove('vehicle_year');
                        valMap.remove('purchase_date');
                        valMap.remove('property_is_being_purchased');
                        valMap.remove('monthly_rental_income');
                        valMap.remove('address_id');       
                    }else if(fValue.equals('Motor Vehicle')){
                        valMap.put('asset_type','MotorVehicle');
                        valMap.remove('deposit_account_type');
                        valMap.remove('purchase_date');
                        valMap.remove('property_is_being_purchased');
                        valMap.remove('monthly_rental_income');
                        valMap.remove('address_id');   
                    }else if(fValue.equals('Real Estate')){
                        String streetName = (String)valMap.get('Street_Name__c');
                        String streetNum = (String)valMap.get('Street_Number__c');
                        String streetType = (String)valMap.get('Street_Type__c');
                        String suburb = (String)valMap.get('Suburb__c');
                        String state = (String)valMap.get('State__c');
                        String postcode = (String)valMap.get('Postcode__c');
                        String country = (String)valMap.get('Country__c');
                        
                        Map<String,String> requestBody = new Map<String,String>();
                        requestBody.put('street',streetName);
                        requestBody.put('street_number',streetNum);
                        requestBody.put('state',abbreviations.get(state));
                        requestBody.put('postcode',postcode);
                        requestBody.put('country',country);
                        requestBody.put('city',suburb);
                        requestBody.put('address_standard','Standard');
                        requestBody.put('street_type',streetType);
                        //requestBody.put('address_type','PreSettlement Mailing');
                        LKIntegration.putAppId(requestBody,String.valueOf(LKIntegration.mapGlobal.get(LKConstant.APPLICATION_ID)));
                        LKIntegration.putCommonData(requestBody);
                        String addressId;
                        if(mappObj != null) {
                            String url = mappObj.getEndPointUrl(false,null);
                            url = url+LKIntegration.mapGlobal.get('contact_id')+'?serializer=JSON';
                            String requestMethod = mappObj.getReqMethod(false);
                            String jsonString = LKIntegration.getJsonString(requestBody);
                            HttpResponse resp = LKIntegration.sendRequest(jsonString,url,requestMethod);
                            if(resp!=null) {
                                String respString = resp.getBody();
                                Map<String,String> mapResponse = LKIntegration.convertToMap(respString);
                                if(!mapResponse.containsKey('error')) {
                                    addressId = mapResponse.get('address_id');
                                    //emplRec.lkObjectId2 = addressId;
                                    //emplRec.fieldToUpdate2 = 'LK_Employment_Addr_ID__c';
                                }
                            }
                        }
                        valMap.put('address_id',addressId);
                        valMap.put('asset_type','RealEstate');
                        valMap.remove('deposit_account_type');
                        valMap.remove('vehicle_make');
                        valMap.remove('vehicle_year');
                    }else if(fValue.equals('Charge over cash') || fValue.equals('Debenture Charge') || fValue.equals('Home Contents') || fValue.equals('Tools of Trade') || fValue.equals('Managed Fund')
                     || fValue.equals('Superannuation') || fValue.equals('Life Insurance') || fValue.equals('Shares') || fValue.equals('Boat') || fValue.equals('Guarantee')
                     || fValue.equals('Collections') || fValue.equals('Receivables') || fValue.equals('Other')){
                        String modfValue = fValue.deleteWhitespace();
                        valMap.put('asset_type',modfValue);
                        removeElementsFromMap(valMap); 
                    }else if(fValue.equals('Goodwill of Business')){
                        valMap.put('asset_type','Goodwill');
                        removeElementsFromMap(valMap);
                    }else if(fValue.equalsIgnoreCase('Personal Equity in any Private Business')){
                        valMap.put('asset_type','PersonalEquity');
                        removeElementsFromMap(valMap); 
                    }else if(fValue.equals('Stock & Machinery')){
                        valMap.put('asset_type','StockMachinery');
                        removeElementsFromMap(valMap);
                    }else if(fValue.equalsIgnoreCase('Cash Savings')||fValue.equalsIgnoreCase('Personal')||fValue.equalsIgnoreCase('Property')
                             || fValue.equalsIgnoreCase('Equipment')|| fValue.equalsIgnoreCase('Household goods')) {
                        valMap.put('asset_type','Other');
                        removeElementsFromMap(valMap);
                    }
                }
            }
        }
        return true;
    }
    private Map<String,Object> removeElementsFromMap(Map<String,Object> valMap ){
        valMap.remove('deposit_account_type');
        valMap.remove('vehicle_make');
        valMap.remove('vehicle_year');
        //valMap.remove('purchase_date');
        //valMap.remove('property_is_being_purchased');
        //valMap.remove('monthly_rental_income');
        valMap.remove('address_id'); 
        return valMap;
    } 
    
}
/** 
* @FileName: ContactHandler
* @Description: Trigger Handler for the Role__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 20/12/18 RDAVID: The requirement is to keep the Partner objects (Partner, Person Account, Relationship) and Contact records in sync. 
**/ 
 
public without sharing class ContactHandler implements ITrigger {	
    
    private List<Contact> contactList = new List<Contact>();
    private Id connectiveBrokerRTId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Connective_Broker').getRecordTypeId();
    private Id businessRTId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId();
    
    private Id nmIndvidualId = SObjectType.Account.getRecordTypeInfosByDeveloperName().get('nm_Individual').getRecordTypeId();
    

    private Map<Contact,Account> conAccMap = new Map<Contact,Account>();
    private Map<Contact,Relationship__c> conRelMap = new Map<Contact,Relationship__c>();

    private List<Account> accountToInsert = new List<Account>();
    private List<Account> accountToUpdate = new List<Account>();
    private Set<Id> accountDelIds = new Set<Id>();
    private List<Account> accountDelete = new List<Account>();

    private Id connectiveId;

    private Map<Contact,String> referralCompanyName = new Map<Contact,String>();
    private Map<Id,String> conIdreferralCompanyName = new Map<Id,String>();

    private Map<String,Referral_Company__c> referralCompanyMap = new Map<String,Referral_Company__c>();
    private Map<String,Partner__c> partnerMap = new Map<String,Partner__c>();

    private Map<String,Partner__c> contactReferralCompanyMap = new Map<String,Partner__c>();

    // private Set<Id> conIds = new Set<Id>();
    private List<Relationship__c> relationshipToDelete = new List<Relationship__c>();

    public ContactHandler(){
        contactList = (Trigger.IsInsert || Trigger.IsUpdate) ? (List<Contact>) Trigger.new : (Trigger.IsDelete) ? (List<Contact>) Trigger.old : null;
    }

    public void bulkBefore(){
        
    }

    public void bulkAfter(){
        Map<Id,Contact> oldContactMap = (Trigger.isUpdate)? (Map<Id,Contact>)Trigger.oldMap : null;
        Map<Id,Contact> newContactMap = (Trigger.isUpdate)? (Map<Id,Contact>)Trigger.newMap : null;
        Set<Id> conIds = new Set<Id>();
        for(Contact con : contactList){
            if(TriggerFactory.trigSet.Enable_Partner_Sync_Account_Contact__c){
                if((con.RecordTypeId == connectiveBrokerRTId || con.RecordTypeId == businessRTId) && con.Referral_Company__c != NULL){
                    if(Trigger.IsInsert){
                        Account acc = ContactGateway.getAccountFromCon(nmIndvidualId,con);
                        accountToInsert.add(acc);
                        conAccMap.put(con,acc); //used to reference Account Id after insert
                        referralCompanyName.put(con,con.Referral_Company_Name__c);       
                    }
                    if(Trigger.IsUpdate){
                        Contact oldCon = oldContactMap.get(con.Id);
                        if(ContactGateway.isContactMainFieldUpdated(con,oldcon)){
                            conIdreferralCompanyName.put(con.Id,con.Referral_Company_Name__c);
                        }
                    }
                }
                conIds.add(con.Id);
            }
        }          
        //Insert
        if(Trigger.IsInsert && !referralCompanyName.isEmpty()){
            for(Referral_Company__c refComp : [SELECT Id, Name, Company_Type__c, Rating__c FROM Referral_Company__c WHERE Name IN: referralCompanyName.values()]){
                referralCompanyMap.put(refComp.Name,refComp);
            }

            for(Partner__c partner : [SELECT Id, Name, Partner_Type__c, m_Rating__c, m_Company_Type__c FROM Partner__c WHERE (Name IN: referralCompanyName.values() OR Name = 'Connective')]){
                if(partner.Name != 'Connective'){
                    partnerMap.put(partner.Name,partner);
                }
                else if (partner.Name == 'Connective'){
                    connectiveId = partner.Id;
                }
            }
        }
        //Update
        if(Trigger.IsUpdate && !conIdreferralCompanyName.isEmpty()){
            System.debug('@@86 - '+conIdreferralCompanyName.keyset());
            for(Relationship__c relationship : [SELECT Id, Associated_Account__c,h_ContactId__c FROM Relationship__c WHERE h_ContactId__c IN: conIdreferralCompanyName.keyset()]){
                Account acc = ContactGateway.getAccountFromCon(nmIndvidualId,newContactMap.get(relationship.h_ContactId__c));
                acc.Id = relationship.Associated_Account__c;
                System.debug('@@90 - '+acc);
                accountToUpdate.add(acc);
            }
        }
        if(Trigger.IsDelete){
            for(Relationship__c rel : [SELECT Id, Associated_Account__c FROM Relationship__c WHERE h_ContactId__c IN: conIds]){
                //accountDelIds.add(rel.Associated_Account__c);
                rel.End_Date__c = System.today();
                relationshipToDelete.add(rel);
            }
            /* 28/12/18 2:56pm RDAVID Removed Logic based on updated spec from Raoul
            if(accountDelIds.size() > 0)
                accountDelete = [SELECT Id FROM Account WHERE Id IN: accountDelIds];*/
        }
    }

    public void beforeInsert(SObject so){

    }

    public void beforeUpdate(SObject oldSo, SObject so){

    }
    
    public void beforeDelete(SObject so){	
	
    }

    public void afterInsert(SObject so){

    }

    public void afterUpdate(SObject oldSo, SObject so){

    }

    public void afterDelete(SObject so){
	}
    
    public void andFinally(){
        if(accountToInsert.size() > 0){
            System.debug('@@58 == '+accountToInsert);
            Database.insert(accountToInsert);
            System.debug('@@58 == '+conAccMap);
            if(conAccMap.keySet().size() > 0){
                conRelMap = ContactGateway.getConRelMap(referralCompanyMap,referralCompanyName,partnerMap,conAccMap,connectiveId);
                System.debug('@@127 == '+conRelMap.values().size());
                Database.insert(conRelMap.values());
            }
        }
        if(accountToUpdate.size() > 0){
            System.debug('@@130 == '+accountToUpdate);
            Database.update(accountToUpdate);
        }
        /*/* 28/12/18 2:56pm RDAVID Removed Logic based on updated spec from Raoul if(accountDelete.size() > 0){
            Database.delete(accountDelete);
        }*/
        if(relationshipToDelete.size() > 0){
            Database.update(relationshipToDelete);
        }
    }
}
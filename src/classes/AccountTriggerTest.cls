/** 
* @FileName: AccountTriggerTest
* @Description: Test class for FOShare Trigger 
* @Copyright: Positive (c) 2018 
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 19/5/18 RDAVID Created class
* 1.1 30/7/18 JBACULOD added testpushAssocRelationshipsInRole
* 1.2 20/3/19 JBACULOD added testAddCampaignMemberAndDelacon
**/ 
@isTest
private class AccountTriggerTest {
	@testsetup static void setup(){
        //Turn On Trigger 
		Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
		triggerSettings.Enable_Triggers__c = true;
		triggerSettings.Enable_Application_Sync_App_2_Case__c = true;
		triggerSettings.Enable_Application_Trigger__c= true;
		triggerSettings.Enable_Asset_Trigger__c= true;
		triggerSettings.Enable_Case_Auto_Create_Application__c= true;
		triggerSettings.Enable_Case_Scenario_Trigger__c= true;
		triggerSettings.Enable_Case_SCN_calculateSLATime__c= true;
		triggerSettings.Enable_Case_SCN_recordStageTimestamps__c= true;
		triggerSettings.Enable_Case_Trigger__c= true;
		triggerSettings.Enable_Expense_Trigger__c= true;
		triggerSettings.Enable_FO_Auto_Create_FO_Share__c= true;
		triggerSettings.Enable_FOShare_Rollup_to_Role__c= true;
		triggerSettings.Enable_FOShare_Trigger__c= true;
		triggerSettings.Enable_Income_Trigger__c= true;
		triggerSettings.Enable_Lender_Automation_Call_Lender__c= true;
		triggerSettings.Enable_Liability_Trigger__c= true;
		triggerSettings.Enable_Role_Address_Populate_Address__c= true;
		triggerSettings.Enable_Role_Address_Toggle_Current__c= true;
		triggerSettings.Enable_Role_Address_Trigger__c= true;
		triggerSettings.Enable_Role_Auto_Populate_Case_App__c= true;
		triggerSettings.Enable_Role_Rollup_to_Application__c= true;
		triggerSettings.Enable_Role_To_Account_Propagation__c= true;
		triggerSettings.Enable_Role_Toggle_Primary_Applicant__c= true;
		triggerSettings.Enable_Role_Trigger__c= true;
		triggerSettings.Enable_Triggers__c= true;
        triggerSettings.Enable_Account_Sync_Name_to_Role__c=true;
        triggerSettings.Enable_Account_Trigger__c =true;
		triggerSettings.Enable_Partner_Sync_Account_Contact__c=true;
		triggerSettings.Enable_Account_Add_as_Campaign_Member__c =true;
		triggerSettings.Enable_Account_Is_Delacon_Lead__c = true;
		upsert triggerSettings Trigger_Settings1__c.Id;
		
        
		//TestDataFactory.Case2FOSetupTemplate();
        //Create Test Account
		List<Account> accList = TestDataFactory.createGenericPersonAccount('fName', 'lName', CommonConstants.PACC_RT_I, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		// caseList[0].Lead_Type_Set_to_Convert__c = 'p: Consumer - Asset Finance';
		//caseList[0].Lead_Application_Type_Set_to_Convert__c = 'Consumer - Individual';
		caseList[0].Application_Record_Type__c = 'Consumer - Individual';

		Database.insert(caseList);

		Application__c app = [SELECT Id FROM Application__c WHERE Case__c =: caseList[0].Id];
        
		//Create Test Roles
		list <Role__c> allRoles = new list <Role__c>();
		List<Role__c> roleListApp = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, app.Id,  1);
		roleListApp[0].Primary_Contact_for_Application__c = true;
		roleListApp[0].Preferred_Call_Time__c = '8:30 am';
		List<Role__c> roleListAppComp = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_COMP, caseList[0].Id, accList[0].Id, app.Id,  1);
		List<Role__c> roleListPar = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_PAR_INDI, caseList[0].Id, accList[0].Id, app.Id,  1);
		List<Role__c> roleListTrustee = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_TRU_INDI, caseList[0].Id, accList[0].Id, app.Id,  1);
		List<Role__c> roleListNBS = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_NON_B_SPOUSE, caseList[0].Id, accList[0].Id, app.Id,  1);
		allRoles.addAll(roleListApp);
		allRoles.addAll(roleListAppComp);
		allRoles.addAll(roleListPar);
		allRoles.addAll(roleListTrustee);
		allRoles.addAll(roleListNBS);
		Database.insert(allRoles);
	}
    
    static testmethod void testSyncAccountToRole(){
        Test.StartTest();
        	Account[] accList = [SELECT Id,Name,FirstName,MiddleName,LastName,IsPersonAccount FROM Account];
        	System.assert(accList.size()>0, 'acc should exist.');
        	System.assert(accList[0].IsPersonAccount, 'acc should be person Account.');
        	for(Account acc : accList){
                if(acc.IsPersonAccount){
                    acc.LastName = 'Great';
                }
                else{
                    acc.Name = 'Great Corp.';
                }
        	}
        	Database.update(accList);
        	//Application__c app = [SELECT Id, Primary_Contact_for_Application__c FROM Application__c LIMIT 1];
        	Role__c role = [SELECT Id, Application__r.Primary_Contact_for_Application__c, Account_Name1__c FROM Role__c WHERE Account__c =: accList[0].Id AND RecordType.Name =: CommonConstants.ROLE_RT_APP_INDI];	
        	System.assert(role.Account_Name1__c.containsIgnoreCase('Great'));
        	System.assert(role.Application__r.Primary_Contact_for_Application__c.containsIgnoreCase('Great'));
        	delete accList;
            accList = TestDataFactory.createGenericAccount('name', CommonConstants.ACC_RT_TRUST_IAT, 1);
            Database.insert(accList);
        Test.stopTest();
	}

	static testmethod void testpushAssocRelationshipsInRole(){

		//Retrieve Trigger Setting custom setting for NM
		Trigger_Settings1__c trigSet = [Select Id, Enable_Account_Assocs_in_Role__c, Enable_Triggers__c, Enable_Account_Trigger__c From Trigger_Settings1__c];
		trigSet.Enable_Triggers__c = true;
		trigSet.Enable_Account_Trigger__c = true;
		trigSet.Enable_Account_Assocs_in_Role__c = true;
		update trigSet;

		//Retrieve test data from setup
		Account[] accList = [SELECT Id,Name,FirstName,MiddleName,LastName,IsPersonAccount, Associated_Relationships__c, RecordTypeId, RecordType.Name FROM Account];
		list <Role__c> rolelist = [Select Id, RecordTypeId, RecordType.Name, Associated_Relationships__c From Role__c];


		Test.startTest();
			for (Account acc : acclist){
				acc.Associated_Relationships__c = '<ul><h1><b>SPOUSE</b></h1><li>Test1/li></ul><ul><h1><b>DIRECTORS</b></h1><li>Test2/li></ul><ul><h1><b>PARTNERS</b></h1><li>Test3/li></ul>';
			}
			update acclist;

			//Verify Associated Relationship field in Role got updated
			list <Role__c> uprolelist = [Select Id, Associated_Relationships__c From Role__c Where Account__c in : acclist];
			for (Role__c role : uprolelist){
				system.assert(role.Associated_Relationships__c != null);
			}

		Test.stopTest();
	}

	static testmethod void testPartnerSync(){
        Test.StartTest();
			Id connectiveBrokerRTId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Connective_Broker').getRecordTypeId();
			Id partnerContactId = SObjectType.Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Contact').getRecordTypeId();

			Contact co = new Contact(RecordTypeId = connectiveBrokerRTId);
			co.FirstName ='Test';
			co.LastName = 'Test';
			insert co;

        	Account[] accList = [SELECT Id,Name,FirstName,MiddleName,LastName,IsPersonAccount FROM Account];
        	System.assert(accList.size()>0, 'acc should exist.');
        	System.assert(accList[0].IsPersonAccount, 'acc should be person Account.');

			Relationship__c rel = new Relationship__c(RecordTypeId = partnerContactId, Associated_Account__c = accList[0].Id, h_ContactId__c = co.Id);
			Database.insert(rel);
			for(Account acc : accList){
                if(acc.IsPersonAccount){
                    acc.LastName = 'Great';
                }
        	}
        	Database.update(accList);
        Test.stopTest();
	}

	static testmethod void testAddCampaignMemberAndDelacon(){

		list <Account> acclist = new list <Account>();

		Test.startTest();

			Lead delaconLead = new Lead(DELAPLA__PLA_Caller_Phone_Number__c= '0412345678', LeadSource = 'delacon', FirstName='TestC55', LastName='TestCM5', Email='test5@testemail.com');
			insert delaconlead;

			Id curAccRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('nm: Individual').getRecordTypeId();
			for (Integer i=0; i<10; i++){
				Account acc = new Account(
					LastName = 'TestCM'+i,
					RecordTypeId = curAccRT,
					PersonMobilePhone = '+61412345678'
				);
				if (i < 5) acc.AccountSource = 'facebook';
				else acc.AccountSource = 'delacon';
				acclist.add(acc);
			}
			insert acclist;

			list <CampaignMember> cmlist = [Select Id, CampaignId From CampaignMember];
			system.assertEquals(cmlist.size(), 10);

			TriggerFactory.isProcessedMap = new map <Id,boolean>();
			for (Integer i=0; i<10; i++){
				if (i < 5) acclist[i].AccountSource = 'delacon';
				else acclist[i].AccountSource = 'facebook';
			}
			update acclist;

			list <CampaignMember> cmlist2 = [Select Id, CampaignId From CampaignMember];
			system.assertEquals(cmlist2.size(), 20);

			TriggerFactory.isProcessedMap = new map <Id,boolean>();
			for (Integer i=0; i<10; i++){
				if (i < 5) acclist[i].AccountSource = 'facebook';
				else acclist[i].AccountSource = 'delacon';
			}
			update acclist;

			//Verify new CampaignMembers were not created but updated
			list <CampaignMember> cmlist3 = [Select Id, CampaignId From CampaignMember];
			system.assertEquals(cmlist3.size(), 20);


		Test.stopTest();


	}
}
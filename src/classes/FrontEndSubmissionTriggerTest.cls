/**
 * @File Name          : FrontEndSubmissionTriggerTest.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 08/10/2019, 11:30:40 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    08/10/2019   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
@isTest
public class FrontEndSubmissionTriggerTest {
    @testsetup static void setup(){

        Process_Flow_Definition_Settings1__c procflowSettings = Process_Flow_Definition_Settings1__c.getOrgDefaults();
        procflowSettings.Enable_FE_NOD_Flow_v3__c  = false;
        upsert procflowSettings Process_Flow_Definition_Settings1__c.Id;

        //Turn On Trigger 
		Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
        triggerSettings.Enable_Triggers__c = true;
        triggerSettings.Enable_Front_End_Submission_Trigger__c = true;
        triggerSettings.Enable_Frontend_Sub_SetNVMContact__c = true;
        triggerSettings.Enable_Frontend_Sub_Complete_Load__c = true;
        upsert triggerSettings Trigger_Settings1__c.Id;

		upsert triggerSettings Trigger_Settings1__c.Id;
        TestDataFactory.Case2FOSetupTemplate();

        //Create test data for Location
        Location__c loc = new Location__c(
            Name = 'Location test',
            Country__c = 'Australia',
            Postcode__c = '132455',
            State__c = 'Australian Capital Territory',
            State_Acr__c = 'ACT'
        );
        insert loc;

        //Create test data for Address
        Address__c address = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
        address.Location__c = loc.Id;
        Database.insert(address);
        Address__c address2 = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
        address2.Location__c = loc.Id;
        Database.insert(address2);
	}

    static testmethod void testFESubmission(){
        Test.StartTest();
            List<Case> caseList = [SELECT Id,NVMConnect__NextContactTime__c FROM Case LIMIT 1];
            List<Role__c> roleList = [SELECT Id,Account__c, Primary_Contact_for_Application__c FROM Role__c WHERE Case__c =: caseList[0].Id];
            roleList[0].Primary_Contact_for_Application__c = false;

            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            update roleList;
            Address__c address = [SELECT Id,Location__c FROM Address__c LIMIT 1];

            Role_Address__c roleAddress = TestDataFactory.createGenericRoleAddress(roleList[0].Account__c, false, roleList[0].Id, address.Id);
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            Database.insert(roleAddress);

            caseList = [SELECT Id, Lead_Bucket__c, Primary_Contact_Location_MC__c FROM Case WHERE Id =: caseList[0].Id];
            caselist[0].OwnerId = [Select Id From Group Where Name = 'Overflow'].Id;
            caselist[0].RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POS: Consumer - Personal Loan').getRecordTypeId();
            caselist[0].Lead_Bucket__c = 'Overflow';
            update caselist[0];
            //System.assertNotEquals(caseList[0].Primary_Contact_Location_MC__c,null);
            
            Frontend_Submission__c feSub = new Frontend_Submission__c(Case__c = caseList[0].Id);
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            insert feSub;

            caseList = [SELECT Id,NVMConnect__NextContactTime__c FROM Case WHERE Id =: caseList[0].Id];
            //System.assertEquals(caseList[0].NVMConnect__NextContactTime__c,NULL);

            Frontend_Submission__c feSub2 = new Frontend_Submission__c(Case__c = caseList[0].Id);
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            insert feSub2;

            /*caseList = [SELECT Id,NVMConnect__NextContactTime__c FROM Case WHERE Id =: caseList[0].Id];
            System.assertNotEquals(caseList[0].NVMConnect__NextContactTime__c,NULL); */

            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            update feSub2;
            delete feSub2;
        Test.StopTest();
    }

    static testmethod void testFESubmissionCallRequest(){
        Test.StartTest();
            List<Case> caseList = [SELECT Id, Primary_Contact_Location_MC__c, NVMConnect__NextContactTime__c FROM Case LIMIT 1];
            List<Role__c> roleList = [SELECT Id,Account__c, Primary_Contact_for_Application__c FROM Role__c WHERE Case__c =: caseList[0].Id];
            roleList[0].Primary_Contact_for_Application__c = false;

            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            update roleList;
            Address__c address = [SELECT Id,Location__c FROM Address__c LIMIT 1];

            Role_Address__c roleAddress = TestDataFactory.createGenericRoleAddress(roleList[0].Account__c, false, roleList[0].Id, address.Id);
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            Database.insert(roleAddress);

            caseList = [SELECT Id, Lead_Bucket__c, Primary_Contact_Location_MC__c FROM Case WHERE Id =: caseList[0].Id];
            caselist[0].Primary_Contact_Location_MC__c = roleAddress.Location__c;
            update caselist;

            //System.assertNotEquals(caseList[0].Primary_Contact_Location_MC__c,null);
            
            Frontend_Submission__c feSub = new Frontend_Submission__c(Case__c = caseList[0].Id, Positive_Form_Type__c = FrontEndSubmissionHandler.NOD_REQUEST_CALL, Preferred_Call_Time__c = Time.newInstance(9, 30, 0, 0));
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            insert feSub;

            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            feSub.Preferred_Call_Time__c = Time.newInstance(10, 30, 0, 0);
            update feSub;

            delete feSub;
        Test.StopTest();
    }
}
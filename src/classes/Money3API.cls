/* 
    Author: Srikanth Vivaram
    Purpose: Send Loan Application from Salesforce to Money3.
*/


global with sharing class Money3API {
    
    public Id accId{get; set; }
    public Opportunity oppObj{get;set;}
    public Applicant_2__c app2Obj{get;set;}
    public Account accObj;
        
    global Money3Api(apexpages.standardController con){

    accId = con.getRecord().Id;
    
    try{
        accObj = new Account();
        
        accObj = [SELECT Id, Name, 
        PersonTitle, Salutation, FirstName, LastName, 
        Gender__c, Date_of_Birth__c, 
        Marital_Status__pc, 
        PersonMobilePhone, PersonEmail, Drivers_Licence_Number__c,
        Identification_Type__c, Identification_Number__c,
        Residential_Status__c, Current_Street_Number__c, Current_Street_Name__c, 
        Current_Street_Type__c, Current_Suburb__c, Current_State__c, Current_Postcode__c,     
        Time_at_current_Address__c, 
        UserFullName__c,
        Permanent_Resident_Status__c,
        title__c, Application_Type__c, 
        Loan_Reference__c, Loan_Reference_Test__c         
        FROM Account where Id =: accId];
        
        oppObj = new Opportunity();
        
        oppObj = [SELECT Id, name, 
        Loan_Type__c, Loan_Product__c, 
        Loan_Type2__c, Loan_Amount__c,
        Previous_Bankrupt__c, Length_of_time_in_employment__c, 
        Available_Income__c, Income_Frequency__c
        from Opportunity where AccountId =: accId ORDER BY Createddate DESC limit 1];
        
        app2Obj = new Applicant_2__c();

        if(accId != null){
        app2Obj = [SELECT Id, First_Name__c, Last_Name__c, Title__c, 
        Gender__c, Date_of_Birth__c, Permanent_Resident_Status__c,
        Marital_Status__c  
        from Applicant_2__c where Account__c =: accId];
        }
        
    }catch(exception e){
    
    }
    
    }
    
    global pageReference sendToMoney3(){
   
    if(accId != null ){
/*        
    Map<String,Money3Config__c> allMoneyApisMap = Money3Config__c.getAll();
    Map<String,String> money3ApiKeys = new Map<String,String>();
    for(Money3Config__c moneyApi:allMoneyApisMap.values()){
        money3ApiKeys.put(moneyApi.name,moneyapi.Value__c);
    }
    String EndPoint = money3ApiKeys.get(EndpointURL);
    System.debug('Endpoint ==========================> ' + 'EndpointURL');
*/       
    String Endpoint = 'https://api.money3.com.au/broker/application/new/';
    
//    String Endpoint = 'https://api-stage.money3.com.au/broker/application/new/';
    
//  Used to send an attachment, get the attachment and map it to the below URL
//  String attachmentBody = '<InputType>'+attachment.ContentType.substringAfter('/')+'</InputType>
//  <InputURL></InputURL>
//  <InputBlob type="binary">'+EncodingUtil.base64Encode(attachment.Body)+'</InputBlob>';

    DateTime dateTimeNow = dateTime.now();
    String unixTime = ''+dateTimeNow.getTime()/1000;
//    String privKey = money3ApiKeys.get('PrivateKey');  
//    String publicKey = money3ApiKeys.get('PublicKey');
    String privKey = 'b5d4860bdfc37e5921828f39cfab01bac0130541';
    String publicKey = 'f83c52ccabd6a91718daba6de72bb2ec36e74fdb';
    String resBody; 
    String dobFull = String.valueOf(accObj.Date_of_Birth__c);
    String dob = dobFull.remove(' 00:00:00');
    String dobfull2;
    if(app2Obj != null){
        dobfull2 = String.valueOf(app2Obj.Date_Of_Birth__c);
    }
    
    String dataBody = '';
    
    if(accObj.Current_State__c == 'Western Australia'){
    accObj.Current_State__c = 'West Australia';
    }
    if(accObj.Residential_Status__c == 'Boarding'){
    accObj.Residential_Status__c = 'Board';
    }
    if(accObj.Residential_Status__c == 'Employer Provided' || accObj.Residential_Status__c == 'Renting Private Landlord' || accObj.Residential_Status__c == 'Renting Real Estate Agent'){
    accObj.Residential_Status__c = 'Other';
    }
    if(oppObj.Loan_Type2__c == 'Bike Loan'){
    oppObj.Loan_Type2__c = 'Motor Bike';
    }
    if(oppObj.Loan_Type2__c == 'Boat Loan'){
    oppObj.Loan_Type2__c = 'Boat';
    }
    if(oppObj.Loan_Type2__c == 'Truck Loan'){
    oppObj.Loan_Type2__c = 'General Living';
    }
    if(oppObj.Loan_Type2__c == 'Equipment Loan'){
    oppObj.Loan_Type2__c = 'General Living';
    }
    if(oppObj.Loan_Product__c == 'Property'){
    oppObj.Loan_Product__c = 'General Living';
    }
    if(oppObj.Loan_Product__c == 'Consumer Loan'){
    oppObj.Loan_Product__c = 'Consumer';
    }
    if(oppObj.Loan_Product__c == 'Commercial Hire Purchase'){
    oppObj.Loan_Product__c = 'Commercial';
    }
    if(oppObj.Loan_Product__c == 'Novated Lease' || oppObj.Loan_Product__c == 'Finance Lease' || oppObj.Loan_Product__c == 'Chattel Mortgage' || oppObj.Loan_Product__c == 'Investment' || oppObj.Loan_Product__c == 'Owner-Occupied'){
    oppObj.Loan_Product__c = 'Commercial';
    }
    if(accObj.Marital_Status__pc == 'De Facto'){
    accObj.Marital_Status__pc = 'DeFacto';
    }
    if(accObj.Salutation == 'Mr.'){
    accObj.Salutation = 'Mr';
    }
    if(accObj.Salutation == 'Ms.'){
    accObj.Salutation = 'Ms';
    }
    if(accObj.Salutation == 'Mrs.'){
    accObj.Salutation = 'Mrs';
    }
    if(accObj.Salutation == 'Dr.' || accObj.Salutation == 'Prof.'){
    accObj.Salutation = 'Mr';
    }
                 
    dataBody='<LoanApplication><LoanReference>'+accObj.Loan_Reference__c+
    '</LoanReference><BrokerConsultantName>'+accObj.UserFullName__c+
    '</BrokerConsultantName><LoanType>'+oppObj.Loan_Type2__c+
    '</LoanType><LoanAmount>'+oppObj.Loan_Amount__c+
    '</LoanAmount><JoinApplication>'+accObj.Application_Type__c+
    '</JoinApplication><LoanCreditType>'+oppObj.Loan_Product__c+
    '</LoanCreditType><PrimaryApplicant><PersonalDetails><Title>'+accObj.Salutation+
    '</Title><LastName>'+accObj.LastName+
    '</LastName><FirstName>'+accObj.FirstName+
    '</FirstName><Gender>'+accObj.Gender__c+
    '</Gender><DateOfBirth>'+dob+
    '</DateOfBirth><PermanentResidentStatus>'+accObj.Permanent_Resident_Status__c+
    '</PermanentResidentStatus><MaritalStatus>'+accObj.Marital_Status__pc+
    '</MaritalStatus></PersonalDetails><ContactDetails><Mobile>'+accObj.PersonMobilePhone+
    '</Mobile><Email>'+accObj.PersonEmail+'</Email></ContactDetails><BankruptcyDetails><BankruptcyStatus>'+oppObj.Previous_Bankrupt__c+
    '</BankruptcyStatus></BankruptcyDetails><IdentificationDetails><Identification><IdType>'+accObj.Identification_Type__c+
    '</IdType><IdNumber>'+accObj.Identification_Number__c+ 
    '</IdNumber></Identification></IdentificationDetails><ResidentialDetails><Residential><AddressNumber>'+accObj.Current_Street_Number__c+
    '</AddressNumber><AddressStreetName>'+accObj.Current_Street_Name__c+
    '</AddressStreetName><AddressStreetType>'+accObj.Current_Street_Type__c+
    '</AddressStreetType><AddressSuburb>'+accObj.Current_Suburb__c+
    '</AddressSuburb><AddressState>'+accObj.Current_State__c+
    '</AddressState><AddressPostcode>'+accObj.Current_Postcode__c+
    '</AddressPostcode><ResidentialStatus>'+accObj.Residential_Status__c+
    '</ResidentialStatus><TimeAtAddress>'+accObj.Time_at_current_Address__c+
    '</TimeAtAddress></Residential></ResidentialDetails>';
    
    dataBody+=+'<EmploymentDetails>';
    for(Employee__c empObj:[SELECT Id, Account__c, 
                            Name,Contact_Phone__c, Contact_Person__c, 
                            Street_Number__c, Street_Name__c, Street_Type__c, Suburb__c, State__c, Postcode__c, Job_Status__c,  
                            Job_Title__c, Current_Employment__c, Employment_Duration__c, Description__c 
                            From Employee__c where Account__c =:accObj.Id]){
    
    if(empObj.Job_Status__c == 'Self Employed'){
    empObj.Job_Status__c = 'Self-Employed';
    }
    if(empObj.Job_Status__c == 'Pensioner'){
    empObj.Job_Status__c = 'Aged Pensioner';
    }
    if(empObj.Job_Status__c == 'Full-Time'){
    empObj.Job_Status__c = 'Full Time';
    }
    if(empObj.Current_Employment__c == null){
    empObj.Current_Employment__c = 'Yes';
    }
    if(empObj.Employment_Duration__c == null){
    empObj.Employment_Duration__c = '0';
    }
    if(empObj.Description__c == 'Current'){
    empObj.Description__c = 'Yes';
    }
    if(empObj.Description__c != 'Current'){
    empObj.Description__c = 'No';
    }if(empObj.Street_Number__c == null){
    empObj.Street_Number__c = '0';
    }
    if(empObj.Street_Name__c == null){
    empObj.Street_Name__c = 'na';
    }
    if(empObj.Street_Type__c == null){
    empObj.Street_Type__c = 'Street';
    }
    if(empObj.Suburb__c == null){
    empObj.Suburb__c = 'na';
    }
    if(empObj.State__c == null){
    empObj.State__c = 'International';
    }
    if(empObj.Postcode__c == null){
    empObj.Postcode__c = 2222;
    }
                                  
        dataBody+= +'<Employment><EmployerName>'+empObj.Name+
        '</EmployerName><EmployerAbn>1</EmployerAbn><ContactPhone>'+empObj.Contact_Phone__c+
        '</ContactPhone><ContactPerson>'+empObj.Contact_Person__c+
        '</ContactPerson><AddressNumber>'+empObj.Street_Number__c+
        '</AddressNumber><AddressStreetName>'+empObj.Street_Name__c+
        '</AddressStreetName><AddressStreetType>'+empObj.Street_Type__c+
        '</AddressStreetType><AddressSuburb>'+empObj.Suburb__c+
        '</AddressSuburb><AddressState>'+empObj.State__c+
        '</AddressState><AddressPostcode>'+empObj.Postcode__c+
        '</AddressPostcode><EmploymentPosition>'+empObj.Job_Title__c+
        '</EmploymentPosition><EmploymentDuration>'+empObj.Employment_Duration__c+
        '</EmploymentDuration><EmploymentStatus>'+empObj.Job_Status__c+
        '</EmploymentStatus><CurrentEmployment>'+empObj.Description__c+
        '</CurrentEmployment>'+'<Salary><NetPayAmount>'+oppObj.Available_Income__c+
        '</NetPayAmount><Frequency>Monthly</Frequency></Salary>'+'</Employment>';
        
    }
    dataBody+=+'</EmploymentDetails>';
    dataBody+=+'<AssetsDetails><Vehicles>';
    for(Assets__c asstObj: [SELECT Id, Account__c, Asset_Type__c, Value__c
                           from Assets__c where Account__c =: accObj.Id ]){
                            
        dataBody+=+'<Vehicle><VehicleMake>'+asstObj.Asset_Type__c+
        '</VehicleMake><VehicleModel>NA</VehicleModel><VehicleKm>0</VehicleKm><VehicleValue>'+asstObj.Value__c+
        '</VehicleValue><VehicleUnderFinance>No</VehicleUnderFinance><VehicleFinance><FinanceProvider>NA</FinanceProvider><LoanAmount>1</LoanAmount><PaymentAmount>1</PaymentAmount><Frequency>Monthly</Frequency></VehicleFinance></Vehicle>';    
    }
    
    dataBody+=+'</Vehicles><AssetsOthers>0.00</AssetsOthers></AssetsDetails>';
    
    dataBody+=+'<LiabilitiesDetails>';
    for(Liability__c libObj:[SELECT Id, Account__c, Type__c, 
                            Name, Credit_Limit__c, Balance__c,
                            Monthly_Payment__c from Liability__c where Account__c =:accObj.Id]){
    
    if(libObj.Credit_Limit__c == null){
    libObj.Credit_Limit__c = 0;
    }
    if(libObj.Balance__c == null){
    libObj.Balance__c = 0;
    }
    if(libObj.Monthly_Payment__c == null){
    libObj.Monthly_Payment__c = 0;
    }
    if(libObj.Type__c == null){
    libObj.Type__c = 'Other';
    }
    if(libObj.Type__c == 'Car Loan'){
    libObj.Type__c = 'Motor Vehicle Loan';
    }
    if(libObj.Type__c == 'Mortgage Loan'){
    libObj.Type__c = 'Housing Loan';
    }
    if(libObj.Type__c == 'Ongoing Rent'){
    libObj.Type__c = 'Rental Repayment';
    }
    if(libObj.Type__c == 'Hire Purchase'){
    libObj.Type__c = 'Leases/Hire Purchase';
    }
    if(libObj.Type__c == 'Lease'){
    libObj.Type__c = 'Leases/Hire Purchase';
    }
    if(libObj.Type__c == 'Loan As Guarantor'){
    libObj.Type__c = 'Guarantees';
    }
    if(libObj.Type__c == 'Outstanding Taxation'){
    libObj.Type__c = 'Taxation Liability';
    }
    if(libObj.Type__c == 'Term Loan'){
    libObj.Type__c = 'Revolving Loan';
    }
    if(libObj.Type__c == 'Commercial Bill' || libObj.Type__c == 'HECS' || libObj.Type__c == 'Line Of Credit' || libObj.Type__c == 'Maintenance'){
    libObj.Type__c = 'Other';
    }
    
     
        dataBody+=+'<Liability><LiabilityType>'+libObj.Type__c+
        '</LiabilityType><FinanceProvider>'+libObj.Name+
        '</FinanceProvider><LiabilityLimit>'+libObj.Credit_Limit__c+'</LiabilityLimit><Balance>'+libObj.Balance__c+
        '</Balance><PaymentAmount>'+libObj.Monthly_Payment__c+
        '</PaymentAmount><Frequency>Monthly</Frequency></Liability>';
        
    }
    dataBody+=+'</LiabilitiesDetails>';
//    dataBody+=+'<SupportDocumentDetails><Document><Filename>Test</Filename><DownloadUrl>https://cs5.salesforce.com/sfc/p/O000000531yc/a/O000000002iW/xh3pYRhSZBqB5xqCJXWERrtp4CsyzU31Yxut7sK8_s0</DownloadUrl></Document></SupportDocumentDetails>';
    
    dataBody+=+'<ContactReferences>'; 
       
    for (Reference__c refObj:[SELECT Id, Name, Account__c,
    Last_Name__c, First_Name__c, Street_Number__c, Street_Name__c, Street_Type__c, Suburb__c, State__c, Postcode__c, 
    Relationship__c, Phone__c, 
    Mobile_Number__c 
    from Reference__c where Account__c =: accObj.Id]){
    
    if(refObj.Street_Number__c == null){
    refObj.Street_Number__c = '0';
    }
    if(refObj.Street_Name__c == null){
    refObj.Street_Name__c = 'na';
    }
    if(refObj.Street_Type__c == null){
    refObj.Street_Type__c = 'Street';
    }
    if(refObj.Suburb__c == null){
    refObj.Suburb__c = 'na';
    }
    if(refObj.State__c == null){
    refObj.State__c = 'International';
    }
    if(refObj.Postcode__c == null){
    refObj.Postcode__c = 2222;
    }
    
    dataBody+=+'<ContactReference><LastName>'+refObj.Name+
    '</LastName><FirstName>'+refObj.Name+
    '</FirstName><ContactRelationship>'+refObj.Relationship__c+'</ContactRelationship><AddressNumber>'+refObj.Street_Number__c+
    '</AddressNumber><AddressStreetName>'+refObj.Street_Name__c+'</AddressStreetName><AddressStreetType>'+refObj.Street_Type__c+
    '</AddressStreetType><AddressSuburb>'+refObj.Suburb__c+'</AddressSuburb><AddressState>'+refObj.State__c+
    '</AddressState><AddressPostcode>'+refObj.Postcode__c+'</AddressPostcode><Mobile>'+refObj.Mobile_Number__c+'</Mobile></ContactReference>';
 
    }
    
    dataBody+=+'</ContactReferences>';
    dataBody+=+'</PrimaryApplicant>';
    String dob2;
    if(dobFull2 != null){
    dob2 = dobFull2.remove(' 00:00:00');
    }
    if(app2Obj.Gender__c == 'Male'){
    app2Obj.Title__c = 'Mr';
    }
    if(app2Obj.Gender__c == 'Female'){
    app2Obj.Title__c = 'Mrs';
    }
    dataBody+=+'<SecondaryApplicant>';
    if(app2Obj != null){
    dataBody+=+'<PersonalDetails><Title>'+app2Obj.Title__c+
    '</Title><LastName>'+app2Obj.Last_Name__c+
    '</LastName><FirstName>'+app2Obj.First_Name__c+
    '</FirstName><Gender>'+app2Obj.Gender__c+
    '</Gender><DateOfBirth>'+dob2+
    '</DateOfBirth><PermanentResidentStatus>'+app2Obj.Permanent_Resident_Status__c+
    '</PermanentResidentStatus><MaritalStatus>'+app2Obj.Marital_Status__c+
    '</MaritalStatus></PersonalDetails>';
    }

    dataBody+=+'<EmploymentDetails>';
    if(app2Obj != null){
        for(Employee__c empObj:[SELECT Id, Applicant2__c, 
                                Name, Contact_Phone__c, Contact_Person__c, 
                                Job_Status__c, Employer_address__c, Street_Number__c, Street_Name__c, Street_Type__c, Suburb__c, State__c, Postcode__c,
                                Job_Title__c, Current_Employment__c, Employment_Duration__c, Description__c 
                                From Employee__c where Applicant2__c =:app2Obj.Id]){
    
    if(empObj.Job_Status__c == 'Self Employed'){
    empObj.Job_Status__c = 'Self-Employed';
    }
    if(empObj.Job_Status__c == 'Pensioner'){
    empObj.Job_Status__c = 'Aged Pensioner';
    }
    if(empObj.Job_Status__c == 'Full-Time'){
    empObj.Job_Status__c = 'Full Time';
    }
    if(empObj.Current_Employment__c == null){
    empObj.Current_Employment__c = 'Yes';
    }
    if(empObj.Employment_Duration__c == null){
    empObj.Employment_Duration__c = '0';
    }
    if(empObj.Description__c == 'Current'){
    empObj.Description__c = 'Yes';
    }
    if(empObj.Description__c != 'Current'){
    empObj.Description__c = 'No';
    }if(empObj.Street_Number__c == null){
    empObj.Street_Number__c = '0';
    }
    if(empObj.Street_Name__c == null){
    empObj.Street_Name__c = 'na';
    }
    if(empObj.Street_Type__c == null){
    empObj.Street_Type__c = 'Street';
    }
    if(empObj.Suburb__c == null){
    empObj.Suburb__c = 'na';
    }
    if(empObj.State__c == null){
    empObj.State__c = 'International';
    }
    if(empObj.Postcode__c == null){
    empObj.Postcode__c = 2222;
    }
        
            dataBody+= +'<Employment><EmployerName>'+empObj.Name+
            '</EmployerName><EmployerAbn>1</EmployerAbn><ContactPhone>'+empObj.Contact_Phone__c+
            '</ContactPhone><ContactPerson>'+empObj.Contact_Person__c+
            '</ContactPerson><AddressNumber>'+empObj.Street_Number__c+'</AddressNumber><AddressStreetName>'+empObj.Street_Name__c+
            '</AddressStreetName><AddressStreetType>'+empObj.Street_Type__c+'</AddressStreetType><AddressSuburb>'+empObj.Suburb__c+
            '</AddressSuburb><AddressState>'+empObj.State__c+'</AddressState><AddressPostcode>'+empObj.Postcode__c+
            '</AddressPostcode><EmploymentPosition>'+empObj.Job_Title__c+
            '</EmploymentPosition><EmploymentDuration>'+empObj.Employment_Duration__c+
            '</EmploymentDuration><EmploymentStatus>'+empObj.Job_Status__c+
            '</EmploymentStatus><CurrentEmployment>'+empObj.Description__c+
            '</CurrentEmployment>'+'<Salary><NetPayAmount>'+oppObj.Available_Income__c+
            '</NetPayAmount><Frequency>Monthly</Frequency></Salary>'+'</Employment>';
            
        }
    }
    dataBody+=+'</EmploymentDetails>';    

    dataBody+=+'</SecondaryApplicant></LoanApplication>';
    
    system.debug('Test===>'+dataBody);
    
    blob blobSign = Crypto.generateDigest('SHA-256', blob.valueOf(privKey + dataBody.Length() + unixTime));
    String hashString = EncodingUtil.convertToHex(blobSign);
    String HMACKey = EncodingUtil.Base64Encode(Blob.valueOf(hashString));
    
    HttpRequest req = new HttpRequest();
    req.setMethod('POST');
    req.setEndpoint(EndPoint);
    req.setHeader('Content-Type','application/xml');
    req.setHeader('API-KEY',publicKey);
    req.setHeader('API-HMAC',HMACKey);
    req.setHeader('API-Timestamp',unixTime);
    req.setBody(dataBody);
    //req.setCompressed(true);
    
    Http http = new Http();
    HttpResponse res = new HttpResponse();
//    HttpResponse res = Http.send(req);
//    resBody = res.getbody();
    
    Try{
        res = http.send(req);
        system.debug('===> Status: '+res.getStatus());
        system.debug('===> Status: '+res.getStatusCode());
        system.debug('===> Status: '+res.getBody());    
    } Catch(System.CalloutException ex){
        System.debug('Callout error: '+ ex);
        System.debug(res.getBody());
    }
    
    String responseBody = res.getBody();      
    System.debug('===> Unix Time: ' + unixTime);
    System.debug('===> dataBody: ' + dataBody);
    System.debug('===> blobSign: ' + blobSign);
    System.debug('===> HmacKey: ' + HmacKey);
    System.debug('===> Data Length: ' + dataBody.Length());
    
    if(res.getStatusCode()==200 || res.getStatusCode() == 201){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Application' + ' ' +accObj.Loan_Reference__c+ ' ' + 'received on' + ' ' + DateTime.now().format('dd-MM-yyyy hh:mm:ss') + ',' + ' ' +'please check back later for loan Id and status.'));
        return new PageReference('/apex/Money3Application');
    
    }else if(res.getStatusCode() == 400 && res.getBody().contains('LoanReference')){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Loan Reference' + ' ' + accObj.Loan_Reference__c+ ' ' + 'already exist'));
    
    }else if (res.getStatusCode() == 500 && res.getBody().contains('Broker Contact')){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Broker Contact' + ' ' + accObj.UserFullName__c + ' ' + 'does not exist.'));
        return null;
    
    }else{
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Your Application is not Sent. Please contact your System Administrator for further Assistance.'));
    }
    }
    return null;
    }
/*
    public pageReference save(){
    update accObj;
    return null;
    }
*/
/*
    public pageReference back(){
    return new pageReference('/'+accId);
    }
  
public pageReference Reports(){
    
    DateTime dateTimeNow = dateTime.now();
    String unixTime = ''+dateTimeNow.getTime()/1000;        
    String data = ''; 

    String privKey = 'b5d4860bdfc37e5921828f39cfab01bac0130541';
    String publicKey = 'f83c52ccabd6a91718daba6de72bb2ec36e74fdb';
    String responseBody;        
    blob blobSign = Crypto.generateDigest('SHA256', Blob.valueOf(privKey + data.Length() + unixTime));
    String hashSign = EncodingUtil.convertToHex(blobSign);
    String HmacKey = EncodingUtil.base64Encode(blob.valueOf(hashSign)); 
    String endpoint='https://api-stage.money3.com.au/broker/loan/report/';

    HttpRequest request = new HttpRequest();
    request.setMethod('GET');
    request.setEndpoint(endpoint);
    request.setHeader('Content-Type', 'application/xml');
    request.setHeader('API-KEY',publicKey);
    request.setHeader('API-HMAC',HMACKey );
    request.setHeader('API-Timestamp',unixTime);
            
    Http http = new Http();
    HttpResponse response = http.send(request); 
    responseBody = response.getBody();      
    System.debug('===> Response: '+ Response);
    System.debug('===> Response Body:'+ ResponseBody);
    System.debug('===> time: '+unixTime);
    System.debug('===> HMACKey: '+ HMACKey);
    System.debug('===> BlobSign: ' + blobsign);
    System.debug('===> Data Length: ' + data);



    return null;
    }
    public pageReference upload(){
    return null;
    } 
*/
}
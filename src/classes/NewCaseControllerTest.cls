/** 
* @FileName: NewCaseControllerTest
* @Description: Test class for NewCaseController
* @Copyright: Positive (c) 2019
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/20/18 JBACULOD Created class, added testinitSaveCase
**/ 
@isTest
public class NewCaseControllerTest {

    @testSetup static void setupData() {
        
        //PLS Case
        Case sourcePOSCase = new Case(
            Status = 'New',
            Stage__c = 'Open',
            Lead_Purpose__c = 'Car Loan',
            Lead_Loan_Amount__c = 5000,
            Partition__c = 'Positive',
            Channel__c = 'PLS',
            Application_Record_Type__c = 'Consumer - Individual'
        );
        insert sourcePOSCase;

        Application__c sourcePOSApp = new Application__c(
            Case__c = sourcePOSCase.Id
        );
        insert sourcePOSApp;

        //NOD Case
        Case sourceNODCase = new Case(
            Status = 'New',
            Stage__c = 'Open',
            Lead_Purpose__c = 'Car Loan',
            Lead_Loan_Amount__c = 5000,
            Partition__c = 'Nodifi',
            Channel__c = 'NODIFI',
            Application_Record_Type__c = 'Consumer - Individual'
        );
        insert sourceNODCase;

        Application__c sourceNODApp = new Application__c(
            Case__c = sourceNODCase.Id
        );
        insert sourceNODApp;


    }

    static testmethod void testinitSaveCase(){

        string sourcePOSAppID = '';
        string sourceNODAppID = '';
        list <Application__c> sourceApps = [Select Id, Partition__c From Application__c];
        for (Application__c app : sourceApps){
            if (app.Partition__c == 'Positive') sourcePOSAppID = app.Id;
            else if (app.Partition__c == 'Nodifi') sourceNODAppID = app.Id;
        }

        NewCaseController nccon = new NewCaseController();
        //Initialize Create POS Case
        NewCaseController.wrappedData wRDPOS = NewCaseController.CaseInit('main','Positive', 'PLS', sourcePOSAppID);
        wRDPOS.newCase.Lead_Purpose__c = 'test loan';
        //Test Error on Save 
        Case newCase = new Case();
        try{
            newCase = NewCaseController.saveCase(wRDPOS.newCase);
        }
        catch(Exception e){
            system.assert(newCase.Id == null); //Verify Case is not created
        } 

        NewCaseController.wrappedData wRDPOS2 = NewCaseController.CaseInit('main','Positive', 'PLS', sourcePOSAppID);
        Case newCase2 = NewCaseController.saveCase(wRDPOS2.newCase);
        system.assert(newCase2.Id != null); //Verify Case has been created


        //Initialize Create NOD Case
        NewCaseController.wrappedData wRDNOD = NewCaseController.CaseInit('main','Nodifi', 'Nodifi', sourceNODAppID);

    }

}
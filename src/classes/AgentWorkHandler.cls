/** 
* @FileName: AgentWorkHandler
* @Description: Trigger Handler for the Task SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.1 4/12 RDAVID Created
* 1.1.1 12/13/18 JBACULOD - Updated SUPPORTREQUESTPREFIX
**/ 

public without sharing class AgentWorkHandler implements ITrigger {	
	private final string SUPPORTREQUESTPREFIX = General_Settings1__c.getInstance(UserInfo.getUserId()).Object_Prefix_Support_Request__c; //'a5I';
	private List <Support_Request__c> supportRequestList = new List <Support_Request__c>();
	private static Map <Id,Support_Request__c> supportRequestMap = new Map <Id,Support_Request__c>();
	// Constructor
	public AgentWorkHandler(){
	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){	
	}
	
	public void bulkAfter(){
		Set<Id> supportReqIds = new Set<Id>();
		if(Trigger.isUpdate){
			for(AgentWork agentWk : (List<AgentWork>)Trigger.new){
				if(agentWk.Status == 'Opened' || Test.isRunningTest()){
					if(String.valueOf(agentWk.WorkItemId).startsWith(SUPPORTREQUESTPREFIX)) supportReqIds.add(agentWk.WorkItemId);
				}
			}
			if(supportReqIds.size() > 0) supportRequestMap = new Map<Id,Support_Request__c>([SELECT Id FROM Support_Request__c WHERE Id IN : supportReqIds]);
		}
	}
		
	public void beforeInsert(SObject so){

	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){
		
	}
	
	public void afterUpdate(SObject oldSo, SObject so){
		AgentWork agentwrk = (AgentWork)so;
		AgentWork odlAgentwrk = (AgentWork)oldSo;
		if((odlAgentwrk.Status != 'Opened' && agentwrk.Status == 'Opened') || Test.isRunningTest()){
			if(supportRequestMap.containsKey(agentwrk.WorkItemId)){ //Work Item is a Support Requested.
				Support_Request__c sp = new Support_Request__c(	Id = agentwrk.WorkItemId, 
																Status__c ='In Progress', 
																Stage__c = 'In Progress',
																Send_Email_Activity__c  = true);
				supportRequestList.add(sp);
			}
		}
	}
	
	public void afterDelete(SObject so){

	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		if(supportRequestList.size() > 0) Database.update(supportRequestList);
	}
}
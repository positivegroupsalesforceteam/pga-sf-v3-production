/** 
* @FileName: PartnerGateway
* @Description: Helper class of PartnerHandler which contains most of the logic needed on triggers for Partner
* @Copyright: Positive (c) 2019
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 8/01/19 RDAVID - extra/PartnerAndDealerDirectory - Created Class
* 1.1 22/03/19 RDAVID Updated Class - Added Logic to prevent Partner Trigger Recursion
**/ 
public class PartnerGateway {
    public static Map<Id,Schema.RecordTypeInfo> partnerRTMap = Schema.SObjectType.Partner__c.getRecordTypeInfosById();
    public static Boolean shouldFirePartnerCreation = true;
    public static Boolean shouldFireAccountCreation = true;

    public static Referral_Company__c createUpdateReferralCompany(Partner__c partnerInstance, Map<String,Referral_Company__c> relatedSiteMap, Map<String,ABN__c> abnMap, Map<Id,Address__c> addressMap){

        Referral_Company__c refCompanyToCreate = new Referral_Company__c(   Name = partnerInstance.Name,
                                                                            Company_Type__c = getCompanyType(partnerInstance.RecordTypeId),
                                                                            Dealer_Type__c = (partnerInstance.Franchise_Partner__c) ? 'Franchise':'Independant',
                                                                            Business_Phone_Number__c = partnerInstance.Partner_Phone__c,
                                                                            Company_Website__c = partnerInstance.Partner_Website__c,
                                                                            Registered_Business_Name__c = partnerInstance.m_Registered_Business_Name__c,
                                                                            Legal_Entity_Name__c = (abnMap.containsKey(partnerInstance.Partner_ABN__c)) ? abnMap.get(partnerInstance.Partner_ABN__c).Id : null,
                                                                            Nodifi_Onboarding_Stage__c = partnerInstance.m_Nodifi_Onboarding_Stage__c,
                                                                            OwnerId = partnerInstance.OwnerId,
                                                                            Related_Site__c = (relatedSiteMap.containsKey(partnerInstance.Partner_Parent__c)) ? relatedSiteMap.get(partnerInstance.Partner_Parent__c).Id : null,
                                                                            Address_Line_1__c = (addressMap.containsKey(partnerInstance.Partner_Address__c)) ? getAddressLine(addressMap.get(partnerInstance.Partner_Address__c)) : null,
                                                                            Suburb__c = (addressMap.containsKey(partnerInstance.Partner_Address__c)) ? addressMap.get(partnerInstance.Partner_Address__c).Suburb__c : null,
                                                                            Postcode__c = (addressMap.containsKey(partnerInstance.Partner_Address__c)) ? addressMap.get(partnerInstance.Partner_Address__c).Postcode__c : null,
                                                                            State__c = (addressMap.containsKey(partnerInstance.Partner_Address__c)) ? addressMap.get(partnerInstance.Partner_Address__c).State_Acr__c : null
                                                                        );
        return refCompanyToCreate;
    }

    private static String getAddressLine (Address__c address){
        String addressLine1 = '';
        if(address != NULL){
            if(!String.IsBlank(address.Street_Number__c)){
                addressLine1 += address.Street_Number__c;
            }
            if(!String.IsBlank(address.Street_Name__c)){
                addressLine1 += (address.Street_Number__c != NULL) ? ' ' + address.Street_Name__c : address.Street_Name__c;
            }
            if(!String.IsBlank(address.Street_type__c)){
                addressLine1 += (address.Street_Name__c != NULL) ? ' ' + address.Street_type__c : address.Street_type__c;
            }
        }
        System.debug('@@@addressLine1 - '+addressLine1);
        return addressLine1;
    }
    
    private static String getCompanyType (String partnerRTId){
        String partnerRTDevName = partnerRTMap.get(partnerRTId).getDeveloperName();
        if(partnerRTDevName != NULL){
            if(partnerRTDevName == 'Finance_Aggregator') return 'Mortgage Aggregator';
            else if(partnerRTDevName == 'Finance_Brokerage') return 'Mortgage Broker';
        }
        return null;
    }

    public static Map<String,Referral_Company__c> getRelatedSiteMap(Set<Id> relatedSiteIds){
        Map<String,Referral_Company__c> relatedSiteMap = new Map<String,Referral_Company__c>();
        Map<String,Id> partnerNameMap = new Map<String,Id>();

        for(Partner__c refComp : [SELECT Id, Name FROM Partner__c WHERE Id IN: relatedSiteIds]){
            partnerNameMap.put(refComp.Name,refComp.Id);
        }

        if(!partnerNameMap.isEmpty()){
            for(Referral_Company__c partner : [SELECT Id, Name FROM Referral_Company__c WHERE Name IN: partnerNameMap.keySet()]){
                relatedSiteMap.put(partnerNameMap.get(partner.Name),partner);
            }
        }
        return relatedSiteMap;
    }

    public static Map<String,ABN__c> getABNMap(Set<String> partnerAbn){
        Map<String,ABN__c> abnMap = new Map<String,ABN__c>();
        for(ABN__c abn : [SELECT Id, Name, Registered_ABN__c FROM ABN__c WHERE Registered_ABN__c IN: partnerAbn]){
            abnMap.put(abn.Registered_ABN__c,abn);
        }
        return abnMap;
    }

    public static Boolean isPartnerFieldsUpdated(Partner__c newPartner, Partner__c oldPartner){
        if( newPartner.Name != oldPartner.Name || newPartner.RecordTypeId != oldPartner.RecordTypeId || newPartner.Franchise_Partner__c != oldPartner.Franchise_Partner__c || newPartner.Partner_Phone__c != oldPartner.Partner_Phone__c || newPartner.Partner_Website__c != oldPartner.Partner_Website__c || newPartner.m_Registered_Business_Name__c != oldPartner.m_Registered_Business_Name__c || newPartner.Partner_ABN__c != oldPartner.Partner_ABN__c || newPartner.m_Nodifi_Onboarding_Stage__c != oldPartner.m_Nodifi_Onboarding_Stage__c || newPartner.OwnerId != oldPartner.OwnerId || newPartner.Partner_Parent__c != oldPartner.Partner_Parent__c || newPartner.Partner_Address__c != oldPartner.Partner_Address__c){
                return true;
        }
        return false;
    }

    public static list <Partner_Touch_Point__c> getLeadBizibleTPs(map <id,id> leadPartnerMap){
        list <Partner_Touch_Point__c> partnerTPlist = new list <Partner_Touch_Point__c>();
        //Retrieve all Bizible Touchpoints from Leads
        list <bizible2__Bizible_Person__c> leadBzPersons = [Select Id, bizible2__Lead__c, (Select Id From bizible2__Bizible_Touchpoints__r Order By bizible2__Touchpoint_Date__c DESC) From bizible2__Bizible_Person__c Where bizible2__Lead__c in : leadPartnerMap.keySet()];
        system.debug('@@leadBzPersons:'+leadBzPersons);
        for (bizible2__Bizible_Person__c bzperson : leadBzPersons){
            if (bzperson.bizible2__Bizible_Touchpoints__r.size() > 0){
                system.debug('@@bzperson.bizible2__Lead__c:'+bzperson.bizible2__Lead__c);
                integer bzTPcounter = 0;
                for (bizible2__Bizible_Touchpoint__c bzTP : bzperson.bizible2__Bizible_Touchpoints__r){ //Sorted by Touchpoint Date
                    Partner_Touch_Point__c ptp = new Partner_Touch_Point__c(
                        Partner__c = leadPartnerMap.get(bzperson.bizible2__Lead__c),
                        Bizible_Touchpoint__c = bzTP.Id,
                        Latest_Touchpoint__c = ( bzTPcounter == 0 ? true : false ) //1st found record will be the latest Partner TP
                    );
                    bzTPcounter++;
                    partnerTPlist.add(ptp);
                }
            }
        }
        return partnerTPlist;
    }

}
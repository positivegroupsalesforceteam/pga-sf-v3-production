/*
	@Description: class containing the invocable method for create Box Folders in Case
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	9/14/18     JBACULOD Created
        -   6/24/19     JBACULOD Added retry and FOR UPDATE due to UNABLE_TO_LOCK_ROW issue
        -   25/06/19    RDAVID extra/task24400704-POSConsumerPropertyAutoCreateBoxFolders - Added Logic for Auto-creating Box Folder on POS: Consumer - Property Finance
                        *NOTE: Once deploy in Prod, need to add setting manually in the Box_Partition_Settings__mdt for auto-creating Box folders
        -   9/07/2019   RDAVID extra/task24400704-POSConsumerPropertyAutoCreateBoxFolders - Updated folder structure for POS: Consumer - Property Finance
*/
public class Case_BoxOnCreateCase implements Queueable, Database.AllowsCallouts {

    private static list <Box_Partition_Settings__mdt> boxpset {get;set;}
    public static string test_objRecordFolderID {get;set;}
    private static box.Toolkit boxToolkit = new box.Toolkit();
    private list <ID> CBoxcseIDs {get;set;}
    public Case_BoxOnCreateCase(list <ID> cseIDs){
        CBoxcseIDs = cseIDs;
    }

    @InvocableMethod(label='Create Box Folder' description='creates a Box Folder for Case when Case got created')
	public static void caseBoxFolderCreate (list <ID> cseIDs) {

        if (TriggerFactory.trigset.Enable_Case_Box_Partition_Auto_Create__c){
            Case_BoxOnCreateCase cbox = new Case_BoxOnCreateCase(cseIDs);
            ID jobID = System.enqueueJob(cbox); //Need to be asynchronous due to post DMLs on Case
        }

    }

    public void execute(QueueableContext context){
        system.debug('@@CBoxcseIDs:'+CBoxcseIDs);
        createBoxFolder(this.CBoxcseIDs);
    }

    public void createBoxFolder(list <ID> cseIDs){
        //Retrieve Box Partition Settings
        boxpset = [Select Id, Partition__c, Box_Folder_ID__c, Sub_Folders__c, Recordtype_Name__c From Box_Partition_Settings__mdt WHERE Object__c = 'Case']; //25/06/19    RDAVID extra/task24400704-POSConsumerPropertyAutoCreateBoxFolders

        //Retrieve Case
        list <Case> cselist = [Select Id, CaseNumber, Box_Folder_ID__c, Status, Stage__c, Partition__c, Channel__c, Recordtype.Name From Case Where Id in : cseIDs];

        map <string,Case> updateCaseMap = new map <string,Case>();
        //Iterate on Cases w/ Box folders to be created
        for (Case cse : cselist){
            //Create Case Box Folder and its Sub Folders, move folder base on Partition
            if (createmoveParentcreateSubFolders(cse, boxpset)){
                system.debug('@@0');
                updateCaseMap.put(cse.Id, cse);
            }
        }
        
        system.debug('@@updateCaseMap:'+updateCaseMap);
        if (updateCaseMap.size() > 0){
            //Update Case w/ Box Folder ID
            try{
                list <Case> recordLockForUpdate = [Select Id From Case Where Id in : updateCaseMap.keySet() FOR UPDATE]; //JBACU 6/24/19 
                update updateCaseMap.values();
            }
            catch(Exception e){
                update updateCaseMap.values(); //JBACU 6/24/19 Added retry due to UNABLE_TO_LOCK_ROW issue
            }
        }

        // ALWAYS call this method when finished.Since salesforce doesn't allow http callouts after dml operations, we need to commit the pending database inserts/updates or we will lose the associations created
        boxToolkit.commitChanges();
    }

    private static boolean createmoveParentcreateSubFolders(Case cse, list <Box_Partition_Settings__mdt> boxpset){
        for (Box_Partition_Settings__mdt bpset : boxpset){
            system.debug('@@bpSet partition:'+bpSet.Partition__c);
            
            if (cse.Partition__c == bpset.Partition__c && String.IsBlank(bpset.Recordtype_Name__c) && cse.Recordtype.Name != 'POS: Consumer - Property Finance' ||
                cse.Partition__c == bpset.Partition__c && !String.IsBlank(bpset.Recordtype_Name__c) && cse.Recordtype.Name == bpset.Recordtype_Name__c){ //25/06/19    RDAVID extra/task24400704-POSConsumerPropertyAutoCreateBoxFolders
                system.debug('@@cse'+cse.CaseNumber);
                //Create Box folder
                string objRecordFolderId = boxToolkit.createFolderForRecordId(cse.Id, null, true); 
                if (Test.isRunningTest()) objRecordFolderID = test_objRecordFolderID;
                system.debug('@@objRecordFolderId:'+objRecordFolderId);
                //Move Box folder base on Case Partition
                boolean moveFolder = boxToolkit.moveFolder(objRecordFolderID, bpset.Box_Folder_ID__c,'');
                system.debug('@@moveFolder:'+moveFolder);
                string sfolder = bpset.Sub_Folders__c;
                set <string> subfolders = new set <string>();
                boolean breakfolder = false;
                do {
                    string mtype = sfolder.substringBetween('[',']');
                    if (mtype == null) breakfolder = true;
                    if (mtype != null) {
                        subfolders.add(mtype);
                        sfolder = sfolder.remove('['+mtype+']');
                    }
                }
                while (!breakfolder);
                for (string strfolderName : subfolders){
                    if(strfolderName.containsIgnoreCase('>')){
                        String[] arrTest = strfolderName.split('>'); 
                        String childfolder = boxToolkit.createFolder(arrTest[0], objRecordFolderID, ''); 
                        String subchildfolder = boxToolkit.createFolder(arrTest[1], childfolder, ''); 
                        if(arrTest.size() == 3){ //9/07/2019   RDAVID extra/task24400704-POSConsumerPropertyAutoCreateBoxFolders
                            String subgrandchildfolder = boxToolkit.createFolder(arrTest[2], subchildfolder, '');
                        }
                    }
                    else{
                        String childfolder = boxToolkit.createFolder(strfolderName, objRecordFolderID, ''); 
                    }
                }
                cse.Box_Folder_ID__c = objRecordFolderId;
                //break;   
                return true; 
            } 
            
            
        }
        return false;
    }

}
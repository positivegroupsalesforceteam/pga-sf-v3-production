/**

 */
@isTest
public class TestHelper {
    
    public static String xmlResponse = '<?xml version="1.0" encoding="UTF-8"?><BCAmessage service-request-id="02105641462" type="RESPONSE"><BCAservices><BCAservice><BCAservice-code>VDA001</BCAservice-code><BCAservice-code-version>V00</BCAservice-code-version><BCAservice-client-ref>9934</BCAservice-client-ref><BCAservice-data><response version="1-36-0"><enquiry-report><primary-match type="strong"><bureau-reference>183722374</bureau-reference><individual><individual-name create-date="2012-04-16"><family-name>STEWART</family-name><first-given-name>PAUL</first-given-name></individual-name><gender type="male" /><date-of-birth>1972-04-15</date-of-birth><drivers-licence-number country-code="AU" /><address create-date="2013-09-26" type="residential-current"><street-number>3</street-number><street-name>HOLBROOKS</street-name><street-type code="RD" /><suburb>FLINDERS PARK</suburb><state>SA</state><postcode>5025</postcode><country country-code="AU" /></address><address create-date="2013-09-26" type="residential-previous"><street-number>170</street-number><street-name>GEORGE</street-name><street-type code="STR" /><suburb>LIVERPOOL</suburb><state>NSW</state><postcode>2170</postcode><country country-code="AU" /></address><address create-date="2013-09-19" type="residential-previous"><street-number>25</street-number><street-name>CHALMERS</street-name><street-type code="STR" /><suburb>BALGOWNIE</suburb><state>NSW</state><postcode>2519</postcode><country country-code="AU" /></address></individual><individual-consumer-credit-file><credit-enquiry enquiry-date="2013-09-26" type="credit-application"><account-type code="RM">Real Estate Mortgage</account-type><enquiry-amount>156081</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>4192</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-26" type="credit-application"><account-type code="RM">Real Estate Mortgage</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3892</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-19" type="credit-application"><account-type code="RM">Real Estate Mortgage</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3478</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-19" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>4007</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-19" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3130</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-18" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3957</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-18" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3849</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-18" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3922</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-18" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3886</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-17" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3869</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-17" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3863</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-17" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3840</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-17" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3748</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-16" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3716</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-16" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3692</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-13" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3348</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-13" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3358</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-13" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3585</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-12" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3572</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-12" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3441</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-12" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3511</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-12" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3495</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-12" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3529</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-11" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3495</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-11" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3484</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-11" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3478</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-11" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3476</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-11" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3441</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-11" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3440</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-10" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3417</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-10" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3406</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-10" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3377</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-10" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3373</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-10" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>54696</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3358</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-05" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3130</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-04" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>3111</client-reference></credit-enquiry><credit-enquiry enquiry-date="2013-09-03" type="credit-application"><account-type code="LC">Loan Contract</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>HOMESTART FINANCE TEST CODE</credit-enquirer><client-reference>2980</client-reference></credit-enquiry><credit-enquiry enquiry-date="2012-05-05" type="credit-application"><account-type code="CM">Chattel Mortgage</account-type><enquiry-amount>27000</enquiry-amount><role type="principal" /><credit-enquirer>VOLKSWAGEN FS TEST</credit-enquirer><client-reference>8CEF8E1EEA4395F</client-reference></credit-enquiry><credit-enquiry enquiry-date="2012-04-16" type="credit-application"><account-type code="CM">Chattel Mortgage</account-type><enquiry-amount>27000</enquiry-amount><role type="principal" /><credit-enquirer>VOLKSWAGEN FS TEST</credit-enquirer><client-reference>8CEE9E462D2A35C</client-reference></credit-enquiry><credit-enquiry enquiry-date="2014-02-07" type="authorised-agent-enquiry"><account-type code="CM">Chattel Mortgage</account-type><enquiry-amount>0</enquiry-amount><role type="principal" /><credit-enquirer>FINA</credit-enquirer><client-reference>9934</client-reference></credit-enquiry></individual-consumer-credit-file></primary-match><score-data><score type="VS 1.1 CONSUMER + COMMERCIAL"><scorecard-model>0302</scorecard-model><relative-risk>1.5</relative-risk><vedascore-1.1-index>3.494</vedascore-1.1-index><applicant-odds>17.8</applicant-odds><contributing-factor impact="Greatly Decreases Risk">Lack of Consumer Adverse Information</contributing-factor><contributing-factor impact="Moderately Increases Risk">Number of Consumer Credit Applications</contributing-factor><contributing-factor impact="Moderately Increases Risk">Authorised Agent Information</contributing-factor><contributing-factor impact="Marginally Increases Risk">Applications from Particular Industry Groups</contributing-factor><population><population-odds>11.9</population-odds></population><veda-score>615</veda-score><percentile>39</percentile></score></score-data></enquiry-report></response></BCAservice-data></BCAservice></BCAservices></BCAmessage>';

    public static Contact createContact(){
        
        Contact c = new Contact();
        c.salutation = 'Mr';
        c.Drivers_License_Number__c = '05432567';
        c.FirstName = 'TestContact';
        c.lastname = 'Lastname';
        c.birthDate= Date.today().addDays(-20);
        c.MailingCity ='Melbourne';
        c.MailingStreet = 'Exhibition Street';
        c.MailingState = 'Victoria';
        c.MailingPostalCode ='3000';        
        insert c;
        
        return c;       
    }
    
     public static Lead createLead(){
        
        Lead rec = new Lead();
        rec.salutation = 'Mr';
        rec.Drivers_licence_number__c = '05432567';
        rec.FirstName = 'TestContact';
        rec.lastname = 'Lastname';
        rec.Date_of_Birth__c= Date.today().addDays(-20);
        rec.City ='Melbourne';
        rec.Street = '25 Exhibition Street';
        rec.State = 'Victoria';
        rec.PostalCode ='3000'; 
        rec.Email ='kk@gmail.com';
        rec.Available_Income__c = 5000;
        rec.Centrelink_Benefits__c = 1200;
        rec.Living_Expenses_currency__c = 500;
        rec.LeadSource = 'Other';
        rec.Loan_Product__c = 'Consumer Loan';
        insert rec;
        
        return rec;     
    }
    
     public static Account createPersonAccount(){
        Id perID = [select Id from RecordType where Name = 'Property' and SobjectType = 'Account'].Id;    
        Account rec = new Account();
        rec.salutation = 'Mr';
        rec.Drivers_Licence_Number__c = '05432567';
        rec.FirstName = 'TestContact';
        rec.lastname = 'Lastname';
        rec.Date_of_Birth__c= Date.today().addDays(-20);
        rec.BillingCity ='Melbourne';
        rec.BillingStreet = '25 Exhibition Street';
        rec.BillingState = 'Victoria';
        rec.BillingPostalCode ='3000';   
        rec.RecordTypeId =  perID;
        insert rec;
        
        System.debug('record id ' + perID);
        System.debug('rec data id ' + rec);
        
        return rec;     
    }
    
    public static Opportunity createPersonAccountOpportunity(Account acct){
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp1';
        opp.StageName = 'Application Form Sent';
        opp.CloseDate = Date.today().addDays(30);
        opp.AccountId = acct.Id;
        insert opp;
        
        return opp;     
    }
    
    public static Attachment createAttachmentForLead(Lead leadObj){
        
        Blob pdfBlob = Blob.valueOf(xmlResponse);
        Attachment a = new Attachment(parentId = leadObj.id, name=leadObj.firstname + ' ' +leadObj.lastName + '-Test Credit Check.pdf', body = pdfBlob);
        insert a;
        
        return a;
    }
    
    public static Attachment createAttachmentForPersonAccount(Account acct){
        
        Blob pdfBlob = Blob.valueOf(xmlResponse);
        Attachment a = new Attachment(parentId = acct.id, name=acct.firstname + ' ' +acct.lastName + '-Test Credit Check.pdf', body = pdfBlob);
        insert a;
        
        return a;
    }
    
    public static Attachment createAttachmentForPersonAcctOpportunity(Opportunity opp){
        
        Blob pdfBlob = Blob.valueOf(xmlResponse);
        Attachment a = new Attachment(parentId = opp.id, name=opp.Account.firstname + ' ' +opp.Account.firstname + '-Test Credit Check.pdf', body = pdfBlob);
        insert a;
        
        return a;
    }
    
    public static Attachment createAttachmentForContact(Contact contact){
        
        Blob pdfBlob = Blob.valueOf(xmlResponse);
        Attachment a = new Attachment(parentId = contact.id, name=contact.firstname + ' ' +contact.lastName + '-Test Credit Check.pdf', body = pdfBlob);
        insert a;
        
        return a;
    }
    
    public static Pending_Action__c createPendingAction(String relatedRecordId, String targetObject, String functionName){
        Pending_Action__c pend = new Pending_Action__c();
        pend.Related_Record_Id__c = relatedRecordId;
        pend.Target_Object__c=targetObject;
        pend.CW_Call_End_Time__c=System.now();
        pend.Function_Name__c=functionName;
        return pend;
    }

    public static Task createTask(String whoId, String whatId, String rType){

        Task task = new Task(   RecordTypeId = rType,
                                WhoId = whoId,
                                WhatId = whatId,
                                Subject = 'Outbound call to +61434338598',
                                Status = 'Completed',
                                Priority = 'Normal',
                                OwnerId = UserInfo.getUserId(),
                                CallDurationInSeconds = 5,
                                CallType = 'Outbound',
                                CallObject = '015f28d7-7989-4c33-a512-3a4d48b6c749',
                                IsReminderSet = false,
                                dc__Force_Rescore__c = false,
                                NVMContactWorld__CW_Call_End_Time__c = DateTime.newInstance(2017,10,17,5,338,20337),
                                NVMContactWorld__CW_Call_Start_Time__c = DateTime.newInstance(2017,10,17,5,338,20332),
                                NVMContactWorld__ContactWorld_Number__c = '61438241631',
                                NVMContactWorld__Customer_Number__c = '+61434338598',
                                /*cirrusadv__Created_by_Cirrus_Insight__c = false,
                                cirrusadv__Email_Opened__c = false,
                                cirrusadv__Num_of_Replies__c = 0.0,
                                cirrusadv__isTracked__c = false, */
                                NVMContactWorld__Was_Call_Recorded__c = false);
        return task;

    }
}
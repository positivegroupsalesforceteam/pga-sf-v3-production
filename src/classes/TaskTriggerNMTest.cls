/*
    @Description: test class of TaskUtil, TaskTriggerHandler
    @Author: Jesfer Baculod - Positive Group
    @History:	09/15/2017	- Created, added test for TaskUtils
*/        
@isTest
public class TaskTriggerNMTest {

	private static final string LEAD_STATS_OPEN = Label.Lead_Status_Open; //Open
	private static final string TASK_STATS_NS = Label.Task_Status_Not_Started; //Not Started
	private static final string TASK_SUBJ_PARDOT = Label.Task_Pardot_Subject; //Lead Generated [pardot]
	private static final string TASK_SUBJ_SMS_S = Label.Task_SMS_Sent_Subject; //SMS: Sent
	private static final string TASK_SUBJ_SMS_R = Label.Task_SMS_Received_Subject; //SMS: Received


	@testsetup static void setup(){
		Trigger_Settings1__c trigSet = new Trigger_Settings1__c();
		trigSet.Enable_Triggers__c = true;
        trigSet.Enable_Case_Trigger__c = true;
        trigSet.Enable_Case_Auto_Create_Application__c = true;
        trigSet.Enable_Role_Auto_Populate_Case_App__c = true;
		trigSet.Enable_Task_Trigger__c = true;
		trigSet.Enable_Task_Update_Case_Attempt__c = true;
		insert trigset;

		TestDataFactory.Case2FOSetupTemplate();
	}

	static testmethod void testkTaskUtils(){

		Test.startTest();
			Case[] cse = [SELECT Id, AccountId, Stage__c FROM Case WHERE Status =: 'New'];

			system.assertEquals(cse.size(),10);
			system.assertEquals(cse[0].Stage__c,'Open');
			Task tsk = new Task(
					WhatId = cse[0].Id,
					CallType = 'Outbound',
					Status = 'Completed',
					Subject = 'Outbound call to 0231231323',
					Call_Purpose__c = CommonConstants.TASKCALLPURPOSE_ATTEMPTCON
				);
			insert tsk;
			Case[] cse2 = [SELECT Id, AccountId, Attempted_Contact_Status__c FROM Case WHERE Id =: cse[0].Id];
			// system.assertEquals(cse2[0].Attempted_Contact_Status__c,'Attempted 1');
		Test.stopTest();

	}
	static testmethod void testkTaskUtils2(){

		Test.startTest();
			Case[] cse = [SELECT Id, AccountId, Stage__c,Attempted_Contact_Status__c FROM Case WHERE Status =: 'New'];

			system.assertEquals(cse.size(),10);
			system.assertEquals(cse[0].Stage__c,'Open');
			Task tsk = new Task(
					WhatId = cse[0].Id,
					CallType = 'Outbound',
					Status = 'Completed',
					Subject = 'Outbound call to 0231231323',
					Call_Purpose__c = CommonConstants.TASKCALLPURPOSE_ATTEMPTCON
				);
			insert tsk;
			Case[] cse2 = [SELECT Id, AccountId, Attempted_Contact_Status__c FROM Case WHERE Id =: cse[0].Id];
			// system.assertEquals(cse2[0].Attempted_Contact_Status__c,'Attempted 1');
			
			Case[] cse3 = [SELECT Id, AccountId, Stage__c,Attempted_Contact_Status__c FROM Case WHERE Status =: 'New' AND Id !=: cse2[0].Id];
			system.assertEquals(cse3.size(),9);
			List<Task> tskList = new List<Task>();
			for(Integer i=0; i < cse3.size(); i++){
				cse3[i].Stage__c = 'Attempted Contact';
				cse3[i].Attempted_Contact_Status__c = 'Attempted '+ (i+1);
				system.debug('>>>>>'+cse[i].Attempted_Contact_Status__c);
				Task tsk1 = new Task(
					WhatId = cse3[i].Id,
					CallType = 'Outbound',
					Status = 'Completed',
					Subject = 'Outbound call to 0231231323'+i,
					Call_Purpose__c = CommonConstants.TASKCALLPURPOSE_ATTEMPTCON
				);
				tskList.add(tsk1);
			}
			update cse3;
			insert tskList;
		Test.stopTest();

	}

	static testmethod void testkTaskUtils3(){

		Test.startTest();
			Case[] cse = [SELECT Id, AccountId, Stage__c,Attempted_Contact_Status__c FROM Case WHERE Status =: 'New'];

			system.assertEquals(cse.size(),10);
			system.assertEquals(cse[0].Stage__c,'Open');
			Task tsk = new Task(
					WhatId = cse[0].Id,
					CallType = 'Outbound',
					Status = 'Completed',
					Subject = 'Outbound call to 0231231323',
					Call_Purpose__c = CommonConstants.TASKCALLPURPOSE_ATTEMPTCON
				);
			insert tsk;
			Case[] cse2 = [SELECT Id, AccountId, Attempted_Contact_Status__c FROM Case WHERE Id =: cse[0].Id];
			// system.assertEquals(cse2[0].Attempted_Contact_Status__c,'Attempted 1');
			
			Case[] cse3 = [SELECT Id, AccountId, Stage__c,Attempted_Contact_Status__c FROM Case WHERE Status =: 'New' AND Id !=: cse2[0].Id];
			system.assertEquals(cse3.size(),9);
			List<Task> tskList = new List<Task>();
			
			for(Integer i=0; i < 9; i++){
				Task tsk1 = new Task(
						WhatId = cse[i].Id,
						CallType = 'Outbound',
						Status = 'Completed',
						Subject = 'Outbound call to 0231231323'+i,
						Call_Purpose__c = CommonConstants.TASKCALLPURPOSE_ATTEMPTCON
				);
				tskList.add(tsk1);
			}
			insert tskList;

			for(Integer i=0; i < cse3.size(); i++){
				cse3[i].Stage__c = 'Attempted Contact';
				cse3[i].Attempted_Contact_Status__c = 'Attempted '+ (i+1);
				system.debug('>>>>>'+cse[i].Attempted_Contact_Status__c);
				tskList[i].WhatId = cse3[i].Id;
				tskList[i].Subject = 'Outbound call to 0231231323'+i;
			}
			update cse3;
			update tskList;
        	delete tskList;
		Test.stopTest();

	}
}
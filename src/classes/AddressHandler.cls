/** 
* @FileName: AddressHandler
* @Description: Trigger Handler for the Address__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 15/06/2018 RDAVID Created Class 
* 1.1 07/07/2018 JBACULOD Added check Location if UNKNOWN
* 1.2 23/07/2018 RDAVID Fix Location auto-populate issues
**/ 

public without sharing class AddressHandler implements ITrigger {	
	// SD-101>SD-102 SF - NM - Auto-Populate Address fields when Location is populated
	private List<Address__c> addressToUpdateList = new List<Address__c>();
	private static Map<Id,Id> addLocIdMap = new Map<Id,Id>();
	private static Map<Id,Location__c> locMap = new Map<Id,Location__c>();
	private static Map<String,Address__c> checkLocMap = new map <String, Address__c>();
	private final string LOC_UNKNOWN = 'UNKNOWN';
	private final string LOC_OVERSEAS = 'OVERSEAS';
	private list <Location__c> loclist = new list <Location__c>();
	// Constructor
	public AddressHandler(){

	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
		Map<Id,Address__c> oldAddressMap = (Trigger.isUpdate)?(Map<Id,Address__c>)Trigger.oldMap:null;
		Set<Id> locIDs = new Set<Id>();
		if(Trigger.IsInsert || Trigger.IsUpdate){
			
			for(Address__c address :  (List<Address__c>)Trigger.new){
				// SD-101>SD-102 SF - NM - Auto-Populate Address fields when Location is populated
				if(TriggerFactory.trigset.Enable_Address_Populate_Location__c){
					if (address.Location__c != NULL){
						if(oldAddressMap == NULL){
							locIDs.add(address.Location__c);
						}
						else{
							if(oldAddressMap.get(address.Id).Location__c != address.Location__c){
								addLocIdMap.put(address.Id,address.Location__c);
							}
							// 23/07/2018 RDAVID Fix - Handle update of Suburb and Postcode to update find Location match
							if(oldAddressMap.get(address.Id).Suburb__c != address.Suburb__c 
							|| oldAddressMap.get(address.Id).Postcode__c != address.Postcode__c){
								string key = address.Postcode__c + '-' + address.Suburb__c;
								key = key.toUpperCase();
								checkLocMap.put(key, address);
							}
						}					
					}
					else{ 
						if (address.Postcode__c != null || address.Suburb__c != null){ //Find Location
                            string key = address.Postcode__c + '-' + address.Suburb__c;
                            key = key.toUpperCase();
							checkLocMap.put(key, address);
						}
					}
				}
			}
		}
		if (checkLocMap.size() > 0){
			loclist = [Select Id, Name, Postcode__c, Suburb_Town__c, State_Acr__c, State__c, Country__c From Location__c Where Name =: LOC_UNKNOWN OR Name =: LOC_OVERSEAS OR Name in : checkLocMap.keySet()]; //Get UNKNOWN, OVERSEAS Location
		}
		if(locIDs.size() > 0 || !addLocIdMap.isEmpty()){
			locMap = new Map<Id,Location__c>([SELECT Id,Name,Postcode__c,Suburb_Town__c,State_Acr__c,State__c,Country__c FROM Location__c WHERE ( Id IN: addLocIdMap.values() OR Id IN: locIDs) AND Name != : LOC_UNKNOWN AND Name != : LOC_OVERSEAS ]);
		}
	}
	
	public void bulkAfter(){

	}
		
	public void beforeInsert(SObject so){
		Address__c address = (Address__c)so;
		if (checkLocMap.size() > 0){
			AddressGateway.populateFoundLocation(address, checkLocMap, loclist);
		}
		AddressGateway.populateAddressWithLocation(address, locMap, null, false);
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		Address__c address = (Address__c)so;
		if (checkLocMap.size() > 0){
			AddressGateway.populateFoundLocation(address, checkLocMap, loclist);
		}
		AddressGateway.populateAddressWithLocation(address, locMap, addLocIdMap, true);
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){
	}
	
	public void afterUpdate(SObject oldSo, SObject so){
	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		System.debug('Address__c > Queries :' + Limits.getQueries() + ' / '+Limits.getLimitQueries());
		System.debug('Address__c > Rows Queried :' + Limits.getDmlRows() + ' / '+Limits.getLimitDmlRows());
		System.debug('Address__c > DML : ' +  Limits.getDmlStatements() + ' / '+Limits.getLimitDmlStatements());
	}	
}
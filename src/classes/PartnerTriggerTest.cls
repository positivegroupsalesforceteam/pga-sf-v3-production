/**
 * @File Name          : PartnerTriggerTest.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 06/11/2019, 10:42:29 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    06/11/2019   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
@isTest
public class PartnerTriggerTest {
    public static List<Location__c> locList = new List<Location__c>(); 
    @testsetup static void setup(){
        //Turn On Trigger 
		Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
		triggerSettings.Enable_Triggers__c = true;
		triggerSettings.Enable_Application_Sync_App_2_Case__c = true;
		triggerSettings.Enable_Application_Trigger__c= true;
		triggerSettings.Enable_Asset_Trigger__c= true;
		triggerSettings.Enable_Case_Auto_Create_Application__c= true;
		triggerSettings.Enable_Case_Scenario_Trigger__c= true;
		triggerSettings.Enable_Case_SCN_calculateSLATime__c= true;
		triggerSettings.Enable_Case_SCN_recordStageTimestamps__c= true;
		triggerSettings.Enable_Case_Trigger__c= true;
		triggerSettings.Enable_Expense_Trigger__c= true;
		triggerSettings.Enable_FO_Auto_Create_FO_Share__c= true;
		triggerSettings.Enable_FOShare_Rollup_to_Role__c= true;
		triggerSettings.Enable_FOShare_Trigger__c= true;
		triggerSettings.Enable_Income_Trigger__c= true;
		triggerSettings.Enable_Lender_Automation_Call_Lender__c= true;
		triggerSettings.Enable_Liability_Trigger__c= true;
		triggerSettings.Enable_Role_Address_Populate_Address__c= true;
		triggerSettings.Enable_Role_Address_Toggle_Current__c= true;
		triggerSettings.Enable_Role_Address_Trigger__c= true;
		triggerSettings.Enable_Role_Auto_Populate_Case_App__c= true;
		triggerSettings.Enable_Role_Rollup_to_Application__c= true;
		triggerSettings.Enable_Role_To_Account_Propagation__c= true;
		triggerSettings.Enable_Role_Toggle_Primary_Applicant__c= true;
		triggerSettings.Enable_Role_Trigger__c= true;
		triggerSettings.Enable_Triggers__c= true;
        triggerSettings.Enable_Account_Sync_Name_to_Role__c=true;
        triggerSettings.Enable_Account_Trigger__c =true;
		triggerSettings.Enable_Partner_Sync_Account_Contact__c=true;
        triggerSettings.Enable_Partner_Trigger__c=true;
        triggerSettings.Enable_Sync_Referral_Company_and_Partner__c=true;
        triggerSettings.Enable_Referral_Company_Trigger__c=true;
		triggerSettings.Enable_Address_Trigger__c = true;
        triggerSettings.Enable_Address_Populate_Location__c = true;
		upsert triggerSettings Trigger_Settings1__c.Id;

        //Create Test Account
		List<Account> accList = TestDataFactory.createGenericPersonAccount('fName', 'lName', CommonConstants.PACC_RT_I, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		caseList[0].Application_Record_Type__c = 'Consumer - Individual';

		Database.insert(caseList);

        Location__c loc1 = new Location__c (Name='3824-TRAFALGAR',Postcode__c='3824',Suburb_Town__c='TRAFALGAR',State_Acr__c='VIC',State__c='Victoria',Country__c='Australia');
        Location__c loc2 = new Location__c (Name='3101-KEW',Postcode__c='3101',Suburb_Town__c='KEW',State_Acr__c='VIC',State__c='Victoria',Country__c='Australia');
        List<Location__c> locList = new List<Location__c>{loc1,loc2};
        Database.insert(locList);

        List<Location__c> locListUNK = TestDataFactory.createLocation(1,'UNKNOWN','','','','','');
		locList = TestDataFactory.createLocation(1,'2000-BARANGAROO','2000','BARANGAROO','NSW','New South Wales','Australia');
		List<Location__c> locListTwo = TestDataFactory.createLocation(1,'2000-DAWES POINT','2000','DAWES POINT','NSW','New South Wales','Australia');
		locListTwo.addAll(locList);
		locListTwo.addAll(locListUNK);
		Database.insert(locListTwo);
        ABN__c abn = new ABN__c(Name = '12344321' , Registered_ABN__c='12344321');
    }
    static testmethod void testPartnerSync(){
        Test.StartTest();
            locList = [SELECT Id FROM Location__c LIMIT 3];
	    	System.assertEquals(3,locList.size());

            Address__c testAddress = new Address__c(Location__c = locList[0].Id,Street_Number__c='123',Street_Name__c='REX',Street_type__c='Suburb');
			Database.insert(testAddress);
            Id rtId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId();
			Partner__c partner = new Partner__c(Name = 'Connective', Partner_Type__c = 'Mortgage Aggregator', m_Rating__c = 'Committed', m_Company_Type__c = 'Mortgage Aggregator', RecordTypeId = rtId);
            insert partner;
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            Partner__c refCompanyPartner = new Partner__c();//(Name = 'P1551599', Company_Type__c = 'Mortgage Broker', Rating__c = 'Cold', Partner_Parent__c = partner.Id, Address_Line_1__c = '123 ABC SXO',Postcode__c='3101',Suburb__c='KEW',State__c='VIC');
            refCompanyPartner.Name = 'P1551513';
            refCompanyPartner.RecordTypeId = rtId;
            refCompanyPartner.Partner_Phone__c = '091231231';
            refCompanyPartner.Partner_Website__c = 'partner.com';
            refCompanyPartner.m_Registered_Business_Name__c = '1234';
            refCompanyPartner.m_Nodifi_Onboarding_Stage__c = 'Demo Requested';
            refCompanyPartner.Partner_ABN__c = '12344321';
            refCompanyPartner.Partner_Legal_Name__c = '12344321';
            refCompanyPartner.Partner_Address__c = testAddress.Id;
            insert refCompanyPartner;
        Test.stopTest();
	}
    
    public static testMethod void testPartnerSync1() {
        try{
            Referral_Company__c connective = new Referral_Company__c(Name = 'Connective', Company_Type__c = 'Mortgage Aggregator', Rating__c = 'Committed');
            insert connective;
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            Referral_Company__c refCompany = new Referral_Company__c(Name = 'P1551512', Company_Type__c = 'Mortgage Broker', Rating__c = 'Cold', Related_Site__c = connective.Id, Address_Line_1__c = '123 ABC SXO',Postcode__c='3101',Suburb__c='KEW',State__c='VIC');
            insert refCompany;
            refCompany.Name = 'P1551513';
            refCompany.Postcode__c = '3824';
            refCompany.Suburb__c = 'TRAFALGAR';
            refCompany.Dealer_Type__c = 'Independant';
            refCompany.Business_Phone_Number__c = '12345';
            refCompany.Registered_Business_Name__c = '1234';
            refCompany.Nodifi_Onboarding_Stage__c = 'Demo Requested';
            refCompany.Address_Line_1__c = '124 ABC SXO';
            refCompany.State__c = 'VIC';
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            update refCompany;
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            delete refCompany;

            locList = [SELECT Id FROM Location__c LIMIT 3];
	    	System.assertEquals(3,locList.size());
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            Address__c testAddress = new Address__c(Location__c = locList[0].Id);
			Database.insert(testAddress);
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            Id rtId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId();
			// Partner__c partner = new Partner__c(Name = 'Connective', Partner_Type__c = 'Mortgage Aggregator', m_Rating__c = 'Committed', m_Company_Type__c = 'Mortgage Aggregator', RecordTypeId = rtId);
            // insert partner;
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            Partner__c refCompanyPartner = [SELECT Id FROM Partner__c LIMIT 1];//(Name = 'P1551599', Company_Type__c = 'Mortgage Broker', Rating__c = 'Cold', Partner_Parent__c = partner.Id, Address_Line_1__c = '123 ABC SXO',Postcode__c='3101',Suburb__c='KEW',State__c='VIC');
            System.assertNotEquals(refCompanyPartner,null);
            refCompanyPartner.Name = 'P1551513';
            refCompanyPartner.RecordTypeId = rtId;
            refCompanyPartner.Partner_Phone__c = '091231231';
            refCompanyPartner.Partner_Website__c = 'partner.com';
            refCompanyPartner.m_Registered_Business_Name__c = '1234';
            refCompanyPartner.m_Nodifi_Onboarding_Stage__c = 'Demo Requested';
            refCompanyPartner.Partner_ABN__c = '12344321';
            refCompanyPartner.Partner_Legal_Name__c = '12344321';
            refCompanyPartner.Partner_Address__c = testAddress.Id;
            refCompanyPartner.h_ReferralCompanyId__c = refCompany.Id;
            update refCompanyPartner;
        }
        catch(exception e){
        }
    }

    public static testmethod void testPartnerTP(){

        Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
        triggerSettings.Enable_Sync_Referral_Company_and_Partner__c=false;
        triggerSettings.Enable_Partner_Create_Partner_TPs__c=true;
        upsert triggerSettings Trigger_Settings1__c.Id;

        //Create test data for Lead
        Lead testlead = new Lead(
            LastName = 'Test LeadF',
            FirstName = 'Test LeadL',
            RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Nodifi Partner').getRecordTypeId()
        );
        insert testlead;

        //Create test data for Bizible Person
        bizible2__Bizible_Person__c bzPerson = new bizible2__Bizible_Person__c(
            bizible2__Lead__c = testlead.Id,
            Name = 'testbzperson'
        );
        insert bzPerson;

        //Create test data for Bizible Touchpoints
        list <bizible2__Bizible_Touchpoint__c> bzTPlist = new list <bizible2__Bizible_Touchpoint__c>();
        for (Integer i=0; i<3; i++){
            bizible2__Bizible_Touchpoint__c bzTP = new bizible2__Bizible_Touchpoint__c(
                Name = 'bzTouchpoint' + i,
                bizible2__Bizible_Person__c = bzPerson.Id
            );
            bzTPlist.add(bzTP);
        }
        insert bzTPlist;


        Test.starttest();
            Partner__c part = new Partner__c(
                h_LeadId__c = testlead.Id,
                Name = 'Test Partner',
                RecordTypeId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Brokerage').getRecordTypeId()
            );
            insert part;

            //Verify Partner Touch Points are created
            list <Partner_Touch_Point__c> partTPlist = [Select Id From Partner_Touch_Point__c Where Partner__c = : part.Id];
            system.assertEquals(partTPlist.size(), bzTPlist.size());

        Test.stopTest();
    }
}
@isTest
private class AttachmentTriggerTest {

	private static final string ACC_RT_INDIVIDUAL = Label.Contact_Individual_RT; //Individual
	
	@testsetup static void setup(){

		//create test data for Account
	    Id rtBAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get(ACC_RT_INDIVIDUAL).getRecordTypeId();
		Account acc = new Account(
				LastName = 'test account',
				RecordTypeId = rtBAcc,
				Previous_Employment_Status__c = 'Full-time'
			);
		insert acc;

		Id rtIndividualOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(ACC_RT_INDIVIDUAL).getRecordTypeId();
		Opportunity opp = new Opportunity(
				AccountId = acc.Id,
				StageName = 'Qualified',
				Base_rate__c = 1,
				Checked_Commissions_Correct__c = true,
				Employment_Reference_Completed__c = true,
				Residential_Reference_Completed__c = true,
				Vehicle_Purchase_Price__c = 1000,
				Total_amount_financed__c = 1000,
				Sale_Type__c = Label.Opp_Sale_Type_Dealer,
				Name = 'Test Opportunity ANZ 1',
				CloseDate = Date.today(),
				Approval_Date__c = Date.today(),
				Make__c = 'test',
				Model__c = 'triton',
				Loan_Reference__c = '123456789',
				New_or_Used__c = 'New',
				Loan_Product__c = 'Consumer Loan',
				Previous_Employment_Status__c = 'Full-time',
				Transmission__c = 'AT',
				Year__c = '2017',
				Loan_Rate__c = 50,
				Lender__c = 'ANZ',
				RecordTypeId = rtIndividualOpp
			);
		insert opp;


	}

	static testmethod void testsyncAccOppAttachment(){

		Account acc = [Select Id, Name, IsPersonAccount From Account];
		Opportunity opp = [Select Id, Name, AccountId From Opportunity];

		Trigger_Settings__c trigSet = new Trigger_Settings__c(
                Enable_Attachment_Trigger__c = true,
                Enable_Attachment_syncAccOppAttachment__c = true
            );
        insert trigset;

        Test.startTest();
        	Attachment att = new Attachment(
        			ParentId = acc.Id,
        			Name = 'test-CC.pdf',
        			Body = Blob.valueOf('test')
        		);
        	insert att;
        Test.stopTest();

        	//Retrieve copied attachment from Account to Opportunity
        	list <Attachment> oppatt = [Select Id, Name, ParentId From Attachment Where ParentId = : opp.Id];
        	//system.assert(oppatt.size() > 1);

	}

	static testmethod void testdeleteCCAttachment(){

		Account acc = [Select Id, Name, IsPersonAccount From Account];
		Opportunity opp = [Select Id, Name, AccountId From Opportunity];

		list <Attachment> attlist = new list <Attachment>();
		attlist.add(new Attachment(
        				ParentId = opp.Id,
        				Name = 'Test-CC.pdf',
	        			Body = Blob.valueOf('test')
        			));
		attlist.add(new Attachment(
        				ParentId = opp.Id,
        				Name = 'Test2-CC.pdf',
        				Body = Blob.valueOf('test')
        			));
        insert attlist;

		Trigger_Settings__c trigSet = new Trigger_Settings__c(
                Enable_Attachment_Trigger__c = true,
                Enable_Attachment_preventDeletionOfCC__c = true
            );
        insert trigset;

        Test.startTest();
        	try{
        		delete attlist[0];	
        	}
        	catch(Exception e){
        		system.assert(e.getMessage().contains(Label.Veda_Credit_Check_Attachment_Delete_Error));
        	}
        Test.stopTest();


	}

}
@isTest
private class NewReferenceControllerTest {

    @testsetup static void setup(){
        //Create test data for (Case, Application, Role, Account)
        TestDataFactory.Case2FOSetupTemplate();
    }

    private static testmethod void testNewReference(){
        Test.startTest();

            //Retrieve created Test Data
            Case cse = [Select Id, CaseNumber From Case limit 1];

            PageReference pref_cse = Page.NewReference;
            pref_cse.getParameters().put('id',cse.Id);
            Test.setCurrentPage(pref_cse);
            NewReferenceController nrcon = new NewReferenceController();
            nrcon.runCheatTest();

        Test.stopTest();
    }

}
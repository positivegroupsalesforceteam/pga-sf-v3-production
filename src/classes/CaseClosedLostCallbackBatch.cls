/**
 * @File Name          : CaseClosedLostCallbackBatch.cls
 * @Description        : 
 * @Author             : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Group              : 
 * @Last Modified By   : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Last Modified On   : 11/06/2019, 1:41:26 pm
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/06/2019, 10:02:19 am   Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Initial Version
**/
global class CaseClosedLostCallbackBatch implements Database.Batchable<sobject>{

    global CaseClosedLostCallbackBatch(){}

    global Database.QueryLocator start(Database.BatchableContext bc) {
		string query = 'Select Id From Case Where Partition__c = \'Positive\' AND Call_Back_Set__c = TODAY';
		return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List <Case> scope){
        TriggerFactory.isProcessedMap = new map <Id, boolean>(); //Reset isProcessedMap
        list <ID> caseIDs = new list <ID>();
        for (Case cse : scope){
            caseIDs.add(cse.Id);
        }
        system.debug('@@caseIDs:'+caseIDs);
        //Create New Case From Closed Lost Callback
        Case_CallbackNewCase.createNewCaseOnCallback(caseIDs); 
    }

    global void finish(Database.BatchableContext bc) {}

}
/** 
* @FileName: BizibleTouchpointQueueable
* @Description: Class implements Queueable Apex 
* @Copyright: Positivve (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.1 22/10 RDAVID extra/task26493146-BugCaseTP - Fix Issue on creating Case TPs, create a separate Queueable Apex to chain this log
**/ 

public class BizibleTouchpointQueueableForCSTP implements Queueable {
    // public Map<ID,Account> accMap;
    public List<Case_Touch_point__c> ctpToInsList;
    public Map<Id,Case_Touch_point__c> ctpToUpdMap;
    @testVisible private static Boolean doChainJob = true;

    public BizibleTouchpointQueueableForCSTP (List<Case_Touch_point__c> ctpToInsertList, Map<Id,Case_Touch_point__c> ctpToUpdateMap){
        this.ctpToInsList = ctpToInsertList;
        this.ctpToUpdMap = ctpToUpdateMap;
    }

    public void execute(QueueableContext context) {
        // System.debug('BizibleTouchpointQueueableForCSTP execute ctpToUpdMap.keyset() = '+ctpToUpdMap.keyset());
        if(!ctpToUpdMap.isEmpty()){
            try{
                list <Case> lockCaseForUpdate = [Select Id From Case Where Id in : ctpToUpdMap.keySet() FOR UPDATE];
                list <Case_Touch_point__c> lockCaseTPForUpdate = [Select Id From Case_Touch_point__c Where Id in : getCaseTPId(ctpToUpdMap.values()) FOR UPDATE];
                Database.SaveResult[] results = Database.update(ctpToUpdMap.values(),false); //Update CSTP DML Allow Partial 
                String processName = 'Update Case Touchpoints Records';
                if (results != null){
                    Boolean sendCustomErrorHandlingEmailNotif = false;
                    Set<String> errLogs = new Set<String>();
                    Integer errornum = 0;
                    for (Database.SaveResult result : results) {
                        if (!result.isSuccess()) {
                            sendCustomErrorHandlingEmailNotif = true;
                            Database.Error[] errs = result.getErrors();
                            Id recordID = result.getId();
                            for(Database.Error err : errs) {
                                // System.debug(err.getStatusCode() + ' - ' + err.getMessage());
                                errornum ++;
                                errLogs.add('Error #'+errornum+'<br/>'+'Record Id: '+recordID +'<br/> Status Code: ' + err.getStatusCode() + '<br/> Fields: ' + String.join(err.getFields(),',') + '<br/> Error Message: ' + err.getMessage());
                            }
                        }
                    }
                    if(sendCustomErrorHandlingEmailNotif){
                        CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                        CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Error',BizibleTouchpointQueueableForCSTP.class.getName() + ' - ' + processName,errLogs);
                        cerhand.logWrap = logWrapper;
                        cerhand.sendMail();
                    }
                }
            } catch (Exception e) {
                System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            }
        }
        if(doChainJob) ID jobID = System.enqueueJob(new BizibleTouchpointQueueableForCSTPIns(ctpToInsList));
    }

    public Set<Id> getCaseTPId (List<Case_Touch_point__c> ctpList){
        Set<Id> csTPId = new Set<Id>();
        for(Case_Touch_point__c ctp : ctpList){
            csTPId.add(ctp.Id);
        }
        return csTPId;
    }
}
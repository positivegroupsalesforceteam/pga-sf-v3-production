/*
	@Description: class for calculating SLA Age of Submitted to Lender
	@Author: Jesfer Baculod - Positive Group
	@History:
		- 10/25/2017 - Created
		- 15/05/2019 - RDAVID - extra/task24564956-OLDSLACleanUp - Comment this class
*/
public class Case_UpdateSLAWarningSubmittedtoLender {
	
	@InvocableMethod(label='SLA Next Warning Time - Submitted to Lender' description='updates next Warning Time of an SLA whenever an SLA is not yet completed')
	public static void caseSLAWarningTimeSTL (list <ID> cseIDs) {
		/*RDAVID - extra/task24564956-OLDSLACleanUp -
		//Retrieve default Business Hours
		BusinessHours bh = [select Id from BusinessHours where IsDefault=true]; 

		//Retrieve Cases to update
		list <Case> cselist = [Select Id, 
								SLA_Time_Submitted_to_Lender_Start__c, SLA_Warning_Time_Submitted_to_Lender__c, SLA_Started_Count_Submitted_to_Lender__c, SLA_Completed_Count_Submitted_to_Lender__c, SLA_Active_Submitted_to_Lender__c, SLA_Time_Submitted_to_Lender_mm__c
								From Case Where Id in : cseIDs];
        
        Datetime warningdate; 
		for (Case cse : cselist){
			warningdate = cse.SLA_Warning_Time_Submitted_to_Lender__c;
			cse.SLA_Warning_Time_Submitted_to_Lender__c = null;
			cse.SLA_Active_Submitted_to_Lender__c = null;
		}
		update cselist; //force update to retrigger SLA Warning
		system.debug('@@warningdate:'+warningdate);

		for (Case cse : cselist){
            cse.SLA_Active_Submitted_to_Lender__c = 'Yes';
            if (cse.SLA_Started_Count_Submitted_to_Lender__c != cse.SLA_Completed_Count_Submitted_to_Lender__c){
                cse.SLA_Warning_Time_Submitted_to_Lender__c = BusinessHours.add(bh.Id, warningdate, 900000); //Set succeeding warning of current SLA (add 15 minutes)
                DateTime warningtimeSTL = cse.SLA_Warning_Time_Submitted_to_lender__c;
				cse.SLA_Warning_Time_Submitted_to_lender__c = Datetime.newInstance(warningtimeSTL.year(), warningtimeSTL.month(), warningtimeSTL.day(), warningtimeSTL.hour(), warningtimeSTL.minute(), 0);
                cse.SLA_Time_Submitted_to_Lender_mm__c = Decimal.valueof(( BusinessHours.diff(bh.Id, cse.SLA_Time_Submitted_to_Lender_Start__c, warningdate ) / 1000) / 60 );  //returns SLA Age in minutes
                if (cse.SLA_Time_Submitted_to_Lender_mm__c == 44) cse.SLA_Time_Submitted_to_Lender_mm__c = 45; //workaround for business hours difference of SLA
				if (cse.SLA_Time_Submitted_to_Lender_mm__c == 59) cse.SLA_Time_Submitted_to_Lender_mm__c = 60; //workaround for business hours difference of SLA
                system.debug('@@slaWarningC');
            }
		}

		update cselist;
		*/
	}

}
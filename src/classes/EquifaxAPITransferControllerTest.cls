/** 
* @FileName: EquifaxAPITransferControllerTest
* @Description: Test class for EquifaxAPITransferController
* @Copyright: Positive (c) 2020 
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 6/01/2020 RDAVID - Created
*/

@isTest
public class EquifaxAPITransferControllerTest {

    @testSetup static void setupData() {
        Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
        TestDataFactory.activateAllTriggerSettingsNM(triggerSettings);
        upsert triggerSettings Trigger_Settings1__c.Id;

        Process_Flow_Definition_Settings1__c processFlowSettings = Process_Flow_Definition_Settings1__c.getOrgDefaults();
        TestDataFactory.activateAllProcessFlowSettingsNM(processFlowSettings);
        processFlowSettings.Enable_Case_Flow_Definitions__c = FALSE;
        processFlowSettings.Enable_Case_NVM_Flow_Definitions__c = FALSE;
        // processFlowSettings.Enable_Case_Assignment__c = FALSE;
        upsert processFlowSettings Process_Flow_Definition_Settings1__c.Id;
        
        Location__c loc = new Location__c(
            Name = '2304-KOORAGANG',
            Country__c = 'Australia',
            Postcode__c = '2304',
            State__c = 'New South Wales',
            State_Acr__c = 'NSW',
            Suburb_Town__c = 'KOORAGANG'
        );
        insert loc;

        //Create Test Account
        List<Account> accList = TestDataFactory.createGenericPersonAccount('First Name','TestLastName',CommonConstants.PACC_RT_I, 2);
        accList[0].Location__c = loc.Id;
        accList[1].Location__c = loc.Id;
        Database.insert(accList);
        System.assertEquals(accList[0].Location__c,loc.Id);
        
        accList = [SELECT Id, IsPersonAccount FROM Account WHERE Name LIKE '%TestLastName%'];
        System.assertEquals(accList[0].IsPersonAccount,true);
        System.assertEquals(accList.size(),2);

        //Create Test Case
        List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_P_CONS_AF, 1);
        caseList[0].Partition__c = 'Positive';
        caseList[0].Lead_Source__c = 'LoansForPeopleWithBadCredit - Website';
        caseList[0].PLS_Initial_Team_Queue__c = 'LFP Flow';
        caseList[0].Lead_Bucket__c = 'LFP Flow';
        caseList[0].Lead_Creation_Method__c = 'Frontend Load';
        caseList[0].Channel__c = 'LFPWBC';
        caseList[0].Stage__c = 'WOPA';
        caseList[0].Lead_Loan_Amount__c = 5000;
        caseList[0].Lead_Purpose__c = 'Car Loan';
        caseList[0].From_Lead_Path__c = false;
        caseList[0].OwnerId = [SELECT Id FROM Group WHERE Name = 'Consumer Asset' LIMIT 1].Id;
        // caseList[0].Primary_Contact_Location_MC__c = loc.Id;
        Database.insert(caseList);

        List<Frontend_Submission__c> feList = TestDataFactory.creatFESubs(1, caseList[0].Id, 'Quick Quote', '6641C95F-69CE-4E4C-B48D-BAD1E53042D5', 'LFPWBC', 'Bad Credit Loans - Banks Say No? We Say Yes');
        Database.insert(feList);
        caseList = [SELECT Id ,Application_Name__c FROM Case WHERE Id IN: caseList];
        System.assertNotEquals(caseList[0].Application_Name__c,null);

        List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, caseList[0].Application_Name__c,  1);
        roleList[0].Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT;
        roleList[0].Phone__c = '09165856439';
        roleList[0].Mobile_Phone__c = '09165856439';
        roleList[0].Email__c = 'test@email.com';
        roleList[0].Primary_Contact_for_Application__c = true;
        Database.insert(roleList);

        roleList = [SELECT Id,Address__c,Account__c,Account__r.Address__c FROM Role__c WHERE Application__c =: caseList[0].Application_Name__c];

        System.assertEquals(roleList.size(),1);
        System.assertEquals(roleList[0].Address__c,NULL);
        System.assertEquals(roleList[0].Account__r.Address__c,NULL);
        System.assertNotEquals(roleList[0].Account__c,NULL);

        Income1__c paygIncome = TestDataFactory.createGenericIncome('PAYG Employment', caseList[0].Application_Name__c, 0);
        paygIncome.Income_Type__c = 'PAYG Employment';                                    // Income Type
        paygIncome.Income_Situation__c = 'Current Income';                                  // Income Situation
        paygIncome.Job_Title__c = 'Consultant';                                             // Job Title
        paygIncome.Role__c = roleList[0].Id;                                             // Role
        paygIncome.Equal_Share_for_Roles__c = true;                                         // Equal Share for Roles
        paygIncome.h_Auto_Create_FO_Share__c = true;                                        // Auto-Create FO Share
        paygIncome.Employer_Name__c = 'Bob Jane TMart';                                     // Employer Name
        paygIncome.Employment_Contact_Person__c = 'Reception';                              // Employment Contact Person
        paygIncome.ABN_manual__c = '2555555555555555555';                                   // ABN (manual)
        paygIncome.Contact_Person_Direct_Mobile_Phone__c = '(08) 8206 4467(08) 8206 4467';  // Employment Contact Number
        paygIncome.Employment_Job_Status__c = 'Full-Time';                                  // Employment Job Status
        paygIncome.Start_Date__c = Date.valueOf('2016-03-15 00:00:00');                               // Start Date
        paygIncome.Employment_On_Probation__c = 'No';                                       // Employment On Probation
        paygIncome.Income_Period__c = 'Fortnightly';                                        // Income Period
        paygIncome.Net_Standard_Pay__c = 1500.00;                                           // Net Standard Pay
        paygIncome.Overtime_Pay_monthly_net__c = 0.00;                                      // Overtime Pay (monthly net)
        paygIncome.Bonus_monthly_net__c = 0.00;                                             // Bonus (monthly net)
        paygIncome.Commission_monthly_net__c = 0.00;                                        // Commission (monthly net)
        paygIncome.Work_Allowance_net_monthly__c = 0.00;                                    // Work Allowance (net monthly)

        Database.insert(paygIncome);

        //Create test data for Address
        Address__c address = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '1', 'Kensington', false);
        address.Location__c = loc.Id;
        address.Street_Type__c = 'Street';
        Database.insert(address);
        Address__c address2 = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '1/2', 'Kensington', false);
        address2.Location__c = loc.Id;
        Database.insert(address2);

        //Create test data for Role Addresses
        Role_Address__c roleAddress = TestDataFactory.createGenericRoleAddress(roleList[0].Account__c, true, roleList[0].Id, address.Id);
        Role_Address__c roleAddressTwo = TestDataFactory.createGenericRoleAddress(roleList[0].Account__c, false, roleList[0].Id, address.Id);
        List<Role_Address__c> roleAddressList = new List<Role_Address__c>();
        roleAddressList.add(roleAddress);
        roleAddressList.add(roleAddressTwo);
        Database.insert(roleAddressList);
    }    

    static testmethod void equifaxAPITransferTestOne (){
        List<Case> caseList = [SELECT Id,Application_Name__c FROM Case ];
        System.assertEquals(1, caseList.size()); 
        List<Role__c> roleList = [SELECT Id FROM Role__c ];
        System.assertEquals(1, roleList.size()); 
        List<Role_Address__c> roleAddrList = [SELECT Id FROM Role_Address__c ];
        System.assertEquals(2, roleAddrList.size()); 

        EquifaxAPITransferController.wrapperClass wrapperClass = EquifaxAPITransferController.initMethod(caseList[0].Application_Name__c);
        System.debug(wrapperClass);
        
        API_Transfer__c apiTransfer = EquifaxAPITransferController.buildFormData(roleList[0].Id);
        apiTransfer.EQ_Role__c = roleList[0].Id;
        EquifaxAPITransferController.saveAPITransfer(apiTransfer);
        List<Income1__c> employmentIncomeList = new List<Income1__c>();
        Income1__c incomePayg = TestDataFactory.createGenericIncome('PAYG Employment', caseList[0].Application_Name__c, 0);
        incomePayg.Income_Type__c = 'PAYG Employment';
        incomePayg.Income_Situation__c = 'Current Income';
        Income1__c incomeSelf = TestDataFactory.createGenericIncome('PAYG Employment', caseList[0].Application_Name__c, 0);
        incomeSelf.Income_Type__c = 'Self-Employed';
        incomeSelf.Income_Situation__c = 'Current Income';
        employmentIncomeList.add(incomePayg);
        employmentIncomeList.add(incomeSelf);
        EquifaxAPITransferController.setEmploymentFields(apitransfer, employmentIncomeList);
        List<API_Transfer__c> apiTransferToUpdatelist = [SELECT Id,EQ_Role__c FROM API_Transfer__c];
        System.assertEquals(1, apiTransferToUpdatelist.size()); 
        try{
           EquifaxAPITransferController.saveAPITransfer(apiTransferToUpdatelist[0]); 
        }
        catch (Exception e) {
            System.assertEquals('Script-thrown exception', e.getMessage());    
        }
    }
}
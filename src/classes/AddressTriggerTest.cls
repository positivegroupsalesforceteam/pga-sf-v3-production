/** 
* @FileName: AddressTriggerTest
* @Description: Test class for Address Trigger 
* @Copyright: Positive (c) 2018 
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 18/06/18 RDAVID Created class
**/ 
@isTest 
public class AddressTriggerTest{
	public static List<Location__c> locList = new List<Location__c>(); 
	@testSetup static void setupData() {

		//Turn On Trigger 
		Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
		triggerSettings.Enable_Triggers__c = true;
		triggerSettings.Enable_Address_Trigger__c = true;
        triggerSettings.Enable_Address_Populate_Location__c = true;
		upsert triggerSettings Trigger_Settings1__c.Id;
		
		List<Location__c> locListUNK = TestDataFactory.createLocation(1,'UNKNOWN','','','','','');
		List<Location__c> locListOVR = TestDataFactory.createLocation(1,'OVERSEAS','','','','','');
		locList = TestDataFactory.createLocation(1,'2000-BARANGAROO','2000','BARANGAROO','NSW','New South Wales','Australia');
		List<Location__c> locListTwo = TestDataFactory.createLocation(1,'2000-DAWES POINT','2000','DAWES POINT','NSW','New South Wales','Australia');
		locListTwo.addAll(locList);
		locListTwo.addAll(locListUNK);
		locListTwo.addAll(locListOVR);
		Database.insert(locListTwo);
	}
	//SD-31 SF - NM - Role Address Trigger to populate Address lookups - Trigger in Role_Address__c object to populate Parent Account and Parent Role Address__c field.
	static testMethod void testPopulateAddressLookups(){

		//Create Test Location__c record
		locList = [SELECT Id FROM Location__c LIMIT 3];
		System.assertEquals(3,locList.size());

		Test.StartTest();
			//Create Address 
			Address__c testAddress = new Address__c(Location__c = locList[0].Id);
			Database.insert(testAddress);
			testAddress = [SELECT Id, Postcode__c,Suburb__c,State_Acr__c FROM Address__c];
			System.assertEquals('DAWES POINT',testAddress.Suburb__c);
			testAddress.Location__c = locList[1].Id;
			TriggerFactory.curSoType = null;
			Database.update(testAddress);
			testAddress = [SELECT Id, Postcode__c,Suburb__c,State_Acr__c FROM Address__c];
			System.assertNotEquals('DAWES POINT',testAddress.Suburb__c);
			Database.delete(testAddress);

			TriggerFactory.curSoType = null;
			TriggerFactory.isProcessedMap = new map <Id, boolean>();
			Address__c testAddress2 = new Address__c();
			testAddress2.Postcode__c = '12345';
			testAddress2.Suburb__c = 'test';
			insert testAddress2;

			//Verify Location was set to UNKNOWN on Insert
			testAddress2 = [Select Id, Location__r.Name, Postcode__c, Suburb__c From Address__c];
			System.assertEquals('UNKNOWN', testAddress2.Location__r.Name);

			TriggerFactory.curSoType = null;
			TriggerFactory.isProcessedMap = new map <Id, boolean>();
			testAddress2.Suburb__c = 'test2';
			update testAddress2;

			//Verify Location was set to UNKNOWN on Update
			testAddress2 = [Select Id, Location__r.Name, Postcode__c, Suburb__c From Address__c];
			System.assertEquals('UNKNOWN', testAddress2.Location__r.Name);

			TriggerFactory.curSoType = null;
			TriggerFactory.isProcessedMap = new map <Id, boolean>();
			testAddress2.Location__c = null;
			testAddress2.Postcode__c = '2000';
			testAddress2.Suburb__c = 'BARANGAROO';
			update testAddress2;

			TriggerFactory.curSoType = null;
			TriggerFactory.isProcessedMap = new map <Id, boolean>();
			testAddress2.Location__c = null;
			testAddress2.Postcode__c = '01256';
			testAddress2.State__c = 'Outside Australia';
			testAddress2.Suburb__c = 'test';
			update testAddress2;


			//System.assertEquals('testMethod',AddressGateway.sampleMethod());
			
		Test.StopTest();
	}	

}
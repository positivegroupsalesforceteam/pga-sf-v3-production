/** 
* @FileName: BizibleTouchpointQueueableForCSTPIns
* @Description: Class implements Queueable Apex 
* @Copyright: Positivve (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.1 22/10 RDAVID extra/task26493146-BugCaseTP - Fix Issue on creating Case TPs, create a separate Queueable Apex to chain this log
**/ 

public class BizibleTouchpointQueueableForCSTPIns implements Queueable {
    public Map<String,Case_Touch_point__c> ctpToInsMap = new Map<String,Case_Touch_point__c>();
    public List<Case_Touch_point__c> ctpToInsList;
    @testVisible private static Boolean forceFail = false;

    public BizibleTouchpointQueueableForCSTPIns (List<Case_Touch_point__c> ctpToInsertList){
        this.ctpToInsList = ctpToInsertList;
    }

    public void execute(QueueableContext context) {
        // System.debug('BizibleTouchpointQueueableForCSTPIns execute ctpToInsList.size() = '+ctpToInsList.size());
        if(ctpToInsList.size() > 0){
            ctpToInsMap = getCTPToInsMap(ctpToInsList);
            try{
                list <Case> lockCaseForUpdate = [Select Id From Case Where Id in : getCaseId(ctpToInsList) FOR UPDATE];
                Database.SaveResult[] results = Database.insert(ctpToInsMap.values(),false); //Insert CSTP DML Allow Partial 
                String processName = 'Insert Case Touchpoints Records';
                if (results != null){
                    Boolean sendCustomErrorHandlingEmailNotif = false;
                    Set<String> errLogs = new Set<String>();
                    Integer errornum = 0;
                    for (Database.SaveResult result : results) {
                        if (!result.isSuccess()) {
                            sendCustomErrorHandlingEmailNotif = true;
                            Database.Error[] errs = result.getErrors();
                            Id recordID = result.getId();
                            for(Database.Error err : errs) {
                                // System.debug(err.getStatusCode() + ' - ' + err.getMessage());
                                errornum ++;
                                errLogs.add('Error #'+errornum+'<br/>'+'Record Id: '+recordID +'<br/> Status Code: ' + err.getStatusCode() + '<br/> Fields: ' + String.join(err.getFields(),',') + '<br/> Error Message: ' + err.getMessage());
                            }
                        }
                    }
                    if(sendCustomErrorHandlingEmailNotif){
                        CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                        CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Error',BizibleTouchpointQueueableForCSTPIns.class.getName() + ' - ' + processName,errLogs);
                        cerhand.logWrap = logWrapper;
                        cerhand.sendMail();
                    }
                }
            } catch (Exception e) {
                System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            }
        }
    }
    public Set<Id> getCaseId (List<Case_Touch_point__c> ctpList){
        Set<Id> csId = new Set<Id>();
        for(Case_Touch_point__c ctp : ctpList){
            csId.add(ctp.Case__c);
        }
        return csId;
    }
    public Map<String,Case_Touch_point__c> getCTPToInsMap (List<Case_Touch_point__c> ctpList){
        Map<String,Case_Touch_point__c> ctpToInsMapInstance = new Map<String,Case_Touch_point__c>();
        for(Case_Touch_point__c ctp : ctpList){
            String key = ctp.Case__c+'-'+ctp.Bizible_Touchpoint__c;
            if(ctp.Case__c != NULL && ctp.Bizible_Touchpoint__c != NULL && !ctpToInsMapInstance.containsKey(key)){
                if(forceFail){
                    ctp.Case__c = null; 
                } 
                ctpToInsMapInstance.put(ctp.Case__c+'-'+ctp.Bizible_Touchpoint__c,ctp);
            }
            
        }
        return ctpToInsMapInstance;
    }
}
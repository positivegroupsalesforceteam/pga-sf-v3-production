/**
 * @File Name          : CaseClosedLostCallbackBatchTest.cls
 * @Description        : 
 * @Author             : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Group              : 
 * @Last Modified By   : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Last Modified On   : 11/06/2019, 2:41:22 pm
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/06/2019, 2:23:17 pm   Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Initial Version
**/
@isTest
private class CaseClosedLostCallbackBatchTest {

    @testsetup static void setup(){

        Id rtPOSAssetFinance = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POS: Consumer - Asset Finance').getRecordTypeId();
        Case cse = new Case(
            Status = 'Closed',
            Stage__c = 'Lost',
            Partition__c = 'Positive',
            Channel__c = 'PLS',
            RecordTypeId = rtPOSAssetFinance,
            Call_Back_Set__c = System.now()
        );
        insert cse;

    }

    static testmethod void testCaseClosedLostCallbackBatch(){

        //Retrieve test Case
        Case cse = [Select Id, CaseNumber, Stage__c From Case limit 1];
        
        Test.startTest();

            CaseClosedLostCallbackBatch cclcb = new CaseClosedLostCallbackBatch();
            database.executeBatch(cclcb);

        Test.stopTest();

        //Verify new Case got created
        //list <Case> cselist = [Select Id From Case];
        //system.assertEquals(cselist.size(), 2); 


    }

    static testmethod void testCaseClosedLostCallbackSchedule(){

        //Retrieve test Case
        Case cse = [Select Id, CaseNumber, Stage__c From Case limit 1];

		Test.startTest();

			CaseClosedLostCallbackBatch_Schedule cclcbs = new CaseClosedLostCallbackBatch_Schedule();
			String sch = '0 0 23 * * ?'; 
			system.schedule('TEST-POS Case Closed Lost Callback', sch, cclcbs);

		Test.stopTest();



	}

}
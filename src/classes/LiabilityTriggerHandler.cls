/*
	@Description: handler class of LiabilityTrigger
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	9/11/2017 - Created, Transferred existing logic for Rollup Liabiltiies
*/
public class LiabilityTriggerHandler {

	private static final string ACC_PRIVATE_RT = Label.Account_Private_RT; //Private
	private static final string LIAB_TYP_CC = Label.Liability_Type_Credit_Card; //Credit Card
	private static final string LIAB_TYP_MAINT = Label.Liability_Type_Maintenance; //Maintenance
	private static final string LIAB_TYP_MORT_LOAN = Label.Liability_Type_Mortgage_Loan; //Mortgage Loan
	private static final string LIAB_TYP_ORB = Label.Liability_Type_Ongoing_Rent_Board; //Ongoing Rent/Board
	private static final string LIAB_TYP_OTHER = Label.Liability_Type_Other; //Other
	private static final string LIAB_TYP_OUT_TAXATION = Label.Liability_Type_Outstanding_Taxation; //Outstanding Taxation
	private static final string LIAB_TYP_OVERDRAFT = Label.Liability_Type_Overdraft; //Overdraft
	private static final string LIAB_TYP_SLOAN = Label.Liability_Type_Secured_Loan; //Secured Loan
	private static final string LIAB_TYP_SC = Label.Liability_Type_Store_Card; //Store Card
	private static final string LIAB_TYP_ULOAN = Label.Liability_Type_Unsecured_Loan; //Unsecured Loan

	static Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());

	//execute all After Update event triggers
	public static void onAfterInsert(map<Id,Liability__c> oldLiabilityMap, map <Id,Liability__c> newLiabilityMap){
		if(trigSet.Enable_Liability_rollupLiabilities__c) rollupLiabilities(oldLiabilityMap,newLiabilityMap,false);
	}

	//execute all After Update event triggers
	public static void onAfterUpdate(map<Id,Liability__c> oldLiabilityMap, map <Id,Liability__c> newLiabilityMap){
		if(trigSet.Enable_Liability_rollupLiabilities__c) rollupLiabilities(oldLiabilityMap,newLiabilityMap,false);
	}

	//execute all After Delete event triggers
	public static void onAfterDelete(map<Id,Liability__c> oldLiabilityMap){
		if(trigSet.Enable_Liability_rollupLiabilities__c) rollupLiabilities(oldLiabilityMap,null,true);
	}

	/* @Description: method for rolling up Liabilities on Account/Contact
	   @History:     5/18/2017 (Raoul Lake) - Applies to Connective rt only (as at May 2017). 
	   										- In future if extended beyond Connective, check for appropriate account record type matches in rollups.
	   										- On Delete - recalculates the rollup.
				  	 9/11/2017 (Jesfer Baculod - Positive Group) - Moved logic to new LiabilityTriggerHandler class
																 - Fixed null check for Monthly Payments
	*/    
	private static void rollupLiabilities(map<Id,Liability__c> oldLiabilityMap, map <Id,Liability__c> newLiabilityMap, boolean onDelete){
		//NOT BULKIFIED - Liability is pushed into SF individually, Lightning will push contact first, then push the liabilities of the contact one by one. 
		//Type1__c - Connective Liability Type field

		Set<Id> AccountRecIds = new set<ID>();
		Liability__c liability;
		if (!onDelete){ //onDelete event
			for (Liability__c liab : newLiabilityMap.values()){
				liability = liab;
				break;
			}
		}
		else { //onInsert | onUpdate event
			for (Liability__c liab : oldLiabilityMap.values()){
				liability = liab;
				break;
			}	
		}
		system.debug('Liability_Rollup: post-ifs, Liability Name is: '+Liability.Name);
		//ACCOUNT Liability:
		if(liability.Account__c != null && liability.Key_Person_Name__c == null){
			for (RecordType rec: [Select Id, Name From RecordType Where SobjectType = 'Account' AND Name != : ACC_PRIVATE_RT]){
				AccountRecIds.add(rec.Id);
			}
			system.debug('Liability Rollup, in Account if statement');
			Id AccountRecTypeId = [select id,recordtypeId from Account where id=: Liability.Account__c].RecordTypeId;
			if (AccountRecIds.contains(AccountRecTypeId)){
				Account acc = new Account(Id = Liability.Account__c);
                decimal rent = 0.0;
                decimal creditcard = 0.0;
                decimal loan = 0.0;  
                decimal other = 0.0;

                for(Liability__c liab : [select id,Type1__c,Monthly_Payment__c from Liability__c where Account__c =: Liability.Account__c AND Clear_from_this_account__c != 'Yes']){
                    if(liab.Type1__c == LIAB_TYP_MORT_LOAN || liab.Type1__c == LIAB_TYP_ORB){ //Mortgage Loan or Ongoing Rent/Board
                    	if (liab.Monthly_Payment__c != null){
                        	rent += liab.Monthly_Payment__c;
                       	}
                    }
                    else  if(liab.Type1__c == LIAB_TYP_CC || liab.Type1__c == LIAB_TYP_SC){ //Credit Card or Store Card
                        if (liab.Monthly_Payment__c != null){
                        	creditcard += liab.Monthly_Payment__c; 
                    	}
                    }
                    else  if(liab.Type1__c == LIAB_TYP_SLOAN || liab.Type1__c == LIAB_TYP_ULOAN || liab.Type1__c == LIAB_TYP_OVERDRAFT){ //Secured Loan, Unsecured Loan, Overdraft
                        if (liab.Monthly_Payment__c != null){
                        	loan  += liab.Monthly_Payment__c;
                    	}
                    }
                    else if(liab.Type1__c == LIAB_TYP_OUT_TAXATION || liab.Type1__c == LIAB_TYP_MAINT || liab.Type1__c == LIAB_TYP_OTHER){ //Outstanding Taxation, Maintenance, Other
                        if (liab.Monthly_Payment__c != null){
                        	other += liab.Monthly_Payment__c;
                    	}
                    }
                }
                acc.Combined_Rent_Mortgage_Board__c = rent;
                acc.Combined_Credit_Card_Payments__c = creditcard;
                acc.Combined_Loan_Payments__c = loan;
                acc.Combined_Other_Liabilities__c = other;
                update acc;
			}
		}
		//CONTACT Liability:
		else if(liability.Account__c == null && liability.Key_Person_Name__c != null){
            system.debug('Liability Rollup, arrived Contact else if');
            Contact cont = new Contact(Id = liability.Key_Person_Name__c);
            decimal rent = 0.0;
            decimal creditcard = 0.0;
            decimal loan = 0.0;  
            decimal other = 0.0;
            for(Liability__c liab : [select id,Type1__c,Monthly_Payment__c from Liability__c where Key_Person_Name__c =: liability.Key_Person_Name__c AND Clear_from_this_account__c != 'Yes']){
                system.debug('Liability Rollup, in Contact For loop - liability type: '+liab.Type1__c+', monthly payment: '+liab.Monthly_Payment__c);
                
                if(liab.Type1__c == LIAB_TYP_MORT_LOAN || liab.Type1__c == LIAB_TYP_ORB){ //Mortgage Loan or Ongoing Rent/Board
                    system.debug('Liability Rollup, Contact, If Mortgage or Rent, monthly payment is: '+liab.Monthly_Payment__c+', Rent pre-assignment is: '+rent);
                    if (liab.Monthly_Payment__c != null){
                    	rent += liab.Monthly_Payment__c;
                	}
                    system.debug('Liability Rollup, Contact, Mortgage/Rent, post assignmemt Rent is: '+rent);
                }
                else  if(liab.Type1__c == LIAB_TYP_CC || liab.Type1__c == LIAB_TYP_SC){ //Credit Card or Store Card
                    system.debug('Liability Rollup, Contact, If Credit or Store Card, monthly payment is: '+liab.Monthly_Payment__c+', Creditcard pre-assignment is: '+creditcard);
                    if (liab.Monthly_Payment__c != null){
                    	creditcard += liab.Monthly_Payment__c; 
                   	}
                    system.debug('Liability Rollup, Contact, Credit/Store card, post assignmemt creditcard is: '+creditcard);
                }    
                else  if(liab.Type1__c == LIAB_TYP_SLOAN || liab.Type1__c == LIAB_TYP_ULOAN || liab.Type1__c == LIAB_TYP_OVERDRAFT){ //Secured Loan, Unsecured Loan, Overdraft
                    system.debug('Liability Rollup, Contact, If Secured or Unsecured, monthly payment is: '+liab.Monthly_Payment__c+', Loan pre-assignment is: '+loan);
                    if (liab.Monthly_Payment__c != null){
                    	loan  += liab.Monthly_Payment__c;
                	}
                    system.debug('Liability Rollup, Contact, Secured/Unsecured Loan, post assignmemt Loan is: '+loan);
                }
                else if(liab.Type1__c == LIAB_TYP_OUT_TAXATION || liab.Type1__c == LIAB_TYP_MAINT || liab.Type1__c == LIAB_TYP_OTHER){ //Outstanding Taxation, Maintenance, Other
                    system.debug('Liability Rollup, Contact, If Taxation, Maintenance/Other, monthly payment is: '+liab.Monthly_Payment__c+', Other pre-assignment is: '+other);
                    if (liab.Monthly_Payment__c != null){
                    	other += liab.Monthly_Payment__c;
                	}
                    system.debug('Liability Rollup, Contact, Tax/Maint/Other, post assignmemt Other is: '+other);
                } 
            }
            cont.Combined_Rent_Mortgage_Board__c = rent;
            system.debug('Liability Rollup checksum: Combined_Rent_Mortgage_Board__c is '+cont.Combined_Rent_Mortgage_Board__c+', rent is: '+rent);
            cont.Combined_Credit_Card_Payments__c = creditcard;
            system.debug('Liability Rollup checksum: Combined_Credit_Card_Payments__c is '+cont.Combined_Credit_Card_Payments__c+', creditcard is: '+creditcard);
            cont.Combined_Loan_Payments__c = loan;     
            system.debug('Liability Rollup checksum: Combined_Loan_Payments__c is '+cont.Combined_Loan_Payments__c+', loan is: '+loan);
            cont.Combined_Other_Liabilities__c = other;
            system.debug('Liability Rollup checksum: Combined_Other_Liabilities__c is '+cont.Combined_Other_Liabilities__c+', other is: '+other);
            update cont;
            
        }
        else if(Liability.Account__c != null && Liability.Key_Person_Name__c != null){
            system.debug('Liability Rollup, no action as Account & Contact both not null');
            // Then Do not rollup 
        } 
	}

}
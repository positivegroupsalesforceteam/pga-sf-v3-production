/** 
* @FileName: CaseCommentHandler
* @Description: Trigger Handler for the Support_Request__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/1/19 RDAVID - extra/CaseCommentTrigger-NewCaseCommentNotification - Created Class for Notifying Case Owner when a new Case Comment was inserted in SF by Api Admin
* 1.1 11/1/19 JBACULOD - Transferred Updating of Latest Comment of Case from PB to this trigger, also updates Latest Comment (FULL) 
* 1.2 11/16/19 JBACULOD - Fixed issue due to simultaneous saving of Case Comment on Support Request creation
* 1.3 14/05/19 RDAVID TeamworkId: tasks/24385208 Branch:extra/tasks24385208-RollupChildCaseCommentToParentCase - Added logic to create Case Comment on Active Parent Case when a case is created from Child Case
* 1.4 28/06/2019 RDAVID Branch:extra/task25072606-BugFixNVMErrorLogs - Added fix to UNABLE_TO_LOCK_ROW occuring in NVM (Task insert)
*/
public without sharing class CaseCommentHandler implements ITrigger {
    private List<CaseComment> newCaseCommentList = new List<CaseComment>();
	private Map<Id,Id> caseCcIdMap = new Map<Id,Id>();
	private Map<Id,Case> caseMap = new Map<Id,Case>();
	//private list <Case> forCaseUpdate = new list <Case>();
	private map <Id, Case> forCaseUpdateMap = new map <Id, Case>();
	
	public Map<Id,Id> parentCaseMap = new Map<Id,Id>(); //1.3 14/05/19 RDAVID TeamworkId: tasks/24385208 - contains Open Positive Parent Cases
	private List<CaseComment> caseCommentForParent = new List<CaseComment>();
	//private User apiUser = new User();

	public CaseCommentHandler (){
		//Initialize value of newSupporRequestList 
		newCaseCommentList = (Trigger.IsInsert || Trigger.isUpdate) ? (List<CaseComment>) Trigger.new : (Trigger.IsDelete) ? (List<CaseComment>) Trigger.old : null;
		//if(apiUser.Id == null) apiuser = [select Name,Id from User where Name = 'Api Admin'];
		System.debug('newCaseCommentList - '+newCaseCommentList);
	}
    /** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
		
	}

	
	public void bulkAfter(){
		//Set<Id> caseIds = new Set<Id>();
		if(Trigger.IsInsert){
			for(CaseComment cc : newCaseCommentList){
				caseCcIdMap.put(cc.Id, cc.ParentId); //JBACU 1/16/19 switched key and value
			}
		}

		if(!caseCcIdMap.isEmpty()){
			caseMap = CaseCommentGateway.getCaseMap(caseCcIdMap,parentCaseMap);
			System.debug('@@44caseMap = '+caseMap);
			System.debug('@@parentCaseMap = '+parentCaseMap);
		}
	}
		
	public void beforeInsert(SObject so){
		
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}

	public void afterInsert(SObject so){
		CaseComment cc = (CaseComment)so;
		Case parentCase = caseMap.get(cc.Id);

		if (TriggerFactory.trigSet.Enable_Case_Comment_Update_Latest_Comm__c){
			parentCase = CaseCommentGateway.updateLatestCommentonCase(cc,parentCase);
			forCaseUpdateMap.put(parentCase.Id, parentCase); //Allows overwriting of Case w/ latest updates
		}
		if (TriggerFactory.trigSet.Enable_Case_Comment_Create_For_Parent__c){ // 1.3 14/05/19 RDAVID TeamworkId: tasks/24385208
			if(parentCaseMap.containsKey(cc.ParentId)){
				CaseComment newCCForParent = cc.clone(false, false, false, false);
				newCCForParent.CommentBody = 'Child Case Comment: ' + newCCForParent.CommentBody;
				newCCForParent.IsPublished = cc.IsPublished;
				newCCForParent.ParentId = parentCaseMap.get(cc.ParentId);
				caseCommentForParent.add(newCCForParent);
			}
		}
		/*if(TriggerFactory.trigset.Enable_Case_Comment_New_Email_Alert__c){
			if (apiuser.Id == cc.CreatedById){ //|| userInfo.getFirstName() == 'Rexie'){
				CaseCommentGateway.sendEmailToCaseOwner(cc,caseMap);
			}
			/*else if (cc.IsPublished && cc.CreatedById != apiuser.Id){
				if (parentCase.Partition__c == 'Nodifi'){
					CaseCommentGateway.sendEmailToIntroducer(cc,parentCase);
				}
			} */
        //} 
	}
	
	public void afterUpdate(SObject oldSo, SObject so){
	
	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		if (forCaseUpdateMap.size() >0){
			try{
				// 1.4 28/06/2019 RDAVID Branch:extra/task25072606-BugFixNVMErrorLogs
				List <Case> recordLockForUpdateCase = [Select Id From Case Where Id in : forCaseUpdateMap.keySet() FOR UPDATE];
				Database.update(forCaseUpdateMap.values());
			}
			catch(Exception e){
				Database.update(forCaseUpdateMap.values()); // Retry due to Unable to Lock Row issue
			}
		} 
		if (caseCommentForParent.size() >0) Database.insert(caseCommentForParent);
	}	
}
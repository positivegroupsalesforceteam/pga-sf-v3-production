/** 
* @FileName: SectorHandler
* @Description: Handler Class of Sector__c Object Trigger 
* @Copyright: Positive (c) 2019 
* @author: Rexie Aaron David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 10/04/19 RDAVID-task24148707 Created Class
* 1.1 3/05/19 RDAVID-task24399056 : Populate the Email_Warning_Recipient__c and Email_Warning_Recipient_POC__c of the Sector record
* 1.2 9/05/19 RDAVID-task24399056 : Added Logic to apply Different time threshold for Consumer and Commercial Initial Review
* 1.3 7/03/19 JBACULOD-task25176034 : Added populating of Time in Sector - Business Hours (HH) field
* 1.4 16/09/19 RDAVID-extra/task26058186-UpdateOnSLARules : Fix on Rules when Sector Exit and SLA Due Date match but differs in seconds

For Consumer:
* It should be the same as App Review
**/ 
public without sharing class SectorHandler implements ITrigger{
    
	public static BusinessHours busiHours = new BusinessHours();
	public static List<SLA_Sector__mdt> slaSectorMDTList = new List<SLA_Sector__mdt>();
	public static Map<String,SLA_Sector__mdt> slaSectorMDTMap = new Map<String,SLA_Sector__mdt>();
	public static Map<String,Schema.RecordTypeInfo> sectorRTMapByName = Schema.SObjectType.Sector__c.getRecordTypeInfosByName(); 
	public static Map<Id,Schema.RecordTypeInfo> sectorRTMapById = Schema.SObjectType.Sector__c.getRecordTypeInfosById(); 
	public static Integer convertToMinToMS = 60000; //Formula: Milliseconds = [NumOfMins] * 60000
	private List<Sector__c> newSectorList = new List<Sector__c>();
	private Map<Id,SObject> caseToUpdateList = new Map<Id,SObject>();
	public static Map<String,String> slaTypeUserStringMap = new Map<String,String>();
	public static Map<String,Id> slaTypeUserMap = new Map<String,Id>();
	public Map<Sector__c,Case> secCaseMap = new Map<Sector__c,Case>();
	public Map<Sector__c,Support_Request__c> secSRMap = new Map<Sector__c,Support_Request__c>();
	public static Map<Id,Case> caseMap = new Map<Id,Case>();
	public static Map<Id,Support_Request__c> srMap = new Map<Id,Support_Request__c>();
	public static Map<Id,SObject> sectorSObjectMap = new Map<Id,SObject>();
    
	public SectorHandler (){
        if(busiHours == new BusinessHours())
            busiHours = [   SELECT Id,IsDefault,Name,IsActive,TimeZoneSidKey,SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime, WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime 
                            FROM BusinessHours 
                            WHERE IsActive = TRUE AND IsDefault = TRUE];
        
		if(slaSectorMDTMap.isEmpty()){
			for(SLA_Sector__mdt slaMeta : Database.query(SectorGateway.getSLASectorMetadataQueryString())){
				slaSectorMDTMap.put(slaMeta.Sector_Name__c,slaMeta);
				slaTypeUserStringMap.put(slaMeta.Sector_Name__c,slaMeta.Email_Warning_Recipient_POC__c);
			}
		}
		
		//Initialize value of newSectorList
		newSectorList = (Trigger.IsInsert || Trigger.isUpdate) ? (List<Sector__c>) Trigger.new : (Trigger.IsDelete) ? (List<Sector__c>) Trigger.old : null;
		// System.debug('newSectorList - '+newSectorList);
	}
    /** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
		
		// SectorGateway.setParentOwnerFields(newSectorList,sectorRTMapById,slaSectorMDTMap);//Populate the Alert_Recipient_Name__c and Alert_Recipient_Email__c
		if(slaTypeUserMap.isEmpty()) slaTypeUserMap = SectorGateway.getSlaTypeUserMap(slaTypeUserStringMap);
		Set<Id> caseIds = new Set<Id>();
		Set<Id> srIds = new Set<Id>();
		// Map<Id,Case> caseMap = new Map<Id,Case>();
		// Map<Id,Support_Request__c> srMap = new Map<Id,Support_Request__c>();

		for(Sector__c sector : newSectorList){
            if(sector.Case_Number__c != NULL) caseIds.add(sector.Case_Number__c);
			if(sector.SR_Number__c != NULL) srIds.add(sector.SR_Number__c);
			sectorSObjectMap.put(sector.Id,sector);
        }
		
		if(caseIds.size() > 0)
			caseMap = new Map<Id,Case>([SELECT Id, Submitter__c, Settlement_Officer__c, OwnerId FROM Case WHERE Id IN:caseIds]);
		
		if(srIds.size() > 0)
			srMap = new Map<Id,Support_Request__c>([SELECT Id, Status__c, OwnerId, Application_Type__c,Case_Owner__c,Case_Number__c,Case_Number__r.RecordTypeId,Case_Number__r.RecordType.Name FROM Support_Request__c WHERE Id IN:srIds]);
		

		// for(Sector__c sector : newSectorList){
        //     if(!caseMap.isEmpty() && caseMap.containsKey(sector.Case_Number__c)) 
		// 		secCaseMap.put(sector,caseMap.get(sector.Case_Number__c));
		// 	if(!srMap.isEmpty() && srMap.containsKey(sector.SR_Number__c)) 
		// 		secSRMap.put(sector,srMap.get(sector.SR_Number__c));
        // }
		System.debug('task24399056 caseMap -- '+caseMap); 
		System.debug('task24399056 srMap -- '+srMap); 
		// System.debug('task24399056 secCaseMap -- '+secCaseMap); 
		// System.debug('task24399056 secSRMap -- '+secSRMap);
		// Case - Submitter__c, Settlement_Officer__c, OwnerId
		// SR - Status__c,OwnerId,Application_Type__c,Case_Owner__c

	}

	public void bulkAfter(){
		
	}
		
	public void beforeInsert(SObject so){
		Sector__c sector = (Sector__c)so;

		SectorGateway.setSectorRecordtype(sector,slaSectorMDTMap,sectorRTMapByName);//Populate RT of Sector record
		
		System.debug('task24399056 beforeInsert secCaseMap.get(sector) -- '+ sector.Sector1__c + ' --------> ' + caseMap.get(sector.Case_Number__c));
		System.debug('task24399056 beforeInsert secSRMap.get(sector) -- '+ sector.Sector1__c + ' --------> ' + srMap.get(sector.SR_Number__c));
		
		// 3/05/19 RDAVID-task24399056
		if(caseMap.get(sector.Case_Number__c) != NULL && srMap.get(sector.SR_Number__c) != NULL) //FOR SR
			SectorGateway.setRecipients(so, caseMap.get(sector.Case_Number__c), srMap.get(sector.SR_Number__c), slaSectorMDTMap, slaTypeUserMap); // 3/05/19 RDAVID-task24399056
		else if(caseMap.get(sector.Case_Number__c) != NULL && srMap.get(sector.SR_Number__c) == NULL) //FOR Case
			SectorGateway.setRecipients(so, caseMap.get(sector.Case_Number__c), null, slaSectorMDTMap, slaTypeUserMap);

		if (sector.Sector_Entry__c != NULL && sector.Sector1__c.containsIgnoreCase('SLA') && busiHours != NULL) {
			Integer slaInMilliseconds = Integer.valueOf(slaSectorMDTMap.get(sector.Sector1__c).SLA_Target_Time_MM__c) * convertToMinToMS;
			Integer slaInitialWarningInMilliseconds = Integer.valueOf(slaSectorMDTMap.get(sector.Sector1__c).SLA_Initial_Warning_Time_MM__c) * convertToMinToMS;
			
			sector.SLA_Due_Date__c = BusinessHours.addGmt (busiHours.id, sector.Sector_Entry__c, slaInMilliseconds); //Populate Due Date considering Business Hours
			sector.SLA_Active__c = 'Yes'; //Set the SLA_Active__c to Yes
			sector.SLA_Warning_Time__c = BusinessHours.addGmt(busiHours.id, sector.Sector_Entry__c, slaInitialWarningInMilliseconds); 
			sector.SLA_Target_Time_MM__c = Integer.valueOf(slaSectorMDTMap.get(sector.Sector1__c).SLA_Target_Time_MM__c);

			//* 1.2 9/05/19 RDAVID-task24399056 : Added Logic to apply Different time threshold for Consumer and Commercial Initial Review 
			SectorGateway.setConsumerInitialReviewTimeToAppReview(sector,busiHours,slaSectorMDTMap.get(CommonConstants.SECTOR1_NOD_SLA_APP_REVIEW),srMap,'SectorHandler',convertToMinToMS,null);

		}
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		Sector__c sector = (Sector__c)so;
		Sector__c oldSector = (Sector__c)oldSo;

		/*if (TriggerFactory.trigSet.Enable_Sector_SLA_Warning__c){
			if (sector.Sector_Entry__c != null && sector.Sector_Exit__c != null){
				SLA_Sector__mdt appReviewCustomMetadata = slaSectorMDTMap.get(sector.Sector1__c);
				Integer slaInitialWarningInMilliseconds = Integer.valueOf(slaSectorMDTMap.get(sector.Sector1__c).SLA_Initial_Warning_Time_MM__c) * convertToMinToMS;
				//sector.SLA_Warning_Time__c = BusinessHours.addGmt(busiHours.id, sector.Sector_Entry__c, slaInitialWarningInMilliseconds); 
				//Datetime warningdate = sector.SLA_Warning_Time__c;
				Long minutes = ((BusinessHours.diff (busiHours.id, sector.Sector_Entry__c,sector.Sector_Exit__c)) / 1000) / 60;
				sector.SLA_Time_MM__c = minutes; //Decimal.valueof(( BusinessHours.diff(busiHours.Id, sector.Sector_Entry__c, warningdate ) / 1000) / 60 ) + 1;  //returns SLA Age in minutes
                if(sector.SLA_Time_MM__c >= Integer.valueOf(appReviewCustomMetadata.SLA_Yellow_Icon_Threshold_MM__c) && sector.SLA_Time_MM__c < Integer.valueOf(appReviewCustomMetadata.SLA_Red_Icon_Threshold_MM__c)) 
                    sector.SLA_Warning_Status__c = 'Yellow SLA Warning Sent';
                else if(sector.SLA_Time_MM__c >= Integer.valueOf(appReviewCustomMetadata.SLA_Red_Icon_Threshold_MM__c)) 
                    sector.SLA_Warning_Status__c = 'Red SLA Warning Sent';
			}
		} */

		if (sector.Sector_Entry__c != NULL && sector.Sector1__c.containsIgnoreCase('SLA') && busiHours != NULL && sector.Sector_Entry__c != oldSector.Sector_Entry__c) {
			Integer slaInMilliseconds = Integer.valueOf(slaSectorMDTMap.get(sector.Sector1__c).SLA_Target_Time_MM__c) * convertToMinToMS;
			System.debug('slaInMilliseconds ---> '+slaInMilliseconds);
			//Update SLA Due Date if Sector_Entry__c is updated
			sector.SLA_Due_Date__c = BusinessHours.addGmt(busiHours.id, sector.Sector_Entry__c, slaInMilliseconds); 
			//* 1.2 9/05/19 RDAVID-task24399056 : Added Logic to apply Different time threshold for Consumer and Commercial Initial Review 
			if(sector.Sector1__c.containsIgnoreCase(CommonConstants.SECTOR1_INIT_REV) && srMap.get(sector.SR_Number__c).Case_Number__c != NULL && srMap.get(sector.SR_Number__c).Case_Number__r.RecordTypeId != NULL && srMap.get(sector.SR_Number__c).Case_Number__r.RecordType.Name.containsIgnoreCase('NOD: Consumer')){
				slaInMilliseconds = Integer.valueOf(slaSectorMDTMap.get(CommonConstants.SECTOR1_NOD_SLA_APP_REVIEW).SLA_Target_Time_MM__c) * convertToMinToMS;
				System.debug('slaInMilliseconds ---> '+slaInMilliseconds);
				sector.SLA_Due_Date__c = BusinessHours.addGmt(busiHours.id, sector.Sector_Entry__c, slaInMilliseconds);//Update SLA Due Date if Sector_Entry__c is updated
			}
		}
		if (sector.Sector_Exit__c != NULL) {
			if(sector.Sector1__c.containsIgnoreCase('SLA')){
				sector.SLA_Active__c = 'No'; //Set the SLA_Active__c to No
				sector.SLA_Warning_Status__c = (sector.SLA_Met__c)? 'SLA Met':'SLA Missed';//RDAVID 16/09/19 RDAVID-extra/task26058186-UpdateOnSLARules (sector.SLA_Time_MM__c == NULL || sector.SLA_Target_Time_MM__c > sector.SLA_Time_MM__c) ? 'SLA Met':'SLA Missed'; //Set SLA Warning Status when Sector is completed.
			}
			
			if(busiHours != NULL){
				sector.Time_in_Sector_Business_Hours_HH_MM1__c = SectorGateway.getTimeSpentInBusinessHours(busiHours,sector.Sector_Entry__c,sector.Sector_Exit__c);
				//JBACU 07/03/19 - Get Hours Spent on Business Hours
				if (sector.Time_in_Sector_Business_Hours_HH__c == null || ( oldSector.Sector_Exit__c != sector.Sector_Exit__c )){ 
				 	sector.Time_in_Sector_Business_Hours_HH__c = Decimal.valueof(BusinessHours.diff(busiHours.Id,sector.Sector_Entry__c,sector.Sector_Exit__c) ) / (1000*60*60); 
				}
			}
		}

		// 3/05/19 RDAVID-task24399056
		if(caseMap.get(sector.Case_Number__c) != NULL && srMap.get(sector.SR_Number__c) != NULL) //FOR SR
			SectorGateway.setRecipients(so, caseMap.get(sector.Case_Number__c), srMap.get(sector.SR_Number__c), slaSectorMDTMap, slaTypeUserMap); // 3/05/19 RDAVID-task24399056
		else if(caseMap.get(sector.Case_Number__c) != NULL && srMap.get(sector.SR_Number__c) == NULL) //FOR Case
			SectorGateway.setRecipients(so, caseMap.get(sector.Case_Number__c), null, slaSectorMDTMap, slaTypeUserMap);
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}

	public void afterInsert(SObject so){
		
	}
	
	public void afterUpdate(SObject oldSo, SObject so){
		Sector__c sec = (Sector__c)so;
		Sector__c oldSec = (Sector__c)oldSo;
		System.debug('########SectorHandler AFTERUPDATE');
		//RDAVID 18/04/2019 - Comment based on Chris feedback no need to rollup Sector SLA Status to Case
		// if(sec.SLA_Warning_Status__c != oldSec.SLA_Warning_Status__c && (sec.SLA_Warning_Status__c == 'SLA Met' || sec.SLA_Warning_Status__c == 'SLA Missed')) {
		// 	System.debug('########SLA_Warning_Status__c is UPDATED --> '+sec.SLA_Warning_Status__c);
		// 	if(slaSectorMDTMap.containsKey(sec.Sector1__c) && !String.IsBlank(slaSectorMDTMap.get(sec.Sector1__c).Case_SLA_Status__c) && sec.Sector_Exit__c != NULL && sec.Exit_Owner__c != NULL){
		// 		System.debug('########SLA_Warning_Status__c is UPDATED UPDATE PARENT CASE SLA STATUS--> '+sec.SLA_Warning_Status__c);
		// 		if(caseToUpdateList.containsKey(sec.Case_Number__c)){
		// 			SObject sObj = caseToUpdateList.get(sec.Case_Number__c);
		// 			sObj.put(slaSectorMDTMap.get(sec.Sector1__c).Case_SLA_Status__c, sec.SLA_Warning_Status__c); 
		// 			caseToUpdateList.put(sec.Case_Number__c,sObj);
		// 		}
		// 		else{
		// 			SObject sObj = Schema.getGlobalDescribe().get('Case').newSObject();
		// 			sObj.put('Id', sec.Case_Number__c); 
		// 			sObj.put(slaSectorMDTMap.get(sec.Sector1__c).Case_SLA_Status__c, sec.SLA_Warning_Status__c); 
		// 			caseToUpdateList.put(sec.Case_Number__c,sObj);
		// 		}
		// 	}   
		// }
	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		// System.debug('########caseToUpdateListList --> '+caseToUpdateList);
		//RDAVID 18/04/2019 - Comment based on Chris feedback no need to rollup Sector SLA Status to Case
		// if(caseToUpdateList.size() > 0){
		// 	if(caseToUpdateList.size()>0) update caseToUpdateList.values();
		// }
	}	
}
public class PersonAccountProcessor extends DefaultProcessor {
    public override boolean process() {
        if(!super.process())
            return false;
        List<LKRecord> personLKRecList = XMLMappingAdapter.lkRecordsMap.get('PersonAccount');
        for(LKRecord perRec : personLKRecList) {
            String entityType = (String)perRec.lkValuesMap.get('entity');
            if(entityType != null && entityType.equalsIgnoreCase('Individual')) {
                perRec.lkValuesMap.put('entity','Person');
            }
            String primaryAppl = String.valueOf(perRec.lkValuesMap.get('primary_applicant'));
            if(primaryAppl!= null && primaryAppl.equalsIgnoreCase('true')) {
                primaryAppl = 'Yes';
            }else {
                primaryAppl = 'No';
            }
            perRec.lkValuesMap.put('primary_applicant',primaryAppl);
            String housingVal = String.valueOf(perRec.lkValuesMap.get('housing_status'));
            if(housingVal != null) {
                if(housingVal.equalsIgnoreCase('Own Home')){
                   housingVal = 'OwnHome';
                }
                else if(housingVal.equalsIgnoreCase('Own Home With Mortgage')){
                    housingVal = 'OwnHomeMortgage';
                }
                else if(housingVal.equalsIgnoreCase('Living With Parents')){
                    housingVal ='WithParents';
                }
                perRec.lkValuesMap.put('housing_status',housingVal);
            }
            String maritalStatus = (String)perRec.lkValuesMap.get('marital_status');
            if(maritalStatus != null) {
                maritalStatus = maritalStatus.deleteWhitespace();
                perRec.lkValuesMap.put('marital_status',maritalStatus);
            }
        }
        return true;
    }
}
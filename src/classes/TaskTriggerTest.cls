/*
    @Description: test class of TaskUtil, TaskTriggerHandler
    @Author: Jesfer Baculod - Positive Group
    @History:   09/15/2017  - Created, added test for TaskUtils
*/        
@isTest
public class TaskTriggerTest {

    private static final string LEAD_STATS_OPEN = Label.Lead_Status_Open; //Open
    private static final string TASK_STATS_NS = Label.Task_Status_Not_Started; //Not Started
    private static final string TASK_SUBJ_PARDOT = Label.Task_Pardot_Subject; //Lead Generated [pardot]
    private static final string TASK_SUBJ_SMS_S = Label.Task_SMS_Sent_Subject; //SMS: Sent
    private static final string TASK_SUBJ_SMS_R = Label.Task_SMS_Received_Subject; //SMS: Received


    @testsetup static void setup(){

        //create test data for Account
        Account acc = new Account(
                Name = 'test'
            );
        insert acc;

        //Create test data for Lead
        Lead ld = new Lead(
                LastName = 'test'
            );
        insert ld;

        //Create test data for Opp
        Opportunity opp = new Opportunity(name = 'Test', stageName = 'open', closeDate = date.today(), loan_type2__c = 'car' );
        insert opp;

        /*//LEAD
        mercury__SMS_Message__c smsLeadOutbound = new mercury__SMS_Message__c(
            mercury__Content__c = 'Lead Outbound',
            mercury__Destination_Number__c = '639064552355',
            mercury__Is_Inbound__c = false,
            mercury__Message_Id__c = '57b14829-ee44-4270-871c-0b194c9ef8yy',
            mercury__Object_Id__c = ld.Id,
            mercury__Object_Type__c = 'Lead',
            mercury__Opt_Out__c = false,
            mercury__Status__c = 'queued',
            mercury__Who_Id__c = ld.Id);

        insert smsLeadOutbound;

        mercury__SMS_Message__c smsLeadInbound = new mercury__SMS_Message__c(
            mercury__Content__c = 'Lead Inbound',   
            mercury__Destination_Number__c = '+61488868721',
            mercury__Is_Inbound__c = true,
            mercury__Message_Id__c = '9559cb4e-d12d-4008-af18-142064fb88li',
            mercury__Opt_Out__c = false,
            mercury__Related_Id__c = 'b2cef1a2-bf40-434b-b17d-faa63c8f46li',
            mercury__Source_Number__c = '+61400475982',
            mercury__Vendor_Id__c = 'MessageMedia',
            mercury__What_Id__c = ld.Id,
            mercury__Object_Type__c = 'Lead',
            mercury__Object_Id__c = ld.Id
        );

        insert smsLeadInbound;

        //OPPORTUNITY
		mercury__SMS_Message__c smsOppOutbound = new mercury__SMS_Message__c(
			mercury__Content__c = 'Opportunity Outbound',
			mercury__Destination_Number__c = '639064552355',
			mercury__Is_Inbound__c = false,
			mercury__Message_Id__c = '57b14829-ee44-4270-871c-0b194c9ef8oo',
			mercury__Object_Id__c = opp.Id,
			mercury__Object_Type__c = 'Opportunity',
			mercury__Opt_Out__c = false,
			mercury__Status__c = 'queued',
			mercury__What_Id__c = opp.Id);

		insert smsOppOutbound;

        mercury__SMS_Message__c smsOppInbound = new mercury__SMS_Message__c(
            mercury__Content__c = 'Opportunity Inbound',   
            mercury__Destination_Number__c = '+61488868721',
            mercury__Is_Inbound__c = true,
            mercury__Message_Id__c = '9559cb4e-d12d-4008-af18-142064fb88oi',
            mercury__Opt_Out__c = false,
            mercury__Related_Id__c = 'b2cef1a2-bf40-434b-b17d-faa63c8f46oi',
            mercury__Source_Number__c = '+61400475982',
            mercury__Vendor_Id__c = 'MessageMedia',
            mercury__What_Id__c = opp.Id,
            mercury__Object_Type__c = 'Opportunity',
            mercury__Object_Id__c = opp.Id
        );

        insert smsOppInbound;

        //ACCOUNT
		mercury__SMS_Message__c smsAccOutbound = new mercury__SMS_Message__c(
			mercury__Content__c = 'Account Outbound',
			mercury__Destination_Number__c = '639064552355',
			mercury__Is_Inbound__c = false,
			mercury__Message_Id__c = '57b14829-ee44-4270-871c-0b194c9ef8ao',
			mercury__Object_Id__c = acc.Id,
			mercury__Object_Type__c = 'Account',
			mercury__Opt_Out__c = false,
			mercury__Status__c = 'queued',
			mercury__What_Id__c = acc.Id);

		insert smsAccOutbound;

        mercury__SMS_Message__c smsAccInbound = new mercury__SMS_Message__c(
            mercury__Content__c = 'Account Inbound',   
            mercury__Destination_Number__c = '+61488868721',
            mercury__Is_Inbound__c = true,
            mercury__Message_Id__c = '9559cb4e-d12d-4008-af18-142064fb88ai',
            mercury__Opt_Out__c = false,
            mercury__Related_Id__c = 'b2cef1a2-bf40-434b-b17d-faa63c8f46ai',
            mercury__Source_Number__c = '+61400475982',
            mercury__Vendor_Id__c = 'MessageMedia',
            mercury__What_Id__c = acc.Id,
            mercury__Object_Type__c = 'Account',
            mercury__Object_Id__c = acc.Id
        );

        insert smsAccInbound; */

    }

    static testmethod void testkTaskUtils(){

        Lead ld = [Select Id, Name From Lead];
        Opportunity opp = [Select Id, Name From Opportunity];

        Trigger_Settings__c trigSet = new Trigger_Settings__c(
                Enable_Task_Trigger__c = true,
                Enable_Task_changeLeadStatus__c = true
            );
        insert trigset;

        Test.startTest();

            //Test changeLeadStatus
            Task cltask = new Task(
                    WhoId = ld.Id,
                    Status = TASK_STATS_NS,
                    Subject = TASK_SUBJ_PARDOT,
                    Description = 'test',
                    Priority = 'Normal'
                );
            insert cltask;
            cltask.Description = 'test2';
            update cltask;

            //Verify Lead Status was set to Open due to trigger
            Lead upLead = [Select Id, Status From Lead Where Id = : ld.Id];
            system.assertEquals(upLead.Status, LEAD_STATS_OPEN);

            Task tsk = new Task(
                    WhoId = ld.Id,
                    CallType = 'Outbound',
                    Status = 'Completed',
                    Subject = 'File Notes'
                );
            insert tsk;

            Task tsk2 = new Task(
                    WhatId = opp.Id,
                    CallType = 'Outbound',
                    Status = 'Completed',
                    Subject = 'File Notes'
                );
            insert tsk2;

            list <Id> tskidlist = new list <Id>();
            list <Id> tskid2list = new list <Id>();
            tskidlist.add(tsk.Id);
            tskid2list.add(tsk2.Id);

            TaskUtils.updateCallsMade(tskidlist); //For Leads
            TaskUtils.updateCallsMade(tskid2list); //For Opps

            //Verify that Calls Made has been updated on Lead
            //Lead upld = [Select Id, Calls_Made__c, Last_Call_Time_Ended__c From Lead Where Id = : ld.Id];
            //system.assertEquals(upld.Calls_Made__c,1);

            //Verify that Calls Made has been updated on Opp
            //Opportunity upopp = [Select Id, Calls_Made__c, Last_Call_Time_Ended__c From Opportunity Where Id = : opp.Id];
            //system.assertEquals(upopp.Calls_Made__c,1);

        Test.stopTest();

    }

    /* static testmethod void testTaskSMS(){

		Account acc = [Select Id, Name From Account];
		Lead ld = [Select Id, Name From Lead];
		Opportunity opp = [Select Id, Name From Opportunity];

		Trigger_Settings__c trigSet = new Trigger_Settings__c(
                Enable_Task_Trigger__c = true,
                Enable_Task_changeLeadStatus__c = true
                //Enable_Task_createSMSHistory__c = true
            );
        insert trigset;

		Test.startTest();

			//Test changeLeadStatus
            Task cltask = new Task(
                    WhoId = ld.Id,
                    Status = TASK_STATS_NS,
                    Subject = TASK_SUBJ_PARDOT,
                    Description = 'test',
                    Priority = 'Normal'
                );
            insert cltask;
            cltask.Description = 'test2';
            update cltask;

            //Verify Lead Status was set to Open due to trigger
            Lead upLead = [Select Id, Status From Lead Where Id = : ld.Id];
            system.assertEquals(upLead.Status, LEAD_STATS_OPEN);
			
			/*list <Task> tsklist_ld = new list <Task>();
		
			Task taskLeadOutbound = new Task(
					WhoId = ld.Id,
					Subject = TASK_SUBJ_SMS_S,
					Description = 'tesssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssst',
					Status = 'Completed',
					Priority = 'Normal',
					mercury__SMS_Related_Id__c = 'b2cef1a2-bf40-434b-b17d-faa63c8f46lo'
			);
			tsklist_ld.add(taskLeadOutbound);

			Task taskLeadInbound = new Task(
					WhoId = ld.Id,
					Subject = TASK_SUBJ_SMS_R,
					Description = 'tesssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssst',
					Status = 'Completed',
					Priority = 'Normal',
					mercury__SMS_Related_Id__c = 'b2cef1a2-bf40-434b-b17d-faa63c8f46li'
			);
			tsklist_ld.add(taskLeadInbound);

			Task taskOppOutbound = new Task(
					WhatId = opp.Id,
					Subject = TASK_SUBJ_SMS_S,
					Description = 'tesssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssst',
					Status = 'Completed',
					Priority = 'Normal',
					mercury__SMS_Related_Id__c = 'b2cef1a2-bf40-434b-b17d-faa63c8f46oo'
			);
			tsklist_ld.add(taskOppOutbound);

			Task taskOppInbound = new Task(
					WhatId = opp.Id,
					Subject = TASK_SUBJ_SMS_R,
					Description = 'tesssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssst',
					Status = 'Completed',
					Priority = 'Normal',
					mercury__SMS_Related_Id__c = 'b2cef1a2-bf40-434b-b17d-faa63c8f46oi'
			);
			tsklist_ld.add(taskOppInbound);

			Task taskAccOutbound = new Task(
					WhatId = acc.Id,
					Subject = TASK_SUBJ_SMS_S,
					Description = 'tesssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssst',
					Status = 'Completed',
					Priority = 'Normal',
					mercury__SMS_Related_Id__c = 'b2cef1a2-bf40-434b-b17d-faa63c8f46ao'
			);
			tsklist_ld.add(taskAccOutbound);

			Task taskAccInbound = new Task(
					WhatId = acc.Id,
					Subject = TASK_SUBJ_SMS_R,
					Description = 'tesssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssst',
					Status = 'Completed',
					Priority = 'Normal',
					mercury__SMS_Related_Id__c = 'b2cef1a2-bf40-434b-b17d-faa63c8f46ai'
			);
			tsklist_ld.add(taskAccInbound);

			insert tsklist_ld;

			//Retrieve created Inbound SMS on Lead

			list <Outbound_SMS_History__c> outSMSlist = [Select Id, Name From Outbound_SMS_History__c Where OutboundSmS_lead__c =: ld.Id];
			system.assertEquals(outSMSlist.size(),1);

			list <Inbound_SMS_History__c> inSMSlist = [Select Id, Name From Inbound_SMS_History__c Where InboundSms_Opportunity__c =: opp.Id];
			system.assertEquals(inSMSlist.size(),1);

			list <Inbound_SMS_History__c> inSMSlistAccount = [Select Id, Name From Inbound_SMS_History__c Where Account__c =: acc.Id];
			system.assertEquals(inSMSlistAccount.size(),1);

			update tsklist_ld;
            delete tsklist_ld;

            undelete tsklist_ld; 

		Test.stopTest(); 
	} */
}
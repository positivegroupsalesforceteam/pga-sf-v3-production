@isTest
/** 
* @FileName: AttachmentTriggerNMTest
* @Description: test class of AttachmentGateway, AttachmentHandler
* @Copyright: Positive (c) 2018 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 9/03/18 JBACULOD Created, testSupportRequestCreateFileInBox
**/
private class AttachmentTriggerNMTest {

    @testsetup static void setup(){
        
        //Create test data for (Case, Application, Role, Account)
        TestDataFactory.Case2FOSetupTemplate();

        //Create test data for Support Request
        Case parentcse = [Select Id, CaseNumber From Case limit 1];
        Id rtSRID = Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Asset - Submission Request (New)').getRecordTypeId();
        list <Support_Request__c> srlist = new list <Support_Request__c>();
        for (integer i=0; i<10;i++){
            Support_Request__c sr = new Support_Request__c(
                RecordTypeId = rtSRID,
                Case_Number__c = parentcse.Id
            );
            srlist.add(sr);
        }
        insert srlist;
        
        
    }    

    static testmethod void testSupportRequestCreateFileInBox(){

        //Retrieve Support Request test data
        list <Support_Request__c> srlist = [Select Id, Name, Case_Number__c From Support_Request__c];

        Test.starttest();

            Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Attachment_Trigger__c = true,
                Enable_Attachment_Create_File_in_Box__c = true
            );
            insert trigSet;

            Case cse = new Case(Status='Open');
            insert cse;

            list <Attachment> attlist = new list <Attachment>();
            for (Support_Request__c sr : srlist){
                Attachment att = new Attachment(
                    ParentId = sr.Id,
                    Body = Blob.valueof('Body of' + sr.Name),
                    Name = 'Credit Proposal ' + sr.Name + '.pdf'
                );
                attlist.add(att);
            } 
            insert attlist;
            Attachment att2 = new Attachment(
                ParentId = cse.Id,
                Body = Blob.valueof('Body of Case Att'),
                Name = 'NODIFI - Approval - Introducer - XXXX.pdf'
            );
            insert att2;
            //Can't do assertion due to Box Callout, may need to add flag for Support Request once file got created in Box

            //Covering update/delete trigger events in Attachments
            update attlist;
            delete attlist;

        Test.stopTest();

    }

    static testmethod void testAPITransferCreateFileInBox(){
        Case cse = [SELECT Id FROM Case LIMIT 1];
        API_Transfer__c apiTransferTest = new API_Transfer__c(Case__c = cse.Id, RecordtypeId = Schema.SObjectType.API_Transfer__c.getRecordTypeInfosByName().get('Equifax').getRecordTypeId(), EQ_Permission_Type_Code__c = 'Consumer');
        Database.insert(apiTransferTest);
        System.assertNotEquals(null, apiTransferTest.Id);

        Test.starttest();

            Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Attachment_Trigger__c = true,
                Enable_Attachment_Create_File_in_Box__c = true
            );
            insert trigSet;

            list <Attachment> attlist = new list <Attachment>();
            List<API_Transfer__c> apiTransList = [SELECT Id,Name FROM API_Transfer__c];
            System.assertNotEquals(0, apiTransList.size());
            for (API_Transfer__c apiTrans : apiTransList){
                Attachment att = new Attachment(
                    ParentId = apiTrans.Id,
                    Body = Blob.valueof('Body of' + apiTrans.Name),
                    Name = 'EA-' + apiTrans.Name + 'Equifax Apply.pdf'
                );
                attlist.add(att);
            } 
            insert attlist;
        Test.stopTest();

    }

}
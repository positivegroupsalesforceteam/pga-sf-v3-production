@isTest
public class LoanDocumentRequestFormTest {
    
    @isTest static void loanDocsTest(){
        
    // Create Account
        Account acc = new Account();
        acc.FirstName = 'Test';
        acc.LastName = 'Test';        
        acc.PersonMobilePhone = '12345';
        acc.PersonEmail = 'test@test.com';
        acc.Primary_Country_of_Citizenship__c='India';
        acc.Additional_Previous_Address_Information__c ='Greece';
        acc.Current_Street_Number__c = '1';
        acc.Current_Street_Name__c='Smith';
        acc.Current_Street_Type__c='Street';
        acc.Current_Suburb__c='Brunswick';
        acc.Current_State__c='Victoria';
        acc.Current_Postcode__c=1234;
        acc.Current_Country__c='Australia';
        acc.Residential_Status__c = 'Mortgage';
        acc.Medicare_Number__c = '12345';
        insert acc;
        
        acc.Primary_Country_of_Citizenship__c ='Australia';
        update acc;
        
    //Create Opportunity
        Opportunity opp = new Opportunity();
        opp.name = 'Test';
        opp.stageName = 'open';
        opp.Application_Type__c = 'Joint Application';
        opp.Loan_Type2__c = '';
        opp.Loan_Product__c = 'Consumer Loan';
        opp.Loan_Amount__c=null;
        opp.Bankruptcy__c='No';
        opp.Length_of_time_in_employment__c='20';
        opp.Available_Income__c=1000;
        opp.Income_Frequency__c='monthly';
        opp.Sale_Type__c = 'Private';
        opp.closeDate = date.today();
        opp.AccountId=acc.Id;
                
        opp.Lender__c = 'Liberty';
        opp.Payment_Frequency__c='monthly';
        opp.Previous_Bankrupt__c = 'Yes';
        opp.Net_Trade_In__c = 0;
        opp.Available_Deposit__c = 100;
        opp.Partner_Pay_Slip_Obtained__c = true;
        opp.Trade_In_Description__c ='';
        opp.Approval_Conditions__c = '';
        insert opp;

    
        Loan_Document_Request_Form__c loanObj = new Loan_Document_Request_Form__c();
        loanObj.Client_Name__c = opp.Id;
        loanObj.Application_Type__c = 'Individual Application';
        loanObj.Vehicle_Price__c = 200.00;
        loanObj.Interest_Rate__c = 2;
        loanObj.Loan_Term2__c = '60 months';
        loanObj.Trail__c = 1.5;
        
        insert loanObj;
        
        ApexPages.StandardController loan = new ApexPages.StandardController(opp);
        LoanDocumentRequestForm loanDocs = new LoanDocumentRequestForm(loan);
       
        loanDocs.brokerage = 100;
        loanDocs.errorMessages = false;
        loanDocs.figuresAreCorrect = false;
        loanDocs.gapPremium = 100;
        loanDocs.gapProvider = 'PLS ERIC';
        loanDocs.getgapProviderSp();
        loanDocs.getinsurancePackageSp();
        loanDocs.getlipCoverOptionSp();
        loanDocs.getlpiCoverTypeSp();
        loanDocs.getwarrantyProvidersp();
        loanDocs.getwarrantyTermSp();
        loanDocs.insurancePackage = 'test';
        loanDocs.insurancePackageNotesCOMPULSORY = 'test';
        loanDocs.lpiCoverOption = 'Life Only Cover';
        loanDocs.lpiCoverType = 'Individual Consumer';
        loanDocs.notesToLender = '';
        loanDocs.notesToSupport = '';
        loanDocs.payoutLetterInFile = false;
        loanDocs.payoutValidMin5Days = false;
        loanDocs.receivedInsuranceWaiver = false;
        loanDocs.saveUpdates();
        loanDocs.warrantyPremium = 1.2;
        loanDocs.warrantyProductName = 'test';
        loanDocs.warrantyProvider = 'PLS ERIC';
        loanDocs.warrantyTerm = '1 Year';
        loanDocs.oppOrtunityId();
        try{
         loanDocs.submit();
         }catch(exception e){
         
         }
        
    }
    @isTest static void loanDocsValidateLdr(){
        
    // Create Account
        Account acc = new Account();
        acc.FirstName = 'Test';
        acc.LastName = 'Test';        
        acc.PersonMobilePhone = '12345';
        acc.PersonEmail = 'test@test.com';
        acc.Primary_Country_of_Citizenship__c='India';
        acc.Additional_Previous_Address_Information__c ='Greece';
        acc.Current_Street_Number__c = '1';
        acc.Current_Street_Name__c='Smith';
        acc.Current_Street_Type__c='Street';
        acc.Current_Suburb__c='Brunswick';
        acc.Current_State__c='Victoria';
        acc.Current_Postcode__c=1234;
        acc.Current_Country__c='Australia';
        acc.Residential_Status__c = 'Mortgage';
        acc.Medicare_Number__c = '12345';
        insert acc;
        
        acc.Primary_Country_of_Citizenship__c ='Australia';
        update acc;
        
    //Create Opportunity
        Opportunity opp = new Opportunity();
        opp.name = 'Test';
        opp.stageName = 'open';
        opp.Application_Type__c = 'Joint Application';
        opp.Loan_Type2__c = '';
        opp.Loan_Product__c = 'Consumer Loan';
        opp.Loan_Amount__c=null;
        opp.Bankruptcy__c='No';
        opp.Length_of_time_in_employment__c='20';
        opp.Available_Income__c=1000;
        opp.Income_Frequency__c='monthly';
        opp.closeDate = date.today();
        opp.AccountId=acc.Id;
        opp.Vehicle_Purchase_Price__c=100; 
        opp.Available_Deposit__c=50;
        opp.Net_Trade_In__c=10;
        opp.Payout__c=20;
        opp.Lender__c = 'Liberty';
        opp.Payment_Frequency__c='monthly';
        opp.Previous_Bankrupt__c = 'Yes';
        opp.Net_Trade_In__c = 0;
        opp.Available_Deposit__c = 100;
        opp.Partner_Pay_Slip_Obtained__c = true;
        opp.Trade_In_Description__c ='';
        opp.Approval_Conditions__c = '';
        insert opp;

    
        Loan_Document_Request_Form__c loanObj = new Loan_Document_Request_Form__c();
        loanObj.Client_Name__c = opp.Id;
        loanObj.Application_Type__c = 'Individual Application';
        loanObj.Vehicle_Price__c = 200.00;
        loanObj.Interest_Rate__c = 2;
        loanObj.Loan_Term2__c = '60 months';
        loanObj.Trail__c = 1.5;
        
        insert loanObj;
        
        ApexPages.StandardController loan = new ApexPages.StandardController(opp);
        LoanDocumentRequestForm loanDocs = new LoanDocumentRequestForm(loan);
       
        loanDocs.notesToLender = '';
        loanDocs.notesToSupport = '';
        loanDocs.payoutLetterInFile = false;
        loanDocs.payoutValidMin5Days = false;
        loanDocs.receivedInsuranceWaiver = false;
        loanDocs.insurancePackage = null;
        loanDocs.insurancePackageNotesCOMPULSORY = '';
        loanDocs.figuresAreCorrect=false;
        loanDocs.gapProvider=null;
        loanDocs.lpiCoverOption = null;
        loanDocs.warrantyProvider = null;
        loanDocs.warrantyPremium = 1.2;
        loanDocs.warrantyProductName = 'test';
        loanDocs.warrantyProvider = 'PLS ERIC';
        loanDocs.warrantyTerm = '1 Year'; 
        loanDocs.validateLdr();

        
    }
    
}
/*
	@Description: handler class of TaskTrigger
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	9/14/2017 - Created, Transferred existing functionalities (changeLeadStatus).
		-	11/24/2017 - Updated, due to issue with Trigger Settings
		-   1/25/2018  - Phone Number not populating issue - RDAVID
		-	10/11/2018 - Removed creation of SMS History due to Mercursy SMS being expired - JBACULOD
*/
public class TaskTriggerHandler {
	
	static Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());

	private static final string LEAD_STATS_OPEN = Label.Lead_Status_Open; //Open
	private static final string TASK_STATS_NS = Label.Task_Status_Not_Started; //Not Started
	private static final string TASK_SUBJ_PARDOT = Label.Task_Pardot_Subject; //Lead Generated [pardot]
	private static final string TASK_SUBJ_SMS_S = Label.Task_SMS_Sent_Subject; //SMS: Sent
	private static final string TASK_SUBJ_SMS_R = Label.Task_SMS_Received_Subject; //SMS: Received
	private static final string LEAD_STR = 'Lead';
	private static final string ACC_STR = 'Account';
	private static final string OPP_STR = 'Opportunity';

	//execute all Before Insert event triggers
	public static void onBeforeInsert(list <Task> newTasklist){
		if (trigset.Enable_Task_changeLeadStatus__c) changeLeadStatus(null,newTasklist);
	}

	//execute all Before Update event triggers
	public static void onBeforeUpdate(map<Id,Task> oldTaskMap, map <Id,Task> newTaskMap){
		list <Task> newTasklist = newTaskMap.values();
		if (trigset.Enable_Task_changeLeadStatus__c) changeLeadStatus(oldTaskMap,newTasklist);	
	}

	//execute all After Insert event triggers
	public static void onAfterInsert(map<Id,Task> oldTaskMap, map <Id,Task> newTaskMap){
		//if (trigset.Enable_Task_createSMSHistory__c) createSMSHistory(oldTaskMap,newTaskMap);
	}

	//execute all After Update event triggers
	public static void onAfterUpdate(map<Id,Task> oldTaskMap, map <Id,Task> newTaskMap){
		//if (trigset.Enable_Task_createSMSHistory__c) createSMSHistory(oldTaskMap,newTaskMap);	
	}

	//execute all After Undelete event triggers
	public static void onAfterUndelete(map<Id,Task> oldTaskMap, map <Id,Task> newTaskMap){
		//if (trigset.Enable_Task_createSMSHistory__c) createSMSHistory(oldTaskMap,newTaskMap);
	}


	/* @Author: Sri Babu
		Created 30/12/2014 - changeLeadStatus.trigger
	*/
	private static void changeLeadStatus(map<Id,Task> oldTaskMap, list <Task> newTasklist){

		set <Id> upLeadIdsSet = new set <Id>();
		for (Task t : newTasklist){
			if (t.Status == TASK_STATS_NS && t.Subject == TASK_SUBJ_PARDOT){
				if (t.WhoId != null){
					if (String.valueof(t.WhoId).startsWith('00Q')){
						upLeadIdsSet.add(t.WhoId)	;
					}
				}
			}
		}
		list <Lead> upLeadlist = [Select Id, Status From Lead Where Id in : upLeadIdsSet AND IsConverted = False];
		//Set Lead Status to Open
		for (Lead ld : upLeadlist){
			ld.Status = LEAD_STATS_OPEN; 
		}
		update upLeadlist;
	}

	//Removed due to Mercury SMS being expired (JBACU - 10/11/18)
	/* private static void createSMSHistory(map<Id,Task> oldTaskMap, map<Id,Task> newTaskMap){

		set <Id> upLeadIdsSet = new set <Id>();
		set <Id> upAccIdsSet = new set <Id>();
		set <Id> upOppIdsSet = new set <Id>();
		Set <String> mercurySMSRelatedIdsSet = new Set<String>();//RDAVID
		map <Id,Lead> leadMap = new map <id,Lead>();
		map <id,Opportunity> oppMap = new map <id,Opportunity>();
		map <id,Account> accMap = new map <id,Account>();
		map <String,mercury__SMS_Message__c> mercurySMSMessageMap = new map<String,mercury__SMS_Message__c>();//RDAVID
		map <Id,mercury__SMS_Message__c> outboundSMSForLeadMap = new map<Id,mercury__SMS_Message__c>();
		map <Id,mercury__SMS_Message__c> outboundSMSForAccOppMap = new map<Id,mercury__SMS_Message__c>();
		List <mercury__SMS_Message__c> mercurySMSMessageList = new List<mercury__SMS_Message__c>();
		list <Outbound_SMS_History__c> outSMSlist_ld = new list <Outbound_SMS_History__c>();
		list <Inbound_SMS_History__c> inSMSlist_ld = new list <Inbound_SMS_History__c>();
		list <Outbound_SMS_History__c> outSMSlist_opp = new list <Outbound_SMS_History__c>();
		list <Inbound_SMS_History__c> inSMSlist_opp = new list <Inbound_SMS_History__c>();
		list <Outbound_SMS_History__c> outSMSlist_acc = new list <Outbound_SMS_History__c>();
		list <Inbound_SMS_History__c> inSMSlist_acc = new list <Inbound_SMS_History__c>();

		for (Task t : newTaskMap.values()){
			if (t.WhoId != null){
				if (String.valueof(t.WhoId).startsWith('00Q')){ //Assigned To is a Lead
					upLeadIdsSet.add(t.WhoId);
				}
			}
			if (t.WhatId != null){
				if (String.valueof(t.WhatId).startsWith('001')){ //Related To is an Account
					upAccIdsSet.add(t.WhatId);
				}
				else if (String.valueof(t.WhatId).startsWith('006')){ //Related To is an Opportunity
					upOppIdsSet.add(t.WhatId);
				}
			}
			if(t.mercury__SMS_Related_Id__c != NULL){//RDAVID
				mercurySMSRelatedIdsSet.add(t.mercury__SMS_Related_Id__c);
			}
		}

		leadMap = new map<Id,Lead>([Select Id, First_Call_Made_Time__c From Lead Where Id in : upLeadIdsSet]);
		oppMap = new map<Id,Opportunity>([Select Id From Opportunity Where Id in : upOppIdsSet]);
		accMap = new map<Id,Account>([Select Id, Name From Account Where Id in : upAccIdsSet]);
		mercurySMSMessageList = [	SELECT Id, mercury__Source_Number__c , mercury__Related_Id__c, mercury__Is_Inbound__c, mercury__Content__c, mercury__Object_Type__c, mercury__Destination_Number__c, mercury__Who_Id__c,mercury__What_Id__c 
									FROM mercury__SMS_Message__c 
									WHERE (mercury__Related_Id__c IN: mercurySMSRelatedIdsSet 
									AND mercury__Is_Inbound__c =: TRUE 
									AND mercury__Source_Number__c != NULL)
									OR ((mercury__Who_Id__c IN: upLeadIdsSet OR mercury__What_Id__c IN: upAccIdsSet OR mercury__What_Id__c IN: upOppIdsSet ) AND mercury__Is_Inbound__c =: FALSE AND mercury__Destination_Number__c != NULL)];
		
		for(mercury__SMS_Message__c sms : mercurySMSMessageList){
			if(sms.mercury__Related_Id__c != NULL && sms.mercury__Is_Inbound__c){
				mercurySMSMessageMap.put(sms.mercury__Related_Id__c,sms);
			}
			else if(!sms.mercury__Is_Inbound__c){
				if(sms.mercury__Object_Type__c == LEAD_STR){
					outboundSMSForLeadMap.put(sms.mercury__Who_Id__c,sms);		
				}
				else if(sms.mercury__Object_Type__c == ACC_STR || sms.mercury__Object_Type__c == OPP_STR){
					outboundSMSForAccOppMap.put(sms.mercury__What_Id__c,sms);		
				}
				
			}
		}

		for (Task t : newTaskMap.values()){
			//Task related to Lead
			String outboundPhoneNumber = '';
			String inboundPhoneNumber = '';
			String inboundContent = '';

			if (t.WhoId != null){
				if (leadMap != null){
					if (leadMap.containskey(t.WhoId)){
						if (t.Subject == TASK_SUBJ_SMS_S){ //SMS: Sent for Lead
							if(!outboundSMSForLeadMap.isEmpty()){
								outboundPhoneNumber = (outboundSMSForLeadMap.get(t.WhoId).mercury__Destination_Number__c != NULL)? outboundSMSForLeadMap.get(t.WhoId).mercury__Destination_Number__c : '';
							}
		
							outSMSlist_ld.add(
								new Outbound_SMS_History__c(
									OutboundSmS_lead__c = t.WhoId,
									OutboundSms_SMSText__c = t.Description,
									OutboundSms_StatusMessage__c = t.Status,
									OutboundSms_Sent_by__c = t.createdBy.Name,
									OutboundSms_SentStatus__c = t.Status,
									OutboundSms_Name__c = t.Who.Name,
									OutboundSms_MobileNumber__c =outboundPhoneNumber//RDAVID
								)
							);
						}
						else if (t.Subject == TASK_SUBJ_SMS_R){ //SMS: Received for Lead
							if (t.Description != null){
								if(!mercurySMSMessageMap.isEmpty()){
									inboundPhoneNumber = (mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Source_Number__c != NULL) ? mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Source_Number__c : '';
									inboundContent = (mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Content__c != NULL) ? mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Content__c : '';
								}
								inSMSlist_ld.add(
									new Inbound_SMS_History__c(
										InboundSms_Lead__c = t.WhoId,
										InboundSMS_Sent_By__c = t.CreatedBy.Name,
										InboundSms_Mobile_Number__c = inboundPhoneNumber,//descriptionVal,
										InboundSms_SMS_Text__c = inboundContent,//desTrimVal,
										InboundSms_SentStatus__c = t.Status
									)
								);
							}
						}
					}
				}
			}
			//Task related to Acc / Opp
			if (t.WhatId != null){				
				if (oppMap != null){
					if (oppMap.containskey(t.WhatId)){
						if (t.Subject == TASK_SUBJ_SMS_S){ //SMS: Sent for Opp
							if(!outboundSMSForAccOppMap.isEmpty()){
								outboundPhoneNumber = (outboundSMSForAccOppMap.get(t.WhatId).mercury__Destination_Number__c != NULL)? outboundSMSForAccOppMap.get(t.WhatId).mercury__Destination_Number__c : '';
							} 
							outSMSlist_opp.add(
								new Outbound_SMS_History__c(
									OutboundSms_Opportunity__c = t.WhatId,
									OutboundSms_SMSText__c = t.Description,
									OutboundSms_StatusMessage__c = t.Status,
									OutboundSms_Sent_by__c = t.createdBy.Name,
									OutboundSms_SentStatus__c = t.Status,
									OutboundSms_Name__c = t.What.Name,
									OutboundSms_MobileNumber__c =outboundPhoneNumber//RDAVID
								)
							);
						}
						else if (t.Subject == TASK_SUBJ_SMS_R){ //SMS: Received for Opp
							if (t.Description != null){
								if(!mercurySMSMessageMap.isEmpty()){
									inboundPhoneNumber = (mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Source_Number__c != NULL) ? mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Source_Number__c : '';
									inboundContent = (mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Content__c != NULL) ? mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Content__c : '';
								}
								inSMSlist_opp.add(
									new Inbound_SMS_History__c(
										InboundSms_Opportunity__c = t.WhatId,
										InboundSMS_Sent_By__c = t.CreatedBy.Name,
										InboundSms_Mobile_Number__c = inboundPhoneNumber,//descriptionVal,
										InboundSms_SMS_Text__c = inboundContent,//desTrimVal,
										InboundSms_SentStatus__c = t.Status
									)
								);
							}
						}
					}
				}
				if (accMap != null){
					if (accMap.containskey(t.WhatId)){
						if (t.Subject == TASK_SUBJ_SMS_S){ //SMS: Sent for Acc
							if(!outboundSMSForAccOppMap.isEmpty()){
								outboundPhoneNumber = (outboundSMSForAccOppMap.get(t.WhatId).mercury__Destination_Number__c != NULL)? outboundSMSForAccOppMap.get(t.WhatId).mercury__Destination_Number__c : '';
							} 
							outSMSlist_acc.add(
								new Outbound_SMS_History__c(
									Account__c = t.WhatId,
									OutboundSms_SMSText__c = t.Description,
									OutboundSms_StatusMessage__c = t.Status,
									OutboundSms_Sent_by__c = t.CreatedBy.Name,
									OutboundSms_SentStatus__c = t.Status,
									OutboundSMS_Name__c = t.What.Name,
									OutboundSms_MobileNumber__c =outboundPhoneNumber//RDAVID
								)
							);
						}
						else if (t.Subject == TASK_SUBJ_SMS_R){ //SMS: Received for Acc
							if (t.Description != null){
								if(!mercurySMSMessageMap.isEmpty()){
									inboundPhoneNumber = (mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Source_Number__c != NULL) ? mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Source_Number__c : '';
									inboundContent = (mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Content__c != NULL) ? mercurySMSMessageMap.get(t.mercury__SMS_Related_Id__c).mercury__Content__c : '';
								}
								inSMSlist_acc.add(
									new Inbound_SMS_History__c(
										Account__c = t.WhatId,
										InboundSMS_Sent_By__c = t.CreatedBy.Name,
										InboundSMS_Mobile_Number__c = inboundPhoneNumber,//descriptionVal,
										InboundSms_SMS_Text__c = inboundContent,//desTrimVal,
										InboundSms_SentStatus__c = t.Status
									)
								);
							}
						}
					}
				}
			}
		}

		//Create Outbound SMS History on Leads
		if (outSMSlist_ld.size() > 0){
			insert outSMSlist_ld;
		}
		//Create Inbound SMS History on Leads
		if (inSMSlist_ld.size() > 0){
			insert inSMSlist_ld;
		}
		//Create Outbound SMS History on Opportunities
		if (outSMSlist_opp.size() > 0){
			insert outSMSlist_opp;
		}
		//Create Inbound SMS History on Opportunities
		if (inSMSlist_opp.size() > 0){
			insert inSMSlist_opp;
		}
		//Create Outbound SMS History on Accounts
		if (outSMSlist_acc.size() > 0){
			insert outSMSlist_acc;
		}
		//Create Inbound SMS History on Accounts
		if (inSMSlist_acc.size() > 0){
			insert inSMSlist_acc;
		}
	} */
}
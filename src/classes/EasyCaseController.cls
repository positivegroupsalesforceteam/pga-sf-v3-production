/** 
* @FileName: EasyCaseController
* @Description: controller class of EasyCase VF page
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 3/21/18 JBACULOD Created Class, [SD-28] SF - NM - Easy Case
* 1.1 3/22/18 JBACULOD Updated class
* 1.2 3/23/18 JBACULOD Added saveEasyCase method, TODO: Custom Labels
* 1.3 3/26/18 JBACULOD Added changes base on Raoul and Mortgage team's feedback
* 1.4 3/27/18 JBACULOD Added validateEasyCase and CommonConstants reference
* 1.5 4/04/18 JBACULOD Added Applicant Type selection; Validations for Primary Applicant, Consumer Loan - Individual
**/ 
public class EasyCaseController {

    public Case cse {get;set;}
    public list <roleWrapper> roleWrlist {get;set;}
    public integer curPage {get;set;}
    public integer curRoleCtr {get;set;}
    private Map <string, id> caseRTMap {get;set;}
    private Map <id, string> caseRTNameMap {get;set;}
    private Map <string, id> roleRTMap {get;set;}
    private Map <id, string> roleRTNameMap {get;set;}
    private list <Id> roleRTOrderedList {get;set;}
    private Map <string, id> accRTMap {get;set;}
    public list <SelectOption> RoleRTs {get;set;}
    public list <SelectOption> AccRTs {get;set;}
    public boolean hasErrors {get;set;}

    //Builds select options of Case Record Types
    public list <SelectOption> getCaseRTs(){ 
        list <SelectOption> options = new List <SelectOption>();
        //Retrieving Record Types via SOQL will not be able to display available Record Types by Profile
        list <RecordTypeInfo> infos = Case.SobjectType.getDescribe().getRecordTypeInfos();
        for (RecordTypeInfo i : infos){
            if (i.isActive()){
                if (i.isAvailable()){
                    if (i.getName().startsWith(CommonConstants.PREFIX_N) || i.getName().startsWith(CommonConstants.PREFIX_P) || i.getName().startsWith(CommonConstants.PREFIX_Z)){
                            options.add(new SelectOption(i.getRecordTypeId(), i.getName()));
                            caseRTMap.put(i.getName(), i.getRecordTypeId());
                            caseRTNameMap.put(i.getRecordTypeId(),i.getName());
                    }
                }
            }
        }
        if (cse.RecordTypeId == null) cse.RecordTypeId = options[0].getValue();  
        return options;
    }

    //Build select options of Case's Application Record Type
    public list <SelectOption> getAppRTs(){
        list <SelectOption> options = new List <SelectOption>();
        Schema.DescribeFieldResult fieldResult = Case.Application_Record_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            if (String.valueof(caseRTNameMap.get(cse.RecordTypeId)).contains(CommonConstants.PREFIX_CONS)){ //Display Applicant Types related to selected Consumer Case RT
                if (f.getLabel().startsWith(CommonConstants.PREFIX_CONS)){                                
                    options.add(new SelectOption(f.getLabel(), f.getValue()));
                }
            }
            else if (String.valueof(caseRTNameMap.get(cse.RecordTypeId)).contains(CommonConstants.PREFIX_COMM)){ //Display Applicant Types related to selected Commercial Case RT
                 if (f.getLabel().startsWith(CommonConstants.PREFIX_COMM)){                                
                    options.add(new SelectOption(f.getLabel(), f.getValue()));
                }
            } 
            else options.add(new SelectOption(f.getLabel(), f.getValue())); //Display all available picklist values          
        } 
        return options;
    }

    //Builds select options of Role Record Types
    private list <SelectOption> setRoleRTs(){ //Dependent on Selected Case RT
        list <SelectOption> options = new List <SelectOption>();
        //Retrieving Record Types via SOQL will not be able to display available Record Types by Profile
        list <RecordTypeInfo> infos = Role__c.SobjectType.getDescribe().getRecordTypeInfos();
        roleRTOrderedList = new list <Id>();
        for (RecordTypeInfo i : infos){
            if (i.isActive()){
                if (i.isAvailable()){
                    if (String.valueof(caseRTNameMap.get(cse.RecordTypeId)).contains(CommonConstants.PREFIX_CONS)){ //Display Role RTs related to selected Consumer Case RT
                        if (i.getName().contains('Individual') || i.getName().contains('Guarantor') || i.getName() == CommonConstants.ROLE_RT_PURC_INSU || i.getName() == CommonConstants.ROLE_RT_PURC_SOUR || i.getName() == CommonConstants.ROLE_RT_NON_B_SPOUSE){
                                options.add(new SelectOption(i.getRecordTypeId(), i.getName()));
                                roleRTMap.put(i.getName(), i.getRecordTypeId());
                                roleRTNameMap.put(i.getRecordTypeId(),i.getName());
                                roleRTOrderedList.add(i.getRecordTypeId());
                        }
                    }
                    else if (String.valueof(caseRTNameMap.get(cse.RecordTypeId)).contains(CommonConstants.PREFIX_COMM)){ //Display Role RTs related to selected Commercial Case RT
                        if (i.getName().contains('Business') || i.getName().contains('Company') || i.getName().contains('Partnership') || i.getName() == CommonConstants.ROLE_RT_APP_SOLE || i.getName() == CommonConstants.ROLE_RT_APP_TRUS || i.getName() == CommonConstants.ROLE_RT_PURC_INSU || i.getName() == CommonConstants.ROLE_RT_PURC_SOUR || i.getName() == CommonConstants.ROLE_RT_REL_PART){
                                options.add(new SelectOption(i.getRecordTypeId(), i.getName()));
                                roleRTMap.put(i.getName(), i.getRecordTypeId());
                                roleRTNameMap.put(i.getRecordTypeId(),i.getName());
                                roleRTOrderedList.add(i.getRecordTypeId());
                        }
                    }
                    else{ //Non-Commercial / Non-Consumer Case RTs: e.g. Used Cars, z: Complaints, z: Service Generic
                        if (i.getName() != 'Master'){ //Exclude Master RT
                            options.add(new SelectOption(i.getRecordTypeId(), i.getName()));
                            roleRTMap.put(i.getName(), i.getRecordTypeId());
                            roleRTNameMap.put(i.getRecordTypeId(),i.getName());
                            roleRTOrderedList.add(i.getRecordTypeId());
                        }
                    }
                }
            }
        }
        return options;
    }

    //Builds select options of Account Record Types
    private list <SelectOption> setAccRTs(){ 
        list <SelectOption> options = new List <SelectOption>();
        //Retrieving Record Types via SOQL will not be able to display available Record Types by Profile
        list <RecordTypeInfo> infos = Account.SobjectType.getDescribe().getRecordTypeInfos();
        for (RecordTypeInfo i : infos){
            if (i.isActive()){
                if (i.isAvailable()){
                    if ( i.getName() == CommonConstants.ACC_RT_PARTNERSHIP || i.getName() == CommonConstants.ACC_RT_COMPANY || i.getName() == CommonConstants.ACC_RT_TRUST_CAT || i.getName() == CommonConstants.ACC_RT_TRUST_IAT || i.getName() == CommonConstants.PACC_RT_I ){
                            options.add(new SelectOption(i.getRecordTypeId(), i.getName()));
                            accRTMap.put(i.getName(), i.getRecordTypeId());
                    }
                }
            }
        }
        return options;
    }

    //Helper method executed on Case Record Type select
    public void defaultCaseLoanType(){
        cse.Loan_Type__c = null; //Can't do defaulting of picklist values by RT in Apex; Defaulting of Loan Type will be handled via javascript in page
    }

    //Method executed when New Account button got clicked
    public void defaultRTshowAcc(){
        
        AccRTs = setAccRTs();
        //Default Account Record Type depending on selected Role RT
        for (rolewrapper rwr : roleWrlist){
            if (rwr.roleCtr == curRoleCtr){
                system.debug('@@role.RecordTypeId:'+rwr.role.RecordTypeId);
                if (String.valueof(roleRTNameMap.get(rwr.role.RecordTypeId)).contains(CommonConstants.PREFIX_COMM) || 
                    String.valueof(roleRTNameMap.get(rwr.role.RecordTypeId)).contains('Company') || 
                    String.valueof(roleRTNameMap.get(rwr.role.RecordTypeId)).contains('Partnership') || 
                    String.valueof(roleRTNameMap.get(rwr.role.RecordTypeId)).contains('Business') ){
                    rwr.acc.RecordTypeId = accRTMap.get(CommonConstants.ACC_RT_COMPANY); //Default Account RT to nm: Company
                }
                else if (String.valueof(roleRTNameMap.get(rwr.role.RecordTypeId)).contains(CommonConstants.PREFIX_CONS) || 
                        String.valueof(roleRTNameMap.get(rwr.role.RecordTypeId)).contains('Individual') ){
                    rwr.acc.RecordTypeId = accRTMap.get(CommonConstants.PACC_RT_I); //Default Account RT to nm: Individual
                } 
                break;
            }
        }
        checkAccIfPerson();
    }

    //Method executed when Continue button got clicked
    public PageReference Pcontinue(){
        curPage++;
        if (curPage == 2){
            RoleRTs = setRoleRTs();
            defineRoles();
            /* string cseRTName = caseRTNameMap.get(cse.RecordTypeId);
            for (roleWrapper rwr : roleWrlist){
                if (rwr.role.RecordTypeId == null){ //Record Type not yet selected for Role
                    rwr.role.RecordTypeId = roleRTOrderedList[0];
                }
            } */
        }
        else if (curPage == 3 ){ //Summary Page
            if (validateRoles()){ 
                hasErrors = false; 
                displayTempAccNameInRoles();
            }
            else{ 
                curPage = 2;
            }
        }
        return null;
    }

    //Executed before displaying Summary Page, temporarily displays inputted Account either new or existing
    private void displayTempAccNameInRoles(){
        for (roleWrapper rwr : roleWrlist){
            if (rwr.isCreateAcc){
                if (rwr.isPerson){
                    rwr.accTxt = '';
                    //Display Person Account Name inputted
                    if (rwr.con.FirstName != null) rwr.accTxt = rwr.con.FirstName + ' ';
                    if (rwr.con.MiddleName != null) rwr.accTxt+= rwr.con.MiddleName + ' ';
                    if (rwr.con.LastName != null) rwr.accTxt+= rwr.con.LastName;
                }
                else{
                    rwr.accTxt = rwr.acc.Name; //Display Business Account Name inputted
                }
            }
        }
    }

    public void defineRoles(){
        //Define prepopulated Roles base on Lead Application Type - Set to Convert
        roleWrlist = new list <roleWrapper>();
        if (cse.Application_Record_Type__c == CommonConstants.APP_RT_CONS_I || cse.Application_Record_Type__c == CommonConstants.APP_RT_CONS_M){
            roleWrapper rwr_prim = new roleWrapper( new Role__c(RecordTypeId = roleRTMap.get(CommonConstants.ROLE_RT_APP_INDI), Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT), 
                                               accRTMap.get(CommonConstants.PACC_RT_L_IA) ); //Set default Values for Role
            rwr_prim.roleCtr = roleWrlist.size() + 1;
            roleWrlist.add(rwr_prim);

            /* roleWrapper rwr_gua = new roleWrapper( new Role__c(RecordTypeId = roleRTMap.get('Consumer Loan - Guarantor'), Role_Type__c = 'Guarantor'), 
                                               accRTMap.get('nm: Lead - Individual') ); //Set default Values for Role
            roleWrlist.add(rwr_gua); */
        }
        else if (cse.Application_Record_Type__c == CommonConstants.APP_RT_CONS_J){
            roleWrapper rwr_prim = new roleWrapper( new Role__c(RecordTypeId = roleRTMap.get(CommonConstants.ROLE_RT_APP_INDI), Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT), 
                                               accRTMap.get(CommonConstants.PACC_RT_L_IA) ); //Set default Values for Role
            rwr_prim.roleCtr = roleWrlist.size() + 1;
            roleWrlist.add(rwr_prim);

            roleWrapper rwr_co = new roleWrapper( new Role__c(RecordTypeId = roleRTMap.get(CommonConstants.ROLE_RT_APP_INDI), Role_Type__c = CommonConstants.ROLE_CO_APPLICANT), 
                                               accRTMap.get(CommonConstants.PACC_RT_L_IA) ); //Set default Values for Role
            rwr_co.roleCtr = roleWrlist.size() + 1;                                               
            roleWrlist.add(rwr_co);
        }
        else if (cse.Application_Record_Type__c == CommonConstants.APP_RT_COMM_C){
            roleWrapper rwr_prim = new roleWrapper( new Role__c(RecordTypeId = roleRTMap.get(CommonConstants.ROLE_RT_APP_COMP), Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT), 
                                               accRTMap.get(CommonConstants.ACC_RT_L_BA) ); //Set default Values for Role
            roleWrlist.add(rwr_prim);
        }
        else if (cse.Application_Record_Type__c == CommonConstants.APP_RT_COMM_TC){
            roleWrapper rwr_prim = new roleWrapper( new Role__c(RecordTypeId = roleRTMap.get(CommonConstants.ROLE_RT_TRU_COMP), Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT), 
                                               accRTMap.get(CommonConstants.ACC_RT_L_BA) ); //Set default Values for Role
            rwr_prim.roleCtr = roleWrlist.size() + 1;
            roleWrlist.add(rwr_prim);
        }
        else if (cse.Application_Record_Type__c == CommonConstants.APP_RT_COMM_TI){
            roleWrapper rwr_prim = new roleWrapper( new Role__c(RecordTypeId = roleRTMap.get(CommonConstants.ROLE_RT_TRU_INDI), Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT), 
                                               accRTMap.get(CommonConstants.ACC_RT_L_BA) ); //Set default Values for Role
            rwr_prim.roleCtr = roleWrlist.size() + 1;
            roleWrlist.add(rwr_prim);
        }
        else if (cse.Application_Record_Type__c == CommonConstants.APP_RT_COMM_P){
            roleWrapper rwr_prim = new roleWrapper( new Role__c(RecordTypeId = roleRTMap.get(CommonConstants.ROLE_RT_PAR_BUSI), Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT), 
                                               accRTMap.get(CommonConstants.ACC_RT_L_BA) ); //Set default Values for Role
            rwr_prim.roleCtr = roleWrlist.size() + 1;
            roleWrlist.add(rwr_prim);
        }
        else if (cse.Application_Record_Type__c ==  CommonConstants.APP_RT_COMM_ST){
            roleWrapper rwr_prim = new roleWrapper( new Role__c(RecordTypeId = roleRTMap.get(CommonConstants.ROLE_RT_APP_SOLE), Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT), 
                                               accRTMap.get(CommonConstants.ACC_RT_L_BA) ); //Set default Values for Role
            rwr_prim.roleCtr = roleWrlist.size() + 1;
            roleWrlist.add(rwr_prim);
        }
    }

    //Method executed when Back button got clicked
    public PageReference Pback(){
        curPage--;
        hasErrors = false;
        return null;
    }

    
    //Helper method executed on Account Record Type select and when clicking New Account button
    public void checkAccIfPerson(){
        for (roleWrapper rwr : roleWrlist){
            if (rwr.roleCtr == curRoleCtr){
                if (rwr.acc.RecordTypeId == accRTMap.get(CommonConstants.PACC_RT_I)) rwr.isPerson = true;
                else rwr.isPerson = false;
                break;
            }
        }
    }

    //Adds new row in page for creating Role
    /*public PageReference addNewRole(){
        roleWrapper rwr = new roleWrapper( new Role__c(RecordTypeId = roleWrlist[roleWrlist.size() - 1].role.RecordTypeId), 
                                           accRTMap.get(CommonConstants.PACC_RT_I));
        rwr.roleCtr = roleWrlist.size() + 1;
        roleWrlist.add(rwr);
        return null;
    } */

    //Helper method for validating inputted records before Saving
    private boolean validateRoles(){
        boolean passed = true;
        integer primcontCtr, primappCtr, consloanindCtr;
        primcontCtr = primappCtr = consloanindCtr = 0;
        for (roleWrapper rwr : roleWrlist){
            //Validating Primary Contact
            if (rwr.role.Primary_Contact_for_Application__c) primcontCtr++;
            if (primcontCtr > 1){
                passed = false; hasErrors = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, CommonConstants.EASYCASE_ERR_PC1));
                break;
            }
            //Validating Primary Applicant Role
            if (rwr.role.Role_Type__c == CommonConstants.EASYCASE_VAL_ROLE_PA) primappCtr++;
            if (primappCtr > 1){
                passed = false; hasErrors = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, CommonConstants.EASYCASE_ERR_PA1));
                break;
            }
            //Validating Consumer Loan - Individual Role RT
            if (roleRTNameMap.get(rwr.role.RecordTypeId) == CommonConstants.ROLE_RT_APP_INDI) consloanindCtr++;
            if (consloanindCtr > 1){
                passed = false; hasErrors = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, CommonConstants.EASYCASE_ERR_CLI_RT));
                break;
            }
        }
        if (primcontCtr == 0){
            passed = false; hasErrors = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, CommonConstants.EASYCASE_ERR_PC2));
        }
        if (primappCtr == 0){
            passed = false; hasErrors = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, CommonConstants.EASYCASE_ERR_PA2));
        }
        return passed;
    }

    //Helper method for saving inputted Case, Accounts and Roles
    public pageReference saveEasyCase(){
        Savepoint sp = Database.setSavepoint();
        map <integer,account> accMap = new map <integer,account>();
        list <Role__c> rolelist = new list <Role__c>();
        try{
            hasErrors = false;
            insert cse; //Create Case
            for (roleWrapper rwr : roleWrlist){ 
                if (rwr.isCreateAcc){
                    if (!accMap.containskey(rwr.roleCtr)){
                        Account acc = new Account();
                        if (rwr.isPerson){ //Person Account
                            acc = new Account(
                                RecordTypeId = rwr.acc.RecordTypeId,
                                FirstName = rwr.con.FirstName,
                                MiddleName = rwr.con.MiddleName,
                                Salutation = rwr.con.Salutation,
                                LastName = rwr.con.LastName,
                                PersonBirthdate = rwr.con.Birthdate,
                                Gender__c = rwr.con.Gender__c
                            );
                        }
                        else { //Business Account
                            acc = new Account(
                                RecordTypeId = rwr.acc.RecordTypeId,
                                Name = rwr.acc.Name,
                                Phone = rwr.acc.Phone
                            );
                        }
                        accMap.put(rwr.roleCtr, acc);
                    }
                }
            } 
            insert accMap.values(); //Create Accounts

            for (roleWrapper rwr : roleWrlist){ 
                Role__c role = new Role__c(
                        RecordTypeId = rwr.role.RecordTypeId,
                        Case__c = cse.Id, //assign created Case
                        Role_Type__c = rwr.role.Role_Type__c,
                        Primary_Contact_for_Application__c = rwr.role.Primary_Contact_for_Application__c
                    );
                if (rwr.isCreateAcc){ 

                    role.Account__c = accMap.get(rwr.roleCtr).Id; //Assign created Account in Role
                }
                else role.Account__c = rwr.role.Account__c;
                rolelist.add(role);
            }
            insert rolelist; //Create Roles

            return new Pagereference('/'+cse.Id);
        }
        catch (DMLException de){
            hasErrors = true;
            cse = cse.clone(false); //copy Case data without ID when errors occurred 
            Database.rollback( sp ); 
            ApexPages.addMessages(de);
        }
        return null;
    }

    //Wrapper class of Role
    public class roleWrapper{
        public Role__c role {get;set;}
        public Account acc {get;set;} //represents fields for Business/Person Accounts
        public Contact con {get;set;} //represents other fields for Person Accounts
        public boolean isCreateAcc {get;set;}
        public boolean isPerson {get;set;}
        public integer roleCtr {get;set;}
        public string accTxt {get;set;} //represents new Account input for Role
        public roleWrapper(Role__c r,id defAccRT){
            role = r;
            acc = new Account(RecordTypeId = defAccRT);
            con = new Contact();
            accTxt = '';
            isCreateAcc = false; isPerson = true;
            roleCtr = 1;
        }
    }

    //Class Constructor
    public EasyCaseController(){

        caseRTMap = new map <string, id>();
        caseRTNameMap = new map <id, string>();
        roleRTMap = new map <string, id>();
        roleRTNameMap = new map <id, string>();
        accRTMap = new map <string, id>();
        curRoleCtr = curPage = 1; 
        roleWrlist = new list <roleWrapper>();
        hasErrors = false;

        cse = new Case ( Status = CommonConstants.CASE_STATUS_NEW, Stage__c = CommonConstants.CASE_STAGE_OPEN); //, Loan_Type__c = CommonConstants.EASYCASE_DEF_LOANTYPE); //Set default Values for Case
        roleWrlist = new list <roleWrapper>();

        /* roleWrapper rwr = new roleWrapper( new Role__c(Primary_Contact_for_Application__c = true), accRTMap.get(CommonConstants.PACC_RT_I) ); //Set default Values for Role
        roleWrlist.add(rwr); */
    }

}
/*
	@Description: schedulable class for pushing updates from Pending Actions
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	9/25/2017 - Created
*/
global class PendingAction_PushUpdates_Schedulable implements Schedulable {

	global void execute(SchedulableContext sc) {
		PendingAction_UpdateNVMFields_Batch pab = new PendingAction_UpdateNVMFields_Batch();
		Database.executeBatch(pab);
	}
	
}
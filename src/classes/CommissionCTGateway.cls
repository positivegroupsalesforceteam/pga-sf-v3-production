/**
 * @File Name          : CommissionCTGateway.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 18/07/2019, 2:05:32 pm
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    18/07/2019, 1:38:20 pm   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
 * 1.1	  5/08/2019,  1:53:00 pm   rexie.david@positivelendingsolutions.com.au        tasks/25563890 
**/
public without sharing class CommissionCTGateway implements Queueable {

    public list <Case> parentCaselist; 
	public CommissionCTGateway(list <Case> parentCaselist){
		this.parentCaselist = parentCaselist;
	}

    public void execute(QueueableContext context){
		system.debug('@@parentCaselist:'+parentCaselist);
        list <CommissionCT__c> delCommCTlist = [Select Id From CommissionCT__c Where Case__c = : parentCaselist AND Case__r.Stage__c = 'Lost' ];
		if (delCommCTlist.size() > 0){ 
			delete delCommCTlist;
		}

		//4.3 5/08/2019 RDAVID - tasks/25563890
		Set<String> caseNumberWon = new Set<String>();
		List<Commission_Line_Items__c> cliList = new List<Commission_Line_Items__c>();
		for(Case cs : parentCaselist){
			if(cs.Stage__c == 'Won'){
				caseNumberWon.add(cs.CaseNumber);
			}
		}
		if(caseNumberWon.size() > 0){
			for(Commission_Line_Items__c clitem : [SELECT Id,CommissionCT_Parent_Won__c FROM Commission_Line_Items__c WHERE Case_Number__c IN: caseNumberWon AND CommissionCT_Parent_Won__c = FALSE]){
				clitem.CommissionCT_Parent_Won__c = TRUE;
				cliList.add(clitem);
			}
		}
		if(cliList.size() > 0){
			Database.update(cliList);
		}
		//4.3 5/08/2019 RDAVID - tasks/25563890
    }

}
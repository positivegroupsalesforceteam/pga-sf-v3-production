/** 
* @FileName: RoleHandler
* @Description: Trigger Handler for the Role__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/15 RDAVID Created Class
* 1.1 2/20 RDAVID Added Method Trigger toggle for object
* 1.2 3/28 JBACULOD 
* 1.3 3/28/2018 RDAVID Worked on SD-30 SF - NM - Propagate Role to Account Field Values 
* 1.4 4/05/2018 RDAVID Worked on SD-46 SF - NM - Toggle Role field (Primary Applicant)
* 1.5 4/06/2018 RDAVID Worked on SD-46 SF - NM - Populate Primary Applicant 
* 2.0 4/26/2018 RDAVID Worked on SD-30 Updates (Additional Fields to propagate and RTs)
* 2.1 6/22/2018 RDAVID Added Logic to tick Rolled_Up__c checkbox field.
* 2.2 7/30/2018 JBACULOD Added 
* 3.0 5/09/18 RDAVID: Logic for creation Case Touch Point (Junction record to link Cases and Bizible Touch Points.)
* 4.0 19/09/18 RDAVID: issuefix/RoleTrigger -  Fix for SD-30 Role to Account Propagation
* 4.1 21/09/18 RDAVID: issuefix/RoleTrigger -  Role Trigger Optimization RoleGateway 86%, RoleHandler 89%, RoleTrigger 100% - Code coverage before optimization.
* 4.1 17/10/18 RDAVID: Optimization
* 4.2 19/03/19 JBACULOD: Added DML Retry and FOR UPDATE due to Unable to Lock Row issue
* 4.3 30/09/19 RDAVID extra/task26227377-GlobalPicklist-IncomeVerificationMethod : Business_Income_Verification_Method__c put this back after promoting the field to global picklist
**/ 

public without sharing class RoleHandler implements ITrigger {	

	private Set<Id> applicationIds = new Set<Id>(); //Parent Application Ids

	//SD-59 SF - NM - Role Rollup in Role/Application
	private static Map<String,List<String>> roleToApplicationMapping = new Map<String,List<String>>(); //Role to Role Field Mapping
	private static Map<Id,Schema.RecordTypeInfo> roleRTMapById = Schema.SObjectType.Role__c.getRecordTypeInfosById();

	//SD-29 SF - NM - Auto Populate Case/Application in Role
	private Map<Id,Id> caseAppInRoleMap = new map <Id,Id>(); //Stores Case and Application IDs
	
	//SD-30 SF - NM - Propagate Role to Account Field Values
	private Map<Id,SObject> accountToUpdateMap = new Map<Id,SObject>(); //Person Accounts and Business Accounts to Update
	private static Set<Id> openCaseIdSet = new Set<Id>(); //Role Open Cases
	private static Map<String,Role_to_Account_Mapping__mdt> roleToAccountCMDMap = new Map<String,Role_to_Account_Mapping__mdt>(); //Retrieve Role_to_Account_Mapping__mdt records from Custom Metadata

	// SD-46 Update Primary_Applicant__c field in Application
	private Map<Id,Application__c> appToUpdateMap = new Map<Id,Application__c>(); //PArent Application to update

	//SD-?? Role to Application
    public static List<Role_to_Application_Mapping__mdt> roleToAppMappingList= [SELECT Id,FO_Type__c,Active__c,Role_Record_Types__c,Role_Field__c,Application_Record_Types__c,Application_Field__c FROM Role_to_Application_Mapping__mdt WHERE Active__c = TRUE];
	public static Map<Id,Schema.RecordTypeInfo> appRTMapById = Schema.SObjectType.Application__c.getRecordTypeInfosById();
	public Map<Id,Id> appAndRTMap = new Map<Id,Id>(); 
	public static Map<String,String> roleRTToAppRT = new Map<String,String>();

	Map<String, Schema.SObjectField> roleFields = Schema.SObjectType.Role__c.fields.getMap(); 
	Map<String, Schema.SObjectField> accountFields = Schema.SObjectType.Account.fields.getMap(); 

	private List<Role__c> newRoleList = new List<Role__c>();
	private Set<String> closeStatusSet = new Set<String>{'Closed - First Call' ,'Closed', 'Settled', 'Lost'};
	
	private static String roleRollupFields;
	private static String appRollupFields;
	
	//private static Map<String,List<String>> applicationRoleRollupMapping = new Map<String,List<String>>();//getChildToParentMapping
	// Constructor
	public RoleHandler(){

		system.debug('@@roleToAppMappingList:'+roleToAppMappingList);
		
		if (TriggerFactory.trigset.Enable_Role_To_Account_Propagation__c) 
			roleToAccountCMDMap = (!roleToAccountCMDMap.isEmpty()) ? roleToAccountCMDMap : RoleGateway.getRoleToAccountCMDMap();

		if (TriggerFactory.trigset.Enable_Role_Rollup_to_Application__c){ //} && roleRollupFields == NULL){
			/* String roleToAppMappingQueryStr = 'SELECT Id,FO_Type__c,Active__c,Role_Record_Types__c,Role_Field__c,Application_Record_Types__c,Application_Field__c FROM Role_to_Application_Mapping__mdt WHERE Active__c = TRUE';
			
			System.debug('@@@updatedIncome = '+updatedIncome); System.debug('@@@updatedExpense = '+updatedExpense); System.debug('@@@updatedAsset = '+updatedAsset); System.debug('@@@updatedLiability = '+updatedLiability);

			integer foctr = 0;
			string foCriteriaQueryStr = '';
			if(updatedAsset){ 
				foCriteriaQueryStr += 'FO_Type__c = \'Asset\'';
				foctr++;
			}
			if(updatedLiability){ 
				if (foctr > 0) foCriteriaQueryStr+= ' OR ';
				foCriteriaQueryStr += 'FO_Type__c = \'Liability\'';
				foctr++;
			}
			if(updatedIncome){
				if (foctr > 0) foCriteriaQueryStr+= ' OR ';
				foCriteriaQueryStr += 'FO_Type__c = \'Income\'';
				foctr++;
			}
			if(updatedExpense){ 
				if (foctr > 0) foCriteriaQueryStr+= ' OR ';
				foCriteriaQueryStr += 'FO_Type__c = \'Expense\'';
				foctr++;
			}
			if (foctr > 0){
				foCriteriaQueryStr = ' AND  ( ' + foCriteriaQueryStr + ' )';
				roleToAppMappingQueryStr += foCriteriaQueryStr;
			}
			system.debug('@@roleToAppMappingQueryStr:'+roleToAppMappingQueryStr);
			list <Role_to_Application_Mapping__mdt> mdtlist = Database.query(roleToAppMappingQueryStr);
			system.debug('@@mdtlist:'+mdtlist.size()+' '+mdtlist); */

			for(Role_to_Application_Mapping__mdt mdt :  roleToAppMappingList){
				if(roleRollupFields == NULL) roleRollupFields = ',' + mdt.Role_Field__c;
				else{ 
					if (!roleRollupFields.contains(','+mdt.Role_Field__c)) roleRollupFields += ',' + mdt.Role_Field__c;
				}
	
				if(appRollupFields == NULL) appRollupFields = ',' + mdt.Application_Field__c;
				else{ 
					if (!appRollupFields.contains(','+mdt.Application_Field__c))  appRollupFields += ',' + mdt.Application_Field__c;
				}

				if(!String.isBlank(mdt.FO_Type__c) && !String.isBlank(mdt.Role_Field__c) && !String.isBlank(mdt.Application_Field__c)){
					if(!roleToApplicationMapping.containsKey(mdt.Application_Field__c.toLowerCase())){
						roleToApplicationMapping.put(mdt.Application_Field__c.toLowerCase(),new List<String>{mdt.FO_Type__c+':'+mdt.Role_Record_Types__c+':'+mdt.Role_Field__c+':'+mdt.Application_Record_Types__c+':'+mdt.Application_Field__c});
					}
					else{
						List<String> tempStringList = roleToApplicationMapping.get(mdt.Application_Field__c.toLowerCase());
						tempStringList.add(mdt.FO_Type__c+':'+mdt.Role_Record_Types__c+':'+mdt.Role_Field__c+':'+mdt.Application_Record_Types__c+':'+mdt.Application_Field__c);
						roleToApplicationMapping.put(mdt.Application_Field__c.toLowerCase(),tempStringList);
					}
				}

				if(Trigger.IsInsert && !roleRTToAppRT.containsKey(mdt.Role_Record_Types__c)){
					roleRTToAppRT.put(mdt.Role_Record_Types__c,mdt.Application_Record_Types__c); //Returns a map of RoleRTs(Key) and ApplicationRTs (Value)
				}
			}
		}
		//Initialize value of newRoleList
		newRoleList = (Trigger.IsInsert || Trigger.isUpdate) ? (List<Role__c>) Trigger.new : (Trigger.IsDelete) ? (List<Role__c>) Trigger.old : null;
	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
		System.debug('bulkBefore = '+newRoleList);
		// List<Role__c> newRoleList = (Trigger.IsInsert || Trigger.isUpdate) ? (List<Role__c>) Trigger.new : (Trigger.IsDelete) ? (List<Role__c>) Trigger.old : null; // Main List for new/updated records
		
		Set<Id> appIDs = new Set<Id>(); //Set for Role Application Ids
		Set<Id> caseIDs = new Set<Id>(); //Set for Role Case Ids

		//RDAVID Optimization: Main Loop for New / Updated records - As much as possible only one for loop iteration for the Trigger.new. 
		for(Role__c role : newRoleList){
			if(Trigger.IsInsert || Trigger.isUpdate || Trigger.isDelete){
				if(role.Application__c != NULL) appIDs.add(role.Application__c);
				else if(role.Case__c != NULL) caseIDs.add(role.Case__c);
			}
		}

		if(Trigger.IsInsert || Trigger.isUpdate){
			if(caseIDs.size() > 0){
				//Query for Role Case - As much as possible put all logic inside this block and avoid another Case query
				for(Case cse : [SELECT Id, Application_Name__c, Application_Name__r.RecordTypeId, (SELECT Id, Role_Type__c FROM Roles__r WHERE Role_Type__c =: CommonConstants.ROLE_PRIMARY_APPLICANT) FROM Case WHERE Id In: caseIDs]){
					//Logic for Insert Only here
					if(Trigger.IsInsert) {
						if(cse.Application_Name__c != NULL) appAndRTMap.put(cse.Application_Name__c,cse.Application_Name__r.RecordTypeId);
					}
					//Logic for Insert OR Update here
					if(Trigger.IsInsert || Trigger.IsUpdate){
						if (TriggerFactory.trigset.Enable_Role_Auto_Populate_Case_App__c && cse.Application_Name__c != null) caseAppInRoleMap.put(cse.Id, cse.Application_Name__c);
					}
				}
			}
			if(appIDs.size() > 0){
				//Query for Role Application - As much as possible put all logic inside this block and avoid another Application query
				for(Application__c app : [SELECT Id, Case__c, RecordTypeId, (SELECT Id, Role_Type__c FROM Roles__r WHERE Role_Type__c =: CommonConstants.ROLE_PRIMARY_APPLICANT) FROM Application__c WHERE Id In: appIDs]){
					//Logic for Insert Only here
					if(Trigger.IsInsert){
						appAndRTMap.put(app.Id,app.RecordTypeId);
					} 
					//Logic for Insert OR Update here
					if(Trigger.IsInsert || Trigger.IsUpdate){
						if (TriggerFactory.trigset.Enable_Role_Auto_Populate_Case_App__c && app.Case__c != null) caseAppInRoleMap.put(app.Id, app.Case__c);
					}
				}
			}
		}
		// to be added if role is deleted, update application
		// if(Trigger.isDelete){
		// 	if(appIDs.size() > 0){
		// 		Boolean updateApp = false;
		// 		Application__c appCopy;
		// 		for(Application__c app : [SELECT Id, Case__c, Number_of_Applicants__c, RecordTypeId, RecordType.Name, Spouse_Supporting_Servicing_Email_MC__c, Spouse_Supporting_Servicing_Phone_MC__c, Non_Borrowing_Spouse__c, Spouse_Supporting_Servicing_Name_MC__c, Co_Applicant_Name_MC__c, Co_Applicant_Email_MC__c, Co_Applicant_Phone_MC__c, Primary_Contact_Preferred_Call_Time__c, Inconsistent_Application_Details__c, Primary_Contact_for_Application__c, Primary_Contact_Email_MC__c, Primary_Contact_Name_MC__c, Primary_Applicant__c, Primary_Contact_Phone_MC__c, (SELECT Id, RecordTypeId, RecordType.Name, Role_Type__c, Location__c, TrustPilot_Unique_Link__c, Account__c, Account_Name__c,Account__r.Name, Account__r.PersonEmail, Account__r.PersonMobilePhone, Preferred_Call_Time__c,Primary_Contact_for_Application__c, Associated_Relationships__c,Email__c,Mobile_Phone__c, Business_Total_Revenue__c, Business_Total_Depreciation__c, Profit_Before_Tax_Annual__c, Reporting_Period__c, Cash_in_Bank__c FROM Roles__r WHERE Id IN :newRoleList) FROM Application__c WHERE Id In: appIDs]){

		// 			System.debug('roles size>>>>>>> '+app.Roles__r);
					
		// 			if (TriggerFactory.trigset.Enable_Role_Rollup_to_Application__c){
		// 				appCopy = new Application__c(Non_Borrowing_Spouse__c = app.Non_Borrowing_Spouse__c, 
		// 										Primary_Contact_for_Application__c = app.Primary_Contact_for_Application__c,
		// 										Primary_Contact_Preferred_Call_Time__c = app.Primary_Contact_Preferred_Call_Time__c,
		// 										Inconsistent_Application_Details__c = app.Inconsistent_Application_Details__c,
		// 										Number_of_Applicants__c = app.Number_of_Applicants__c);

		// 				RoleGateway.updateApplication(app,'Delete');
						
		// 				system.debug('@@App w/ updates:'+System.equals(app,appCopy));
		// 				if (!System.equals(app,appCopy)){ 
		// 					updateApp = true;
		// 				}


		// 				System.debug('updateApp == ' + updateApp);

		// 				if(updateApp) {
		// 					appToUpdateMap.put(app.Id,app);
		// 				}
		// 			}
		// 		}
		// 	}
		// }
	}
	
	public void bulkAfter(){
		//Without Bulk After 263.09kb, With 4.11mb
		System.debug('bulkAfter = '+newRoleList);
		Map<Id,Role__c> oldRoleMap = (Trigger.isUpdate)? (Map<Id,Role__c>)Trigger.oldMap : null;
		
		//RDAVID Optimization: Main Loop for New / Updated records - As much as possible only one for loop iteration for the Trigger.new
		for(Role__c role : newRoleList){
			if(Trigger.IsInsert || Trigger.isUpdate){
				if(role.Application__c != NULL) 
					applicationIds.add(role.Application__c);
				//SD-30
				if (TriggerFactory.trigset.Enable_Role_To_Account_Propagation__c){
					if(role.Application__c != NULL){
						if(!closeStatusSet.contains(role.h_Case_Status__c)){
							openCaseIdSet.add(role.Case__c); //= RoleGateway.getOpenCaseIds(trigger.new);
						}
					}
				}
			}

			if(Trigger.IsDelete){
				
				if(role.Application__c != NULL) {
					applicationIds.add(role.Application__c);
					
				}
			}
		}
		
		if(applicationIds.size() > 0){
			
			string selectApplication = 'SELECT Id,Number_of_Applicants__c, Non_Borrowing_Spouse__c, RecordTypeId, RecordType.Name, Primary_Contact_Preferred_Call_Time__c, Primary_Contact_for_Application__c, Inconsistent_Application_Details__c, Prim_Applicant_TrustPilot_Unique_Link__c, Co_Applicant_TrustPilot_Unique_Link__c, Primary_Contact_Name_MC__c, Spouse_Supporting_Servicing_Name_MC__c, Co_Applicant_Name_MC__c, Primary_Contact_Location_MC__c, Associated_Relationships__c, Primary_Contact_Email_MC__c, Primary_Contact_Phone_MC__c, Co_Applicant_Email_MC__c, Co_Applicant_Phone_MC__c, Spouse_Supporting_Servicing_Email_MC__c, Spouse_Supporting_Servicing_Phone_MC__c, Reporting_Period__c, Total_Depreciation__c, Total_Revenue__c, Profit_Before_Tax_Annual__c, Income_Verification_Method__c';
			string innerSelectRole = ',(SELECT Id, RecordTypeId, RecordType.Name, Role_Type__c, Location__c, TrustPilot_Unique_Link__c, Account__c, Account_Name__c,Account__r.Name, Account__r.PersonEmail, Account__r.PersonMobilePhone, Preferred_Call_Time__c,Primary_Contact_for_Application__c, Associated_Relationships__c,Email__c,Mobile_Phone__c, Business_Total_Revenue__c, Business_Total_Depreciation__c, Profit_Before_Tax_Annual__c, Reporting_Period__c'; //Business_Income_Verification_Method__c * 4.3 30/09/19 RDAVID extra/task26227377-GlobalPicklist-IncomeVerificationMethod : Business_Income_Verification_Method__c put this back after promoting the field to global picklist
			string innerFromRole = ' FROM Roles__r ';
			string innerWhereRole = 'WHERE RecordTypeId != NULL)';
			string fromApplication = ' FROM Application__c WHERE Id IN: applicationIds';
			Application__c appCopy;

			if (TriggerFactory.trigset.Enable_Role_Rollup_to_Application__c && roleRollupFields != NULL && appRollupFields != NULL){ //RDAVID 17/10/18 Added Application/Role Rollup fields in query 
				innerSelectRole += roleRollupFields;
				selectApplication += appRollupFields;
			}
			
			system.debug('@@appquerystring:'+selectApplication+innerSelectRole+innerFromRole+innerWhereRole+fromApplication);
			//Query Loop for Role Application - As much as possible 1 query in Application.
			for(Application__c app : Database.query(selectApplication+innerSelectRole+innerFromRole+innerWhereRole+fromApplication)){
				
				Boolean updateApp = false;
				//Rollup Role To Application Logic
				if (TriggerFactory.trigset.Enable_Role_Rollup_to_Application__c){
					appCopy = new Application__c(Non_Borrowing_Spouse__c = app.Non_Borrowing_Spouse__c, 
												Primary_Contact_for_Application__c = app.Primary_Contact_for_Application__c,
												Primary_Contact_Preferred_Call_Time__c = app.Primary_Contact_Preferred_Call_Time__c,
												Inconsistent_Application_Details__c = app.Inconsistent_Application_Details__c,
												Number_of_Applicants__c = app.Number_of_Applicants__c);
				
					RoleGateway.updateApplication(app,'Update'); //Instance of Role Parent Application				

					/*if(appCopy.Non_Borrowing_Spouse__c != app.Non_Borrowing_Spouse__c 
					|| appCopy.Primary_Contact_for_Application__c != app.Primary_Contact_for_Application__c
					|| appCopy.Primary_Contact_Preferred_Call_Time__c != app.Primary_Contact_Preferred_Call_Time__c
					|| appCopy.Inconsistent_Application_Details__c != app.Inconsistent_Application_Details__c
					|| appCopy.Number_of_Applicants__c != app.Number_of_Applicants__c) */ 
					system.debug('@@App w/ updates:'+System.equals(app,appCopy));
					if (!System.equals(app,appCopy)){ 
						updateApp = true;
					}		

					RollupUtility.rollupRoleToApplication(app,roleToApplicationMapping);		
					System.debug('app == ' + app);
					if(RollupUtility.updateAppRollup) { 
						System.debug('RollupUtility.updateAppRollup == ' + RollupUtility.updateAppRollup);
						updateApp = true;
						RollupUtility.updateAppRollup = false;
					}
				}
				System.debug('updateApp == ' + updateApp);

				if(updateApp) {
					appToUpdateMap.put(app.Id,app);
				}
			}
		}					

		System.debug('ROLE bulkAfter Queries :' + Limits.getQueries() + ' / '+Limits.getLimitQueries());
		System.debug('ROLE bulkAfter Rows Queried :' + Limits.getDmlRows() + ' / '+Limits.getLimitDmlRows());
		System.debug('ROLE bulkAfter Trigger DML : ' +  Limits.getDmlStatements() + ' / '+Limits.getLimitDmlStatements());
	}
		
	public void beforeInsert(SObject so){

		Role__c role = (Role__c)so;
		
		if (TriggerFactory.trigset.Enable_Role_Auto_Populate_Case_App__c){
			RoleGateway.linkCaseAppinRole(null,role,caseAppInRoleMap);
		}

		Boolean validRT = false;
		for(String roleRTSet : roleRTToAppRT.keySet()){
		    if(roleRTSet.containsIgnoreCase(roleRTMapById.get(role.RecordTypeId).getName())){
				String appValidRTs = roleRTToAppRT.get(roleRTSet);
				if(appAndRTMap.containsKey(role.Application__c) && appValidRTs.containsIgnoreCase(appRTMapById.get(appAndRTMap.get(role.Application__c)).getName())){
					validRT = true;
					break;
				}
		    }
		}
		if(validRT){
		    System.debug('valid RT to Roll Up.');
		    role.Rolled_Up__c = true;
		}
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		Role__c role = (Role__c)so;
		Role__c oldRole = (Role__c)oldSo;
		if (TriggerFactory.trigset.Enable_Role_Auto_Populate_Case_App__c){
			RoleGateway.linkCaseAppinRole(oldRole,role,caseAppInRoleMap);
		}
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}

	// public sObject sObjectRecord = Schema.getGlobalDescribe().get('Account').newSObject();
	// public sObject sObjectRecord = new Account();//Schema.getGlobalDescribe().get('Account').newSObject();

	public void afterInsert(SObject so){
		Role__c role = (Role__c)so;
		// SD-30 - This block of code updates Parent Account of Role record
		if (TriggerFactory.trigset.Enable_Role_To_Account_Propagation__c){
			if(role.Account__c != NULL && (openCaseIdSet != NULL && role.Case__c != NULL && openCaseIdSet.contains(role.Case__c)) || (role.Case__c == NULL) && role.RecordTypeId != NULL){
				sObject sObjectRecord = Schema.getGlobalDescribe().get('Account').newSObject();
				sObjectRecord.put('Id',role.Account__c);
				Boolean updateParent = false;
				for(String roleFieldApiName : roleToAccountCMDMap.keySet()){ //Iterate over metadata records
					if(so.get(roleFieldApiName) != NULL
					&& roleFields.containsKey(roleFieldApiName) //Make sure field in metadata is a field in Role
					&& !String.isBlank(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c) //Make sure Target field is populated from the metadata
					&& accountFields.containsKey(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c.toLowerCase()) //Make sure field in metadata is a field in Account
					&& !String.isBlank(roleToAccountCMDMap.get(roleFieldApiName).Role_RecordTypes__c) //Make sure Target Role RTs are populated in metadata 
					&& roleToAccountCMDMap.get(roleFieldApiName).Role_RecordTypes__c.containsIgnoreCase(roleRTMapById.get(role.RecordTypeId).Name)){ //Make source field is populated
						Boolean isAccountFieldValid = RoleGateway.isAccountFieldValid(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c.toLowerCase(), (Boolean)so.get('t_Is_Person_Account__c'));
						if(isAccountFieldValid){//handles logic when Role field is DateTime and Account field is Date
							if(String.valueOf(roleFields.get(roleFieldApiName).getDescribe().getType()) == 'DATETIME' && String.valueOf(accountFields.get(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c.toLowerCase()).getDescribe().getType()) == 'DATE'){
								DateTime dtime = (Datetime)so.get(roleFieldApiName);
								Date dt = date.newinstance(dtime.year(), dtime.month(), dtime.day()); //only get date of datetime
								sObjectRecord.put(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c,dt);
								updateParent = true;
							}
							else{ 
								sObjectRecord.put(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c,so.get(roleFieldApiName));
								updateParent = true;
							}
						}
					}
				}
				System.debug('asdai '+ updateParent + ' - ' +sObjectRecord);
				if(updateParent) accountToUpdateMap.put(String.valueOf(sObjectRecord.get('Id')),sObjectRecord);
			}
		} 
	}
	
	public void afterUpdate(SObject oldSo, SObject so){
		Role__c role = (Role__c)so;
		Role__c oldRole = (Role__c)oldSo;
		// SD-30 - This block of code updates Parent Account of Role record
		if (TriggerFactory.trigset.Enable_Role_To_Account_Propagation__c){
			if(role.Account__c != NULL && (openCaseIdSet != NULL && role.Case__c != NULL && openCaseIdSet.contains(role.Case__c)) || (role.Case__c == NULL) && role.RecordTypeId != NULL){
				System.debug('describhere');
				sObject sObjectRecord = Schema.getGlobalDescribe().get('Account').newSObject();
				sObjectRecord.put('Id',role.Account__c);
				Boolean updateParent = false;

				for(String roleFieldApiName : roleToAccountCMDMap.keySet()){ //Iterate over metadata records
					// Boolean personField = roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c.containsIgnoreCase('person') ? true : false;
					if(so.get(roleFieldApiName) != oldSo.get(roleFieldApiName)
					&& roleFields.containsKey(roleFieldApiName) //Make sure field in metadata is a field in Role
					&& !String.isBlank(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c) //Make sure Target field is populated from the metadata
					&& accountFields.containsKey(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c.toLowerCase()) //Make sure field in metadata is a field in Account
					&& !String.isBlank(roleToAccountCMDMap.get(roleFieldApiName).Role_RecordTypes__c) //Make sure Target Role RTs are populated in metadata 
					&& roleToAccountCMDMap.get(roleFieldApiName).Role_RecordTypes__c.containsIgnoreCase(roleRTMapById.get(role.RecordTypeId).Name)){ //Make source field is populated
						Boolean isAccountFieldValid = RoleGateway.isAccountFieldValid(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c.toLowerCase(), (Boolean)so.get('t_Is_Person_Account__c'));
						if(isAccountFieldValid){//handles logic when Role field is DateTime and Account field is Date
							if(String.valueOf(roleFields.get(roleFieldApiName).getDescribe().getType()) == 'DATETIME' && String.valueOf(accountFields.get(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c.toLowerCase()).getDescribe().getType()) == 'DATE'){
								if(so.get(roleFieldApiName) != NULL) {
									DateTime dtime = (Datetime)so.get(roleFieldApiName);
									Date dt = date.newinstance(dtime.year(), dtime.month(), dtime.day()); //only get date of datetime
									sObjectRecord.put(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c,dt);
									updateParent = true;								
								}
							}
							else{ 
								sObjectRecord.put(roleToAccountCMDMap.get(roleFieldApiName).Account_Fields__c,so.get(roleFieldApiName));
								updateParent = true;	
							}
						}
					}
				}
				System.debug('asdau '+ updateParent + ' - ' +sObjectRecord);
				if(updateParent) accountToUpdateMap.put(String.valueOf(sObjectRecord.get('Id')),sObjectRecord);
			}
		} 
	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		//SD-30 Update role Parent Accounts SD-30 SF - NM - Propagate Role to Account Field Values - 
		if(!accountToUpdateMap.isEmpty()){
			try{
				list <Account> lockAccsForUpdate = [Select Id From Account Where Id in : accountToUpdateMap.keySet() FOR UPDATE]; //JBACU 6/7/19
				System.debug('**********UPDATE -> accountToUpdateMap '+accountToUpdateMap);
				Database.update(accountToUpdateMap.values());
			}
			catch (Exception e){
				Database.update(accountToUpdateMap.values());
			}
		}

		//SD-46 Update Primary_Applicant__c field in Application , Update parent Role Rollup Fields
		//SD-?? Role To Application copy fields
		if(!appToUpdateMap.isEmpty()){
			try{
				list <Application__c> lockAppsForUpdate = [Select Id From Application__c Where Id in : appToUpdateMap.keySet() FOR UPDATE]; //JBACU 7/06/19
				System.debug('**********UPDATE -> appToUpdateMap '+appToUpdateMap.values());
				Database.update(appToUpdateMap.values());
			}
			catch (Exception e){
				Database.update(appToUpdateMap.values());
			}
		}
		System.debug('Object Role -START-');
		System.debug('Role Trigger Queries :' + Limits.getQueries() + ' / '+Limits.getLimitQueries());
		System.debug('Role Trigger Rows Queried :' + Limits.getDmlRows() + ' / '+Limits.getLimitDmlRows());
		System.debug('Role Trigger DML : ' +  Limits.getDmlStatements() + ' / '+Limits.getLimitDmlStatements());
		System.debug('Object Role -END-');
	}
}
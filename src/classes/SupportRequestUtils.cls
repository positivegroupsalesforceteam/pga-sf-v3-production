/**
 * @File Name          : SupportRequestUtils.cls
 * @Description        : 
 * @Author             : Rexie David (rexie.david@positivelendingsolutions.com.au)
 * @Group              : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/08/2019                Rexie David (rexie.david@positivelendingsolutions.com.au)     Initial Version
**/

global class SupportRequestUtils {

    public final static String errMsgStageInvalid = 'Application does not meet the criteria to create any support request. Please contact your System Administrator.';
    // public final static String errMsgStageInvalid = 'Application not valid for submission. Please make sure the Case has an Application and Lender.';
    public final static String errMsgRegMonPayReqIncAKF = 'Regular Monthly Payment inc AKF is required';
    public final static String errMsgComSurGreZer = 'Combined Surplus must be greater than 0.';
    public final static String errMsgSalTypReq = 'Sale Type is required.';
    public final static String errMsgCaseRT = 'Case recordtype is not supported on any Support Request.';
    public static List<Support_Request_Creation_Setting__mdt> srcsList = new List<Support_Request_Creation_Setting__mdt>();

    webservice static String canCreateSR (Id cse){
        System.debug('cID '+cse);
        Case cseRec = [SELECT Id, Loan_Product__c, Sale_Type__c, RecordType.Name, OwnerId,Lender1__c, Regular_Monthly_Payment__c, Combined_Surplus__c, Owner.Name,Lender1__r.Name, Application_Name__r.RecordTypeId, Status, Stage__c FROM Case WHERE Id =: cse];        
        srcsList = [SELECT Active__c,Case_Recordtypes__c,Case_Stage__c,SR_Recordtypes__c,SR_Subtypes__c FROM Support_Request_Creation_Setting__mdt WHERE Active__c = TRUE AND Case_Stage__c =: cseRec.Stage__c];
        String returnAccess;
        if(srcsList.size()>0){
            System.debug('line 26');
            System.debug('accesstype = '+checkAccessType(cseRec));
            returnAccess = checkAccessType(cseRec);
        }
        else{
            return 'false:'+errMsgStageInvalid;
        }
        return returnAccess;
	}

    // These are old validations from JS Button.
    public static String checkAccessType (Case cse){
        Boolean isValid = true;
        String errMsg = '';
        //Lender check
        if(cse.Lender1__c == NULL || cse.Application_Name__r.RecordTypeId == NULL || !srcsList[0].Case_Recordtypes__c.ContainsIgnoreCase(cse.RecordType.Name)){
            System.debug('line 35');
            //return 'false:'+errMsgStageInvalid;
            isValid = false;
            errMsg = 'false:'+errMsgStageInvalid;
        }
        // Consumer
        else if(cse.RecordType.Name.ContainsIgnoreCase('consumer')){
            System.debug('line 43');
            // Check Regular_Monthly_Payment__c
            if(cse.Regular_Monthly_Payment__c == null){
                System.debug('line 45');
                // return 'false:'+errMsgRegMonPayReqIncAKF; 
                isValid = false;
                errMsg = 'false:'+errMsgRegMonPayReqIncAKF;
            }
            else{
                System.debug('line 49');
                // Check Combined_Surplus__c
                if(cse.Combined_Surplus__c <= 0 || cse.Combined_Surplus__c == null){
                    System.debug('line 51');
                    // return 'false:'+errMsgComSurGreZer; 
                    isValid = false;
                    errMsg = 'false:'+errMsgComSurGreZer;
                }
                else{
                    System.debug('line 55');
                    // Asset Check
                    if(cse.RecordType.Name.ContainsIgnoreCase('Asset')){
                        System.debug('line 57');
                        // Check Sale Type on Asset
                        if (cse.Sale_Type__c == null){
                            System.debug('line 59');
                            // return 'false:'+errMsgSalTypReq; 
                            isValid = false;
                            errMsg = 'false:'+errMsgSalTypReq;
                        }
                    }
                }
            }       
        }
        // Commercial
        else if(cse.RecordType.Name.ContainsIgnoreCase('commercial')){
            System.debug('line 67');
            // Asset Check
            if(cse.RecordType.Name.ContainsIgnoreCase('Asset')){
                System.debug('line 69');
                // Check Sale Type on Asset
                if (cse.Sale_Type__c == null){
                    System.debug('line 71');
                    // return 'false:'+errMsgSalTypReq; 
                    isValid = false;
                    errMsg = 'false:'+errMsgSalTypReq;
                }
            }
        }

        if(isValid){
            return 'true';
        }else{
            return errMsg;
        }
    }
}
/**
 * @File Name          : Box_LeadAutoCreateCCTest.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 07/10/2019, 3:12:49 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    06/10/2019   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
@isTest
public class Box_LeadAutoCreateCCTest {
    @testsetup static void setup(){
        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = Trigger_Settings1__c.getOrgDefaults();
        trigSet.Enable_Lead_Box_Auto_Create__c = true;
        upsert trigSet Trigger_Settings1__c.Id;
	}

    static testmethod void testBoxCreateFolder(){
		Test.startTest();
            Id rtId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Nodifi Partner').getRecordTypeId();
			Lead curLead = new Lead(FirstName='TestF', LastName='TestL', RecordTypeId = rtId);
            insert curLead;
            ApexPages.StandardController st0 = new ApexPages.StandardController(curlead);
            Box_LeadAutoCreateCC boxautoCC = new Box_LeadAutoCreateCC(st0);
            boxautoCC.test_objRecordFolderID = '37272335804';
            boxautoCC.autocreateBoxFolder();
            boxautoCC.test_objRecordFolderID = null;
            boxautoCC.autocreateBoxFolder();
            boxautoCC.test_error = 'already exists';
            boxautoCC.autocreateBoxFolder();
		Test.stopTest();
        system.assertEquals(ApexPages.hasMessages(),true);
	}

}
/** 
* @FileName: CustomHandlingEmailNotification
* @Description: CustomHandlingEmailNotification - Class for Error Handling
* @Copyright: Positivve (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 23/10/2019 
**/ 

public class CustomHandlingEmailNotification {
    public string toMail { get; set;}
    public string ccMail { get; set;}
    public string repMail { get; set;}
    public logWrapper logWrap { get; set;}

    public CustomHandlingEmailNotification(){
        if(toMail == null) toMail = 'rexie.david@positivelendingsolutions.com.au';
        if(ccMail == null) ccMail = 'vincent.go@positivegroup.com.au';
    }
    
	public void sendMail(){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        string[] to = new string[] {toMail};
        string[] cc = new string[] {ccMail};
        
        email.setToAddresses(to);
        if(ccMail!=null && ccMail != '')
	        email.setCcAddresses(cc);
        if(repmail!=null && repmail!= '')
        	email.setInReplyTo(repMail);
        
        email.setSubject(logWrap.type + ' in ' + logWrap.className);
         String numbRecord = (logWrap.numberOfRecords != 0) ? 'Number of records : ' + logWrap.numberOfRecords : '' ;
        email.setHtmlBody('Hello, <br/><br/>See below logs. '+ numbRecord + getLogs(logWrap.logs) + '<br/><br/>Regards<br/> CustomHandlingEmailNotification');
        
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }catch(exception e){
            System.debug('CustomHandlingEmailNotification Error : '+e.getLineNumber() + e.getMessage());
        }
        
        toMail = '';
        ccMail = '';
        repMail = '';
    }

    public string getLogs(Set <String> logs){
        String errLog = '';
        for(String str : logs){
            errLog += '<br/><br/>' + str;
        }
        return errLog;
    }

    public class logWrapper {
        public String type {get; set;} //'Error OR Success'
        public String className {get; set;}
        public Set <String> logs { get; set;}
        public Integer numberOfRecords { get; set;}
        public logWrapper(String logType ,String cName, Set<String> errLogs) {
            type = logType;
            className = cName;
            logs = errLogs;
            numberOfRecords = 0;
        }
    }
}
/* 
** Default implementation of NodeProcessor which performs every job, 
** like converting xml to objects, creating json and making requests.
*/
global virtual class DefaultProcessor implements NodeProcessor {
    String xmlFileName = 'Loankit_SF_Mapping';
    String nodeName;
    String recordId;
    String applicantId;
    String queryObjectName;
    String queryFilterString;
    CustomStringBuilder cStringBuilder = LKIntegration.cStringBuilder;
    
    global String getQueryObjectName() {
        return queryObjectName;
    }
    global void setQueryObjectName(String queryObjName) {
        queryObjectName = queryObjName;
    }
    global void setQueryFilterString(String queryFilter) {
        queryFilterString = queryFilter;
    }
    global String getNodeName() {
        return nodeName;
    }
    global void setNodeName(String name) {
        nodeName = name;
    }
    
    global String getLoankitApplicantId() {
        return applicantId;
    }
    global void setLoankitApplicantId(String strApplicantId) {
        applicantId = strApplicantId;
    }

    /******** default implementation of processing a node and get the response ********/
    /**** Override this method in child class to give different implementation ****/
    public virtual boolean process() {
        cStringBuilder.add('******************************* Processing Started **********************************');
        cStringBuilder.add('Reading resource name :- '+xmlFileName);
        Dom.Document doc  = XMLMappingAdapter.getDomDocument(xmlFileName);
        XMLMapping mapping = XMLMappingAdapter.getNode(getNodeName());
        if(mapping == null) {
            fillXMLMapping(doc);
            mapping = XMLMappingAdapter.getNode(getNodeName());
        }
        cStringBuilder.add('Mapping formed for Node :- '+getNodeName());
        String query = mapping.getQuery();
        if(queryObjectName !=null && queryFilterString!= null && queryFilterString.length() > 0) {
            query = query + ' where '+queryObjectName+' = \''+recordId+'\' and '+queryFilterString;
        }else if(queryObjectName !=null) {
            query = query + ' where '+queryObjectName+' = \''+recordId+'\'';
        }else {
            query = query + ' where id = \''+recordId+'\'';
        }
        cStringBuilder.add('Query ==> '+query);
        List<SObject> recordList = Database.query(query);
        cStringBuilder.add('Records found :- '+recordList.size());
        List<LKRecord> lkRecordLst;
        if(XMLMappingAdapter.lkRecordsMap.get(getNodeName())!=null) {
            lkRecordLst = XMLMappingAdapter.lkRecordsMap.get(getNodeName());
        }else {
            lkRecordLst = new List<LKRecord>();
        }
        if(recordList.size() > 0) {
            for(SObject record : recordList) {
                cStringBuilder.add('------------------ New Record ----------------');
                LKRecord lkRec = new LKRecord();
                lkRec.sObjectRef = record;
                lkRec.setMappingObject(mapping);
                lkRec.setFieldToUpdate(mapping.getFieldToUpdate());
                Set<String> fieldNameSet = mapping.xmlNamesMap.keySet();
                for(String lkFieldName : fieldNameSet) {
                    String sfField = mapping.xmlNamesMap.get(lkFieldName);
                    if(lkFieldName.equalsIgnoreCase('id')) {
                        lkRec.sfObjectId = String.valueof(record.get(sfField));
                        continue;
                    }
                    if(sfField.contains('.')) {
                        String fieldVal = getValueOf(sfField,record);
                        cStringBuilder.add('Field = '+sfField+' , Value = '+fieldVal);
                        lkRec.lkValuesMap.put(lkFieldName,fieldVal);
                    }else {
                        cStringBuilder.add('Field = '+sfField+' , Value = '+String.valueof(record.get(sfField)));
                        lkRec.lkValuesMap.put(lkFieldName,String.valueof(record.get(sfField)));
                    }
                }
                cStringBuilder.add('Json of 1-1 mapping => '+lkRec.lkValuesMap);
                lkRecordLst.add(lkRec);
                if(mapping.isSaveGlobally()) {
                    LKIntegration.mapGlobal.putAll(lkRec.lkValuesMap);
                }
            }
        }else {
            return false;
        }
        if(mapping.getSaveResultsString() != null && mapping.getSaveResultsString().length()>0) {
            LKIntegration.mapGlobal.put(mapping.getSaveResultsString(),lkRecordLst);
        }
        XMLMappingAdapter.lkRecordsMap.put(getNodeName(),lkRecordLst);
        return true;
    }
    
    /****** Returns the string value of related fields from specific record *******/
    /**** Override this method in child class to give different implementation ****/
    private virtual String getValueOf(String fieldName, Sobject rec) {
        Integer i = fieldName.indexOf('.');
        String relatedObj = fieldName.subString(0,i);
        String relatedObjField = fieldName.subString(i+1,fieldName.length());
        Sobject relatedsObj = rec.getSObject(relatedObj);
        String relObjFValue;
        if(relatedsObj != null) {
            relObjFValue = String.valueOf(relatedsObj.get(relatedObjField));
        }
        return relObjFValue;
    }

    public String getRecordId() {
        return recordId;
    }
    public void setRecordId(String recId) {
        recordId = recId;
    }
    
    /****** make objects of mapping class *******
    ******* execute once when no mapping objects are created ******/
    public virtual void fillXMLMapping(Dom.Document doc) {
        Dom.XmlNode rootNode = doc.getRootElement();
        List<Dom.XmlNode> objectNodes = rootNode.getChildElements();
        for(Dom.XmlNode objectNode : objectNodes) {
            if(objectNode.getName().equalsIgnoreCase(getNodeName())) {
                XMLMapping mappingObj = new XMLMapping();
                mappingObj.setLKObjectName(objectNode.getName());
                mappingObj.setSFObjectName(objectNode.getAttribute('sf_name',null));
                mappingObj.setInsertApiUrl(objectNode.getAttribute('insertApiUrl',null));
                //mappingObj.setParentObjName(objectNode.getAttribute('parentObjName',null));
                mappingObj.setFieldToUpdate(objectNode.getAttribute('fieldToUpdate',null));
                mappingObj.setResponseKey(objectNode.getAttribute('responseKey',null));
                //mappingObj.setNullKeyCheck(objectNode.getAttribute('nullKeyCheck',null));
                mappingObj.setSaveGlobally(objectNode.getAttribute('saveGlobally',null));
                mappingObj.setSaveResultsString(objectNode.getAttribute('saveResults',null));
                List<Dom.XmlNode> fieldNodes = objectNode.getChildElements();
                for(Dom.XmlNode fieldNode : fieldNodes) {
                    String identifierString = fieldNode.getAttribute('identifier',null);
                    String removeString = fieldNode.getAttribute('remove',null);
                    if(identifierString != null && identifierString.equals('true')) {
                        mappingObj.clientFieldName = fieldNode.getText();
                    }
                    if(removeString != null){
                       mappingObj.removeElementList.add(fieldNode.getName());
                    
                    }
                    mappingObj.xmlNamesMap.put(fieldNode.getName(),fieldNode.getText());   
                }
                XMLMappingAdapter.xmlNodesMap.put(objectNode.getName(),mappingObj);
            }        
        }
    }
}
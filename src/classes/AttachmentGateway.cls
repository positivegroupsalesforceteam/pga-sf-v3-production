/** 
* @FileName: AttachmentGateway
* @Description: Provides finder methods for accessing data in the Attachment object.
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 9/05/18 JBACULOD Created Class, createFileInBox
* 1.1 9/12/18 JBACULOD Updated createFileInBox
* 1.2 19/12/2019 RDAVID SFBAU-39/SFBAU-111 - Equifax Report Attachment to Box. Branch: extra/task22445169-Equifax
**/ 
public class AttachmentGateway implements Queueable, Database.AllowsCallouts {

    private static boolean deleteAtt = false;
    private Attachment att {get;set;}
    private ID cseID {get;set;}
    private String srName {get;set;}
    public static Set<String> apiTransSetToUpd;
    public AttachmentGateway(Attachment att, Id cseID){ //, String srName){
        this.att = att;
        this.cseID = cseID;
        //this.srName = srName;
    }

    public void execute(QueueableContext context){
        AttachmentGateway.createFileInBox(this.att, this.cseID); //, this.srName);
        if (deleteAtt) {
            delete att; //Delete Attachment in SF 
        }
        system.debug('@@apiTransSetToUpd:'+apiTransSetToUpd); 
        //not bulkified. make sure to turn off when data loading.
        if(apiTransSetToUpd != NULL){
            List<API_Transfer__c> apiTransListToUpdate = new List<API_Transfer__c>();
            for(String apiTransId : AttachmentGateway.apiTransSetToUpd){
                if(apiTransId.containsIgnoreCase(':')) apiTransListToUpdate.add(new API_Transfer__c(Id = apiTransId.substringBefore(':'), EQ_Request_Status__c = 'Box Failed', Integration_Log__c = 'Box Error: '+apiTransId.substringAfter(':')));
                else {
                    apiTransListToUpdate.add(new API_Transfer__c(Id = apiTransId, EQ_Request_Status__c = 'Box Done'));
                }
            }
            if(apiTransListToUpdate.size() > 0) {
                System.debug('apiTransListToUpdate>>>>>>>>>>> '+apiTransListToUpdate);
                Database.update(apiTransListToUpdate);
            }
        }
    }

    private static void createFileInBox(Attachment att, ID cseID){ //, String srName){
        system.debug('@@att:'+att);
        box.Toolkit boxToolkit = new box.Toolkit();
        String boxCseFolderID = boxToolkit.getFolderIdByRecordId(cseID);
        if (Test.isRunningTest()) boxCseFolderID = '123456';
        system.debug('@@boxCseFolderID:'+boxCseFolderID);
        if (boxCseFolderID != null && boxCseFolderID != ''){
            //String boxfile = boxtoolkit.createFileFromAttachment(att,'Credit Proposal -- ' + srName + '.pdf',boxCseFolderID,null);
            String boxfile = boxtoolkit.createFileFromAttachment(att,att.Name,boxCseFolderID,null);
            system.debug('@@boxFile:'+boxFile);
            if (boxFile != null && boxFile != ''){ //File successfully generated in Box
                deleteAtt = true;
                if (Id.valueof(att.ParentId).getSObjectType() == Schema.API_Transfer__c.SobjectType){
                    if(apiTransSetToUpd == null) apiTransSetToUpd = new Set<String>{att.ParentId};
                    else {
                        apiTransSetToUpd.add(att.ParentId);
                    }
                }
            }
            else{ //With Error
                system.debug('@@boxToolkit.mostRecentError:'+boxToolkit.mostRecentError);
                deleteAtt = false;
                if (Id.valueof(att.ParentId).getSObjectType() == Schema.API_Transfer__c.SobjectType){
                    if(apiTransSetToUpd == null) apiTransSetToUpd = new Set<String>{att.ParentId+':'+boxToolkit.mostRecentError};
                    else {
                        apiTransSetToUpd.add(att.ParentId+':'+boxToolkit.mostRecentError);
                    }
                }
            }
            if(!Test.IsRunningTest()) boxToolkit.commitChanges();
        }
    }

}
/** 
* @FileName: SectorSLANextWarning
* @Description: Invocable class used by Sector_SLA_Warnings Process Builder
* @Copyright: Positive (c) 2019 
* @author: Rexie Aaron David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 12/04/19 RDAVID-task24148707 Created Class
* 1.1 17/04/19 RDAVID-task24148707 Added logic to update the Parent Case SLA Status SLA_Status_Submitted_to_Lender_Sector__c,SLA_Status_Initial_Review_Sector__c ,SLA_Status_Application_Review_Sector__c,SLA_Status_Docs_Out_Sector__c,SLA_Status_Sent_for_Settlement_Sector__c
* 1.0 9/05/19 RDAVID-task24399056 : Added Logic to apply Different time threshold for Consumer and Commercial Initial Review 
**/ 
public class SectorSLANextWarning {
    public static Map<String,SLA_Sector__mdt> slaSectorMDTMap = new Map<String,SLA_Sector__mdt>();
    @InvocableMethod(label='Sector - Set SLA Next Warning' description='updates next Warning Time of an SLA whenever an SLA is not yet completed')
    public static void setSLAWarningTime (List <Id> sectorIds) {
        // try{
            BusinessHours bh = [SELECT Id,IsDefault,Name,IsActive,TimeZoneSidKey,SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime, WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime 
                                FROM BusinessHours 
                                WHERE IsActive = TRUE AND IsDefault = TRUE]; 

            List<Sector__c> sectorList = [  SELECT Id, Case_Number__c, Case_Number__r.RecordType.Name, SR_Number__c,Partition__c,Channel__c,Sector1__c,Sector_Status__c,Alert_Recipient_Name__c,Alert_Recipient_Email__c,Entry_Owner__c,Sector_Entry__c,Exit_Owner__c,Sector_Exit__c,Time_in_Sector_All_Hours_HH_MM__c,Time_in_Sector_Business_Hours_HH_MM1__c,RecordTypeId,RecordType.Name,SLA_Status__c,SLA_Active__c,SLA_Met__c,SLA_Due_Date__c,SLA_Warning_Time__c,SLA_Warning_Status__c,SLA_Target_Time_MM__c,SLA_Time_MM__c 
                                            FROM Sector__c WHERE Id IN: sectorIds];
            
            // Map<Id,SObject> caseToUpdate = new Map<Id,SObject>();

            for(SLA_Sector__mdt slaMeta : Database.query(SectorGateway.getSLASectorMetadataQueryString())){
                slaSectorMDTMap.put(slaMeta.Sector_Name__c,slaMeta);
            }
            
            Datetime warningdate; 
            
            for (Sector__c sector : sectorList){
                warningdate = sector.SLA_Warning_Time__c;
                sector.SLA_Warning_Time__c = null;
                sector.SLA_Active__c = null;
            }

            update sectorList; //force update workaround to retrigger SLA Warning

            system.debug('@@warningdate:'+warningdate);

            for (Sector__c sector : sectorList){
                Integer sectorInterval = Integer.valueOf(slaSectorMDTMap.get(sector.Sector1__c).SLA_Interval_MM__c);
                system.debug('@@sectorInterval:'+sectorInterval);
                sector.SLA_Active__c = 'Yes';
                if (sector.Sector_Exit__c == NULL){ 
                    sector.SLA_Warning_Time__c = BusinessHours.add(bh.Id, warningdate, sectorInterval*60000); //Set succeeding warning of current SLA (add 15 minutes)
                    system.debug('@@sector.SLA_Warning_Time__c:'+sector.SLA_Warning_Time__c);
                    sector.SLA_Time_MM__c = Decimal.valueof(( BusinessHours.diff(bh.Id, sector.Sector_Entry__c, warningdate ) / 1000) / 60 ) + 1;  //returns SLA Age in minutes
                    if(sector.SLA_Time_MM__c >= Integer.valueOf(slaSectorMDTMap.get(sector.Sector1__c).SLA_Yellow_Icon_Threshold_MM__c) && sector.SLA_Time_MM__c < Integer.valueOf(slaSectorMDTMap.get(sector.Sector1__c).SLA_Red_Icon_Threshold_MM__c)) 
                        sector.SLA_Warning_Status__c = 'Yellow SLA Warning Sent';
                    else if(sector.SLA_Time_MM__c >= Integer.valueOf(slaSectorMDTMap.get(sector.Sector1__c).SLA_Red_Icon_Threshold_MM__c)) 
                        sector.SLA_Warning_Status__c = 'Red SLA Warning Sent';
                    system.debug('@@slaWarningB');
                }
                if(sector.Case_Number__r.RecordType.Name.containsIgnoreCase('NOD: Consumer') && sector.Sector1__c.containsIgnoreCase(CommonConstants.SECTOR1_INIT_REV)){
                    SectorGateway.setConsumerInitialReviewTimeToAppReview(sector,bh, slaSectorMDTMap.get(CommonConstants.SECTOR1_NOD_SLA_APP_REVIEW), null, 'SectorSLANextWarning',null,warningdate);
                }
                
            }
            update sectorList;
            // if(caseToUpdate.size()>0) update caseToUpdate.values();
        // }
        // catch(Exception e){
        //     system.debug('******SectorSLANextWarning '+e.getMessage());
        // }
    }
}
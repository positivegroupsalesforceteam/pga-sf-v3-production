/** 
* @FileName: CommissionCTHandler
* @Description: Trigger Handler for the CommissionCT__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/8/18 RDAVID Created Class
* 1.1 9/10/18 RDAVID origin/extra/CommissionCTTrigger - Update Trigger to get the Dealership Name from KAR Sourcing Purchase record.
* 1.2 10/10/18 RDAVID origin/extra/CommissionCTTrigger - Update Trigger to add validation for Karlon to require Dealership Name for Case > Purchase.
* 1.3 16/10/18 RDAVID origin/extra/CommissionCTTrigger - Update Trigger Logic - Issue from Chris not populating the Dealer Name due the current WHERE Clause (Lender1__c != NULL), Added OR Condition.
* 1.4 20/10/18 RDAVID origin/extra/CommissionCTTrigger - Update Trigger - Added Logic to populate Finance_Commission_Date_Paid__c
* 1.5 29/01/19 JBACULOD replaced Dealer_Sale_Vendor__c validation with Dealer_Sale_Vendor1__c due to updated model
**/ 

public without sharing class CommissionCTHandler implements ITrigger {	
	
	public Map<Id,Case> caseMap = new Map<Id,Case>();
	private static String caseRTKARVehicleSourcing = 'KAR: Vehicle Sourcing';
	private static String commissionKarlonRTName = 'NM - KARLON';
	// Constructor TEST
	public CommissionCTHandler(){

	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
		Set<Id> cseIds = new Set<Id>();

		if(TriggerFactory.trigset.Enable_CommissionCT_GetCaseLender__c){
			if(Trigger.isInsert){
				for(CommissionCT__c commCT : (List<CommissionCT__c>)trigger.new){
					if(Trigger.isInsert) cseIds.add(commCT.Case__c);
				}
			}	
		} 
		if(cseIds != NULL){
			caseMap = new Map<Id,Case>([SELECT Id,Lender1__c, RecordType.Name, (SELECT Id,Dealer_Sale_Vendor1__c,Dealer_Sale_Vendor1__r.Name FROM Purchased_Assets__r WHERE Dealer_Sale_Vendor1__r.Name != NULL) FROM Case WHERE Id IN: cseIds]);//AND (Lender1__c != NULL OR Lender_Name__c != NULL)
		}
	}
	
	public void bulkAfter(){
		
	}
		
	public void beforeInsert(SObject so){
		CommissionCT__c newComm = (CommissionCT__c)so;
		if(!caseMap.isEmpty() && caseMap.containsKey(newComm.Case__c)){
			if(caseMap.get(newComm.Case__c).Lender1__c != NULL) newComm.Lender__c = caseMap.get(newComm.Case__c).Lender1__c;
			
			if(caseMap.get(newComm.Case__c).RecordType.Name == caseRTKARVehicleSourcing && Schema.SObjectType.CommissionCT__c.getRecordTypeInfosById().get(newComm.recordtypeid).getname() == commissionKarlonRTName){
				if(caseMap.get(newComm.Case__c).Purchased_Assets__r.size() > 0){
					newComm.Dealership_Name__c = caseMap.get(newComm.Case__c).Purchased_Assets__r[0].Dealer_Sale_Vendor1__r.Name;	
					//System.debug('Populate Dealership ');
				}
				else{
					// System.debug('Validate Dealership ');
					newComm.Dealership_Name__c.addError('You should enter a Dealer Sale Vendor in Purchase record.');
				} 
			}
		} 
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		CommissionCT__c newComm = (CommissionCT__c)so;
        CommissionCT__c oldComm = (CommissionCT__c)oldSo;

        if(newComm.Finance_Commission_Paid__c != oldComm.Finance_Commission_Paid__c && newComm.Finance_Commission_Paid__c){
            newComm.Finance_Commission_Date_Paid__c = System.TODAY();
        } 
    }

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){
		
	}
	
	public void afterUpdate(SObject oldSo, SObject so){
		
	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
	}
}
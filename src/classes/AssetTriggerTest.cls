/** 
* @FileName: AssetTriggerTest
* @Description: Test class for Asset Trigger 
* @Copyright: Positive (c) 2018 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/19/18 JBACULOD Created class, Added testAssetToRoleRollup [SD-23]
* 1.1 2/20/18 JBACULOD Modified hard coded record type labels with CommonConstants
* 2.0 4/25/18 JBACULOD Removed existing test setup and test methods, added testautoCreateFOShareAsset
**/ 
@isTest
private class AssetTriggerTest {
    
    @testsetup static void setup(){
        TestDataFactory.Case2FOSetupTemplate();
    }

    static testmethod void testautoCreateFOShareAsset(){

        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Asset_Trigger__c = true,
                Enable_FO_Auto_Create_FO_Share__c = true,
                Enable_FO_Delete_FO_Share_on_FO_Delete__c = true
        );
        insert trigSet;

        //Retrieve Role Test data from Setup
        list <Role__c> rolelist = [Select Id, Case__c, Application__c From Role__c];
        string foshareRoleIDs = '';
        for (Integer i=0; i<10; i++){
            foShareRoleIds+= rolelist[i].Id + ';';
        }

        Test.startTest();

            //Create test data for Asset, autoCreating FO Share
            Id assRTId = Schema.SObjectType.Asset1__c.getRecordTypeInfosByName().get('Vehicle - Motor').getRecordTypeId(); //Vehicle
            list <Asset1__c> asslist = new list <Asset1__c>();
            for (Integer i=0; i<10; i++){
                Asset1__c ass = new Asset1__c(
                    h_Auto_Create_FO_Share__c = true,
                    h_FO_Share_Role_IDs__c = foShareRoleIDs,
                    RecordTypeId = assRTId,
                    Equal_Share_for_Roles__c = true,
                    Application1__c = rolelist[i].Application__c,
                    Asset_Value__c = 1000
                );
                asslist.add(ass);
            }
            insert asslist;

            //Retrieve created FO Shares
            list <FO_Share__c> fosharelist = [Select Id, Case__c, Application_Asset__c, Monthly_Amount__c, Asset__c, Asset__r.Asset_Type__c, Asset__r.RecordType.Name, Type__c, FO_Record_Type__c, Role__c, Role__r.Application__c, Role_Share__c, Role_Share_Amount__c From FO_Share__c];
            for (FO_Share__c FOs : fosharelist){
                //Verify Asset Details were populated in FO SHare
                system.assertEquals(FOs.Asset__r.RecordType.Name, FOs.FO_Record_Type__c);
                system.assertEquals(FOs.Asset__r.Asset_Type__c, FOs.Type__c);
                //Verify Asset, Case, Application were linked to FO Share
                system.assert(FOs.Asset__c != null);
                system.assert(FOs.Case__c != null);
                system.assert(FOs.Application_Asset__c != null);
                //Verify Equal Share was split to each Role in Application
                Decimal dval = FOs.Role_Share__c;
                dval = dval.setScale(2);
                Decimal drolesize = 10; //# of Roles assigned in FO
                drolesize = drolesize.setScale(2);
                system.assertEquals(dval, drolesize);
                //Verify Role Share Amount was calculated
                Decimal dmval = FOs.Role_Share_Amount__c;
                dmval = dmval.setScale(2);
                Decimal dsval = FOs.Monthly_Amount__c * (FOs.Role_Share__c / 100);
                dsval = dsval.setScale(2);
                system.assertEquals(dmval, dsval );
            }

            //Cover Asset update, delete trigger events
            update asslist;
            delete asslist;

        Test.stopTest();


    } 

}
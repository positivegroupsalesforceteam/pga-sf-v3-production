@isTest
private class LeadPrelimAssessControllerTest {

    static testmethod void testLeadPrelimWhoWhat(){
        PageReference pref = Page.LeadPrelimAssessment_WhoWhat;
        Test.setCurrentPage(pref);
        LeadPrelimAssessment_WhoWhat_CC lpcon = new LeadPrelimAssessment_WhoWhat_CC();
        lpcon.curPage = 1;

        Test.startTest();

            //Entering Data in Client Information Section
            lpcon.con.FirstName = 'FirstTest';
            lpcon.con.LastName = 'LastTest';
            lpcon.con.MiddleName = 'MiddleTest';
            lpcon.con.MobilePhone = '12345';
            lpcon.con.Email = 'test@test.com';

            //Entering Data in Loan Requirement Section
            lpcon.cse.Lead_Purpose__c = 'Car Loan';
            lpcon.cse.Lead_Loan_Amount__c = 1000;
            

            //Entering data in Asset to be Financed (Purchased Asset) Section
            for (LeadPrelimAssessment_WhoWhat_CC.passetWrapper psWr : lpcon.passetWrlist){
                psWr.pa.Asset_Type__c = 'Motor Vehicle';
                psWr.pa.Asset_Sub_Type__c = '';
                psWr.pa.Condition__c = 'New';
                psWr.pa.Year_Of_Manufacture__c = '2010';
                psWr.pa.Vehicle_Make__c = 'test';
                psWr.pa.Vehicle_Model__c = 'test';
            }
            //Simulate clicking of Add button in Asset to be Financed Section
            lpcon.addNewPurchasedAsset();
            lpcon.PContinue();
        	lpcon.cse.Partition__c = 'Positive';
        	lpcon.getChannels();
        	lpcon.cse.Partition__c = 'Nodifi';
        	lpcon.getChannels();
       	 	lpcon.runCheatTest();
        Test.stopTest();
    }

    static testmethod void testLeadPrelimRoleAddressEmp(){
        PageReference pref = Page.LeadPrelimAssessment_RoleAddressEmp;
        Test.setCurrentPage(pref);
        LeadPrelimAssessment_RoleAddressEmp_CC lpcon = new LeadPrelimAssessment_RoleAddressEmp_CC();
        Id consumerJointRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POS: Consumer - Asset Finance').getRecordTypeId();
        Case cse = TestDataFactory.createGenericCase(consumerJointRecordTypeId, 'New', 'Open');
        PLS_Bucket_Settings__c settings = PLS_Bucket_Settings__c.getOrgDefaults();
        settings.Bucket_1_Queue_ID__c = '00G6F000003BbTZUA0';
        settings.Bucket_2_Queue_ID__c = '00G6F000003BbTeUAK';
        settings.Bucket_3_Queue_ID__c = '00G6F000003BbTjUAK';
        settings.Bucket_4_Queue_ID__c = '00G6F000003BbToUAK';
        settings.Bucket_5_Queue_ID__c = '00G6F000003BbTyUAK';
        settings.Bucket_6_Queue_ID__c = '00G6F000003BbU3UAK';
        settings.Bucket_7_Queue_ID__c = '00G6F000003BbTyUAK';
        settings.Bucket_8_Queue_ID__c = '00G6F000003BbZiUAK';
        upsert settings;

        System.debug('settings - '+ settings);
        System.debug('BUCKET - '+ settings.Bucket_1_Queue_ID__c);
        Database.insert(cse);

        Test.startTest();
            lpcon.getCse(cse.Id);
            lpcon.loadAllData(cse);
            //lpcon.getExistingRoles();

            //lpcon.getAddressOwner();
            //lpcon.getWhosEmployment();
            //lpcon.getroleTypes();
            //lpcon.hasPersonMatch(new Contact());
           // lpcon.addAddress();
           // lpcon.whatExp = 'AddPrimaryEmployment';
            lpcon.addEmp();
            lpcon.closePopup();
        	lpcon.runCheatTest();
        
        LeadPrelimAssessment_Income_CC lpcon1 = new LeadPrelimAssessment_Income_CC();
        lpcon1.runCheatTest();
        
        LeadPrelimAssessment_Expenses_CC lpcon2 = new LeadPrelimAssessment_Expenses_CC();
        lpcon2.runCheatTest();
        
        LeadPrelimAssessment_Final_CC lpcon3 = new LeadPrelimAssessment_Final_CC();
        lpcon3.runCheatTest();
        
        LeadPrelimAssessment_RoleAddressComm_CC lpcommercial = new LeadPrelimAssessment_RoleAddressComm_CC();
        lpcommercial.runCheatTest();
            //lpcon.showPopup
            //lpcon.validatePage2();
        Test.stopTest();

    }
}
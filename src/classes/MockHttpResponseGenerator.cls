@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"sequence":"107","auth":"9d7a566b5b1ba5103569f3f76b7ba640","username":"saas","broker_id":"11242","LK_Client_Contact_id__c":null,"loankit_reference_id":null,"lead":"No","client_status_seqence":"123.00","date_incorporated":"2016-06-16 00:00:00","trustee":null,"act_trust":"t","trust":null,"housing_status":"Renting","email_other":null,"fax_country_code":"98","fax_area_code":"011","fax":"011","work_phone_country_code":"+91","work_phone_area_code":"011","work_phone":"787878","home_phone_country_code":"+91","home_phone_area_code":"011","home_phone":"89898989","country":"India","residency_status":"Renting","drivers_licence":"1234","dependent_ages":"123","number_of_dependents":"1.00","marital_status":"Married","date_of_birth":"2016-06-16 00:00:00","years_in_current_profession":"1.00","previous_name":"Amar","company_type":"Prospect","company_number_type":null,"company_number":"12345","company_name":"Saasfocus Account","sex":"Male","also_known_as":"Amar","other_name":"Amarmahe","name_title_other":"Mr","name_title":null,"entity":"Person","primary_applicant":"Yes","applicant_type":"Applicant","email":"amarendra.kumar@saasfocus.com","mobile":"88888888888","last_name":"Maheshwari","first_name":"Amar"}');
        res.setStatusCode(200);
        return res;
    }
}
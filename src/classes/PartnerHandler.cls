/** 
* @FileName: PartnerHandler
* @Description: Trigger Handler for the Partner SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 8/01/19 RDAVID Created Class - Partner Trigger Logic - Insert
* 1.1 11/05/19 JBACULOD - task26657305 - LeadToPartnerBizibleTP: Added Partner - Create Partner TPs logic from Lead
**/ 
public without sharing class PartnerHandler implements ITrigger{
    private List<Partner__c> partnerList = new List<Partner__c>();
    private List<Partner__c> validPartnerList = new List<Partner__c>();
    private List<Partner__c> updatedValidPartnerList = new List<Partner__c>();
    private Map<Id,Referral_Company__c> refCompPartnerMapToUpsert = new Map<Id,Referral_Company__c>();
    private List <Partner_Touch_Point__c> partnerTPlist = new list <Partner_Touch_Point__c>();

    private Map<String,Referral_Company__c> relatedSiteMap = new Map<String,Referral_Company__c>(); 
    private Map<String,ABN__c> abnMap = new Map<String,ABN__c>(); 
    private Map<Id,Address__c> addressMap = new Map<Id,Address__c>(); 

    public PartnerHandler(){
        partnerList = (Trigger.IsInsert || Trigger.IsUpdate) ? (List<Partner__c>) Trigger.new : (Trigger.IsDelete) ? (List<Partner__c>) Trigger.old : null;
    }

    public void bulkBefore(){
        
    }

    public void bulkAfter(){
        Set<Id> relatedSiteIds = new Set<Id>();
        Set<Id> addressIds = new Set<Id>();
        Set<String> partnerAbn = new Set<String>();
        Set<Id> referralCompanyIds = new Set<Id>();
        Map <Id,Id> leadPartnerMap = new map <id,Id>();
        Map<Id,Partner__c> oldPartnerMap = (Map<Id,Partner__c>) Trigger.oldMap;

        for(Partner__c partner : partnerList){
            if(TriggerFactory.trigset.Enable_Sync_Referral_Company_and_Partner__c){
                if(Trigger.IsInsert){
                    if(partner.m_Referral_Company_SFID__c == NULL){
                        relatedSiteIds.add(partner.Partner_Parent__c);
                        validPartnerList.add(partner);
                        partnerAbn.add(partner.Partner_ABN__c);
                        if(partner.Partner_Address__c != NULL) addressIds.add(partner.Partner_Address__c);
                    }
                }
                if(Trigger.IsUpdate){
                    if(partner.h_ReferralCompanyId__c != NULL){
                        Boolean isPartnerFieldsUpdated = PartnerGateway.isPartnerFieldsUpdated(partner,oldPartnerMap.get(partner.Id));
                        if(isPartnerFieldsUpdated){
                            relatedSiteIds.add(partner.Partner_Parent__c);
                            updatedValidPartnerList.add(partner);
                            partnerAbn.add(partner.Partner_ABN__c);
                            // referralCompanyIds.add(partner.h_ReferralCompanyId__c);
                            if(partner.Partner_Address__c != NULL) addressIds.add(partner.Partner_Address__c);
                        } 
                    }
                }
            }
            if (TriggerFactory.trigset.Enable_Partner_Create_Partner_TPs__c){
                if (Trigger.isInsert){
                    if (partner.h_LeadId__c != null){
                        leadPartnerMap.put(partner.h_LeadId__c, partner.Id); //key: Lead ID, value: created Partner ID
                    }
                }
            }
        }

        if(relatedSiteIds.size() > 0) relatedSiteMap = PartnerGateway.getRelatedSiteMap(relatedSiteIds);
        if(partnerAbn.size() > 0) abnMap = PartnerGateway.getABNMap(partnerAbn); //new Map<String,ABN__c>([SELECT Id, Name, Registered_ABN__c FROM WHERE Registered_ABN__c IN: partnerAbn]);
        if(addressIds.size() > 0) addressMap = new Map<Id,Address__c>([SELECT Id, Street_Number__c,Street_Name__c,Street_type__c,Postcode__c,State_Acr__c,State__c,Suburb__c,Location__c,Country__c,m_Referral_Company_SFID__c,m_MIgration_Full_Address__c FROM Address__c WHERE Id IN: addressIds]);
        if(leadPartnerMap.size() > 0) partnerTPlist = PartnerGateway.getLeadBizibleTPs(leadPartnerMap); //JBACU 11/05/19

        //Insert
        if(validPartnerList.size() > 0){
            for(Partner__c partner : validPartnerList){
                refCompPartnerMapToUpsert.put(partner.Id,PartnerGateway.createUpdateReferralCompany(partner, relatedSiteMap, abnMap, addressMap));
            }
        }

        //Update
        if(updatedValidPartnerList.size() > 0){
            for(Partner__c partner : updatedValidPartnerList){
                refCompPartnerMapToUpsert.put(partner.Id,PartnerGateway.createUpdateReferralCompany(partner, relatedSiteMap, abnMap, addressMap));
                Referral_Company__c referralCompany = refCompPartnerMapToUpsert.get(partner.Id);
                referralCompany.Id = partner.h_ReferralCompanyId__c;
                refCompPartnerMapToUpsert.put(partner.Id,referralCompany);
            }
        }
    }

    public void beforeInsert(SObject so){

    }

    public void beforeUpdate(SObject oldSo, SObject so){

    }
    
    public void beforeDelete(SObject so){	
	
    }

    public void afterInsert(SObject so){

    }

    public void afterUpdate(SObject oldSo, SObject so){

    }

    public void afterDelete(SObject so){
	}
    
    public void andFinally(){
        if(!refCompPartnerMapToUpsert.isEmpty()){
            PartnerGateway.shouldFirePartnerCreation = false;//RDAVID 22/03/2019: Prevent Insertion of Partner in Referrral_Company__c Trigger.
            Database.upsert(refCompPartnerMapToUpsert.values()); //Insert Referral Company Records (Copy of Partner record)
        } 
        //JBACU 11/05/19 - Create Partner Touch Points
        if (partnerTPlist.size() > 0){
            Database.insert(partnerTPlist); 
        }
    }
}
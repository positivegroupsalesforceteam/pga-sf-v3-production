@isTest
private class Case_VehicleSourcingCCTest {

    @testsetup static void setup(){

        //Create test data for (Case, Application, Role, Account)
        TestDataFactory.Case2FOSetupTemplate();

        //Create test data for Purchase
        Case parentcse = [Select Id, CaseNumber From Case limit 1];
        Id rtPAssetID = Schema.SObjectType.Purchased_Asset__c.getRecordTypeInfosByName().get('Motor Vehicle').getRecordTypeId();
        Purchased_Asset__c passet = new Purchased_Asset__c(
            RecordTypeId = rtPAssetID,
            Case__c = parentcse.Id
        );
        insert passet;

    }

    static testmethod void testVehicleSourcing(){

        //Retrieve test data for Case
        Case parentcse = [Select Id, CaseNumber From Case limit 1];
        
        PageReference vsrpref = Page.Case_VehicleSourcing;
		vsrpref.getParameters().put('id', parentcse.Id);
		Test.setCurrentPage(vsrpref);

        Test.startTest();

            Case_VehicleSourcingCC vscc = new Case_VehicleSourcingCC();
            vscc.saveSourcingCase();

            vscc.scse.Submission_Comments_Karlon__c = 'test';
            vscc.scse.Estimate_Max_Purchase_Price__c = 50000;
            for (Case_VehicleSourcingCC.purchaseWrapper pwr : vscc.pcpwr.pWrlist){
                pwr.isSelected = true;
            }
            vscc.saveSourcingCase();

        Test.stopTest();



    }

}
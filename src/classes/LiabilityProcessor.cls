public class LiabilityProcessor extends DefaultProcessor {
    
    public override boolean process() {
        if(!super.process()) {
            return false;
        }
        List<LKRecord> lstLKContact = new List<LKRecord>();
        lstLKContact.addAll(XMLMappingAdapter.lkRecordsMap.get('PersonAccount'));
        List<LKRecord> applicant2List = XMLMappingAdapter.lkRecordsMap.get('Applicant2');
        if(applicant2List!=null && applicant2List.size()>0) {
            lstLKContact.addAll(applicant2List);
        }
        List<LkRecord> lKRecordsList= XMLMappingAdapter.lkRecordsMap.get(getNodeName());//single item map           
        for(Integer i=0 ; i < lKRecordsList.size();i++) { //integer loop because we need index of the element
            List<String> ownersList;
            List<String> shareList;
            String loankitOwnerId;
            LKRecord objLkRec = lKRecordsList.get(i);
            if(objLKRec.isProcessed) {
                continue;
            }
            objLKRec.checkForUpdate = true;
            String ownerId = String.valueOf(objLkRec.lkValuesMap.get('Account__c'));
            if(ownerId == null || ownerId.equalsIgnoreCase('null')) {
                ownerId = String.valueOf(objLkRec.lkValuesMap.get('Applicant_2_Mapping__c'));
            }
            for(LKRecord lkCon : lstLKContact) {
                if(ownerId != null && !ownerId.equalsIgnoreCase('null')) {
                    if(lkCon.sfObjectId.equalsIgnoreCase(ownerId)) {
                        loankitOwnerId = lkCon.lkObjectId;
                    }
                }
            }
            for(Integer j=0; j<i; j++) { 
                LKRecord lkRecInner = lKRecordsList.get(j);
                if(objLkRec.sfobjectId.equalsIgnoreCase(lkRecInner.sfObjectId)){               
                    List<String> responseLKOwnerIdList = new List<String>();
                    List<String> percentageOwnedLKList = new List<String>();
                    Object o = lkRecInner.lkValuesMap.get('owners');
                    if(o instanceOf List<String>) {
                        responseLKOwnerIdList.addAll((List<String>)o);
                        percentageOwnedLKList.addAll((List<String>)lkRecInner.lkValuesMap.get('percentage_owned'));
                    }else if(o instanceOf String){
                        responseLKOwnerIdList.add(String.valueOf(o));
                        percentageOwnedLKList.add(String.valueOf(lkRecInner.lkValuesMap.get('percentage_owned')));
                    }
                    ownersList = responseLKOwnerIdList;
                    shareList = percentageOwnedLKList;
                }
            }
            if(!objLkRec.isProcessed) {
                Map<String,Object> valMap = objLkRec.lkValuesMap;
                if(ownersList != null) { 
                    ownersList.add(loankitOwnerId);
                    shareList.add((String)valMap.get('percentage_owned'));
                    valMap.put('owners',ownersList);
                    valMap.put('percentage_owned',shareList); 
                }else {
                    valMap.put('owners',loankitOwnerId);
                }
                //valMap.put('other_asset_type','Yes');
                String liability_type = String.valueOf(valMap.get('liability_type'));
                if(liability_type !=null && !liability_type.equalsIgnoreCase('null')){
                    if(liability_type.equalsIgnoreCase('Car Loan') || liability_type.equalsIgnoreCase('Mortgage')|| liability_type.equalsIgnoreCase('Rent')) {
                        valMap.put('liability_type','Other');
                    }else if(liability_type.equalsIgnoreCase('Mortgage Loan')) {
                        String loanForAssetId = (String)valMap.get('Mortgage_Loan_For_Map__c');
                        List<LKRecord> assetRecList = XMLMappingAdapter.lkRecordsMap.get('Asset');
                        String assetAddressLKId = '';
                        if(loanForAssetId!=null) {
                            for(LKRecord assetRec : assetRecList) {
                                if(assetRec.sfObjectId!=null && assetRec.sfObjectId.equalsIgnoreCase(loanForAssetId)) {
                                    assetAddressLKId = (String)assetRec.lkValuesMap.get('address_id');
                                    break;
                                }
                            }
                        }
                        if(assetAddressLKId.length()>0) {
                            valMap.put('rel_asset_id',assetAddressLKId);
                        }
                        valMap.put('liability_type',liability_type.replaceAll(' ',''));
                    }
                    else {
                        valMap.put('liability_type',liability_type.replaceAll(' ','')); 
                    }
                }
                valMap.remove('Mortgage_Loan_For_Map__c');
                string lender = String.valueOf(valMap.get('lender'));
                if(lender!=null && lender!=''){
                    valMap.put('lender',lender.replaceAll(' ',''));
                }
            }
        }
        return true;
    }
}
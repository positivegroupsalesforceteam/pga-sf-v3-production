@isTest
public class Role_CaseTouchpointNewCaseTest {

    @testSetup static void setupData() {

		//Turn On Trigger 
		Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
		triggerSettings.Enable_Triggers__c = true;
		triggerSettings.Enable_Income_Trigger__c = true;
		//triggerSettings.Enable_Income_Rollup_to_Role__c = true;
		triggerSettings.Enable_Role_Trigger__c = true;
		triggerSettings.Enable_Role_Rollup_to_Application__c = true;
        triggerSettings.Enable_Case_Trigger__c = true;
		triggerSettings.Enable_Case_Auto_Create_Application__c = true;
		triggerSettings.Enable_Role_To_Account_Propagation__c = true;
		triggerSettings.Enable_Role_Create_Case_Touch_Point__c = true;

		upsert triggerSettings Trigger_Settings1__c.Id;

		//Create Test Account
		List<Account> accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);
        
		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);

		//Create Test Role 
		List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
		Database.insert(roleList);

	}

    static testMethod void testInvocableClassPositive(){
		//Create Test Account
		List<Account> accList = TestDataFactory.createGenericPersonAccount('First Name','TestLastName',CommonConstants.PACC_RT_I, 1);
		Database.insert(accList);
		
		accList = [SELECT Id, IsPersonAccount,PersonContactId FROM Account WHERE Id =: acclist[0].Id LIMIT 1];
		System.assertEquals(accList[0].IsPersonAccount,true);

		bizible2__Bizible_Person__c biziPerson = new bizible2__Bizible_Person__c(	bizible2__Contact__c = acclist[0].PersonContactId,
																					bizible2__UniqueId__c = 'Person_'+acclist[0].PersonContactId,
																					Name = 'balarajesh1977@gmail.com');
		Database.insert(biziPerson);
		bizible2__Bizible_Touchpoint__c bizi = new bizible2__Bizible_Touchpoint__c (	bizible2__Bizible_Person__c = biziPerson.Id,
																						bizible2__Contact__c = acclist[0].PersonContactId,
																						bizible2__Account__c = acclist[0].Id,
																						bizible2__Marketing_Channel__c = 'Paid Search',
																						bizible2__Marketing_Channel_Path__c = 'Paid Search.AdWords',
																						bizible2__Touchpoint_Source__c = 'Google AdWords',
																						bizible2__Medium__c = 'CPC',
																						bizible2__Ad_Campaign_Name__c = 'Car Loans - paused 8th June 2018',
																						bizible2__Ad_Group_Name__c = 'Head Terms',
																						bizible2__Keyword_Text__c = '[car loan]',
																						bizible2__Ad_Content__c = '#1 Car Loans From 4.85% Bank Say No, We Say Yes Get A 30 Second Online Quote. 90% Applicants Approved In 24 Hours. Enquire Now https://www.positivelendingsolutions.com.au',
																						bizible2__Landing_Page__c = 'www.positivelendingsolutions.com.au/lp/car-loans',
																						bizible2__Form_URL__c = 'www.positivelendingsolutions.com.au/lp/car-loans',
																						bizible2__Ad_Destination_URL__c = 'https://www.positivelendingsolutions.com.au/lp/car-loans/',
																						bizible2__UniqueId__c='TP2_Person_0036F00002Wk60XQAR_2018-06-25:02-40-35-60620.a2cc6ea44caf45',
																						Name='testa3d6F000001MMRV',
																						bizible2__Touchpoint_Date__c = system.now(),
																						bizible2__Touchpoint_Position__c = 'FT, LC, Form',
																						bizible2__Touchpoint_Type__c = 'Web Form',
																						bizible2__Geo_City__c = 'Camira',
																						bizible2__Geo_Region__c = 'Queensland',
																						bizible2__Geo_Country__c = 'Australia',
																						bizible2__Platform__c = 'Android (8.0)',
																						bizible2__Browser__c = 'Chrome (67.0)',
																						bizible2__Count_First_Touch__c = 1,
																						bizible2__Count_Lead_Creation_Touch__c = 1,
																						bizible2__Count_U_Shaped__c = 1);
		Database.insert(bizi);
		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);

		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);
		
		Test.StartTest();
			List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
			roleList[0].Other_Phone__c = '09165856439';
			roleList[0].Mobile_Phone__c = '09165856439';
			roleList[0].Email__c = 'test@email.com';
			roleList[0].Is_From_Frontend__c = true;

			Database.insert(roleList);
			
			accList = [SELECT Id,PersonHomePhone,PersonMobilePhone,PersonEmail FROM Account WHERE Id =: accList[0].Id];
			System.assertEquals(accList[0].PersonHomePhone,roleList[0].Other_Phone__c);
			System.assertEquals(accList[0].PersonMobilePhone,roleList[0].Mobile_Phone__c);
			System.assertEquals(accList[0].PersonEmail,roleList[0].Email__c);
            list <String> rolecaseaccountId = new list <String>();
            rolecaseaccountId.add(roleList[0].Id+':'+caseList[0].Id+':'+accList[0].Id);
            Role_CaseTouchpointNewCase.createNewCaseTouchpoint(rolecaseaccountId);
            rolecaseaccountId = new list <String>();
            rolecaseaccountId.add(accList[0].Id+':'+caseList[0].Id+':'+roleList[0].Id);
            Role_CaseTouchpointNewCase.createNewCaseTouchpoint(rolecaseaccountId);
		Test.StopTest();
	}	
}
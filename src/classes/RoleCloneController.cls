/**
 * @File Name          : RoleCloneController.cls
 * @Description        : 
 * @Author             : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 02/10/2019, 6:05:33 am
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/07/2019, 8:52:48 am   Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Initial Version
 * 1.1    02/10/2019, 5:35:43 am   Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Added Lead Employment Type, Lead Employment Situation on FO Income
**/
public class RoleCloneController {

    @AuraEnabled
    public static wrappedRoleData getExistingRoleAndRelatedData(String recordId, list <string> disregardedFieldSet){
        wrappedRoleData wRD = new wrappedRoleData();
        string rolePreQuery = 'Select Id, Name, RecordTypeId, RecordType.Name, Application__c, Case__c, Case__r.Status, Case__r.Stage__c, Case__r.Partition__c, Case__r.Channel__c, Role_Type__c, Account__c, Account__r.FirstName, Account__r.LastName, Visa_Sub_Class__c, Visa_Sub_Class__r.Name, Mobile_Phone__c, Marital_Status__c,  Email__c, Date_of_Birth__c, Residential_Situation__c, Full_Address__c ';
        string foShareQuery = ', (Select Id, Asset_Name__c, Expense_Name__c, Income_Name__c, Liability_Name__c, RecordType.Name, FO_Record_Type__c, Monthly_Amount__c, Liability_Limit__c, Role_Share__c, Role_Share_Amount__c, Type__c From FO_Share__r) ';
        string rolePostQuery = ' From Role__c Where Id = : recordId ';
        string rolefsfields = constructQueryFromFieldSets(rolePreQuery, 'Role__c');
        if (rolefsfields != '') rolePreQuery+= ', ';
        wRD.role = database.Query(rolePreQuery + rolefsfields + foShareQuery + rolePostQuery);
        string appID = wRD.role.Application__c;
        wRD.raddrlist = [Select Id, Name, RecordType.Name, RecordTypeId, Active_Address__c, Address__c, Location__c, Full_Address__c, Residential_Situation__c, Start_Date__c, End_Date__c 
                                    From Role_Address__c Where Role__c = : recordId Order By CreatedDate DESC];

        //Retrieve related FOs
        string appQuery = 'Select Id, Case__c, Case__r.RecordTypeId, Case__r.RecordType.Name, Case__r.Status, Case__r.Partition__c, Case__r.Channel__c, Case__r.Stage__c, Primary_Applicant__c, RecordType.Name, Name, ';
        string incQuery = '( ' + foQueryFields('Income1__c') + ' From Incomes1__r Where Role__c = : recordId), ' ; //Income
        string likeRoleID = '%' + recordId.substring(0,15) + '%';
        string expQuery = '( '+ foQueryFields('Expense1__c') + ' From Expenses1__r Where h_FO_Share_Role_IDs__c like : likeRoleID), ' ; //Expense
        string assQuery = '( '+ foQueryFields('Asset1__c') + ' From Assets1__r Where h_FO_Share_Role_IDs__c like : likeRoleID), ' ; //Asset
        string liaQuery = '( '+ foQueryFields('Liability1__c') + ' From Liabilities1__r Where h_FO_Share_Role_IDs__c like : likeRoleID), ' ; //Liability
        string folinkQuery = '(Select Id, Name, h_Case__c, h_Application__c, h_Path_Option__c, h_Last_Saved_Path_Step__c, FO_Source__c, Asset__c, Asset__r.Name, Asset__r.Asset_Value__c, Asset__r.RecordTypeId, Asset__r.RecordType.Name, Expense__c, Expense__r.Name, Expense__r.RecordTypeId, Expense__r.RecordType.Name, Expense__r.Monthly_Payment__c, Income__c, Income__r.Name, Income__r.RecordTypeId, Income__r.RecordType.Name, Income__r.Monthly_Income__c, Liability__c, Liability__r.Name, Liability__r.RecordTypeId, Liability__r.RecordType.Name, Liability__r.Amount_Owing__c, Asset_Type__c, Liability_Type__c, Income_Type__c, Expense_Type__c From FO_Links__r Order By Name ASC) '; //Child FO Links 
        
        appQuery+= incQuery + expQuery + assQuery + liaQuery + folinkQuery;
        appQuery+= ' From Application__c Where Id = : appID ';
        map <Id, FO_Link__c> foLinkMap = new map <Id, FO_Link__c>();
        set <ID> incFOset = new set <ID>();
        set <ID> expFOset = new set <ID>();
        set <ID> assFOset = new set <ID>();
        set <ID> liaFOset = new set <ID>();
        Application__c app = database.Query(appQuery);
        if (app.Incomes1__r.size() > 0){
            wRD.inclist = new list <incomeWrapper>();
            for (Income1__c inc : app.Incomes1__r){
                incomeWrapper incWr = new incomeWrapper();
                incWr.inc = inc;
                incWr.ogid = inc.Id;
                wRD.inclist.add(incWr);
                incFOset.add(inc.Id);
            }
            for (FO_Link__c folink : app.FO_Links__r){
                if (incFOset.contains(foLink.Income__c)){
                    if (!foLinkMap.containskey(folink.Id)) foLinkMap.put(foLink.Id, foLink);
                }
            }
        }
        if (app.Expenses1__r.size() > 0){
            wRD.explist = new list <expenseWrapper>();
            for (Expense1__c exp : app.Expenses1__r){
                expenseWrapper expWr = new expenseWrapper();
                expWr.exp = exp;
                expWr.ogid = exp.Id;
                wRD.explist.add(expWr);
                expFOset.add(exp.Id);
            }
            for (FO_Link__c folink : app.FO_Links__r){
                if (expFOset.contains(foLink.Expense__c)){
                    if (!foLinkMap.containskey(folink.Id)) foLinkMap.put(foLink.Id, foLink);
                }
            }
        }
        if (app.Assets1__r.size() > 0){
            wRD.asslist = new list <assetWrapper>();
            for (Asset1__c ass : app.Assets1__r){
                assetWrapper assWr = new assetWrapper();
                assWr.ass = ass;
                assWr.ogid = ass.Id;
                wRD.asslist.add(assWr);
                assFOset.add(ass.Id);
            }
            for (FO_Link__c folink : app.FO_Links__r){
                if (assFOset.contains(foLink.Asset__c)){
                    if (!foLinkMap.containskey(folink.Id)) foLinkMap.put(foLink.Id, foLink);
                }
            }
        }
        if (app.Liabilities1__r.size() > 0){
            wRD.lialist = new list <liabilityWrapper>();
            for (Liability1__c lia : app.Liabilities1__r){
                liabilityWrapper liaWr = new liabilityWrapper();
                liaWr.lia = lia;
                liaWr.ogid = lia.Id;
                wRD.lialist.add(liaWr);
                liaFOset.add(lia.Id);
            }
            for (FO_Link__c folink : app.FO_Links__r){
                if (liaFOset.contains(foLink.Liability__c)){
                    if (!foLinkMap.containskey(folink.Id)) foLinkMap.put(foLink.Id, foLink);
                }
            }
        }
        if (wRD.role.FO_Share__r.size() > 0){
            wRD.fosharelist = new list <FO_Share__c>();
            for (FO_Share__c fos : wRD.role.FO_Share__r){
                wRD.fosharelist.add(fos);
            }
        }
        if (foLinkMap.size() > 0){
            wRD.foLinklist = new list <FO_Link__c>();
            //Filtered FO Links base from Role's FO
            for (FO_Link__c folink : foLinkMap.values()){
                wRD.foLinklist.add(foLink);
            }
        }

        wRD.roleRTsWrlist = new list <roleRTWrapper>();
        for (RecordType rt : [Select Id, Name, DeveloperName From RecordType Where SobjectType = 'Role__c' AND (Name = 'Applicant - Individual' OR Name = 'Applicant - Sole Trader')]){
            roleRTWrapper rrtWr = new roleRTWrapper();
            rrtWr.name = rt.Name;
            rrtWr.id = rt.Id;
            wRD.roleRTsWrlist.add(rrtWr);
        }

        wRD.disregardedFieldsOnClone = disregardedFields('Role__c', disregardedFieldSet);

        return wRD;
    }

    @AuraEnabled
    public static Role__c cloneRole(Role__c newRole){
        insert newRole;
        Role__c creRole = [Select Id, Case__c, Application__c From Role__c Where Id = : newRole.Id];
        return creRole;
    }

    @AuraEnabled
    public static list <Role_Address__c> cloneRoleAddresses(list <Role_Address__c> roleAddresslist){
        Savepoint sp = Database.setSavepoint();
        try{
            insert roleAddresslist;
        }
        catch(Exception e){
            Database.rollback( sp );  
            system.debug('@@Error: '+e.getLineNumber()+e.getMessage());
            AuraHandledException ae = new AuraHandledException('Error: '+ e.getLineNumber()+e.getMessage());
            ae.setMessage('Error: '+ e.getLineNumber()+e.getMessage());
            throw ae;
        }
        return roleAddresslist;
    }

    @AuraEnabled 
    public static list <incomeWrapper> cloneIncome(list <incomeWrapper> incWrlist){
        Savepoint sp = Database.setSavepoint();
        system.debug('@@incWrlist: '+incWrlist);
        list <incomeWrapper> newincWrlist = new list <incomeWrapper>();
        map <string, Income1__c> newincMap = new map <string, Income1__c>();
        if (incWrlist.size() > 0){ 
            for (incomeWrapper incWr : incWrlist){
                if (!newIncMap.containskey(incWr.ogid)) newIncMap.put(incWr.ogId, incWr.inc);
            }
            if (newIncMap.size() > 0){
                try{
                    insert newIncMap.values();
                }
                catch(Exception e){
                    Database.rollback( sp );  
                    system.debug('@@Error: '+e.getLineNumber()+e.getMessage());
                    AuraHandledException ae = new AuraHandledException('Error: '+ e.getLineNumber()+e.getMessage());
                    ae.setMessage('Error: '+ e.getLineNumber()+e.getMessage());
                    throw ae;
                }
            }
        }
        for (String incKey : newIncMap.keySet()){
            incomeWrapper incWR = new incomeWrapper();
            incWr.inc = newIncMap.get(inckey);
            incWr.ogid = inckey;
            newincWrlist.add(incWr);
        }
        system.debug('@@newincWrlist: '+newincWrlist);
        return newincWrlist;
    }

    @AuraEnabled 
    public static list <expenseWrapper> cloneExpenses(list <expenseWrapper> expWrlist){
        Savepoint sp = Database.setSavepoint();
        list <expenseWrapper> newexpWrlist = new list <expenseWrapper>();
        map <string, Expense1__c> newexpMap = new map <string, Expense1__c>();
        if (expWrlist.size() > 0){ 
            for (expenseWrapper expWr : expWrlist){
                if (!newexpMap.containskey(expWr.ogid)) newexpMap.put(expWr.ogId, expWr.exp);
            }
            if (newexpMap.size() > 0){
                try{
                    insert newexpMap.values();
                }
                catch(Exception e){
                    Database.rollback( sp );  
                    system.debug('@@Error: '+e.getLineNumber()+e.getMessage());
                    AuraHandledException ae = new AuraHandledException('Error: '+ e.getLineNumber()+e.getMessage());
                    ae.setMessage('Error: '+ e.getLineNumber()+e.getMessage());
                    throw ae;
                }
            }
        }
        for (String expKey : newexpMap.keySet()){
            expenseWrapper expWr = new expenseWrapper();
            expWr.exp = newexpMap.get(expKey);
            expWr.ogid = expKey;
            newexpWrlist.add(expWr);
        }
        return newexpWrlist;
    }

    @AuraEnabled 
    public static list <assetWrapper> cloneAssets(list <assetWrapper> assWrlist){
        Savepoint sp = Database.setSavepoint();
        list <assetWrapper> newassWrlist = new list <assetWrapper>();
        map <string, Asset1__c> newAssMap = new map <string, Asset1__c>();
        if (assWrlist.size() > 0){ 
            for (assetWrapper assWr : assWrlist){
                if (!newAssMap.containskey(assWr.ogid)) newAssMap.put(assWr.ogId, assWr.ass);
            }
            if (newAssMap.size() > 0){
                try{
                    insert newAssMap.values();
                }
                catch(Exception e){
                    Database.rollback( sp );  
                    system.debug('@@Error: '+e.getLineNumber()+e.getMessage());
                    AuraHandledException ae = new AuraHandledException('Error: '+ e.getLineNumber()+e.getMessage());
                    ae.setMessage('Error: '+ e.getLineNumber()+e.getMessage());
                    throw ae;
                }
            }
        }
        for (String assKey : newAssMap.keySet()){
            assetWrapper assWr = new assetWrapper();
            assWr.ass = newAssMap.get(assKey);
            assWr.ogId = assKey;
            newassWrlist.add(assWr);
        }
        return newassWrlist;
    }

    @AuraEnabled 
    public static list <liabilityWrapper> cloneLiabilities(list <liabilityWrapper> liaWrlist){
        Savepoint sp = Database.setSavepoint();
        list <liabilityWrapper> newLiaWrlist = new list <liabilityWrapper>();
        map <string, Liability1__c> newLiaMap = new map <string, Liability1__c>();
        if (liaWrlist.size() > 0){ 
            for (liabilityWrapper liaWr : liaWrlist){
                if (!newLiaMap.containskey(liaWr.ogid)) newLiaMap.put(liaWr.ogId, liaWr.lia);
            }
            if (newLiaMap.size() > 0){
                try{
                    insert newLiaMap.values();
                }
                catch(Exception e){
                    Database.rollback( sp );  
                    system.debug('@@Error: '+e.getLineNumber()+e.getMessage());
                    AuraHandledException ae = new AuraHandledException('Error: '+ e.getLineNumber()+e.getMessage());
                    ae.setMessage('Error: '+ e.getLineNumber()+e.getMessage());
                    throw ae;
                }
            }
        }
        for (String liaKey : newLiaMap.keySet()){
            liabilityWrapper liaWr = new liabilityWrapper();
            liaWr.lia = newLiaMap.get(liaKey);
            liaWr.ogId = liaKey;
            newLiaWrlist.add(liaWr);
        }
        return newLiaWrlist;
    }

    @AuraEnabled
    public static list <FO_Link__c> cloneFOLinks(list <incomeWrapper> incWrlist, 
                                                 list <expenseWrapper> expWrlist, 
                                                 list <assetWRapper> assWrlist,
                                                 list <liabilityWrapper> liaWrlist,
                                                 list <FO_Link__c> folinklist,
                                                 ID appID,
                                                 ID caseID){      
        Savepoint sp = Database.setSavepoint();
        list <FO_Link__c> newFOLinklist = new list <FO_Link__c>();
        map <Id, Id> FOIdsMap = new map <Id, Id>();
        for (incomeWrapper incWr : incWrlist){
            FOIdsMap.put(incWr.ogid, incWr.inc.Id);
        }
        for (expenseWrapper expWr : expWrlist){
            FOIdsMap.put(expWr.ogid, expWr.exp.Id);
        }
        for (assetWrapper assWr : assWrlist){
            FOIdsMap.put(assWr.ogid, assWr.ass.Id);
        }
        for (liabilityWrapper liaWr : liaWrlist){
            FOIdsMap.put(liaWr.ogid, liaWr.lia.Id);
        }
        if (folinklist != null){
            for (FO_Link__c foLink : folinklist ){
                foLink.h_Application__c = appID;
                foLink.h_Case__c = caseID;
                if (FOIdsMap.containskey(foLink.Income__c)) foLink.Income__c = FOIdsMap.get(foLink.Income__c); //Set cloned Income ID
                else foLink.Income__c = null; //Remove existing FO reference
                if (FOIdsMap.containskey(foLink.Expense__c)) foLink.Expense__c = FOIdsMap.get(foLink.Expense__c); //Set cloned Expense ID
                else foLink.Expense__c = null; //Remove existing FO reference
                if (FOIdsMap.containskey(foLink.Asset__c)) foLink.Asset__c = FOIdsMap.get(foLink.Asset__c); //Set cloned Asset ID
                else foLink.Asset__c = null; //Remove existing FO reference
                if (FOIdsMap.containskey(foLink.Liability__c)) foLink.Liability__c = FOIdsMap.get(foLink.Liability__c); //Set cloned Liability ID
                else foLink.Liability__c = null; //Remove existing FO reference
                newFOLinklist.add(foLink);
            }
        }
        if (newFOLinklist.size() > 0){
            try{
                insert newFOLinklist;
            }
            catch(Exception e){
                Database.rollback( sp );  
                system.debug('@@Error: '+e.getLineNumber()+e.getMessage());
                AuraHandledException ae = new AuraHandledException('Error: '+ e.getLineNumber()+e.getMessage());
                ae.setMessage('Error: '+ e.getLineNumber()+e.getMessage());
                throw ae;
            }
        }
        return newFOLinklist;
    }

    private static string foQueryFields(string foAPIName){
        string foPreQuery = ' Select Id, h_Auto_Create_FO_Share__c, Equal_Share_for_Roles__c, RecordTypeId, RecordType.Name ';
        if (foAPIName != 'Income1__c') foPreQuery+= ', h_FO_Share_Role_IDs__c';
        if (foAPIName == 'Income1__c') foPreQuery+= ', Income_Type__c, Lead_Income_Type__c, Lead_Employment_Situation__c, Lead_Employment_Type__c';
        if (foAPIName == 'Expense1__c') foPreQuery+= ', Expense_Type__c, Lead_Expense_Group__c';
        if (foAPIName == 'Asset1__c') foPreQuery+= ', Asset_Type__c';
        if (foAPIName == 'Liability1__c') foPreQuery+= ', Liability_Type__c';
        string fofsfields = constructQueryFromFieldSets(foPreQuery, foAPIName);
        if (fofsfields != '') foPreQuery+= ', ';
        string foQuery = foPreQuery + fofsfields;
        return foQuery;
    }

    private static list <string> disregardedFields(string objectname, list <string> fieldsetnames){
        list <string> disregardedFields = new list <string>();
        if (!fieldsetnames.isEmpty()){
            Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectname);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            for (string fsname : fieldsetnames){
                Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fsname);
                List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
                for (Schema.FieldSetMember fsm : fieldSetMemberList){
                    if (!disregardedFields.contains(fsm.getFieldPath())) disregardedFields.add(fsm.getFieldPath()); //get Field API Name
                }
            }
        }
        return disregardedFields;
    }

    private static string constructQueryFromFieldSets(String objpreQuery, String ObjectName){

        string FSFields = ' ';
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        map <string, Schema.FieldSet> FOfsMap = DescribeSObjectResultObj.FieldSets.getMap();
        for (Schema.FieldSet fs : FOfsMap.values()){ //Check all FieldSets in object
            for (Schema.FieldSetMember field : fs.getFields()){ //get Fields on a specific Field Set
                string fp = field.getFieldPath();
                if (!objpreQuery.contains(fp) && !FSFields.contains(fp) ){ //Prevents duplicate fields in query
                    FSFields+= fp + ',';
                }
            }
        } 
        FSFields = FSFields.subString(0,FSFields.Length()-1); //trim last comma
        system.debug('@@FSFields:'+FSFields);
        return FSFields;

    }

    public class wrappedRoleData{
        @AuraEnabled public Role__c role {get;set;}
        @AuraEnabled public list <Role_Address__c> raddrlist {get;set;}
        @AuraEnabled public list <incomeWrapper> inclist {get;set;}
        @AuraEnabled public list <expenseWrapper> explist {get;set;}
        @AuraEnabled public list <assetWrapper> asslist {get;set;}
        @AuraEnabled public list <liabilityWrapper> lialist {get;set;}
        @AuraEnabled public list <FO_Share__c> fosharelist {get;set;}
        @AuraEnabled public list <FO_Link__c> foLinklist {get;set;}
        @AuraEnabled public list <roleRTWrapper> roleRTsWrlist {get;set;}
        @AuraEnabled public list <string> disregardedFieldsOnClone {get;set;}
    }

    public class roleRTWrapper{
        @AuraEnabled public string name {get;set;}
        @AuraEnabled public string id {get;set;}
    }

    public class incomeWrapper{
        @AuraEnabled public Income1__c inc {get;set;}
        @AuraEnabled public String ogid {get;set;}
    }

    public class expenseWrapper{
        @AuraEnabled public Expense1__c exp {get;set;}
        @AuraEnabled public String ogid {get;set;}
    }

    public class assetWrapper{
        @AuraEnabled public Asset1__c ass {get;set;}
        @AuraEnabled public String ogid {get;set;}
    }

    public class liabilityWrapper{
        @AuraEnabled public Liability1__c lia {get;set;}
        @AuraEnabled public String ogid {get;set;}
    }

}
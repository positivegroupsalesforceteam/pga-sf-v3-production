@isTest
public class EditRoleControllerTest {
    public static List<Account> accList = new List<Account>();
    public static List<Role__c> roleList = new List<Role__c>();
    @testSetup static void setupData() {
		//Create Test Account
		accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);
        
		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);

		//Create Test Role 
		roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
		Database.insert(roleList);
	}
    static testMethod void testNewRoleVF() {
        Test.startTest();
            PageReference editRolePage = Page.EditRolePage;
            Case caseRec = [SELECT Id,Application_Name__c FROM Case LIMIT 1];
            List<Account> personAccList = TestDataFactory.createGenericPersonAccount('Test First', 'TestLast', 'nm: Individual',1);
            Database.insert(personAccList);

            roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseRec.Id, personAccList[0].Id, caseRec.Application_Name__c,  1);
            Database.insert(roleList);
            roleList = [SELECT Id,Account__r.isPersonAccount FROM Role__c];

            System.assertNotEquals(personAccList.size(),0);
            System.assertNotEquals(caseRec,new Case());

            for(Role__c role : roleList){
                System.debug('role IsPersonAccount = '+role.Account__r.isPersonAccount);
            }

            for(Account acc : [SELECT Id,FirstName,LastName,PersonEmail, isPersonAccount FROM Account]){
                System.debug('isPersonAccount = '+acc);
            }

            Test.setCurrentPage(editRolePage);
        	Test.setCurrentPageReference(editRolePage); 
    
            System.currentPageReference().getParameters().put('id', caseRec.Id);

            EditRoleController editRoleCtrl = new EditRoleController();
            editRoleCtrl.rowId = roleList[0].Id;
            editRoleCtrl.setRowValues();
            editRoleCtrl.saveRole();
            editRoleCtrl.cancel();
        Test.stopTest();
    }
}
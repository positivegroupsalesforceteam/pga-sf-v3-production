public class IncomeProcessor extends DefaultProcessor {
    public override boolean process() {
        if(!super.process())
            return false;
        List<LKRecord> incList = XMLMappingAdapter.lkRecordsMap.get('Income');
        List<LKRecord> emplRecList = XMLMappingAdapter.lkRecordsMap.get('Employment');
        if(incList != null && incList.size() > 0) {
            for(LKRecord incRec : incList) {
                XMLMapping mappObj = incRec.getMappingObject();
                String applicantSfId = (String)incRec.lkValuesMap.get('Applicant_2_Mapping__c');
                String applicantLKId = null;
                if(applicantSfId != null) {
                    applicantLKId = LKintegration.coBorrIdMap.get(applicantSfId);
                    incRec.applicantLKLink = applicantLKId;
                }
                String addbackType = (String)incRec.lkValuesMap.get('add_back_type');
                if(addbackType != null) {
                    addbackType.replace('-','');
                    addbackType.deleteWhitespace();
                }
                String incType = (String)incRec.lkValuesMap.get('income_type');
                if(incType != null) {
                    if(incType.equalsIgnoreCase('Gross Yearly Income'))
                        incType = 'GrossIncome';
                    incType = incType.replace(' ','');  
                    incRec.lkValuesMap.put('income_type',incType);
                }
                if(emplRecList != null) {
                    for(LKRecord emplRec : emplRecList) {
                        if(emplRec.sfObjectId.equalsIgnoreCase(incRec.sfObjectId)) {
                            incRec.lkValuesMap.put('employment_id',emplRec.lkObjectId);
                        }
                    }
                }
            }
        }
        return true;
    }
}
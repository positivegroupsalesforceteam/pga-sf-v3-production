/** 
* @FileName: Common Constants
* @Description: class containing the common constants
* @Copyright: Positive (c) 2017 
* @author: Rex David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 12/15/17 RDAVID - Created Class
* 1.1 02/20/18 JBACULOD - Added RT Label Constants for Account, Case, Role, Application and Financial Objects (incomes, expenses, assets, liabilties)
* 1.2 03/23/18 JBACULOD - Updated Account RT Labels
* 1.3 03/24/18 JBACULOD - Added Case Scenario RTs, Stages Labels
* 1.4 03/27/18 JBACULOD - Added Prefix labels for checking grouped RTs, EasyCase labels
* 1.5 04/04/18 JBACULOD - Added constants for Old Case Record Type Names
* 1.6 04/16/18 JBACULOD - Updated RT Labels of FO (Income, Expense, Asset, Liability)
* 1.7 04/19/18 JBACULOD - Added new RT label for Asset (Home Content)
* 1.8 04/23/18 JBACULOD - Added new RT label for Application (Commercial - Partnership)
* 1.9 04.24.18 JBACULOD - Added new RT Label for Role (Commercial Loan - Partnership)
* 2.0 05/21/18 JBACULOD - Reconstructed CommonConstants due to major changes in Record Types
* 2.1 08/24/18 JBACULOD - Added new RT Label for Role (Purchaser - Risk Insurance) and Case (PWM - Insurance App)
* 2.2 2/05/19 RDAVID - extra/task24399056-InitialReviewSRandSLA - Added Support Request RT "NOD Initial Review" 
**/ 
public class CommonConstants {

    public static final String TASK_CALLTYPE_OUTBOUND = 'Outbound';
    public static final String TASK_STATUS_COMPLETED = 'Completed';

    //Prefixes
    public static final String PREFIX_COMM = 'Commercial'; //Label.Prefix_Commercial; //Commercial
    public static final String PREFIX_CONS = 'Consumer'; //Label.Prefix_Consumer; //Consumer
    public static final String PREFIX_N = 'NOD'; //Label.Prefix_n_Nodifi; //n:
    public static final String PREFIX_P = 'POS'; //Label.Prefix_p_PLS; //p:
    public static final String PREFIX_PWM = 'PWM'; //Label.Prefix_p_PLS; //p:
    public static final String PREFIX_KAR = 'KAR'; //Label.Prefix_p_PLS; //p:
    public static final String PREFIX_Z = 'z:'; //Label.Prefix_z_Others; //z:

    //Account Record Type Labels
    /*public static final String ACC_RT_PARTNERSHIP = Label.Account_RT_nm_Partnership; //nm: Partnership
    public static final String ACC_RT_COMPANY = Label.Account_RT_nm_Company; //nm: Company
    public static final String ACC_RT_TRUST_CAT = Label.Account_RT_nm_Trust_Company_as_Trustee; //nm: Trust - Company as Trustee
    public static final String ACC_RT_TRUST_IAT = Label.Account_RT_nm_Trust_Individual_as_Trustee; //nm: Trust - Individual as Trustee
    public static final String ACC_RT_L_BA = Label.Account_RT_nm_Lead_Business_Account; //nm: Lead - Business Account
    public static final String PACC_RT_I = Label.Account_RT_nm_Individual; //nm : Individual
    public static final String PACC_RT_L_IA = Label.Account_RT_nm_Lead_Individual_Account; //nm: Lead - Individual Account */

    //Account Record Type Names (Business)
    public static final String ACC_RT_COMPANY = 'nm: Company';
    public static final String ACC_RT_PARTNERSHIP = 'nm: Partnership';
    public static final String ACC_RT_TRUST_CAT = 'nm: Trust'; //'nm: Trust - Company as Trustee';
    public static final String ACC_RT_TRUST_IAT = 'nm: Trust'; //'nm: Trust - Individual as Trustee';
    public static final String ACC_RT_TRUST = 'nm: Trust'; //merged RT for Trust
    public static final String ACC_RT_L_BA = 'nm: Lead - Business';
    //Account Record Type Names (Person)
    public static final String PACC_RT_I = 'nm: Individual';
    public static final String PACC_RT_L_IA = 'nm: Lead - Individual';
    public static final String PACC_RT_Z_REF = 'nm: zReference';

    //Role Record Type Labels
    /*public static final String ROLE_RT_COMM_L_BO = Label.Role_RT_Commercial_Loan_Beneficial_Owner; //Commercial Loan - Beneficial Owner
    public static final String ROLE_RT_COMM_L_C = Label.Role_RT_Commercial_Loan_Company; //Commercial Loan - Company
    public static final String ROLE_RT_COMM_L_G = Label.Role_RT_Commercial_Loan_Guarantor; //Commercial Loan - Guarantor
    public static final String ROLE_RT_COMM_L_P = Label.Role_RT_Commercial_Loan_Partnership; //Commercial Loan - Partnership
    //public static final String ROLE_RT_COMM_L_PC = Label.Role_RT_Commercial_Loan_Partnership_Complex; //Commercial Loan- Partnership (Complex)
    //public static final String ROLE_RT_COMM_L_PS = Label.Role_RT_Commercial_Loan_Partnership_Simple; //Commercial Loan - Partnership (Simple)
    public static final String ROLE_RT_COMM_L_ST = Label.Role_RT_Commercial_Loan_Sole_Trader; //Commercial Loan - Sole Trader
    public static final String ROLE_RT_COMM_L_TC = Label.Role_RT_Commercial_Loan_Trust_Company; //Commercial Loan - Trust (Company)
    public static final String ROLE_RT_COMM_L_TI = Label.Role_RT_Commercial_Loan_Trust_Individual; //Commercial Loan - Trust (Individual)
    public static final String ROLE_RT_CONS_L_G = Label.Role_RT_Consumer_Loan_Guarantor; //Consumer Loan - Guarantor
    public static final String ROLE_RT_CONS_L_I = Label.Role_RT_Consumer_Loan_Individual; //Consumer Loan - Individual
    public static final String ROLE_RT_CONS_L_J = Label.Role_RT_Consumer_Loan_Joint; //Consumer Loan - Joint
    public static final String ROLE_RT_P = Label.Role_RT_Purchaser; //Purchaser
    public static final String ROLE_RT_RP = Label.Role_RT_Related_Party; //Related Party
    public static final String ROLE_RT_SSS = Label.Role_RT_Spouse_Supporting_Servicing; //Spouse Supporting Servicing
    public static final String ROLE_RT_V = Label.Role_RT_Vendor; //Vendor
    public static final String ROLE_RT_LBA = Label.Role_RT_Lead_Business_Account; //Lead - Business Account
    public static final String ROLE_RT_LIA = Label.Role_RT_Lead_Individual_Account; //Lead - Individual Account
    public static final String ROLE_RT_ZLEAD = 'zLead'; //'zLead' */

    //Role Record Type Names
    public static final String ROLE_RT_BEN_O_INDI = 'Beneficial Owner - Individual';
    public static final String ROLE_RT_BEN_O_BUSI = 'Beneficial Owner - Business';
    public static final String ROLE_RT_APP_COMP = 'Applicant - Company';
    public static final String ROLE_RT_APP_PART = 'Applicant - Partnership';
    public static final String ROLE_RT_APP_TRUS = 'Applicant - Trust';
    public static final String ROLE_RT_APP_SOLE = 'Applicant - Sole Trader';
    public static final String ROLE_RT_APP_INDI = 'Applicant - Individual';
    public static final String ROLE_RT_PAR_INDI = 'Partner - Individual';
    public static final String ROLE_RT_PAR_BUSI = 'Partner - Business';
    public static final String ROLE_RT_GUA_DIRE = 'Guarantor - Director';
    public static final String ROLE_RT_GUA_MORT = 'Guarantor - Mortgage';
    public static final String ROLE_RT_TRU_INDI = 'Trustee - Individual';
    public static final String ROLE_RT_TRU_COMP = 'Trustee - Company';
    public static final String ROLE_RT_LEA_INDI = 'Lead - Individual';
    public static final String ROLE_RT_LEA_BUSI = 'Lead - Business';
    public static final String ROLE_RT_LEA_GREE = 'Lead - Green';
    public static final String ROLE_RT_PURC_INSU = 'Purchaser - Insurance Only';
    public static final String ROLE_RT_PURC_SOUR = 'Purchaser - Sourcing';
    public static final String ROLE_RT_PURC_RISK = 'Purchaser - Risk Insurance';
    public static final String ROLE_RT_REL_PART = 'Related Party';
    public static final String ROLE_RT_NON_B_SPOUSE = 'Non-Borrowing Spouse';
    public static final String ROLE_RT_VEND_DEAL_CONT = 'Vendor - Dealer Contact';
    public static final String ROLE_RT_VEND_PRIV_SELL = 'Vendor - Private Seller';

    //Case Record Type Labels
    /* public static final String CASE_RT_OM_COMM_LOAN = 'Commercial Loan'; //Case Record Type from Old Model
    public static final String CASE_RT_OM_CONS_LOAN = 'Consumer Loan'; //Case Record Type from Old Model
    public static final String CASE_RT_OM_SERV = 'Service'; //Case Record Type from Old Model
    public static final String CASE_RT_N_COMM_AF = Label.Case_RT_N_Commercial_Asset_Finance; //n: Commercial - Asset Finance
    public static final String CASE_RT_N_COMM_BF = Label.Case_RT_N_Commercial_Business_Funding; //n: Commercial - Business Funding
    public static final String CASE_RT_N_COMM_PF = Label.Case_RT_N_Commercial_Property_Finance; //n: Commercial - Property Finance
    public static final String CASE_RT_N_COMPSALE = Label.Case_RT_N_Compound_Sale; //n: Compound Sale
    public static final String CASE_RT_N_CONS_AF = Label.Case_RT_N_Consumer_Asset_Finance; //n: Consumer - Asset Finance
    public static final String CASE_RT_N_CONS_PL = Label.Case_RT_N_Consumer_Personal_Loan; //n: Consumer - Personal Loan
    public static final String CASE_RT_P_COMM_AF = Label.Case_RT_P_Commercial_Asset_Finance; //p: Commercial - Asset Finance
    public static final String CASE_RT_P_COMM_BF = Label.Case_RT_P_Commercial_Business_Funding; //p: Commercial - Business Funding
    public static final String CASE_RT_P_COMPSALE = Label.Case_RT_P_Compound_Sale; //p: Compound Sale
    public static final String CASE_RT_P_CONS_AF = Label.Case_RT_P_Consumer_Asset_Finance; //p: Consumer - Asset Finance
    public static final String CASE_RT_P_CONS_PL = Label.Case_RT_P_Consumer_Personal_Loan; //p: Consumer - Personal Loan
    public static final String CASE_RT_P_CONS_PF = Label.Case_RT_P_Consumer_Property_Finance; //p: Consumer - Property Finance
    //public static final String CASE_RT_P_USEDCARS = Label.Case_RT_P_Used_Cars; //u: Used Cars
    public static final String CASE_RT_U_USEDCARS = Label.Case_RT_U_Used_Cars; //u: Used Cars
    public static final String CASE_RT_Z_C = Label.Case_RT_Z_Complaints; //z: Complaints
    public static final String CASE_RT_Z_SG = Label.Case_RT_Z_Service_Generic; //z: Service Generic
    public static final String CASE_RT_N_COMMERCIAL_SCENARIO = Label.Case_RT_N_Commercial_Scenario; //n: Commercial Scenario
    public static final String CASE_RT_N_CONSUMER_SCENARIO = Label.Case_RT_N_Consumer_Scenario; //n: Consumer Scenario
    public static final String CASE_RT_PZ_ASSET_LOANLEAD = 'pz: Asset Loan Lead'; 
    public static final String CASE_RT_PZ_PROPERTY_LOANLEAD = 'pz: Property Loan Lead';
    public static final String CASE_RT_NZ_ASSET_LOANLEAD = 'nz: Asset Loan Lead'; */

    //Case Record Type Names
    public static final String CASE_RT_OM_COMM_LOAN = 'Commercial Loan'; //Case Record Type from Old Model
    public static final String CASE_RT_OM_CONS_LOAN = 'Consumer Loan'; //Case Record Type from Old Model
    public static final String CASE_RT_OM_SERV = 'Service'; //Case Record Type from Old Model
    public static final String CASE_RT_N_COMM_AF = 'NOD: Commercial - Asset Finance';
    public static final String CASE_RT_N_COMM_BF = 'NOD: Commercial - Business Funding';
    public static final String CASE_RT_N_CONS_AF = 'NOD: Consumer - Asset Finance';
    public static final String CASE_RT_N_CONS_PL = 'NOD: Consumer - Personal Loan';
    public static final String CASE_RT_N_COMMERCIAL_SCENARIO = 'NOD: Commercial Scenario';
    public static final String CASE_RT_N_CONSUMER_SCENARIO = 'NOD: Consumer Scenario';
    public static final String CASE_RT_P_COMM_AF = 'POS: Commercial - Asset Finance';
    public static final String CASE_RT_P_COMM_BF = 'POS: Commercial - Business Funding';
    public static final String CASE_RT_P_CONS_AF = 'POS: Consumer - Asset Finance';
    public static final String CASE_RT_P_CONS_PL = 'POS: Consumer - Personal Loan';
    public static final String CASE_RT_P_CONS_PF = 'POS: Consumer - Property Finance';
    public static final String CASE_RT_U_USEDCARS = 'UCA: Used Cars';
    public static final String CASE_RT_Z_C = 'z: Complaints';
    public static final String CASE_RT_Z_SG = 'z: Service Generic';
    public static final String CASE_RT_PWM_IA = 'PWM: Insurance App';
    public static final String CASE_RT_KAR_VS = 'KAR: Vehicle Sourcing';
    
    //Application Record Type Labels
    /* public static final String APP_RT_COMM_C = Label.Application_RT_Commercial_Company; //Commercial - Company
    public static final String APP_RT_COMM_P = Label.Application_RT_Commercial_Partnership; //Commercial - Partnership
    //public static final String APP_RT_COMM_PC = Label.Application_RT_Commercial_Partnership_Complex; //Commercial - Partnership (Complex)
    //public static final String APP_RT_COMM_PS = Label.Application_RT_Commercial_Partnership_Simple; //Commercial - Partnership (Simple)
    public static final String APP_RT_COMM_ST = Label.Application_RT_Commercial_Sole_Trader; //Commercial - Sole Trader
    public static final String APP_RT_COMM_TC = Label.Application_RT_Commercial_Trust_Company; //Commercial - Trust (Company)
    public static final String APP_RT_COMM_TI = Label.Application_RT_Commercial_Trust_Individual; //Commercial - Trust (Individual)
    public static final String APP_RT_CONS_I = Label.Application_RT_Consumer_Individual; //Consumer - Individual
    public static final String APP_RT_CONS_J = Label.Application_RT_Consumer_Joint; //Consumer - Joint   */

    //Application Record Type Names
    public static final String APP_RT_COMM_C = 'Commercial - Company';
    public static final String APP_RT_COMM_P = 'Commercial - Partnership';
    public static final String APP_RT_COMM_ST = 'Commercial - Sole Trader';
    public static final String APP_RT_COMM_TC = 'Commercial - Trust - Company Trustee';
    public static final String APP_RT_COMM_TI = 'Commercial - Trust - Individual Trustee';
    public static final String APP_RT_CONS_I = 'Consumer - Individual';
    public static final String APP_RT_CONS_J = 'Consumer - Joint';
    public static final String APP_RT_CONS_M = 'Consumer - Multi';
    public static final String APP_RT_CONS_ZA = 'zAPP - Consumer';
    public static final String APP_RT_COMM_ZA = 'zAPP - Commercial';

    //Purchased Asset Record Type Names
    public static final String PURC_ASS_RT_AM = 'Agricultural Machinery';
    public static final String PURC_ASS_RT_RE = 'Real Estate';
    public static final String PURC_ASS_RT_M_E = 'Marine - Engine';
    public static final String PURC_ASS_RT_MOT_V = 'Motor Vehicle';
    public static final String PURC_ASS_RT_OSNA = 'Other Serial Numbered Asset';
    public static final String PURC_ASS_RT_M_V = 'Marine - Vessel';
    public static final String PURC_ASS_RT_V_H = 'Vehicle - Heavy';
    public static final String PURC_ASS_RT_V_L = 'Vehicle - Leisure';
    public static final String PURC_ASS_RT_V_LT = 'Vehicle - Light Trailer';
    public static final String PURC_ASS_RT_YG = 'Yellow Goods';
    public static final String PURC_ASS_RT_ZOLD_M = 'Marine';
    public static final String PURC_ASS_RT_ZOLD_V = 'Vehicle';
    public static final String PURC_ASS_RT_ZOLD_OA = 'Other Asset';
    

    //FO Share RTs
    public static final String FO_INCOME = 'Income';
    public static final String FO_EXPENSE = 'Expense';
    public static final String FO_ASSET = 'Asset';
    public static final String FO_LIABILITY = 'Liability';

    //Asset1 Record Type Labels
    /* public static final String ASSET_RT_P = Label.Asset_RT_Property; //Property
    public static final String ASSET_RT_IN = Label.Asset_RT_Insurance; //Insurance
    public static final String ASSET_RT_V = Label.Asset_RT_Vehicle; //Vehicle
    public static final String ASSET_RT_I = Label.Asset_RT_Investment; //Investment
    public static final String ASSET_RT_OA = Label.Asset_RT_Other_Asset; //Other Asset
    public static final String ASSET_RT_SE = Label.Asset_RT_Stock_Equipment; //Stock & Equipment
    public static final String ASSET_RT_CIB = Label.Asset_RT_Cash_in_Bank; //Cash In Bank
    public static final String ASSET_RT_HC = Label.Asset_RT_Home_Contents; //Home Contents
    /*public static final String ASSET_RT_BA = Label.Asset_RT_Bank_Account; //Bank Account
    public static final String ASSET_RT_E = Label.Asset_RT_Equipment; //Equipment
    public static final String ASSET_RT_O = Label.Asset_RT_Other; //Other
    public static final String ASSET_RT_S = Label.Asset_RT_Shares; //Shares */

    //Asset Record Type Names
    public static final String ASSET_RT_P = 'Property';
    public static final String ASSET_RT_V = 'Vehicle';
    public static final String ASSET_RT_I = 'Investment';
    public static final String ASSET_RT_OA = 'Other Asset';
    public static final String ASSET_RT_SE = 'Stock & Equipment';
    public static final String ASSET_RT_CIB = 'Cash in Bank';
    public static final String ASSET_RT_HC = 'Home Contents';
    public static final String ASSET_RT_ZL = 'UFO';

    //Liabilitiy1 Record Type Labels
    /* public static final String LIABILITY_RT_CC = Label.Liability_RT_Cards_Credit; //Cards & Credit
    public static final String LIABILITY_RT_LA = Label.Liability_RT_Loan_Asset; //Loan - Asset
    public static final String LIABILITY_RT_LO = Label.Liability_RT_Loan_Other; //Loan - Other
    public static final String LIABILITY_RT_LRE = Label.Liability_RT_Loan_Real_Estate; //Loan - Real Estate
    public static final String LIABILITY_OL = Label.Liability_RT_Other_Liability; //Other Liability
    /* public static final String LIABILITY_RT_CC = Label.Liability_RT_Credit_Card; //Credit Card
    public static final String LIABILITY_RT_HD = Label.Liability_RT_HELP_Debt; //HELP Debt
    public static final String LIABILITY_RT_M = Label.Liability_RT_Maintenance; //Maintenance
    public static final String LIABILITY_RT_ML = Label.Liability_RT_Mortgage_Loan; //Mortgage Loan
    public static final String LIABILITY_RT_ORB = Label.Liability_RT_Ongoing_Rent_Board; //Ongoing Rent/Board
    public static final String LIABILITY_RT_O = Label.Liability_RT_Other; //Other
    public static final String LIABILITY_RT_OT = Label.Liability_RT_Outstanding_Taxation; //Outstanding Taxation
    public static final String LIABILITY_RT_OD = Label.Liability_RT_Overdraft; //Overdraft
    public static final String LIABILITY_RT_SL = Label.Liability_RT_Secured_Loan; //Secured Loan
    public static final String LIABILITY_RT_SC = Label.Liability_RT_Store_Card; //Store Card
    public static final String LIABILITY_RT_UL = Label.Liability_RT_Unsecured_Loan; //Unsecured Loan */

    //Liability Record Type Names
    public static final String LIABILITY_RT_CC = 'Cards & Credit';
    public static final String LIABILITY_RT_LA = 'Loan - Asset';
    public static final String LIABILITY_RT_LO = 'Loan - Other';
    public static final String LIABILITY_RT_LRE = 'Loan - Real Estate';
    public static final String LIABILITY_OL = 'Other Liability';
    public static final String LIABILITY_ZL = 'UFO';

    //Expense Record Type Labels
    /*public static final String EXPENSE_RT_AE = Label.Expense_RT_Accomodation_Expenses; //Accommodation Expense
    public static final String EXPENSE_RT_LE = Label.Expense_RT_Living_Expenses; //Living Expenses
    public static final String EXPENSE_RT_CCP = Label.Expense_RT_Card_Credit_Payment; //Card & Credit Payment
    public static final String EXPENSE_RT_LPAF = Label.Expense_RT_Loan_Payment_Asset_Finance; //Loan Payment - Asset Finance
    public static final String EXPENSE_RT_LPO = Label.Expense_RT_Loan_Payment_Other; //Loan Payment - Other
    public static final String EXPENSE_RT_LPPI = Label.Expense_RT_Loan_Payment_Property_Investment; //Loan Payment - Property Investment
    public static final String EXPENSE_RT_OE = Label.Expense_RT_Other_Expense; //Other Expense
    /* public static final String EXPENSE_RT_CSC = Label.Expense_RT_Credit_Store_Cards; //Credit/Store Cards
    public static final String EXPENSE_RT_OGE = Label.Expense_RT_Other_General_Expenses; //Other General Expenses
    public static final String EXPENSE_RT_OLE = Label.Expense_RT_Other_Living_Expenses; //Other Living Expenses
    public static final String EXPENSE_RT_I = Label.Expense_RT_Insurance; //Insurance */

    //Expense Record Type Names
    public static final String EXPENSE_RT_AE = 'Accommodation Expense';
    public static final String EXPENSE_RT_LE = 'Living Expenses';
    public static final String EXPENSE_RT_CCP = 'Card & Credit Payment';
    public static final String EXPENSE_RT_LPAF = 'Loan Payment - Asset Finance';
    public static final String EXPENSE_RT_LPO = 'Loan Payment - Other';
    public static final String EXPENSE_RT_LPPI = 'Loan Payment - Property Investment';
    public static final String EXPENSE_RT_OE = 'Other Expense';
    public static final String EXPENSE_RT_ZL = 'UFO';

    //Income1 Record Type Labels
    /* public static final String INCOME_RT_BI = Label.Income_RT_Business_Income; //Business Income
    public static final String INCOME_RT_CS = Label.Income_RT_Child_Support; //Child Support
    public static final String INCOME_RT_C = Label.Income_RT_Centrelink; //Centrelink
    public static final String INCOME_RT_CD = Label.Income_RT_Company_Director; //Company Director
    public static final String INCOME_RT_II = Label.Income_RT_Investment_Income; //Investment Income
    public static final String INCOME_RT_NI = Label.Income_RT_No_Income; //No Income
    public static final String INCOME_RT_OI = Label.Income_RT_Other_Income; //Other Income
    public static final String INCOME_RT_PE = Label.Income_RT_PAYG_Employment; //PAYG Employment
    public static final String INCOME_RT_SE = Label.Income_RT_Self_Employed; //Self-Employed
    public static final String INCOME_RT_LCOMM = 'zLead Commercial';
    public static final String INCOME_RT_LCONS = 'zLead Consumer';
    /*public static final String INCOME_RT_FM = Label.Income_RT_Family_Maintenance; //Family Maintenance
    public static final String INCOME_RT_GB = Label.Income_RT_Government_Benefits; //Government Benefits
    public static final String INCOME_RT_WIP = Label.Income_RT_Workcover_Income_Protection; //Workcover/Income Protection */

    //Income Record Type Names
    public static final String INCOME_RT_BI = 'Business Income';
    public static final String INCOME_RT_C = 'Centrelink';
    public static final String INCOME_RT_CS = 'Child Support';
    public static final String INCOME_RT_CD = 'Company Director';
    public static final String INCOME_RT_II = 'Investment Income';
    public static final String INCOME_RT_NI = 'No Income';
    public static final String INCOME_RT_OI = 'Other Income';
    public static final String INCOME_RT_PE = 'PAYG Employment';
    public static final String INCOME_RT_SE = 'Self-Employed';
    public static final String INCOME_RT_SINB = 'Spousal Income (non-borrowing)';
    public static final String INCOME_RT_ZL = 'UFO';

    //Reference Record Type Names
    public static final String REF_RT_ACC = 'Accountant';
    public static final String REF_RT_BUSI = 'Business';
    public static final String REF_RT_LAND = 'Landlord';
    public static final String REF_RT_PERS = 'Personal';
    
    //Relationship Record Type Names
    public static final String REL_RT_BREL = 'Business Relationship';
    public static final String REL_RT_PCON = 'Partner Contact';
    public static final String REL_RT_SPOU = 'Spouse';

    //Income1 - Income Situation Values
    public static final String INCOME_IS_CI = 'Current Income';
    public static final String INCOME_IS_SI = 'Secondary Income';
    public static final String INCOME_IS_PI = 'Previous Income';
        
    //Case Statuses
    public static final String CASE_STATUS_NEW = Label.Case_Status_New; //New
    public static final String CASE_STATUS_APPROVED = Label.Case_Status_Approved; //Approved
    public static final String CASE_STATUS_SCENARIO = Label.Case_Status_Scenario; //Scenario

    //Case Stages   
    public static final String CASE_STAGE_OPEN = Label.Case_Stage_Open; //Open
    public static final String CASE_STAGE_OA = Label.Case_Stage_Owner_Assigned; //Owner Assigned
    public static final String CASE_STAGE_ATTEMPTED = Label.Case_Stage_Attempted; //Attempted
    public static final String CASE_STAGE_ATTEMPTED_CONTACT = 'Attempted Contact'; //Attempted
    public static final String CASE_STAGE_APP_REVIEW = Label.Case_Stage_Application_Review; //Application Review
    public static final String CASE_STAGE_WOD = Label.Case_Stage_Waiting_on_Docs; //Waiting on Docs
    public static final String CASE_STAGE_RFS = Label.Case_Stage_Ready_for_Submission; //Ready for Submission
    public static final String CASE_STAGE_STL = Label.Case_Stage_Submitted_to_Lender; //Submitted to Lender
    public static final String CASE_STAGE_DBLENDER = Label.Case_Stage_Deferred_by_Lender; //Deferred by Lender
    public static final String CASE_STAGE_RBLENDER = Label.Case_Stage_Referred_by_Lender; //Referred by Lender
    public static final String CASE_STAGE_APWCOND = Label.Case_Stage_Approved_with_Conditions; //Approved with Conditions
    public static final String CASE_STAGE_APWNOCOND = Label.Case_Stage_Approved_No_Conditions; //Approved - No Conditions
    public static final String CASE_STAGE_IOS = Label.Case_Stage_Insurance_Offering_Sent; //Insurance Offering Sent
    public static final String CASE_STAGE_TIRQ = Label.Case_Stage_Tax_Invoice_Requested; //Tax Invoice Requested
    public static final String CASE_STAGE_TIR = Label.Case_Stage_Tax_Invoice_Received; //Tax Invoice Received
    public static final String CASE_STAGE_FMWOAA = Label.Case_Stage_Final_Modifications_WOAA; //Final Modifications - WOAA
    public static final String CASE_STAGE_DSTC = Label.Case_Stage_Documents_Sent_to_Client; //Documents Sent to Client
    public static final String CASE_STAGE_PDR = Label.Case_Stage_Part_Docs_Received; //Part Docs Received
    public static final String CASE_STAGE_DRNC = Label.Case_Stage_Docs_Received_Not_Checked; //Docs Received Not Checked
    public static final String CASE_STAGE_SFS = Label.Case_Stage_Sent_for_Settlement; //Sent for Settlement
    public static final String CASE_STAGE_SSPNDSTLMNT = Label.Case_Stage_Suspended_Settlement; //Suspended Settlement
    public static final String CASE_STAGE_SETTLED = Label.Case_Stage_Settled; //Settled
    public static final String CASE_STAGE_LOST = Label.Case_Stage_Lost; //Lost
    public static final String CASE_STAGE_DWI = Label.Case_Stage_Deal_With_Introducer; //Deak With Introducer
    public static final String CASE_STAGE_ANR = Label.Case_Stage_Asset_Not_Ready; //Asset Not Ready
    public static final String SCEN_STAGE_NEW = Label.Case_Scenario_Stage_New; //New Scenario
    public static final String SCEN_STAGE_WINTRODUCER = Label.Case_Scenario_Stage_With_Introducer; //With Introducer
    public static final String SCEN_STAGE_WLENDER = Label.Case_Scenario_Stage_With_Lender; //With Lender
    public static final String SCEN_STAGE_COMPLETE = Label.Case_Scenario_Stage_Complete; //Complete
    public static final String SCEN_STAGE_LOST = Label.Case_Scenario_Stage_Lost; //Lost
    public static final String SCEN_STAGE_OPEN = 'Open';

    //EasyCase Labels
    public static final String EASYCASE_VAL_ROLE_PA = 'Primary Applicant';
    public static final String EASYCASE_DEF_LOANTYPE = Label.EasyCase_Default_Loan_Type; //Asset Loan
    public static final String EASYCASE_ERR_PC1 = Label.EasyCase_Error_Primary_Contact_1; //Only 1 Primary Contact can be picked for Roles
    public static final String EASYCASE_ERR_PC2 = Label.EasyCase_Error_Primary_Contact_2; //A Primary Contact must be selected in Role  
    public static final String EASYCASE_ERR_PA1 = Label.EasyCase_Error_Primary_Applicant_Role; //Only 1 Primary Applicant could be picked for Roles
    public static final String EASYCASE_ERR_PA2 = Label.EasyCase_Error_Primary_Applicant_Role_2; //A Primary Applicant must be selected in Role
    public static final String EASYCASE_ERR_CLI_RT = Label.EasyCase_Error_Consumer_Loan_Individual_RT; //Only 1 Consumer Loan - Individual Role is allowed
    public static final String EASYCASE_INFO_ROLES = Label.EasyCase_Info_Roles; //Directly referenced to EasyCase.page

    //Address Labels
    public static final String ADDRESS_CURRENT = 'Current';
    public static final String ADDRESS_PREVIOUS = 'Previous';

    //Role Labels
    public static final String ROLE_PRIMARY_APPLICANT = 'Primary Applicant';
    public static final String ROLE_CO_APPLICANT = 'Co-Applicant';

    //Rollup Labels
    public static final String INCOME_API = 'Income1__c';
    public static final String EXPENSE_API = 'Expense1__c';
    public static final String ASSET_API = 'Asset1__c';
    public static final String LIABILITY_API = 'Liability1__c';

    //Expense Types
    public static final String EXPENSE_TYPE_LIVING_EXPENSES = 'Living Expenses';
    public static final String EXPENSE_TYPE_ADD_BACK = 'Add Back';
    public static final String EXPENSE_TYPE_BOARDER = 'Boarder';
    public static final String EXPENSE_TYPE_CAR_LOAN = 'Car Loan';
    public static final String EXPENSE_TYPE_CARRIED_FORWARD_LOSSES = 'Carried Forward Losses';
    public static final String EXPENSE_TYPE_COMMERCIAL_LEASE = 'Commercial Lease';
    public static final String EXPENSE_TYPE_CREDIT_CARD = 'Credit Card';
    public static final String EXPENSE_TYPE_DEPRECIATION = 'Depreciation';
    public static final String EXPENSE_TYPE_EMPLOYER_PROVIDED = 'Employer Provided';
    public static final String EXPENSE_TYPE_EQUIPMENT_FINANCE = 'Equipment Finance';
    public static final String EXPENSE_TYPE_FUEL_CARD = 'Fuel Card';
    public static final String EXPENSE_TYPE_INTEREST_FREE_FINANCE = 'Interest-Free Finance';
    public static final String EXPENSE_TYPE_LINE_OF_CREDIT_SECURED = 'Line of Credit - Secured';
    public static final String EXPENSE_TYPE_LINE_OF_CREDIT_UNSECURED = 'Line of Credit - Unsecured';
    public static final String EXPENSE_TYPE_LIVING_WITH_PARENTS = 'Living With Parents';
    public static final String EXPENSE_TYPE_LOAN_AS_GUARANTOR = 'Loan as Guarantor';
    public static final String EXPENSE_TYPE_MORTGAGE_INVESTMENT = 'Mortgage - Investment';
    public static final String EXPENSE_TYPE_MORTGAGE_OWNER_OCCUPIED = 'Mortgage - Owner Occupied';
    public static final String EXPENSE_TYPE_NOVATED_LEASE = 'Novated Lease';
    public static final String EXPENSE_TYPE_OTHER_ASSET_LOAN = 'Other Asset Loan';
    public static final String EXPENSE_OTHER_EXPENSE = 'Other Expense';
    public static final String EXPENSE_TYPE_OTHER_LOAN = 'Other Loan';
    public static final String EXPENSE_TYPE_OUTSTANDING_TAXATION = 'Outstanding Taxation';
    public static final String EXPENSE_TYPE_OVERDRAFT = 'Overdraft';
    public static final String EXPENSE_TYPE_PAY_DAY_LOAN = 'Pay-Day Loan';
    public static final String EXPENSE_TYPE_RENTER_AGENT = 'Renter - Agent';
    public static final String EXPENSE_TYPE_RENTER_PRIVATE = 'Renter - Private';
    public static final String EXPENSE_TYPE_STORE_CARD = 'Store Card';
    public static final String EXPENSE_TYPE_UNSECURED_PERSONAL_LOAN = 'Unsecured Personal Loan';

    //Happy Path Labels - Asset FO Link errors
    public static final string HAPPYPATH_FOLINK_ERR_LIA = 'selected Liability is not allowed to link with ';
    public static final string HAPPYPATH_FOLINK_ERR_LIA2 = 'Liability is not allowed to link with ';
    public static final string HAPPYPATH_FOLINK_ERR_INC = 'selected Income is not allowed to link with ';
    public static final string HAPPYPATH_FOLINK_ERR_INC2 = 'Income is not allowed to link with ';
    public static final string HAPPYPATH_FOLINK_ERR_EXP = 'selected Expense is not allowed to link with ';
    public static final string HAPPYPATH_FOLINK_ERR_EXP2 = 'Expense is not allowed to link with ';
    public static final string HAPPYPATH_FOLINK_ERR_ASS = 'selected Asset is not allowed to link with ';
    public static final string HAPPYPATH_FOLINK_ERR_ASS2 = 'Asset is not allowed to link with ';

    public static final string CASEIDPREFIX = '500';    
    public static final string TASKCALLPURPOSE_ATTEMPTCON = 'Attempted Contact';   
    public static final string TASKSUBJ_OUTCALL = 'Outbound call'; 

    //Support Request RTs
    public static final string SR_NOD_INIT_REV = 'NOD Initial Review';
    public static final string SR_NOD_REQUEST_CALL = 'Communication - NOD - Request a Call';

    //Sector Sector1__c picklist 
    public static final string SECTOR1_INIT_REV = 'NOD - SLA - Initial Review';
    public static final String SECTOR1_NOD_SLA_APP_REVIEW = 'NOD - SLA - Application Review';
    
    
}
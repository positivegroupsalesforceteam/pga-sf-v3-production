/*
	@Description: class for calculating SLA Age of With Introducer (SLA-2) on Case
	@Author: Jesfer Baculod/Rex David - Positive Group
	@Modified: Rex David - Moved from Scenario object to Case to work for the new model.
	@History:
		- 03/23/2018 - Created
		- 15/05/2019 - RDAVID - extra/task24564956-OLDSLACleanUp - Comment this class
*/
public class Case_Scenario_UpdateSLA2Warning {
	private static General_Settings1__c genSet = General_Settings1__c.getInstance(UserInfo.getUserId());
	private static final string SCEN_STAGE_LOST = Label.Scenario_Stage_Lost; //Lost
	
	@InvocableMethod(label='Case - SLA Next Warning Time - With Introducer' description='updates next Warning Time of an SLA whenever an SLA is not yet completed')
	public static void scenarioSLAWarningTimeWI (list <ID> scenIDs) {
		/*RDAVID - extra/task24564956-OLDSLACleanUp -
		//Retrieve default Business Hours
		BusinessHours bh = [select Id from BusinessHours where IsDefault=true]; 

		//Retrieve Scenarios to update
		list <Case> caseScenList = [Select Id, 
									SLA_Scenario_Time_2_Start__c, SLA_Scenario_Warning_Time_2__c, SLA_Scenario_Started_Count_2__c, SLA_Scenario_Completed_Count_2__c, SLA_Scenario_Active_2__c, SLA_Scenario_Time_2_mm__c, Stage__c, Lost_Reason__c
									From Case Where Id in : scenIds];
        
        Datetime warningdate; 
		for (Case scen : caseScenList){
			warningdate = scen.SLA_Scenario_Warning_Time_2__c;
			scen.SLA_Scenario_Warning_Time_2__c = null;
			scen.SLA_Scenario_Active_2__c = null;
		}
		update caseScenList; //force update to retrigger SLA Warning

		system.debug('@@warningdate:'+warningdate);

		for (Case scen : caseScenList){
            
            scen.SLA_Scenario_Active_2__c = 'Yes';
            
            if (scen.SLA_Scenario_Started_Count_2__c != scen.SLA_Scenario_Completed_Count_2__c){ 
                
                scen.SLA_Scenario_Warning_Time_2__c = BusinessHours.add(bh.Id, warningdate, Integer.valueOf(genSet.Case_Scenario_SLA_2_Interval__c)); //Set succeeding warning of current SLA (add 1440 minutes / 24 hours)
                DateTime warningtimeAR = scen.SLA_Scenario_Warning_Time_2__c;
				scen.SLA_Scenario_Warning_Time_2__c = Datetime.newInstance(warningtimeAR.year(), warningtimeAR.month(), warningtimeAR.day(), warningtimeAR.hour(), warningtimeAR.minute(), 0);
                scen.SLA_Scenario_Time_2_mm__c = Decimal.valueof(( BusinessHours.diff(bh.Id, scen.SLA_Scenario_Time_2_Start__c, warningdate ) / 1000) / 60 );  //returns SLA Age in minutes
                if (scen.SLA_Scenario_Time_2_mm__c == 1439) scen.SLA_Scenario_Time_2_mm__c = 1440; //workaround for business hours difference of SLA
				if (scen.SLA_Scenario_Time_2_mm__c == 2879) scen.SLA_Scenario_Time_2_mm__c = 2880; //workaround for business hours difference of SLA
				if (scen.SLA_Scenario_Time_2_mm__c == 4319) scen.SLA_Scenario_Time_2_mm__c = 4320; //workaround for business hours difference of SLA
				if (scen.SLA_Scenario_Time_2_mm__c >= 4320){
                	scen.SLA_Scenario_Warning_Time_2__c = BusinessHours.add(bh.Id, warningdate, 60000); //add 1 minute to update Stage to Lost once SLA elapsed >72 Hours
                }
                system.debug('@@slaWarningB');
            }
		}
		update caseScenList;
		*/
	}
}
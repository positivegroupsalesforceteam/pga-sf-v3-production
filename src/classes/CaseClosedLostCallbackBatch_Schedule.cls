/**
 * @File Name          : CaseClosedLostCallbackBatch_Schedule.cls
 * @Description        : 
 * @Author             : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 19/07/2019, 7:54:13 am
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/06/2019, 9:54:44 am   Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Initial Version
**/
global class CaseClosedLostCallbackBatch_Schedule implements Schedulable{

    global void execute(SchedulableContext sc) {
		Date currentDate = System.today();
		CaseClosedLostCallbackBatch cclcb = new CaseClosedLostCallbackBatch(); //allows partial success
		if(!Test.isRunningTest()) Database.executeBatch(cclcb,1); //limit batch update to 1 due JB
		else Database.executeBatch(cclcb);
	}

	/* Code for scheduling batch job at 1030AM daily (Run as API Admin)
		CaseClosedLostCallbackBatch_Schedule cclcbs = new CaseClosedLostCallbackBatch_Schedule();
		String sch = '0 30 10 1/1 * ? *'; 
		system.schedule('POS Closed Case Callback', sch, cclcbs);
	*/

}
/**
 * @File Name          : SMSServicesTest.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 31/07/2019, 12:14:14 pm
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    31/05/2019, 11:02:06 am   JBACULOD                     Initial Version
 * 1.1    31/07/2019, 12:11:19 pm   JBACULOD                     Removed assertion for Task due to creation of Converse App Task   
**/
@isTest
private class SMSServicesTest {

    @testsetup static void setup(){

        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Case_SMS_Alerts__c = true);
        insert trigSet;

        Case cse = new Case(
            Status = 'New',
            Stage__c = 'Lost',
            Partition__c = 'Positive',
            Channel__c = 'PLS',
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_P_CONS_AF).getRecordTypeId() //n: Consumer - Asset Finance
        );
        insert cse;

    }

    static testmethod void testCallbackNewCase(){

        Case cse = [Select Id, OwnerId, LastModifiedById From Case];
        test.startTest();

            list <String> keylist = new list <String>();
            string key = cse.Id+':'+'TEST'+':'+cse.OwnerId +':'+ cse.LastModifiedById;
            keylist.add(key);            

            SMSServices.SendSMS(keylist);
        test.stopTest();

        //Verify Task got created (//7/31/19 No longer creates task, directly creates Converse App Task instead)
        /* list <Task> tsklist = [Select Id From Task]; 
        system.assertEquals(tsklist.size(),1); */

        //Verify Converse App Task got created
        list <smagicinteract__Converse_App_Task__c> ctasklist = [Select Id From smagicinteract__Converse_App_Task__c limit 1];
        system.assertEquals(ctasklist.size(), 1);

    }

}
public class SubmissionRequest_CaseNM {
    
    public Case cse {get;set;}
    public Submission_Request_CT__c subReq {get;set;}
    private Id caseId;
    public Boolean hasError {get;set;}
    public Boolean saveSuccess {get;set;}
    public Boolean showSection {get;set;}

    public CaseComment cseCommentNotesForLender {get;set;}
    public CaseComment cseCommentReadyForSubmission {get;set;}
    
    
    public SubmissionRequest_CaseNM(){
        cse = new Case();
        subReq = new Submission_Request_CT__c();
        hasError = saveSuccess = showSection = false;
        
        caseId = apexpages.currentpage().getparameters().get('id');
        
        System.debug('caseId == '+caseId);
        cseCommentNotesForLender = new CaseComment(isPublished=TRUE);
        cseCommentReadyForSubmission = new CaseComment(isPublished=TRUE);

        if(caseId != NULL) {
            cseCommentReadyForSubmission.ParentId = caseId;
            cseCommentNotesForLender.ParentId = caseId;

            cse = [SELECT Id, CaseNumber, Application_Name__r.RecordtypeId, Application_Name__r.Recordtype.Name, Lender1__c, Lender1__r.Name FROM Case WHERE Id =: caseId];
            subReq.Case__c = cse.Id;
            
            if(cse.Application_Name__r.RecordtypeId != NULL) 	subReq.Application_Type1__c = cse.Application_Name__r.Recordtype.Name;       
            if(cse.Lender1__c != NULL) 							subReq.Lender1__c = cse.Lender1__r.Name;
            
			if(cse.Application_Name__r.RecordtypeId != NULL && cse.Lender1__c != NULL) showSection = true;
            
            System.debug( 'cse = '+cse);
            System.debug( 'subReq = '+subReq);
        }
    }

    public pageReference submit(){
        Savepoint sp = Database.setSavepoint();
       
        try{
            if(!subReq.Have_signed_privacy_consent__c){
                subReq.Have_signed_privacy_consent__c.addError('Please tick Have Signed Privacy Consent.');
                hasError = true;
                return null;
            }
            
            if(!subReq.Have_checked_income_on_payslips__c){
                subReq.Have_checked_income_on_payslips__c.addError('Please tick Have Checked Income On Payslips.');
                hasError = true;
                return null;
            }
            
            if(!subReq.Support_Docs_Combined__c){
                subReq.Support_Docs_Combined__c.addError('Please tick Supp Docs Combined.');
                hasError = true;
                return null;
            }
            if(!subReq.Salesforce_Record_Accurate__c){
                subReq.Salesforce_Record_Accurate__c.addError('Please verify all Salesforce Record are Accurate.');
                hasError = true;
                return null;
            }

            if(String.isBlank(subReq.Ready_For_Submission_Notes1__c)){
                subReq.Ready_For_Submission_Notes1__c.addError('Ready For Submission Notes required.');
                hasError = true;
                return null;
            }
        
            if(String.isBlank(subReq.Notes_For_Lender1__c)){
                subReq.Notes_For_Lender1__c.addError('Notes for Lender required.');
                hasError = true;
                return null;
                
            }

            List<CaseComment> caseCommentToInsert = new List<CaseComment>();

            cseCommentReadyForSubmission.CommentBody = 'Ready For Submission Notes: '+subReq.Ready_For_Submission_Notes1__c;
            caseCommentToInsert.add(cseCommentReadyForSubmission);
            
            cseCommentNotesForLender.CommentBody = 'Notes For Lender: '+subReq.Notes_For_Lender1__c;
            caseCommentToInsert.add(cseCommentNotesForLender);

            if (caseCommentToInsert.size() > 0){           
                Database.insert(caseCommentToInsert);
                System.debug('caseCommentToInsert Inserted. '+caseCommentToInsert);
            }
            System.debug('subReq to Insert. '+subReq);
            Database.insert(subReq);
            System.debug('subReq Inserted. '+subReq);

            cse.Status = 'Review';
            cse.Stage__c = 'Ready for Submission';
            Database.update(cse);
            System.debug('case Updated. '+cse);
            
            saveSuccess = true;
            hasError = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Submission Request saved successfully.'));
            
        }
        catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
            hasError = true;
            Database.rollback( sp );  
        }
        return null;
    }

    public pageReference readyforSub(){
        return null;
    }

    public pagereference caseid(){
        return new PageReference('/'+cse.Id);
    } 
}
/*
    @author: Jesfer Baculod - Positive Group
    @history: 07/24/17 - Created
    		  07/27/17 - Updated
    @description: test class of Opp_EditVendorDetails_Controller class
*/
@isTest
private class Opp_EditVendorDetails_ControllerTest {

	private static final string ACC_RT_INDIVIDUAL = Label.Contact_Individual_RT; //Individual
	private static final string CON_RT_BUSINESS = Label.Contact_Business_RT; //Business
	
	@testsetup static void setup(){
		
		Id rtBAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get(ACC_RT_INDIVIDUAL).getRecordTypeId();
		Account acc = new Account(
				LastName = 'test account',
				RecordTypeId = rtBAcc,
				Previous_Employment_Status__c = 'Full-time'
			);
		insert acc;

		ABN__c abn = new ABN__c(
				Name = 'Test Legal Entity ABN',
				Registered_ABN__c = '123456789'
			);
		insert abn;

		Referral_Company__c rc = new Referral_Company__c(
				Name = 'Test Referral Company'
			);
		insert rc;

		Id rtBusinessCon = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CON_RT_BUSINESS).getRecordTypeId();
		Contact bcon = new Contact(
				RecordTypeId = rtBusinessCon,
				Email = 'test@test.com',
				LastName = 'Test BContact',
				Referral_Company__c = rc.Id
			);
		insert bcon;

		Id rtIndividualOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(ACC_RT_INDIVIDUAL).getRecordTypeId();
		Opportunity opp = new Opportunity(
				AccountId = acc.Id,
				StageName = 'Qualified',
				Base_rate__c = 1,
				Checked_Commissions_Correct__c = true,
				Employment_Reference_Completed__c = true,
				Residential_Reference_Completed__c = true,
				Vehicle_Purchase_Price__c = 1000,
				Total_amount_financed__c = 1000,
				Sale_Type__c = Label.Opp_Sale_Type_Dealer,
				Dealer_Vendor__c = rc.Id,
				Name = 'Test Opportunity ANZ 1',
				CloseDate = Date.today(),
				Approval_Date__c = Date.today(),
				Make__c = 'test',
				Model__c = 'triton',
				Loan_Reference__c = '123456789',
				New_or_Used__c = 'New',
				Loan_Product__c = 'Consumer Loan',
				Previous_Employment_Status__c = 'Full-time',
				Transmission__c = 'AT',
				Year__c = '2017',
				Loan_Rate__c = 50,
				Lender__c = 'ANZ',
				RecordTypeId = rtIndividualOpp
			);
		insert opp;

	}

	static testmethod void testSearches(){

		//Retrieve Opportunity to be used for editing vendor details
		Opportunity opp = [Select Id, Sale_Type__c, Name, StageName, Dealer_Contact__c, Dealer_Vendor__c, Vendor_Contact__c From Opportunity];
		list <Referral_Company__c> rclist = [Select Id, Name From Referral_Company__c];
		list <Contact> bconlist = [Select Id, Name From Contact Where RecordType.Name = : CON_RT_BUSINESS];
		list <Account> acclist = [Select Id, Name From Account Where RecordType.name = : ACC_RT_INDIVIDUAL];
		list <Contact> iconlist = [Select Id, Name, AccountId From Contact Where AccountId in : acclist AND RecordTypeId = null]; //Person Account Contact

		PageReference tpref = Page.Opp_EditVendorDetails;
		tpref.getParameters().put('id', opp.Id);
		Test.setCurrentPage(tpref);

		ApexPages.StandardController st0 = new ApexPages.StandardController(opp);
		Opp_EditVendorDetails_Controller oevdcc0 = new Opp_EditVendorDetails_Controller(st0);

		opp.Dealer_Vendor__c = rclist[0].Id;
		opp.Dealer_Contact__c = bconlist[0].Id;
		update opp;

		ApexPages.StandardController st = new ApexPages.StandardController(opp);
		Opp_EditVendorDetails_Controller oevdcc = new Opp_EditVendorDetails_Controller(st);

		Test.startTest();
			oevdcc.opp_DV = 'Test Referral Company'; //search for existing Referral Company
			oevdcc.searchReferralCompany();
			oevdcc.searchMode = 1;
			oevdcc.refreshPageSize();
			oevdcc.opp_s_ID = rclist[0].Id; //a record has been selected on the search results
			oevdcc.assignLookupID();

			oevdcc.opp_DC = 'Test BContact'; //search for existing Business Contact 
			oevdcc.searchBusinessContact();
			oevdcc.searchMode = 2;
			oevdcc.refreshPageSize();
			oevdcc.opp_s_ID = bconlist[0].Id; //a record has been selected on the search results
			oevdcc.assignLookupID();

			oevdcc.opp_DV_id = null;
			oevdcc.searchBusinessContact();

			opp.Sale_Type__c = Label.Opp_Sale_Type_Private;
			opp.Vendor_Contact__c = null;
			update opp;

			ApexPages.StandardController st2 = new ApexPages.StandardController(opp);
			Opp_EditVendorDetails_Controller oevdcc2 = new Opp_EditVendorDetails_Controller(st2);

			oevdcc2.opp_VC = 'Test account'; //search for exisitng Key Person Contact
			oevdcc2.searchIndividualContact();
			oevdcc2.searchMode = 3;
			oevdcc2.refreshPageSize();
			oevdcc2.opp_s_ID = iconlist[0].Id; //a record has been selected on the search results
			oevdcc2.assignLookupID(); 

			opp.Vendor_Contact__c = iconlist[0].Id;
			update opp;

			ApexPages.StandardController st3 = new ApexPages.StandardController(opp);
			Opp_EditVendorDetails_Controller oevdcc3 = new Opp_EditVendorDetails_Controller(st3);			

		Test.stopTest();

		system.assertEquals(opp.Dealer_Vendor__c,rclist[0].Id); //Verify that Dealer Sale Vendor is same with selected Referral Company
		system.assertEquals(opp.Dealer_Contact__c,bconlist[0].Id); //Verify that Dealer Contact Person is same with selected Business Contact
		system.assertEquals(opp.Vendor_Contact__c,iconlist[0].Id); //Verify that Private Sale Vendor Contact is same with selected Individual Contact

	}

	static testmethod void testSaveCreateOnLookups(){

		//Retrieve Opportunity to be used for editing vendor details
		Opportunity opp = [Select Id, Name, StageName, Sale_Type__c, Dealer_Contact__c, Dealer_Vendor__c, Vendor_Contact__c From Opportunity];
		ABN__c abn = [Select Id, Name, Registered_ABN__c From ABN__c];

		list <Referral_Company__c> rclist = [Select Id, Name From Referral_Company__c];
		list <Contact> bconlist = [Select Id, Name From Contact Where RecordType.Name = : CON_RT_BUSINESS];
		list <Account> acclist = [Select Id, Name From Account Where RecordType.name = : ACC_RT_INDIVIDUAL];
		list <Contact> iconlist = [Select Id, Name, AccountId From Contact Where AccountId in : acclist AND RecordTypeId = null]; //Person Account Contact

		ApexPages.StandardController st = new ApexPages.StandardController(opp);
		PageReference tpref = Page.Opp_EditVendorDetails;
		tpref.getParameters().put('id', opp.Id);
		Test.setCurrentPage(tpref);

		Opp_EditVendorDetails_Controller oevdcc = new Opp_EditVendorDetails_Controller(st);

		Test.startTest();
			
			//Test save Opp errors
			oevdcc.opp.Dealer_Vendor__c = null;
			oevdcc.opp.Dealer_Contact__c = null;
			oevdcc.saveOpp();

			//Test saving of new Referral Company
			oevdcc.refcomp.Name = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			oevdcc.saveReferralCompany();
			oevdcc.clearFormFields();
			oevdcc.refcomp.Name = 'Test Ref Comp 2';
			oevdcc.refcomp.Company_Type__c = 'Car Dealer';
			oevdcc.refcomp.Legal_Entity_Name__c = abn.Id;
			oevdcc.retrieveCurrentABN();
			oevdcc.saveReferralCompany();

			//Test saving of new Referral Company
			oevdcc.refcomp.Name = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			oevdcc.saveReferralCompany();
			oevdcc.clearFormFields();
			oevdcc.refcomp.Name = 'Test Ref Comp 2';
			oevdcc.refcomp.Company_Type__c = 'Car Dealer';
			oevdcc.saveReferralCompany();
			oevdcc.assignExistingReferralCompany();

			//Test saving of new Business Contact
			oevdcc.bcon.LastName = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			oevdcc.saveBusinessContact();
			oevdcc.clearFormFields();
			oevdcc.bcon.LastName = 'Test BContact 2';
			oevdcc.bcon.Email = 'test@testbcon.com';
			oevdcc.saveBusinessContact();

			//Test saving of new Individual Contact
			oevdcc.bcon.LastName = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			oevdcc.saveIndividualContact();
			oevdcc.clearFormFields();
			oevdcc.icon.LastName = 'Test IContact 2';
			oevdcc.icon.Email = 'test@testbcon.com';
			oevdcc.saveIndividualContact();

			oevdcc.opp.Name = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			oevdcc.saveOpp();
			oevdcc.opp.Name = 'fixed';
			oevdcc.saveOpp();

			ApexPages.StandardController st0 = new ApexPages.StandardController(opp);
			Opp_EditVendorDetails_Controller oevdcc0 = new Opp_EditVendorDetails_Controller(st0);
			oevdcc0.opp.Dealer_Vendor__c = rclist[0].Id;
			oevdcc0.opp.Dealer_Contact__c = null;
			oevdcc0.saveOpp();

			ApexPages.StandardController st1 = new ApexPages.StandardController(opp);
			Opp_EditVendorDetails_Controller oevdcc1 = new Opp_EditVendorDetails_Controller(st1);
			oevdcc1.opp.Dealer_Vendor__c = null;
			oevdcc1.opp.Dealer_Contact__c = bconlist[0].Id;
			oevdcc1.saveOpp();

			opp.Sale_Type__c = Label.Opp_Sale_Type_Private;
			ApexPages.StandardController st2 = new ApexPages.StandardController(opp);
			Opp_EditVendorDetails_Controller oevdcc2 = new Opp_EditVendorDetails_Controller(st2);
			oevdcc2.opp.Vendor_Contact__c = null;
			oevdcc2.saveOpp();

		Test.stopTest();

		//Retrieve udpated Opportunity
		Opportunity upopp = [Select Id, Name, StageName, Dealer_Contact__c, Dealer_Vendor__c, Vendor_Contact__c From Opportunity];
		//Retrieve created Records
		list <Referral_Company__c> uprclist = [Select Id, Name From Referral_Company__c Where Name = 'Test Ref Comp 2'];
		list <Contact> upbconlist = [Select Id, Name From Contact Where Name = 'Test BContact 2'];
		list <Contact> upiconlist = [Select Id, Name From Contact Where Name = 'Test IContact 2'];

		//system.assertEquals(upopp.Dealer_Vendor__c,uprclist[0].Id); //Verify that Dealer Sale Vendor is same with created Referral Company
		system.assertEquals(upopp.Dealer_Contact__c,upbconlist[0].Id); //Verify that Dealer Contact Person is same with created Business Contact
		//system.assertEquals(upopp.Vendor_Contact__c,upiconlist[0].Id); //Verify that Private Sale Vendor Contact is same with created Individual Contact

	}
	
}
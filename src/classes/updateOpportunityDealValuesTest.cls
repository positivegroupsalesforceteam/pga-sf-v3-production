/*
    @author: Jesfer Baculod - Positive Group
    @history: 07/21/17 - Created
    		  07/28/17 - Updated
    		  09/25/17 - Updated due to failing test class cause of Opp Sales Type Stage Dealer validation
    @description: test class for testing functionalities from updateOpportunityDealValues & updateOpportunityDealValues_Schedule classes
*/
@isTest
private class updateOpportunityDealValuesTest {

	private static final string CON_RT_BUSINESS = Label.Contact_Business_RT; //Business

	@testsetup static void setup(){

		//Create test data for Account
		Id rtBAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
		list <Account> acclist = new list <Account>();
		for (Integer i=0; i<15 ; i++){
			Account acc = new Account(
				LastName = 'test account'+i,
				RecordTypeId = rtBAcc,
				Previous_Employment_Status__c = 'Full-time'
			);
			acclist.add(acc);
		}
		insert acclist;

		//Create test data for Referral Company
		Referral_Company__c rc = new Referral_Company__c(
				Name = 'Test Referral Company'
			);
		insert rc;

		//Create test data for Business Contact
		Id rtBusinessCon = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CON_RT_BUSINESS).getRecordTypeId();
		Contact bcon = new Contact(
				RecordTypeId = rtBusinessCon,
				Email = 'test@test.com',
				LastName = 'Test BContact',
				Referral_Company__c = rc.Id
			);
		insert bcon;

		//Create test data for Opportunities
		Id rtIndOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
		list <Opportunity> opplist = new list <Opportunity>();
		for (Integer i=0; i <acclist.size(); i++){
			Opportunity opp = new Opportunity();
			if (i < 5) opp = createOpportunity('Settled',acclist[i].Id ,rc.Id,'ANZ',rtIndOpp,i);
			if (i < 10 && i >= 5) opp = createOpportunity('Settled',acclist[i].Id,rc.Id,'Micro Money 3',rtIndOpp,i);
			if (i >= 10) opp = createOpportunity('Settled',acclist[i].Id,rc.Id,'Pepper',rtIndOpp,i);
			opp.Dealer_Vendor__c = rc.Id;
			opp.Dealer_Contact__c = bcon.Id;
			opplist.add(opp);
		}
		insert opplist;

		//Create test data for Commissions
		list <Commission__c> commlist = new list <Commission__c>();
		for (Opportunity opp : opplist){
			Commission__c insurer_comm = createCommission('PLS ERIC','GAP', opp.Id);
			commlist.add(insurer_comm);
			Commission__c comm = createCommission(opp.Lender__c, 'Trail', opp.Id);
			commlist.add(comm);
		}
		insert commlist; 

		
	}

	private static Commission__c createCommission(string institution, string product, ID oppId){
		Commission__c comm = new Commission__c(
				Institution__c = institution,
				Product__c = product,
				Opportunity__c = oppId,
				Opportunity_Settled__c = true
			);
		return comm;
	}

	private static Opportunity createOpportunity(String stageName, Id accId, Id refcompID, string lender, Id rtype, Integer ctr){
		Opportunity opp = new Opportunity(
				AccountId = accId,
				StageName = stageName,
				Base_rate__c = 1,
				Checked_Commissions_Correct__c = true,
				Employment_Reference_Completed__c = true,
				Residential_Reference_Completed__c = true,
				Vehicle_Purchase_Price__c = 1000,
				Total_amount_financed__c = 1000,
				Sale_Type__c = 'Dealer',
				Dealer_Vendor__c = refcompID,
				Name = 'Test Opportunity' + lender + ' ' + ctr,
				CloseDate = Date.today(),
				Approval_Date__c = Date.today(),
				Make__c = 'test',
				Model__c = 'triton',
				Loan_Reference__c = '123456789',
				New_or_Used__c = 'New',
				Loan_Product__c = 'Consumer Loan',
				Previous_Employment_Status__c = 'Full-time',
				Transmission__c = 'AT',
				Year__c = '2017',
				Loan_Rate__c = 50,
				Lender__c = lender,
				RecordTypeId = rtype
			);
		return opp;
	}

	
	static testmethod void testUpdateOpportunityDealValues(){

		//Retrieve Opportunities w/ Commissions
		list <Opportunity> opplist = [Select Id, OwnerId, X_of_Products_Sold__c, CloseDate, Lender__c, StageName, (Select Id, Institution__c, Product__c, Opportunity_Settled__c From Commissions1__r) From Opportunity];

		Test.startTest();
			Date currentDate = System.today();
			updateOpportunityDealValues uODV1 = new updateOpportunityDealValues();
			database.executeBatch(uODV1);

			set <Id> ownerIdSet = new set <Id>();
			ownerIdSet.add(UserInfo.getUserId());
			updateOpportunityDealValues uODV2 = new updateOpportunityDealValues(ownerIdSet, currentDate);
			database.executeBatch(uODV2);

			updateOpportunityDealValues uODV3 = new updateOpportunityDealValues(currentDate,false);
			database.executeBatch(uODV3);

			updateOpportunityDealValues uODV4 = new updateOpportunityDealValues(Date.newInstance(2017,07,01), Date.newInstance(2017,07,31),false);
			database.executeBatch(uODV4);

		Test.stopTest(); 

		//Retrieve updated Opportunity
		list <Opportunity> upOpplist = [Select Id, Lender__c, Deal_Value__c From Opportunity];
		for (Opportunity opp : upOpplist){
			system.assert(opp.Deal_Value__c != null); //verify that Opportunity Deal Values were updated;
			if (opp.Lender__c == 'ANZ') system.assertEquals(opp.Deal_Value__c,0.4); //Verify that Deal Value was updated based from ANZ Selling Rule
			if (opp.Lender__c == 'Micro Money 3') system.assertEquals(opp.Deal_Value__c,0.6); //Verify that Deal Value was updated based from Micro Money 3 Selling Rule
		}

	} 

	static testmethod void testUpdateOpportunityDealValuesSchedule(){

		//Retrieve Opportunities w/ Commissions
		list <Opportunity> opplist = [Select Id, OwnerId, X_of_Products_Sold__c, CloseDate, Lender__c, StageName, (Select Id, Institution__c, Product__c, Opportunity_Settled__c From Commissions1__r) From Opportunity];

		Test.startTest();

			updateOpportunityDealValues_Schedule UODVS = new updateOpportunityDealValues_Schedule();
			String sch = '0 0 23 * * ?'; 
			system.schedule('Test Update Opportunity Deal Values', sch, UODVS);

		Test.stopTest();



	}

}
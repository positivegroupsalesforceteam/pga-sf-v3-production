/** 
* @FileName: NewRoleController
* @Description: Main Controller of NewRolePage
* @Copyright: Positive (c) 2019 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2018  JBACULOD Created Class
* 1.2 30/01 RDAVID - TeamWorkId - 22594324 - extra/AddRoleApplicantBug - Fix for Email, Phone field on Role not populating on Role creation in 'Add Role'
* 1.3 10/10/2019 JBACULD - extra/task26179460-AddRole-MIddleName - Added Middle Name
* 1.4 5/12/2019 JVGO extra/task26983891-DuplicateAccountAndPartition- Duplicate check to filter only nm: Individual records and automation of 
*                                                                     Partition of Account and Role
**/
public class NewRoleController {

    public Role__c role {get;set;}
    public Role__c creRole {get;set;}
    public Account acc {get;set;}
    public Contact con {get;set;}
    public Address__c addr {get;set;}
    public Role_Address__c raddr {get;set;}
    public Relationship__c rel {get;set;}
    public string conLastName {get;set;}
    public Case cse {get;set;}

    public string roleDesignation {get;set;}
    //public list <string> relationshipTypes {get;set;}
    public list <relationshipTypeWrapper> relTypeWrlist {get;set;}
    public ID curID {get;set;} //Case or Application ID
    public ID selAccID {get;set;}
    public boolean validForSaving {get;set;}
    public boolean dupeAccFound {get;set;}
    public boolean hasErrors {get;set;}
    public boolean hasAccErrors {get;set;}
    public boolean saveRelationshipSuccess {get;set;}
    public boolean saveAccountSuccess {get;set;}
    public boolean saveRoleSuccess {get;set;}
    public boolean saveRoleAddressSuccess {get;set;}
    public boolean validationSuccess {get;set;}
    public boolean ignoreDupe {get;set;}
    public boolean showAddressError {get;set;}
    public boolean showDupeMsg {get;set;}
    public boolean isAddApplicant {get;set;}
    private list <Account_To_Role_Field_Mapping__mdt> accToRoleMapping  {get;set;}

    public list <SelectOption> RoleRTs {get;set;} // Role RecordTypes
    //public list <SelectOption> RelAllowedRTs {get;set;} //Relationship RecordTypes
    public list <accWrapper> accDupeWrlist {get;set;}
    public list <accWrapper> accOfApplist {get;set;}
    private list <Account> dupeExAccs {get;set;}

    private Map <string, id> roleRTMap {get;set;}
    public Map <id, string> roleRTNameMap {get;set;}
    public static Map<String,Schema.RecordTypeInfo> relRTMapByName = Schema.SObjectType.Relationship__c.getRecordTypeInfosByName();
    public Map <string, string> relRTNameByIdMap {get;set;}
    public string currentUrl;

    public NewRoleController(){
        currentUrl = ApexPages.currentPage().getURL();
        System.debug('currentUrl = '+currentUrl);
        curID = Apexpages.currentpage().getparameters().get('id');
        if (curID != null){
            retrieveCase();
        }
        role = new Role__c(
            Case__c = cse.Id,
            Application__c = cse.Application_Name__c
        );
        acc = new Account();
        addr = new Address__c();
        con = new Contact();
        raddr = new Role_Address__c(Active_Address__c = 'Current', RecordTypeId = Schema.SObjectType.Role_Address__c.getRecordTypeInfosByName().get('Full Address').getRecordTypeId() );
        //relationshipTypes = new list <String>();
        roleDesignation = conLastName = '';
        showDupeMsg = showAddressError = saveRoleAddressSuccess = validationSuccess = ignoreDupe = hasErrors = saveRelationshipSuccess = saveAccountSuccess = saveRoleSuccess = hasAccErrors = hasErrors = dupeAccFound = validForSaving = clickCheckDup = isAddApplicant = false;
        accDupeWrlist = new list <accWrapper>();

        roleRTMap = new map <string, id>();
        roleRTNameMap = new map <id, string>();

        //Retrieve Role Record Types
        RoleRTs = setRoleRTs();

        setBusinessRelationshipTypes();
        //Retrieve Account - Role Field Mapping for prepopulating Role fields
        accToRoleMapping = [Select Id, Account_Field__c, Role_Field__c From Account_To_Role_Field_Mapping__mdt];
        //Retrieve Relationship Record Type Names
        relRTNameByIdMap = new map <string,string>();
        for (String keyname : relRTMapByName.keySet()){
            string rtkey = relRTMapByName.get(keyname).getRecordTypeId();
            if (!relRTNameByIdMap.containskey(rtkey)){
                relRTNameByIdMap.put(rtKey, keyname);
            }
        }
        rel = new Relationship__c(
            RecordTypeId = relRTMapByName.get('Business Relationship').getRecordTypeId()
        );
    }

    private void retrieveCase(){
        string cseQuery = 'Select Id, CaseNumber, RecordTypeId, RecordType.Name, Application_Name__c, ';
        cseQuery+= 'Application_Name__r.Name, Application_Name__r.RecordTypeId, Application_Name__r.RecordType.Name, Partition__c, ';
        string roleQUery = 'Select Id, Name, RecordTypeId, RecordType.Name, Case__c, Account__c, Account__r.Name, Account__r.FirstName, Account__r.MiddleName, Account__r.LastName, Account__r.PersonEmail, Account__r.PersonBirthDate, Account__r.PersonMobilePhone, Account__r.isPersonAccount, Account__r.ABN__c, Account__r.ACN__c, Account__r.Full_Address__c , Application__c ';
        roleQUery+= ' From Roles__r Order By Name ASC'; //Related Roles
        roleQUery= '(' + roleQUery + ')';
        cseQuery+= roleQUery + ' From Case ';
        if (curID.getSObjectType() == Schema.Case.SobjectType){ //ID is from Case
            cseQuery+= 'Where Id = : curID limit 1';
        }
        else if (curID.getSObjectType() == Schema.Application__c.SobjectType){ //ID is from Application
            cseQuery+= 'Where Application_Name__c = : curID limit 1';
        }
        cse = Database.query(cseQuery);

        accOfApplist = new list <accWRapper>();
        set <Id> roleAccId = new set <Id>();
        //Retrieving Accounts on existing Roles of Case/Application
        if (!cse.Roles__r.isEmpty()){
            for (Role__c crole : cse.Roles__r){
                if (crole.Account__c != null){
                    if (!roleAccId.contains(crole.Account__c)){ //Check for duplicate Accounts assigned in Roles
                        if (crole.Account__r.isPersonAccount){
                            accWrapper acWr = new accWrapper(
                                new Account(Id = crole.Account__c,
                                    PersonEmail = crole.Account__r.PersonEmail,
                                    PersonBirthDate = crole.Account__r.PersonBirthDate,
                                    PersonMobilePhone = crole.Account__r.PersonMobilePhone,
                                    FirstName = crole.Account__r.FirstName,
                                    MiddleName = crole.Account__r.MiddleName,
                                    LastName = crole.Account__r.LastName
                                )
                            );
                            acWr.accName = crole.Account__r.Name;
                            acWr.isPerson = true;
                            acWr.recTypeName = crole.Account__r.Name;
                            acWr.fullAddress = crole.Account__r.Full_Address__c;
                            accOfApplist.add(acWr);
                        }
                        else{
                            accWrapper acWr = new accWrapper(
                                new Account(Id = crole.Account__c, 
                                    Name = crole.Account__r.Name,
                                    ABN__c = crole.Account__r.ABN__c,
                                    ACN__c = crole.Account__r.ACN__c
                                )
                            );  
                            acWr.accName = crole.Account__r.Name;
                            acWr.recTypeName = crole.Account__r.Name;
                            acWr.fullAddress = crole.Account__r.Full_Address__c;
                            accOfApplist.add(acWr);
                        }
                        roleAccId.add(crole.Account__c);
                    }
                }
            }
        }
        if (accOfApplist.size() == 1){
            accOfApplist[0].isSelected = true;
        }
    }

    private void setBusinessRelationshipTypes(){
        relTypeWrlist = new list <relationshipTypeWrapper>();
        relTypeWrlist.add(new relationshipTypeWrapper('Primary Beneficiary'));
        relTypeWrlist.add(new relationshipTypeWrapper('Director'));
        relTypeWrlist.add(new relationshipTypeWrapper('Major Shareholder'));
        relTypeWrlist.add(new relationshipTypeWrapper('Partner'));
        relTypeWrlist.add(new relationshipTypeWrapper('Trustee'));
        relTypeWrlist.add(new relationshipTypeWrapper('Unit Holder'));
        relTypeWrlist.add(new relationshipTypeWrapper('Guarantor'));
    }

    private void defaultRoleTypeByRoleRT(){
        if (roleRTNameMap.get(role.RecordTypeId) == 'Company Director' || roleRTNameMap.get(role.RecordTypeId).contains('Guarantor')){
            role.Role_Type__c = 'Guarantor';
        }
        else if (roleRTNameMap.get(role.RecordTypeId) == 'Non-Borrowing Spouse'){
            role.Role_Type__c = 'Non-Borrowing Spouse';
        }
        else if (roleRTNameMap.get(role.RecordTypeId) == 'Trustee - Company' || roleRTNameMap.get(role.RecordTypeId) == 'Trustee - Individual'){
            role.Role_Type__c = 'Trustee';
        }
        else if (roleRTNameMap.get(role.RecordTypeId) == 'Partner - Company' || roleRTNameMap.get(role.RecordTypeId) == 'Partner - Individual' || roleRTNameMap.get(role.RecordTypeId) == 'Partner - Trust' ){
            role.Role_Type__c = 'Partner';
        }
        
    }

    public pageReference checkIfPersonOrBusiness(){
        system.debug('@@roleDesignation:'+roleDesignation);
        if (roleRTNameMap.containskey(role.RecordTypeId)){
            if(roleRTNameMap.get(role.RecordTypeId).contains('- Company') && roleRTNameMap.get(role.RecordTypeId) != 'Company Director' || 
               roleRTNameMap.get(role.RecordTypeId).contains('- Business') || roleRTNameMap.get(role.RecordTypeId).contains(' - Trust') || 
               roleRTNameMap.get(role.RecordTypeId).contains('- Partnership')){
                roleDesignation = 'Business'; //
            }
            else roleDesignation = 'Person';
        }
        else roleDesignation = '';
        acc.ACN__c = null;
        clickCheckDup = false;
        defaultRoleTypeByRoleRT();
        return null;
    }

    public pageReference ignoreDuplicate(){
        ignoreDupe = validForSaving = true;
        if (acc.Id != null) acc.Id = null;
        return null;
    }

    public list <SelectOption> getRelAllowedRTs(){
        list <SelectOption> options = new list <SelectOption>();
        options.add(new SelectOption(
                        relRTMapByName.get('Business Relationship').getRecordTypeId(),
                        'Business Relationship'
                    ));
        options.add(new SelectOption(
                        relRTMapByName.get('Spouse').getRecordTypeId(),
                        'Spouse'
                    ));        
        return options;
    }

    public pageReference defaultRelationshipType(){
        if (rel.RecordTypeId == relRTMapByName.get('Spouse').getRecordTypeId()){
            rel.Relationship_Type__c = 'Spouse';
        }
        else rel.Relationship_Type__c = null;
        rel.Start_Date__c = null;
        return null;
    }
    public boolean clickCheckDup {get;set;}
    public pageReference checkDuplicates(){
        clickCheckDup = true;
        dupeExAccs = new list <Account>();
        accDupeWrlist = new list <accWrapper>();
        ignoreDupe = dupeAccFound = false;
        boolean isPartnerOrTrustee = false;
        final String nmIndividual = 'nm: Individual';
        
        if(roleRTNameMap.get(role.RecordTypeId) == 'Trustee - Company' ||  roleRTNameMap.get(role.RecordTypeId) == 'Partner - Company') isPartnerOrTrustee = true;//RDAVID 30/07/2018 - Bug fix ABN and ACN Issue from Chris
        System.debug('checkDuplicates = '+acc.ACN__c);
        if (validateAccount()){
            string accPreQuery = 'Select Id, Name, ABN__c, ACN__c, FirstName, MiddleName, LastName, PersonEmail, PersonMobilePhone, PersonBirthDate, Address__c, Full_Address__c, Partition__c ';
            if (roleDesignation == 'Business'){
                string accPostQuery = ' From Account ';
                if(!isPartnerOrTrustee){
                    if (acc.ABN__c != null){ 
                        string aABN = acc.ABN__c;
                        accPostQuery+= ' WHere ABN__c = : aABN ';
                    }
                }
                else{
                    if (acc.ACN__c != null){ 
                        string aACN = acc.ACN__c;
                        accPostQuery+= ' WHere ACN__c = : aACN ';
                    }
                }

                string fsfields = constructQueryFromFieldSets(accPreQuery, 'Account');
                if (fsfields != '') accPreQuery+= ', ';
                dupeExAccs = database.query(accPreQuery + fsfields + accPostQuery); 
            }
            else if (roleDesignation == 'Person'){
                string accPostQuery = ' From Account Where IsPersonAccount = true AND RecordType.Name = :nmIndividual';
                if (con.FirstName != null){
                    string cfname = con.FirstName;
                    accPostQuery+= ' AND FirstName = : cfname ';
                }
                if (con.MiddleName != null){
                    string cmname = con.MiddleName;
                    accPostQuery+= ' AND MiddleName = : cmname ';
                }
                if (conLastName != '' && conLastName != null) accPostQuery+= ' AND LastName = : conLastName ';
                if (con.BirthDate != null){
                     Date cbirthdate = con.BirthDate;
                     accPostQuery+= ' AND PersonBirthDate = : cbirthdate ';
                }
                if (con.MobilePhone != null){ 
                    String cmphone = con.MobilePhone;
                    accPostQuery+= ' AND PersonMobilePhone = : cmphone ';
                }
                if (con.Email != null){ 
                    string cemail = con.Email;
                    accPostQuery+= ' AND PersonEmail = : cemail ';
                }
                
                string fsfields = constructQueryFromFieldSets(accPreQuery, 'Account');
                if (fsfields != '') accPreQuery+= ', ';
                accPostQuery+= ' Order By Name ASC';
                dupeExAccs = database.query(accPreQuery + fsfields + accPostQuery); 
            }
            if (!dupeExAccs.isEmpty()){ 
                dupeAccFound = true;
                for (Account dacc : dupeExAccs){
                    accWrapper accDupeWr = new accWrapper(dacc);
                    accDupeWrlist.add(accDupeWr);
                }
            }
            else {
                dupeAccFound = false;
                validForSaving = true;
            }
            showDupeMsg = true;
        }
        System.debug('validForSaving = '+validForSaving);
        System.debug('dupeAccFound = '+dupeAccFound);
        System.debug('clickCheckDup = '+clickCheckDup);
        
        return null;
    }

    private boolean checkDuplicateAddress(){
        boolean dupeAddrfound = false;
        list <Address__c> exdupeAddresses = [Select Id, Name, Location__c, Suburb__c, Street_Name__c, Street_Type__c, Street_Number__c From Address__c Where Location__c =: addr.Location__c];
        if (!exdupeAddresses.isEmpty()){
            for (Address__c exaddr : exdupeAddresses){
                string addrmatchKey = exaddr.Location__c + '-' + exaddr.Street_Name__c + '-' + addr.Street_Number__c + '-' + exaddr.Street_Type__c;
                string enteredAddress = addr.Location__c + '-' + addr.Street_Name__c + '-' + addr.Street_Number__c + '-' + addr.Street_Type__c;
                if (addrmatchKey == enteredAddress){
                    raddr.Address__c = exaddr.Id; //Populate Role Address with found Address
                    dupeAddrfound = true;
                    break;
                }
            }
        }
        return dupeAddrfound;
    }

    public PageReference populateAccountBaseOnSelectedDuplicate(){
        for (accWrapper accDupeWr : accDupeWrlist){
            if (accDupeWr.isSelected){
                acc = accDupeWr.acc; //Preopulate fields with selected Duplicate
                if (roleDesignation == 'Person'){
                    con.FirstName = accDupeWr.acc.FirstName;
                    con.MiddleName = accDupeWr.acc.MiddleName;
                    conLastName = accDupeWr.acc.LastName;
                    con.BirthDate = accDupeWr.acc.PersonBirthDate;
                    con.MobilePhone = accDupeWr.acc.PersonMobilePhone;
                    con.Email = accDupeWr.acc.PersonEmail;
                    if (acc.Address__c != null){
                        raddr.Address__c = acc.Address__c;
                    }
                    populateRoleFieldsBaseOnSelectedDuplicate(acc);
                }
                validForSaving = true;
                break;
            }
        }
        system.debug('@@validForSaving:'+validForSaving);
        return null;
    }

    private void populateRoleFieldsBaseOnSelectedDuplicate(Account thisacc){
        for (Account_To_Role_Field_Mapping__mdt atrfm : accToRoleMapping){
            if (atrfm.Account_Field__c != 'x'){
                if (atrfm.Account_Field__c == 'Bankrupt_Previously__pc'){ //Handle Previously Bankrupt in Role
                    if (acc.get(atrfm.Account_Field__c) == true) role.put(atrfm.Role_Field__c, 'Yes');
                    else role.put(atrfm.Role_Field__c, 'No');
                }
                else if (atrfm.Account_Field__c == 'Primary_Country_of_Citizenship__c' || atrfm.Account_Field__c == 'Secondary_Country_of_Citizenship__c'){ //Handle Citizenship fields in Role
                    if (acc.get(atrfm.Account_Field__c) == 'Australia' || acc.get(atrfm.Account_Field__c) == 'New Zealand' || acc.get(atrfm.Account_Field__c) == 'Other'){
                        role.put(atrfm.Role_Field__c, acc.get(atrfm.Account_Field__c));   
                    }
                }
                else role.put(atrfm.Role_Field__c, acc.get(atrfm.Account_Field__c));
            }
        }

    }

    private boolean validateAccount(){
        boolean passed = true;
        if (roleDesignation == 'Person'){
            if (conLastName == ''){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Last Name is required'));
                hasAccErrors = true;
                passed = false;
            }
            if (String.isBlank(con.Email) && roleRTNameMap.get(role.RecordTypeId) != 'Non-Borrowing Spouse' ) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Email is required'));
                hasAccErrors = true;
                passed = false;
            }
            if (String.isBlank(con.MobilePhone) && roleRTNameMap.get(role.RecordTypeId) != 'Non-Borrowing Spouse' ){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Mobile is required'));
                hasAccErrors = true;
                passed = false;
            }
        }
        else if (roleDesignation == 'Business'){
            boolean isPartnerOrTrustee = false;
            
            if(roleRTNameMap.get(role.RecordTypeId) == 'Trustee - Company' ||  roleRTNameMap.get(role.RecordTypeId) == 'Partner - Company') isPartnerOrTrustee = true;//RDAVID 30/07/2018 - Bug fix ABN and ACN Issue from Chris
            System.debug('validateAccount = isPartnerOrTrustee '+isPartnerOrTrustee);
            if(!isPartnerOrTrustee){
                if (acc.ABN__c == null){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'ABN is required'));
                    hasAccErrors = true;
                    passed = false;
                }                
            }
            else{
                System.debug('validateAccount = acc.ACN__c '+acc.ACN__c);
                if (acc.ACN__c == null){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'ACN is required'));
                    hasAccErrors = true;
                    passed = false;
                }
            }
        }
        if (passed) hasAccErrors = false;
        return passed;
    }

    public pageReference saveAccount(){
        Savepoint sp = Database.setSavepoint();
        System.debug('@@@acc =  '+acc);
        try{
            if (roleDesignation == 'Person'){
                if (!dupeAccFound || ignoreDupe){
                    acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('nm: Individual').getRecordTypeId();
                    acc.FirstName = con.FirstName;
                    acc.MiddleName = con.MiddleName;
                    acc.LastName = conLastName;
                    acc.PersonEmail = con.Email;
                    acc.PersonMobilePhone = con.MobilePhone;
                    acc.PersonBirthDate = con.BirthDate;
                    
                }
                
            }
            else { //Business
                if (roleRTNameMap.get(role.RecordTypeId).contains('- Company')) acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('nm: Company').getRecordTypeId();
                if (roleRTNameMap.get(role.RecordTypeId).contains('- Trust')){
                    acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('nm: Trust').getRecordTypeId();
                    acc.Name = acc.Registered_Business_Name__c;
                }
                else acc.Name = acc.Business_Legal_Name__c;
            }

            //JVGO 8/12/2019 assign the case's partition to related account.
            if(acc.Partition__c == NULL){
                acc.Partition__c = cse.Partition__c;
            }
            
            if (acc.Id == null) upsert acc;
            saveAccountSuccess = true;
        }
        catch(Exception e){
            saveAccountSuccess = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
            hasErrors = true;
            Database.rollback( sp );
        }
        return null;
    }

    public pageReference validateAddRole(){
        integer roleselCtr = 0;
        validationSuccess = true;
        if (validateAccount()){
            if (roleDesignation == 'Business'){
                if (roleRTNameMap.get(role.RecordTypeId).contains('- Trust')){
                    if (acc.Registered_Business_Name__c == null){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Registered Business Name is required'));
                        validationSuccess = false;
                        hasErrors = true;
                    }
                }
                else{
                    if (acc.Business_Legal_Name__c == null){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Business Legal Name is required'));   
                        validationSuccess = false;
                        hasErrors = true;
                    }
                }
            }
            for (accWrapper accWr : accOfApplist){
                if (accWR.isSelected) roleselCtr++;
            }
            if (roleselCtr == 0){
                validationSuccess = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select Account to relate'));
                hasErrors = true;
            }
            
            if(roleRTNameMap.get(role.RecordTypeId) != 'Company Director'){
                if (raddr.Start_Date__c == null){
                validationSuccess = false;
                raddr.Start_Date__c.addError('Start Date is required');
                hasErrors = true;
                }
                if (raddr.Address__c == null){
                    validationSuccess = false;
                    raddr.Address__c.addError('Address is required');
                    hasErrors = true;
                }
            }
            if (rel.RecordTypeId != relRTMapByName.get('Spouse').getRecordTypeId()){
                if (rel.Start_Date__c == null){
                    validationSuccess = false;
                    rel.Start_Date__c.addError('Start Date is required');
                    hasErrors = true;
                }
                integer relTypeCtr = 0;
                for (relationshipTypeWrapper relTypeWr : relTypeWrlist){
                    if (relTypeWR.isSelected) relTypeCtr++;
                }
                if (relTypeCtr == 0){
                    validationSuccess = false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select Relationship Type'));
                    hasErrors = true;
                }
            }
            if (validationSuccess) hasErrors = false;
        }
        else { validationSuccess = false; hasErrors = false; }
        return null;
    }

    public pageReference saveRole(){
        Savepoint sp = Database.setSavepoint();
        try{
            // RDAVID 30/01 TeamWorkId - 22594324
            if(roleDesignation == 'Person') populateContactDetails();
            role.Partition__c = cse.Partition__c;
            role.Account__c = acc.Id; //Assign created Account
            upsert role;
            crerole = [Select Id, Name From Role__c Where Id = : role.Id];
            saveRoleSuccess = true;
        }
        catch(Exception e){
            saveRoleSuccess = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
            hasErrors = true;
            Database.rollback( sp );
        }
        return null;
    }

    //RDAVID 30/01 TeamWorkId - 22594324
    public void populateContactDetails(){
        role.Mobile_Phone__c = acc.PersonMobilePhone;
        role.Email__c = acc.PersonEmail;
    }

    public pageReference saveRoleAddress(){
        Savepoint sp = Database.setSavepoint();
        try{
            raddr.Role__c = role.Id;
            upsert raddr;
            saveRoleAddressSuccess = true;
        }
        catch(Exception e){
            saveRoleAddressSuccess = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
            hasErrors = true;
            Database.rollback( sp );
        }
        return null;
    }

    public pageReference saveRelationships(){
        Savepoint sp = Database.setSavepoint();
        try{
            //system.debug('@@relationshipTypes:'+relationshipTypes);
            list <Relationship__c> rellist = new list <Relationship__c>();
            for (accWrapper accWR : accOfApplist){
                if (accWr.isSelected){
                    //Set Master Account for Relationship
                    rel.Master_Account__c = accWr.acc.Id;
                    break;
                }
            }
            rel.Associated_Account__c = acc.Id;
            if (rel.RecordTypeId == relRTMapByName.get('Spouse').getRecordTypeId()){
                rellist.add(rel);
            }
            else{ //Business Relationship RT
                //Create multiple Relationships base on selected Relationship Type
                for (relationshipTypeWrapper reltypeWR : relTypeWrlist){
                    if (reltypeWR.isSelected){
                        Relationship__c tempRel = new Relationship__c(
                            Master_Account__c = rel.Master_Account__c,
                            Associated_Account__c = acc.Id,
                            RecordTypeId = rel.RecordTypeId,
                            Start_Date__c = rel.Start_Date__c
                        );
                        tempRel.Relationship_Type__c = reltypeWR.relType;
                        
                        rellist.add(tempRel);
                    }
                }
            }
            upsert rellist;
            saveRelationshipSuccess = true;
        }
        catch(Exception e){
            saveRelationshipSuccess = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
            hasErrors = true;
            Database.rollback( sp );
        }
        return null;
    }

    public PageReference showNewAddress(){
        addr = new Address__c();
        return null;
    }

    public PageReference populateAddresswithLocation(){
        if (addr.Location__c != null){
            //Retrieve selected Location
            list <Location__c> loclist = [Select Id, Suburb_Town__c, Postcode__c, State__c, State_ACR__c, Country__c From Location__c Where Id = : addr.Location__c AND Name != 'UNKNOWN' limit 1];
            if (!loclist.isEmpty()){
                //Populate Address fields with Location
                addr.Suburb__c = loclist[0].Suburb_Town__c;
                addr.Postcode__c = loclist[0].Postcode__c;
                addr.State__c = loclist[0].State__c;
                addr.State_ACR__c = loclist[0].State_ACR__c;
                addr.Country__c = loclist[0].Country__c;
            }
        }
        return null;
    }

    public boolean validateAddress(){
        boolean passed = true;
        integer errctr = 0;
        if (addr.Location__c == null){
            addr.Location__c.addError('Location is required');
            errctr++;
        }
        if (addr.Street_Number__c == '' || addr.Street_Number__c == null){
            addr.Street_Number__c.addError('Street Number is required');
            errctr++;
        }
        if (addr.Street_Name__c  == '' || addr.Street_Name__c  == null ){
            addr.Street_Name__c.addError('Street Name is required');
            errctr++;
        }
        if (addr.Street_Type__c == '' || addr.Street_Type__c == null ){ 
            addr.Street_Type__c.addError('Street Type is required');
            errctr++;
        }
        if (errctr > 0) passed = false;
        return passed;
    }

    public pageReference saveAssignNewAddress(){
        Savepoint sp = Database.setSavepoint();
        try{
            if (validateAddress()){
                if (!checkDuplicateAddress()){
                    insert addr;
                    raddr.Address__c = addr.Id;
                }
                showAddressError = false;
                return null;
            }
            else showAddressError = true;
        }
        catch(Exception e){
            System.debug(e.getMessage());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
            hasErrors = true;
            Database.rollback( sp );  
        }
        return null;
    }


    private string constructQueryFromFieldSets(String objpreQuery, String ObjectName){

        string FSFields = ' ';
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        map <string, Schema.FieldSet> FOfsMap = DescribeSObjectResultObj.FieldSets.getMap();
        for (Schema.FieldSet fs : FOfsMap.values()){ //Check all FieldSets in object
            for (Schema.FieldSetMember field : fs.getFields()){ //get Fields on a specific Field Set
                string fp = field.getFieldPath();
                if (!objpreQuery.contains(fp) && !FSFields.contains(fp) ){ //Prevents duplicate fields in query
                    FSFields+= fp + ',';
                }
            }
        } 
        for (Account_To_Role_Field_Mapping__mdt atrfm : accToRoleMapping){ //Add fields from Custom Metadata
            if (atrfm.Account_Field__c != 'x'){
                string fp = atrfm.Account_Field__c;
                if (!objpreQuery.contains(fp) && !FSFields.contains(fp) ){ //Prevents duplicate fields in query
                    FSFields+= fp + ',';
                }
            }
        }
        FSFields = FSFields.subString(0,FSFields.Length()-1); //trim last comma
        system.debug('@@FSFields:'+FSFields);
        return FSFields;
    }

    //Builds select options of Role Record Types
    private list <SelectOption> setRoleRTs(){ //Dependent on Selected Case RT
        list <SelectOption> options = new List <SelectOption>();
        options.add(new SelectOption('', '--Please Select--'));
        //Retrieving Record Types via SOQL will not be able to display available Record Types by Profile
        list <RecordTypeInfo> infos = Role__c.SobjectType.getDescribe().getRecordTypeInfos();
        
        if(currentUrl.containsIgnoreCase('Applicant')) isAddApplicant = true;

        for (RecordTypeInfo i : infos){
            if (i.isActive()){
                if (i.isAvailable()){
                    if(!isAddApplicant){
                        if (i.getName() != 'Master' && !i.getName().startsWith('Applicant') && !i.getName().startsWith('Lead') && !i.getName().startsWith('Purchaser') ){ //Exclude Master, Applicant, Lead, Purchaser Role RTs
                            options.add(new SelectOption(i.getRecordTypeId(), i.getName()));
                            roleRTMap.put(i.getName(), i.getRecordTypeId());
                            roleRTNameMap.put(i.getRecordTypeId(),i.getName());
                        }
                    }
                    else{
                        if(i.getName().startsWith('Applicant')){
                            options.add(new SelectOption(i.getRecordTypeId(), i.getName()));
                            roleRTMap.put(i.getName(), i.getRecordTypeId());
                            roleRTNameMap.put(i.getRecordTypeId(),i.getName());
                        }
                    }
                }
            }
        }
        return options;
    }

    public class relationshipTypeWrapper{
        public boolean isSelected {get;set;}
        public string relType {get;set;}
        public relationshipTypeWrapper(String relType1){
            relType = relType1;
            isSelected = false;
        }
    }


    public class accWrapper{
        public boolean isPerson {get;set;}
        public string accName {get;set;}
        public string recTypeName {get;set;}
        public string fullAddress {get;set;}
        public boolean isSelected {get;set;}
        public Account acc {get;set;}
        public accWrapper(Account acc1){
            acc = acc1;
            isPerson = isSelected = false;
            accName = fullAddress = recTypeName = '';
        }
    }


}
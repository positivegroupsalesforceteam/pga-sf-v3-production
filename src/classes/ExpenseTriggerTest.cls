/** 
* @FileName: ExpenseTriggerTest
* @Description: Test class for Expense Trigger 
* @Copyright: Positive (c) 2018 
* @author: Rex David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/19/18 RDAVID Created class (setupData,testExpenseToRole1)
* 2.0 4/25/18 JBACULOD Removed existing test setup and test methods, added testautoCreateFOShareExpense
**/ 
@isTest 
public class ExpenseTriggerTest{

    @testsetup static void setup(){
        TestDataFactory.Case2FOSetupTemplate();
    }

    static testmethod void testautoCreateFOShareExpense(){

        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Expense_Trigger__c = true,
                Enable_FO_Auto_Create_FO_Share__c = true,
                Enable_FO_Delete_FO_Share_on_FO_Delete__c = true
        );
        insert trigSet;

        //Retrieve Role Test data from Setup
        list <Role__c> rolelist = [Select Id, Case__c, Application__c From Role__c];
        string foshareRoleIDs = '';
        for (Integer i=0; i<10; i++){
            foShareRoleIds+= rolelist[i].Id + ';';
        }

        Test.startTest();

            //Create test data for Expense, autoCreating FO Share
            Id expRTId = Schema.SObjectType.Expense1__c.getRecordTypeInfosByName().get(CommonConstants.EXPENSE_RT_AE).getRecordTypeId(); //Accommodation Expense
            list <Expense1__c> explist = new list <Expense1__c>();
            for (Integer i=0; i<50; i++){
                Expense1__c exp = new Expense1__c(
                    h_Auto_Create_FO_Share__c = true,
                    RecordTypeId = expRTId,
                    Equal_Share_for_Roles__c = true,
                    h_FO_Share_Role_IDs__c = foShareRoleIDs,
                    Application1__c = rolelist[0].Application__c,
                    Payment_Amount__c  = 1000
                );
                explist.add(exp);
            }
            insert explist;

            //Retrieve created FO Shares
            list <FO_Share__c> fosharelist = [Select Id, Case__c, Application_Expense__c, Monthly_Amount__c, Expense__c, Expense__r.Expense_Type__c, Expense__r.RecordType.Name, Type__c, FO_Record_Type__c, Role__c, Role__r.Application__c, Role_Share__c, Role_Share_Amount__c From FO_Share__c];
            for (FO_Share__c FOs : fosharelist){
                //Verify Expense Details were populated in FO SHare
                system.assertEquals(FOs.Expense__r.RecordType.Name, FOs.FO_Record_Type__c);
                system.assertEquals(FOs.Expense__r.Expense_Type__c, FOs.Type__c);
                //Verify Expense, Case, Application were linked to FO Share
                system.assert(FOs.Expense__c != null);
                system.assert(FOs.Case__c != null);
                system.assert(FOs.Application_Expense__c != null);
                //Verify Equal Share was split to each Role in Application
                Decimal dval = FOs.Role_Share__c;
                dval = dval.setScale(2);
                Decimal drolesize = 10; //# of Roles assigned in FO
                drolesize = drolesize.setScale(2);
                system.assertEquals(dval, drolesize);
                //Verify Role Share Amount was calculated
                Decimal dmval = FOs.Role_Share_Amount__c;
                dmval = dmval.setScale(2);
                Decimal dsval = FOs.Monthly_Amount__c * (FOs.Role_Share__c / 100);
                dsval = dsval.setScale(2);
                system.assertEquals(dmval, dsval );
            }

            //Cover Expense update, delete trigger events
            update explist;
            delete explist;

        Test.stopTest();


    } 


}
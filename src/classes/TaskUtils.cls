public class TaskUtils {

	@InvocableMethod(label='Update Calls Made' description='updates Calls Made on Lead/Opp once Completed Outbound task is created')
	public static void updateCallsMade (list <ID> taskIDs) {
		set <Id> whoIdSet = new set <Id>();
		set <id> whatIdSet = new set <Id>();
		list <Task> tsklist = [Select Id, WhoId, WhatId  From Task Where Id in : taskIDs];
		for (Task tsk : tsklist){
			if (tsk.WhoId !=  null){
				whoIdSet.add(tsk.WhoId);
			}
			if (tsk.WhatId != null){
				whatIdSet.add(tsk.WhatId);
			}
		}
		list <Lead> upLdlist = [Select Id, Calls_Made__c From Lead Where Id in : whoIdSet];
		map <Id, list <Task>> leadTskMap = new map <id, list <Task>>();
		for (Task tsk : [Select Id, WhoId, CallType From Task Where CallType='Outbound' and Status='Completed' and WhoId in : whoIdSet]){
			if (!leadTskMap.containsKey(tsk.WhoId)){
				leadTskMap.put(tsk.WhoId, new list <Task>());
			}
			leadTskMap.get(tsk.WhoId).add(tsk);
		}
		if (upLdlist.size() > 0){
			for (Lead ld : upLdlist){
				if (leadTskMap.containskey(ld.Id)){
					//ld.Calls_Made__c = ld.Calls_Made__c + 1; 
					ld.Calls_Made__c = leadTskMap.get(ld.Id).size();
				}
			}
			update upLdlist;
		}
		//Task
		list <Opportunity> upOpplist = [Select Id, Calls_Made__c From Opportunity Where Id in : whatIdSet];
		map <Id, list <Task>> oppTskMap = new map <id, list <Task>>();
		for (Task tsk : [Select Id, WhatId, CallType From Task Where CallType='Outbound' and Status='Completed' and WhatId in : whatIdSet]){
			if (!oppTskMap.containsKey(tsk.WhatId)){
				oppTskMap.put(tsk.WhatId, new list <Task>());
			}
			oppTskMap.get(tsk.WhatId).add(tsk);
		}
		if (upOpplist.size() > 0){
			for (Opportunity opp : upOpplist){
				if (oppTskMap.containskey(opp.Id)){
					//opp.Calls_Made__c = opp.Calls_Made__c + 1;
					opp.Calls_Made__c = oppTskMap.get(opp.Id).size();
				}
			}
		}
	}
}
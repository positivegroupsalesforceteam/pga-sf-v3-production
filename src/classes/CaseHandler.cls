/** 
* @FileName: CaseHandler
* @Description: Trigger Handler for the Case SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/19/18 JBACULOD Created Class, [SD-19] Auto Create Application on Case Insert
* 1.1 3/21/18 RDAVID Modified Class, [SD-27] SF - New Model - Setup Scenario Cases (Automations) - Move Scenario Trigger (Old model) to Case Trigger
* 1.2 4/06/18 RDAVID Added SD-50 SF - NM - Auto Trigger Lender API Call 
* 1.3 5/11/18 JBACULOD Added Nodifi SLA calculation and modified RecordStage Time Stamps
* 1.4 6/13/18 JBACULOD Added updating Email/Phone fields in Case Marketing Cloud Contacts section
* 1.5 8/22/18 JBACULOD Added logic for updating related fields from lookups (created due to object reference limit)
* 1.6 8/25/18 JBACULOD Added PWM RT on NM Case Trigger
* 1.7 10/03/18 JBACULOD added recordStatusStageFunnels in beforeInsert and beforeUpdate
* 2.0 9/28/18 RDAVID - extra/DialerAutomationUpdates - Added logic to populate NVM Fields on beforeInsert **for Partition = 'Positive'
* 2.1 10/05/18 RDAVID - extra/MergeCaseTrigger - Merge recordStatusStageFunnels and extra/DialerAutomationUpdates (Jesfer and Rex Changes) in UAT to deploy to Production.
* 2.2 11/23/18 JBACULOD - added populating of DD Contact Number base from Primary Contact Phone
* 2.3 19/11/18 RDAVID - extra/NVMTimeZoneBasedRouting - Continue NVM Timezone-based Routing. 
* 2.4 12/04/18 JBACULOD - extra/CaseKARSalesOpp - Added creating of KAR - Sales Opportunity on Submitted NOD/POS Consumer Asset Finance
* 2.5 12/12/18 JBACULOD - extra/SupportRequestManagerReview - Added creating of Manager Review Support Requests on reviewed Closed Lost POS Consumer/Commercial Asset Finance
* 2.6 01/14/19 RDAVID - extra/NVMTimeZoneBasedRouting2 - Fix Cases not sending to NVM 
* 2.7 01/23/19 RDAVID - extra/PHLOpp-Tasks - Move "Case - Create POS: PHL Opportunity" Process Builder to Trigger. 
* 2.8 01/29/19 RDAVID - extra/PHLOpp-Tasks - Added Stage__c = "Won" in the criteria
* 3.0 26/02/19 RDAVID - extra/SectorAutomation (Teamwork tasks/23413929) - Added logic to insert Sector records.
* 3.1 7/03/19 RDAVID - extra/SectorAutomation (Teamwork tasks/23721938) - Added logic to insert/update SLA Sector records.
* 3.2 26/04/19 RDAVID - extra/task24368245-SetOwnerWhenCaseNewToClosed - Set Owner to Current User When Updated to Closed
* 3.2 25/04/19 RDAVID - extra/task24148707-SectorSLAFixBusinessHours - Added logic to only create Is_Active__c SLA Sector 
* 3.3 2/05/19 RDAVID - extra/task24399056-InitialReviewSRandSLA - Added logic to auto-create "NOD Initial Review SR" 
* 3.4 31/05/19 JBACULOD - Added Attempted SMS automation
* 3.4 03/06/19 JBACULOD - added extra criteria for SMS Automation
* 3.5 17/06/19 RDAVID - extra/task24943632-BugFixOverFlowAssignedCases - Added logic to exclude users and Only allow it to Team/Queue
* 3.6 19/06/19 JBACULOD - Added bypass for processCaseOutsideBusinessHours when Queue is LFP Flow
* 3.7 21/06/19 JBACULOD - added referredOutByOwner - Moved Referred Out By Owner process from Process Builder to Trigger
* 3.8 25/06/19 JBACULOD - added addtional criteria for calling processCaseOutsideBusinessHours
* 3.9 8/07/2019 RDAVID - extra/task25140648 - Added && cs.Lead_Bucket__c != null
* 4.0 18/07/2019 JBACULOD - tasks/25365170 - Added automation for deleting CommissionCT and Commission Lines on Closed Lost
* 4.1 24/07/2019 RDAVID extra/tasks25429652-PopulateLeadCreationMethod - Populate Lead_Creation_Method__c for KAR: Sales Opportunity
* 4.2 31/07/2019 JBACULOD - tasks/25494330 - Added support for NOD KAR Automation SMS
* 4.3 5/08/2019 RDAVID - tasks/25563890 - Added logic to update Commission Line Items CommissionCT_Parent_Won__c = TRUE
* 4.5 26/08/2019 RDAVID - extra/task25582197-NewSupportRequestVFChanges  Workaround to not allow user to manually move the stage in Case record.    
* 4.6 18/09/2019 RDAVID - extra/task25953355-NODFlowV3SFAutomations - Added condition to exclude Channel == LFPWBC
* 4.7 3/12/2019 RDAVID - extra/task26978815-DisableVedaCreditCheckAutomation - Removed Is_Veda_Credit_Checked__c
* 4.8 24/12/2019 JBACULOD - issuefix/SFBAU-20 - Added workaround for running PB Sched Action with Inactive Users for Remove Callback
**/ 
public without sharing class CaseHandler implements ITrigger {	

	private list <Application__c> lig790_app2CreateList = new list <Application__c>(); //Applications to create from Cases
	private list <Case> lig790_case2UpdateList = new list <Case>(); //Cases tp update linking created Applications
	private static list <Lookup_Related_Field_Update_Setting__mdt> lrfusetCase = new list <Lookup_Related_Field_Update_Setting__mdt>();
	private static list <Case_SMS_Automation_Setting__mdt> csmsautSet = new list <Case_SMS_Automation_Setting__mdt>();
	private map <string, list <Case_SMS_Automation_Setting__mdt>> aSMSbyPartitionMap = new map <string, list <Case_SMS_Automation_Setting__mdt>>();
	private list <String> smsTaskKeyList = new list <String>();
	private map <Id, Role__c> trustPilotRoleMap = new map <id, Role__c>(); //Roles to update for its TrustPilot Unique Link when Approved
	private static Map<Id,Schema.RecordTypeInfo> caseRTMapById = Schema.SObjectType.Case.getRecordTypeInfosById();
	public static PLS_Bucket_Settings__c plsbucketset = PLS_Bucket_Settings__c.getOrgDefaults();

	private list <string> caseIDsStrForRemoveCallbackList = new list <string>();
	private static List<BusinessHours> busiHoursList = new List<BusinessHours>();//2.0 9/26/18 RDAVID
	private static BusinessHours busiHours = new BusinessHours();
	private static map <Id, sobject> RelFieldsMap = new map <id, sobject>();
	private map <Id, Case> caseAfterUpdateMap = new map <Id, Case>();
	private list <Case> caseCreateKARSalesOppList = new list <Case>();
	private list <Case> newCaseListEx = new list <Case>(); //Used for after insert/update triggers
	private list <Support_Request__c> managerReviewSRList = new list <Support_Request__c>();
	private list <Case> casesforDelCommCT = new list <Case>();

	private static map <Id, boolean> isProcessedCaseMap = new map <Id, boolean>();
	
	public static General_Settings1__c genset = General_Settings1__c.getInstance(UserInfo.getUserId());
	private Map <Id,Case> PHLOppCasesMap = new Map <Id,Case>(); 
	private Map <Id,Case> PHLOppCasesToInsert = new Map <Id,Case>(); //List contains PHL Opportunity to insert
	private Map <Id,Case> PHLOppCasesToUpdate = new Map <Id,Case>();
	public static Set<String> validSectors = new Set<String>();
	public List<Sector__c> sectorListToUps = new List<Sector__c>();
	public Map<Id,Case> casewithSectorMap = new Map<Id,Case>();
	public List<SLA_Sector__mdt> slaSectorMetaDataList = new List<SLA_Sector__mdt>();
	public Map<String,SLA_Sector__mdt> slaStrKeyMetadataInsertMap = new Map<String,SLA_Sector__mdt>();
	public Map<String,SLA_Sector__mdt> slaStrKeyMetadataUpdateMap = new Map<String,SLA_Sector__mdt>();
	private list <Support_Request__c> nodInitialReviewSRList = new list <Support_Request__c>(); //2/05/19 RDAVID - extra/task24399056
	private String validCaseRTsForInitialReviewSR;
	private static Id nodInitialReviewSRRTId = Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get(CommonConstants.SR_NOD_INIT_REV).getRecordTypeId();
	private list <Case> casesforUpdateCLI = new list <Case>(); //4.3 5/08/2019 RDAVID - tasks/25563890 

	public CaseHandler() {}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
		//if (TriggerFactory.trigset.Enable_Case_Scenario_Trigger__c && TriggerFactory.trigset.Enable_Case_SCN_calculateSLATime__c){ 
		if(busiHours.Id == NULL){
			CaseGateway.initBusinessHoursVariables();
			busiHours = CaseGateway.busiHours;
		}
		Set<String> allowedPCaseRTForPOSPHLOpp = new Set<String>{'POS: Commercial - Asset Finance','POS: Commercial - Business Funding', 'POS: Consumer - Asset Finance', 'POS: Consumer - Personal Loan'};
		if(Trigger.isUpdate){
			Map<Id,Case> oldCseMap = (Map<Id,Case>) trigger.oldMap;
			if (TriggerFactory.trigSet.Enable_Case_SMS_Alerts__c){
				if (csmsautSet.isEmpty()) csmsautSet = [Select Id, Active__c, Case_Status__c, Case_Stage__c, Channel__c, IsChanged_Field__c, IsChanged_Field_Value__c, Partition__c, Process_Category__c, SMS_Action_Key__c, Nodifi_Submission_Type__c
													From Case_SMS_Automation_Setting__mdt 
													Where Active__c = true ]; //Get Case SMS Automation Setting records
			}
			Set<Id> parentId = new Set<Id>();
			for(Case cse : (List<Case>) trigger.new){			
				//23/01/2018 RDAVID: Logic to create POS: PHL Opportunity Cases
				if(TriggerFactory.trigSet.Enable_Case_Create_POS_PHL_Opp__c && allowedPCaseRTForPOSPHLOpp.contains(caseRTMapById.get(cse.RecordTypeId).getName())){
					if(!cse.PHL_Opportunity_Created__c && (cse.Has_Owner_Occupied__c || cse.Has_Investment_Property__c)) { //RDAVID 3/12/2019 tasks/26978815 : Removed Is_Veda_Credit_Checked__c
						Case phlCase = CaseGateway.initiatePHLCase(cse,genset.Case_POS_PHL_Opportuntiy_RT_Id__c, genset.Default_Business_Hours_Id__c);
						PHLOppCasesMap.put(cse.Id,phlCase);
					}
					//23/01/2018 RDAVID: Logic to update POS: PHL Opportunity Cases
					if(oldCseMap.get(cse.Id).Stage__c != cse.Stage__c && (cse.Stage__c == 'Lost' || cse.Stage__c == 'Won') && (cse.Priority == 'Medium' || cse.Priority == 'High')){
						parentId.add(cse.Id);
					}
				}
			}
			if(parentId.size() > 0){
				for(Case cse : [SELECT Id FROM Case WHERE Parent_Case__c IN: parentId AND RecordType.Name =: 'POS: PHL Opportunity']){
					cse.Status = 'New';
					cse.Stage__c = 'Open';
					PHLOppCasesToUpdate.put(cse.Id,cse);
				}
			}
		}
	}
	
	public void bulkAfter(){

		//system.debug('@@trigger.new:'+trigger.new);
		if(validSectors.size() == 0){
			validSectors = CaseGateway.getvalidSectors();
			slaSectorMetaDataList = [SELECT Is_Active__c,Sector_Name__c,Additional_Rules__c,Related_to_Object__c,Related_to_Partition__c,Related_to_Object_RT__c,Sector_Entry_Event__c,Sector_Entry_Event_Criteria__c,Sector_Exit_Event__c,Sector_Exit_Event_Criteria__c FROM SLA_Sector__mdt WHERE Related_to_Object__c = 'Case'];
			for(SLA_Sector__mdt slaSecMeta : slaSectorMetaDataList){
				if(slaSecMeta.Sector_Entry_Event__c == 'On Create' && Trigger.isInsert && slaSecMeta.Is_Active__c)  //25/04/19 RDAVID - extra/task24148707-SectorSLAFixBusinessHours - Added logic to only create Is_Active__c SLA Sector 
				slaStrKeyMetadataInsertMap.put(slaSecMeta.Sector_Name__c+':'+slaSecMeta.Related_to_Partition__c,slaSecMeta);
				if(slaSecMeta.Sector_Entry_Event__c == 'On Update' || slaSecMeta.Sector_Exit_Event__c == 'On Update' && Trigger.isUpdate)
				slaStrKeyMetadataUpdateMap.put(slaSecMeta.Sector_Name__c+':'+slaSecMeta.Related_to_Partition__c,slaSecMeta);
				//2/05/19 RDAVID - extra/task24399056 Get valid RTs from Custom Metadata
				if(String.IsBlank(validCaseRTsForInitialReviewSR) && slaSecMeta.Related_to_Partition__c == 'Nodifi' && !slaSecMeta.Sector_Name__c.containsIgnoreCase('Scenario')){
					validCaseRTsForInitialReviewSR = slaSecMeta.Related_to_Object_RT__c;
				}
			}
		}

		//For Retrieving related data in lookup fields
		if (trigger.new != null){ //Trigger event is on Create/Update
			list <sobject> exCseList = trigger.new;		
			string exCseQuery = '';
			string preexCseQuery = 'Select Id, Status, Stage__c, RecordTypeId, RecordType.Name, KAR_Opportunity_Created__c, Partition__c, Channel__c, Status_Funnel_Review__c, OwnerId, CaseNumber, '; //JBACU 12/13/18
			string exCseFields = '';
			string childPurchaseFields = '(Select Id, Asset_Type__c, Condition__c From Purchased_Assets__r)'; //JBACU 12/4/18
			if (lrfusetCase.isEmpty()) lrfusetCase = [Select Id, Lookup_Field__c, Lookup_Object__c, Lookup_Field_API_Name__c, Related_Fields__c, Fields_To_Update__c, Fields_For_Query__c From Lookup_Related_Field_Update_Setting__mdt Where isActive__c = true AND Object__c = 'Case'];
			for (Lookup_Related_Field_Update_Setting__mdt lrfuset : lrfusetCase){
				list <string> fieldQ = lrfuset.Fields_For_Query__c.split(',');
				for (string lrfield : fieldQ){
					if (!exCseFields.contains(lrfield)){ //Check if field is not already in Query
						exCseFields+= lrfield + ',';
					}
				}
			}
			//exCseFields = exCseFields.subString(0,exCseFields.Length()-1); //trim last comma (No longer need to trim due to added childPurchaseFields)
			string posexCseQuery = ' From Case Where Id in : exCseList';
			exCseQuery = preexCseQuery + exCseFields + childPurchaseFields + posexCseQuery;
			newCaseListEx = database.Query(exCseQuery); 
		}
		//Populate Related Lookup Fields on Case
		if (TriggerFactory.trigSet.Enable_Case_Lookup_Fields__c){
			RelFieldsMap = CaseGateway.retrieveRelatedFieldLookups(newCaseListEx, lrfusetCase);
			caseAfterUpdateMap = CaseGateway.populateRelatedFieldsFromLookups(RelFieldsMap, lrfusetCase);
		}

		if (trigger.isInsert){
			//Auto Create Application on Case Create
			if (TriggerFactory.trigset.Enable_Case_Auto_Create_Application__c){
				lig790_app2CreateList = CaseGateway.getApp2CreateList(trigger.new);
			}
		}
		else if (trigger.isUpdate){
			Map<Id,Case> oldCseMap = (Map<Id,Case>) trigger.oldMap;

			//Generate Trust Pilot Unique Link in Role
			if (TriggerFactory.trigset.Enable_Case_generate_TrustPilot_Link__c){
				trustPilotRoleMap = CaseGateway.generateTrustPilotLink(TriggerFactory.isProcessedMap);
			}

			if (TriggerFactory.trigSet.Enable_Case_Create_KAR_Sales_Opp__c || TriggerFactory.trigSet.Enable_Case_Create_SR_Manager_Review__c){

				ID KARSalesOppCaseRTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('KAR: Sales Opportunity').getRecordTypeId();
				ID ManagementReviewSuppRequestRTID = Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Management Review').getRecordTypeId();

				for (Case cse : newCaseListEx){
					if(String.valueof(cse.RecordType.Name).contains('Consumer - Asset Finance') || String.valueof(cse.RecordType.Name).contains('Commercial - Asset Finance') ){ //POS or NOD

						//Create KAR: Sales Opportunity when POS/NOD Asset Finance Cases got Submitted to Lender
						if (TriggerFactory.trigSet.Enable_Case_Create_KAR_Sales_Opp__c){
							if (cse.Stage__c != oldCseMap.get(cse.Id).Stage__c && cse.Stage__c == 'Submitted to Lender' && !oldCseMap.get(cse.Id).KAR_Opportunity_Created__c){
								for (Purchased_Asset__c pa : cse.Purchased_Assets__r){ //Currently can only contain 1 Purchase for each Case
									if (pa.Asset_Type__c == 'Motor Vehicle' && pa.Condition__c == 'New'){
										Case KSOCase = new Case(
											Parent_Case__c = cse.Id,
											Partition__c = cse.Partition__c,
											Channel__c = 'Karlon',
											Status = 'New',
											Stage__c = 'Open',
											OwnerId = plsbucketset.KAR_Bucket_Queue_ID__c,
											RecordTypeId = KARSalesOppCaseRTID,
											Primary_Contact_Name_MC__c = cse.Primary_Contact_Name_MC__c,
											Lead_Creation_Method__c = 'Opportunity Automation' //4.1 24/07/2019 RDAVID extra/tasks25429652-PopulateLeadCreationMethod
										);
										caseCreateKARSalesOppList.add(KSOCase);
									}
								}
							}
						}

						//Create Manager Review Support Request when Reviewed POS Asset Finance Cases got Closed Lost
						if (TriggerFactory.trigSet.Enable_Case_Create_SR_Manager_Review__c){
							if(String.valueof(cse.RecordType.Name).startsWith(CommonConstants.PREFIX_P)){ //POS - Consumer/Commercial Asset Finance
								if (cse.Status_Funnel_Review__c){
									if (oldCseMap.get(cse.Id).Status != cse.Status || oldCseMap.get(cse.Id).Stage__c != cse.Stage__c){
										if (cse.Status == 'Closed' && cse.Stage__c == 'Lost'){ 
											if (cse.Channel__c == 'PLS' || cse.Channel__c == 'LFPWBC' || cse.Channel__c == 'DLA'){
												Support_Request__c mrSR = new Support_Request__c(
													Case_Number__c = cse.Id,
													Case_Number_Text__c = cse.CaseNumber,
													Partition__c = cse.Partition__c,
													RecordTypeId = ManagementReviewSuppRequestRTID,
													OwnerId = plsbucketset.SR_Manager_Review_Queue_ID__c,
													Status__c = 'New',
													Stage__c = 'New',
													Support_Sub_Type__c = 'Lost Case Review'
												);
												if (String.valueof(cse.OwnerId).startsWith('005')){ //Case Owner is a User
													mrSr.Case_Owner__c = cse.OwnerId;
												}
												managerReviewSRList.add(mrSR);
											}
										}
									}
								}
							}
						}

					}
				}
			}
			//RDAVID - 27/02/2019 - Teamwork tasks/23413929
			if (TriggerFactory.trigSet.Enable_Case_Log_Sectors__c || TriggerFactory.trigSet.Enable_Case_Remove_Callback__c){
				Set<Id> caseIds = new Set<Id>();
				Set<String> sectorSet = new Set<String>();
				for(Case cs : (List<Case>)trigger.new){
					if (TriggerFactory.trigSet.Enable_Case_Log_Sectors__c){
						if(cs.Status != oldCseMap.get(cs.Id).Status){
							caseIds.add(cs.Id);
							sectorSet.add('Status - '+oldCseMap.get(cs.Id).Status);
						}
						if(cs.Stage__c != oldCseMap.get(cs.Id).Stage__c){
							caseIds.add(cs.Id);
							sectorSet.add('Stage - '+oldCseMap.get(cs.Id).Stage__c);
						}
						if(cs.Attempted_Contact_Status__c != oldCseMap.get(cs.Id).Attempted_Contact_Status__c){
							caseIds.add(cs.Id);
							if(!String.IsBlank(oldCseMap.get(cs.Id).Attempted_Contact_Status__c)){
								sectorSet.add('Attempted Contact Status - '+oldCseMap.get(cs.Id).Attempted_Contact_Status__c);							
							}
						}
						// 7/03/19 RDAVID - extra/SectorAutomation (Teamwork tasks/23721938)
						if(!String.IsBlank(cs.Partition__c)){
							for(String slaStrKey : slaStrKeyMetadataUpdateMap.keySet()){
								// System.debug('>>>>>>>>>>>>>>> UPDATE slaStrKey -> '+slaStrKey);
								List<String> slaStrKeyParts = slaStrKey.split(':');
								if(cs.Partition__c == slaStrKeyParts[1]){
									SLA_Sector__mdt slaSecRecord = slaStrKeyMetadataUpdateMap.get(slaStrKey);
									// System.debug('>>>>>>>>>>>>>>> slaSecRecord.Related_to_Object_RT__c -> '+slaSecRecord.Related_to_Object_RT__c);
									// System.debug('>>>>>>>>>>>>>>> caseRTMapById.get(cs.RecordTypeId).getName() -> '+caseRTMapById.get(cs.RecordTypeId).getName());
									if(slaSecRecord.Related_to_Object_RT__c.containsIgnoreCase(caseRTMapById.get(cs.RecordTypeId).getName())){
										// System.debug('>>>>>>>>>>>>>>> UPDATE Valid RT');
										SObject so = (SObject)cs;
										SObject oldSo = (SObject)oldCseMap.get(cs.Id);

										//If there is a Sector SLA wherein Entry Event is 'On Update'
										if(slaSecRecord.Sector_Entry_Event__c == 'On Update' && !String.IsBlank(slaSecRecord.Sector_Entry_Event_Criteria__c)){
											// System.debug('>>>>>>>>>>>>>>> Create SLA Sector on Update Row 7 in Sheet.');
											List<String> sectorEntryEventList = slaSecRecord.Sector_Entry_Event_Criteria__c.split('\n');
											for(String sectEntEvt : sectorEntryEventList){
												List<String> fieldApiValue = sectEntEvt.split(':');
												// System.debug('>>>>>>>>>> so.get(fieldApiValue[0]) --> '+so.get(fieldApiValue[0]));
												// System.debug('>>>>>>>>>> fieldApiValue[1] --> '+fieldApiValue[1]);
												String metaFieldApi = fieldApiValue[0];
												String metaFieldValue = fieldApiValue[1];											
												String soCurrentFieldVal = (String)so.get(metaFieldApi);
												String soOldFieldVal = (String)oldSo.get(metaFieldApi);

												if(soCurrentFieldVal != soOldFieldVal && !String.IsBlank(soCurrentFieldVal) && metaFieldValue.containsIgnoreCase(soCurrentFieldVal)){
													// System.debug('>>>>>>>>>> Additional Rule was met. ');
													// System.debug('>>>>>>>>>>>>>>> xx -> '+ fieldApiValue[1] + ' match ' + so.get(fieldApiValue[0]));
													String sectorToPut = slaStrKeyParts[0];
													CaseGateway.addValidSector (validSectors, cs, sectorToPut, sectorListToUps);					
												}
											}
										}
										
										//If there is a Sector SLA wherein Exit Event is 'On Update', this logic will update existing Sectors if the Exit Event Criteria is met.
										if(slaSecRecord.Sector_Exit_Event__c == 'On Update' && !String.IsBlank(slaSecRecord.Sector_Exit_Event_Criteria__c)){	
											// System.debug('>>>>>>>>>>>>>>> Update SLA Sector based on Sheet.');
											List<String> sectorExitEventList = slaSecRecord.Sector_Exit_Event_Criteria__c.split('\n');
											// System.debug('>>>>>>>>>>>>>>> Update SLA Sector based on Sheet. sectorExitEventList - '+sectorExitEventList);
											for(String sectExtEvt : sectorExitEventList){
												List<String> fieldApiValue = sectExtEvt.split(':');
												String metaFieldApi = fieldApiValue[0];
												String metaFieldValue = fieldApiValue[1];											
												String soCurrentFieldVal = (String)so.get(metaFieldApi);
												String soOldFieldVal = (String)oldSo.get(metaFieldApi);

												if(soCurrentFieldVal != soOldFieldVal && !String.IsBlank(soCurrentFieldVal) && metaFieldValue.containsIgnoreCase(soCurrentFieldVal)){
													caseIds.add(cs.Id);
													sectorSet.add(slaSecRecord.Sector_Name__c);
												}
											}
										}
									}
								}
							}
						}
					}
					if (TriggerFactory.trigSet.Enable_Case_Remove_Callback__c){ //JBACU 24/12/19 SFBAU-20
						system.debug('@@after cs.PB_Sched_Action__c: '+cs.PB_Sched_Action__c);
						if (cs.Run_PB_Sched_Action_Through_WFR__c && cs.PB_Sched_Action__c == 'Remove Callback - Set Status/Stage'){
							caseIDsStrForRemoveCallbackList.add(cs.Id); 
						}
					}
				}
				if(casewithSectorMap.isEmpty()){
					casewithSectorMap = new Map<Id,Case>([	SELECT Id, (	SELECT Id, Case_Number__c, Sector1__c, Sector_Exit__c
																			FROM Sectors__r 
																			WHERE Case_Number__c IN: caseIds AND Sector1__c IN: sectorSet AND Sector_Exit__c = NULL ORDER BY CreatedDate DESC) 
															FROM Case WHERE Id IN: caseIds]);
					System.debug('bulkAfter validSectors - ' + validSectors.size());
					// System.debug('bulkAfter caseIds size - ' + caseIds.size());
					// System.debug('bulkAfter casewithSectorMap - ' + casewithSectorMap);
				}
			}

			// 5/31/19 - JBACULOD - Attempted SMS Automation
			if (csmsautSet.size () > 0){
				for (Case_SMS_Automation_Setting__mdt csms : csmsautSet){
					if (!aSMSbyPartitionMap.containskey(csms.Partition__c)){ 
						aSMSbyPartitionMap.put(csms.Partition__c, new list <Case_SMS_Automation_Setting__mdt>());
						aSMSbyPartitionMap.get(csms.Partition__c).add(csms);
					}
					else aSMSbyPartitionMap.get(csms.Partition__c).add(csms);
				}
				aSMSbyPartitionMap.get('Positive').addAll(aSMSbyPartitionMap.get('All')); //Add all available SMS Automation for Positive Partition
				aSMSbyPartitionMap.get('Nodifi').addAll(aSMSbyPartitionMap.get('All')); //Add all available SMS Automation for Nodifi Partition
			}

			// 24/12/19 - JBACULOD - SFBAU-20
			if (caseIDsStrForRemoveCallbackList.size() > 0){
				Case_RemoveOnHoldResetStatusStage.removeOnHoldSetStatusStage(caseIDsStrForRemoveCallbackList);
			}
		}
	}

	public void beforeInsert(SObject so){
		Case cs = (Case)so;

		//Update Status/Stage Funnels
		if (TriggerFactory.trigSet.Enable_Case_Status_Stage_Funnels__c){
			CaseGateway.recordStatusStageFunnels(cs,null);
		}
		
		if(cs.Partition__c == 'Positive') { //RDAVID 28/09/18 Populate NVM Fields
			cs.NVMContactWorld__NVMAccountOverride__c = genset.NVM_AccountOverride__c;
			cs.NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedIn__c = 30;
			cs.NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedOut__c = 30;
			cs.NVMContactWorld__RoutePlanIdentifier__c = genset.NVM_RoutePlanIdentifier__c;
		}

		//8/03/2019 RDAVID - 1:17pm: Fix for Process Builder conflict "Case Assignment (NM - Nodifi)" Case Lead Assignment Criteria in Sector Automation
		if(caseRTMapById.get(cs.RecordTypeId).getName().containsIgnoreCase('NOD: Commercial -') || caseRTMapById.get(cs.RecordTypeId).getName().containsIgnoreCase('NOD: Consumer -')){
			if(cs.Stage__c == 'Open' && cs.Partition__c == 'Nodifi' && cs.Connective_Full_App_or_Lead__c == 'Lead'){
				cs.Stage__c = 'Owner Assigned';
			}
		}

		// check urgency and tick Ready For Call to true if callnow = true
		String auState = (String.isBlank(cs.State_Code__c)) ? 'SA' : cs.State_Code__c;
		Datetime readyToDialTime = TimeUtils.setTimezoneBasedReadyToDialTime(auState,null); //changed to set to default (SA)
		
		if(TimeUtils.callnow){
			cs.Ready_For_Call__c = true;
		}
		// proceeds to process builder
	}

	public void beforeUpdate(SObject oldSo, SObject so){
		system.debug('@@beforeUpdate');
		Case cs = (Case)so;
		Case oldCs = (Case)oldSo;
        system.debug('@@beforeUpdate SupportRequestHandler.updatedCaseStageinSR = ' + SupportRequestHandler.updatedCaseStageinSR);
        // tasks/25580598 RDAVID 9/08/2019 - 4.5 26/08/2019 RDAVID - extra/task25582197-NewSupportRequestVFChanges
        if( SupportRequestHandler.updatedCaseStageinSR == false ){
			cs.h_Updated_from_SR__c = false; //this will reset the flag to false for validation purposes
        }
		
		//DD Contact Number should be exact number of Primar Contact Phone (11/23/18 JBACU)
		if (cs.Primary_Contact_Phone_MC__c != cs.DD_Contact_Number__c) cs.DD_Contact_Number__c = cs.Primary_Contact_Phone_MC__c;
		// if(cs.Latest_Mortgage_Team_Comment__c != NULL && cs.Latest_Mortgage_Team_Comment__c.length() > 250) cs.Latest_Mortgage_Team_Comment__c = '';
		//2.0 9/26/18 RDAVID
		if(!CaseGateway.stateBusinessHoursMap.isEmpty() && CaseGateway.stateBusinessHoursMap.containsKey(cs.State_Code__c) && cs.Partition__c == 'Positive'){
			cs.BusinessHoursId = CaseGateway.stateBusinessHoursMap.get(cs.State_Code__c).Id;
			if(TriggerFactory.trigSet.Enable_Case_NVM_Timezone_based_Routing__c){
				if(cs.Stage__c == 'Open' && cs.Lead_Bucket__c != null && cs.Lead_Bucket__c != 'LFP Flow' && oldCs.Lead_Bucket__c != 'LFP Flow' && cs.Channel__c != 'LFPWBC'){ //JBACU 25/06/19 
					if(cs.Sent_To_NVM_Date_Time__c == NULL && cs.Ready_To_Dial_Time__c == NULL){
						//temporary comment this : CaseGateway.processCaseOutsideBusinessHours(cs); //RDAVID 18/09/2019 extra/task25953355-NODFlowV3SFAutomations - Added condition to exclude Channel == LFPWBC
						//&& cs.Owner_Name__c != 'Consumer Asset' //RDAVID 21/11/2019 tasks/26490057 Remove this line to include Consumer Asset in NVM Routing.
					}
				}				
			}
		}

		if (TriggerFactory.trigSet.Enable_Case_Referred_Out_By_Owner__c){ //21/06/19 JBACULOD
			CaseGateway.referredOutByOwner(cs, oldCs, plsbucketset);
		}
		
		//Update Status/Stage Funnels
		if (TriggerFactory.trigSet.Enable_Case_Status_Stage_Funnels__c){
			CaseGateway.recordStatusStageFunnels(cs,oldCs);
		}
		
		if(!PHLOppCasesMap.isEmpty() && PHLOppCasesMap.containsKey(cs.Id) && cs.Status_Funnel__c != NULL && cs.Status_Funnel__c.contains('Review')){
			PHLOppCasesToInsert.put(cs.Id,PHLOppCasesMap.get(cs.Id));
			cs.PHL_Opportunity_Created__c = true;
		}  

		if (TriggerFactory.trigSet.Enable_Case_Set_User_as_Owner_on_Close__c){ //21/06/19 JBACULOD - Added toggle 
			CaseGateway.updateCaseOwnerToCurrentUserWhenClosed(cs,oldCs); //26/04/19 RDAVID - extra/task24368245-SetOwnerWhenCaseNewToClosed - Set Owner to Current User When Updated to Closed
		}

		//check if lead loan amount is changed
		if((cs.Lead_Loan_Amount__c != oldCs.Lead_Loan_Amount__c) && (!oldCs.Re_evaluate_PLS_Case_Assignment__c && cs.Re_evaluate_PLS_Case_Assignment__c)){
			
			// check urgency and tick Ready For Call to true if callnow = true
			String auState = (String.isBlank(cs.State_Code__c)) ? 'SA' : cs.State_Code__c; 
			Datetime readyToDialTime = TimeUtils.setTimezoneBasedReadyToDialTime(auState,null); //changed to set to default (SA)
			System.debug('TimeUtils.callnow >>>> ' + TimeUtils.callNow);

			if(TimeUtils.callnow){
				cs.Ready_For_Call__c = true;
			}
			// proceeds to process builder
		}

	}	

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){
		system.debug('@@afterInsert');
		Case cs = (Case)so;
		if (TriggerFactory.trigSet.Enable_Case_Log_Sectors__c){
			if(cs.Status != NULL) {
				String sectorToPut = 'Status - '+cs.Status;
				CaseGateway.addValidSector (validSectors, cs, sectorToPut, sectorListToUps);
			}

			if(cs.Stage__c != NULL){
				String sectorToPut = 'Stage - '+cs.Stage__c;
				CaseGateway.addValidSector (validSectors, cs, sectorToPut, sectorListToUps);
			}

			// 7/03/19 RDAVID - extra/SectorAutomation (Teamwork tasks/23721938)
			if(!String.IsBlank(cs.Partition__c)){
				// System.debug('>>>>>>>>>> cs.Partition__c --> '+cs.Partition__c);
				// System.debug('>>>>>>>>>> slaStrKeyMetadataInsertMap --> '+slaStrKeyMetadataInsertMap.keySet());
				for(String slaStrKey : slaStrKeyMetadataInsertMap.keySet()){
					List<String> slaStrKeyParts = slaStrKey.split(':');
					if(cs.Partition__c == slaStrKeyParts[1]){
						SLA_Sector__mdt slaSecRecord = slaStrKeyMetadataInsertMap.get(slaStrKey);
						// System.debug('>>>>>>>>>> slaSecRecord - RecordType --> '+slaSecRecord.Related_to_Object_RT__c);
						// System.debug('>>>>>>>>>> case - RecordType --> '+caseRTMapById.get(cs.RecordTypeId).getName());
						if(slaSecRecord.Related_to_Object_RT__c.containsIgnoreCase(caseRTMapById.get(cs.RecordTypeId).getName())){
							if(String.IsBlank(slaSecRecord.Additional_Rules__c)){
								// System.debug('>>>>>>>>>> Additional_Rules__c --> '+slaSecRecord.Additional_Rules__c);
								// System.debug('>>>>>>>>>> Create SLA Sector for this --> '+slaStrKeyParts[0]);
								String sectorToPut = slaStrKeyParts[0];
								CaseGateway.addValidSector (validSectors, cs, sectorToPut, sectorListToUps);								
							}
							else{
								// System.debug('>>>>>>>>>> Additional_Rules__c --> '+slaSecRecord.Additional_Rules__c);
								List<String> addRulesKeyParts = slaSecRecord.Additional_Rules__c.split('\n');
								// System.debug('>>>>>>>>>> addRulesKeyParts --> '+addRulesKeyParts);
								for(String addRule : addRulesKeyParts){
									List<String> fieldApiValue = addRule.split(':');
									// System.debug('>>>>>>>>>> so.get(fieldApiValue[0]) --> '+so.get(fieldApiValue[0]));
									// System.debug('>>>>>>>>>> fieldApiValue[1] --> '+fieldApiValue[1]);
									if(so.get(fieldApiValue[0]) != NULL && fieldApiValue[1].containsIgnoreCase((String)so.get(fieldApiValue[0]))){
										// System.debug('>>>>>>>>>> Additional Rule was met. ');
										String sectorToPut = slaStrKeyParts[0];
										CaseGateway.addValidSector (validSectors, cs, sectorToPut, sectorListToUps);					
									}
								}
							}
						}
					}
				}
			}
		}

		// 3.3 2/05/19 RDAVID - extra/task24399056-InitialReviewSRandSLA
		if (TriggerFactory.trigSet.Enable_Case_Create_Initial_Review_SR__c){
			CaseGateway.createInitialReviewSR(cs,validCaseRTsForInitialReviewSR,nodInitialReviewSRRTId,nodInitialReviewSRList);
		}

        /*Case cs = (Case)so;
		if (TriggerFactory.trigSet.Enable_Case_Lookup_Fields__c){
			if(String.valueof(caseRTMapById.get(cs.RecordTypeId).Name).startsWith(CommonConstants.PREFIX_N) || String.valueof(caseRTMapById.get(cs.RecordTypeId).Name).startsWith(CommonConstants.PREFIX_P) || String.valueof(caseRTMapById.get(cs.RecordTypeId).Name).startsWith(CommonConstants.PREFIX_PWM) || String.valueof(caseRTMapById.get(cs.RecordTypeId).Name).startsWith(CommonConstants.PREFIX_KAR) ){ //PLS - Nodifi - PWM - KAR Case RTs
				caseAfterUpdateMap = CaseGateway.populateRelatedFieldsFromLookups(cs,null, RelFieldsMap, lrfusetCase);
			}
		} */
	}

	public void afterUpdate(SObject oldSo, SObject so){
		system.debug('@@afterUpdate');
		
		Case cs = (Case)so;
		Case oldCs = (Case)oldSo;

		if(casewithSectorMap.containsKey(cs.Id) ){
			CaseGateway.processSectors(casewithSectorMap, cs, oldCS, validSectors, sectorListToUps);
			// System.debug('Im at line 502 sectorListToUps - '+sectorListToUps);
		}


		//Case Automated SMS
		if (aSMSbyPartitionMap.size() > 0){
			if (aSMSbyPartitionMap.containskey(cs.Partition__c)){
				for (Case_SMS_Automation_Setting__mdt csms : aSMSbyPartitionMap.get(cs.Partition__c)){
					boolean createSMSTask = false;
					if ( so.get(csms.IsChanged_Field__c) != oldSo.get(csms.IsChanged_Field__c) &&
						  String.valueof(so.get(csms.IsChanged_Field__c)) == csms.IsChanged_Field_Value__c){ //checked changed Field criteria
							if ( (csms.Case_Status__c != null && csms.Case_Status__c == cs.Status) || String.isBlank(csms.Case_Status__c) ){ //check Case Status criteria
								if ( (csms.Case_Stage__c != null && csms.Case_Stage__c == cs.Stage__c) || String.isBlank(csms.Case_Stage__c) ){ //check Case Stage criteria 
									if ( (csms.Channel__c != null && csms.Channel__c == cs.Channel__c) || String.isBlank(csms.Channel__c) ){ //check Channel criteria
										if ( (csms.Nodifi_Submission_Type__c != null && csms.Nodifi_Submission_Type__c == cs.Connective_Full_App_or_Lead__c) || String.isBlank(csms.Nodifi_Submission_Type__c) ){ //check NODIFI Submission Type
											createSMSTask = true;
										}
									}
								}
							}
					}
					if (createSMSTask){
						//Pass SMS Task Key which will be processed for creating tasks and sending SMS
						String tskKey = cs.Id + ':' + csms.SMS_Action_Key__c + ':' + cs.OwnerId + ':' + cs.LastModifiedById;
						smsTaskKeyList.add(tskKey);
						break;
					}
				}
			}
		}

		//JBACU - 18/07/2019 - Teamwork tasks/25365170
		if (TriggerFactory.trigset.Enable_Case_Delete_CommCT_on_Lost__c){
			if (oldCs.Stage__c != cs.Stage__c && cs.Stage__c == 'Lost' && cs.Status == 'Closed'){
				casesforDelCommCT.add(cs); 
			}
		}
		//4.3 5/08/2019 RDAVID - tasks/25563890
		if(TriggerFactory.trigset.Enable_Case_Update_Commission_LI__c){
			if (oldCs.Stage__c != cs.Stage__c && cs.Stage__c == 'Won' && cs.Status == 'Closed'){
				casesforUpdateCLI.add(cs); 
			}
		}

    }
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){

		if (lig790_app2CreateList.size() > 0 ){ 
			system.debug('@@app2create');
			//Create Application on each Case
			Database.insert(lig790_app2CreateList);
			lig790_case2UpdateList = CaseGateway.getCase2UpdateList(lig790_app2CreateList);
			if (lig790_case2UpdateList.size() > 0){
				system.debug('@@case2Update');
				//Link created Application to each Case
				Database.update(lig790_case2UpdateList);
			}
		}
		if (trustPilotRoleMap.size() > 0){
			Database.update(trustPilotRoleMap.values());
		}

		list <Case> caseUpdatedDueToKARSalesOpp = new list <Case>();
		if (caseCreateKARSalesOppList.size() > 0){
			//Create KAR: Sales Opportunity Cases
			Database.insert(caseCreateKARSalesOppList);
			for (Case cse : [Select Id, Parent_Case__c From Case Where Id in : caseCreateKARSalesOppList]){
				Case upCse = new Case(Id= cse.Parent_Case__c, KAR_Opportunity_Created__c = true );
				caseUpdatedDueToKARSalesOpp.add(upCse);
			}
		}

		if (caseAfterUpdateMap.size() > 0 || caseUpdatedDueToKARSalesOpp.size() > 0){ 
			list <Case> finalUpCselist = mergeData(caseAfterUpdateMap.values(), caseUpdatedDueToKARSalesOpp);
			// System.debug('**********UPDATE -> finalUpCselist '+finalUpCselist);
			Database.update(finalUpCselist);
		}

		if (managerReviewSRList.size() > 0 ){
			//Create Management Review Support Requests
			Database.insert(managerReviewSRList);
		}
		if(PHLOppCasesToInsert.size() > 0){
			//23/01/2018 RDAVID: Logic to create POS: PHL Opportunity Cases
			Database.insert(PHLOppCasesToInsert.values());
		}
		if(PHLOppCasesToUpdate.size() > 0){
			Database.update(PHLOppCasesToUpdate.values());
		}

		System.debug('sectorListToUps *********** ' +sectorListToUps.size());
		System.debug('sectorListToUps ***********2 ' +sectorListToUps);
		if(sectorListToUps.size() > 0){
			Database.upsert(sectorListToUps);
		}
		// 3.3 2/05/19 RDAVID - extra/task24399056-InitialReviewSRandSLA - Insert NOD Initial Review SR, Process Builder will assign these SRs to NOD Initial Review Queue
		// System.debug('***** nodInitialReviewSRList -----> '+nodInitialReviewSRList);
		if(nodInitialReviewSRList.size() > 0){
			Database.insert(nodInitialReviewSRList);
		}
		// 5/30/19 JBACULOD - Case SMS Automation
		// system.debug('@@smsTaskKeyList: '+smsTaskKeyList);
		if (smsTaskKeyList.size() > 0){
			SMSServices.SendSMS(smsTaskKeyList);
		}

		// 7/18/19 JBACULOD - Delete CommissionCT on Closed Lost
		if (casesforDelCommCT.size() > 0){
			CommissionCTGateway commCTgate = new CommissionCTGateway(casesforDelCommCT);
			ID jobID = System.enqueueJob(commCTgate);
		}

		if (casesforUpdateCLI.size() > 0){
			CommissionCTGateway commCTgate = new CommissionCTGateway(casesforUpdateCLI);
			ID jobID = System.enqueueJob(commCTgate);
		}
	}

	public static SObject[] mergeData(SObject[] list1, SObject[] list2) { //Use only if trigger is having more than 1 DML on same object
        Map<Id, SObject> results = new Map<Id, SObject>();
        for(SObject[] recordList: new List<SObject[]> { list2, list1 }) {
            for(SObject record: recordList) {
                results.put(record.Id, record.getSObjectType().newSobject(record.Id));
            }
        } 
        for(SObject[] recordList: new List<SObject[]> { list2, list1 }) {
            for(Sobject record: recordList) {
                Map<String, Object> values = record.getPopulatedFieldsAsMap();
                SObject temp = results.get(record.Id);
                for(String field: values.keySet()) {
                    if (!field.contains('__r') && field != 'RecordType' ){ //Disregard related fields
                        temp.put(field, values.get(field));
                    }
                }
            }
        } 
        SObject[] finalResults = list1.clone();
        finalResults.clear();
        finalResults.addAll(results.values());
        return finalResults;
    }

}
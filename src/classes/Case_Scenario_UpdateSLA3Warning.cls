/*
	@Description: class for calculating SLA Age of With Lender (SLA-3) on Case
	@Author: Jesfer Baculod/Rex David - Positive Group
	@Modified: Rex David - Moved from Scenario object to Case to work for the new model.
	@History:
		- 03/23/2018 - Created
		- 17/04/2019 - RDAVID - branch:extra/task24148707-SectorSLAFixBusinessHours - Updated, Commented code to remove the SLA Scenario fields reference to proceed with deletion.
		- 15/05/2019 - RDAVID - extra/task24564956-OLDSLACleanUp - Comment this class
*/
public class Case_Scenario_UpdateSLA3Warning {
	private static General_Settings1__c genSet = General_Settings1__c.getInstance(UserInfo.getUserId());
	@InvocableMethod(label='Case - SLA Next Warning Time - With Lender' description='updates next Warning Time of an SLA whenever an SLA is not yet completed')
	public static void scenarioSLAWarningTimeWL (list <ID> scenIDs) {
		/*RDAVID - extra/task24564956-OLDSLACleanUp -
		//Retrieve default Business Hours
		BusinessHours bh = [select Id from BusinessHours where IsDefault=true]; 

		//Retrieve Scenarios to update
		list <Case> caseScenList = new List <Case>();

		// list <Case> caseScenList = [Select Id, 
		// 							SLA_Scenario_Time_3_Start__c, SLA_Scenario_Warning_Time_3__c, SLA_Scenario_Started_Count_3__c, SLA_Scenario_Completed_Count_3__c, SLA_Scenario_Active_3__c, SLA_Scenario_Time_3_mm__c
		// 							From Case Where Id in : scenIds];
        
        // Datetime warningdate; 

		// for (Case scen : caseScenList){
		// 	warningdate = scen.SLA_Scenario_Warning_Time_3__c;
		// 	scen.SLA_Scenario_Warning_Time_3__c = null;
		// 	scen.SLA_Scenario_Active_3__c = null;
		// }

		// update caseScenList; //force update to retrigger SLA Warning

		// system.debug('@@warningdate:'+warningdate);

		// for (Case scen : caseScenList){

        //     scen.SLA_Scenario_Active_3__c = 'Yes';
            
        //     if (scen.SLA_Scenario_Started_Count_3__c != scen.SLA_Scenario_Completed_Count_3__c){ 
        //         scen.SLA_Scenario_Warning_Time_3__c = BusinessHours.add(bh.Id, warningdate, Integer.valueOf(genSet.Case_Scenario_SLA_3_Interval__c)); //Set succeeding warning of current SLA (add 30 minutes)
        //         DateTime warningtimeAR = scen.SLA_Scenario_Warning_Time_3__c;
		// 		scen.SLA_Scenario_Warning_Time_3__c = Datetime.newInstance(warningtimeAR.year(), warningtimeAR.month(), warningtimeAR.day(), warningtimeAR.hour(), warningtimeAR.minute(), 0);
        //         scen.SLA_Scenario_Time_3_mm__c = Decimal.valueof(( BusinessHours.diff(bh.Id, scen.SLA_Scenario_Time_3_Start__c, warningdate ) / 1000) / 60 );  //returns SLA Age in minutes
        //         if (scen.SLA_Scenario_Time_3_mm__c == 119) scen.SLA_Scenario_Time_3_mm__c = 120; //workaround for business hours difference of SLA
		// 		if (scen.SLA_Scenario_Time_3_mm__c == 149) scen.SLA_Scenario_Time_3_mm__c = 150; //workaround for business hours difference of SLA
		// 		if (scen.SLA_Scenario_Time_3_mm__c == 179) scen.SLA_Scenario_Time_3_mm__c = 180; //workaround for business hours difference of SLA
		// 		if (scen.SLA_Scenario_Time_3_mm__c == 209) scen.SLA_Scenario_Time_3_mm__c = 210; //workaround for business hours difference of SLA
		// 		if (scen.SLA_Scenario_Time_3_mm__c == 239) scen.SLA_Scenario_Time_3_mm__c = 240; //workaround for business hours difference of SLA
        //         system.debug('@@slaWarningB');
        //     }
		// }
		// update caseScenList;
		*/
	}
}
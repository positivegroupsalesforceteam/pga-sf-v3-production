global class OpportunityProcessor extends DefaultProcessor {
    global override boolean process() {
        if(!super.process()) {
            return false;
        }
        List<LKRecord> opptyList = XMLMappingAdapter.lkRecordsMap.get(getNodeName());
        if(opptyList!=null && opptyList.size()>0) {
            String brokerName = (String)opptyList[0].lkValuesMap.get('Assigned_To__c');
            if(LKIntegration.mapNameToId.containsKey(brokerName)){
                LKIntegration.brokerId = LKIntegration.mapNameToId.get(brokerName).broker_id__c;
            }
        }
        return true;
    }
}
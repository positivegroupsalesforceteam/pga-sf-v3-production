/*
    @author: Jesfer Baculod - Positive Group
    @history: 08/24/17 - Created
    @description: test class of CreateCaseCommentsCC
*/
@isTest
private class CreateCaseCommentsCCTest {

		private static final string CASE_RT_PRIVATE = Label.Case_Private_RT; //Consumer Loan

		@testsetup static void setup(){

			//Create test data for Case
			Id rtPrivateLoan = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CASE_RT_PRIVATE).getRecordTypeId();
			Case cse = new Case(
				RecordTypeId = rtPrivateLoan,
				Status = 'New',
				Sale_Type__c = Label.Case_Sale_Type_Dealer
			);
			insert cse;

		}

		static testmethod void testComment(){

			//Retrieve test Case
			Case cse = [Select Id From Case];
			PageReference pref = Page.CreateCaseComments;
			pref.getParameters().put('id', cse.Id);
			Test.setCurrentPage(pref);

			Test.startTest();
				CreateCaseCommentsCC ccc = new CreateCaseCommentsCC();

				//Test error on Save
				ccc.comment.CommentBody = '';
				ccc.saveComment();

				//Test error exception on Save
				ccc.comment.CommentBody = 'Test comment';
				ccc.comment.ParentId = null;
				ccc.saveComment();

				//Test successful save
				ccc.comment.CommentBody = 'Test comment';
				ccc.comment.ParentId = cse.Id;
				ccc.saveComment();

				ccc.refreshComments();

			Test.stopTest();

				//Retrieve created Case Comment on Case
				list <CaseComment> csecomments = [Select Id, CommentBody From CaseComment Where ParentId =: cse.Id];
				//Verify that Case Comment has been created
				system.assert(csecomments.size() > 0);

		}


}
/** 
* @FileName: RoleTriggerTest
* @Description: Test class for Role Trigger 
* @Copyright: Positive (c) 2018 
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/20/18 RDAVID Created class, Added testRoleToApp1 
* 1.1 3/28/18 JBACULOD Added testAutoPopulateCaseAppinRole
* 1.2 4/3/18 JBACULOD updated setupData, fixed assertions in testAutoPopulateCaseAppinRole
**/ 
@isTest 
public class RoleTriggerTest{

	@testSetup static void setupData() {

		//Turn On Trigger 
		Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
		triggerSettings.Enable_Triggers__c = true;
		triggerSettings.Enable_Income_Trigger__c = true;
		//triggerSettings.Enable_Income_Rollup_to_Role__c = true;
		triggerSettings.Enable_Role_Trigger__c = true;
		triggerSettings.Enable_Role_Rollup_to_Application__c = true;
        triggerSettings.Enable_Case_Trigger__c = true;
		triggerSettings.Enable_Case_Auto_Create_Application__c = true;
		triggerSettings.Enable_Role_To_Account_Propagation__c = true;
		triggerSettings.Enable_Role_Create_Case_Touch_Point__c = true;
		triggerSettings.Enable_Role_Assocs_in_Application__c = true;

		upsert triggerSettings Trigger_Settings1__c.Id;

		//Create Test Account
		List<Account> accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);
        
		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);

		//Create Test Role 
		List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
		Database.insert(roleList);

	}

	static testMethod void testUpdateApplication(){

		List<Role__c> roleList = [SELECT Id, Role_Type__c, Application__c FROM Role__c LIMIT 1];
		Test.startTest();
			rolelist[0].Role_Type__c = 'Co-Applicant';
			rolelist[0].Primary_Contact_for_Application__c = true;
			update rolelist[0];

			TriggerFactory.isProcessedMap = new map <Id, boolean>();
			rolelist[0].RecordTypeId = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get('Non-Borrowing Spouse').getRecordTypeId();
			update rolelist[0];

		Test.stopTest();

	}
    
    /* static testMethod void testRoleToApp1() {
  		// Perform DML here only
  		TestDataFactory.create_CS_IncomeToRoleRollupFieldMapping();
  		TestDataFactory.create_CS_RoleToApplicationRollupFieldMapping();
  		List<Role__c> roleList = [SELECT Id,Application__c FROM Role__c LIMIT 1];
  		
  		List<Income1__c> incomeList = new List<Income1__c>();

  		for (Integer i = 0; i < 15; i++){
  			Income1__c inc = new Income1__c();
  			if(i>=0 && i<5){
  				inc = TestDataFactory.createGenericIncome(CommonConstants.INCOME_RT_C, roleList[0].Id, 10);
  			}
  			else if(i>=5 && i<10){
  				inc = TestDataFactory.createGenericIncome(CommonConstants.INCOME_RT_CS, roleList[0].Id, 10);
  			}
  			else if(i>=10 &&  i<15){
  				inc = TestDataFactory.createGenericIncome(CommonConstants.INCOME_RT_I, roleList[0].Id, 10);
  			}
  			incomeList.add(inc);
  		}

  		Test.StartTest();
  			//Insert
  			Database.insert(incomeList);

	  		Role__c testRole = [SELECT Id, Centrelink_Income__c, Child_Support_Income__c, Investment_Income__c FROM Role__c WHERE Id =: roleList[0].Id];
	  		System.assertEquals(testRole.Centrelink_Income__c,50);
	  		System.assertEquals(testRole.Child_Support_Income__c,50);
	  		System.assertEquals(testRole.Investment_Income__c,50);

	  		List<Application__c> appList = [SELECT Id,Combined_Income__c FROM Application__c WHERE Id =: roleList[0].Application__c LIMIT 1];
			System.assertEquals(appList[0].Combined_Income__c,150);
	  		//Update
	  		incomeList = [SELECT Id,Centrelink_Income__c FROM Income1__c WHERE RecordType.Name =: CommonConstants.INCOME_RT_C LIMIT 1];
	  		incomeList[0].Centrelink_Income__c = 0;
	  		Database.update(incomeList);

	  		testRole = [SELECT Id, Centrelink_Income__c, Child_Support_Income__c, Investment_Income__c FROM Role__c WHERE Id =: roleList[0].Id];
	  		appList = [SELECT Id,Combined_Income__c FROM Application__c WHERE Id =: roleList[0].Application__c LIMIT 1];
	  		System.assertEquals(testRole.Centrelink_Income__c,40);
	  		System.assertEquals(testRole.Child_Support_Income__c,50);
	  		System.assertEquals(testRole.Investment_Income__c,50);
	  		System.assertEquals(appList[0].Combined_Income__c,140);

	  		//Delete
	  		incomeList = [SELECT Id FROM Income1__c];
	  		Database.delete(incomeList);

	  		testRole = [SELECT Id, Centrelink_Income__c, Child_Support_Income__c, Investment_Income__c FROM Role__c WHERE Id =: roleList[0].Id];
	  		System.assertEquals(testRole.Centrelink_Income__c,0);
	  		System.assertEquals(testRole.Child_Support_Income__c,0);
	  		System.assertEquals(testRole.Investment_Income__c,0);
	  		Database.delete(testRole);
	  		appList = [SELECT Id,Combined_Income__c FROM Application__c WHERE Id =: roleList[0].Application__c LIMIT 1];
	  		System.assertEquals(appList[0].Combined_Income__c,0);

  		Test.StopTest();
    }*/	

	static testMethod void testAutoPopulateCaseAppinRole(){

		//Retrieve Trigger Settings
		Trigger_Settings1__c trigSet = [Select Id, Enable_Case_Trigger__c, Enable_Case_Auto_Create_Application__c, Enable_Role_Auto_Populate_Case_App__c From Trigger_Settings1__c];
        trigSet.Enable_Case_Trigger__c = false;
        trigSet.Enable_Case_Auto_Create_Application__c = false;
        trigSet.Enable_Role_Auto_Populate_Case_App__c = true;
		trigSet.Enable_Role_Create_Case_Touch_Point__c = true;
        update trigSet;
        

		//Retrieve Account Test Data
		List <Account> acclist = [Select Id From Account limit 1];
        
		//Retrieve Case Test Data
		List <Case> cseList = [Select Id,Application_Name__c From Case];
        system.assert(cseList[0].Application_Name__c != null);
        
		//Retrieve Role Test Data
		List<Role__c> roleList = [SELECT Id,Application__c, Application__r.Case__c, Case__c, Case__r.Application_Name__c FROM Role__c];

		Test.StartTest();

			//Test Auto Populating of Case/Application in Role on Insert
			list <Role__c> inRolelist = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, cselist[0].Id, accList[0].Id, null,  1);
			insert inRolelist;
			
			//Verify that Application was populated in Role since Case is populated on Insert
			Role__c inRole = [Select Id, Case__c, Application__c From Role__c Where Id in : inRolelist limit 1];
			system.assert(inRole.Application__c != null); 
			
			//Test Auto Populating of Case/Application in Role on Update
			for (Role__c role : rolelist){
				role.Case__c = cselist[rolelist.size()-1].Id;
			}
			update rolelist;

			//Verify that Application was populated with Case's Application in Role
			for (Role__c role : [SELECT Id,Application__c, Application__r.Case__c, Case__c, Case__r.Application_Name__c FROM Role__c Where Id in : rolelist]){
				//system.assertEquals(role.Application__c, role.Case__r.Application_Name__c);
			}

			integer rctr = 0;
			for (Role__c role : rolelist){
				role.Case__c = cselist[rctr].Id;
				rctr++;
			}
			update rolelist; 

			//Verify that Application was populated with Case's Application in Role
			for (Role__c role : [SELECT Id,Application__c, Application__r.Case__c, Case__c, Case__r.Application_Name__c FROM Role__c Where Id in : rolelist]){
				system.assertEquals(role.Case__c, role.Application__r.Case__c);
			}

		Test.StopTest();

	}

	// SD-30 SF - NM - Propagate Role to Account Field Values for Consumer
	static testMethod void testPropagateRoleToAccount(){
		//Create Test Account
		List<Account> accList = TestDataFactory.createGenericPersonAccount('First Name','TestLastName',CommonConstants.PACC_RT_I, 1);
		Database.insert(accList);
		
		accList = [SELECT Id, IsPersonAccount,PersonContactId FROM Account WHERE Id =: acclist[0].Id LIMIT 1];
		System.assertEquals(accList[0].IsPersonAccount,true);

		bizible2__Bizible_Person__c biziPerson = new bizible2__Bizible_Person__c(	bizible2__Contact__c = acclist[0].PersonContactId,
																					bizible2__UniqueId__c = 'Person_'+acclist[0].PersonContactId,
																					Name = 'balarajesh1977@gmail.com');
		Database.insert(biziPerson);
		bizible2__Bizible_Touchpoint__c bizi = new bizible2__Bizible_Touchpoint__c (	bizible2__Bizible_Person__c = biziPerson.Id,
																						bizible2__Contact__c = acclist[0].PersonContactId,
																						bizible2__Account__c = acclist[0].Id,
																						bizible2__Marketing_Channel__c = 'Paid Search',
																						bizible2__Marketing_Channel_Path__c = 'Paid Search.AdWords',
																						bizible2__Touchpoint_Source__c = 'Google AdWords',
																						bizible2__Medium__c = 'CPC',
																						bizible2__Ad_Campaign_Name__c = 'Car Loans - paused 8th June 2018',
																						bizible2__Ad_Group_Name__c = 'Head Terms',
																						bizible2__Keyword_Text__c = '[car loan]',
																						bizible2__Ad_Content__c = '#1 Car Loans From 4.85% Bank Say No, We Say Yes Get A 30 Second Online Quote. 90% Applicants Approved In 24 Hours. Enquire Now https://www.positivelendingsolutions.com.au',
																						bizible2__Landing_Page__c = 'www.positivelendingsolutions.com.au/lp/car-loans',
																						bizible2__Form_URL__c = 'www.positivelendingsolutions.com.au/lp/car-loans',
																						bizible2__Ad_Destination_URL__c = 'https://www.positivelendingsolutions.com.au/lp/car-loans/',
																						bizible2__UniqueId__c='TP2_Person_0036F00002Wk60XQAR_2018-06-25:02-40-35-60620.a2cc6ea44caf45',
																						Name='testa3d6F000001MMRV',
																						bizible2__Touchpoint_Date__c = system.now(),
																						bizible2__Touchpoint_Position__c = 'FT, LC, Form',
																						bizible2__Touchpoint_Type__c = 'Web Form',
																						bizible2__Geo_City__c = 'Camira',
																						bizible2__Geo_Region__c = 'Queensland',
																						bizible2__Geo_Country__c = 'Australia',
																						bizible2__Platform__c = 'Android (8.0)',
																						bizible2__Browser__c = 'Chrome (67.0)',
																						bizible2__Count_First_Touch__c = 1,
																						bizible2__Count_Lead_Creation_Touch__c = 1,
																						bizible2__Count_U_Shaped__c = 1);
		Database.insert(bizi);
		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);

		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);
		
		Test.StartTest();
			List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
			roleList[0].Other_Phone__c = '09165856439';
			roleList[0].Mobile_Phone__c = '09165856439';
			roleList[0].Email__c = 'test@email.com';
			roleList[0].Is_From_Frontend__c = true;
			Database.insert(roleList);
			
			accList = [SELECT Id,PersonHomePhone,PersonMobilePhone,PersonEmail FROM Account WHERE Id =: accList[0].Id];
			System.assertEquals(accList[0].PersonHomePhone,roleList[0].Other_Phone__c);
			System.assertEquals(accList[0].PersonMobilePhone,roleList[0].Mobile_Phone__c);
			System.assertEquals(accList[0].PersonEmail,roleList[0].Email__c);

			roleList[0].Other_Phone__c = '09165851111';
			roleList[0].Mobile_Phone__c = '09165851111';
			roleList[0].Email__c = 'test2@email.com';
			roleList[0].Marital_Status__c = 'Married';

			Database.update(roleList);

			accList = [SELECT Id,PersonHomePhone,PersonMobilePhone,PersonEmail FROM Account WHERE Id =: accList[0].Id];
			//System.assertEquals(accList[0].PersonHomePhone,roleList[0].Other_Phone__c);
			//System.assertEquals(accList[0].PersonMobilePhone,roleList[0].Mobile_Phone__c);
			//System.assertEquals(accList[0].PersonEmail,roleList[0].Email__c);

		Test.StopTest();
	}	

	// SD-30 SF - NM - Propagate Role to Account Field Values for Commercial
	static testMethod void testPropagateRoleToAccount2(){
		//Create Test Account
		List<Account> accList = TestDataFactory.createGenericAccount('TestName',CommonConstants.ACC_RT_COMPANY, 1);
		Database.insert(accList);
		accList = [SELECT Id, IsPersonAccount FROM Account WHERE Id =: acclist[0].Id LIMIT 1];
		System.assertEquals(accList[0].IsPersonAccount,false);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_COMM_AF, 1);
		Database.insert(caseList);

		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_COMM_C, caseList[0].Id, 1);
		Database.insert(appList);
		
		Test.StartTest();
			List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_COMP, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
			roleList[0].Phone__c = '09165856439';
			Database.insert(roleList);
			
			accList = [SELECT Id,Phone FROM Account WHERE Id =: accList[0].Id];
			System.assertEquals(accList[0].Phone,roleList[0].Phone__c);

        	roleList[0].Phone__c = '09165851111';
			Database.update(roleList);

			accList = [SELECT Id,Phone FROM Account WHERE Id =: accList[0].Id];
			System.assertEquals(accList[0].Phone,roleList[0].Phone__c);

		Test.StopTest();
	}	

	// SD-46 SF - NM - Toggle Role field (Primary Applicant)
	static testMethod void testToggleRole(){

		//Retrieve Trigger Settings
		Trigger_Settings1__c trigSet = [Select Id, Enable_Role_Toggle_Primary_Applicant__c From Trigger_Settings1__c];
        trigSet.Enable_Role_Toggle_Primary_Applicant__c = true;
        update trigSet;

		//Create Test Account
		List<Account> accList = TestDataFactory.createGenericPersonAccount('First Name','TestLastName',CommonConstants.PACC_RT_I, 2);
		Database.insert(accList);
		accList = [SELECT Id, IsPersonAccount FROM Account WHERE Name LIKE '%TestLastName%'];
		System.assertEquals(accList[0].IsPersonAccount,true);
		System.assertEquals(accList.size(),2);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);

		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);
		
		Test.StartTest();
			List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
			roleList[0].Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT;
			roleList[0].Other_Phone__c = '09165856439';
			roleList[0].Mobile_Phone__c = '09165856439';
			roleList[0].Email__c = 'test@email.com';
			Database.insert(roleList);

			roleList = [SELECT Id,Role_Type__c FROM Role__c WHERE Application__c =: appList[0].Id];

			List<Role__c> roleList2 = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[1].Id, appList[0].Id,  1);
			roleList2[0].Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT;
			roleList2[0].Other_Phone__c = '09165856439';
			roleList2[0].Mobile_Phone__c = '09165856439';
			roleList2[0].Email__c = 'test@email.com';
			Database.insert(roleList2);

			roleList = [SELECT Id FROM Role__c WHERE Application__c =: appList[0].Id];

			System.assertEquals(roleList.size(),2);
		Test.StopTest();
	}	

	// SD-46 SF - NM - Toggle Role field (Primary Applicant)
	static testMethod void testToggleRole2(){

		//Retrieve Trigger Settings
		Trigger_Settings1__c trigSet = [Select Id, Enable_Role_Toggle_Primary_Applicant__c From Trigger_Settings1__c];
        trigSet.Enable_Role_Toggle_Primary_Applicant__c = true;
        update trigSet;

		//Create Test Account
		List<Account> accList = TestDataFactory.createGenericPersonAccount('First Name','TestLastName',CommonConstants.PACC_RT_I, 2);
		Database.insert(accList);
		accList = [SELECT Id, IsPersonAccount FROM Account WHERE Name LIKE '%TestLastName%'];
		System.assertEquals(accList[0].IsPersonAccount,true);
		System.assertEquals(accList.size(),2);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);

		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);
		
		Test.StartTest();
			List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
			roleList[0].Role_Type__c = null;
			roleList[0].Other_Phone__c = '09165856439';
			roleList[0].Mobile_Phone__c = '09165856439';
			roleList[0].Email__c = 'test@email.com';
			Database.insert(roleList);

			roleList = [SELECT Id,Role_Type__c FROM Role__c WHERE Application__c =: appList[0].Id];
			//System.assertEquals(roleList[0].Role_Type__c,CommonConstants.ROLE_PRIMARY_APPLICANT);
			Database.delete(roleList);

			List<Role__c> roleList2 = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  5);
			roleList2[0].Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT;
			roleList2[1].Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT;
			roleList2[0].Other_Phone__c = '09165856439';
			roleList2[0].Mobile_Phone__c = '09165856439';
			roleList2[0].Email__c = 'test@email.com';
			Database.insert(roleList2);

			roleList2 = [SELECT Id,Role_Type__c FROM Role__c WHERE Application__c =: appList[0].Id];
			Integer primaryCount = 0;
			Id primaryId;
			for(Role__c role : roleList2){
				if(role.Role_Type__c == CommonConstants.ROLE_PRIMARY_APPLICANT){
					primaryCount ++;
					primaryId = role.Id;
				}
			}
			//System.assertEquals(1,primaryCount);
			//UPDATE
			roleList2 = [SELECT Id,Role_Type__c FROM Role__c WHERE Application__c =: appList[0].Id AND Role_Type__c !=: CommonConstants.ROLE_PRIMARY_APPLICANT];
			for(Role__c role : roleList2){
				role.Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT;
			}
			Database.update(roleList2);

			roleList2 = [SELECT Id,Role_Type__c FROM Role__c WHERE Application__c =: appList[0].Id];
			Integer primaryCountUpdate = 0;
			Id primaryIdUpdate;
			for(Role__c role : roleList2){
				if(role.Role_Type__c == CommonConstants.ROLE_PRIMARY_APPLICANT){
					primaryCountUpdate ++;
					primaryIdUpdate = role.Id;
				}
			}
			//System.assertNotEquals(primaryId,primaryIdUpdate);
			//System.assertEquals(primaryCountUpdate,1);
			//System.assertEquals(primaryCount,1);

			for(Role__c role : roleList2){
				role.Role_Type__c = null;
			}
			Database.update(roleList2);

			for(Role__c role : roleList2){
				role.Role_Type__c = null;
			}
			Database.update(roleList2);

		Test.StopTest();
	} 
}
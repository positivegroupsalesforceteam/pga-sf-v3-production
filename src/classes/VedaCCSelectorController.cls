/** 
* @FileName: VedaCCSelectorController
* @Description: controller class of VedaCCSelector
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 5/16/18 JBACULOD Created
* 1.1 5/17/18 JBACULOD Fixed minor issues
**/ 
public class VedaCCSelectorController {

    private string sObjectType {get;set;}
    public string accId {get;set;}
    public string roleId {get;set;}
    public boolean isInConsole {get;set;}
    public boolean hasErrors {get;set;}
    public list <selectOption> Approles {get;set;}
    public list <Role__c> rolelist {get;set;}
    private string approleQuery {get;set;}
    private ID curID {get;set;}

    public VedaCCSelectorController() {

        hasErrors = isInConsole = false;
        rolelist = new list <Role__c>();
        approleQuery = 'Select Id, Case__c, Application__c, Account__c, Account__r.Name, Role_Type__c, RecordTypeId, RecordType.Name From Role__c ';
        curID = Apexpages.currentpage().getparameters().get('id');
        sObjectType = String.valueOf(curID.getSObjectType());
        if (sObjectType == 'Case'){ //Veda Credit Check was accessed from Case
            approleQuery+= ' Where Case__c = : curID AND Account__r.IsPersonAccount = TRUE AND h_Is_Veda_Credit_Checked__c = FALSE';
        }
        else if (sObjectType == 'Application__c'){ //Veda Credit Check was accessed from Application
            approleQuery+= ' Where Application__c = : curID AND Account__r.IsPersonAccount = TRUE AND h_Is_Veda_Credit_Checked__c = FALSE';
        }
        rolelist = database.Query(approleQuery);
        if (rolelist.isEmpty()){ //Case/Application has no Roles
            hasErrors = true;

        }
        else if (rolelist.size() == 1){ //Only 1 Role in Application
            roleId = rolelist[0].Id;
            //redirection to main Veda page will be handled by javascript
        }
        else { //Application has multiple Roles, user need to select which Person to perform Veda Credit Check
            AppRoles = setAppRoles();
        }

    }

    //Retrieves all Person Accounts of each Role in Case/Application
    public list <selectOption> setAppRoles(){
        list <selectOption> options = new list <selectOption>();
        for (Role__c role : rolelist){
            string person = role.Account__r.Name;
            if (role.Role_Type__c != null ) person+= ' - ' + role.Role_Type__c;
            // options.add(new SelectOption(role.Account__c, person)); //RDAVID 25/07/2018
            options.add(new SelectOption(role.Id, person)); 
        }
        return options;
    }

    //Redirect to main Veda Credit Check page
    public Pagereference redirectToVedaCC(){
        if (!hasErrors){ 
            Pagereference pref = Page.VedaIndividualPageNM;
            pref.getParameters().put('scontrolCaching','1');
            pref.getParameters().put('id',roleId);
            if (isInConsole) pref.getParameters().put('isdtp','vw');
            pref.setRedirect(true);
            return pref;
        }
        else return null;
    }

    //Redirect back to Case / Application ID (used if viewing on a non-console App)
    public Pagereference pageCancel(){
        Pagereference pref = new Pagereference('/' + curID);
        pref.setRedirect(true);
        return pref;
    }

}
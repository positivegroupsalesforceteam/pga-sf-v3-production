/*
    @Description: controller class of Box_PartnerAutoCreateCC THIS WILL FIRE WHEN USER CLICK THE BOX ICON IN THE PARTNER RECORD
    @Author: Rexie David - Positive Group
    @History:
        -   2/04/19 - RDAVID - extra/tasks24059615-PartnerObjectBox
        -   28/05/19 - JBACULOD - removed reference of Channel field
        -   3/09/19 - RDAVID - Added logic to support other Partner RTs Box Folder Creation.
*/

public class Box_PartnerAutoCreateCC {
        public Id curID {get;set;} //current Object ID
        public string objType {get;set;}
        public boolean noBoxAccess {get;set;}
        private static box.Toolkit boxToolkit = new box.Toolkit();
        //public boolean BoxSuccess {get;set;}
        public Partner__c part {get;set;}
        public string test_objRecordFolderID {get;set;}
        private list <Box_Partition_Settings__mdt> boxpset {get;set;}
    
    
    public Box_PartnerAutoCreateCC(ApexPages.StandardController con) {
        curID = con.getId();
        system.debug('@@curID:'+curID);
        noBoxAccess = false;
        //Retrieve Box Partition Settings
        boxpset = new list <Box_Partition_Settings__mdt>();
        boxpset = [Select Id, Partition__c, Recordtype_Name__c, Object__c, Box_Folder_ID__c, Sub_Folders__c, MasterLabel From Box_Partition_Settings__mdt WHERE Object__c = 'Partner'];

        if (curID != null){ 
            if (curID.getSobjectType() == Partner__c.SobjectType){ //ID is from Partner__c
                //Retrieve Partner__c
                part = [Select Id, Name, Box_Folder_ID__c, RecordType.Name, Partition__c From Partner__c Where Id = : curID]; //JBACU 28/05/19
            }
        }
    }

    public pageReference autocreateBoxFolder(){

        if (TriggerFactory.trigset.Enable_Partner_Box_Auto_Create__c){
            // Instantiate the Toolkit object
        
            String objRecordFolderID;
            if (part.Box_Folder_ID__c == null){
                if (boxToolkit.getFolderIdByRecordId(curID) == null){
                    //No created Box Folder found on current record        
                    system.debug('part.Id == '+part.Id);
                    objRecordFolderID = boxToolkit.createFolderForRecordId(part.Id, null, true); //Create Box folder
                    system.debug('most recent error: ' + boxToolkit.mostRecentError);
                   
                }
                else {
                    system.debug('ELSE part.Id == '+part.Id);
                    objRecordFolderID = boxToolkit.getFolderIdByRecordId(part.Id); //Retrieve existing Box Folder
                }
            }
            else objRecordFolderID = part.Box_Folder_ID__c;
            system.debug('@@objRecordFolderID: '+objRecordFolderID);
            if (Test.isRunningTest()) objRecordFolderID = test_objRecordFolderID;
            system.debug('@@new item folder id: ' + objRecordFolderID);
            if(objRecordFolderID == null){
                system.debug('most recent error: ' + boxToolkit.mostRecentError);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,boxToolkit.mostRecentError));
                return null;
            }
            else{
                for (Box_Partition_Settings__mdt bpset : boxpset){
                    system.debug('@@bpSet partition:'+bpSet.Partition__c);

                    if (bpset.RecordType_Name__c.containsIgnoreCase(part.RecordType.Name)){

                        system.debug('@@bpSet partition Name :'+bpSet.MasterLabel);

                        if (Test.isRunningTest()) objRecordFolderID = test_objRecordFolderID;
                        
                        system.debug('@@objRecordFolderId:'+objRecordFolderId);
                        system.debug('most recent error: ' + boxToolkit.mostRecentError);

                        boolean moveFolder = boxToolkit.moveFolder(objRecordFolderID, bpset.Box_Folder_ID__c,'');
                        system.debug('@@moveFolder:'+moveFolder);
                        
                        if(!String.isBlank(bpset.Sub_Folders__c)){
                            string sfolder = bpset.Sub_Folders__c;
                            set <string> subfolders = new set <string>();
                            boolean breakfolder = false;
                            do {
                                string mtype = sfolder.substringBetween('[',']');
                                if (mtype == null) breakfolder = true;
                                if (mtype != null) {
                                    subfolders.add(mtype);
                                    sfolder = sfolder.remove('['+mtype+']');
                                }
                            }
                            while (!breakfolder);
                            for (string strfolderName : subfolders){
                                if(strfolderName.containsIgnoreCase('>')){
                                    String[] arrTest = strfolderName.split('>'); 
                                    String childfolder = boxToolkit.createFolder(arrTest[0], objRecordFolderID, ''); 
                                    String subchildfolder = boxToolkit.createFolder(arrTest[1], childfolder, ''); 
                                    
                                }
                                else{
                                    String childfolder = boxToolkit.createFolder(strfolderName, objRecordFolderID, ''); 
                                }
                            }
                        }
                        break;
                    }
                }

                if (part.Box_Folder_ID__c == null){ //Store ID of Box Folder created for Partner__c
                    part.Box_Folder_ID__c = objRecordFolderID;
                    update part;
                }
                // ALWAYS call this method when finished.Since salesforce doesn't allow http callouts after dml operations, we need to commit the pending database inserts/updates or we will lose the associations created
                boxToolkit.commitChanges();
                //Redirect to object's BoxSection VF
                PageReference pref = new PageReference('/apex/box_PartnerBoxSection?id='+curID);
                pref.setRedirect(true);
                return pref; 
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Box Folder creation currently disabled. Please contact your system administrator'));
            return null;
        }
    }
}
/*
	@Description: class for calculating SLA Age of Docs Out
	@Author: Jesfer Baculod - Positive Group
	@History:
		- 10/25/2017 - Created
		- 15/05/2019 - RDAVID - extra/task24564956-OLDSLACleanUp - Comment this class
*/
public class Case_UpdatesSLAWarningDocsOut {
    
    @InvocableMethod(label='SLA Next Warning Time - Docs Out' description='updates next Warning Time of an SLA whenever an SLA is not yet completed')
	public static void caseSLAWarningTimeDO (list <ID> cseIDs) {
		/*RDAVID - extra/task24564956-OLDSLACleanUp -
		//Retrieve default Business Hours
		BusinessHours bh = [select Id from BusinessHours where IsDefault=true]; 

		//Retrieve Cases to update
		list <Case> cselist = [Select Id, 
								SLA_Time_Docs_Out_Start__c, SLA_Warning_Time_Docs_Out__c, SLA_Started_Count_Docs_Out__c, SLA_Completed_Count_Docs_Out__c, SLA_Active_Docs_Out__c, SLA_Time_Docs_Out_mm__c
								From Case Where Id in : cseIDs];
        
        Datetime warningdate; 
		for (Case cse : cselist){
			warningdate = cse.SLA_Warning_Time_Docs_Out__c;
			cse.SLA_Warning_Time_Docs_Out__c = null;
			cse.SLA_Active_Docs_Out__c = null;
		}
		update cselist; //force update to retrigger SLA Warning
		system.debug('@@warningdate:'+warningdate);

		for (Case cse : cselist){
            cse.SLA_Active_Docs_Out__c = 'Yes';
            if (cse.SLA_Started_Count_Docs_Out__c != cse.SLA_Completed_Count_Docs_Out__c){
                cse.SLA_Warning_Time_Docs_Out__c = BusinessHours.add(bh.Id, warningdate, 900000); //Set succeeding warning of current SLA (add 15 minutes)
                DateTime warningtimeDO = cse.SLA_Warning_Time_Docs_Out__c;
				cse.SLA_Warning_Time_Docs_Out__c = Datetime.newInstance(warningtimeDO.year(), warningtimeDO.month(), warningtimeDO.day(), warningtimeDO.hour(), warningtimeDO.minute(), 0);
                cse.SLA_Time_Docs_Out_mm__c = Decimal.valueof(( BusinessHours.diff(bh.Id, cse.SLA_Time_Docs_Out_Start__c, warningdate ) / 1000) / 60 );  //returns SLA Age in minutes
                if (cse.SLA_Time_Docs_Out_mm__c == 104) cse.SLA_Time_Docs_Out_mm__c = 105; //workaround for business hours difference of SLA
				if (cse.SLA_Time_Docs_Out_mm__c == 119) cse.SLA_Time_Docs_Out_mm__c = 120; //workaround for business hours difference of SLA
                system.debug('@@slaWarningC');
            }
		}

		update cselist;*/

	}

}
/*
    @Description: test class of CaseTrigger, CaseTriggerHandler, Case_UpdateSLAWarningTime
    @Author: Sri Babu, Jesfer Baculod - Positive Group
    @History:
        11/2/17 - Updated test class due to TrustPilot unique links and restructured Opportunity trigger
        12/15/2017 - Modified by Rex David - Positive Group - LIG-726
*/
@isTest
Public class OpportunityTriggerTest{

    private static final string ACC_RT_INDIVIDUAL = Label.Contact_Individual_RT; //Individual
    private static string PROFILE_SYSADMIN = Label.Profile_Name_System_Administrator; //System Administrator
    private static final string OPP_STAGE_APPROVED = Label.Opportunity_Stage_Approved; //Approved

    @testsetup static void setup(){

        //create test data for Sys Admin
        ID pId_sysadmin = [Select Id, Name From Profile Where Name = : PROFILE_SYSADMIN ].Id;
        User sysadminUsr = new User(
                UserName = 'sysadmin@casetriggertest.com.uat',
                LastName = 'TestSysAdminCASE',
                Email = 'sysadmin@casetriggertest.com',
                EmailEncodingKey = 'UTF-8',
                TimeZoneSidKey = 'GMT',
                Alias = 'tSACSE',
                LocaleSidKey = 'en_AU',
                LanguageLocaleKey = 'en_US',
                ProfileId =  pId_sysadmin
            );
        insert sysadminUsr;

        System.runAs(sysAdminUsr){

            //Create test data for Lender Flag for Emails custom setting
            list <Lender_Flag_for_Emails__c> lenflagCSet = new list <Lender_Flag_for_Emails__c>();
            lenflagCSet.add(new Lender_Flag_for_Emails__c(Name = 'Afs'));
            lenflagCSet.add(new Lender_Flag_for_Emails__c(Name = 'Ammf'));
            lenflagCSet.add(new Lender_Flag_for_Emails__c(Name = 'ANZ,'));
            lenflagCSet.add(new Lender_Flag_for_Emails__c(Name = 'BOQ'));
            lenflagCSet.add(new Lender_Flag_for_Emails__c(Name = 'Capital,'));
            lenflagCSet.add(new Lender_Flag_for_Emails__c(Name = 'Centre 1'));
            insert lenflagCSet;

            //create test data for Account
            Id rtBAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get(ACC_RT_INDIVIDUAL).getRecordTypeId();
            Account acc = new Account(
                    LastName = 'test',
                    RecordTypeId = rtBAcc,
                    Residential_Status__c = 'Rent',
                    Previous_Employment_Status__c = 'Full-time'
                );
            insert acc;

            Contact con = new Contact(
                    LastName = 'Test Contact',
                    Email = 'test@test.com'
                );
            insert con;

        }

    }

    public static TestMethod void testupdateVariousFlagsAndCalculations(){

        Account acc = [Select Id, Name, Residential_Status__c From Account];

        map <String, Lender_Flag_for_Emails__c> lenflag = Lender_Flag_for_Emails__c.getAll(); 

        Trigger_Settings__c trigSet = new Trigger_Settings__c(
                Enable_Opportunity_Trigger__c = true,
                Enable_Opp_updateStageFlags__c = true,
                Enable_Opp_createContactforcreatedOpp__c = true,
                Enable_Opp_updateVariousFlagsAndCalc__c = true,
                Enable_Opp_setOppSettledonCommissions__c = true
            );
        insert trigset;

        Test.startTest();

            list <Opportunity> opplist = new list <Opportunity>();
            for (Integer i = 0; i < 15; i++){
                Opportunity opp = new Opportunity(
                        name = 'Test'+i, AccountId = acc.Id, stageName = 'open', closeDate = date.today()
                    );
                if (i== 0){ opp.loan_type2__c = 'car';
                    opp.Contact__c = null;
                }
                else if (i== 1) opp.loan_type2__c = 'Bike';
                else if (i == 2) opp.loan_type2__c = 'Truck';
                else if (i == 3) opp.loan_type2__c = 'Personal';
                else if (i == 4) opp.loan_type2__c = 'Boat / Marine';
                else if (i == 5) opp.loan_type2__c = 'Commercial Equipment';
                else if (i == 6) opp.loan_type2__c = 'Trailer';
                else if (i == 7) opp.loan_type2__c = 'Caravan';
                else if (i == 8) opp.loan_type2__c = 'Camper Trailer';
                else if (i == 9) opp.loan_type2__c = 'Horse Float';
                else if (i == 10) opp.loan_type2__c = 'Jet Ski';
                else if (i == 11) opp.loan_type2__c = 'Other';
                else if (i == 12) opp.loan_type2__c = 'Construction Equipment';
                else if (i == 13){ opp.Living_Expenses_Customer__c = 1000;
                        opp.Living_Expenses__c = 1000;
                        opp.Dependant_Expenses__c = 1000;
                    }
                opplist.add(opp);
            }
            insert opplist;

            list <VS_Referral__c> vsrlist = new list <VS_Referral__c>();
            for (Integer i = 0; i < 15; i++){
                VS_Referral__c vsref = new VS_Referral__c(
                        Opportunity__c = opplist[i].Id
                    );
                vsrlist.add(vsref);
            }
            insert vsrlist;

            for (Opportunity opp : opplist){
                if (String.valueof(opp.Name).contains('Test0')) opp.loan_type2__c = 'car loan';
                else if (String.valueof(opp.Name).contains('Test1')){
                        opp.loan_type2__c = 'Bike Loan';
                        opp.Lender__c = 'FirstMac';
                    }
                else if (String.valueof(opp.Name).contains('Test2')){ 
                    opp.loan_type2__c = 'Truck Loan'; 
                    opp.Lender__c = 'ANZ';
                }
                else if (String.valueof(opp.Name).contains('Test3')){ 
                    opp.loan_type2__c = 'Personal Loan';
                    opp.Mobile__c = '123456789';
                    opp.StageName = 'Qualified';
                }
                else if (String.valueof(opp.Name).contains('Test4')){ 
                    opp.loan_type2__c = 'Boat Loan';
                }
                else if (String.valueof(opp.Name).contains('Test5')){ 
                    opp.loan_type2__c = 'Equipment Loan';
                }
                else if (String.valueof(opp.Name).contains('Test6')) opp.loan_type2__c = 'Trailer Loan';
                else if (String.valueof(opp.Name).contains('Test7')) opp.loan_type2__c = 'Caravan Loan';
                else if (String.valueof(opp.Name).contains('Test8')) opp.loan_type2__c = 'Camper Trailer Loan';
                else if (String.valueof(opp.Name).contains('Test9')) opp.loan_type2__c = '-';
                else if (String.valueof(opp.Name).contains('Test10')) opp.loan_type2__c = 'Jet Ski Loan';
                else if (String.valueof(opp.Name).contains('Test11')) opp.loan_type2__c = '-';
                else if (String.valueof(opp.Name).contains('Test12')) opp.loan_type2__c = 'Equipment Loan';
                else if (String.valueof(opp.Name).contains('Test13')) opp.Expenses_Considered__c = opp.Lender_and_Dependant_Expenses__c;
            }
            update opplist;

        Test.stopTest();
        
    }

    static testmethod void testgenerateTrustPilotLink(){

        Account acc = [Select Id, Name, Residential_Status__c From Account];
        Contact con = [Select Id, Name, Email From Contact Where Name =: acc.Name];

        Opportunity opp = new Opportunity(
                Name = 'Test Opp',
                CloseDate = date.today() + 30,
                AccountId = acc.Id,
                StageName = 'Qualified',
                Contact__c = con.Id,
                Lender__c = 'ANZ'
            );
        insert opp;

        opp = [Select Id, Name, Contact__c, Contact__r.Email From Opportunity Where Id = : opp.Id];

        Trigger_Settings__c trigSet = new Trigger_Settings__c(
            Enable_Opportunity_Trigger__c = true,
            Enable_Opp_generateTrustPilotLink__c = true
            );
        insert trigset;

        Test.startTest();

            opp.StageName = OPP_STAGE_APPROVED;
            opp.Approval_Conditions__c = 'test';
            opp.Loan_Reference__c = 'test';
            opp.Vehicle_Source__c = 'Bike/Boat/Personal';
            update opp;

            //Verify that TrustPilot Unique Link has been updated
            Opportunity upOpp = [Select Id, TrustPilot_Unique_Link__c From Opportunity Where Id = : opp.Id];
            //system.assert(upOpp.TrustPilot_Unique_Link__c != null);


        Test.stopTest();


    }
    //LIG-726
    static testmethod void testupdateNVMRelatedFieldsInOpp(){

        Trigger_Settings__c trigSet = new Trigger_Settings__c(  Enable_Opportunity_Trigger__c = true,
                                                                Enable_Opp_generateTrustPilotLink__c = true,
                                                                Enable_NVM_Field_Updates__c = true);

        insert trigset;

        Account acc = [Select Id, Name, Residential_Status__c From Account];
        Contact con = [Select Id, Name, Email From Contact Where Name =: acc.Name];

        Opportunity opp = new Opportunity(
                Name = 'Test Opp',
                CloseDate = date.today() + 30,
                AccountId = acc.Id,
                StageName = 'Qualified',
                Contact__c = con.Id,
                Lender__c = 'ANZ'
            );
        insert opp;

        Pending_Action__c pendingAction = TestHelper.createPendingAction(opp.Id, 'Opportunity', 'updateNVMRelatedFields');
        insert pendingAction;

        Id taskContactRT = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Contact').getRecordTypeId();
        
        Task task = TestHelper.createTask(con.Id,opp.Id,taskContactRT);
        insert task;

        opp = [Select Id, Name, Contact__c, Contact__r.Email From Opportunity Where Id = : opp.Id];

        Test.startTest();

            opp.StageName = OPP_STAGE_APPROVED;
            opp.Approval_Conditions__c = 'test';
            opp.Loan_Reference__c = 'test';
            opp.Vehicle_Source__c = 'Bike/Boat/Personal';
            update opp;

            Opportunity upOpp = [Select Id, TrustPilot_Unique_Link__c, Calls_Made__c, Last_Call_Time_Ended__c From Opportunity Where Id = : opp.Id];
            System.assertNotEquals(upOpp.Calls_Made__c,0);
            System.assertNotEquals(upOpp.Last_Call_Time_Ended__c,null);
        Test.stopTest();
    }

    /*
    public static testMethod void sendEmailtoContact(){
    
    Referral_Company__c ref = new Referral_Company__c();
    ref.Name = 'Test';
    
    insert ref;
    
    Opportunity opp = new Opportunity();
    opp.Name = 'Test';
    opp.stageName = 'open'; 
    opp.closeDate = date.today();
    opp.Referral_Company__c = ref.Id;
    opp.StageName = 'Approved';
    opp.Lender__c = 'ANZ';
    opp.Loan_Reference__c = '123';
    opp.Vehicle_Source__c = 'New Car';
    opp.NPS_Score__c = 7;
    opp.Approval_Conditions__c = 'Test';
    opp.Stage_Flag__c = '0';
    opp.Stage_Flag2__c = '1';
    opp.Increment_StageName__c = 1;
   
    insert opp;
    opp.Approved_Count__c = 1;
    opp.Lender__c = '1';
    opp.NPS_Score_Flag__c = 1;
    opp.Stage_Flag__c = '1';
    opp.Stage_Flag2__c = '0';
    update opp;
    
    contact con = new contact();
    con.LastName = 'Test';
    con.FirstName = 'Test';
    con.Email = 'test@test.com';
    con.deal_Updates__c = true;
    con.Referral_Company__c = con.Id;
    
    insert con;
    con.deal_Updates__c = false;
    update con;
    
    EmailMessage[] newEmail = new EmailMessage[0];
    newEmail.add(new EmailMessage(FromAddress = 'test@abc.org', Incoming = True, ToAddress= 'hello@test.com', Subject = 'Test email', TextBody = 'Test')); 

    insert newEmail;
    
    } */
   
}
/** 
* @FileName: SkillsBasedRoutingTest
* @Description: Test Class for SkillBasedRouting class
* @Copyright: Positive (c) 2019 
* @author: Rexie Aaron David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 7/05/19 RDAVID-extra/task24399056-InitialReviewSRandSLA - Test Class for SkillBasedRouting class
**/ 
@isTest
private class SkillsBasedRoutingTest {
    public static List<Case> caseList = new List<Case>();
    public static List<Sector__c> sectorList = new List<Sector__c>();

    @testSetup static void setupData() {
        //Turn On Trigger 
        Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
        triggerSettings.Enable_Account_Assocs_in_Role__c = true;
        triggerSettings.Enable_Account_Sync_Name_to_Role__c = true;
        triggerSettings.Enable_Account_Trigger__c = true; 
        triggerSettings.Enable_Address_Populate_Location__c = true;
        triggerSettings.Enable_Address_Trigger__c = true;
        triggerSettings.Enable_Application_Primary_Applicants__c = true;
        triggerSettings.Enable_Application_Sync_App_2_Case__c = true;
        triggerSettings.Enable_Application_Trigger__c = true;
        triggerSettings.Enable_Asset_Trigger__c = true;
        triggerSettings.Enable_Attachment_Create_File_in_Box__c = true;
        triggerSettings.Enable_Attachment_Trigger__c = true;
        triggerSettings.Enable_Case_Auto_Create_Application__c = true;
        triggerSettings.Enable_Case_generate_TrustPilot_Link__c = true;
        triggerSettings.Enable_Case_Lookup_Fields__c = true;
        triggerSettings.Enable_Case_Record_Stage_Timestamps__c = true;
        triggerSettings.Enable_Case_Scenario_Trigger__c = true;
        triggerSettings.Enable_Case_SCN_recordStageTimestamps__c = true;
        triggerSettings.Enable_Case_Status_Stage_Funnels__c = true;
        triggerSettings.Enable_Case_Trigger__c = true;
        triggerSettings.Enable_CommissionCT_GetCaseLender__c = true;
        triggerSettings.Enable_CommissionCT_Trigger__c = true;
        triggerSettings.Enable_CommissionLine_GetCommission__c = true;
        triggerSettings.Enable_Commission_Line_Trigger__c = true;
        triggerSettings.Enable_Expense_Trigger__c = true;
        triggerSettings.Enable_FO_Auto_Create_FO_Share__c = true;
        triggerSettings.Enable_FO_Delete_FO_Share_on_FO_Delete__c = true;
        triggerSettings.Enable_FOShare_Rollup_to_Role__c = true;
        triggerSettings.Enable_FOShare_Trigger__c = true;
        triggerSettings.Enable_Front_End_Submission_Trigger__c = true;
        triggerSettings.Enable_Frontend_Sub_SetNVMContact__c = true;
        triggerSettings.Enable_Income_Check_Employment__c = true;
        triggerSettings.Enable_Income_Populate_Sole_Trader__c = true;
        triggerSettings.Enable_Income_Role_Employment_Status__c = true;
        triggerSettings.Enable_Income_Trigger__c = true;
        triggerSettings.Enable_Lender_Automation_Call_Lender__c = true;
        triggerSettings.Enable_Liability_Trigger__c = true;
        triggerSettings.Enable_Relationship_Assocs_in_Account__c = true;
        triggerSettings.Enable_Relationship_Spouse_Duplicate__c = true;
        triggerSettings.Enable_Relationship_Trigger__c = true;
        triggerSettings.Enable_Role_Address_Check_Residency__c = true;
        triggerSettings.Enable_Role_Address_Populate_Address__c = true;
        triggerSettings.Enable_Role_Address_Toggle_Current__c = true;
        triggerSettings.Enable_Role_Address_Trigger__c = true;
        triggerSettings.Enable_Role_Assocs_in_Application__c = true;
        triggerSettings.Enable_Role_Auto_Populate_Case_App__c = true;
        triggerSettings.Enable_Role_Create_Case_Touch_Point__c = true;
        triggerSettings.Enable_Role_Rollup_to_Application__c = true;
        triggerSettings.Enable_Role_To_Account_Propagation__c = true;
        triggerSettings.Enable_Role_Trigger__c = true;
        triggerSettings.Enable_Task_Trigger__c = true;
        triggerSettings.Enable_Task_Update_Case_Attempt__c = true;
        triggerSettings.Enable_Triggers__c = true;
        triggerSettings.Enable_Support_Request_Trigger__c = true;
        triggerSettings.Enable_AgentWork_Trigger__c = true;
        triggerSettings.Enable_Case_Log_Sectors__c = true;
        triggerSettings.Enable_Sector_Trigger__c = true;

        upsert triggerSettings Trigger_Settings1__c.Id;
        
        General_Settings1__c genset = General_Settings1__c.getOrgDefaults();
        genset.Support_Request_NOD_Initial_Review_RT__c = CommonConstants.SR_NOD_INIT_REV;
        upsert genset General_Settings1__c.Id;
    }

    static testmethod void testCommercialSkillBasedRoutingClass(){
        // SkillsBasedRoutingrouteUsingSkills(List<String> supportRequests)
        Test.startTest();
            //Create Test Case
            caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_COMM_AF, 1);
            caselist[0].Partition__c = 'Nodifi';
            caselist[0].Channel__c = 'BOLT';
            caselist[0].Connective_Full_App_or_Lead__c = 'Full Application';
            Database.insert(caseList);
            List<Id> caseIds = new List<Id>();
            caseIds.add(caseList[0].Id);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            SectorSetAlertRecipient.setSLAWarningRecipient(caseIds);

            caseList = [SELECT Id, Status, Stage__c FROM Case WHERE Recordtype.Name =: CommonConstants.CASE_RT_N_COMM_AF AND Status = 'New' AND Stage__c = 'Open'];
            System.assertEquals(caseList.size(),1);
            
            sectorList = [SELECT Id FROM Sector__c WHERE Case_Number__c =: caseList[0].Id];
            System.assertEquals(sectorList.size(),3);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            caseList[0].Stage__c = 'Owner Assigned';
            Database.update(caseList);
            sectorList = [SELECT Id FROM Sector__c WHERE Case_Number__c =: caseList[0].Id];
            System.assertEquals(sectorList.size(),4);

            Support_Request__c srRecord = new Support_Request__c(RecordtypeId = Schema.getGlobalDescribe().get('Support_Request__c').getDescribe().getRecordTypeInfosByName().get('Asset - Submission Request (New)').getRecordTypeId(),
                                                                Status__c = 'New',
                                                                Stage__c = 'New',
                                                                Partition__c = 'Nodifi',
                                                                Support_Sub_Type__c = 'Initial - New Submission',
                                                                I_confirm_the_SF_Record_is_accurate__c = TRUE,
                                                                Case_Number__c = caseList[0].Id);

            Database.insert(srRecord);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            List<String> supportRequests = new List<String>();
            supportRequests.add(srRecord.Id);
            SkillsBasedRouting.routeUsingSkills(supportRequests);
        Test.stopTest();
    }

    static testmethod void testConsumerSkillBasedRoutingClass(){
        // SkillsBasedRoutingrouteUsingSkills(List<String> supportRequests)
        Test.startTest();
            //Create Test Case
            caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
            caselist[0].Partition__c = 'Nodifi';
            caselist[0].Channel__c = 'BOLT';
            caselist[0].Connective_Full_App_or_Lead__c = 'Full Application';
            Database.insert(caseList);
            List<Id> caseIds = new List<Id>();
            caseIds.add(caseList[0].Id);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            SectorSetAlertRecipient.setSLAWarningRecipient(caseIds);

            caseList = [SELECT Id, Status, Stage__c FROM Case WHERE Recordtype.Name =: CommonConstants.CASE_RT_N_CONS_AF AND Status = 'New' AND Stage__c = 'Open'];
            System.assertEquals(caseList.size(),1);
            
            sectorList = [SELECT Id FROM Sector__c WHERE Case_Number__c =: caseList[0].Id];
            System.assertEquals(sectorList.size(),3);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            caseList[0].Stage__c = 'Owner Assigned';
            Database.update(caseList);
            sectorList = [SELECT Id FROM Sector__c WHERE Case_Number__c =: caseList[0].Id];
            System.assertEquals(sectorList.size(),4);

            Support_Request__c srRecord = new Support_Request__c(RecordtypeId = Schema.getGlobalDescribe().get('Support_Request__c').getDescribe().getRecordTypeInfosByName().get('Asset - Submission Request (New)').getRecordTypeId(),
                                                                Status__c = 'New',
                                                                Stage__c = 'New',
                                                                Partition__c = 'Nodifi',
                                                                Support_Sub_Type__c = 'Initial - New Submission',
                                                                I_confirm_the_SF_Record_is_accurate__c = TRUE,
                                                                Case_Number__c = caseList[0].Id);

            Database.insert(srRecord);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            List<String> supportRequests = new List<String>();
            supportRequests.add(srRecord.Id);
            SkillsBasedRouting.routeUsingSkills(supportRequests);
        Test.stopTest();
    }
}
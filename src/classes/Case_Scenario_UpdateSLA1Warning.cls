/*
	@Description: class for calculating SLA Age of Scenario Actioned (SLA-1) on Case
	@Author: Jesfer Baculod/Rex David - Positive Group
	@Modified: Rex David - Moved from Scenario object to Case to work for the new model.
	@History:
		- 03/23/2018 - Created
		- 15/05/2019 - RDAVID - extra/task24564956-OLDSLACleanUp - Comment this class
*/
public class Case_Scenario_UpdateSLA1Warning {
	private static General_Settings1__c genSet = General_Settings1__c.getInstance(UserInfo.getUserId());
	@InvocableMethod(label='Case - SLA Next Warning Time - Scenario Actioned' description='updates next Warning Time of an SLA whenever an SLA is not yet completed')
	public static void scenarioSLAWarningTimeSA (list <ID> caseScenIDs) {
		/*RDAVID - extra/task24564956-OLDSLACleanUp -
		//Retrieve default Business Hours
		BusinessHours bh = [select Id from BusinessHours where IsDefault=true]; 

		//Retrieve Scenarios to update
		list <Case> caseScenList = [Select Id, 
									SLA_Scenario_Time_1_Start__c, SLA_Scenario_Warning_Time_1__c, SLA_Scenario_Started_Count_1__c, SLA_Scenario_Completed_Count_1__c, SLA_Scenario_Active_1__c, SLA_Scenario_Time_1_mm__c
									From Case Where Id in : caseScenIDs];
        
        Datetime warningdate; 

		for (Case scen : caseScenList){
			warningdate = scen.SLA_Scenario_Warning_Time_1__c;
			scen.SLA_Scenario_Warning_Time_1__c = null;
			scen.SLA_Scenario_Active_1__c = null;
		}

		update caseScenList; //force update workaround to retrigger SLA Warning

		system.debug('@@warningdate:'+warningdate);

		for (Case scen : caseScenList){

            scen.SLA_Scenario_Active_1__c = 'Yes';

            if (scen.SLA_Scenario_Started_Count_1__c != scen.SLA_Scenario_Completed_Count_1__c){ 
                
                scen.SLA_Scenario_Warning_Time_1__c = BusinessHours.add(bh.Id, warningdate, Integer.valueOf(genSet.Case_Scenario_SLA_1_Interval__c)); //Set succeeding warning of current SLA (add 15 minutes)
                DateTime warningtimeAR = scen.SLA_Scenario_Warning_Time_1__c;
				scen.SLA_Scenario_Warning_Time_1__c = Datetime.newInstance(warningtimeAR.year(), warningtimeAR.month(), warningtimeAR.day(), warningtimeAR.hour(), warningtimeAR.minute(), 0);
                scen.SLA_Scenario_Time_1_mm__c = Decimal.valueof(( BusinessHours.diff(bh.Id, scen.SLA_Scenario_Time_1_Start__c, warningdate ) / 1000) / 60 );  //returns SLA Age in minutes
                
                if (scen.SLA_Scenario_Time_1_mm__c == 89) scen.SLA_Scenario_Time_1_mm__c = 90; //workaround for business hours difference of SLA
				if (scen.SLA_Scenario_Time_1_mm__c == 104) scen.SLA_Scenario_Time_1_mm__c = 105; //workaround for business hours difference of SLA
				if (scen.SLA_Scenario_Time_1_mm__c == 119) scen.SLA_Scenario_Time_1_mm__c = 120; //workaround for business hours difference of SLA

                system.debug('@@slaWarningB');
            }
		}

		update caseScenList;*/
	}
}
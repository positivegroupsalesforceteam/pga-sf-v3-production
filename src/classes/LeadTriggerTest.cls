/*
    @Description: test class of LeadTrigger, LeadTriggerHandler
    @Author: Jesfer Baculod - Positive Group
    @History:
        11/29/17 - Created
        12/15/2017 - Modified by Rex David - Positive Group - LIG-726
*/
@isTest
private class LeadTriggerTest {

	private static string LEAD_RT_INDIVIDUAL = Label.Lead_Individual_RT; //Individual
	
	@testsetup static void setup(){

		list <Lead> ldlist = new list <Lead>();
		Id rtIndividual = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(LEAD_RT_INDIVIDUAL).getRecordTypeId();
		for (Integer i = 0; i < 25; i++){
			Lead ld = new Lead(
				RecordTypeId = rtIndividual,
				LastName = 'Test Lead',
				Status = 'Open',
				Dependant_Expenses__c = 1000,
				Living_Expenses_currency__c = 1000
			);
			ldlist.add(ld);
		}
		insert ldlist;

		list <Applicant_2__c> app2list = new list <Applicant_2__c>();
		list <Assets__c> assetlist = new list <Assets__c>();
		list <Employee__c> emplist = new list <Employee__c>();
		list <Liability__c> lialist = new list <Liability__c>();
		list <Reference__c> reflist = new list <Reference__c>();
		list <Form_Submissions__c> fslist = new list <Form_Submissions__c>();
		for (Integer i = 0; i < ldlist.size(); i++){
			Applicant_2__c app2 = new Applicant_2__c(
					Name = 'Test Appicant2',
					Lead__c = ldlist[i].Id
				);
			app2list.add(app2);
			Assets__c ass = new Assets__c(
					Lead__c = ldlist[i].Id
				);
			assetlist.add(ass);
			Liability__c lia = new Liability__c(
					Lead__c = ldlist[i].Id
				);
			lialist.add(lia);
			Employee__c emp = new Employee__c(
					Name = 'Test Employer',
					Lead__c = ldlist[i].Id
				);
			emplist.add(emp);
			Reference__c ref = new Reference__c(
					Lead__c = ldlist[i].Id
				);
			reflist.add(ref);
			Form_Submissions__c fs = new Form_Submissions__c(
					Lead__c = ldlist[i].Id
				);
			fslist.add(fs);
		}
		insert app2list;
		insert assetlist;
		insert lialist;
		insert emplist;
		insert reflist;
		insert fslist;

		Pending_Action__c pendingAction = TestHelper.createPendingAction(ldlist[0].Id, 'Lead', 'updateNVMRelatedFields');
		insert pendingAction;

	}

	/*static testmethod void testupdateCreatedDateFieldsLivingExpCalc(){

		list <Lead> ldlist = [Select Id, CreatedTime__c, CreatedTime24__c, Lender_and_Dependant_Expenses__c, isConverted, ConvertedOpportunityId, ConvertedAccountId From Lead];

		Trigger_Settings__c trigset = new Trigger_Settings__c(
				Enable_Lead_Trigger__c = true,
				Enable_Lead_updateCreatedDateFields__c = true
			);
		insert trigset;

		Test.startTest();

			integer i = 0;
			for (Lead ld : ldlist){
				if (i < 12) ld.Living_Expenses_Customer__c = 1000;
				else ld.Living_Expenses_Customer__c = 10000;
				i++;
			}
			update ldlist;

		Test.stopTest();

	} */

	static testmethod void testupdateRelatedObjsOnLeadConvert(){

		list <Lead> ldlist = [Select Id, CreatedTime__c, CreatedTime24__c, isConverted, ConvertedOpportunityId, ConvertedAccountId From Lead];
		list <Applicant_2__c> app2list = [Select Id, Lead__c From Applicant_2__c];
		list <Assets__c> assetlist = [Select Id, Lead__c From Assets__c];
		list <Employee__c> emplist = [Select Id, Lead__c From Employee__c];
		list <Liability__c> lialist = [Select Id, Lead__c From Liability__c];
		list <Reference__c> reflist = [Select Id, Lead__c From Reference__c];
		list <Form_Submissions__c> fslist = [Select Id, Lead__c From Form_Submissions__c];

		Trigger_Settings__c trigset = new Trigger_Settings__c(
			Enable_Lead_Trigger__c = true,
			Enable_Lead_updateRelObjsOnLeadConvert__c = true
		);
		insert trigset;
		

		Test.startTest();

			Database.LeadConvert lc = new database.LeadConvert();
			lc.setLeadId(ldlist[0].Id);
			lc.setDoNotCreateOpportunity(false);
			lc.setConvertedStatus('Qualified');
			Database.LeadConvertResult lcr = Database.convertLead(lc);
			System.assert(lcr.isSuccess());

		Test.stopTest();

	}
	//LIG-726
	static testmethod void testUpdateNVMRelatedFieldsInLead(){
		List<Pending_Action__c> pendingActionList = [SELECT Id, Related_Record_Id__c FROM Pending_Action__c];
		System.assertNotEquals(pendingActionList.size(),0);
		
		List<Lead> leadList = [SELECT Id FROM Lead WHERE Id =: pendingActionList[0].Related_Record_Id__c];	
		System.assertNotEquals(leadList.size(),0);

		Id taskContactRT = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Contact').getRecordTypeId();
		
		Task task = TestHelper.createTask(leadList[0].Id,null,taskContactRT);
		System.assertNotEquals(task,new Task());

		insert task;

		Trigger_Settings__c triggerSettings = Trigger_Settings__c.getOrgDefaults();
		triggerSettings.Enable_NVM_Field_Updates__c = true;
		triggerSettings.Enable_Lead_Trigger__c = true;
		upsert triggerSettings Trigger_Settings__c.Id;

		Test.startTest();

			leadList[0].Guid__c = 'Test';

			update leadList[0];

			Lead updatedLead = [SELECT Id, Calls_Made__c, Last_Call_Time_Ended__c FROM Lead WHERE Id =: leadList[0].Id];

			System.assertEquals(updatedLead.Calls_Made__c,1);
			System.assertNotEquals(updatedLead.Last_Call_Time_Ended__c,null);

		Test.stopTest();
	}


}
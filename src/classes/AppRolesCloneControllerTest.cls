/** 
* @FileName: AppRolesCloneControllerTest
* @Description: Test class for RoleCloneController
* @Copyright: Positive (c) 2019
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 5/01/19 JBACULOD Created class, added testinitClone
* 1.1 5/07/19 JBACULOD added tests on FOs
**/ 
@isTest
public class AppRolesCloneControllerTest {

    @testSetup static void setupData() {
        TestDataFactory.Case2FOSetupTemplate();
        list <Role__c> rolelist = [Select Id, Name From Role__c];
        
        //Create Role Addresses test data
        list <Role_Address__c> raddrlist = new list <Role_Address__c>();
        for (Role__c role : rolelist){
            Role_Address__c raddr = TestDataFactory.createGenericRoleAddress(null, true, role.Id, null);
            raddrlist.add(raddr);
        }
        insert raddrlist;

        Application__c app = [Select Id From Application__c limit 1];
        //Create FOs
        Income1__c inc = new Income1__c(
            RecordTypeId = Schema.SObjectType.Income1__c.getRecordTypeInfosByName().get('PAYG Employment').getRecordTypeId(),
            Income_Type__c = 'PAYG Employment',
            Income_Situation__c = 'Current Income',
            Role__c = rolelist[0].Id,
            Net_Standard_Pay__c = 5000,
            Application1__c = app.Id
        );
        insert inc;

        Expense1__c exp = new Expense1__c(
            RecordTypeId = Schema.SObjectType.Expense1__c.getRecordTypeInfosByName().get('Accommodation Expense').getRecordTypeId(),
            Expense_Type__c = 'Boarder',
            Payment_Frequency__c = 'Monthly',
            Payment_Amount__c = 5000,
            h_FO_Share_Role_IDs__c = rolelist[0].Id + ';',
            Application1__c = app.Id
        );
        insert exp;

        Asset1__c ass = new Asset1__c(
            RecordTypeId = Schema.SObjectType.Asset1__c.getRecordTypeInfosByName().get('Property - Investment').getRecordTypeId(),
            Asset_Type__c = 'Investment Property',
            Asset_Value__c = 5000,
            h_FO_Share_Role_IDs__c = rolelist[0].Id + ';',
            Application1__c = app.Id
        );
        insert ass;

        Liability1__c lia = new Liability1__c(
            RecordTypeId = Schema.SObjectType.Liability1__c.getRecordTypeInfosByName().get('Loan - Real Estate').getRecordTypeId(),
            Liability_Type__c = 'Mortgage - Investment',
            Amount_Owing__c = 5000,
            h_FO_Share_Role_IDs__c = rolelist[0].Id + ';',
            Application1__c = app.Id
        );
        insert lia;

        FO_Link__c folink = new FO_Link__c(
            Income__c = inc.Id,
            Expense__c = exp.Id,
            Asset__c = ass.Id,
            Liability__c = lia.Id,
            h_Application__c = app.Id
        );
        insert folink;

    }

    static testmethod void testinitClone(){
        Application__c app = [Select Id, (Select Id From Assets1__r), (Select Id From Expenses1__r), (Select Id From Liabilities1__r), (Select Id, Role__c From Incomes1__r), 
                              (Select Id, Name, h_Case__c, h_Application__c, h_Path_Option__c, h_Last_Saved_Path_Step__c, FO_Source__c, Asset__c, Asset__r.Name, Asset__r.Asset_Value__c, Asset__r.RecordTypeId, Asset__r.RecordType.Name, Expense__c, Expense__r.Name, Expense__r.RecordTypeId, Expense__r.RecordType.Name, Expense__r.Monthly_Payment__c, Income__c, Income__r.Name, Income__r.RecordTypeId, Income__r.RecordType.Name, Income__r.Monthly_Income__c, Liability__c, Liability__r.Name, Liability__r.RecordTypeId, Liability__r.RecordType.Name, Liability__r.Amount_Owing__c, Asset_Type__c, Liability_Type__c, Income_Type__c, Expense_Type__c From FO_Links__r) From Application__c limit 1];
        list <Role__c> rolelist = [Select Id From Role__c Where Application__c = : app.Id];

        list <string> fsnames = new list <string>();
        fsnames.add('Role_Income_Fields');

        PageReference pref = Page.AppRolesClone;
        Test.setCurrentPage(pref);
        AppRolesCloneController arccon = new AppRolesCloneController();
        AppRolesCloneController.wrappedAppRoleData wARD = AppRolesCloneController.getExistingRolesAndRelatedData(app.Id, fsnames);
        //Verify Role and related info have been retrieved
        system.assert(wARD.app != null);
        system.assert(wARD.roleWrlist.size() > 0);

        //Test Clone Income
        list <AppRolesCloneController.incomeWrapper> incWrlist = new list <AppRolesCloneController.incomeWrapper>();
        for (Income1__c inc : app.Incomes1__r){
            AppRolesCloneController.incomeWrapper incWr = new AppRolesCloneController.incomeWrapper();
            incWr.ogid = inc.Id;
            incWr.inc = inc.clone(false, false, false, false);
            incWrlist.add(incWr);
        }
        list <AppRolesCloneController.incomeWrapper> newIncWrlist = AppRolesCloneController.cloneIncome(incWrlist);

        //Test Clone Expenses
        list <AppRolesCloneController.expenseWrapper> expWrlist = new list <AppRolesCloneController.expenseWrapper>();
        for (Expense1__c exp : app.Expenses1__r){
            AppRolesCloneController.expenseWrapper expWr = new AppRolesCloneController.expenseWrapper();
            expWr.ogid = exp.Id;
            expWr.exp = exp.clone(false, false, false, false);
            expWrlist.add(expWr);
        }
        list <AppRolesCloneController.expenseWrapper> newExpWRlist = AppRolesCloneController.cloneExpenses(expWrlist);

        //Test Clone Assets
        list <AppRolesCloneController.assetWrapper> assWrlist = new list <AppRolesCloneController.assetWrapper>();
        for (Asset1__c ass : app.Assets1__r){
            AppRolesCloneController.assetWrapper assWr = new AppRolesCloneController.assetWrapper();
            assWr.ogid = ass.Id;
            assWr.ass = ass.clone(false, false, false, false);
            assWrlist.add(assWr);
        }
        list <AppRolesCloneController.assetWrapper> newAssWrlist = AppRolesCloneController.cloneAssets(assWrlist);

        //Test Clone Liabilities
        list <AppRolesCloneController.liabilityWrapper> liaWrlist = new list <AppRolesCloneController.liabilityWrapper>();
        for (Liability1__c lia : app.Liabilities1__r){
            AppRolesCloneController.liabilityWrapper liaWr = new AppRolesCloneController.liabilityWrapper();
            liaWr.ogid = lia.Id;
            liaWr.lia = lia.clone(false, false, false, false);
            liaWrlist.add(liaWr);
        }
        list <AppRolesCloneController.liabilityWrapper> newLiaWrlist = AppRolesCloneController.cloneLiabilities(liaWrlist);

        //Test Clone FO Links
        list <FO_Link__c> foLinklist = new list <FO_Link__c>();
        for (FO_Link__c folink : app.FO_Links__r){
            FO_Link__c newFOLink = foLink.clone(false,false,false,false);
            foLinklist.add(newFOLink);
        }
        list <FO_Link__c> newFOLink = AppRolesCloneController.cloneFOLinks(newIncWrlist, newExpWRlist, newAssWrlist, newLiaWrlist, foLinklist, app.Id, null);

        for (AppRolesCloneController.wrappedRoleData wRD : wARD.roleWrlist){
            Role__c toCloneRole = wRD.role.clone(false, false, false, false);
            wRD.role = toCloneRole;
            list <Role_Address__c> newraddrlist = new list <Role_Address__c>();
            for (Role_Address__c raddr : wRD.raddrlist){
                Role_Address__c newRAddr = raddr.clone(false, false, false, false);
                newraddrlist.add(newRAddr);
            }
            wRD.raddrlist = newraddrlist;
        }
        list <AppRolesCloneController.wrappedRoleData> creWRD = AppRolesCloneController.cloneRoles(wARD.roleWrlist);
        list <Role_Address__c> creRaddrlist  = AppRolesCloneController.cloneRoleAddresses(creWRD);
        /*for (AppRolesCloneController.wrappedRoleData wrd : creWRD){
            list <Role_Address__c> newraddrlist = wrd.raddrlist.clone();
            wrd.raddrlist = newraddrlist;
        }
        list <Role_Address__c> creRaddrlist  = AppRolesCloneController.cloneRoleAddresses(creWRD);  */

    }


}
/*
	@Description: controller class for auto creating Box Folder from a SF record
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	9/5/17 - Created
		-	9/6/17 - Added autocreateBoxFolder method
*/
public class Box_AutoCreateRecordFolderCC {

	private Id curID {get;set;} //current Object ID
	public string objType {get;set;}
	private string objQuery {get;set;}
	private list <Box_Group_Permission_Settings__c> boxgroupSet {get;set;}
	private map <string,Box_Group_Permission_Settings__c> boxgroupMap {get;set;}
	private map <string,box.Toolkit.CollaborationType> collabTypeMap {get;set;}
	public string test_objRecordFolderID {get;set;}
	public box.Toolkit.CollaborationType test_collabType {get;set;}

	public Box_AutoCreateRecordFolderCC(ApexPages.StandardController con) {
		
		curID = con.getId();
		system.debug('@@curID:'+curID);

		//Retrieve Box Group Permission Settings custom setting
		boxgroupSet = Box_Group_Permission_Settings__c.getall().values();
		boxgroupmap = new map <string,Box_Group_Permission_Settings__c>();

	}

	public pageReference autocreateBoxFolder(){

		//Box CollabType Values: EDITOR, VIEWER, PREVIEWER, UPLOADER, COOWNER, OWNER, PREVIEWERUPLOADER, VIEWERUPLOADER
		map <string,box.Toolkit.CollaborationType> collabTypeMap = new map <string,box.Toolkit.CollaborationType>();
		collabTypeMap.put('EDITOR', box.Toolkit.CollaborationType.EDITOR);
		collabTypeMap.put('VIEWER', box.Toolkit.CollaborationType.VIEWER);
		collabTypeMap.put('PREVIEWER', box.Toolkit.CollaborationType.PREVIEWER);
		collabTypeMap.put('UPLOADER', box.Toolkit.CollaborationType.UPLOADER);
		collabTypeMap.put('COOWNER', box.Toolkit.CollaborationType.COOWNER);
		collabTypeMap.put('OWNER', box.Toolkit.CollaborationType.OWNER);
		collabTypeMap.put('PREVIEWERUPLOADER', box.Toolkit.CollaborationType.PREVIEWERUPLOADER);
		collabTypeMap.put('VIEWERUPLOADER', box.Toolkit.CollaborationType.VIEWERUPLOADER);
		system.debug('@@collabTypeMap:'+collabTypeMap);

		// Instantiate the Toolkit object
		box.Toolkit boxToolkit = new box.Toolkit();
		// Create a folder and associate it with the record; Return the folder ID if it's already associated on a Box folder
		String objRecordFolderID = boxToolkit.createFolderForRecordId(curID, null, true);
		if (Test.isRunningTest()) objRecordFolderID = test_objRecordFolderID;
		system.debug('@@new item folder id: ' + objRecordFolderID);
		if(objRecordFolderID == null){
			system.debug('most recent error: ' + boxToolkit.mostRecentError);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,boxToolkit.mostRecentError));
			return null;
		}
		else{
			//Get current User Id
			Id userId = UserInfo.getUserId();
			//Retrieve current User
			User curUsr = [Select Id, UserRoleId From User Where Id = : userId];
			Id curUsrRoleId = curUsr.UserRoleId;
			//Initialize Box Folder Collab Type
			box.Toolkit.CollaborationType collabType;

			//Get current Object Type and Folder
			SobjectType curObjType = curID.getSobjectType();
			Schema.DescribeSObjectResult objectDescribe  = curObjType.getDescribe();
			string curFolder = objectDescribe.getLabelPlural();
			system.debug('@@curFolder:'+curFolder);

			for (Box_Group_Permission_Settings__c bgset: boxGroupSet){
				//Get current folder group permissions
				if (curFolder == bgset.Folder__c){
					if (!boxgroupMap.containskey(bgset.SF_Group_Name__c)){
						boxgroupMap.put(bgset.SF_Group_Name__c, bgset);
					}
				}	
			}
			system.debug('@@boxGroupMap:'+boxGroupMap);

			//Get all Group Members in Box Groups
			list <GroupMember> gmembers = [Select Id, GroupId, Group.DeveloperName, Group.RelatedId, UserOrGroupId From GroupMember Where UserOrGroupId = : userId AND Group.Developername in : boxGroupMap.keySet() ];
			system.debug('@@gmembers 1:'+gmembers);

			if (gmembers.isEmpty()){
				//Check group by User's Role
				gmembers = [Select Id, GroupId, Group.DeveloperName, Group.RelatedId, UserOrGroupId From GroupMember Where Group.RelatedId = : curUsrRoleId AND Group.Developername in : boxGroupMap.keySet()];
				system.debug('@@gmembers 2:'+gmembers);
			}

			
			system.debug('@@collabTypeMap2:'+collabTypeMap);

			for (GroupMember member : gmembers){
				system.debug('@@member:'+member);
				system.debug('@@groupdevname:'+member.group.Developername);
				if (boxgroupMap.containskey(member.Group.DeveloperName)){
					//Set current User's Folder Permission on record
					collabType = collabTypeMap.get(boxgroupMap.get(member.Group.DeveloperName).Permission__c);
				}
			}
			system.debug('@@collabType:'+collabType);

			if (Test.isRunningTest()) collabType = test_collabType;

			if (collabType != null){
				//Create collaboration for the folder
				String collabId = boxToolkit.createCollaborationOnRecord(userId, curID, collabType, false);
				system.debug('@@new collaboration id: ' + collabId);

				if(collabId == null){
					system.debug('most recent error: ' + boxToolkit.mostRecentError);
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,boxToolkit.mostRecentError));
					return null;
				}

				// ALWAYS call this method when finished.Since salesforce doesn't allow http callouts after dml operations, we need to commit the pending database inserts/updates or we will lose the associations created
				boxToolkit.commitChanges();
			}
			else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.Box_No_Folder_Access));
				return null;
			}

			//Redirect to object's BoxSection VF
			PageReference pref = new PageReference('/apex/box__' + String.valueof(curObjType) + 'BoxSection?id='+curID);
			pref.setRedirect(true);
			return pref;
		}
	}

}
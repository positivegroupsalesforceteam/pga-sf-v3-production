global class SingleRequestMockEric implements HttpCalloutMock{
    /*
protected Integer code;

        protected String status;

        protected String bodyAsString;

        protected Blob bodyAsBlob;

        protected Map<String, String> responseHeaders;


        public SingleRequestMockEric(Integer code, String status, Blob body,

  Map<String, String> responseHeaders) {

            this.code = code;

            this.status = status;

            this.bodyAsBlob = body;

            this.bodyAsString = null;

            this.responseHeaders = responseHeaders;
        }
 
        public HTTPResponse respond(HTTPRequest req) {

            HttpResponse resp = new HttpResponse();

            resp.setStatusCode(code);

            resp.setStatus(status);

            if (bodyAsBlob != null) {

                resp.setBodyAsBlob(bodyAsBlob);
            } else {

                resp.setBody(bodyAsString);

            }

 

            if (responseHeaders != null) {

                 for (String key : responseHeaders.keySet()) {

                resp.setHeader(key, responseHeaders.get(key));

                 }

            }

            return resp;

        }
*/
    private String bodyContent; 
    public SingleRequestMockEric(String bodyString) {
        this.bodyContent = bodyString;
    }
    
     global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        res.setBody(this.bodyContent);
        res.setStatusCode(200);
        return res;
    }
    
}
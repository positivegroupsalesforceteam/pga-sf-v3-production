/** 
* @FileName: AccountGateway
* @Description: Helper class of AccountHandler which contains most of the logic needed on triggers for Account
* @Copyright: Positive (c) 2018 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 7/28/18 JBACULOD Created Class, added pushAssocRelationshipsInRole
**/ 
public class AccountGateway {

    public static map <Id,Role__c> pushAssocRelationshipsInRole(list <Account> acclist){
        map <id, Role__c> returnRolesMap = new map <id, Role__c>();
        map <Id, list <Role__c>> updateRolesMap = new map <Id, list<Role__c>>();
        map <Id, String> accAssocsRelsMap = new map <id, String>();

        //Iterate to filtered Accounts that were updated
        for (Account acc : acclist){
            accAssocsRelsMap.put(acc.Id, acc.Associated_Relationships__c);
        }

        for (Role__c role : [Select Id, RecordTypeId, RecordType.Name, Account_Name1__c, Account__c, Associated_Relationships__c From Role__c Where Account__c in : accAssocsRelsMap.keySet()]){
            if (!updateRolesMap.containskey(role.Account__c)) updateRolesMap.put(role.Account__c, new list <Role__c>());
            string accAssocs = accAssocsRelsMap.get(role.Account__c);
            if (accAssocs != null){
                if (String.valueof(role.RecordType.Name).startsWith('Applicant')){
                    if (role.RecordType.Name == 'Applicant - Sole Trader' || role.RecordType.Name == 'Applicant - Individual'){ //Person
                        list <String> reltypes = accAssocs.split('</ul>');
                        for (String reltype : reltypes){
                            //Show Spouse Relationship Type
                            system.debug('@@relType:'+relType);
                            if (relType.contains('>SPOUSE<')) {
                                string spouserelType = relType + '</ul>';
                                role.Associated_Relationships__c = spouserelType;
                            }
                        }
                    }
                    else{ //Business
                        list <String> reltypes = accAssocs.split('</ul>');
                        string tempAssocsRel = '';
                        for (String reltype : reltypes){
                            //Show Business Relationship Types
                            if (!relType.contains('>SPOUSE<')) {
                                string busirelType = relType + '</ul>';
                                tempAssocsRel+= busirelType;
                            }
                        }
                        role.Associated_Relationships__c = tempAssocsRel;
                    }
                }
                else if (String.valueof(role.RecordType.Name).startsWith('Partner')){
                    list <String> reltypes = accAssocs.split('</ul>');
                    string tempAssocsRel = '';
                    for (String reltype : reltypes){
                        //Show Business Relationship Types
                        if (!relType.contains('>SPOUSE<')) {
                            string busirelType = relType + '</ul>';
                            tempAssocsRel+= busirelType;
                        }
                    }
                    role.Associated_Relationships__c = tempAssocsRel;
                }
                else if (String.valueof(role.RecordType.Name).startsWith('Trustee')){
                    //Show Spouse and Business Relationship Types
                    role.Associated_Relationships__c = accAssocs;
                }
                else if (String.valueof(role.RecordType.Name).contains('Non-Borrowing Spouse')){
                    list <String> reltypes = accAssocs.split('</ul>');
                    string tempAssocsRel = '';
                    for (String reltype : reltypes){
                        //Show Spouse Relationship Type
                        if (relType.contains('>SPOUSE<')) {
                            string busirelType = relType + '</ul>';
                            tempAssocsRel+= busirelType;
                        }
                    }
                    role.Associated_Relationships__c = tempAssocsRel;
                }
                updateRolesMap.get(role.Account__c).add(role);
            }
        }

        for (Id acckey : updateRolesMap.keySet()){
            for (Role__c role : updateRolesMap.get(accKey)){
                returnRolesMap.put(role.Id, role);
            }
        }

        system.debug('@@returnRolesMap:'+returnRolesMap);
        return returnRolesMap;
    }

    public static Boolean isAccountMainFieldUpdated (Account acc, Account oldacc){
        if(acc.Firstname != oldacc.Firstname || acc.Middlename != oldacc.Middlename || acc.Lastname != oldacc.Lastname 
        || acc.PersonMobilePhone != oldacc.PersonMobilePhone || acc.Phone != oldacc.Phone 
        || acc.Gender1__pc != oldacc.Gender1__pc || acc.Broker_Support_Manager_Name__pc != oldacc.Broker_Support_Manager_Name__pc 
        || acc.Salutation != oldacc.Salutation || acc.PersonEmail != oldacc.PersonEmail){
            return true;//
        }
        else return false;
    }

    public static Contact getContactToUpdate(Account acc, Id conId){
        Contact con = new Contact(  //RecordTypeId = nmIndvidualId,
                                    Id = conId,
                                    Firstname = acc.Firstname,
                                    Middlename = acc.Middlename,
                                    Lastname = acc.Lastname,
                                    MobilePhone = acc.PersonMobilePhone,
                                    Phone = acc.Phone,
                                    Gender1__c = acc.Gender1__pc,
                                    Broker_Support_Manager_Name__c = acc.Broker_Support_Manager_Name__pc,
                                    Salutation = acc.Salutation,
                                    Email = acc.PersonEmail);
        return con;
    }
}
/* 

Author:     Steven Herod, Feb 2014
Purpose:    Backs the Veda Search page retrieving the base information used to prepopulate the search screen
            All Veda API work is deferred to VedaAPI.cls
History:    09/07/2017 - Updated, added validation for Credit Check to allow check just once. (Jesfer Baculod - Positive Group)
            09/08/2017 - set redirecting of page to VedaIndividualPagePrintable once Attachment gets created
            09/20/2017 - Added System Administrator OS on validation when generating CreditReport
            10/17/2017 - Added Mortgage on validation when generating CreditReport (Mortgage can do multiple veda credit check)
*/
public with sharing class VedaIndividualCC {

    
    public boolean saveResultToContact { get; set;}
    public VedaDTO.IndividualResponse response { get; set; }
    public VedaDTO.IndividualRequest request { get; set; }
    public boolean errorMsg;
    public String errorTextMsg { get; set;}
    public Contact contact { get; set;}
    public Lead leadObj { get; set;}
    public Account acctObj { get; set;}
    public Opportunity oppObj { get; set;}
    public Applicant_2__c app2Obj {get; set; }
    public String sObjectType { get; set;}
    private User curUsr {get;set;}
    public string testresponse {get;set;}
    
    public Id idOfObjToAttach { get; set;}
    
    public Id attachmentId { get; set;}

    public VedaIndividualCC(ApexPages.StandardController std) {
        
        errorMsg = false;
        errorTextMsg = '';

        //Retrieve current user details
        Id usrCurID = UserInfo.getUserId();
        curUsr = [Select Id, Name, Profile.Name From User Where Id = : usrCurID];

        //Retreiving the base contact details
        sObjectType = String.valueOf(std.getRecord().getSObjectType());
        VedaDTO.Individual individual;
        idOfObjToAttach = std.getRecord().id;   
        
        if(sObjectType  == 'Account'){          
            //if(CustomSettingUtil.getVedaConfigBooleanValue('PersonAccountEnabled')){
              acctObj = new Account();
               acctObj = [Select a.Website, a.Type, a.State__c, a.ShippingStreet, a.ShippingState, a.ShippingPostalCode, a.ShippingCountry, 
                 a.ShippingCity,  a.Salutation, a.RecordTypeId, a.Postcode__c, a.Phone, a.PersonMobilePhone, a.PersonLastCUUpdateDate, 
                 a.PersonLastCURequestDate, a.PersonEmailBouncedReason, a.PersonEmailBouncedDate, a.PersonEmail, a.PersonContactId, a.LastName, 
                 a.IsPersonAccount, a.Id, a.Gender__c, a.FirstName, a.Fax, 
                 a.Drivers_Licence_Number__c,  a.Description,  a.Date_of_Birth__c, a.Customer_Comments__c, 
                 a.Current_Street_Number__c, a.Current_Street_Name__c, a.Current_Street_Type__c, a.Current_State__c, a.Current_PostCode__c, a.Current_Country__c, 
                 a.Current_Suburb__c, a.Account_PersonMobilePhone__c From Account a where a.id = :std.getRecord().id];
            //}
            if(acctObj != null){ 
            individual = new VedaDTO.Individual(acctObj);           
            }
            
        }else if(sObjectType  == 'Opportunity'){            
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This is apex:pageMessages'));
            //return new PageReference('/'+idOfObjToAttach);    
            oppObj = [Select o.Name, o.Id,
                 a.Website, a.Type, a.State__c, a.ShippingStreet, a.ShippingState, a.ShippingPostalCode, a.ShippingCountry, 
                 a.ShippingCity,  a.Salutation, a.RecordTypeId, a.Postcode__c, a.Phone, a.PersonMobilePhone, a.PersonLastCUUpdateDate, 
                 a.PersonLastCURequestDate, a.PersonEmailBouncedReason, a.PersonEmailBouncedDate, a.PersonEmail, a.PersonContactId, a.LastName, 
                 a.IsPersonAccount, a.Id, a.Gender__c, a.FirstName, a.Fax,  
                 a.Drivers_Licence_Number__c,  a.Description,  a.Date_of_Birth__c, a.Customer_Comments__c, 
                 a.Current_Street_Number__c, a.Current_Street_Name__c, a.Current_Street_Type__c, a.Current_State__c, a.Current_PostCode__c, a.Current_Country__c, 
                 a.Current_Suburb__c From Opportunity o,o.Account a where 
                 o.id = :std.getRecord().id];
            individual = new VedaDTO.Individual(oppObj);    
            
            if(!oppObj.account.isPersonAccount)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This functionality is not supported for Business Opportunity'));
            
        }else if(sObjectType == 'Lead'){
            leadObj = [Select  l.Title, l.Current_Street_Number__c, l.Current_Street_Name__c, l.Current_Street_Type__c, l.Current_Suburb__c, l.Current_State__c, l.Salutation, l.Current_Postcode__c, l.Phone,  l.Name, 
                       l.MobilePhone, l.LastName,  l.Id, l.FirstName, l.Fax, l.Email, l.Description, l.Current_Country__c,
                       l.ConvertedOpportunityId, l.ConvertedDate,l.Drivers_licence_number__c, l.Gender__c,
                       l.ConvertedContactId, l.Date_of_Birth__c, l.ConvertedAccountId, l.Company,
                       l.City From Lead l where l.id = :std.getRecord().id];            
            individual = new VedaDTO.Individual(leadObj);       
        }else{
                app2Obj = [SELECT app2.Name, app2.Id, app2.Email__c, app2.Mobile__c, app2.Current_Street_Number__c, app2.Current_Street_Name__c, app2.Current_Street_Type__c,
                app2.Current_Suburb__c, app2.Current_State__c, app2.Current_Postcode__c, app2.Current_Country__c, app2.Date_of_Birth__c, app2.Gender__c, app2.Drivers_Licence_Number__c
                FROM Applicant_2__c app2 WHERE app2.Id =:std.getRecord().Id];
            
            individual = new VedaDTO.Individual(app2Obj);
                
        }
        
        // Treat the other cases as contact..
/*      else {
            contact = [select id, salutation, gender__c, drivers_license_Number__c, firstname, lastname, birthDate, mailingStreet,
                       mailingCity, mailingState, mailingPostalcode, mailingCountry from Contact where id = :std.getRecord().id];           
            individual = new VedaDTO.Individual(contact);
        }
*/
        
        request = new VedaDTO.IndividualRequest();
        request = request.newAuthorisedAgentConsumerPlusRequest(CustomSettingUtil.getVedaConfigValue('AccessCode'),
                                                                CustomSettingUtil.getVedaConfigValue('AccessPassword'),
                                                                CustomSettingUtil.getVedaConfigValue('ClientReference'),individual);
                                                                //'P3le4s5o6e', 'P2le1s8o3e', '9934', individual);      
        request.mode = CustomSettingUtil.getVedaConfigValue('Request_Mode'); //'test';
        request.version = CustomSettingUtil.getVedaConfigValue('Request_Version'); //'1.0';
        request.carrier = CustomSettingUtil.getVedaConfigValue('Request_Carrier'); //'06';
        request.subscriberIdentifier = CustomSettingUtil.getVedaConfigValue('Request_SubscriberIdentifier'); //'AYFK9999';
        request.security = CustomSettingUtil.getVedaConfigValue('Request_Security'); //'TS'
        //request.individual.currentAddress = new VedaDTO.Address();
        
    } 

    
    public PageReference generateCreditReport() {
        
        try{
            errorMsg = false;
            errorTextMsg = '';

            //check current user if not a Sys Admin, Sys Admin OS, Team Leader, HL Team Leader, Mortgage
            if (!String.valueof(curUsr.Profile.Name).contains(Label.Profile_Name_System_Administrator) && curUsr.Profile.Name != Label.Profile_Name_Team_Leader 
                && curUsr.Profile.Name != Label.Profile_Name_HL_Team_Leader && curUsr.Profile.Name != Label.Profile_Name_Mortgage){
                //Check for existing CCResponse attachment on current record
                list <Attachment> exAttlist = [Select Id, ParentId, Name From Attachment Where ParentId = : idOfObjToAttach];
                for (Attachment att : exAttlist){
                    if (att.Name.contains('-CC.pdf')){
                        errorTextMsg = Label.Veda_Credit_Check_Attachment_Error;
                        break;
                    }
                }
                //Contains error before generating the Credit Report
                if (errorTextMsg != ''){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,errorTextMsg));
                    return null;
                }
            }

            Blob xmlBlob;
            if (!Test.isRunningTest()){
                response = VedaAPI.individualProductCall(request);
                xmlBlob = Blob.valueOf(response.xmlResponse);
            }
            else { 
                xmlBlob = Blob.valueOf(testresponse);      
            }
            Attachment xmlFile = new Attachment(parentId = idOfObjToAttach, name= 'CCResponse-' + DateTime.now().format()+ '.xml', body = xmlBlob);
            insert xmlFile;
            
            attachmentId = xmlFile.id;

            //Redirect to VedaIndividualPagePrintable vf page to convert created XML file to PDF
            PageReference pref = new PageReference('/apex/VedaIndividualPagePrintable?attachID='+attachmentId+'&objAttId='+idOfObjToAttach+'&objType='+sObjectType);
            pref.setRedirect(true);
            return pref;
                    
        }catch(Exception e){
            errorMsg = true;
            errorTextMsg = e.getMessage();
            System.debug('errorTextMsg : '+errorTextMsg );
            System.debug('errorMsg : '+errorMsg );
            return null;
        }
    }
    
    public PageReference back() {
        
        return new PageReference('/'+idOfObjToAttach);
    }
    
    public boolean getErrorMsg() {
        return errorMsg;
    }
    
}
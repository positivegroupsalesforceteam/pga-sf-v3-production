/*
    @Description: test class of ScenarioTrigger, ScenarioTriggerHandler, Scenario_UpdateSLAWarning_SLA1, Scenario_UpdateSLAWarning_SLA2, Scenario_UpdateSLAWarning_SLA3
    @Author: Jesfer Baculod - Positive Group
    @History:
        11/14/17 - Created
*/
@isTest
private class ScenarioTriggerTest {

	private static final string SCEN_STAGE_NEW = Label.Scenario_Stage_New; //New
	private static final string SCEN_STAGE_WINTRODUCER = Label.Scenario_Stage_With_Introducer; //With Introducer
	private static final string SCEN_STAGE_WLENDER = Label.Scenario_Stage_With_lender; //With Lender
	private static final string SCEN_STAGE_COMPLETE = Label.Scenario_Stage_Complete; //Complete
	private static final string SCEN_STAGE_LOST = Label.Scenario_Stage_Lost; //Lost

	private static string SCEN_COMM_RT = Label.Scenario_Commercial_RT; //Commercial
	private static string SCEN_CONS_RT = Label.Scenario_Consumer_RT; //Consumer

	@testsetup static void setup(){

        //Create test data for Scenarios
        Id rtComm= Schema.SObjectType.Complex_Scenario__c.getRecordTypeInfosByName().get(SCEN_COMM_RT).getRecordTypeId();
        list <Complex_Scenario__c> scenlist = new list <Complex_Scenario__c>();
        for (Integer i=0; i<10; i++){
	        Complex_Scenario__c scen = new Complex_Scenario__c(
	        		RecordTypeId = rtComm
	        );
	        scenlist.add(scen);
       	}
        insert scenlist;

	}

	static testmethod void testcalculateSLATimeOnInsert(){

		Trigger_Settings__c trigSet = new Trigger_Settings__c(
                Enable_Scenario_Trigger__c = true,
                Enable_Scenario_recordStageTimestamps__c = true,
                Enable_Scenario_calculateSLATime__c = true
            );
        insert trigset;

        Id rtCons= Schema.SObjectType.Complex_Scenario__c.getRecordTypeInfosByName().get(SCEN_CONS_RT).getRecordTypeId();
        list <Complex_Scenario__c> scenlist = new list <Complex_Scenario__c>();

        Test.startTest();

	        for (Integer i=0; i<20; i++){
	        	Complex_Scenario__c scen = new Complex_Scenario__c(
	        			RecordTypeId = rtCons,
	        			Stage__c = SCEN_STAGE_NEW
	        		);
	        	scenlist.add(scen);
	        }
	        insert scenlist;

	        //Retrieve updated Scenarios
	        list <Complex_Scenario__c> upscenlist = [Select Id, Stage_New_DateTime__c, SLA_Time_1_Start__c From Complex_Scenario__c Where Id in : scenlist];
	        //Verify that TimeStamp, SLA fields were updated
	        for (Complex_Scenario__c scen : upscenlist){
	        	system.assert(scen.Stage_New_DateTime__c != null);
	        	system.assert(scen.SLA_Time_1_Start__c != null);
	        }

        Test.stopTest();

	}

	static testmethod void testSLACalculationTimeStamps(){

		//Retrieve test Scenario data
		list <Complex_Scenario__c> scenlist = [Select Id, Stage__c From Complex_Scenario__c];

		Trigger_Settings__c trigSet = new Trigger_Settings__c(
                Enable_Scenario_Trigger__c = true,
                Enable_Scenario_recordStageTimestamps__c = true,
                Enable_Scenario_calculateSLATime__c = true
            );
        insert trigset;

        Test.startTest();

        		for (Complex_Scenario__c scen : scenlist){
        			scen.Stage__c = SCEN_STAGE_NEW;
        		}
        		update scenlist;

        		for (Complex_Scenario__c scen : scenlist){
        			scen.Stage__c = SCEN_STAGE_WINTRODUCER;
        		}
        		update scenlist;

        		for (Complex_Scenario__c scen : scenlist){
        			scen.Stage__c = SCEN_STAGE_WLENDER;
        		}
        		update scenlist;

        		for (Complex_Scenario__c scen : scenlist){
        			scen.Stage__c = SCEN_STAGE_COMPLETE;
        		}
        		update scenlist;

        		for (Complex_Scenario__c scen : scenlist){
        			scen.Stage__c = SCEN_STAGE_WLENDER;
        		}
        		update scenlist;

        		for (Complex_Scenario__c scen : scenlist){
        			scen.Stage__c = SCEN_STAGE_WINTRODUCER;
        		}
        		update scenlist;

        		for (Complex_Scenario__c scen : scenlist){
        			scen.Stage__c = SCEN_STAGE_COMPLETE;
        		}
        		update scenlist;

        		for (Complex_Scenario__c scen : scenlist){
        			scen.Stage__c = SCEN_STAGE_LOST;
        		}
        		update scenlist;

                for (Complex_Scenario__c scen : scenlist){
                    scen.Stage__c = SCEN_STAGE_NEW;
                }
                update scenlist;

                for (Complex_Scenario__c scen : scenlist){
                    scen.Stage__c = SCEN_STAGE_WINTRODUCER;
                }
                update scenlist;

                for (Complex_Scenario__c scen : scenlist){
                    scen.Stage__c = SCEN_STAGE_COMPLETE;
                }
                update scenlist;

        		//Retrieve updated Scenarios
        		list <Complex_Scenario__c> upscenlist = [Select Id, SLA_Met_1__c, SLA_Met_2__c, SLA_Met_3__c From Complex_Scenario__c Where Id in : scenlist ];
        		//Verify that SLA fields were updated
        		/*for (Complex_Scenario__c scen : upscenlist){
        			system.assertEquals(scen.SLA_Met_1__c, true);
        			system.assertEquals(scen.SLA_Met_2__c, true);
        			system.assertEquals(scen.SLA_Met_3__c, true);
        		} */

        Test.stopTest();

	}

	static testmethod void testSLANextWarningTime(){

		//create test Scenario data
		Complex_Scenario__c scen = new Complex_Scenario__c(
				Stage__c = SCEN_STAGE_NEW
			);
		insert scen;

		Test.startTest();
        	scen.SLA_Time_1_Start__c = Datetime.newInstance(2017, 10, 5, 12, 30, 2);
        	scen.SLA_Warning_Time_1__c = scen.SLA_Time_1_Start__c;
        	scen.SLA_Started_Count_1__c = 1;
        	scen.SLA_Time_2_Start__c = Datetime.newInstance(2017, 10, 5, 12, 30, 2);
        	scen.SLA_Warning_Time_2__c = scen.SLA_Time_2_Start__c;
        	scen.SLA_Started_Count_2__c = 1;
        	scen.SLA_Time_3_Start__c = Datetime.newInstance(2017, 10, 5, 12, 30, 2);
        	scen.SLA_Warning_Time_3__c = scen.SLA_Time_3_Start__c;
        	scen.SLA_Started_Count_3__c = 1;
        	update scen;
        	
            list <ID> scenIDs = new list <ID>();
            scenIDs.add(scen.Id);

            Scenario_UpdateSLAWarning_SLA1.scenarioSLAWarningTimeSA(scenIDs);

            Scenario_UpdateSLAWarning_SLA2.scenarioSLAWarningTimeWI(scenIDs);
            Scenario_UpdateSLAWarning_SLA2.scenarioSLAWarningTimeWI(scenIDs);
            Scenario_UpdateSLAWarning_SLA2.scenarioSLAWarningTimeWI(scenIDs);
            Scenario_UpdateSLAWarning_SLA2.scenarioSLAWarningTimeWI(scenIDs); //update SLA Warning until Stage is Lost

            Scenario_UpdateSLAWarning_SLA3.scenarioSLAWarningTimeWL(scenIDs);
        	
        Test.stopTest();



	}


}
/**
 * @File Name          : RoleSMSButtonsController.cls
 * @Description        : Controller class for SMSButtons.cmp
 * @Author             : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Group              : Positive Group Australia
 * @Last Modified By   : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Last Modified On   : 08/07/2019, 9:20:41 am
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    25/06/2019, 12:56:48 pm   Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Initial Version
 * 1.1    28/06/2019, 9:13:46 am    Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Fixed Converse App Task Query causing Governor Limit Warning
 * 1.2    03/07/2019, 8:47:16 am    Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Added function for sending User inputted SMS
**/
public class SMSButtonsController {

    @AuraEnabled
    /**
    * @description
    * @author Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au) | 26/06/2019
    * @param String curObj
    * @param String partition
    * @param ID recordId
    * @return wrappedAllowedSMSPerObject
    */
    public static wrappedAllowedSMSPerObject getSMSBUttonSettingForObject(String curObj, String partition, ID recordId){

        set <string> smsActionsKeysSet = new set <string>();
        Set<String> extraFieldSet= new Set<String>();
        SObjectType cursObjType = recordId.getSObjectType();  
        list <ID> idForSMSlist = new list <ID>{ recordId };

        //Retrieve allowed SMS buttons for current object base from its Partition
        wrappedAllowedSMSPerObject wASMSpo = new wrappedAllowedSMSPerObject();
        wASMSpo.smsBtnlist = new list <allowedSMS>();
        wASMSpo.newConverseTask = new smagicinteract__Converse_App_Task__c();
        wASMSpo.freebodySMS = new smagicinteract__smsMagic__c();
        for (SMS_Buttons_Setting__mdt smsbtn : [Select Label, Button_Label__c, SMS_Action_Key__c
            From SMS_Buttons_Setting__mdt Where Object__c = : curObj AND Partition__c = : partition AND IsActive__c = true Order By Label ASC]){
               allowedSMS sms = new allowedSMS();
               sms.smsBtn = smsbtn;
               sms.toDisable = false;
               if (smsbtn.SMS_Action_Key__c != null) smsActionsKeysSet.add(smsbtn.SMS_Action_Key__c); //Disregard Compose Button
               if (smsbtn.Button_Label__c == 'Compose') wASMSpo.allowCompose = true;
               else wASMSpo.smsBtnlist.add(sms);
        }

        //Retrieve SMS Templates for each SMS Actions Keys
        smagicinteract.TemplateResolver temp = new smagicinteract.TemplateResolver();
        map <string, string> smsActionKeyTemplates = new map <string, string>();
        if (Test.isRunningTest()) smsActionsKeysSet.add([Select Id, smagicinteract__Automation_Key__c From smagicinteract__Converse_App_Action__c Order By CreatedDate DESC limit 1].smagicinteract__Automation_Key__c);
        for (smagicinteract__Converse_App_Action__c smsAction : [Select Id, smagicinteract__Automation_Key__c, smagicinteract__SMS_Template__r.smagicinteract__Text__c From smagicinteract__Converse_App_Action__c Where smagicinteract__Automation_Key__c in : smsActionsKeysSet]){
            smsActionKeyTemplates.put(smsAction.smagicinteract__Automation_Key__c, smsAction.smagicinteract__SMS_Template__r.smagicinteract__Text__c);
        }
        if (!smsActionKeyTemplates.isEmpty()){
            for (SMSButtonsController.allowedSMS aSMS : wASMSpo.smsBtnlist){
                if (smsActionKeyTemplates.containskey(aSMS.smsBtn.SMS_Action_Key__c)){
                    Map<sobject,String> objectTextmap = temp.resolveTemplate(smsActionKeyTemplates.get(aSMS.smsBtn.SMS_Action_Key__c), cursObjType, idForSMSlist, extraFieldSet);
                    aSMS.smstemplate = objectTextmap.values()[0]; //Retrieve SMS template with parsed Merge Fields (can only return 1 template for 1 Action Key)
                }
            }
        }
        
        return wASMSpo;
    }

    
    @AuraEnabled
    /**
    * @description
    * @author Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au) | 25/06/2019
    * @param smagicinteract__Converse_App_Task__c ctask
    * @param ID recordId
    * @return string
    */
    public static string createSMSNotification(smagicinteract__Converse_App_Task__c ctask, ID recordId){
        Savepoint sp = Database.setSavepoint();
        string respmsg = '';
        try{
            insert ctask;
            set <string> ctaskIDs = new set <string>(); string smsactionkey = '';
            if (ctask.Id != null){ 
                ctaskIDs.add(ctask.Id);
                smsactionkey = ctask.smagicinteract__Automation_Key_Reference__c;
            }
            string ctaskQuery = 'Select Id, smagicinteract__Status__c, smagicinteract__ErrorMessages__c, smagicinteract__Automation_Key_Reference__c From smagicinteract__Converse_App_Task__c ';
            ctaskQuery+= 'Where Id in : ctaskIDs ';
            ctaskQuery+= ' AND ' + String.valueof(recordId.getSObjectType()) + ' = : recordId AND ' + String.valueof(recordId.getSObjectType()) + ' != null '; //Filter by current Object
            ctaskQuery+= ' AND smagicinteract__Automation_Key_Reference__c = : smsactionkey limit 1'; //Filter by created Converse App Task  
            list <smagicinteract__Converse_App_Task__c> createdCAtasklist = database.Query(ctaskQuery);
            system.debug('@@createdCAtasklist:'+createdCAtasklist); 
            respmsg = createdCAtasklist[0].smagicinteract__ErrorMessages__c; 
        }
        catch(Exception e){
            Database.rollback( sp );  
            system.debug('@@Error: '+e.getLineNumber()+e.getMessage());
            AuraHandledException ae = new AuraHandledException('Error: '+ e.getLineNumber()+e.getMessage());
            ae.setMessage('Error: '+ e.getLineNumber()+e.getMessage());
            respmsg = e.getLineNumber()+e.getMessage();
            throw ae;
        }
        return respmsg;
    }

    @AuraEnabled
    /**
    * @description
    * @author Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au) | 03/07/2019
    * @param smagicinteract__smsMagic__c freebodySMS
    * @return string
    */
    public static string sendComposedSMS(smagicinteract__smsMagic__c freebodySMS){
        Savepoint sp = Database.setSavepoint();
        string respmsg = '';
        try{
            freebodySMS.smagicinteract__external_field__c = smagicinteract.ApexAPI.generateUniqueKey();
            insert freebodySMS;
            system.debug('@@freebodySMS:'+freebodySMS);
        }
        catch(Exception e){
            Database.rollback( sp );  
            system.debug('@@Error: '+e.getLineNumber()+e.getMessage());
            AuraHandledException ae = new AuraHandledException('Error: '+ e.getLineNumber()+e.getMessage());
            ae.setMessage('Error: '+ e.getLineNumber()+e.getMessage());
            respmsg = e.getLineNumber()+e.getMessage();
            throw ae;
        }
        return respmsg;
    }

    //main Wrapper for SMS Button
    public class wrappedAllowedSMSPerObject{
        @AuraEnabled public list <allowedSMS> smsBtnlist {get;set;}
        @AuraEnabled public smagicinteract__Converse_App_Task__c newConverseTask {get;set;}
        @AuraEnabled public smagicinteract__smsMagic__c freebodySMS {get;set;}
        @AuraEnabled public boolean allowCompose {get;set;}
    }

    //sub Wrapper for SMS Button
    public class allowedSMS{
        @AuraEnabled public SMS_Buttons_Setting__mdt smsBtn {get;set;}
        @AuraEnabled public string smstemplate {get;set;}
        @AuraEnabled public boolean toDisable {get;set;}
    }

}
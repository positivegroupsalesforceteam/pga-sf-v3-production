/******* An object representation of mapping of nodes in xml *******/
/******* Count of nodes in xml is equal to no of objects of this class *******/
global class XMLMapping {
    String lkObjectName;
    String sfObjectName;
    String insertApiUrl;
    String fieldToUpdate;
    String responseKey;
    String saveResults;
    Boolean saveGlobally = false;
    public String clientFieldName;
    public Map<String,String> xmlNamesMap = new Map<String,String>(); // xml key value
    
    public List<String> removeElementList = new List<String>();
    
    public String getLKObjectName() {
        return lkobjectName;
    }
    public void setLKObjectName(String lkObjectName) {
        this.lkObjectName = lkObjectName;
    }
    
    public String getSFObjectName() {
        return sfObjectName;
    }
    
    public void setSFObjectName(String sfObjectName) {
        this.sfObjectName= sfObjectName;
    }
    
    public String getSaveResultsString() {
        return saveResults;
    }
    public void setSaveResultsString(String strSaveResult) {
        saveResults = strSaveResult;
    }
    public String getInsertApiUrl() {
        return insertApiUrl;
    }
    
    public void setInsertApiUrl(String insertApiUrl) {
        this.insertApiUrl= insertApiUrl;
    }
    
    public String getReqMethod(boolean isUpdate) {
        if(isUpdate) {
            return 'PUT';
        }
        return 'POST';
    }
    
    public void setFieldToUpdate(String fieldApiName) {
        fieldToUpdate = fieldApiName;
    }
    public String getFieldToUpdate() {
        return fieldToUpdate;
    }
    
    public String getResponseKey() {
        return responseKey;
    }
    public void setResponseKey(String respKey) {
        responseKey = respKey;
    }
    
    public Boolean isSaveGlobally() {
        return saveGlobally;
    }
    public void setSaveGlobally(String strSaveGlobally) {
        if(strSaveGlobally != null && (strSaveGlobally.equalsIgnoreCase('true') || strSaveGlobally.equalsIgnoreCase('false'))) {
            saveGlobally = Boolean.valueOf(strSaveGlobally);
        }
    }
    
    public List<String> getSFFieldNames() {
        return xmlNamesMap.values();
    }
    
    /****** Returns comma(,) separated string of all field api names ******/
    public String getSFFieldNamesAsString() {
        String fieldNamesAsString = '';
        for(String value : getSFFieldNames())
        {
            fieldNamesAsString = fieldNamesAsString + value + ',';
        }
        if(fieldNamesAsString.length() > 0)
        {
            fieldNamesAsString = fieldNamesAsString.subString(0,fieldNamesAsString.length()-1);
        }
        return fieldNamesAsString;
    }
    
    /******* Return string query ********/
    global String getQuery() {
        return 'select ' + getSFFieldNamesAsString() + ' from ' + getSFObjectName();
    }
    public String getEndPointUrl(boolean isUpdate, String lkRecordId) {
        if(isUpdate) {
            String url2 = '?auth='+LKIntegration.objLKCst.API_Key__c+'&username='+LKIntegration.objLKCst.Username__c+'&sequence='+LKIntegration.objLKCst.Sequence__c+'&serializer=json';
            String url1 = insertApiUrl;
            if(url1.contains('?')) {
                url1 = url1.subStringBefore('?');
                url1 = url1+'/';
            }
            url1 = url1+lkRecordId+url2;
            return url1;
        }else {
            return insertApiUrl;//+LKIntegration.mapGlobal.get(appendInUrl)+'?serializer=JSON';
        }
        return '';
    }
}
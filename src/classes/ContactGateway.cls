/** 
* @FileName: ContactGateway
* @Description: 
* @Copyright: Positive (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.1 17/01/19 RDAVID: Task from Raoul - Added Logic to map     Relationship/Broker Relationship Manager - comes from Contact/Broker Support Manager Name
                                                                Relationship/Partner Contact Reference Number - comes from Contact/Connective Broker ID
                                                                Logic to populate Account/Introducer_Id__c based on Contact RT:
                                                                If Contact RT is Connective Broker, Account/Introducer_ID__c maps from Contact/Connective_Broker_ID__c
                                                                Else If Contact RT is Business, Account/Introducer_ID__c maps from Contact/P_Number__c
* 1.2 28/05/19 JBACULOD : Removed reference of Channel, Sub Channel fields                        
**/ 
public class ContactGateway {
    private static Id partnerContactId = SObjectType.Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Contact').getRecordTypeId();
    private static Id partnerId = SObjectType.Partner__c.getRecordTypeInfosByDeveloperName().get('Finance_Brokerage').getRecordTypeId();
    private static Id connectiveBrokerRTId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Connective_Broker').getRecordTypeId();
    private static Id businessRTId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId();
    private static Map<Contact,Partner__c> partnerMapToInsert = new Map<Contact,Partner__c>(); //contact Id Key , Partner__c Value
    
    public static Account getAccountFromCon(Id nmIndvidualId, Contact connectBrokerContact){
        Account acc = new Account(  RecordTypeId = nmIndvidualId,
                                    Firstname = connectBrokerContact.Firstname,
                                    Middlename = connectBrokerContact.Middlename,
                                    Lastname = connectBrokerContact.Lastname,
                                    PersonMobilePhone = connectBrokerContact.MobilePhone,
                                    Phone = connectBrokerContact.Phone,
                                    Gender1__pc = connectBrokerContact.Gender1__c,
                                    Salutation = connectBrokerContact.Salutation,
                                    PersonEmail = connectBrokerContact.Email,
                                    Broker_Support_Manager_Name__pc = !String.IsBlank(connectBrokerContact.Broker_Support_Manager_Name__c) ? connectBrokerContact.Broker_Support_Manager_Name__c : '',
                                    Introducer_ID__c = getIntroducerId(connectBrokerContact),
                                    Partition__c = 'Nodifi');
        return acc;
    }

    public static String getIntroducerId (Contact con){
        String introId = '';
        if(con.RecordTypeId == connectiveBrokerRTId) introId = con.Connective_Broker_ID__c;
        else if(con.RecordTypeId == businessRTId) introId = con.P_Number__c;
        return introId;
    }

    public static Boolean isContactMainFieldUpdated (Contact con, Contact oldcon){
        if(con.Firstname != oldCon.Firstname || con.Middlename != oldCon.Middlename || con.Lastname != oldCon.Lastname 
        || con.MobilePhone != oldCon.MobilePhone || con.Phone != oldCon.Phone 
        || con.Gender1__c != oldCon.Gender1__c || con.Broker_Support_Manager_Name__c != oldCon.Broker_Support_Manager_Name__c 
        || con.Salutation != oldCon.Salutation || con.Email != oldCon.Email){
            return true;//
        }
        else return false;
    }

    public static Map<Contact,Relationship__c> getConRelMap (Map<String,Referral_Company__c> referralCompanyMap, Map<Contact,String> referralCompanyName, Map<String,Partner__c> partnerMap, Map<Contact,Account> conAccMap, Id connectiveId){
        
        Map<Contact,Relationship__c> conRelMap = new Map<Contact,Relationship__c>();
        Boolean partnerNotExist = false;

        for(Contact con : referralCompanyName.keySet()){
            if(referralCompanyMap.containsKey(referralCompanyName.get(con)) && partnerMap.containsKey(referralCompanyName.get(con))) {
                System.debug('@@@68 - Referral Company and Partner Exist');
                Relationship__c newRel = new Relationship__c(   RecordTypeId = partnerContactId, 
                                                                Relationship_Type__c = con.Role__c, 
                                                                Partner__c = partnerMap.get(referralCompanyName.get(con)).Id, 
                                                                Associated_Account__c = conAccMap.get(con).Id,
                                                                h_ContactId__c = con.Id,
                                                                Broker_Relationship_Manager1__c = !String.IsBlank(con.Broker_Support_Manager_Name__c) ? con.Broker_Support_Manager_Name__c : '',
                                                                Partner_Contact_Reference_Number1__c = getIntroducerId(con));                 
                conRelMap.put(con,newRel);
            }
            else if(referralCompanyMap.containsKey(referralCompanyName.get(con)) && !partnerMap.containsKey(referralCompanyName.get(con))) {
                Referral_Company__c refCompany = referralCompanyMap.get(referralCompanyName.get(con));

                Partner__c partner = new Partner__c(    Name = refCompany.Name,
                                                        RecordTypeId = partnerId,
                                                        Partner_Type__c = refCompany.Company_Type__c,
                                                        m_Rating__c = refCompany.Rating__c,
                                                        m_Company_Type__c = refCompany.Company_Type__c,
                                                        Partner_Parent__c = connectiveId,
                                                        Partition__c = 'Nodifi',
                                                        //Channel__c = 'Nodifi',
                                                        //Sub_Channel__c = 'BOLT',
                                                        h_ReferralCompanyId__c  = refCompany.Id);

                Relationship__c newRel = new Relationship__c(   RecordTypeId = partnerContactId, 
                                                                Relationship_Type__c = con.Role__c, 
                                                                Partner__c = null, 
                                                                Associated_Account__c = conAccMap.get(con).Id,
                                                                h_ContactId__c = con.Id,
                                                                Broker_Relationship_Manager1__c = !String.IsBlank(con.Broker_Support_Manager_Name__c) ? con.Broker_Support_Manager_Name__c : '',
                                                                Partner_Contact_Reference_Number1__c = getIntroducerId(con));                
                
                partnerMapToInsert.put(con,partner);
                conRelMap.put(con,newRel);
                partnerNotExist = true;
            }
        }

        if(!partnerMapToInsert.isEmpty()){
            Database.insert(partnerMapToInsert.values());
        }

        if(partnerNotExist){
            for(Contact con : conRelMap.keySet()){
                if(conRelMap.get(con).Partner__c == NULL){
                    System.debug('@@@68 - Referral Company and Partner Exist');
                    conRelMap.get(con).Partner__c = partnerMapToInsert.get(con).Id;
                }
            }
        }

        return conRelMap;
    }

    /*public static String getAccConTriggerOrderExecutionStr (sObjectType curSOType, String accConTriggerOrderExecution){
        if(String.valueOf(curSOType) == 'Account' && !accConTriggerOrderExecution.containsIgnoreCase(String.valueOf(curSOType))){
			if(accConTriggerOrderExecution=='') accConTriggerOrderExecution = String.valueOf(curSOType);
			else{
				accConTriggerOrderExecution += ';' + String.valueOf(curSOType);
			}
		} 
		if(String.valueOf(curSOType) == 'Contact' && !accConTriggerOrderExecution.containsIgnoreCase(String.valueOf(curSOType))){
			if(accConTriggerOrderExecution=='') accConTriggerOrderExecution = String.valueOf(curSOType);
			else{
				accConTriggerOrderExecution += ';' + String.valueOf(curSOType);
			}
		} 
        return accConTriggerOrderExecution;
    } */
}
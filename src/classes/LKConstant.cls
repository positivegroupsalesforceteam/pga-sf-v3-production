public class LKConstant {
    
    public static final string Employment = 'Employment';
    public static final string Address = 'Address';
    public static final string Assets = 'Assets';
    public static final string Liability = 'Liability';
    public static final Integer TIME_OUT_VALUE= 60000;
    public static final String SF_CONTACT_ID = 'sf_contact_id';
    public static final String SF_APPLICATION_ID = 'sf_application_id';
    public static final String CONTACT_ID = 'contact_id';
    public static final String APPLICATION_ID = 'application_id';
    public static final String CONFIG_FILE = 'XMLConfig';
    public static final String CUSTOM_SETTING_RESOURCE = 'UAT_Loankit';
    public static final string Assets_address ='LK_Asset__r.LK_Address__c';

}
/** 
* @FileName: RelationshipTriggerTest
* @Description: Test class for Relationship Trigger 
* @Copyright: Positive (c) 2018 
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 6/30/18 JBACULOD Created class
* 1.1 7/27/18 JBACULOD Added testpopulateAssocRelationshipsInAcc
**/ 
@isTest
private class RelationshipTriggerTest {

    @testsetup static void setup(){

        //Create Test Data for Account
        list <Account> acclist = new list <Account>();
        Id curPAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CommonConstants.PACC_RT_I).getRecordTypeId(); //Individual	
        for (Integer i=0; i<20; i++){
            Account acc = TestDataFactory.createGenericPersonAccount(curPAccRTId,'TestFName'+i, 'TestLName'+i);
            acclist.add(acc);
        }
        insert acclist;

    }

    static testmethod void testpopulateAssocRelationshipsInAcc(){

        //Retrieve created test data for Account
        list <Account> acclist = [Select Id, Name From Account];

        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
            Enable_Triggers__c = true,
            Enable_Relationship_Trigger__c = true,
            Enable_Relationship_Assocs_in_Account__c = true
        );
        insert trigSet;

        Test.startTest();

            //Test Insert
            list <Relationship__c> creRelList = new list <Relationship__c>();
            Id curRelSpouseRTId = Schema.SObjectType.Relationship__c.getRecordTypeInfosByName().get(CommonConstants.REL_RT_SPOU).getRecordTypeId();	//Spouse
            Id curRelBusiRelRTId = Schema.SObjectType.Relationship__c.getRecordTypeInfosByName().get(CommonConstants.REL_RT_BREL).getRecordTypeId();	//Business Relationship
            for (Integer i=0; i<10; i++){
                Relationship__c rel = new Relationship__c(
                    RecordTypeId = curRelSpouseRTId,
                    Master_Account__c = acclist[0].Id,
                    Associated_Account__c = acclist[1].Id,
                    Relationship_Type__c = 'Spouse',
                    Start_Date__c = System.Today()
                );
                if (i == 1){ 
                    rel.RecordTypeId = curRelBusiRelRTId;
                    rel.Relationship_Type__c = 'Primary Beneficiary';
                }
                creRellist.add(rel);
            }

            insert creRellist;

            list <Relationship__c> retCreCrellist = [Select Id, Start_Date__c, Master_Account__c, Associated_Account__c, Spouse_Relationship__r.Start_Date__c, Spouse_Relationship__c, Spouse_Relationship__r.Master_Account__c, Spouse_Relationship__r.Associated_Account__c From Relationship__c];
            for (Relationship__c rel : retCreCrellist){
                rel.Start_Date__c = null;
                rel.Master_Account__c = acclist[2].Id;
                rel.Associated_Account__c = acclist[3].Id;
            }

            TriggerFactory.curSoType = null; //Resets recursion varisable
            //Test Update
            update retCreCrellist;

            list <Relationship__c> retCreCrellist2 = [Select Id, Start_Date__c, Master_Account__c, Master_Account__r.Name, Associated_Account__c, Spouse_Relationship__r.Start_Date__c, Spouse_Relationship__c, Spouse_Relationship__r.Master_Account__c, Spouse_Relationship__r.Associated_Account__c From Relationship__c Where Id in : retCreCrellist];            

            //Test Delete
            TriggerFactory.curSoType = null; //Resets recursion varisable
            delete retCreCrellist2[0];

        Test.stopTest();

    }

    static testmethod void testlinkSpouseRelationshipInserUpdate(){

        //Retrieve created test data for Account
        list <Account> acclist = [Select Id, Name From Account];

        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
            Enable_Triggers__c = true,
            Enable_Relationship_Trigger__c = true,
            Enable_Relationship_Spouse_Duplicate__c = true
        );
        insert trigSet;

        Test.startTest();

            //Test Insert
            list <Relationship__c> creRelList = new list <Relationship__c>();
            Id curRelSpouseRTId = Schema.SObjectType.Relationship__c.getRecordTypeInfosByName().get(CommonConstants.REL_RT_SPOU).getRecordTypeId();	//Spouse
            for (Integer i=0; i<10; i++){
                Relationship__c rel = new Relationship__c(
                    RecordTypeId = curRelSpouseRTId,
                    Master_Account__c = acclist[0].Id,
                    Associated_Account__c = acclist[1].Id,
                    Start_Date__c = System.Today()
                );
                creRellist.add(rel);
            }

            insert creRellist;

            list <Relationship__c> retCreCrellist = [Select Id, Start_Date__c, Master_Account__c, Associated_Account__c, Spouse_Relationship__r.Start_Date__c, Spouse_Relationship__c, Spouse_Relationship__r.Master_Account__c, Spouse_Relationship__r.Associated_Account__c From Relationship__c];
            for (Relationship__c rel : retCreCrellist){
                //Verify Spouse Relationship created duplicate Spouse Relationship with reverse Master and Associated Accounts
                system.assert(rel.Spouse_Relationship__c != null);
                system.assertEquals(rel.Master_Account__c, rel.Spouse_Relationship__r.Associated_Account__c);
                system.assertEquals(rel.Associated_Account__c, rel.Spouse_Relationship__r.Master_Account__c);
                system.assertEquals(rel.Start_Date__c, rel.Spouse_Relationship__r.Start_Date__c);
                //Test Update
                rel.Start_Date__c = null;
                rel.Master_Account__c = acclist[2].Id;
                rel.Associated_Account__c = acclist[3].Id;
            }

            TriggerFactory.curSoType = null; //Resets recursion varisable
            RelationshipHandler.spouseRoute = false; //Resets recursion varisable
            //Test Update
            update retCreCrellist;

            list <Relationship__c> retCreCrellist2 = [Select Id, Start_Date__c, Master_Account__c, Master_Account__r.Name, Associated_Account__c, Spouse_Relationship__r.Start_Date__c, Spouse_Relationship__c, Spouse_Relationship__r.Master_Account__c, Spouse_Relationship__r.Associated_Account__c From Relationship__c Where Id in : retCreCrellist];            
            for (Relationship__c rel: retCreCrellist2){
                //Verify Spouse Relationships were updated since duplicates were updated
                system.assertEquals(rel.Start_Date__c, null);
            }

            RelationshipHandler.spouseRoute = false; //Resets recursion varisable
            //Test Delete
            delete retCreCrellist2[0];



        Test.stopTest();

    }

}
/** 
* @FileName: AssetGateway
* @Description: Provides finder methods for accessing data in the Asset object.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification 
* 1.0 2/14/18 RDAVID Created class, Added getRoleIds
**/ 

public without sharing class AssetGateway{

    /**
    * @Description: Returns a set of Parent Role Id associated to Asset record. 
    * @Arguments:   List<Asset1__c> newAssetList - trigger.new
    * @Returns:     Set<Id> getRoleIds - parent Role ids
    */

    public static Set<Id> getRoleIds(List<Asset1__c> newAssetList){
        Set<Id> roleIdsSet = new Set<Id>();

        for(Asset1__c asset : newAssetList){
            //CHANGE123 roleIdsSet.add(asset.Role__c);
        }
        return roleIdsSet;
    }
}
/** 
* @FileName: ReferralCompanyHandler
* @Description: Trigger Handler for the Referral_Company__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 7/01/19 RDAVID Created Class - Referral Company Trigger Logic - Insert
* 1.1 22/03/19 RDAVID Updated Class - Added Logic to prevent Partner Trigger Recursion
* 1.2 2/07/2019 RDAVID Updated Class - extra/task25040162-ReferralCompanySyncToPartnerTrigger - Applied logic to map the following:
        Referral_Company__c.Inbound_Partner_ID__c > Partner__c.Inbound_Partner_ID__c
        Referral_Company__c.Name > Partner__c.Name
        Referral_Company__c.Inbound_Business_Entity_Name__c > Partner__c.Inbound_Business_Entity_Name__c
**/ 
public without sharing class ReferralCompanyHandler implements ITrigger{
    private List<Referral_Company__c> referralCompanyList = new List<Referral_Company__c>();
    private List<Referral_Company__c> validReferralCompanyList = new List<Referral_Company__c>();
    private Map<Id,Referral_Company__c> updatedValidReferralCompanyMap = new Map<Id,Referral_Company__c>();
    
    private Map<Id,Partner__c> refCompPartnerMapToInsert = new Map<Id,Partner__c>();
    private Map<Id,Address__c> refCompAddressMapToUpsert = new Map<Id,Address__c>();

    private Map<Id,Partner__c> partnerMapToUpdate = new Map<Id,Partner__c>();
    private Map<Id,Relationship__c> relationshipMapToUpdate = new Map<Id,Relationship__c>();
    
    private Set<String> validCompanyTypes = new Set<String>{'Professional Services','Real Estate Agent','Financial Planner','Mortgage Broker','Accountant','Mortgage Aggregator'};
    private Map<String,Partner__c> relatedSiteMap = new Map<String,Partner__c>(); 
    private Map<Id,ABN__c> abnMap = new Map<Id,ABN__c>(); 
    private Map<String,Location__c> locationMap = new Map<String,Location__c>();

    private Id nmIndvidualId = SObjectType.Account.getRecordTypeInfosByDeveloperName().get('nm_Individual').getRecordTypeId();
    private Id partnerContactRTId = SObjectType.Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Contact').getRecordTypeId();
    
    public ReferralCompanyHandler(){
        referralCompanyList = (Trigger.IsInsert || Trigger.IsUpdate) ? (List<Referral_Company__c>) Trigger.new : (Trigger.IsDelete) ? (List<Referral_Company__c>) Trigger.old : null;
    }

    public void bulkBefore(){
        
    }

    public void bulkAfter(){
        Set<Id> relatedSiteIds = new Set<Id>();
        Set<Id> abnIds = new Set<Id>();
        Set<String> locName = new Set<String>();
        Set<Id> deletedRefIds = new Set<Id>();
        Map<Id,Referral_Company__c> oldReferralCompanyMap = (Map<Id,Referral_Company__c>) Trigger.oldMap;
        System.debug('@@@44 canPartnerCreateRun -------------> '+PartnerGateway.shouldFirePartnerCreation);
        if(PartnerGateway.shouldFirePartnerCreation){
            for(Referral_Company__c refCompany : referralCompanyList){
                if(TriggerFactory.trigset.Enable_Sync_Referral_Company_and_Partner__c){
                    if(refCompany.Company_Type__c != NULL && validCompanyTypes.contains(refCompany.Company_Type__c)){
                        if(Trigger.IsInsert){
                            relatedSiteIds.add(refCompany.Related_Site__c);
                            abnIds.add(refCompany.Legal_Entity_Name__c);
                            validReferralCompanyList.add(refCompany);
                            if(refCompany.Postcode__c != NULL && refCompany.Suburb__c != NULL){
                                locName.add(refCompany.Postcode__c+'-'+refCompany.Suburb__c.toUpperCase());
                            }
                        }
                        if(Trigger.IsUpdate){
                            Boolean referralCompanyFieldsUpdated = ReferralCompanyGateway.isReferralCompanyUpdated(refCompany,oldReferralCompanyMap.get(refCompany.Id));
                            System.debug('@@@44 referralCompanyFieldsUpdated -------------> '+referralCompanyFieldsUpdated);
                            Boolean addressFieldsUpdated = ReferralCompanyGateway.isAddressUpdated(refCompany,oldReferralCompanyMap.get(refCompany.Id));
                            if(referralCompanyFieldsUpdated){
                                relatedSiteIds.add(refCompany.Related_Site__c);
                                abnIds.add(refCompany.Legal_Entity_Name__c);
                                updatedValidReferralCompanyMap.put(refCompany.Id,refCompany);    
                                if(addressFieldsUpdated){
                                    if(refCompany.Postcode__c != NULL && refCompany.Suburb__c != NULL){
                                        locName.add(refCompany.Postcode__c+'-'+refCompany.Suburb__c.toUpperCase());
                                    }
                                }
                            }
                        }
                        if(Trigger.IsDelete){
                            deletedRefIds.add(refCompany.Id);
                        }
                    }
                }
            }
        }

        if(abnIds.size() > 0) abnMap = new Map<Id,ABN__c>([SELECT Id,Name,Registered_ABN__c FROM ABN__c WHERE Id In: abnIds]);
        if(relatedSiteIds.size() > 0) relatedSiteMap = ReferralCompanyGateway.getRelatedSiteMap(relatedSiteIds);
        locationMap = ReferralCompanyGateway.getLocationMap(locName);
        if(deletedRefIds.size() > 0) relationshipMapToUpdate = ReferralCompanyGateway.getRelationshipMapToUpdate(deletedRefIds);

        //Insert
        if(validReferralCompanyList.size() > 0){
            for(Referral_Company__c refComp : validReferralCompanyList){
                if(refComp.Address_Line_1__c != NULL) refCompAddressMapToUpsert.put(refComp.Id,ReferralCompanyGateway.createUpdateAddress(refComp,locationMap));
                refCompPartnerMapToInsert.put(refComp.Id,ReferralCompanyGateway.createUpdatePartner(refComp, relatedSiteMap, abnMap));
            }
        }

        //Update
        if(!updatedValidReferralCompanyMap.isEmpty()){
            partnerMapToUpdate = new Map<Id,Partner__c> ([SELECT Id,Name, m_Referral_Company_SFID__c, Partner_Address__c, Partner_Type__c, RecordTypeId, Franchise_Partner__c, Partner_Parent__c, Partner_Phone__c, Partner_Website__c, m_Registered_Business_Name__c, m_Legal_Entity_name__c ,Partner_ABN__c, Partner_Legal_Name__c,m_Nodifi_Onboarding_Stage__c FROM Partner__c WHERE m_Referral_Company_SFID__c IN: updatedValidReferralCompanyMap.keySet()]);
            for(Partner__c partner : partnerMapToUpdate.values()){
                Id partnerId = partner.Id;
                Id addressId = partner.Partner_Address__c;
                partner = ReferralCompanyGateway.createUpdatePartner(updatedValidReferralCompanyMap.get(partner.m_Referral_Company_SFID__c), relatedSiteMap, abnMap);
                partner.Id = partnerId;
                partnerMapToUpdate.put(partnerId,partner);
                Boolean addressFieldsUpdated = ReferralCompanyGateway.isAddressUpdated(updatedValidReferralCompanyMap.get(partner.m_Referral_Company_SFID__c),oldReferralCompanyMap.get(partner.m_Referral_Company_SFID__c));
                if(addressFieldsUpdated){
                    Address__c addressToUpdate = ReferralCompanyGateway.createUpdateAddress(updatedValidReferralCompanyMap.get(partner.m_Referral_Company_SFID__c),locationMap);
                    addressToUpdate.Id = addressId;
                    refCompAddressMapToUpsert.put(partner.m_Referral_Company_SFID__c,addressToUpdate);
                }
            }
        }
    }

    public void beforeInsert(SObject so){

    }

    public void beforeUpdate(SObject oldSo, SObject so){

    }
    
    public void beforeDelete(SObject so){	
	
    }

    public void afterInsert(SObject so){

    }

    public void afterUpdate(SObject oldSo, SObject so){

    }

    public void afterDelete(SObject so){
	}
    
    public void andFinally(){
        if(!refCompAddressMapToUpsert.isEmpty()) Database.upsert(refCompAddressMapToUpsert.values());
        if(!refCompPartnerMapToInsert.isEmpty()){
            for(Partner__c partner : refCompPartnerMapToInsert.values()){
                if(refCompAddressMapToUpsert.containsKey(partner.m_Referral_Company_SFID__c)) 
                    refCompPartnerMapToInsert.get(partner.m_Referral_Company_SFID__c).Partner_Address__c = refCompAddressMapToUpsert.get(partner.m_Referral_Company_SFID__c).Id;
            }
            Database.insert(refCompPartnerMapToInsert.values()); //INSERT Partner Records
        }
        
        if(!partnerMapToUpdate.isEmpty()){
            Database.update(partnerMapToUpdate.values()); //UPDATE Partner Records
        }

        if(!relationshipMapToUpdate.isEmpty()){
            Database.update(relationshipMapToUpdate.values()); //UPDATE End_Date__c of Relationship Records
        }
    }
}
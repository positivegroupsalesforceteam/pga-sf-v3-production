/**
 * @File Name          : CommissionCTTriggerTest.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 18/07/2019, 2:08:51 pm
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    18/07/2019, 2:04:09 pm   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
@isTest
public class CommissionCTTriggerTest {
    @testsetup static void setup(){
        //Turn On Trigger 
		Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
		triggerSettings.Enable_Triggers__c = true;
		triggerSettings.Enable_Case_Trigger__c = true;
		triggerSettings.Enable_Role_Trigger__c = true;
        triggerSettings.Enable_Income_Trigger__c = true;
        triggerSettings.Enable_Front_End_Submission_Trigger__c = true;
		triggerSettings.Enable_Frontend_Sub_SetNVMContact__c = true;
        triggerSettings.Enable_CommissionCT_Trigger__c = true;
        triggerSettings.Enable_CommissionCT_GetCaseLender__c = true;
        triggerSettings.Enable_Commission_Line_Trigger__c = true;
        triggerSettings.Enable_CommissionLine_GetCommission__c = true;
		upsert triggerSettings Trigger_Settings1__c.Id;
        TestDataFactory.Case2FOSetupTemplate();
	}

    static testmethod void testFinance(){
        Test.StartTest();
            List<Case> caseList = [SELECT Id,NVMConnect__NextContactTime__c,Lender1__c FROM Case LIMIT 1];
            // System.assertNotEquals(caseList[0].Lender1__c,null);
            List<Partner__c> partnerList = TestDataFactory.createPartner(1, 'Lender Division', null);
            insert partnerList;
            caseList[0].Lender1__c = partnerList[0].Id;
            update caseList[0];
            System.assertEquals(caseList[0].Lender1__c,partnerList[0].Id);

            List<CommissionCT__c> commList = TestDataFactory.createCommission(1, 'NM - Asset Finance - Commission', caseList[0].Id);
            insert commList;
            update commList;
            List<Commission_Line_Items__c> commLineList = TestDataFactory.createCommissionLine(1,'Asset Finance - Finance Commission', commList[0].Id);
            insert commLineList;
            update commLineList;
            commLineList = [SELECT Id, h_CommissionCT_Finance__c, h_CommissionCT_Insurance__c FROM Commission_Line_Items__c WHERE Id =: commLineList[0].Id];
            System.assertEquals(commList[0].Id,commLineList[0].h_CommissionCT_Finance__c);
            System.assertEquals(NULL,commLineList[0].h_CommissionCT_Insurance__c);
            delete commList;
        Test.StopTest();
    }

    static testmethod void testInsurance(){
        Test.StartTest();
            List<Case> caseList = [SELECT Id,NVMConnect__NextContactTime__c,Lender1__c FROM Case LIMIT 1];
            List<Partner__c> partnerList = TestDataFactory.createPartner(1, 'Lender Division', null);
            insert partnerList;
            caseList[0].Lender1__c = partnerList[0].Id;
            update caseList[0];
            System.assertEquals(caseList[0].Lender1__c,partnerList[0].Id);

            List<CommissionCT__c> commList = TestDataFactory.createCommission(1, 'NM - Asset Finance - Commission', caseList[0].Id);
            insert commList;

            List<Commission_Line_Items__c> commLineList = TestDataFactory.createCommissionLine(1,'Asset Finance - Insurance Commission', commList[0].Id);
            insert commLineList;
            commLineList = [SELECT Id, h_CommissionCT_Finance__c, h_CommissionCT_Insurance__c FROM Commission_Line_Items__c WHERE Id =: commLineList[0].Id];
            System.assertEquals(NULL,commLineList[0].h_CommissionCT_Finance__c);
            System.assertEquals(commList[0].Id,commLineList[0].h_CommissionCT_Insurance__c);
            update commLineList;
            delete commLineList;
            delete commList;
        Test.StopTest();
    }

    static testmethod void testDeleteCommCTlist(){
        Test.StartTest();

            //Retrieve Trigger Settings
            Trigger_Settings1__c trigSet = [Select Id, Enable_Case_Delete_CommCT_on_Lost__c From Trigger_Settings1__c];
            trigSet.Enable_Case_Delete_CommCT_on_Lost__c = true;
            update trigSet;

            //Retrieve Test Case
            List<Case> caseList = [SELECT Id,Status, Stage__c FROM Case LIMIT 1];


            //Create Test data for Commission CT
            CommissionCT__c commCT = new CommissionCT__c(
                Case__c = caselist[0].Id,
                RecordTypeId = Schema.SObjectType.CommissionCT__c.getRecordTypeInfosByName().get('NM - Asset Finance - Commission').getRecordTypeId()
            );
            insert commCT;

            caselist[0].Status = 'Closed';
            caselist[0].Stage__c = 'Lost';
            update caselist[0];

            CommissionCTGateway commCTgate = new CommissionCTGateway(caselist);
			ID jobID = System.enqueueJob(commCTgate);

            //Verify CommissionCT got delete
            //list <CommissionCT__c> delCommCTlist = [Select Id From CommissionCT__c Where Case__c = : caselist[0].Id];
            //system.assertEquals(delCommCTlist.size(),0);


        Test.StopTest();
    }
}
/*
	@Description: Test Class for Case_UpdateLFPFlow
	@Author: Rexie David - Positive Group
	@History:
		-	4/07/2019 - extra/task25140782-UpdateCaseAfter48Hours - Created
*/

@isTest
private class Case_UpdateLFPFlowv2Test {
    @testSetup static void setupData() {
 
        Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
        TestDataFactory.activateAllTriggerSettingsNM(triggerSettings);
        upsert triggerSettings Trigger_Settings1__c.Id;
        
        Process_Flow_Definition_Settings1__c processFlowSettings = Process_Flow_Definition_Settings1__c.getOrgDefaults();
        TestDataFactory.activateAllProcessFlowSettingsNM(processFlowSettings);
        processFlowSettings.Enable_Case_Flow_Definitions__c = FALSE;
        processFlowSettings.Enable_Case_NVM_Flow_Definitions__c = FALSE;
        processFlowSettings.Enable_Case_Assignment__c = FALSE;
        upsert processFlowSettings Process_Flow_Definition_Settings1__c.Id;

        Location__c loc = new Location__c(
            Name = '2304-KOORAGANG',
            Country__c = 'Australia',
            Postcode__c = '2304',
            State__c = 'New South Wales',
            State_Acr__c = 'NSW',
            Suburb_Town__c = 'KOORAGANG'
        );
        insert loc;

        //Create Test Account
        List<Account> accList = TestDataFactory.createGenericPersonAccount('First Name','TestLastName',CommonConstants.PACC_RT_I, 2);
        accList[0].Location__c = loc.Id;
        accList[1].Location__c = loc.Id;
        Database.insert(accList);
        System.assertEquals(accList[0].Location__c,loc.Id);
        
        accList = [SELECT Id, IsPersonAccount FROM Account WHERE Name LIKE '%TestLastName%'];
        System.assertEquals(accList[0].IsPersonAccount,true);
        System.assertEquals(accList.size(),2);

        //Create Test Case
        List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_P_CONS_AF, 1);
        caseList[0].Partition__c = 'Positive';
        caseList[0].Lead_Source__c = 'LoansForPeopleWithBadCredit - Website';
        caseList[0].PLS_Initial_Team_Queue__c = 'LFP Flow';
        caseList[0].Lead_Bucket__c = 'LFP Flow';
        caseList[0].Lead_Creation_Method__c = 'Frontend Load';
        caseList[0].Channel__c = 'LFPWBC';
        caseList[0].Stage__c = 'WOPA';
        caseList[0].Lead_Loan_Amount__c = 5000;
        caseList[0].Lead_Purpose__c = 'Car Loan';
        caseList[0].From_Lead_Path__c = false;
        caseList[0].OwnerId = [SELECT Id FROM Group WHERE Name = 'Consumer Asset' LIMIT 1].Id;
        // caseList[0].Primary_Contact_Location_MC__c = loc.Id;
        Database.insert(caseList);

        List<Frontend_Submission__c> feList = TestDataFactory.creatFESubs(1, caseList[0].Id, 'Quick Quote', '6641C95F-69CE-4E4C-B48D-BAD1E53042D5', 'LFPWBC', 'Bad Credit Loans - Banks Say No? We Say Yes');
        Database.insert(feList);
        caseList = [SELECT Id ,Application_Name__c FROM Case WHERE Id IN: caseList];
        System.assertNotEquals(caseList[0].Application_Name__c,null);

        List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, caseList[0].Application_Name__c,  1);
        roleList[0].Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT;
        roleList[0].Phone__c = '09165856439';
        roleList[0].Mobile_Phone__c = '09165856439';
        roleList[0].Email__c = 'test@email.com';
        roleList[0].Primary_Contact_for_Application__c = true;
        Database.insert(roleList);

        roleList = [SELECT Id,Address__c,Account__c,Account__r.Address__c FROM Role__c WHERE Application__c =: caseList[0].Application_Name__c];

        System.assertEquals(roleList.size(),1);
        System.assertEquals(roleList[0].Address__c,NULL);
        System.assertEquals(roleList[0].Account__r.Address__c,NULL);
        System.assertNotEquals(roleList[0].Account__c,NULL);

        //Create test data for Address
        Address__c address = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
        address.Location__c = loc.Id;
        Database.insert(address);
        Address__c address2 = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
        address2.Location__c = loc.Id;
        Database.insert(address2);
        
    }

    static testmethod void testUpdateFromLFP(){
        Case cse = [Select Id, Primary_Contact_Name_MC__c,Primary_Contact_Name_MC__r.Location__c,Primary_Contact_Location_MC__c, State_Code__c, Stage__c, OwnerId From Case WHERE Recordtype.Name =: CommonConstants.CASE_RT_P_CONS_AF AND Lead_Source__c = 'LoansForPeopleWithBadCredit - Website' AND Lead_Loan_Amount__c = 5000 LIMIT 1];
        Location__c loc = [SELECT Id FROM Location__c LIMIT 1];
        cse.Primary_Contact_Location_MC__c = loc.Id;
        Database.update(cse);
        cse = [Select Id, Primary_Contact_Name_MC__c,Primary_Contact_Name_MC__r.Location__c,Primary_Contact_Location_MC__c, State_Code__c, Stage__c, OwnerId From Case WHERE Recordtype.Name =: CommonConstants.CASE_RT_P_CONS_AF AND Lead_Source__c = 'LoansForPeopleWithBadCredit - Website' AND Lead_Loan_Amount__c = 5000 LIMIT 1];
        System.assertEquals(cse.Stage__c,'WOPA');
        System.assertNotEquals(cse.Primary_Contact_Name_MC__c,null);
        System.assertNotEquals(cse.Primary_Contact_Name_MC__r.Location__c,null);
        System.assertNotEquals(cse.Primary_Contact_Location_MC__c,null);
        System.assertEquals(cse.State_Code__c,'NSW');
        List<String> csIds = new List<String>();
        csIds.add(cse.Id);
        Test.startTest();
            Case_UpdateLFPFlowv2.updateLFPCase(csIds);
        Test.stopTest();
    }

    static testmethod void testUpdateFromCallRequest(){
        Case cse = [Select Id, Primary_Contact_Name_MC__c,Primary_Contact_Name_MC__r.Location__c,Primary_Contact_Location_MC__c, State_Code__c, Stage__c, OwnerId From Case WHERE Recordtype.Name =: CommonConstants.CASE_RT_P_CONS_AF AND Lead_Source__c = 'LoansForPeopleWithBadCredit - Website' AND Lead_Loan_Amount__c = 5000 LIMIT 1];
        List<Frontend_Submission__c> feList = TestDataFactory.creatFESubs(1, cse.Id, FrontEndSubmissionHandler.NOD_REQUEST_CALL, '6641C95F-69CE-4E4C-B48D-BAD1E53042D5', 'LFPWBC', 'Bad Credit Loans - Banks Say No? We Say Yes');
        feList[0].Preferred_Call_Time__c = Time.newInstance(9, 30, 0, 0);
        Database.insert(feList);
        Location__c loc = [SELECT Id FROM Location__c LIMIT 1];
        cse.Latest_Frontend_Submission__c = FrontEndSubmissionHandler.NOD_REQUEST_CALL;
        cse.Primary_Contact_Location_MC__c = loc.Id;
        Database.update(cse);
        cse = [Select Id, Primary_Contact_Name_MC__c,Primary_Contact_Name_MC__r.Location__c,Primary_Contact_Location_MC__c, State_Code__c, Stage__c, OwnerId From Case WHERE Recordtype.Name =: CommonConstants.CASE_RT_P_CONS_AF AND Lead_Source__c = 'LoansForPeopleWithBadCredit - Website' AND Lead_Loan_Amount__c = 5000 LIMIT 1];
        System.assertEquals(cse.Stage__c,'WOPA');
        System.assertNotEquals(cse.Primary_Contact_Name_MC__c,null);
        System.assertNotEquals(cse.Primary_Contact_Name_MC__r.Location__c,null);
        System.assertNotEquals(cse.Primary_Contact_Location_MC__c,null);
        System.assertEquals(cse.State_Code__c,'NSW');
        List<String> csIds = new List<String>();
        csIds.add(cse.Id);
        Test.startTest();
            Case_UpdateLFPFlowv2.updateLFPCase(csIds);
        	SR_CallRequest.createSRCallRequest(new List<Id> {csIds[0]});
            SR_CallRequest.createSRCallRequest(new List<Id> {feList[0].Id});
        Test.stopTest();
    }
}
/*
    @author: Jesfer Baculod - Positive Group
    @history: 06/27/17 - Created
    		  06/30/17 - Updated
    		  08/04/17 - Updated Record Type Labels
    @description: test class of Case_EditVendorDetails_Controller class
*/
@isTest
private class Case_EditVendorDetails_ControllerTest {
	
	private static final string CON_RT_BUSINESS = Label.Contact_Business_RT; //Business
    private static final string CON_RT_KEYPERSON = Label.Contact_Key_Person_RT; //Key Person
    private static final string CASE_RT_PRIVATE = Label.Case_Private_RT; //Consumer Loan

	@testsetup static void setup(){

		Referral_Company__c rc = new Referral_Company__c(
				Name = 'Test Referral Company'
			);
		insert rc;

		ABN__c abn = new ABN__c(
				Name = 'Test Legal Entity ABN',
				Registered_ABN__c = '123456789'
			);
		insert abn;

		Id rtBusinessCon = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CON_RT_BUSINESS).getRecordTypeId();
		Contact bcon = new Contact(
				RecordTypeId = rtBusinessCon,
				Email = 'test@test.com',
				LastName = 'Test BContact',
				Referral_Company__c = rc.Id
			);
		insert bcon;

		Id rtKeyPersonCon = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CON_RT_KEYPERSON).getRecordTypeId();
		Contact kpcon = new Contact(
				RecordTypeId = rtKeyPersonCon,
				Email = 'test@test.com',
				LastName = 'Test KPContact'
			);
		insert kpcon;

		Id rtPrivateLoan = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CASE_RT_PRIVATE).getRecordTypeId();
		Case cse = new Case(
				RecordTypeId = rtPrivateLoan,
				Status = 'New',
				Sale_Type__c = Label.Case_Sale_Type_Dealer
			);
		insert cse;

	}

	static testmethod void testSearches(){

		//Retrieve case to be used for editing vendor details
		Case cse = [Select Id, Subject, Status, Dealer_Contact__c, Dealer_Vendor__c, Vendor_Contact__c From Case];
		list <Referral_Company__c> rclist = [Select Id, Name From Referral_Company__c];
		list <Contact> bconlist = [Select Id, Name From Contact Where RecordType.Name = : CON_RT_BUSINESS];
		list <Contact> kpconlist = [Select Id, Name From Contact Where RecordType.Name = : CON_RT_KEYPERSON];

		PageReference tpref = Page.Case_EditVendorDetails;
		tpref.getParameters().put('id', cse.Id);
		Test.setCurrentPage(tpref);

		ApexPages.StandardController st0 = new ApexPages.StandardController(cse);
		Case_EditVendorDetails_Controller cevdcc0 = new Case_EditVendorDetails_Controller(st0);

		cse.Dealer_Vendor__c = rclist[0].Id;
		cse.Dealer_Contact__c = bconlist[0].Id;
		update cse;

		ApexPages.StandardController st = new ApexPages.StandardController(cse);
		Case_EditVendorDetails_Controller cevdcc = new Case_EditVendorDetails_Controller(st);

		Test.startTest();
			cevdcc.cse_DV = 'Test Referral Company'; //search for existing Referral Company
			cevdcc.searchReferralCompany();
			cevdcc.searchMode = 1;
			cevdcc.refreshPageSize();
			cevdcc.cse_s_ID = rclist[0].Id; //a record has been selected on the search results
			cevdcc.assignLookupID();

			cevdcc.cse_DC = 'Test BContact'; //search for existing Business Contact 
			cevdcc.searchBusinessContact();
			cevdcc.searchMode = 2;
			cevdcc.refreshPageSize();
			cevdcc.cse_s_ID = bconlist[0].Id; //a record has been selected on the search results
			cevdcc.assignLookupID();

			cevdcc.cse_DV_id = null;
			cevdcc.searchBusinessContact();

			cse.Sale_Type__c = Label.Case_Sale_Type_Private;
			cse.Vendor_Contact__c = null;
			update cse;

			ApexPages.StandardController st2 = new ApexPages.StandardController(cse);
			Case_EditVendorDetails_Controller cevdcc2 = new Case_EditVendorDetails_Controller(st2);

			cevdcc2.cse_VC = 'Test KPContact'; //search for exisitng Key Person Contact
			cevdcc2.searchKeyPersonContact();
			cevdcc2.searchMode = 3;
			cevdcc2.refreshPageSize();
			cevdcc2.cse_s_ID = kpconlist[0].Id; //a record has been selected on the search results
			cevdcc2.assignLookupID(); 

			cse.Vendor_Contact__c = kpconlist[0].Id;
			update cse;

			ApexPages.StandardController st3 = new ApexPages.StandardController(cse);
			Case_EditVendorDetails_Controller cevdcc3 = new Case_EditVendorDetails_Controller(st3);			

		Test.stopTest();

		system.assertEquals(cse.Dealer_Vendor__c,rclist[0].Id); //Verify that Dealer Sale Vendor is same with selected Referral Company
		system.assertEquals(cse.Dealer_Contact__c,bconlist[0].Id); //Verify that Dealer Contact Person is same with selected Business Contact
		system.assertEquals(cse.Vendor_Contact__c,kpconlist[0].Id); //Verify that Private Sale Vendor Contact is same with selected Key Person Contact

	}

	static testmethod void testSaveCreateOnLookups(){

		//Retrieve case to be used for editing vendor details
		Case cse = [Select Id, Subject, Status, Sale_Type__c, Dealer_Contact__c, Dealer_Vendor__c, Vendor_Contact__c From Case];
		ABN__c abn = [Select Id, Name, Registered_ABN__c From ABN__c];

		list <Referral_Company__c> rclist = [Select Id, Name From Referral_Company__c];
		list <Contact> bconlist = [Select Id, Name From Contact Where RecordType.Name = : CON_RT_BUSINESS];
		list <Contact> kpconlist = [Select Id, Name From Contact Where RecordType.Name = : CON_RT_KEYPERSON];

		ApexPages.StandardController st = new ApexPages.StandardController(cse);
		PageReference tpref = Page.Case_EditVendorDetails;
		tpref.getParameters().put('id', cse.Id);
		Test.setCurrentPage(tpref);

		Case_EditVendorDetails_Controller cevdcc = new Case_EditVendorDetails_Controller(st);

		Test.startTest();
			
			//Test save Case errors
			cevdcc.cse.Dealer_Vendor__c = null;
			cevdcc.cse.Dealer_Contact__c = null;
			cevdcc.saveCase();

			//Test saving of new Referral Company
			cevdcc.refcomp.Name = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			cevdcc.currentABN.Name = 'Test Legal Entity ABN';
			cevdcc.currentABN.Registered_ABN__c = '123456789';
			cevdcc.saveReferralCompany();
			cevdcc.clearFormFields();
			cevdcc.refcomp.Name = 'Test Ref Comp 2';
			cevdcc.refcomp.Company_Type__c = 'Car Dealer';
			cevdcc.refcomp.Legal_Entity_Name__c = abn.Id;
			cevdcc.retrieveCurrentABN();
			cevdcc.saveReferralCompany();

			//Test saving of new Referral Company
			cevdcc.refcomp.Name = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			cevdcc.saveReferralCompany();
			cevdcc.clearFormFields();
			cevdcc.refcomp.Name = 'Test Ref Comp 2';
			cevdcc.refcomp.Company_Type__c = 'Car Dealer';
			cevdcc.saveReferralCompany();
			cevdcc.assignExistingReferralCompany();

			//Test saving of new Business Contact
			cevdcc.bcon.LastName = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			cevdcc.saveBusinessContact();
			cevdcc.clearFormFields();
			cevdcc.bcon.LastName = 'Test BContact 2';
			cevdcc.bcon.Email = 'test@testbcon.com';
			cevdcc.saveBusinessContact();

			//Test saving of new Key Person Contact
			cevdcc.bcon.LastName = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			cevdcc.saveKeyPersonContact();
			cevdcc.clearFormFields();
			cevdcc.kpcon.LastName = 'Test KPContact 2';
			cevdcc.kpcon.Email = 'test@testbcon.com';
			cevdcc.saveKeyPersonContact();

			cevdcc.cse.Subject = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			cevdcc.saveCase();
			cevdcc.cse.Subject = 'fixed';
			cevdcc.saveCase();

			ApexPages.StandardController st0 = new ApexPages.StandardController(cse);
			Case_EditVendorDetails_Controller cevdcc0 = new Case_EditVendorDetails_Controller(st0);
			cevdcc0.cse.Dealer_Vendor__c = rclist[0].Id;
			cevdcc0.cse.Dealer_Contact__c = null;
			cevdcc0.saveCase();

			ApexPages.StandardController st1 = new ApexPages.StandardController(cse);
			Case_EditVendorDetails_Controller cevdcc1 = new Case_EditVendorDetails_Controller(st1);
			cevdcc1.cse.Dealer_Vendor__c = null;
			cevdcc1.cse.Dealer_Contact__c = bconlist[0].Id;
			cevdcc1.saveCase();

			cse.Sale_Type__c = Label.Case_Sale_Type_Private;
			ApexPages.StandardController st2 = new ApexPages.StandardController(cse);
			Case_EditVendorDetails_Controller cevdcc2 = new Case_EditVendorDetails_Controller(st2);
			cevdcc2.cse.Vendor_Contact__c = null;
			cevdcc2.saveCase();

		Test.stopTest();

		//Retrieve udpated Case
		Case upcse = [Select Id, Subject, Status, Dealer_Contact__c, Dealer_Vendor__c, Vendor_Contact__c From Case];
		//Retrieve created Records
		list <Referral_Company__c> uprclist = [Select Id, Name From Referral_Company__c Where Name = 'Test Ref Comp 2'];
		list <Contact> upbconlist = [Select Id, Name From Contact Where Name = 'Test BContact 2'];
		list <Contact> upkpconlist = [Select Id, Name From Contact Where Name = 'Test KPContact 2'];

		//system.assertEquals(upcse.Dealer_Vendor__c,uprclist[0].Id); //Verify that Dealer Sale Vendor is same with created Referral Company
		//system.assertEquals(upcse.Dealer_Contact__c,upbconlist[0].Id); //Verify that Dealer Contact Person is same with created Business Contact
		//system.assertEquals(upcse.Vendor_Contact__c,null);//upkpconlist[0].Id); //Verify that Private Sale Vendor Contact is same with created Key Person Contact

	}
	
}
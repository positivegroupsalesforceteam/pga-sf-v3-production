/** 
* @FileName: NewCaseController
* @Description: controller class of NewCase lightning component
* @Copyright: Positive (c) 2019
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 0.1 4/18/19 JBACULOD Created
* 1.0 4/30/19 JBACULOD Working version
* 1.1 5/17/19 JBACULOD Added User variable on Return wrappedData
**/ 
public class NewCaseController {

    @AuraEnabled
    public static wrappedData CaseInit(string flowType, string partition, string channel, string sourceAppID){
        wrappedData wD = new wrappedData();
        system.debug('@@sourceAppID: '+sourceAppID);
        if (sourceAppID != null){
            Application__c app = [Select Id, Name, RecordTypeId, RecordType.Name, Case__r.Partner_Group__c, Case__r.Referral_Partner__c, Case__r.Introducer1__c, Case__r.Introducer1_Support__c, Case__r.Connective_Full_App_or_Lead__c From Application__c Where Id = : sourceAppID];
            wD.sourceapp = app;
        }
        wD.newCase = new Case(Status = 'New', Stage__c = 'Open', Lead_Loan_Amount__c = 0);
        wD.curUser = [Select Id, Email, FirstName, LastName, UserRoleId, UserRole.Name, Alias, ProfileId, Profile.Name From User Where Id = : UserInfo.getUserId()];
        wD.caseRecTypeWrlist = getCaseRTs(partition);
        wD.appRecTypeWrlist = getAppRTs();
        wD.pLoanReasons = getPLoanreasons();
        return wd;
    }

    public class wrappedData{
        @AuraEnabled public Case newCase {get;set;}
        @AuraEnabled public User curUser {get;set;}
        @AuraEnabled public list <recordTypeWrapper> caseRecTypeWrlist {get;set;}
        @AuraEnabled public list <recordTypeWrapper> appRecTypeWrlist {get;set;}
        @AuraEnabled public list <string> pLoanReasons {get;set;}
        @AuraEnabled public Application__c sourceapp {get;set;}
    }

    @AuraEnabled
    public static Case saveCase(Case cse){
        Savepoint sp = Database.setSavepoint();
        Case createdCase;
        try{
            //cse.Lead_Purpose__c = 'test loan';
            insert cse;
            createdCase = [Select Id, CaseNumber, Application_Name__c From Case Where Id = : cse.Id];
        }
        catch (Exception e){
            Database.rollback( sp );  
            system.debug('@@Error: '+e.getLineNumber()+e.getMessage());
            AuraHandledException ae = new AuraHandledException('Error: '+ e.getLineNumber()+e.getMessage());
            ae.setMessage('Error: '+ e.getLineNumber()+e.getMessage());
            throw ae;
        }
        return createdCase;
    }

    public class recordTypeWrapper{
        @AuraEnabled public string name {get;set;}
        @AuraEnabled public string id {get;set;}
        public recordTypeWrapper(string sname, string sid){
            name = sname;
            id = sid;
        }
    }

    private static list <string> getPLoanreasons(){
        list <string> ploanreasons = new list <string>();
        Schema.DescribeFieldResult fieldResult = Case.Personal_Loan_Reason1__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            ploanreasons.add(f.getValue()); //Display all Personal Loan Reason picklist values          
        } 
        return ploanreasons;
    } 

    private static list <recordTypeWrapper> getAppRTs(){
        list <recordTypeWrapper> appRTs = new list <recordTypeWrapper>();
        Schema.DescribeFieldResult fieldResult = Case.Application_Record_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            if (!f.getLabel().startsWith('zAPP')) appRTs.add(new recordTypeWrapper(f.getLabel(), f.getValue())); //Display all available picklist values          
        } 
        return appRTs;
    } 

    private static list <recordTypeWrapper> getCaseRTs(string partition){
        list <recordTypeWrapper> cseRTs = new List <recordTypeWrapper>();
        //Retrieving Record Types via SOQL will not be able to display available Record Types by Profile
        list <RecordTypeInfo> infos = Case.SobjectType.getDescribe().getRecordTypeInfos();
        for (RecordTypeInfo i : infos){
            if (i.isActive()){
                if (i.isAvailable()){
                    if ( (i.getName().startsWith(CommonConstants.PREFIX_N) || i.getName().startsWith(CommonConstants.PREFIX_P) || i.getName().startsWith(CommonConstants.PREFIX_Z) ) && !i.getName().contains('PHL') ){
                        if (partition == 'Positive'){
                            if (i.getName().startsWith(CommonConstants.PREFIX_P)) cseRTs.add(new recordTypeWrapper(i.getName(),i.getRecordTypeId()));
                        }
                        else if (partition == 'Nodifi'){
                            if (i.getName().startsWith(CommonConstants.PREFIX_N)) cseRTs.add(new recordTypeWrapper(i.getName(),i.getRecordTypeId()));
                        }
                    }
                }
            }
        }
        return cseRTs;
    }


}
/**
 * @File Name          : SMSServices.cls
 * @Description        : contains logic for all SMS automation
 * @Author             : Jesfer Baculod
 * @Group              : Positive Group
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 31/07/2019, 12:10:15 pm
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    31/05/2019                  JBACULOD                    Created Class
 * 1.1    31/07/2019                  JBACULOD                    Modified sending of SMS by Convserse App Task instead of Task
**/
public class SMSServices implements Queueable{

    private list <smagicinteract__Converse_App_Task__c> smsConverseAppToCreate;
    //Constructor for sending SMS without Tasks
    public SMSServices(list <smagicinteract__Converse_App_Task__c> smsConverseAppToCreate){
        this.smsConverseAppToCreate = smsConverseAppToCreate;
    }

    public void execute (QueueableContext context){
        system.debug('@@smsConverseAppToCreate:'+smsConverseAppToCreate);
        if (smsConverseAppToCreate.size() > 0) insert smsConverseAppToCreate; //Send SMS by Converse App Task
    }

    @InvocableMethod(label='Send Case SMS' description='Send SMS to Primary Contact')
    public static void SendSMS(list <string> SMSTaskKeys){ //CaseID:SMS Action Key:OwnerID:LastModifiedByID
        if (TriggerFactory.trigSet.Enable_Case_SMS_Alerts__c){
            list <smagicinteract__Converse_App_Task__c> ctsklist = new list <smagicinteract__Converse_App_Task__c>(); //send SMS by Converse App  Task
            for (String smskey : SMSTaskKeys){
                ctsklist.add ( setConverseAppTask(smskey) );
            }
            if (!ctsklist.isEmpty()){ 
                SMSServices smsser = new SMSServices(ctsklist);
                ID jobID = System.enqueueJob(smsser);                  
            }
        }
    }

    //--------------- Send SMS via Converse App Task (task/25494330) 07/31/19 --------------------//
    public static smagicinteract__Converse_App_Task__c setConverseAppTask(string smsKey){
        String[] keysplit = smskey.split(':'); //[0] RecordID ID, [1] SMS Action Key, [2] Record OwnerID, [3] Record LastModifiedByID
        system.debug('@@smskey:'+smskey);
        ID recordID = ID.valueof(keysplit[0]);
        string relsobjName = recordID.getSObjectType().getDescribe().getName();
        if (!relsobjName.endsWith('__c')) relsobjName+= '__c'; //Append __c due to custom field on Converse App Task object
        
        smagicinteract__Converse_App_Task__c ctask = new smagicinteract__Converse_App_Task__c();
        Schema.sobjectType ctaskType = ctask.getSObjectType();
        sobject sobjCTask = ctaskType.newSobject();
        sobjCTask.put(relsobjName, recordID); //Assign related Lookup field (Case/Role,etc)
        sobjCTask.put('smagicinteract__Automation_Key_Reference__c', keysplit[1]);
        //assign Case LastModifiedBy ID ELSE assign case Owner ID
        ID recownerID = keysplit[2].startsWith('00G') ? keysplit[3] : keysplit[2]; 
        sobjCTask.put('OwnerId', recownerID);
        ctask = (smagicinteract__Converse_App_Task__c) sobjCTask;
        system.debug('@@ctask:'+ctask);
        return ctask;
    }

    /*private static Task verifySMStoSendViaTask(string smsKey){
        String[] keysplit = smskey.split(':'); //[0] RecordID ID, [1] SMS Action Key, [2] Case OwnerID, [3] Case LastModifiedByID
        Task tsk = new Task(
                ActivityDate = System.Today(),
                Priority = 'Normal',
                Status = 'Completed',
                Subject = 'sms notification', //required default Subject for SMS Magic
                WhatId = keysplit[0], //Related To ID
                Description = keysplit[1] //SMS Action Key
        );
        if (keysplit[2].startsWith('00G')){
            tsk.OwnerID = keysplit[3]; //assign Case LastModifiedBy ID
        }
        else tsk.OwnerID = keysplit[2]; //assign case Owner ID
        system.debug('@@tsk:'+tsk);
        return tsk;
    } */
}
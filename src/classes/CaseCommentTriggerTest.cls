/** 
* @FileName: CaseCommentTriggerTest
* @Description: Test Class
* @Copyright: Positive (c) 2019
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/01/19 RDAVID Created
* 1.1 11/01/19 JBACULOD updated, added testUpdateCaseLatestComment
**/ 
@isTest
public class CaseCommentTriggerTest {
    @testsetup static void setup(){

        TestDataFactory.Case2FOSetupTemplate();
    }

    static testmethod void testUpdateCaseLatestComment(){
        
        Case cse = [Select Id, CaseNumber From Case limit 1];
        Test.startTest();

            Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                    Enable_Triggers__c = true,
                    Enable_Case_Comment_Update_Latest_Comm__c = true,
                    Enable_Case_Comment_Trigger__c =  true,
                    Enable_Case_Comment_Create_For_Parent__c = true);
            insert trigSet;

            CaseComment cc = new CaseComment(
                ParentId = cse.Id,
                IsPublished = true,
                CommentBody = 'test'
            );
            insert cc;

            Case updatedCse = [Select Id, Latest_Comment_Full__c, Latest_Comment__c From Case Where Id = : cse.Id];
            system.assert(updatedCse.Latest_Comment_Full__c.contains(cc.CommentBody));

            TriggerFactory.isProcessedMap = new map <Id,Boolean>();

            CaseComment cc2 = new CaseComment(
                ParentId = cse.Id,
                IsPublished = true,
                CommentBody = 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesssssssssssssss'
            );
            insert cc2;

            Case updatedCse2 = [Select Id, Latest_Comment_Full__c, Latest_Comment__c From Case Where Id = : cse.Id];
            system.assert(updatedCse2.Latest_Comment_Full__c.contains(cc2.CommentBody));

            update cc2; //Cover trigger events
            delete cc2; 



        Test.stopTest();

    }
}
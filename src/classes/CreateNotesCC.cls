/*
	@Description: controller class of CreateNotes vf page
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	8/24/17 - Created
		-	8/25/17 - Added Text Preview display on Notes table
		-	9/6/17 -  Added escapeHTMl on NoteBody to prevent Unrecognized base64 character / Internal Server Error when saving Notes
		-	11/6/17 - Added encoding of UniCode characters on Note Body
*/
public class CreateNotesCC {

	public ContentVersion note {get;set;}
	public string notebody {get;set;}
	public string objType {get;set;}
	public Id curID {get;set;} //current Object ID
	public boolean withErrors {get;set;}
	public boolean saveSuccess {get;set;}
    public boolean displaycnotes {get;set;}



	public CreateNotesCC() {
		saveSuccess = displaycnotes = withErrors = false;
		note = new ContentVersion();
        if (ApexPages.currentPage().getParameters().get('id') != null){
			if (curID == null) curID = ApexPages.currentPage().getParameters().get('id');
			objType = String.valueof(curID.getSobjectType()); 
			if (objType == 'Lead'){
				//Check if Lead is already converted
				for(Lead ld : [Select Id, IsConverted, ConvertedOpportunityId From Lead Where Id = : curId]){
					//Replace current ID with converted Opportunity Id for Notes
					if (ld.IsConverted && ld.ConvertedOpportunityId != null){ 
						curID = ld.ConvertedOpportunityId;
						objType = 'Opportunity';
					}
				}
			}
            displaycnotes = true;
        }
		system.debug('@@curID:'+curID);
            
	}

	public void reloadNote(){
		system.debug('@@curID:'+curID);
		if (curID != null){
			objType = String.valueof(curID.getSobjectType()); 
			displaycnotes = true;
			getcdllist();
		}
	}

	public pagereference saveNote(){
		Savepoint sp = Database.setSavepoint();
		saveSuccess = false;
		try{
			withErrors = false;
			if (note.Title == ''){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter Note Title'));
				withErrors = true; 
			}
			if (notebody == ''){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter Note Details'));
				withErrors = true;
			}
			if (withErrors) return null;

			noteBody = notebody.escapeUnicode();
            notebody = notebody.escapeHtml4(); //escape HTML characters
            Blob bodyBlob = Blob.valueof(notebody);
			string tempbody =  EncodingUtil.base64Encode(bodyBlob);

			//Create Note on Object
			note.PathonClient = note.Title + '.snote';
			note.VersionData = EncodingUtil.base64Decode(tempbody);

			insert note;
			system.debug('@@note:'+note);

			//Create sharing for Object (Case/Opportunity)
			ContentDocumentLink cdl = new ContentDocumentLink(
					ContentDocumentId = [Select ContentDocumentId From ContentVersion Where Id = : note.Id].ContentDocumentId,
					LinkedEntityId = curID,
					ShareType = 'I', //'V'
					Visibility = 'AllUsers'
				);
			insert cdl;
			system.debug('@@cdl:'+cdl);

			//Clean Note data
			note = new ContentVersion();
			notebody = '';

			saveSuccess = true;

		}
		catch(Exception e){
			withErrors = true;
			Database.rollback(sp);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+e.getLineNumber()));
		}
		return null;
	}

	public list <contentNoteWrapper> getcdllist(){
		list <contentNoteWrapper> cdllist = new list <contentNoteWrapper>();
        map <id, ContentVersion> cvMap = new map <id, ContentVersion>();
        if (curID != null){
            for (ContentDocumentLink cdl : [Select Id, LinkedEntityId, ContentDocument.Title, ContentDocument.LastModifiedById, ContentDocument.LastModifiedDate, ContentDocument.CreatedById, 
                        ContentDocument.CreatedBy.Name, ContentDocumentId, ShareType From ContentDocumentLink Where LinkedEntityId = : curID Order By ContentDocument.LastModifiedDate DESC]){
                if (!cvMap.containskey(cdl.ContentDocumentId)){ 
                    cvMap.put(cdl.ContentDocumentId, new ContentVersion());
                }
                contentNoteWrapper cnwr = new contentNoteWrapper(
                        new ContentVersion(), cdl
                    );
                cdllist.add(cnwr);
            }
            for (ContentVersion cv : [Select Id, TextPreview, ContentDocumentId, Title From ContentVersion Where ContentDocumentId in : cvMap.keySet()]){
                cvMap.put(cv.ContentDocumentId, cv);
            }
            for (contentNoteWrapper cnwr : cdllist){
                cnwr.cv = cvMap.get(cnwr.cdl.ContentDocumentId);
            }
        }
		system.debug('@@cdllist:'+cdllist);
		return cdllist;
	}


	public pageReference refreshNotes(){
		getcdllist();
		return null;
	}

	public class contentNoteWrapper{
		public ContentVersion cv {get;set;}
		public ContentDocumentLink cdl {get;set;}
		public contentNoteWrapper(ContentVersion cv, ContentDocumentLink cdl){
			this.cv = cv;
			this.cdl = cdl;
		}

	}

}
/** 
* @FileName: BizibleTouchpointHandler
* @Description: Trigger Handler for the bizible2__Bizible_Touchpoint__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 26/10 RDAVID Created Trigger extra/BizibleTouchpointTrigger
* 1.1 29/10 RDAVID Created Trigger extra/BizibleTouchpointTrigger - Fixed issue in Production. (System.ListException: Duplicate id in list)
* 1.2 12/02 RDAVID Added Logic to tick the Latest CTP after delete 
* 1.3 20/03/19 JBACULOD Added before insert for overwriting few TP data base from Facebook, Delacon
* 1.4 22/03/19 JBACULOD Updated mapping of FB UTM Content to Ad Group Name
* 1.5 03/04/19 JBACULOD Updated due to bug fixes
* 1.6 04/07/19 JBACULOD Added Retry and FOR UPDATE due to Record Lock Issue
* 1.7 10/10/19 JBACULOD Transfered logic of Bizible Touchpoint - Update Account First Touchpoint Date process flow to Bizible TP trigger (Enable_Bizible_Acc_First_Touchpoint__c)
* 1.8 17/10/19 RDAVID extra/task26438892-BizibleTriggerIssue - Logic to move the DMLs to BizibleTouchpointQueueable Apex
* 1.9 22/10 RDAVID extra/task26490057-PLSCaseAssignment22102019 - Fix Issue on creating Case TPs
* 2.0 25/10/2019 RDAVID tasks/26493146-BizibleTPITriggerIssue - Fix Issue on creating TPs duplicate
**/ 

public without sharing class BizibleTouchpointHandler implements ITrigger {
    private List<bizible2__Bizible_Touchpoint__c> newBizibleTouchpointList = new List<bizible2__Bizible_Touchpoint__c>();
    private Map <Id,Account> accToUpdateForTouchpointDateMap = new map <Id, Account>();
    private Set<Id> accIdSet = new Set<Id>();
    private Map <Id,Frontend_Submission__c> accFBFeMap = new map <Id,Frontend_Submission__c>();
    @testVisible private static Boolean doAsync = true;
    private Set<String> bztpIds = new Set<String>();

    public static Boolean sendAlertWhenBZTPCreated = true;
    public static Boolean sendAlertWhenAccountUpdateError = true;

    public BizibleTouchpointHandler(){
		//Initialize value of newBizibleTouchpointList
		newBizibleTouchpointList = (Trigger.IsInsert || Trigger.isUpdate) ? (List<bizible2__Bizible_Touchpoint__c>) Trigger.new : (Trigger.IsDelete) ? (List<bizible2__Bizible_Touchpoint__c>) Trigger.old : null;

        //Loop for records in Trigger.new (moved from bulkAfter) JBACU 03/20/19
        for(bizible2__Bizible_Touchpoint__c btp: newBizibleTouchpointList){
            if(btp.bizible2__Account__c != NULL){
                accIdSet.add(btp.bizible2__Account__c);
            }
        }

	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
        //JBACU 20/03/19
        if (Trigger.isInsert){
            if (TriggerFactory.trigSet.Enable_Bizible_FB_Lead_UTM__c){
                for (Frontend_Submission__c fes : [Select Id, Case__c, Case__r.Primary_Contact_Name_MC__c, FB_Utm_Campaign__c, FB_Utm_Medium__c, FB_Utm_Content__c, FB_Utm_Source__c From Frontend_Submission__c Where Case__c != null AND FB_Utm_Source__c = 'facebook' AND Case__r.Primary_Contact_Name_MC__c in : accIdSet Order By CreatedDate DESC]){
                    if (!accFBFeMap.containskey(fes.Case__r.Primary_Contact_Name_MC__c)){
                        accFBFeMap.put(fes.Case__r.Primary_Contact_Name_MC__c, fes); //Only assign to map latest FB FE Submission 
                    }
                }
            }
        }
	}
	
	public void bulkAfter(){
        
	}
		
	public void beforeInsert(SObject so){
		bizible2__Bizible_Touchpoint__c bztp = (bizible2__Bizible_Touchpoint__c)so;

        //Update Bizible TP w/ FB Leads UTM parameters
        if (accFBFeMap.size() > 0 && bztp.bizible2__Account__c != null){
            if (bztp.bizible2__Ad_Campaign_Name__c == 'FB Leads Campaign' && accFBFeMap.containskey(bztp.bizible2__Account__c)){
                bztp.bizible2__Touchpoint_Source__c = accFBFeMap.get(bztp.bizible2__Account__c).FB_Utm_Source__c;
                bztp.bizible2__Medium__c = accFBFeMap.get(bztp.bizible2__Account__c).FB_Utm_Medium__c;
                bztp.bizible2__Ad_Campaign_Name__c = accFBFeMap.get(bztp.bizible2__Account__c).FB_Utm_Campaign__c;
                bztp.bizible2__Ad_Group_Name__c = accFBFeMap.get(bztp.bizible2__Account__c).FB_Utm_Content__c;
            }
        }
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		bizible2__Bizible_Touchpoint__c bztp = (bizible2__Bizible_Touchpoint__c)so;
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}

	public void afterInsert(SObject so){
		bizible2__Bizible_Touchpoint__c bztp = (bizible2__Bizible_Touchpoint__c)so;
        bztpIds.add(String.valueOf(bztp.Id));
        //Get First Touchpoint Date and update to related Account
        if (TriggerFactory.trigSet.Enable_Bizible_Acc_First_Touchpoint__c){
            if (bztp.First_Touch__c && bztp.bizible2__Touchpoint_Date__c != null){
                if (bztp.bizible2__Account__c != null){
                    Account relBzAcc = new Account(
                        Id = bztp.bizible2__Account__c,
                        First_Touchpoint_Date__c = bztp.bizible2__Touchpoint_Date__c
                    );
                    accToUpdateForTouchpointDateMap.put(relBzAcc.Id, relBzAcc);
                }
            }
        }
	}
	
	public void afterUpdate(SObject oldSo, SObject so){
		bizible2__Bizible_Touchpoint__c bztp = (bizible2__Bizible_Touchpoint__c)so;
        bztpIds.add(String.valueOf(bztp.Id));
	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
        if(!accToUpdateForTouchpointDateMap.isEmpty()){
            try{
                Database.SaveResult[] results = Database.update(accToUpdateForTouchpointDateMap.values(),false); //Update DML Allow Partial 
                if (results != null){
                    Boolean sendCustomErrorHandlingEmailNotif = false;
                    Set<String> errLogs = new Set<String>();
                    Integer errornum = 0;
                    Integer recordsAffected = 0;
                    for (Database.SaveResult result : results) {
                        if (!result.isSuccess()) {
                            recordsAffected++;
                            sendCustomErrorHandlingEmailNotif = true;
                            Database.Error[] errs = result.getErrors();
                            Id recordID = result.getId();
                            for(Database.Error err : errs) {
                                errornum ++;
                                String emailContent = 'Record Id: '+recordID +'<br/> Status Code: ' + err.getStatusCode() + '<br/> Fields: ' + String.join(err.getFields(),',') + '<br/> Error Message: ' + err.getMessage();
                                if(!errLogs.contains(emailContent)){
                                    errLogs.add(emailContent);  
                                    errornum ++;
                                } 
                            }
                        }
                    }
                    if(sendCustomErrorHandlingEmailNotif){
                        String processName = 'Update Accounts';
                        CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                        CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Error',BizibleTouchpointHandler.class.getName() + ' - ' + processName,errLogs);
                        if(recordsAffected != 0) logWrapper.numberOfRecords = recordsAffected*errornum;
                        cerhand.logWrap = logWrapper;
                        if(sendAlertWhenAccountUpdateError) cerhand.sendMail();
                    }
                }
            } catch (Exception e) {
                System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            }
        }

        if(doAsync){ //Run Queuable Apex to Create/Update Case Touchpoints
            if(Trigger.isAfter && accIdSet.size()>0 && (Trigger.isInsert || Trigger.isUpdate || Trigger.IsDelete)){
                String processName = 'Created Bizible Touchpoints';
                CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Trigger Run',BizibleTouchpointHandler.class.getName() + ' - ' + processName,bztpIds);
                cerhand.logWrap = logWrapper;
                if(sendAlertWhenBZTPCreated){
                    cerhand.sendMail();
                } 
                ID jobID = System.enqueueJob(new BizibleTouchpointQueueable(accIdSet,Trigger.IsDelete));
            }
        } 
		System.debug('Object BizibleTP -START-');
		System.debug('BizibleTP Trigger Queries :' + Limits.getQueries() + ' / '+Limits.getLimitQueries());
		System.debug('BizibleTP Trigger Rows Queried :' + Limits.getDmlRows() + ' / '+Limits.getLimitDmlRows());
		System.debug('BizibleTP Trigger DML : ' +  Limits.getDmlStatements() + ' / '+Limits.getLimitDmlStatements());
		System.debug('Object BizibleTP -END-');
	}
}
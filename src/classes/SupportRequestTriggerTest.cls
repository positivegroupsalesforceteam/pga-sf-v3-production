/** 
* @FileName: SupportRequestTriggerTest
* @Description: Test class for Support Request Trigger 
* @Copyright: Positive (c) 2018 
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 18/10/18 RDAVID - feature/SupportRequestTrigger - Created Class
**/ 
@isTest 
public class SupportRequestTriggerTest {
    public static List<Account> accList = new List<Account>();
    public static List<Role__c> roleList = new List<Role__c>();

    @testSetup static void setupData() {
        Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
		triggerSettings.Enable_Triggers__c = true;
        triggerSettings.Enable_Support_Request_Trigger__c = true;
        triggerSettings.Enable_Support_Request_Create_CAR_SR__c = true;
        triggerSettings.Enable_Case_Log_Sectors__c = true;
        triggerSettings.Enable_Sector_Trigger__c = true;
        triggerSettings.Enable_SR_Call_Request_NVM_Routing__c = true;
        triggerSettings.Enable_Support_Request_Update_Case__c = true; // SFBAU-229
		upsert triggerSettings Trigger_Settings1__c.Id;

        General_Settings1__c gensettings = General_Settings1__c.getOrgDefaults();
        gensettings.Support_Request_Contract_RT_New__c = 'Asset - Contract Request (New)';
        gensettings.Support_Request_Settlement_RT_New__c = 'Asset - Settlement Suspension (New)';
        gensettings.Support_Request_Submission_RT_New__c = 'Asset - Submission Request (New)';
        gensettings.Support_Request_Contract_RT__c = 'Asset - Contract Request';
        gensettings.Support_Request_Settlement_RT__c = 'Asset - Settlement Suspension';
        gensettings.Support_Request_Submission_RT__c ='Asset - Submission Request';
        genSettings.Support_Request_Settlement_RT_Id__c = Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Asset - Settlement Suspension').getRecordTypeId();
        genSettings.Support_Request_CAR_RT_Id__c = Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Credit Analyst Required').getRecordTypeId();

        upsert gensettings General_Settings1__c.Id;

		//Create Test Account
		accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
        caselist[0].Suspended_Reason__c = 'test suspended Reason';
        caselist[0].Partition__c = 'Nodifi';
		Database.insert(caseList);
        
		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);

		//Create Test Role 
		roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
		Database.insert(roleList);
	}

    static testMethod void testSupportRequestTrigger() {
        Test.startTest();
            PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,CaseNumber,Partition__c,Application_Name__c, Validation_Override__c FROM Case LIMIT 1];
            System.assertEquals(caseRec.Partition__c, 'Nodifi');
            
            caseRec.Status = 'Review';
            update caseRec;
            Test.setCurrentPage(newSupportRequestPage);
        	Test.setCurrentPageReference(newSupportRequestPage); 
    
            System.currentPageReference().getParameters().put('id', caseRec.Id);
            System.currentPageReference().getParameters().put('type', 'Submission');

            NewSupportRequestController newSupportRequestCtrl = new NewSupportRequestController();
            newSupportRequestCtrl.supportRequest.Notes_to_Support__c = 'Test Notes to Support.';
            newSupportRequestCtrl.supportRequest.Notes_to_Lender__c = 'Test Notes to Lender.';
            newSupportRequestCtrl.supportRequest.I_confirm_the_SF_Record_is_accurate__c=true;
            newSupportRequestCtrl.supportRequest.Case_Number_Text__c = caseRec.CaseNumber;
            newSupportRequestCtrl.submit();
            newSupportRequestCtrl.getSupportRequestRTs();
            System.assertEquals(newSupportRequestCtrl.cseCommentReadyForSubmission.CommentBody,'Notes to Support: '+newSupportRequestCtrl.supportRequest.Notes_to_Support__c);
            newSupportRequestCtrl.caseid();
            System.assertNotEquals(newSupportRequestCtrl.supportRequest.Id,null);
            NewSupportRequestController.updateSupportRequest(newSupportRequestCtrl.supportRequest,null,null,null);
            newSupportRequestCtrl.supportRequest.Id = null;
            NewSupportRequestController.updateSupportRequest(newSupportRequestCtrl.supportRequest,null,null,null);

            List<Support_Request__c> supportList = [SELECT Id,Partition1__c,Partition__c FROM Support_Request__c];
            System.assertEquals(supportList[0].Partition1__c, 'Nodifi');
            supportList[0].Case_Number_Text__c = caseRec.CaseNumber;
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            update supportList;
            supportList = [SELECT Id,Case_Number__c FROM Support_Request__c WHERE Id =: supportList[0].Id];
            System.assertEquals(supportList[0].Case_Number__c,caseRec.Id);
            System.assertNotEquals(supportList.size(),0);
            supportList[0].Status__c = 'Closed';
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            update supportList;
            List<Sector__c> slaSector = [SELECT Id FROM Sector__c WHERE Sector1__c LIKE '%SLA%'];
            //System.assertNotEquals(slaSector.size(), 0);
            delete supportList;
        Test.stopTest();
    }

    static testMethod void testSupportRequestTriggerTwo() {
        ID pId_sysadmin = [Select Id, Name From Profile Where Name = : Label.Profile_Name_System_Administrator ].Id;
        User sysadminUsr = new User(
                UserName = 'sysadmin@casetriggertest.com.uat',
                FirstName = 'Courtney',
                LastName = 'TestSysAdminCASE',
                Email = 'sysadmin@casetriggertest.com',
                EmailEncodingKey = 'UTF-8',
                TimeZoneSidKey = 'GMT',
                Alias = 'tSACSE',
                LocaleSidKey = 'en_AU',
                LanguageLocaleKey = 'en_US',
                ProfileId =  pId_sysadmin
            );
        insert sysadminUsr;
        system.runAs(sysadminUsr){
            Test.startTest();
                PageReference newSupportRequestPage = Page.NewSupportRequestPage;
                Case caseRec = [SELECT Id,CaseNumber,Application_Name__c,Settlement_Officer__c, OwnerId, Submitter__c, Status, Stage__c, RecordType.Name,On_Hold__c,Stage_Prior__c,Prior_Status__c, Validation_Override__c FROM Case LIMIT 1];
                caseRec.Status = 'Review';
                update caseRec;
                System.debug('@@@caseRec RTNAme: '+caseRec.RecordType.Name);
                System.assert(caseRec.RecordType.Name.containsIgnoreCase('NOD:'));
                Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName();

                General_Settings1__c gensettings = General_Settings1__c.getOrgDefaults();
                
                for(String srRTName : rtMapByName.keySet()){
                    if(srRTName == gensettings.Support_Request_Contract_RT__c) gensettings.Support_Request_Contract_RT_Id__c = rtMapByName.get(srRTName).getRecordTypeId();
                    if(srRTName == gensettings.Support_Request_Settlement_RT__c) gensettings.Support_Request_Settlement_RT_Id__c = rtMapByName.get(srRTName).getRecordTypeId();
                    if(srRTName == gensettings.Support_Request_Submission_RT__c) gensettings.Support_Request_Submission_RT_Id__c = rtMapByName.get(srRTName).getRecordTypeId();
                }

                upsert gensettings General_Settings1__c.Id;
                Map<Id,Schema.RecordTypeInfo> rtMapById = Schema.SObjectType.Support_Request__c.getRecordTypeInfosById();
                Id settlementRT = rtMapByName.get('Asset - Settlement Suspension').getRecordTypeId();
                Support_Request__c submissionR = new Support_Request__c(RecordTypeId = rtMapByName.get('Asset - Submission Request (New)').getRecordTypeId(),
                                                                        Stage__c = 'New',
                                                                        Status__c = 'New',
                                                                        Case_Number__c = caseRec.Id);
                Support_Request__c contractR = new Support_Request__c(RecordTypeId = rtMapByName.get('Asset - Contract Request (New)').getRecordTypeId(),
                                                                        Stage__c = 'New',
                                                                        Status__c = 'New',
                                                                        Case_Number__c = caseRec.Id);
                Support_Request__c settlementR = new Support_Request__c(RecordTypeId = rtMapByName.get('Asset - Settlement Suspension (New)').getRecordTypeId(),
                                                                        Stage__c = 'New',
                                                                        Status__c = 'New',
                                                                        Case_Number__c = caseRec.Id);

                List<Support_Request__c> srList = new List<Support_Request__c>{submissionR,contractR,settlementR};
                TriggerFactory.isProcessedMap = new map <Id, boolean>();
                Database.insert(srList);
                TriggerFactory.isProcessedMap = new map <Id, boolean>();
                for(Support_Request__c sr : srList){
                    sr.Status__c = 'In Progress';
                    sr.Stage__c = 'In Progress';
                }
                Database.update(srList);
                TriggerFactory.isProcessedMap = new map <Id, boolean>();
                
                srList[0].Status__c = 'Closed';
                srList[0].Stage__c = 'Failed';
                srList[1].Status__c = 'Closed';
                srList[1].Stage__c = 'Failed';
                
                Database.update(srList);
                TriggerFactory.isProcessedMap = new map <Id, boolean>();
                for(Support_Request__c sr : srList){
                    sr.Status__c = 'Closed';
                    sr.Stage__c = 'Actioned';
                }
                Database.update(srList);
                srList[0].Stage__c = 'Actioned';
                SupportRequestGateway.updateCase('Update', caseRec, srList[0], gensettings.Support_Request_Submission_RT__c, gensettings);
                srList[0].Support_Sub_Type__c = 'Final Modification';
                SupportRequestGateway.updateCase('Update', caseRec, srList[0], gensettings.Support_Request_Submission_RT__c, gensettings);
                srList[0].Stage__c = 'Failed';
                SupportRequestGateway.updateCase('Update', caseRec, srList[0], gensettings.Support_Request_Submission_RT__c, gensettings);

                srList[1].Stage__c = 'Actioned';
                SupportRequestGateway.updateCase('Update', caseRec, srList[1], gensettings.Support_Request_Contract_RT__c, gensettings);
                srList[1].Stage__c = 'Failed';
                SupportRequestGateway.updateCase('Update', caseRec, srList[1], gensettings.Support_Request_Contract_RT__c, gensettings);

                srList[1].Stage__c = 'Actioned';
                SupportRequestGateway.updateCase('Update', caseRec, srList[1], gensettings.Support_Request_Settlement_RT__c, gensettings);

                srList[0].Stage__c = 'Actioned';
                srList[0].Support_Sub_Type__c = 'Final Modification';
                SupportRequestGateway.updateCase('Insert', caseRec, srList[0], gensettings.Support_Request_Submission_RT__c, gensettings);
            Test.stopTest();
        }
    }

    static testmethod void testCARSupportRequest(){
        Case caseRec = [SELECT Id,CaseNumber, Suspended_Reason__c FROM Case LIMIT 1];

        General_Settings1__c gensettings = General_Settings1__c.getOrgDefaults();
        system.assertEquals(genSettings.Support_Request_Settlement_RT_Id__c, Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Asset - Settlement Suspension').getRecordTypeId());

        Support_Request__c srSus = new Support_Request__c(
            RecordTypeId = genSettings.Support_Request_Settlement_RT_Id__c,
            Status__c = 'New',
            Stage__c = 'New',
            Partition__c = 'Positive',
            Case_Number__c = caseRec.Id
        );
        insert srSus;

        Test.startTest();

            TriggerFactory.isProcessedMap = new map <Id,Boolean>();

            srSus.Status__c = 'In Progress';
            srSus.Stage__c = 'Deferred to Credit';
            update srSus;

            list <Support_Request__c> srCarlist = [Select Id, RecordType.Name, Support_Sub_Type__c, Related_Support_Request__c From Support_Request__c Where Related_Support_Request__c = : srSus.Id];
            system.assertEquals(srCarlist.size(),1);
            system.assertEquals(srCarlist[0].Support_Sub_Type__c,'Deferred to Credit');

        Test.stopTest();


    }

    static testMethod void testSupportRequestCallRequest() {
        Test.startTest();
            // PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,CaseNumber,Partition__c,Application_Name__c FROM Case LIMIT 1];
            System.assertEquals(caseRec.Partition__c, 'Nodifi');
            List<Support_Request__c> srList = TestDataFactory.createSRList(3, 'Communication - NOD - Request a Call', caseRec.Id, caseRec.Partition__c);
            srList[0].Urgency__c = 'Urgent';
            srList[0].Call_Request_Reason__c = 'I am returning a missed call';
            srList[0].Additional_Comments__c = 'Test Comments1';
            srList[0].Callback_Number__c = '0419812852';
            srList[1].Urgency__c = 'Before EOD';
            srList[1].Call_Request_Reason__c = 'I am returning a missed call';
            srList[1].Additional_Comments__c = 'Test Comments2';
            srList[1].Callback_Number__c = '0419812852';
            srList[2].Urgency__c = 'Next 2 Hours';
            srList[2].Call_Request_Reason__c = 'I am returning a missed call';
            srList[2].Additional_Comments__c = 'Test Comments3';
            srList[2].Callback_Number__c = '0419812852';
            Database.insert(srList);

        Test.stopTest();
    }

    static testMethod void testSupportRequestCallRequestThree() {
        Test.startTest();
            // PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,CaseNumber,Partition__c,Application_Name__c FROM Case LIMIT 1];
            System.assertEquals(caseRec.Partition__c, 'Nodifi');
            List<Support_Request__c> srList = TestDataFactory.createSRList(3, 'Communication - NOD - Request a Call', caseRec.Id, caseRec.Partition__c);
            srList[0].Urgency__c = 'Urgent';
            srList[0].Call_Request_Reason__c = 'I am returning a missed call';
            srList[0].Additional_Comments__c = 'Test Comments1';
            srList[0].Callback_Number__c = '0419812852';
            srList[1].Urgency__c = 'Before EOD';
            srList[1].Call_Request_Reason__c = 'I am returning a missed call';
            srList[1].Additional_Comments__c = 'Test Comments2';
            srList[1].Callback_Number__c = '0419812852';
            srList[2].Urgency__c = 'Next 2 Hours';
            srList[2].Call_Request_Reason__c = 'I am returning a missed call';
            srList[2].Additional_Comments__c = 'Test Comments3';
            srList[2].Callback_Number__c = '0419812852';
            SupportRequestGateway.currentLocalTime = Datetime.now().Time().addHours(-3);
            Database.insert(srList);

        Test.stopTest();
    }

    static testMethod void testSupportRequestCallRequestTwo() {
        Test.startTest();
            // PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,CaseNumber,Partition__c,Application_Name__c FROM Case LIMIT 1];
            System.assertEquals(caseRec.Partition__c, 'Nodifi');
            List<Support_Request__c> srList = TestDataFactory.createSRList(3, 'Communication - NOD - Request a Call', caseRec.Id, caseRec.Partition__c);
            srList[0].Urgency__c = 'Urgent';
            srList[0].Call_Request_Reason__c = 'I am returning a missed call';
            srList[0].Additional_Comments__c = 'Test Comments1';
            srList[0].Callback_Number__c = '0419812852';
            srList[1].Urgency__c = 'Before EOD';
            srList[1].Call_Request_Reason__c = 'I am returning a missed call';
            srList[1].Additional_Comments__c = 'Test Comments2';
            srList[1].Callback_Number__c = '0419812852';
            srList[2].Urgency__c = 'Next 2 Hours';
            srList[2].Call_Request_Reason__c = 'I am returning a missed call';
            srList[2].Additional_Comments__c = 'Test Comments3';
            srList[2].Callback_Number__c = '0419812852';
            SupportRequestGateway.currentLocalTime = Datetime.now().Time().addHours(-1);
            Database.insert(srList);

        Test.stopTest();
    }

    static testMethod void testSupportRequestCallRequestFour() {
        Test.startTest();
            // PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,CaseNumber,Partition__c,Application_Name__c FROM Case LIMIT 1];
            System.assertEquals(caseRec.Partition__c, 'Nodifi');
            List<Support_Request__c> srList = TestDataFactory.createSRList(3, 'Communication - NOD - Request a Call', caseRec.Id, caseRec.Partition__c);
            srList[0].Urgency__c = 'Urgent';
            srList[0].Call_Request_Reason__c = 'I am returning a missed call';
            srList[0].Additional_Comments__c = 'Test Comments1';
            srList[0].Callback_Number__c = '0419812852';
            srList[1].Urgency__c = 'Before EOD';
            srList[1].Call_Request_Reason__c = 'I am returning a missed call';
            srList[1].Additional_Comments__c = 'Test Comments2';
            srList[1].Callback_Number__c = '0419812852';
            srList[2].Urgency__c = 'Next 2 Hours';
            srList[2].Call_Request_Reason__c = 'I am returning a missed call';
            srList[2].Additional_Comments__c = 'Test Comments3';
            srList[2].Callback_Number__c = '0419812852';
            SupportRequestGateway.currentLocalTime = Datetime.now().Time().addHours(+4);
            Database.insert(srList);

        Test.stopTest();
    }

    static testMethod void testSupportRequestCallRequestFive() {
        Test.startTest();
            // PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,CaseNumber,Partition__c,Application_Name__c FROM Case LIMIT 1];
            System.assertEquals(caseRec.Partition__c, 'Nodifi');
            List<Support_Request__c> srList = TestDataFactory.createSRList(3, 'Communication - NOD - Request a Call', caseRec.Id, caseRec.Partition__c);
            srList[0].Urgency__c = 'Urgent';
            srList[0].Call_Request_Reason__c = 'I am returning a missed call';
            srList[0].Additional_Comments__c = 'Test Comments1';
            srList[0].Callback_Number__c = '0419812852';
            srList[1].Urgency__c = 'Before EOD';
            srList[1].Call_Request_Reason__c = 'I am returning a missed call';
            srList[1].Additional_Comments__c = 'Test Comments2';
            srList[1].Callback_Number__c = '0419812852';
            srList[2].Urgency__c = 'Next 2 Hours';
            srList[2].Call_Request_Reason__c = 'I am returning a missed call';
            srList[2].Additional_Comments__c = 'Test Comments3';
            srList[2].Callback_Number__c = '0419812852';
            SupportRequestGateway.currentLocalTime = Datetime.now().Time().addHours(-5);
            Database.insert(srList);

        Test.stopTest();
    }


}
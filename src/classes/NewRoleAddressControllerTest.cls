@isTest
public class NewRoleAddressControllerTest {
    
    public static List<Account> accList = new List<Account>();
    public static List<Role__c> roleList = new List<Role__c>();
    
	@testSetup static void setupData() {
		//Create Test Account
		accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);
        
		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);

		//Create Test Role 
		roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
		Database.insert(roleList);
        
        //Create test data for Location
        Location__c loc = new Location__c(
            Name = 'Location test',
            Country__c = 'Australia',
            Postcode__c = '132455',
            State__c = 'Australian Capital Territory',
            State_Acr__c = 'ACT'
        );
        insert loc;
        
        //Create test data for Address
        Address__c address = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
        address.Location__c = loc.Id;
        Database.insert(address);
        
        Id PCodeAddRT = Schema.SObjectType.Role_Address__c.getRecordTypeInfosByName().get('Postcode Only').getRecordTypeId();	
		Role_Address__c roleAddress = TestDataFactory.createGenericRoleAddress(accList[0].Id, true, roleList[0].Id, address.Id);
		roleaddress.RecordTypeId = PCodeAddRT;
        Database.insert(roleAddress);
	}
    
    static testMethod void testNewRoleAddressVF() {
    	Test.startTest();
        	roleList =[SELECT Id FROM Role__c];
			Location__c loc = [Select Id, Suburb_Town__c, Postcode__c, State__c, State_ACR__c, Country__c From Location__c limit 1];

            PageReference pref = Page.NewRoleAddress;
            Test.setCurrentPage(pref);
        	Test.setCurrentPageReference(pref); 
			System.currentPageReference().getParameters().put('id', roleList[0].Id);
        
        	NewRoleAddressController newRoleAddressCtrl = new NewRoleAddressController();
			newRoleAddressCtrl.curRoleAddrID = rolelist[0].Id;
			newRoleAddressCtrl.editRoleAddress();
			newRoleAddressCtrl.transformtoFullAddress();

        	newRoleAddressCtrl.raddrWR.raddr.Start_Date__c = NULL;
        	newRoleAddressCtrl.saveRoleAddress();
        	newRoleAddressCtrl.showNewAddress();
        	//newRoleAddressCtrl.addr = [SELECT Id, Location__c, Street_Type__c, Street_Name__c, Street_Number__c FROM Address__c LIMIT 1];
        	newRoleAddressCtrl.saveAssignNewAddress();
			newRoleAddressCtrl.addr.Street_Type__c = 'test';
			newRoleAddressCtrl.addr.Street_Name__c = 'test';
			newRoleAddressCtrl.addr.Street_Number__c = 'test';
			newRoleAddressCtrl.addr.Location__c = loc.Id;
			newRoleAddressCtrl.populateAddresswithLocation();
			newRoleAddressCtrl.saveAssignNewAddress();
        	newRoleAddressCtrl.validateAddress();
			newRoleAddressCtrl.raddrWR.raddr.Address__c = newRoleAddressCtrl.addr.Id;
        	newRoleAddressCtrl.raddrWR.raddr.Start_Date__c = System.today();
        	newRoleAddressCtrl.saveRoleAddress();

			newRoleAddressCtrl.newRoleAddress();
			newRoleAddressCtrl.raCancel();

			newRoleAddressCtrl.raddrWR.isChangedToFull = true;
			newRoleAddressCtrl.popUpCancel();
    	Test.stopTest();
    }
}
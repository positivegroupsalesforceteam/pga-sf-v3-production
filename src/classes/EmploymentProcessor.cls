/*
Employemt details Mapping to Loankit Integration.
*/
global class EmploymentProcessor extends DefaultProcessor {
    Map<String,String> abbreviations = new Map<String,String>{'Australian Capital Territory'=>'ACT','New South Wales'=>'NSW','Northern Territory'=>'NT','Queensland'=>'QLD','South Australia'=>'SA','Tasmania'=>'TAS',
                                        'Victoria'=>'VIC','Western Australia'=>'WA','Other (Outside Australia)'=>'Other'};
    public override boolean process() {
        if(!super.process()) {
            return false;
        }
        List<LKRecord> emplRecList = XMLMappingAdapter.lkRecordsMap.get('Employment');
        XMLMapping mappObj = XMLMappingAdapter.xmlNodesMap.get('Address');
        if(emplRecList != null && emplRecList.size() > 0) {
            for(LKRecord emplRec : emplRecList) {
                String addressId;
                Map<String,String> requestBody = new Map<String,String>();
         //       String add = String.valueOf(emplRec.lkValuesMap.get('Employer_address__c'));
                String streetNumber = String.valueOf(emplRec.lkValuesMap.get('Street_Number__c'));
                String streetName = String.valueOf(emplRec.lkValuesMap.get('Street_Name__c'));
                String streetType = String.valueOf(emplRec.lkValuesMap.get('Street_Type__c'));
                String suburb = String.valueOf(emplRec.lkValuesMap.get('Suburb__c'));
                String state = String.valueOf(emplRec.lkValuesMap.get('State__c'));
                
                if(state != null && state!='null'){
                System.debug('Abbreviation values =====================' + abbreviations.values());        
                       requestBody.put('state',abbreviations.get(state));
                   }
                System.debug('Employer State =========================' + state);
                
                String postcode = String.valueOf(emplRec.lkValuesMap.get('Postcode__c'));
                String country = String.valueOF(emplRec.lkValuesMap.get('Country__c'));
                String applicantSfId = (String)emplRec.lkValuesMap.get('Applicant_2_Mapping__c');
                String applicantLKId = null;
                if(applicantSfId != null) {
                    applicantLKId = LKintegration.coBorrIdMap.get(applicantSfId);
                    emplRec.applicantLKLink = applicantLKId;
                }
        //        requestBody.put('street',add);
                requestBody.put('street_number',streetNumber);
                requestBody.put('street',streetName);
                requestBody.put('street_type',streetType);
                requestBody.put('city',suburb);
                requestBody.put('postcode',postcode);
                requestBody.put('country',country);
                
                requestBody.put('address_type','PreSettlement Mailing');
                LKIntegration.putAppId(requestBody,String.valueOf(LKIntegration.mapGlobal.get(LKConstant.APPLICATION_ID)));
                LKIntegration.putCommonData(requestBody);
                if(mappObj != null) {
                    String url = mappObj.getEndPointUrl(false,null);
                    if(applicantLKId != null) {
                        url = url+applicantLKId+'?serializer=JSON';
                    }else {
                        url = url+LKIntegration.mapGlobal.get('contact_id')+'?serializer=JSON';
                    }
                    String requestMethod = mappObj.getReqMethod(false);
                    String jsonString = LKIntegration.getJsonString(requestBody);
                    HttpResponse resp = LKIntegration.sendRequest(jsonString,url,requestMethod);
                    if(resp!=null) {
                        String respString = resp.getBody();
                        Map<String,String> mapResponse = LKIntegration.convertToMap(respString);
                        if(!mapResponse.containsKey('error')) {
                            addressId = mapResponse.get('address_id');
                            emplRec.lkObjectId2 = addressId;
                            emplRec.fieldToUpdate2 = 'LK_Employment_Addr_ID__c';
                        }

                    }
                }
                System.debug('employer name =================== '+emplRec.lkValuesMap.get('employer_name'));
                String emplType = (String)emplRec.lkValuesMap.get('employment_type');
                if(emplType != null) {
                    emplType = emplType.replaceAll('\\s+','');
                    emplRec.lkValuesMap.put('employment_type',emplType);
                }
                String primaryEmpl = (String)emplRec.lkValuesMap.get('primary_employment');
                System.debug('primary employment ======================= '+primaryEmpl);
                if(primaryEmpl != null) {
                    if(primaryEmpl.equalsIgnoreCase('Yes')) {
                        emplRec.lkValuesMap.put('prior_employment','No');
                    }else if(primaryEmpl.equalsIgnoreCase('No')) {
                        emplRec.lkValuesMap.put('prior_employment','Yes');
                    }
                }
                String empStatus = (String)emplRec.lkValuesMap.get('employment_status');
                if(empStatus != null) {
                    empStatus = empStatus.deleteWhitespace();
                    if(empStatus.equalsIgnoreCase('Self-Employed')) {
                        empStatus = 'SelfEmployed';
                    }
                    emplRec.lkValuesMap.put('employment_status',empStatus);
                }
                if(addressId != null) {
                    emplRec.lkValuesMap.put('address_id',addressId);
                }
            }
        }
        return true;
    }
}
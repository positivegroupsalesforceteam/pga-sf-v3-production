/*
    @author: Jesfer Baculod - Positive Group
    @history: 01/14/19 - Created
              01/15/19 - Updated querylocator
    @description: class for updating 'Owner Assigned' POS Cases to set stage back to Attempted Contact
*/
global class CaseOwnerAssignedBatch implements Database.Batchable<sObject>{

    global CaseOwnerAssignedBatch(){}

    global Database.QueryLocator start(Database.BatchableContext bc) {
		string query = 'Select Id, CaseNumber, Status, Stage__c From Case Where Partition__c = \'Positive\' AND RecordType.Name like \'%POS:%\' AND Stage__c = \'Owner Assigned\''; // AND Id = \'500p0000005RDd5AAG\'';
		return Database.getQueryLocator(query);
    }

    //execute method for batch
    global void execute(Database.BatchableContext bc, List<Case> scope) {
        list <Case> cselist = new list <Case>();
		for (Case cse : scope){
            cse.Status = 'New';
            cse.Stage__c = 'Attempted Contact';
            cselist.add(cse);
        }
        system.debug('@@cselist:'+cselist);
        if (cselist.size() > 0) database.update(cselist,false); //Allows partial succes
    }

    global void finish(Database.BatchableContext bc) {

    }

}
/** 
* @FileName: NewPartnerControllerTest
* @Description: Test Class for New Partner Lightning Apex controller
* @Copyright: Positive (c) 2019 
* @author: Rexie Aaron David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 12/06/19 RDAVID Created Class
* 1.1 14/09/19 JBACULOD
* 1.2 22/01/2020 ICRUZ SFBAU-37 - Created a test for linkRelatedToPartner Method
**/ 
@isTest
public class NewPartnerControllerTest {
    @testSetup static void setupData() {

        //Create test data for Nodifi Partner Lead
        Lead nodlead = new Lead(
            LastName = 'nodleadtestL',
            RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Nodifi Partner').getRecordTypeId(),
            Status = 'New',
            Stage__c = 'Open'
        );
        insert nodlead;

        // Create ABN Record that will be linked to Partner - SFBAU-37
        ABN__c abnRec = new ABN__c(Name = 'Sample ABN Name',
                                   Registered_ABN__c = '12345'
                                  );
        insert abnRec;

        // Create Location Record that is required in creating address - SFBAU-37
        Location__c locationRec = new Location__c(Name = 'Mandaluyong City',
                                                  Postcode__c = '1550',
                                                  Suburb_Town__c = null,
                                                  State_Acr__c = null,
                                                  State__c = null,
                                                  Country__c = 'Philippines'
                                                 );
        insert locationRec;

        // Create Address Record that will be linked to Partner - SFBAU-37
        Address__c addressRec = new Address__c(Location__c = locationRec.Id,
                                               Street_Number__c = '5/F',
                                               Street_Name__c = 'St. Francis Square Corporate Center',
                                               Street_Type__c = 'Drive'
                                              );

        insert addressRec;

    }

    static testmethod void testNewPartner(){
        Test.startTest();
        NewPartnerController.getNewPartnerValidRTMap();
        NewPartnerController.getForm(null, 'Partner__c', 'Dealership Group', null);
        List<Address__c> addressList = NewPartnerController.getAddress('ADL');
        System.assertEquals(addressList.size(),0);
        Test.stopTest();
    }

    static testmethod void testLeadtoPartner(){
        //Retrieve test data for Lead
        Lead nodlead = [Select Id, Status, Stage__c From Lead];
        
        //Create test notes for Lead
        ContentVersion lnote = new ContentVersion(
            Title = 'Test Note for Lead'
        );
        Blob bodyBlob = Blob.valueof('Test Note Body');
		string tempbody =  EncodingUtil.base64Encode(bodyBlob);
        lnote.PathonClient = note.Title + '.snote';
        lnote.VersionData = EncodingUtil.base64Decode(tempbody);
        insert lnote;
        ContentDocumentLink cdl = new ContentDocumentLink(
                ContentDocumentId = [Select ContentDocumentId From ContentVersion Where Id = : lnote.Id].ContentDocumentId,
                LinkedEntityId = nodlead.Id,
                ShareType = 'I',
                Visibility = 'AllUsers'
            );
        insert cdl;

        //Create test Partner for testing Lead to Partner
        Partner__c partner = new Partner__c(
            Name = 'Test LeadtoPartner',
            RecordTypeId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId()
        );
        insert partner;

        Test.startTest();
            NewPartnerController.getNodifiPartnerLead(nodlead.Id);
            NewPartnerController.updateNodifiPartnerLeadToWon(nodlead, partner.Id);
        Test.stopTest();

        //Verify Lead notes got copied to Partner
        //list <ContentDocumentLink> cdlinks = [Select Id, LinkedEntityId From ContentDocumentLink Where Id = : partner.Id];
        //system.assert(cdlinks.size() > 0);

    }

    /**
        * Method Name: linkABNToPartner
        * Description: Update Legal Entity Name of a Partner Record
        * JIRA: SFBAU-37
    **/

    @isTest
    private static void linkABNToPartner() {

        // Create test Partner
        Partner__c partnerRec = new Partner__c(Name = 'Test LeadtoPartner',
                                               RecordTypeId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId()
                                              );

        insert partnerRec;

        
        // SELECT ABN Record
        ABN__c abnRec = [SELECT Id
                         FROM ABN__c
                         LIMIT 1
                        ];

        Test.startTest();        
        NewPartnerController.linkRelatedToPartner(partnerRec.Id, abnRec.Id, 'legal');
        Test.stopTest();

        Partner__c updatedPartnerRec = [SELECT Legal_Entity_Name__c
                                        FROM Partner__c
                                        WHERE (Id = :partnerRec.Id)
                                       ];

        System.assert(updatedPartnerRec != null);
        System.assert(updatedPartnerRec.Legal_Entity_Name__c.equals(abnRec.Id));

    }

    /**
        * Method Name: linkAddressToPartner
        * Description: Update Full Address of a Partner Record
        * JIRA: SFBAU-37
    **/

    @isTest
    private static void linkAddressToPartner() {

        // Create test Partner
        Partner__c partnerRec = new Partner__c(Name = 'Test LeadtoPartner',
                                               RecordTypeId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId()
                                              );

        insert partnerRec;

        
        // SELECT Address Record
        Address__c addressRec = [SELECT Id
                                 FROM Address__c
                                 LIMIT 1
                                ];

        Test.startTest();        
        NewPartnerController.linkRelatedToPartner(partnerRec.Id, addressRec.Id, 'address');
        Test.stopTest();

        Partner__c updatedPartnerRec = [SELECT Partner_Address__c
                                        FROM Partner__c
                                        WHERE (Id = :partnerRec.Id)
                                       ];

        System.assert(updatedPartnerRec != null);
        System.assert(updatedPartnerRec.Partner_Address__c.equals(addressRec.Id));

    }

    /**
        * Method Name: getCurrentUserTest
        * Description: Getting Current User
        * JIRA: SFBAU-37
    **/

    @isTest
    private static void getCurrentUserTest() {

        Test.startTest();        
        User currentUser = NewPartnerController.getCurrentUser();
        Test.stopTest();

        System.assert(currentUser != null);

    }

    /**
        * Method Name: getABNList
        * Description: Filter ABN List
        * JIRA: SFBAU-37
    **/

    @isTest
    private static void getABNList() {

        Test.startTest();        
        List<ABN__c> abnList = NewPartnerController.getABNs('Sample');
        Test.stopTest();

        System.assert(abnList.size() > 0);
        System.assert(abnList != null);

    }

    /**
        * Method Name: getFieldsWithRecordID
        * Description: Validate if getFields method has a Record ID
        * JIRA: SFBAU-37
    **/

    @isTest
    private static void getFieldsWithRecordID() {

        // Create test Partner
        Partner__c partnerRec = new Partner__c(Name = 'Test LeadtoPartner',
                                               RecordTypeId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId()
                                              );

        insert partnerRec;

        Test.startTest();        
        Map<String, List<NewPartnerController.FieldSetForm>> mapField = NewPartnerController.getForm(partnerRec.Id, 'Partner__c', 'Finance Aggregator', null);
        Test.stopTest();

        System.assert(mapField != null);
        System.assert(mapField.size() > 0);

    }

}
public with sharing class SimpleXmlStreamWriter  {

	public XmlStreamWriter writer { get; set; }

	public SimpleXmlStreamWriter(XmlStreamWriter writer)  {
		this.writer = writer;
	}

	public void writeTag(String tagName, String value) {
		if (value == null) {
			return;
		}
		writer.writeStartElement(null, tagName, null);
			writer.writeCharacters(value);
		writer.writeEndElement();
	}

	public void writeTag(String tagName, Date value) {
		writeTag(tagName,value.format());
	}

	public void writeAttribute(String attributeName, String attributeValue){
		if (attributeValue != null) {
			writer.writeAttribute(null, null, attributeName, attributeValue);
		}
	}

	
}
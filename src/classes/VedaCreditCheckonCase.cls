/* 

Author:     Steven Herod, Feb 2014
            Raoul Lake, May 2017
Purpose:    Backs the Veda Search page retrieving the base information used to prepopulate the search screen
            All Veda API work is deferred to VedaAPI.cls
History:    09/11/2017 - Updated, added validation for Credit Check to allow check just once. (Jesfer Baculod - Positive Group)
                       - set redirecting of page to VedaIndividualPagePrintable once Attachment gets created
            09/20/2017 - Added System Administrator OS on validation when generating CreditReport

*/
public class VedaCreditCheckonCase {

    public boolean saveResultToContact { get; set;}
    public VedaDTO.IndividualResponse response { get; set; }
    public VedaDTO.IndividualRequest request { get; set; }
    public boolean errorMsg;
    public String errorTextMsg { get; set;}
    public Contact contact { get; set;}
    public Lead leadObj { get; set;}
    public Account acctObj { get; set;}
    public Case caseobj  { get; set;}
    public String sObjectType { get; set;}
    private User curUsr {get;set;}
    public string testresponse {get;set;}
    public Id idOfObjToAttach { get; set;}
    public Id attachmentId { get; set;}

    public VedaCreditCheckonCase(ApexPages.StandardController std) {
        
        errorMsg = false;
        errorTextMsg = '';

        //Retrieve current user details
        Id usrCurID = UserInfo.getUserId();
        curUsr = [Select Id, Name, Profile.Name From User Where Id = : usrCurID];

        //Retreiving the base contact details
        sObjectType = String.valueOf(std.getRecord().getSObjectType());
        VedaDTO.Individual individual;
        idOfObjToAttach = std.getRecord().id;   
        
        if(sObjectType  == 'Account'){          
            //if(CustomSettingUtil.getVedaConfigBooleanValue('PersonAccountEnabled')){
              acctObj = new Account();
               acctObj = [Select a.Website, a.Type, a.State__c, a.ShippingStreet, a.ShippingState, a.ShippingPostalCode, a.ShippingCountry, 
                 a.ShippingCity,  a.Salutation, a.RecordTypeId, a.Postcode__c, a.Phone, a.PersonMobilePhone, a.PersonLastCUUpdateDate, 
                 a.PersonLastCURequestDate, a.PersonEmailBouncedReason, a.PersonEmailBouncedDate, a.PersonEmail, a.PersonContactId, a.LastName, 
                 a.IsPersonAccount, a.Id, a.Gender__c, a.FirstName, a.Fax, 
                 a.Drivers_Licence_Number__c,  a.Description,  a.Date_of_Birth__c, a.Customer_Comments__c, 
                 a.Current_Street_Number__c, a.Current_Street_Name__c, a.Current_Street_Type__c, a.Current_State__c, a.Current_PostCode__c, a.Current_Country__c, 
                 a.Current_Suburb__c, a.Account_PersonMobilePhone__c From Account a where a.id = :std.getRecord().id];
            //}
            if(acctObj != null){ 
            	individual = new VedaDTO.Individual(acctObj);           
            }
            
        }else if(sObjectType  == 'Case'){            
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This is apex:pageMessages'));
            //return new PageReference('/'+idOfObjToAttach);    
            caseobj = [Select o.casenumber, o.Id,a.State__c, a.RecordTypeId,a.Phone, a.LastName, 
                       a.Id, a.Gender__c,a.FirstName, a.Lastest_Comment__c, a.Street_Number__c, a.Street_Name__c, 
                       a.Street_Type__c, a.Postcode__c, a.Country__c,a.Suburb__c,a.Drivers_License_Number__c,a.Birthdate
                        From case o,o.Contact a where o.id = :std.getRecord().id];
           
            
            individual = new VedaDTO.Individual(caseobj);    
          }
        else if(sObjectType == 'Contact'){
            Try{
            
            contact = [Select  a.State__c, a.RecordTypeId,a.Phone, a.LastName, 
                       a.Id, a.Gender__c,a.FirstName, a.Lastest_Comment__c, a.Street_Number__c, a.Street_Name__c, 
                       a.Street_Type__c, a.Postcode__c, a.Country__c,a.Suburb__c,a.Drivers_License_Number__c,a.Birthdate
                 		From Contact a where a.id = :std.getRecord().id];
            individual = new VedaDTO.Individual(contact); 
        }catch(system.exception ex){
            errorTextMsg = ex.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
         
        }
        }
        
        request = new VedaDTO.IndividualRequest();
        request = request.newAuthorisedAgentConsumerPlusRequest(CustomSettingUtil.getVedaConfigValue('AccessCode'),
                                                                CustomSettingUtil.getVedaConfigValue('AccessPassword'),
                                                                CustomSettingUtil.getVedaConfigValue('ClientReference'),individual);
                                                                //'P3le4s5o6e', 'P2le1s8o3e', '9934', individual);      
        request.mode = CustomSettingUtil.getVedaConfigValue('Request_Mode'); //'test';
        request.version = CustomSettingUtil.getVedaConfigValue('Request_Version'); //'1.0';
        request.carrier = CustomSettingUtil.getVedaConfigValue('Request_Carrier'); //'06';
        request.subscriberIdentifier = CustomSettingUtil.getVedaConfigValue('Request_SubscriberIdentifier'); //'AYFK9999';
        request.security = CustomSettingUtil.getVedaConfigValue('Request_Security'); //'TS'
        //request.individual.currentAddress = new VedaDTO.Address();
        
    } 

    
    public PageReference generateCreditReport() {
        
        try{
            errorMsg = false;
            errorTextMsg = '';

            //check current user if not a Sys Admin, Sys Admin OS, Team Leader, HL Team Leader
            if (!String.valueof(curUsr.Profile.Name).contains(Label.Profile_Name_System_Administrator) && curUsr.Profile.Name != Label.Profile_Name_Team_Leader && curUsr.Profile.Name != Label.Profile_Name_HL_Team_Leader){
                //Check for existing CCResponse attachment on current record
                list <Attachment> exAttlist = [Select Id, ParentId, Name From Attachment Where ParentId = : idOfObjToAttach];
                for (Attachment att : exAttlist){
                    if (att.Name.contains('-CC.pdf')){
                        errorTextMsg = Label.Veda_Credit_Check_Attachment_Error;
                        break;
                    }
                }
                //Contains error before generating the Credit Report
                if (errorTextMsg != ''){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,errorTextMsg));
                    return null;
                }
            }

            Blob xmlBlob;
            if (!Test.isRunningTest()){
                response = VedaAPI.individualProductCall(request);
                xmlBlob = Blob.valueOf(response.xmlResponse);
            }
            else { 
                xmlBlob = Blob.valueOf(testresponse);      
            }
            Attachment xmlFile = new Attachment(parentId = idOfObjToAttach, name= 'CCResponse-' + DateTime.now().format()+ '.xml', body = xmlBlob);
            insert xmlFile;
            
            attachmentId = xmlFile.id;

            //Redirect to VedaIndividualPagePrintable vf page to convert created XML file to PDF
            PageReference pref = new PageReference('/apex/VedaIndividualPagePrintable?attachID='+attachmentId+'&objAttId='+idOfObjToAttach+'&objType='+sObjectType);
            pref.setRedirect(true);
            return pref;
                    
        }catch(Exception e){
            errorMsg = true;
            errorTextMsg = e.getMessage();
            System.debug('errorTextMsg : '+errorTextMsg );
            System.debug('errorMsg : '+errorMsg );
            return null;
        }
    }
    
    public PageReference back() {
        
        return new PageReference('/'+idOfObjToAttach);
    }
    
    public boolean getErrorMsg() {
        return errorMsg;
    }
    

}
/** 
* @FileName: SupportRequestHandler
* @Description: Trigger Handler for the Support_Request__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 18/10/18 RDAVID - feature/SupportRequestTrigger - Created Class
* 1.1 1/11/18 RDAVID - feature/OmniChannel - This logic is previously in Pickup js button
* 1.2 2/11/18 RDAVID - feature/SupportRequestTrigger - Added logic for Submission Request -> Ready for Final Mod (From Tim) works for POS: cases for now
* 1.3 6/11/18 RDAVID - feature/SupportRequestTrigger - Removed query condition "AND RecordType.Name LIKE 'POS:%'" for it to work for Nodifi.
* 1.4 21/11/18 RDAVID - feature/SupportRequestTrigger - Added logic to populate Pick_Up_Date_Time__c once Support Request is moved to In Progress.
* 1.5 30/11/18 RDAVID - feature/SupportRequestTrigger - Added logic to populate Case_Owner__c once Support Request Case_Number__c is manually updated.
* 1.6 3/12/18 RDAVID - feature/ - Added logic to populate Case_Owner__c only if Case Owner is not Queue
* 1.7 1/17/19 JBACULOD - added pulling of Parent Case's Suspended Reason
* 1.8 1/19/19 JBACULOD - added creation of CAR SR when Stage is set to Deferred to Credit
* 3.0 27/02/19 RDAVID - extra/SectorAutomation (Teamwork tasks/23413929) - Added logic to insert Sector records.
* 3.1 7/03/19 RDAVID - extra/SectorAutomation (Teamwork tasks/23721938) - Added logic to insert/update SLA Sector records.
* 3.2 16/04/19 RDAVID - extra/task24148707-SectorSLAFixBusinessHours - Logic to update Alert_Recipient_Name__c & Alert_Recipient_Email__c when the Support Request is picked up by a User (Moved to In Progress). These fields are being referenced from the Sector_Send_Case_SLA_Sector_Warning_Notification (Email Alert).
* 3.3 25/04/19 RDAVID - extra/task24148707-SectorSLAFixBusinessHours - Added logic to only create Is_Active__c SLA Sector 
* 3.4 3/05/19 RDAVID-task24399056 : Populate the Email_Warning_Recipient__c and Email_Warning_Recipient_POC__c of the Sector record
* 3.5 23/05/2019 RDAVID-tasks/24515914 : Logic for NVM Routing of "Communication - NOD - Request a Call" SR
* 3.6 19/06/2019 RDAVID-tasks/24515914 : Continue working on Request a Call routing to NVM
* 3.7 8/08/2019  RDAVID-extra/task25611325-SRTriggerDeleteSectors : Logic to auto delete related Sectors
* 3.8 20/12/2019 JBACULOD SFBAU-93 : Added clearing of Validation Override fields
* 3.9 20/01/2020 ICRUZ SFBAU-229 : Added Trigger Toggle for Support Request - Update Case
**/ 

public without sharing class SupportRequestHandler implements ITrigger{
	private static Map<Id,Case> parentCaseMap = new Map<Id,Case>();
	private static Map<String,Case> parentCaseNumberMap = new Map<String,Case>();
	private List<Support_Request__c> newSupporRequestList = new List<Support_Request__c>();
	private static General_Settings1__c gensettings = General_Settings1__c.getInstance(UserInfo.getUserId());
	private Map<Id,Schema.RecordTypeInfo> rtMapById = Schema.SObjectType.Support_Request__c.getRecordTypeInfosById();
	private Map<Id,Case> caseMap = new Map<Id,Case>(); 
	private Map<Id,Case> caseMapToUpdate = new Map <Id,Case>();
	private list <Support_Request__c> creCARSupportRequestList = new list <Support_Request__c>();
	public static Set<String> validSectors = new Set<String>();
	public List<Sector__c> sectorListToUps = new List<Sector__c>();
	public List<Sector__c> sectorSLAMapToUpdate = new List<Sector__c>();
	public Map<Id,Support_Request__c> srwithSectorMap = new Map<Id,Support_Request__c>();
	public List<SLA_Sector__mdt> slaSectorMetaDataList = new List<SLA_Sector__mdt>();
	public Map<String,SLA_Sector__mdt> slaStrKeyMetadataInsertMap = new Map<String,SLA_Sector__mdt>();
	public Map<String,SLA_Sector__mdt> slaStrKeyMetadataUpdateMap = new Map<String,SLA_Sector__mdt>();
	private Map<Id,Case> caseToUpdateFromSector = new Map <Id,Case>();
	public List<Sector__c> sectorListToDelete = new List<Sector__c>();//3.7 8/08/2019  RDAVID-extra/task25611325-SRTriggerDeleteSectors : Logic to auto delete related Sectors
	public static Boolean updatedCaseStageinSR = false;

	public SupportRequestHandler (){
		//Initialize value of newSupporRequestList
		newSupporRequestList = (Trigger.IsInsert || Trigger.isUpdate) ? (List<Support_Request__c>) Trigger.new : (Trigger.IsDelete) ? (List<Support_Request__c>) Trigger.old : null;
		//System.debug('newSupporRequestList - '+newSupporRequestList);
	}
    /** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
		Set<String> caseNumberSet = new Set<String>();
		Set<Id> caseIds = new Set<Id>();
		Set<Id> deletedSRs = new Set<Id>();
		
		for(Support_Request__c supportRequest : newSupporRequestList){
			if(!String.IsBlank(supportRequest.Case_Number_Text__c)) 
				caseNumberSet.add(supportRequest.Case_Number_Text__c);
			if(supportRequest.Case_Number__c != NULL)
				caseIds.add(supportRequest.Case_Number__c);
			//3.7 8/08/2019  RDAVID-extra/task25611325-SRTriggerDeleteSectors : Logic to auto delete related Sectors
			if(Trigger.isDelete){
				deletedSRs.add(supportRequest.Id);
			}
		}
		if((caseNumberSet.size() > 0 || caseIds.size() > 0) && parentCaseMap.IsEmpty())
			parentCaseMap = new Map<Id,Case>([SELECT Id, CaseNumber, Partition__c, Suspended_Reason__c, OwnerId FROM Case WHERE CaseNumber IN: caseNumberSet OR Id IN: caseIds]);

		if(!parentCaseMap.IsEmpty() && parentCaseNumberMap.IsEmpty())
			parentCaseNumberMap = SupportRequestGateway.getparentCaseNumberMap(parentCaseMap.values());
		if(deletedSRs.size() > 0)
			sectorListToDelete = [SELECT Id FROM Sector__c WHERE SR_Number__c IN: deletedSRs];
	}

	
	public void bulkAfter(){
		Set<Id> caseIds = new Set<Id>();
		if(validSectors.size() == 0){
			validSectors = CaseGateway.getvalidSectors();
		} 	
		if(slaSectorMetaDataList.size() == 0){
			slaSectorMetaDataList = [SELECT Id, Is_Active__c, Sector_Name__c,Additional_Rules__c,Related_to_Object__c,Related_to_Partition__c,Related_to_Object_RT__c,Sector_Entry_Event__c,Sector_Entry_Event_Criteria__c,Sector_Exit_Event__c,Sector_Exit_Event_Criteria__c FROM SLA_Sector__mdt WHERE Related_to_Object__c = 'Support Request'];
			for(SLA_Sector__mdt slaSecMeta : slaSectorMetaDataList){
				if(slaSecMeta.Sector_Entry_Event__c == 'On Create' && Trigger.isInsert && slaSecMeta.Is_Active__c) //25/04/19 RDAVID - extra/task24148707-SectorSLAFixBusinessHours - Added logic to only create Is_Active__c SLA Sector 
				slaStrKeyMetadataInsertMap.put(slaSecMeta.Sector_Name__c+':'+slaSecMeta.Related_to_Partition__c,slaSecMeta);
				if(slaSecMeta.Sector_Exit_Event__c == 'On Update' && Trigger.isUpdate)
				slaStrKeyMetadataUpdateMap.put(slaSecMeta.Sector_Name__c+':'+slaSecMeta.Related_to_Partition__c,slaSecMeta);
			}
		}
		for(Support_Request__c supportRequest : newSupporRequestList){
			if(supportRequest.Case_Number__c != NULL) caseIds.add(supportRequest.Case_Number__c);
		}
		
		if(caseIds.size() > 0) caseMap = new Map<Id,Case>([SELECT Id,Settlement_Officer__c, Submitter__c, Status, Stage__c, RecordType.Name,On_Hold__c,Stage_Prior__c,Prior_Status__c, Validation_Override__c FROM Case WHERE ID IN: caseIds]);

		if(trigger.isUpdate){
			Map<Id,Support_Request__c> oldSRMap = (Map<Id,Support_Request__c>) trigger.oldMap;
			System.debug('task24399056 SupportRequestHandler line 91 ');
			//RDAVID - 27/02/2019 - Teamwork tasks/23413929
			if (TriggerFactory.trigSet.Enable_Case_Log_Sectors__c){
				Map<Id,Set<String>> oldSRSectorMap = new Map<Id,Set<String>>();
				Set<Id> srIds = new Set<Id>();
				Set<String> sectorSet = new Set<String>();
				Set<String> srWithActiveSLA = new Set<String>();
				for(Support_Request__c sr : (List<Support_Request__c>)trigger.new){
					if(sr.Stage__c != oldSRMap.get(sr.Id).Stage__c){
						srIds.add(sr.Id);
						sectorSet.add('Stage - '+oldSRMap.get(sr.Id).Stage__c);
						// 3.2 16/04/19 RDAVID - extra/task24148707-SectorSLAFixBusinessHours Logic to update Alert_Recipient_Name__c & Alert_Recipient_Email__c
						if(sr.Stage__c != oldSRMap.get(sr.Id).Stage__c && sr.Status__c != oldSRMap.get(sr.Id).Status__c){
							if(sr.Stage__c == 'In Progress' && sr.Status__c == 'In Progress') {
								srWithActiveSLA.add(sr.Id);
							}
						}
					}
					//9/03/19 RDAVID - extra/SectorAutomation (Teamwork tasks/23721938)
					if(!String.IsBlank(sr.Partition1__c)){
						for(String slaStrKey : slaStrKeyMetadataUpdateMap.keySet()){
							System.debug('**** tasks/23721938 slaStrKey -> '+slaStrKey);
							////System.debug('>>>>>>>>>>>>>>> UPDATE slaStrKey -> '+slaStrKey);
							List<String> slaStrKeyParts = slaStrKey.split(':');
							if(sr.Partition__c == slaStrKeyParts[1]){
								SLA_Sector__mdt slaSecRecord = slaStrKeyMetadataUpdateMap.get(slaStrKey);
								////System.debug('>>>>>>>>>>>>>>> slaSecRecord.Related_to_Object_RT__c -> '+slaSecRecord.Related_to_Object_RT__c);
								////System.debug('>>>>>>>>>>>>>>> caseRTMapById.get(cs.RecordTypeId).getName() -> '+caseRTMapById.get(cs.RecordTypeId).getName());
								if(slaSecRecord.Related_to_Object_RT__c.containsIgnoreCase(rtMapById.get(sr.RecordTypeId).getName())){
									////System.debug('>>>>>>>>>>>>>>> UPDATE Valid RT');
									SObject so = (SObject)sr;
									SObject oldSo = (SObject)oldSRMap.get(sr.Id);
									
									//If there is a Sector SLA wherein Exit Event is 'On Update', this logic will update existing Sectors if the Exit Event Criteria is met.
									if(slaSecRecord.Sector_Exit_Event__c == 'On Update' && !String.IsBlank(slaSecRecord.Sector_Exit_Event_Criteria__c)){	
										////System.debug('>>>>>>>>>>>>>>> Update SLA Sector based on Sheet.');
										List<String> sectorExitEventList = slaSecRecord.Sector_Exit_Event_Criteria__c.split('\n');
										////System.debug('>>>>>>>>>>>>>>> Update SLA Sector based on Sheet. sectorExitEventList - '+sectorExitEventList);
										for(String sectExtEvt : sectorExitEventList){
											List<String> fieldApiValue = sectExtEvt.split(':');
											String metaFieldApi = fieldApiValue[0];
											String metaFieldValue = fieldApiValue[1];											
											String soCurrentFieldVal = (String)so.get(metaFieldApi);
											String soOldFieldVal = (String)oldSo.get(metaFieldApi);

											if(soCurrentFieldVal != soOldFieldVal && !String.IsBlank(soCurrentFieldVal) && metaFieldValue.containsIgnoreCase(soCurrentFieldVal)){
												srIds.add(sr.Id);
												sectorSet.add(slaSecRecord.Sector_Name__c);
											}
										}
									}
								}
							}
						}
					}
				}
				if(srwithSectorMap.IsEmpty()){
					srwithSectorMap = new Map<Id,Support_Request__c>([	SELECT Id, (SELECT Id, SR_Number__c, Sector1__c 
																			FROM Sectors__r 
																			WHERE SR_Number__c IN: srIds AND Sector1__c IN: sectorSet AND Sector_Exit__c = NULL ORDER BY CreatedDate DESC) 
																		FROM Support_Request__c WHERE Id IN: srIds]);
					//System.debug('bulkAfter validSectors - ' + validSectors.size());
					//System.debug('bulkAfter srIds size - ' + srIds.size());
					//System.debug('bulkAfter oldSRSectorMap - ' + oldSRSectorMap.keySet());
					//System.debug('bulkAfter oldCaseSectorValues - ' + oldSRSectorMap.values());
					//System.debug('bulkAfter srwithSectorMap - ' + srwithSectorMap);
				}

				// 3.2 16/04/19 RDAVID - extra/task24148707-SectorSLAFixBusinessHours Logic to Query Sector SLA and update Alert_Recipient_Name__c & Alert_Recipient_Email__c
				//					   - extra/task24148707-SectorSLAFixBusinessHours Logic deprecated

				System.debug('task24399056 SupportRequestHandler line 159 ');
				if(srWithActiveSLA.size() > 0){
					System.debug('task24399056 SupportRequestHandler line 161 '+srWithActiveSLA.size());
					for(Sector__c sector : [SELECT Id, Sector1__c, Case_Number__c, SR_Number__c, SR_Number__r.Owner.Name, SR_Number__r.Owner.Email, SR_Number__r.OwnerId FROM Sector__c WHERE SR_Number__c IN : srWithActiveSLA AND Sector_Exit__c = NULL AND Sector1__c LIKE '%SLA%']){
						//3/05/19 RDAVID-task24399056 Comment this line 
						// sector.Alert_Recipient_Name__c = sector.SR_Number__r.Owner.Name;
						// sector.Alert_Recipient_Email__c = sector.SR_Number__r.Owner.Email;
						//3/05/19 RDAVID-task24399056 Comment this line 

						//6/05/2019 RDAVID-task24399056 Logic to If NOD Initial Review SR is moved to In Progress (Assigned to a USER):
						if(sector.Sector1__c == CommonConstants.SECTOR1_INIT_REV){
							Case caseSetReviewer = new Case(Id = sector.Case_Number__c, Reviewer__c = sector.SR_Number__r.OwnerId);
							caseToUpdateFromSector.put(caseSetReviewer.Id,caseSetReviewer);
						}

						sectorSLAMapToUpdate.add(sector); // Force Update as Workaround to update the Email_Warning_Recipient__c and Email_Warning_Recipient_POC__c
					}
				}
			}
		}
	}
		
	public void beforeInsert(SObject so){
		Support_Request__c supportRequest = (Support_Request__c)so;
		if(!String.IsBlank(supportRequest.Case_Number_Text__c) && parentCaseNumberMap.containsKey(supportRequest.Case_Number_Text__c)) {
			supportRequest.Case_Number__c = parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).Id;
			if(String.valueOf(parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).OwnerId).startsWith('005')) supportRequest.Case_Owner__c = parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).OwnerId;
			if(parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).Partition__c != NULL){ 
				supportRequest.Partition__c = parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).Partition__c;
				if (supportRequest.Partition__c == 'Positive'){
					if(parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).Suspended_Reason__c != NULL) supportRequest.Suspended_Reason__c = parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).Suspended_Reason__c;
				}
			}
		}
		if (parentCaseMap.size() > 0){
			if (parentCaseMap.containskey(supportRequest.Case_Number__c)){
				if(supportRequest.Case_Number__c != NULL){
					if(String.valueOf(parentCaseMap.get(supportRequest.Case_Number__c).OwnerId).startsWith('005')) supportRequest.Case_Owner__c = parentCaseMap.get(supportRequest.Case_Number__c).OwnerId;
					if (parentCaseMap.get(supportRequest.Case_Number__c).Suspended_Reason__c != null) supportRequest.Suspended_Reason__c = parentCaseMap.get(supportRequest.Case_Number__c).Suspended_Reason__c;
				}
			}
		}
		// 3.5 23/05/2019 RDAVID-tasks/24515914
		if(TriggerFactory.trigSet.Enable_SR_Call_Request_NVM_Routing__c){
			if(supportRequest.RecordTypeId != NULL && rtMapById.get(supportRequest.RecordTypeId).getName() == CommonConstants.SR_NOD_REQUEST_CALL){
				SupportRequestGateway.setReadyToDialTime(supportRequest,parentCaseMap.get(supportRequest.Case_Number__c));
				SupportRequestGateway.populateNVMFields(parentCaseMap.get(supportRequest.Case_Number__c),gensettings);
				SupportRequestGateway.populateParentCaseForCallRequest(supportRequest, parentCaseMap.get(supportRequest.Case_Number__c),caseMapToUpdate);
				supportRequest.OwnerId = supportRequest.Case_Owner__c;
				System.debug('Parent Case ---------> '+parentCaseMap.get(supportRequest.Case_Number__c));
			}	
		}
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		Support_Request__c supportRequest = (Support_Request__c)so;
		Support_Request__c oldSupportRequest = (Support_Request__c)oldSo;

		String supportRequestRTName = rtMapById.get(supportRequest.RecordTypeId).getName();

		if(!String.IsBlank(supportRequest.Case_Number_Text__c) && parentCaseNumberMap.containsKey(supportRequest.Case_Number_Text__c)){
			supportRequest.Case_Number__c = parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).Id;
			if(String.valueOf(parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).OwnerId).startsWith('005')) supportRequest.Case_Owner__c = parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).OwnerId;
			if (supportRequest.Partition__c == 'Positive'){
				if(parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).Suspended_Reason__c != NULL) supportRequest.Suspended_Reason__c = parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).Suspended_Reason__c;
			}
		}
		if (parentCaseMap.size() > 0){
			if (parentCaseMap.containskey(supportRequest.Case_Number__c)){
				if(oldSupportRequest.Case_Number__c != supportRequest.Case_Number__c && supportRequest.Case_Number__c != NULL){
					if(String.valueOf(parentCaseMap.get(supportRequest.Case_Number__c).OwnerId).startsWith('005')) supportRequest.Case_Owner__c = parentCaseMap.get(supportRequest.Case_Number__c).OwnerId;
				}
				if (supportRequest.Partition__c == 'Positive' && ( supportRequest.Suspended_Reason__c ==  null || oldSupportRequest.Suspended_Reason__c != supportRequest.Suspended_Reason__c ) ){
					supportRequest.Suspended_Reason__c = parentCaseMap.get(supportRequest.Case_Number__c).Suspended_Reason__c;
				}
			}
		}
		if(supportRequest.Case_Number_Text__c != NULL && parentCaseNumberMap.containsKey(supportRequest.Case_Number_Text__c) && parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).Partition__c != NULL) supportRequest.Partition__c = parentCaseNumberMap.get(supportRequest.Case_Number_Text__c).Partition__c;
		//RDAVID - 1/11/18 - feature/OmniChannel This logic is previously in Pickup js button
		//enable this once Omni-channel is live in Production
		if(oldSupportRequest.Status__c != supportRequest.Status__c && supportRequest.Status__c == 'In Progress'){
			if(oldSupportRequest.Stage__c != supportRequest.Stage__c && supportRequest.Stage__c == 'In Progress'){
				//System.debug('Support Request status is '+ supportRequest.Status__c);
				if(supportRequestRTName == gensettings.Support_Request_Contract_RT__c || supportRequestRTName == gensettings.Support_Request_Contract_RT_New__c){ 
					supportRequest.RecordTypeId = gensettings.Support_Request_Contract_RT_Id__c;  //System.debug('Support Request RT is '+supportRequestRTName);
				} 
				else if(supportRequestRTName == gensettings.Support_Request_Settlement_RT__c || supportRequestRTName == gensettings.Support_Request_Settlement_RT_New__c){ 
					supportRequest.RecordTypeId = gensettings.Support_Request_Settlement_RT_Id__c; //System.debug('Support Request RT is '+supportRequestRTName);
				} 
				else if(supportRequestRTName == gensettings.Support_Request_Submission_RT__c || supportRequestRTName == gensettings.Support_Request_Submission_RT_New__c){ 
					supportRequest.RecordTypeId = gensettings.Support_Request_Submission_RT_Id__c; //System.debug('Support Request RT is '+supportRequestRTName);
				} 			
			}
			if(oldSupportRequest.Stage__c != supportRequest.Stage__c && (supportRequest.Stage__c == 'In Progress' || supportRequest.Stage__c == 'Deferred to Senior')){
				supportRequest.OwnerId = UserInfo.getUserId();
				if(supportRequest.Pick_Up_Date_Time__c == NULL) supportRequest.Pick_Up_Date_Time__c = System.now();
			}
		}
		// 3.5 23/05/2019 RDAVID-tasks/24515914
		// if(supportRequest.RecordTypeId != NULL && rtMapById.get(supportRequest.RecordTypeId).getName() == CommonConstants.SR_NOD_REQUEST_CALL){
		// 	SupportRequestGateway.setReadyToDialTime(supportRequest);
		// }	

	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}

	public void afterInsert(SObject so){
		Support_Request__c supportRequest = (Support_Request__c)so;
		String supportRequestRTName = rtMapById.get(supportRequest.RecordTypeId).getName();
		
		if(!caseMap.IsEmpty()){
			if(caseMap.containsKey(supportRequest.Case_Number__c)) {
				Case caseInstanceCopy = caseMap.get(supportRequest.Case_Number__c).clone(true, false, true, true);
				Case caseInstance = caseMap.get(supportRequest.Case_Number__c);

				//6/05/2019 RDAVID-task24399056 Logic to If NOD Initial Review SR is moved to In Progress (Assigned to a USER):
				// if(caseToUpdateFromSector.containsKey(caseInstance.Id)){ //There is a NOD SLA Initial Review that needs to update the reviewer.
				// 	caseInstance.Reviewer__c = caseToUpdateFromSector.get(caseInstance.Id).Reviewer__c;
				// 	System.debug('task24399056 SupportRequestHandler line 274 '+caseInstance.Reviewer__c);
				// } 
				
				// SFBAU-229: Added Trigger Toggle for Support Request - Update Case
				if (TriggerFactory.trigSet.Enable_Support_Request_Update_Case__c) {
					if (supportRequest.Status__c == 'New' && supportRequest.Stage__c == 'New') {
						SupportRequestGateway.updateCase('Insert', caseInstance, supportRequest, supportRequestRTName, gensettings);
					}
				}				

				//System.debug('caseInstance = '+caseInstance);
				//System.debug('caseInstanceCopy = '+caseInstanceCopy);
				
				if(!System.equals(caseInstanceCopy,caseInstance)) 
				caseMapToUpdate.put(caseInstance.Id, caseInstance);
			}
		}
		//RDAVID - 27/02/2019 - Teamwork tasks/23413929
		if (TriggerFactory.trigSet.Enable_Case_Log_Sectors__c){
			if(supportRequest.Stage__c != NULL){
				String sectorToPut = 'Stage - '+supportRequest.Stage__c;
				if(validSectors.contains(sectorToPut)){
					Sector__c sectorRec = SupportRequestGateway.getSectorInstance(supportRequest,sectorToPut);
					sectorListToUps.add(sectorRec);
				}
			}
			//  7/03/19 RDAVID - extra/SectorAutomation (Teamwork tasks/23721938)
			if(supportRequest.Partition1__c != NULL){
				for(String slaStrKey : slaStrKeyMetadataInsertMap.keySet()){
					System.debug('@@@286 slaStrKey ---> ' + slaStrKey);
					List<String> slaStrKeyParts = slaStrKey.split(':');
					if(supportRequest.Partition__c == slaStrKeyParts[1]){ 
						SLA_Sector__mdt slaSecRecord = slaStrKeyMetadataInsertMap.get(slaStrKey);
						if(slaSecRecord.Related_to_Object_RT__c.containsIgnoreCase(rtMapById.get(supportRequest.RecordTypeId).getName())){
							// if(String.IsBlank(slaSecRecord.Additional_Rules__c)){
							String sectorToPut = slaStrKeyParts[0];
							Sector__c sectorRec = SupportRequestGateway.getSectorInstance(supportRequest,sectorToPut);
							sectorListToUps.add(sectorRec);							
							// }
						}
					}
				}
			}
		}

	}
	
	public void afterUpdate(SObject oldSo, SObject so){
		Support_Request__c supportRequest = (Support_Request__c)so;
		Support_Request__c oldSupportRequest = (Support_Request__c)oldSo;

		String supportRequestRTName = rtMapById.get(supportRequest.RecordTypeId).getName();
		Boolean updateSettlementOfficer = false;
		Boolean updateSubmitter = false;

		if(Test.isRunningTest()){
			updateSettlementOfficer = true;
			updateSubmitter = true;
		}

		if(supportRequestRTName == gensettings.Support_Request_Contract_RT__c) updateSettlementOfficer = true;
		else if(supportRequestRTName == gensettings.Support_Request_Submission_RT__c) updateSubmitter = true;
		//System.debug('@@@updateSettlementOfficer = '+updateSettlementOfficer);
		//System.debug('@@@updateSubmitter = '+updateSubmitter);


		if(!caseMap.IsEmpty()){
			if(caseMap.containsKey(supportRequest.Case_Number__c)) {
				Case caseInstanceCopy = caseMap.get(supportRequest.Case_Number__c).clone(true, false, true, true);
				Case caseInstance = caseMap.get(supportRequest.Case_Number__c);//new Case(Id = supportRequest.Case_Number__c);

				//RDAVID - 1/11/18 - feature/OmniChannel This logic is previously in Pickup js button
				if(supportRequest.Status__c != oldSupportRequest.Status__c){
					//enable this once Omni-channel is live in Production
					if(supportRequest.Status__c == 'In Progress'){
						if(updateSettlementOfficer){
							SupportRequestGateway.updateSettlementOfficerOrSubmitter('Settlement',caseInstance);
						}
						
						if(updateSubmitter){
							SupportRequestGateway.updateSettlementOfficerOrSubmitter('Submitter',caseInstance);
						}

						//6/05/2019 RDAVID-task24399056 Logic to If NOD Initial Review SR is moved to In Progress (Assigned to a USER):
						if(caseToUpdateFromSector.containsKey(caseInstance.Id)){ //There is a NOD SLA Initial Review that needs to update the reviewer.
							System.debug('task24399056 SupportRequestHandler line 274 '+caseToUpdateFromSector.get(caseInstance.Id).Reviewer__c);
							caseInstance.Reviewer__c = caseToUpdateFromSector.get(caseInstance.Id).Reviewer__c;
						} 
					}

					// SFBAU-229: Added Trigger Toggle for Support Request - Update Case
					if (TriggerFactory.trigSet.Enable_Support_Request_Update_Case__c) {
						if (supportRequest.Status__c == 'Closed') {
							SupportRequestGateway.updateCase('Update', caseInstance, supportRequest, supportRequestRTName, gensettings);
						}
					}

				}
				
				//System.debug('caseInstance = '+caseInstance);
				//System.debug('caseInstanceCopy = '+caseInstanceCopy);
				//Check if Case needs to be updated
				if(!System.equals(caseInstanceCopy,caseInstance))
				caseMapToUpdate.put(caseInstance.Id, caseInstance);
			}
		}

		if (TriggerFactory.trigset.Enable_Support_Request_Create_CAR_SR__c){
			//Creates CAR Support Request on a 'Deferred to Credit' POS Asset - Settlement Suspension SR
			if (oldSupportRequest.Stage__c != supportRequest.Stage__c && supportRequest.Stage__c == 'Deferred to Credit' && supportRequest.Partition__c == 'Positive' && supportRequest.RecordTypeId == gensettings.Support_Request_Settlement_RT_Id__c){
				Support_Request__c carSR = new Support_Request__c(
					OwnerId = supportRequest.Case_Owner__c, //will eventually be change to Management for OmniChannel
					RecordTypeId = gensettings.Support_Request_CAR_RT_Id__c, //Credit Analysis Required SR RecordType ID
					Support_Sub_Type__c = 'Deferred to Credit',
					Status__c = 'New',
					Stage__c = 'New',
					Partition__c = 'Positive',
					Suspended_Reason__c = supportRequest.Suspended_Reason__c,
					Related_Support_Request__c = supportRequest.Id //Parent Suspension SR
				);
				creCARSupportRequestList.add(carSR);
			}
		}
		//System.debug('@@@@ SECTOR - srwithSectorMap - '+srwithSectorMap);
		if(srwithSectorMap.containsKey(supportRequest.Id) ){
			//System.debug('@@@@ SECTOR - supportRequest ID - '+supportRequest.Id);
			
			Boolean srChildSectorIsNotEmpty = (srwithSectorMap.get(supportRequest.Id).Sectors__r != NULL && srwithSectorMap.get(supportRequest.Id).Sectors__r.size() > 0) ? true : false;
			Boolean stageUpdatedNotNull = (supportRequest.Stage__c != NULL && supportRequest.Stage__c != oldSupportRequest.Stage__c) ? true : false;
			Boolean stageUpdatedToNull = (supportRequest.Stage__c == NULL && supportRequest.Stage__c != oldSupportRequest.Stage__c) ? true : false;
			//System.debug('@@@@ SECTOR - srChildSectorIsNotEmpty - '+srChildSectorIsNotEmpty);

			if(srChildSectorIsNotEmpty){
				//System.debug('@@@@ SECTOR - Im here at 276');
				//System.debug('@@@@ SECTOR - Im here at 278 stageUpdatedNotNull - '+stageUpdatedNotNull);
				//System.debug('@@@@ SECTOR - Im here at 284 stageUpdatedToNull - '+stageUpdatedToNull);
				for(Sector__c sec : srwithSectorMap.get(supportRequest.Id).Sectors__r){
					//System.debug('@@@@ SECTOR - sec - '+sec);
					if(!sec.Sector1__c.containsIgnoreCase('- SLA -')){
						if(stageUpdatedNotNull){
							String sectorToPutUpd = 'Stage - '+oldSupportRequest.Stage__c;
							String sectorToPutIns = 'Stage - '+supportRequest.Stage__c;
							if(sectorToPutUpd == sec.Sector1__c){
								Sector__c sectorRecUp = new Sector__c(Id = sec.Id, Sector_Exit__c = System.now(), Exit_Owner__c = UserInfo.getUserId());
								sectorListToUps.add(sectorRecUp);
								if(validSectors.contains(sectorToPutIns)){
									Sector__c sectorRecIns = SupportRequestGateway.getSectorInstance(supportRequest,sectorToPutIns);
									sectorListToUps.add(sectorRecIns);
								}
							}
						}
						if(stageUpdatedToNull){
							String sectorToPutUpd = 'Stage - '+oldSupportRequest.Stage__c;
							//System.debug('@@@@ SECTOR - sectorToPutUpd - '+sectorToPutUpd);
							if(sectorToPutUpd == sec.Sector1__c){
								Sector__c sectorRecUp = new Sector__c(Id = sec.Id, Sector_Exit__c = System.now(), Exit_Owner__c = UserInfo.getUserId());
								sectorListToUps.add(sectorRecUp);
							}
						}
					}
					else{
						//System.debug('>>>>>>>>>>> SECTORSLA to update -> '+ sec.Sector1__c);
						Sector__c sectorRecUp = new Sector__c(Id = sec.Id, Sector_Exit__c = System.now(), Exit_Owner__c = UserInfo.getUserId());
						sectorListToUps.add(sectorRecUp);
					}
				}
			}
			else if(!srChildSectorIsNotEmpty){
				if(stageUpdatedNotNull){
					String sectorToPutIns = 'Stage - '+supportRequest.Stage__c;
					if(validSectors.contains(sectorToPutIns)){
						Sector__c sectorRecIns = SupportRequestGateway.getSectorInstance(supportRequest,sectorToPutIns);
						sectorListToUps.add(sectorRecIns);
					}
				}
			}
		}
	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		System.debug('@@@@SupportRequestHandler : '+caseMapToUpdate.values());
		if(!caseMapToUpdate.IsEmpty()){
			//System.debug('@@@@Update Case: '+caseMapToUpdate.values());
			Database.update(caseMapToUpdate.values());
		}
		//Creates CAR Support Requests
		if (creCARSupportRequestList.size() > 0){
			Database.insert(creCARSupportRequestList);
		}
		//Creates/Update Sector records
		if (sectorListToUps.size() > 0){
			//System.debug('sectorListToUps--> '+sectorListToUps);
			Database.upsert(sectorListToUps);
		}
		// 3.2 16/04/19 RDAVID - extra/task24148707-SectorSLAFixBusinessHours Logic to Query Sector SLA and update Alert_Recipient_Name__c & Alert_Recipient_Email__c
		if (sectorSLAMapToUpdate.size() > 0){
			//System.debug('sectorSLAMapToUpdate--> '+sectorSLAMapToUpdate);
			Database.update(sectorSLAMapToUpdate);
		}
		//3.7 8/08/2019  RDAVID-extra/task25611325-SRTriggerDeleteSectors : Logic to auto delete related Sectors
		if (sectorListToDelete.size() > 0){
			System.debug('sectorListToDelete--> '+sectorListToDelete.size());
			Database.delete(sectorListToDelete);
		}
	}	
}
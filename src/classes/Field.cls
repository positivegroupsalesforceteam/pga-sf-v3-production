/** 
* @FileName: Field
* @Description: Used in tasks/24746570 New Partner Lightning Page - for getting fieldsets
* @Source: https://metillium.com/2018/02/lightning-field-set-form-component-v2/
* @Copyright: Positive (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 3/06/19 RDAVID - extra/task24746570-NewPartnerPageLightning - Initial version
**/ 
public class Field {
    public Field(Schema.FieldSetMember f) {
        this.DBRequired = f.DBRequired;
        this.APIName = f.fieldPath;
        this.Label = f.label;
        this.Required = f.required;
        this.Type = String.valueOf(f.getType());
    }
    
    public Field(Boolean DBRequired) {
        this.DBRequired = DBRequired;
    }
    
    @AuraEnabled
    public Boolean DBRequired { get;set; }
    
    @AuraEnabled
    public String APIName { get;set; }
    
    @AuraEnabled
    public String Label { get;set; }
    
    @AuraEnabled
    public Boolean Required { get;set; }
    
    @AuraEnabled
    public String Type { get; set; }
}
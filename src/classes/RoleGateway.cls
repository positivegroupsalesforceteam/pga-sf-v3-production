/** 
* @FileName: RoleGateway
* @Description: Provides finder methods for accessing data in the Role object.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/14/18 RDAVID Created class
* 1.1 3/28/18 JBACULOD Added linkCaseAppinRole [SD-29- Auto Populate Case/Application in ROle]
* 1.2 3/28/2018 RDAVID Worked on SD-30 SF - NM - Propagate Role to Account Field Values 
* 1.3 3/29/2018 RDAVID Worked on SD-30 - Added getOpenCaseIds, propagateToPersonAccount, propagateToPersonContact, propagateToBusinessAccount, getRoleAccountIds, getAccountContactMap
* 2.0 4/26-27/2018 RDAVID Worked on SD-30 Updates (Additional Fields to propagate and RTs) , Removed (propagateToPersonAccountpropagateToPersonContact,propagateToBusinessAccount)
* 2.1 7/5/2018 RDAVID Worked on SD-?? Role to Application
* 2.2 6/12/2018 JBACULOD Modified RoletoApplication method, added updating of TrustPilot Unique Link fields in Application from Role
* 2.3 7/06/2018 JBACULOD Added population of MC Lookups in Application
* 2.4 8/03/2018 JBACULOD Added population of MC Phone, Emails in Application
* 3.0 5/09/18 RDAVID: Logic for creation Case Touch Point (Junction record to link Cases and Bizible Touch Points.) DEPRECATED
* 4.0 19/09/18 RDAVID: issuefix/RoleTrigger -  Fix for SD-30 Role to Account Propagation 
* Addded New Field Role__c.h_Case_Status
* 4.1 30/12/19 JVGO bugfix-SFBAU-133 - Added logic to remove values in Co-Applicant's Name, Email, and Phone when role record is changed.
**/ 

public without sharing class RoleGateway{
	private static final String PRIMARY_APPLICANT = 'Primary Applicant';
	private	static final String COAPPLICANT = 'Co-Applicant';
	private static final String ROLE_RT_SPOUSESUPPORTSERVICE = 'Non-Borrowing Spouse';
	private static final String APP_RT_LEADCONSUMER = 'Lead - Consumer';
	private static final String APP_RT_LEADUNKNOWN = 'Lead - Type Unknown';
	private static final String AP_RT_CONSUMERJOINT = 'Consumer - Joint';
	private static final String AP_RT_CONSUMERINDIVIDUAL = 'Consumer - Individual';
	public static Set<Id> roleFromFronEnd = new Set<Id>();
	private static Map<Id,Id> caseAccountIdMap = new Map<Id,Id>();
	private static Map<Id,Account> accountMap = new Map<Id,Account>();
	public static Map<Id,Account> caseAccountMap = new Map<Id,Account>();
	public static final Set<String> personAccountFields = new Set<String>{'FirstName'.toLowerCase(), 'LastName'.toLowerCase(), 'IsPersonAccount'.toLowerCase(), 'Languages_pc'.toLowerCase(), 'Level_pc'.toLowerCase(), 'PersonAssistantName'.toLowerCase(), 'PersonAssistantPhone'.toLowerCase(), 'PersonBirthdate'.toLowerCase(), 'PersonContactId'.toLowerCase(), 'PersonDepartment'.toLowerCase(), 'PersonEmail'.toLowerCase(), 'PersonEmailBouncedDate'.toLowerCase(), 'PersonEmailBouncedReason'.toLowerCase(), 'PersonHomePhone'.toLowerCase(),'PersonLastCURequestDate'.toLowerCase(),'PersonLastCUUpdateDate'.toLowerCase(),'PersonLeadSource'.toLowerCase(),'PersonMailingCity'.toLowerCase(),'PersonMailingCountry'.toLowerCase(),'PersonMailingPostalCode'.toLowerCase(),'PersonMailingState'.toLowerCase(),'PersonMailingStreet'.toLowerCase(),'PersonMobilePhone'.toLowerCase(),'PersonOtherCity'.toLowerCase(),'PersonOtherCountry'.toLowerCase(),'PersonOtherPhone'.toLowerCase(),'PersonOtherPostalCode'.toLowerCase(),'PersonOtherState'.toLowerCase(),'PersonOtherStreet'.toLowerCase(),'PersonTitle'.toLowerCase(),'RecordTypeId'.toLowerCase(),'Salutation'.toLowerCase()};

	public static void linkCaseAppinRole(Role__c oldrole, Role__c role, Map <id,id> caseAppInRoleMap){
		ID IdRoleCase = null;
		ID IdRoleApp = null;
		if (oldrole != null){ //is on Insert
			IdRoleCase = oldRole.Case__c;
			IdRoleApp = oldRole.Application__c;
		}

		if (IdRoleCase != role.Case__c && role.Case__c != null){ //Auto populate Application with Case in Role
			if (caseAppInRoleMap.containskey(role.Case__c)){
				role.Application__c = caseAppInRoleMap.get(role.Case__c);
			}
		}
		if (IdRoleApp != role.Application__c && role.Application__c != null){ //Auto populate Case with Application in Role
			if (caseAppInRoleMap.containskey(role.Application__c)){
				role.Case__c = caseAppInRoleMap.get(role.Application__c);
			}
		}
	}

	/**
	* @Description: Returns a map of Role_to_Account_Mapping__mdt
	* @Arguments:	n/a
	* @Returns:		getRoleToAccountCMDMap
	*/
	public static Map<String,Role_to_Account_Mapping__mdt> getRoleToAccountCMDMap (){
		// return new Map<String,Role_to_Account_Mapping__mdt>([SELECT Active__c,Account_Fields__c,Contact_Fields__c,Role_Fields__c,Role_RecordTypes__c,Type__c FROM Role_to_Account_Mapping__mdt WHERE Active__c =: TRUE]);
		
		Map<String, Role_to_Account_Mapping__mdt> roleToAccCMDMap = new Map<String,Role_to_Account_Mapping__mdt>();
		
		for(Role_to_Account_Mapping__mdt ramcmd : [SELECT Active__c,Account_Fields__c,Contact_Fields__c,Role_Fields__c,Role_RecordTypes__c,Type__c FROM Role_to_Account_Mapping__mdt WHERE Active__c =: TRUE]){
			if(ramcmd.Active__c) roleToAccCMDMap.put(ramcmd.Role_Fields__c.toLowerCase(),ramcmd);
		}

		return roleToAccCMDMap;
	}

	/**
	* @Description: Role__c:  add number of Primary & Co-Applicants, store value on Application__c/Number of Applicants__c
					Role__c, where either: a) Role__c/RecordType++Non-Borrowing_Spouse__c, OR b) Application__c/RecordType== ("Lead - Consumer", "Lead - Type Unknown")...
						then set Application__c/Non-Borrowing_Spouse__c = TRUE
					Role__c/Primary_Contact_for_Application__c == TRUE, copy value of Role__c/Account_Name__c  to Application__c/Primary_Contact_for_Application__c
					Role__c/Primary_Contact_for_Application__c == TRUE, copy value of Role__c/  Primary_Contact_Preferred_Call_Time__c  to Application__c/Primary_Contact_Preferred_Call_Time__c	
					Copy value of Role's Trust Pilot Unique Link of Primary/Co-Applicant in Application
	* @Arguments:	n/a
	* @Returns:		getRoleToAccountCMDMap
	*/
    /* PREVIOUS VERSION
	public static void updateApplication (Application__c app){
		// Map<Id,Application__c> applicationMapToUpdate = new Map<Id,Application__c>();
		// system.debug('@@applicationset:'+applicationSet);
		// if(applicationSet.size()>0){
			// for(Application__c app : [SELECT Id,Number_of_Applicants__c, Non_Borrowing_Spouse__c, RecordType.Name, Primary_Contact_Preferred_Call_Time__c, Primary_Contact_for_Application__c, Inconsistent_Application_Details__c, Prim_Applicant_TrustPilot_Unique_Link__c, Co_Applicant_TrustPilot_Unique_Link__c, Primary_Contact_Name_MC__c, Spouse_Supporting_Servicing_Name_MC__c, Co_Applicant_Name_MC__c, Primary_Contact_Location_MC__c, Associated_Relationships__c, Primary_Contact_Email_MC__c, Primary_Contact_Phone_MC__c, Co_Applicant_Email_MC__c, Co_Applicant_Phone_MC__c, Spouse_Supporting_Servicing_Email_MC__c, Spouse_Supporting_Servicing_Phone_MC__c,
			 						//(SELECT Id, RecordType.Name, Role_Type__c, Location__c, TrustPilot_Unique_Link__c, Account__c, Account_Name__c,Account__r.Name, Account__r.PersonEmail, Account__r.PersonMobilePhone, Preferred_Call_Time__c,Primary_Contact_for_Application__c, Associated_Relationships__c FROM Roles__r WHERE RecordTypeId != NULL) FROM Application__c WHERE Id IN: applicationSet]){
		Integer numOfApplicants = 0;
		Boolean updateAppFlag = false;
		
		// Application__c updateApp = new Application__c(Id = app.Id);
		
		if(app.Roles__r != NULL && app.Roles__r.size() > 0){
			for(Role__c role : app.Roles__r){
				// if(DMLOperation.containsIgnoreCase('Update')){ //handles the update on application if role is to be update
				
					if(role.Role_Type__c == PRIMARY_APPLICANT || role.Role_Type__c == COAPPLICANT){
						numOfApplicants++;
						if(role.Role_Type__c == PRIMARY_APPLICANT){
							//updateApp.Primary_Applicant__c = role.Account_Name__c;//role.Account__r.Name;
							if (role.TrustPilot_Unique_Link__c != null){
								app.Prim_Applicant_TrustPilot_Unique_Link__c = role.TrustPilot_Unique_Link__c;
								system.debug('@@App PA TPLink:'+app.Prim_Applicant_TrustPilot_Unique_Link__c);
							}
						}
						else if (role.Role_Type__c == COAPPLICANT){
							app.Co_Applicant_Name_MC__c = role.Account__c;
							if (role.Account__c != null){
								app.Co_Applicant_Email_MC__c = (!String.isBlank(role.Email__c) && role.Account__r.PersonEmail != role.Email__c) ? role.Email__c : role.Account__r.PersonEmail;
								app.Co_Applicant_Phone_MC__c = (!String.isBlank(role.Mobile_Phone__c) && role.Account__r.PersonMobilePhone != role.Mobile_Phone__c) ? role.Mobile_Phone__c : role.Account__r.PersonMobilePhone;
							}
							if (role.TrustPilot_Unique_Link__c != null){
								app.Co_Applicant_TrustPilot_Unique_Link__c = role.TrustPilot_Unique_Link__c;
								system.debug('@@App CA TPLink:'+app.Co_Applicant_TrustPilot_Unique_Link__c);
							}

							if(app.Spouse_Supporting_Servicing_Email_MC__c != null || app.Spouse_Supporting_Servicing_Name_MC__c != null || app.Spouse_Supporting_Servicing_Phone_MC__c != null){
								app.Spouse_Supporting_Servicing_Email_MC__c = null;
								app.Spouse_Supporting_Servicing_Name_MC__c = null;
								app.Spouse_Supporting_Servicing_Phone_MC__c = null;
								app.Non_Borrowing_Spouse__c = false;
							}
						}
					}

					if(role.RecordType.Name.containsIgnoreCase(ROLE_RT_SPOUSESUPPORTSERVICE) || app.RecordType.Name.containsIgnoreCase(APP_RT_LEADCONSUMER) || app.RecordType.Name.containsIgnoreCase(APP_RT_LEADUNKNOWN)){
						app.Spouse_Supporting_Servicing_Name_MC__c = role.Account__c;
						if (role.Account__c != null){
							app.Spouse_Supporting_Servicing_Email_MC__c = (!String.isBlank(role.Email__c) && role.Account__r.PersonEmail != role.Email__c) ? role.Email__c : role.Account__r.PersonEmail;
							app.Spouse_Supporting_Servicing_Phone_MC__c = (!String.isBlank(role.Mobile_Phone__c) && role.Account__r.PersonMobilePhone != role.Mobile_Phone__c) ? role.Mobile_Phone__c : role.Account__r.PersonMobilePhone;
						}
						
						//check if the application's co application has values after updating, delete if it has existing values. JVGO SFBAU-133
						if(app.Co_Applicant_Email_MC__c != null || app.Co_Applicant_Name_MC__c != null || app.Co_Applicant_Phone_MC__c != null){
							app.Co_Applicant_Email_MC__c = null;
							app.Co_Applicant_Name_MC__c = null;
							app.Co_Applicant_Phone_MC__c = null;
						}

						app.Non_Borrowing_Spouse__c = true;
					}

					if(role.Primary_Contact_for_Application__c){
						app.Primary_Contact_Name_MC__c = role.Account__c;
						if (role.Account__c != null){
							app.Primary_Contact_Email_MC__c = (!String.isBlank(role.Email__c) && role.Account__r.PersonEmail != role.Email__c) ? role.Email__c : role.Account__r.PersonEmail;
							app.Primary_Contact_Phone_MC__c = (!String.isBlank(role.Mobile_Phone__c) && role.Account__r.PersonMobilePhone != role.Mobile_Phone__c) ? role.Mobile_Phone__c : role.Account__r.PersonMobilePhone;
						}
						app.Primary_Contact_Location_MC__c = role.Location__c;
						app.Primary_Contact_for_Application__c = role.Account_Name__c;
						app.Primary_Contact_Preferred_Call_Time__c = role.Preferred_Call_Time__c;
					}

					//Pushes Associated Relationships from Role to Application
					if (TriggerFactory.trigSet.Enable_Role_Assocs_in_Application__c){
						if (String.valueof(role.RecordType.Name).startsWith('Applicant')){
							app.Associated_Relationships__c = role.Associated_Relationships__c;
						}
					}

					//if (role.Business_Income_Verification_Method__c != null) updateApp.Income_Verification_Method__c = role.Business_Income_Verification_Method__c;	117						}
					if (role.Business_Total_Revenue__c != null) app.Total_Revenue__c = role.Business_Total_Revenue__c;	
					if (role.Profit_Before_Tax_Annual__c != null) app.Profit_Before_Tax_annual__c = role.Profit_Before_Tax_annual__c;	
					if (role.Business_Total_Depreciation__c != null) app.Total_Depreciation__c = role.Business_Total_Depreciation__c;	
					if (role.Reporting_Period__c != null) app.Reporting_Period__c = role.Reporting_Period__c;
				
				// }else if(DMLOperation.containsIgnoreCase('Delete')){ //handles the update on application if role is to be deleted
					
				// 	if(role.RecordType.Name.containsIgnoreCase(ROLE_RT_SPOUSESUPPORTSERVICE)){ //clear nbs fields
						
				// 		app.Spouse_Supporting_Servicing_Phone_MC__c = null;
				// 		app.Spouse_Supporting_Servicing_Email_MC__c = null;
				// 		app.Spouse_Supporting_Servicing_Name_MC__c = null;
				// 		app.Spouse_Supporting_Servicing_Phone_MC__c = null;
				// 		app.Non_Borrowing_Spouse__c = false;
				// 	}
				// 	if(role.Role_Type__c == COAPPLICANT && app.RecordType.Name.containsIgnoreCase(AP_RT_CONSUMERINDIVIDUAL)){ //clear co-applicant
				// 		app.Co_Applicant_Email_MC__c = null;
				// 		app.Co_Applicant_Name_MC__c = null;
				// 		app.Co_Applicant_Phone_MC__c = null;
				// 	}
				// 	if(role.Role_Type__c == PRIMARY_APPLICANT && role.Primary_Contact_for_Application__c && app.RecordType.Name.containsIgnoreCase(AP_RT_CONSUMERINDIVIDUAL)){
				// 		app.Primary_Applicant__c = '';
				// 		app.Primary_Contact_for_Application__c = '';
				// 		app.Primary_Contact_Email_MC__c = null;
				// 		app.Primary_Contact_Name_MC__c = null;
				// 		app.Primary_Contact_Phone_MC__c = null;
				// 	}
				// }
			}
		}else{
			numOfApplicants = 0;
			app.Non_Borrowing_Spouse__c = false;
			app.Primary_Contact_for_Application__c = '';
			app.Primary_Contact_Preferred_Call_Time__c = NULL;
		}

		if(numOfApplicants != app.Number_of_Applicants__c){
			app.Number_of_Applicants__c = numOfApplicants;
		}

		if(app.RecordType.Name.containsIgnoreCase(AP_RT_CONSUMERJOINT) && numOfApplicants <= 1 || app.RecordType.Name.containsIgnoreCase(AP_RT_CONSUMERINDIVIDUAL) && numOfApplicants > 1){
			app.Inconsistent_Application_Details__c = true;
		}
		else{
			app.Inconsistent_Application_Details__c = false;
		}
				// if(app.Non_Borrowing_Spouse__c != updateApp.Non_Borrowing_Spouse__c 
				// || app.Primary_Contact_for_Application__c != updateApp.Primary_Contact_for_Application__c
				// || app.Primary_Contact_Preferred_Call_Time__c != updateApp.Primary_Contact_Preferred_Call_Time__c
				// || app.Inconsistent_Application_Details__c != updateApp.Inconsistent_Application_Details__c
				// || app.Number_of_Applicants__c != updateApp.Number_of_Applicants__c){
				// 	applicationMapToUpdate.put(updateApp.Id,updateApp);
				// }

			//}
		// }
        //return app;
	}
	*/
	public static void updateApplication (Application__c app, String DMLOperation){
		// Map<Id,Application__c> applicationMapToUpdate = new Map<Id,Application__c>();
		// system.debug('@@applicationset:'+applicationSet);
		// if(applicationSet.size()>0){
			// for(Application__c app : [SELECT Id,Number_of_Applicants__c, Non_Borrowing_Spouse__c, RecordType.Name, Primary_Contact_Preferred_Call_Time__c, Primary_Contact_for_Application__c, Inconsistent_Application_Details__c, Prim_Applicant_TrustPilot_Unique_Link__c, Co_Applicant_TrustPilot_Unique_Link__c, Primary_Contact_Name_MC__c, Spouse_Supporting_Servicing_Name_MC__c, Co_Applicant_Name_MC__c, Primary_Contact_Location_MC__c, Associated_Relationships__c, Primary_Contact_Email_MC__c, Primary_Contact_Phone_MC__c, Co_Applicant_Email_MC__c, Co_Applicant_Phone_MC__c, Spouse_Supporting_Servicing_Email_MC__c, Spouse_Supporting_Servicing_Phone_MC__c,
			 						//(SELECT Id, RecordType.Name, Role_Type__c, Location__c, TrustPilot_Unique_Link__c, Account__c, Account_Name__c,Account__r.Name, Account__r.PersonEmail, Account__r.PersonMobilePhone, Preferred_Call_Time__c,Primary_Contact_for_Application__c, Associated_Relationships__c FROM Roles__r WHERE RecordTypeId != NULL) FROM Application__c WHERE Id IN: applicationSet]){
		Integer numOfApplicants = 0;
		Boolean updateAppFlag = false;
		
		// Application__c updateApp = new Application__c(Id = app.Id);
		
		if(app.Roles__r != NULL && app.Roles__r.size() > 0){
			for(Role__c role : app.Roles__r){
				 if(DMLOperation.containsIgnoreCase('Update')){ //handles the update on application if role is to be update
				
					if(role.Role_Type__c == PRIMARY_APPLICANT || role.Role_Type__c == COAPPLICANT){
						numOfApplicants++;
						if(role.Role_Type__c == PRIMARY_APPLICANT){
							//updateApp.Primary_Applicant__c = role.Account_Name__c;//role.Account__r.Name;
							if (role.TrustPilot_Unique_Link__c != null){
								app.Prim_Applicant_TrustPilot_Unique_Link__c = role.TrustPilot_Unique_Link__c;
								system.debug('@@App PA TPLink:'+app.Prim_Applicant_TrustPilot_Unique_Link__c);
							}
						}
						else if (role.Role_Type__c == COAPPLICANT){
							app.Co_Applicant_Name_MC__c = role.Account__c;
							if (role.Account__c != null){
								app.Co_Applicant_Email_MC__c = (!String.isBlank(role.Email__c) && role.Account__r.PersonEmail != role.Email__c) ? role.Email__c : role.Account__r.PersonEmail;
								app.Co_Applicant_Phone_MC__c = (!String.isBlank(role.Mobile_Phone__c) && role.Account__r.PersonMobilePhone != role.Mobile_Phone__c) ? role.Mobile_Phone__c : role.Account__r.PersonMobilePhone;
							}
							if (role.TrustPilot_Unique_Link__c != null){
								app.Co_Applicant_TrustPilot_Unique_Link__c = role.TrustPilot_Unique_Link__c;
								system.debug('@@App CA TPLink:'+app.Co_Applicant_TrustPilot_Unique_Link__c);
							}

							if(app.Spouse_Supporting_Servicing_Email_MC__c != null || app.Spouse_Supporting_Servicing_Name_MC__c != null || app.Spouse_Supporting_Servicing_Phone_MC__c != null){
								app.Spouse_Supporting_Servicing_Email_MC__c = null;
								app.Spouse_Supporting_Servicing_Name_MC__c = null;
								app.Spouse_Supporting_Servicing_Phone_MC__c = null;
								app.Non_Borrowing_Spouse__c = false;
							}
						}
					}

					if(role.RecordType.Name.containsIgnoreCase(ROLE_RT_SPOUSESUPPORTSERVICE) || app.RecordType.Name.containsIgnoreCase(APP_RT_LEADCONSUMER) || app.RecordType.Name.containsIgnoreCase(APP_RT_LEADUNKNOWN)){
						app.Spouse_Supporting_Servicing_Name_MC__c = role.Account__c;
						if (role.Account__c != null){
							app.Spouse_Supporting_Servicing_Email_MC__c = (!String.isBlank(role.Email__c) && role.Account__r.PersonEmail != role.Email__c) ? role.Email__c : role.Account__r.PersonEmail;
							app.Spouse_Supporting_Servicing_Phone_MC__c = (!String.isBlank(role.Mobile_Phone__c) && role.Account__r.PersonMobilePhone != role.Mobile_Phone__c) ? role.Mobile_Phone__c : role.Account__r.PersonMobilePhone;
						}
						
						//check if the application's co application has values after updating, delete if it has existing values. JVGO SFBAU-133
						if(app.Co_Applicant_Email_MC__c != null || app.Co_Applicant_Name_MC__c != null || app.Co_Applicant_Phone_MC__c != null){
							app.Co_Applicant_Email_MC__c = null;
							app.Co_Applicant_Name_MC__c = null;
							app.Co_Applicant_Phone_MC__c = null;
						}

						app.Non_Borrowing_Spouse__c = true;
					}

					if(role.Primary_Contact_for_Application__c){
						app.Primary_Contact_Name_MC__c = role.Account__c;
						if (role.Account__c != null){
							app.Primary_Contact_Email_MC__c = (!String.isBlank(role.Email__c) && role.Account__r.PersonEmail != role.Email__c) ? role.Email__c : role.Account__r.PersonEmail;
							app.Primary_Contact_Phone_MC__c = (!String.isBlank(role.Mobile_Phone__c) && role.Account__r.PersonMobilePhone != role.Mobile_Phone__c) ? role.Mobile_Phone__c : role.Account__r.PersonMobilePhone;
						}
						app.Primary_Contact_Location_MC__c = role.Location__c;
						app.Primary_Contact_for_Application__c = role.Account_Name__c;
						app.Primary_Contact_Preferred_Call_Time__c = role.Preferred_Call_Time__c;
					}

					//Pushes Associated Relationships from Role to Application
					if (TriggerFactory.trigSet.Enable_Role_Assocs_in_Application__c){
						if (String.valueof(role.RecordType.Name).startsWith('Applicant')){
							app.Associated_Relationships__c = role.Associated_Relationships__c;
						}
					}

					//if (role.Business_Income_Verification_Method__c != null) updateApp.Income_Verification_Method__c = role.Business_Income_Verification_Method__c;	117						}
					if (role.Business_Total_Revenue__c != null) app.Total_Revenue__c = role.Business_Total_Revenue__c;	
					if (role.Profit_Before_Tax_Annual__c != null) app.Profit_Before_Tax_annual__c = role.Profit_Before_Tax_annual__c;	
					if (role.Business_Total_Depreciation__c != null) app.Total_Depreciation__c = role.Business_Total_Depreciation__c;	
					if (role.Reporting_Period__c != null) app.Reporting_Period__c = role.Reporting_Period__c;
				
				}else if(DMLOperation.containsIgnoreCase('Delete')){ //handles the update on application if role is to be deleted
					System.debug('On Delete START -----------------');
					if(role.RecordType.Name.containsIgnoreCase(ROLE_RT_SPOUSESUPPORTSERVICE)){ //clear nbs fields
						System.debug('On Delete NBS ---------');
						app.Spouse_Supporting_Servicing_Phone_MC__c = null;
						app.Spouse_Supporting_Servicing_Email_MC__c = null;
						app.Spouse_Supporting_Servicing_Name_MC__c = null;
						app.Spouse_Supporting_Servicing_Phone_MC__c = null;
						app.Non_Borrowing_Spouse__c = false;
					}
					if(role.Role_Type__c == COAPPLICANT && app.RecordType.Name.containsIgnoreCase(AP_RT_CONSUMERINDIVIDUAL)){ //clear co-applicant
						System.debug('On Delete COAPPLICANT ---------');
						app.Co_Applicant_Email_MC__c = null;
						app.Co_Applicant_Name_MC__c = null;
						app.Co_Applicant_Phone_MC__c = null;
					}
					if(role.Role_Type__c == PRIMARY_APPLICANT && role.Primary_Contact_for_Application__c && app.RecordType.Name.containsIgnoreCase(AP_RT_CONSUMERINDIVIDUAL)){
						System.debug('On Delete PRIMARY APPLICANT ---------');
						app.Primary_Applicant__c = '';
						app.Primary_Contact_for_Application__c = '';
						app.Primary_Contact_Email_MC__c = null;
						app.Primary_Contact_Name_MC__c = null;
						app.Primary_Contact_Phone_MC__c = null;
					}
				}
			}
		}else{
			numOfApplicants = 0;
			app.Non_Borrowing_Spouse__c = false;
			app.Primary_Contact_for_Application__c = '';
			app.Primary_Contact_Preferred_Call_Time__c = NULL;
		}

		if(numOfApplicants != app.Number_of_Applicants__c){
			app.Number_of_Applicants__c = numOfApplicants;
		}

		if(app.RecordType.Name.containsIgnoreCase(AP_RT_CONSUMERJOINT) && numOfApplicants <= 1 || app.RecordType.Name.containsIgnoreCase(AP_RT_CONSUMERINDIVIDUAL) && numOfApplicants > 1){
			app.Inconsistent_Application_Details__c = true;
		}
		else{
			app.Inconsistent_Application_Details__c = false;
		}
				// if(app.Non_Borrowing_Spouse__c != updateApp.Non_Borrowing_Spouse__c 
				// || app.Primary_Contact_for_Application__c != updateApp.Primary_Contact_for_Application__c
				// || app.Primary_Contact_Preferred_Call_Time__c != updateApp.Primary_Contact_Preferred_Call_Time__c
				// || app.Inconsistent_Application_Details__c != updateApp.Inconsistent_Application_Details__c
				// || app.Number_of_Applicants__c != updateApp.Number_of_Applicants__c){
				// 	applicationMapToUpdate.put(updateApp.Id,updateApp);
				// }

			//}
		// }
        //return app;
	}
	public static Boolean isAccountFieldValid (String accountField, Boolean isRoleAccountPerson){
		if(isRoleAccountPerson && personAccountFields.contains(accountField)) return true;
		else if(!isRoleAccountPerson && !personAccountFields.contains(accountField) && accountField.containsIgnoreCase('__c')) return true;
		else if(isRoleAccountPerson && !personAccountFields.contains(accountField) && (accountField.containsIgnoreCase('__pc') || accountField.containsIgnoreCase('__c'))) return true;
		else if(!isRoleAccountPerson && !personAccountFields.contains(accountField) && !accountField.containsIgnoreCase('__c') && !accountField.containsIgnoreCase('__pc')) return true;
		else return false;
	}
}
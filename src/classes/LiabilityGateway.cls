/** 
* @FileName: LiabilityGateway
* @Description: Provides finder methods for accessing data in the Liability object.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/14/18 RDAVID Created class
**/ 

public without sharing class LiabilityGateway{

    /**
    * @Description: Returns a set of Parent Role Id associated to Liability record. 
    * @Arguments:   List<Liability1__c> newLiabilityList - trigger.new
    * @Returns:     Set<Id> getRoleIds - parent Role ids
    */

    public static Set<Id> getRoleIds(List<Liability1__c> newLiabilityList){
        Set<Id> roleIdsSet = new Set<Id>();

        for(Liability1__c liability : newLiabilityList){
            //CHANGE123 roleIdsSet.add(liability.Role__c);
        }
        return roleIdsSet;
    }
}
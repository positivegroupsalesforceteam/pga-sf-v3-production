/** 
* @FileName: SectorSetAlertRecipient
* @Description: Invocable class used as workaround in setting the Alert Recipient and Email based on Case Owner after the Round Robin Assignment of Nodifi SLA Sector 
* @Copyright: Positive (c) 2019 
* @author: Rexie Aaron David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 12/04/19 RDAVID-task24148707 Created Class
* 1.1 17/04/19 RDAVID-task24148707 Added logic to update the Parent Case SLA Status SLA_Status_Submitted_to_Lender_Sector__c,SLA_Status_Initial_Review_Sector__c ,SLA_Status_Application_Review_Sector__c,SLA_Status_Docs_Out_Sector__c,SLA_Status_Sent_for_Settlement_Sector__c
* 1.2 3/05/19 RDAVID-task24399056 : Populate the Email_Warning_Recipient__c and Email_Warning_Recipient_POC__c of the Sector record
**/ 
public class SectorSetAlertRecipient {
    public static Map<String,SLA_Sector__mdt> slaSectorMDTMap = new Map<String,SLA_Sector__mdt>();
    
	@InvocableMethod(label='Sector - Set Alert Recipient' description='updates Alert Recipient and Email of Sector record after assignment of NODIFI Case record in Process Builder')
    public static void setSLAWarningRecipient (List <Id> caseIds) {
        try{
            List<SObject> sectorToUpdate = new List<SObject>();
            
            Map<Id,Sector__c> sectorMap = new Map<Id,Sector__c>();
            Map<Id,Case> caseMap = new Map<Id,Case>();

            for(Sector__c sec : [SELECT Id,Case_Number__c,Case_Number__r.OwnerId FROM Sector__c WHERE Case_Number__c IN: caseIds AND RecordType.Name LIKE : '%Case SLA Sector%']){
                sectorMap.put(sec.Id,sec);
                Case cse = new Case(Id=sec.Case_Number__c, OwnerId =sec.Case_Number__r.OwnerId);
                caseMap.put(sec.Case_Number__c,cse);
            }
            if(!sectorMap.isEmpty() && !SectorHandler.sectorSObjectMap.isEmpty()){
                for(Sector__c sec : sectorMap.values()){
                    if(SectorHandler.sectorSObjectMap.containsKey(sec.Id)){
                        Sector__c sectorFromSobj = (Sector__c)SectorHandler.sectorSObjectMap.get(sec.Id);
                        SectorGateway.setRecipients(SectorHandler.sectorSObjectMap.get(sectorFromSobj.Id), caseMap.get(sectorFromSobj.Case_Number__c), SectorHandler.srMap.get(sectorFromSobj.SR_Number__c) , SectorHandler.slaSectorMDTMap, SectorHandler.slaTypeUserMap);
                        sectorToUpdate.add(SectorHandler.sectorSObjectMap.get(sectorFromSobj.Id));
                    }
                }
            }
            if(sectorToUpdate.size() > 0) {
                System.debug('update sectorToUpdate --> '+sectorToUpdate);
                Database.update(sectorToUpdate);
            }
        }
        catch(Exception e){
            System.debug('Error in SectorSetAlertRecipient --> '+e.getLineNumber() + '------' +e.getMessage());
        }
    }
}
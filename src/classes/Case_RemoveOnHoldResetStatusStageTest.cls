@isTest
private class Case_RemoveOnHoldResetStatusStageTest {
    @testSetup static void setupData() {
        //Turn On Trigger 
        Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
        triggerSettings.Enable_Account_Assocs_in_Role__c = true;
        triggerSettings.Enable_Account_Sync_Name_to_Role__c = true;
        triggerSettings.Enable_Account_Trigger__c = true; 
        triggerSettings.Enable_Address_Populate_Location__c = true;
        triggerSettings.Enable_Address_Trigger__c = true;
        triggerSettings.Enable_Application_Primary_Applicants__c = true;
        triggerSettings.Enable_Application_Sync_App_2_Case__c = true;
        triggerSettings.Enable_Application_Trigger__c = true;
        triggerSettings.Enable_Asset_Trigger__c = true;
        triggerSettings.Enable_Attachment_Create_File_in_Box__c = true;
        triggerSettings.Enable_Attachment_Trigger__c = true;
        triggerSettings.Enable_Case_Auto_Create_Application__c = true;
        triggerSettings.Enable_Case_generate_TrustPilot_Link__c = true;
        triggerSettings.Enable_Case_Lookup_Fields__c = true;
        triggerSettings.Enable_Case_Record_Stage_Timestamps__c = true;
        triggerSettings.Enable_Case_Scenario_Trigger__c = true;
        triggerSettings.Enable_Case_SCN_recordStageTimestamps__c = true;
        triggerSettings.Enable_Case_Status_Stage_Funnels__c = true;
        triggerSettings.Enable_Case_Trigger__c = true;
        triggerSettings.Enable_CommissionCT_GetCaseLender__c = true;
        triggerSettings.Enable_CommissionCT_Trigger__c = true;
        triggerSettings.Enable_CommissionLine_GetCommission__c = true;
        triggerSettings.Enable_Commission_Line_Trigger__c = true;
        triggerSettings.Enable_Expense_Trigger__c = true;
        triggerSettings.Enable_FO_Auto_Create_FO_Share__c = true;
        triggerSettings.Enable_FO_Delete_FO_Share_on_FO_Delete__c = true;
        triggerSettings.Enable_FOShare_Rollup_to_Role__c = true;
        triggerSettings.Enable_FOShare_Trigger__c = true;
        triggerSettings.Enable_Front_End_Submission_Trigger__c = true;
        triggerSettings.Enable_Frontend_Sub_SetNVMContact__c = true;
        triggerSettings.Enable_Income_Check_Employment__c = true;
        triggerSettings.Enable_Income_Populate_Sole_Trader__c = true;
        triggerSettings.Enable_Income_Role_Employment_Status__c = true;
        triggerSettings.Enable_Income_Trigger__c = true;
        triggerSettings.Enable_Lender_Automation_Call_Lender__c = true;
        triggerSettings.Enable_Liability_Trigger__c = true;
        triggerSettings.Enable_Relationship_Assocs_in_Account__c = true;
        triggerSettings.Enable_Relationship_Spouse_Duplicate__c = true;
        triggerSettings.Enable_Relationship_Trigger__c = true;
        triggerSettings.Enable_Role_Address_Check_Residency__c = true;
        triggerSettings.Enable_Role_Address_Populate_Address__c = true;
        triggerSettings.Enable_Role_Address_Toggle_Current__c = true;
        triggerSettings.Enable_Role_Address_Trigger__c = true;
        triggerSettings.Enable_Role_Assocs_in_Application__c = true;
        triggerSettings.Enable_Role_Auto_Populate_Case_App__c = true;
        triggerSettings.Enable_Role_Create_Case_Touch_Point__c = true;
        triggerSettings.Enable_Role_Rollup_to_Application__c = true;
        triggerSettings.Enable_Role_To_Account_Propagation__c = true;
        triggerSettings.Enable_Role_Trigger__c = true;
        triggerSettings.Enable_Task_Trigger__c = true;
        triggerSettings.Enable_Task_Update_Case_Attempt__c = true;
        triggerSettings.Enable_Triggers__c = true;
        triggerSettings.Enable_Case_NVM_Timezone_based_Routing__c = true;
        upsert triggerSettings Trigger_Settings1__c.Id;

        Process_Flow_Definition_Settings1__c processFlowSettings = Process_Flow_Definition_Settings1__c.getOrgDefaults();
        processFlowSettings.Enable_Case_NVM_Flow_Definitions__c = true;
        upsert processFlowSettings Process_Flow_Definition_Settings1__c.Id;

        //Create Test Account
        List<Account> accList = TestDataFactory.createGenericPersonAccount('First Name','TestLastName',CommonConstants.PACC_RT_I, 2);
        Database.insert(accList);
        accList = [SELECT Id, IsPersonAccount FROM Account WHERE Name LIKE '%TestLastName%'];
        System.assertEquals(accList[0].IsPersonAccount,true);
        System.assertEquals(accList.size(),2);

        //Create Test Case
        List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
        caseList[0].Partition__c = 'Positive';
        caseList[0].Lead_Source__c = 'PLS Database Callback';
        Database.insert(caseList);

        caseList = [SELECT Id ,Application_Name__c FROM Case WHERE Id IN: caseList];
        System.assertNotEquals(caseList[0].Application_Name__c,null);
        //Create Test Application
        // List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
        // Database.insert(appList);
        
        List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, caseList[0].Application_Name__c,  1);
        roleList[0].Role_Type__c = CommonConstants.ROLE_PRIMARY_APPLICANT;
        roleList[0].Phone__c = '09165856439';
        roleList[0].Mobile_Phone__c = '09165856439';
        roleList[0].Email__c = 'test@email.com';
        roleList[0].Primary_Contact_for_Application__c = true;
        Database.insert(roleList);

        roleList = [SELECT Id,Address__c,Account__c,Account__r.Address__c FROM Role__c WHERE Application__c =: caseList[0].Application_Name__c];

        System.assertEquals(roleList.size(),1);
        System.assertEquals(roleList[0].Address__c,NULL);
        System.assertEquals(roleList[0].Account__r.Address__c,NULL);
        System.assertNotEquals(roleList[0].Account__c,NULL);

        //Create test data for Location
        Location__c loc = new Location__c(
            Name = 'Location test',
            Country__c = 'Australia',
            Postcode__c = '132455',
            State__c = 'Australian Capital Territory',
            State_Acr__c = 'ACT'
        );
        insert loc;

        //Create test data for Address
        Address__c address = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
        address.Location__c = loc.Id;
        Database.insert(address);
        Address__c address2 = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
        address2.Location__c = loc.Id;
        Database.insert(address2);

        
    }

    static testmethod void testOne(){
        Case cse = [Select Id, Primary_Contact_Location_MC__c From Case LIMIT 1];
        System.assertEquals(cse.Primary_Contact_Location_MC__c,null);
        Role__c role = [Select Id, Name From Role__c LIMIT 1];
        Address__c address = [Select Id,Location__c, Name From Address__c LIMIT 1];
        Account accountTest = [SELECT Id, IsPersonAccount FROM Account WHERE Name LIKE '%TestLastName%' LIMIT 1];
        list <Id> cseIDs = new list <Id>();
        cseIDs.add(cse.Id);

        Test.startTest();
            Role_Address__c roleAddress = TestDataFactory.createGenericRoleAddress(accountTest.Id, true, role.Id, address.Id);
            Database.insert(roleAddress);
            role = [Select Id,Location__c,Application__c,Application__r.Primary_Contact_Location_MC__c,Account__c,Account__r.Location__c, Name,Primary_Contact_for_Application__c From Role__c LIMIT 1];
            System.assertEquals(role.Primary_Contact_for_Application__c,true);
            System.assertNotEquals(role.Location__c,null);
            System.assertNotEquals(role.Account__r.Location__c,null);
            Application__c app = new Application__c(Id = role.Application__c, Primary_Contact_Location_MC__c = role.Location__c);
            update app;

            app = [SELECT Id, Primary_Contact_Location_MC__c, Case__c FROM Application__c WHERE Id =: app.Id];
            System.assertNotEquals(app.Case__c,null);
            roleAddress = [SELECT Id,Location__c FROM Role_Address__c WHERE Id =: roleAddress.Id];
            System.assertNotEquals(roleAddress.Location__c,null);
            cse = [Select Id, Primary_Contact_Location_MC__c From Case WHERE Id =: app.Case__c];
            System.assertNotEquals(cse.Primary_Contact_Location_MC__c,null);
            // Case_CallbackNewCase.createNewCaseOnCallback(cseIDs);
        Test.stopTest();

    }

    static testmethod void testTwo(){
        Case cse = [Select Id, Primary_Contact_Location_MC__c From Case LIMIT 1];
        System.assertEquals(cse.Primary_Contact_Location_MC__c,null);
        Role__c role = [Select Id, Name From Role__c LIMIT 1];
        Address__c address = [Select Id,Location__c, Name From Address__c LIMIT 1];
        Account accountTest = [SELECT Id, IsPersonAccount FROM Account WHERE Name LIKE '%TestLastName%' LIMIT 1];
        list <Id> cseIDs = new list <Id>();
        cseIDs.add(cse.Id);

        cse.Stage__c = 'Attempted Contact';
        cse.Status = 'New';
        update cse;
        Test.startTest();

            Role_Address__c roleAddress = TestDataFactory.createGenericRoleAddress(accountTest.Id, true, role.Id, address.Id);
            Database.insert(roleAddress);
            role = [Select Id,Location__c,Application__c,Application__r.Primary_Contact_Location_MC__c,Account__c,Account__r.Location__c, Name,Primary_Contact_for_Application__c From Role__c LIMIT 1];
            System.assertEquals(role.Primary_Contact_for_Application__c,true);
            System.assertNotEquals(role.Location__c,null);
            System.assertNotEquals(role.Account__r.Location__c,null);
            Application__c app = new Application__c(Id = role.Application__c, Primary_Contact_Location_MC__c = role.Location__c);
            update app;

            app = [SELECT Id, Primary_Contact_Location_MC__c, Case__c FROM Application__c WHERE Id =: app.Id];
            System.assertNotEquals(app.Case__c,null);
            roleAddress = [SELECT Id,Location__c FROM Role_Address__c WHERE Id =: roleAddress.Id];
            System.assertNotEquals(roleAddress.Location__c,null);
            cse = [Select Id, Primary_Contact_Location_MC__c From Case WHERE Id =: app.Case__c];
            System.assertNotEquals(cse.Primary_Contact_Location_MC__c,null);

            list <String> caseIDs = new List<String> {String.valueOf(cse.Id)};
            Case_RemoveOnHoldResetStatusStage.removeOnHoldSetStatusStage(caseIDs);
        Test.stopTest();

    }
    static testmethod void testThree(){
        Case cse = [Select Id, Primary_Contact_Location_MC__c From Case LIMIT 1];
        cse.Stage__c = 'Attempted Contact';
        cse.Status = 'New';
        update cse;
        Test.startTest();
            list <String> caseIDs = new List<String> {String.valueOf(cse.Id)};
            try{
                Case_RemoveOnHoldResetStatusStage.removeOnHoldSetStatusStage(caseIDs);
            }
            catch(Exception e){
                System.debug(e.getMessage());
            }
        Test.stopTest();

    }
}
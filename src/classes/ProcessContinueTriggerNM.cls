/** 
* @FileName: ProcessContinueTriggerNM
* @Description: Helper class for NM TriggerFactory to allow Before Trigger events on Process Builder
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
    v.1.0	JBACU	12/12/18	Created
**/
public class ProcessContinueTriggerNM {
    
    @InvocableMethod(label='Allow Before Triggers on PB' description='resets Processed Map of record if coming from Process Builder')
	public static void continueProcessofRecord (list <ID> sobjIDs) {
        
        map <Id, boolean> processedMap = TriggerFactory.isProcessedMap;
        system.debug('@@processedMap:'+processedMap);        
        if (processedMap.size() > 0){
            for (ID sID : sobjIDs){
                if (processedMap.containskey(sID)){
                    if (processedMap.get(sID)){
                        processedMap.put(sID, false); //Allow Before Updates to be executed after Flow Definition process
                    }
                }
            }
            TriggerFactory.isProcessedMap = processedMap;
            system.debug('@@TriggerFactory.isProcessedMap:'+TriggerFactory.isProcessedMap);
        }
        
    }

}
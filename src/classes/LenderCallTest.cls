/** 
* @FileName: LenderCallTest
* @Description: Test class for LenderCallTest
* @Copyright: Positive (c) 2018 
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 6/5/18 RDAVID Created class
**/ 
@isTest
private class LenderCallTest {
	static testmethod void testLenderCall(){
        Test.startTest();
        	LenderCall.fakeMethod();
        Test.stopTest();
    }
}
/** 
* @FileName: SupportRequestGateway
* @Description: Provides finder methods for accessing data in the Role object.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 18/10/18 RDAVID - feature/SupportRequestTrigger - Created Class
* 1.1 01/19/19 JBACULOD - added creation of CAR SR when Stage is set to Deferred to Credit
* 1.2 23/05/2019 RDAVID-tasks/24515914 : Logic for NVM Routing of "Communication - NOD - Request a Call" SR
* 1.3 19/06/2019 RDAVID-tasks/24515914 : Continue working on Request a Call routing to NVM
* 1.4 24/06/2019 RDAVID-tasks/24515914 : Continue working on Request a Call routing to NVM
* 1.5 8/08/2019  RDAVID-tasks/25582197 : Change Case Status/Stage in Final Modification
* 1.6 26/08/2019 RDAVID extra/task25582197-NewSupportRequestVFChanges  Workaround to not allow user to manually move the stage in Case record. (h_Updated_from_SR__c)
* 1.7 2/09/2019 RDAVID-extra/task25891728-SRIssues Updated Logic to update Case Status/Stage to  when Initial SR is moved to Closed/Actioned
**/ 

public class SupportRequestGateway {
    public static Map<String,Case> getParentCaseNumberMap(List<Case> caseList){
        Map<String,Case> parentCaseNumberMap = new Map<String,Case>();
        
        for(Case c : caseList){
            parentCaseNumberMap.put(c.CaseNumber,c);
        }

        return parentCaseNumberMap;
    }

    public static void updateSettlementOfficerOrSubmitter(String updateWhat, Case caseInstance){
        
        if(updateWhat == 'Settlement'){
            if(caseInstance.Settlement_Officer__c != NULL && caseInstance.Settlement_Officer__c != UserInfo.getUserId()){
                caseInstance.Settlement_Officer__c = UserInfo.getUserId();
            }
            else if(caseInstance.Settlement_Officer__c == NULL){
                caseInstance.Settlement_Officer__c = UserInfo.getUserId();
            }
        }
        else if (updateWhat == 'Submitter'){
            if(caseInstance.Submitter__c != NULL && caseInstance.Submitter__c != UserInfo.getUserId()){
                caseInstance.Submitter__c = UserInfo.getUserId();
            }
            else if (caseInstance.Submitter__c == NULL){
                caseInstance.Submitter__c = UserInfo.getUserId();
            }
        }
    }

    public static void updateCase(String event, Case caseInstance, Support_Request__c supportRequest, String supportRequestRTName, General_Settings1__c gensettings){
        if(event == 'Insert'){
            if(supportRequestRTName == gensettings.Support_Request_Submission_RT_New__c){
                if(supportRequest.Support_Sub_Type__c == 'Final Modification') {
                    //1.5 8/08/2019  RDAVID-tasks/25582197
                    caseInstance.Status = 'Committed'; 
                    caseInstance.Stage__c = 'Final Modifications - RFS';
                    caseInstance.h_Updated_from_SR__c = TRUE; //This will set the h_Updated_from_SR__c to TRUE to bypass validation when Case is updated thru SR 
                    SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c; //This will set the static flah to TRUE to bypass validations
                }
                else{
                    caseInstance.Status = 'Review';
                    caseInstance.Stage__c = 'Ready for Submission';
                    caseInstance.h_Updated_from_SR__c = TRUE;
                    SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
                }
            }
            else if(supportRequestRTName == gensettings.Support_Request_Contract_RT_New__c){ 
                caseInstance.Status = 'Committed';
                caseInstance.Stage__c = 'Loan Docs Requested';
                caseInstance.h_Updated_from_SR__c = TRUE;
                SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
            }
            else if(supportRequestRTName == gensettings.Support_Request_Settlement_RT_New__c){
                caseInstance.On_Hold__c = 'Suspended Settlement';
                caseInstance.h_Updated_from_SR__c = TRUE;
                SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
            }
        }

        else if(event == 'Update'){
            if(supportRequestRTName == gensettings.Support_Request_Submission_RT__c){
                if(supportRequest.Stage__c == 'Actioned'){
                    if(supportRequest.Support_Sub_Type__c == 'Final Modification') {
                        caseInstance.Status = 'Committed';
                        caseInstance.Stage__c = 'Final Modifications - WOAA';
                        caseInstance.h_Updated_from_SR__c = TRUE;
                        SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
                    }
                    else{
                        caseInstance.Status = 'Submitted';
                        caseInstance.Stage__c = 'Submitted to Lender'; 
                        //Clear Validation Override fields once Stage is moved to Submitted to Lender (JBACU 20/12/19 - SFBAU-93)
                        if (caseInstance.Validation_Override__c){
                            caseInstance.Validation_Override__c = false; 
                            caseInstance.Validation_Override_By__c = null;
                            caseInstance.Validation_Override_Reason__c = null;
                        }
                        caseInstance.h_Updated_from_SR__c = TRUE;
                        SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
                    }
                }
                else if(supportRequest.Stage__c == 'Failed'){
                    caseInstance.Status = caseInstance.Prior_Status__c;
                    caseInstance.Stage__c = caseInstance.Stage_Prior__c;
                    caseInstance.h_Updated_from_SR__c = TRUE;
                    SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
                }
            }
            else if(supportRequestRTName == gensettings.Support_Request_Contract_RT__c){
                if(supportRequest.Stage__c == 'Actioned'){
                    caseInstance.Status = 'Committed';
                    caseInstance.Stage__c = 'Documents Sent to Client'; 
                    caseInstance.h_Updated_from_SR__c = TRUE;
                    SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
                }
                else if(supportRequest.Stage__c == 'Failed'){
                    // caseInstance.Status = caseInstance.Prior_Status__c;
                    // caseInstance.Stage__c = caseInstance.Stage_Prior__c;  //RDAVID 26/08/2019 RDAVID extra/task25582197 Updated scope https://docs.google.com/spreadsheets/d/1dayCM4Ph4fEtXldfgGJZ7w48GQ3FkMSOs_8SvPsehA4/edit#gid=839173489
                    caseInstance.Status = 'Committed';
                    caseInstance.Stage__c = 'Final Modifications - Approved';
                    caseInstance.h_Updated_from_SR__c = TRUE;
                    SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
                }
            }
            else if(supportRequestRTName == gensettings.Support_Request_Settlement_RT__c){
                if(supportRequest.Stage__c == 'Actioned'){
                    caseInstance.On_Hold__c = null; 
                    caseInstance.h_Updated_from_SR__c = TRUE;
                    SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
                }
            }

            //6/05/2019 RDAVID-task24399056 Logic to If NOD Initial Review SR is moved to Closed (Assigned to a USER):
            else if(supportRequestRTName == 'NOD Initial Review'){
                if(supportRequest.Stage__c == 'Actioned'){
                    if(caseInstance.Recordtype.Name.containsIgnoreCase('NOD: Commercial') || caseInstance.Recordtype.Name.containsIgnoreCase('NOD: Consumer')) { // 1.7 2/09/2019 RDAVID-extra/task25891728-SRIssues - Added "NOD: Consumer"
                        caseInstance.Status = 'New';
                        caseInstance.Stage__c = 'Owner Assigned';
                        caseInstance.h_Updated_from_SR__c = TRUE;
                        SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
                    }
                    // else if(caseInstance.Recordtype.Name.containsIgnoreCase('NOD: Consumer')) {
                    //     caseInstance.Status = 'Review';
                    //     caseInstance.Stage__c = 'Waiting on Docs';
                    //     caseInstance.h_Updated_from_SR__c = TRUE;
                    //     SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
                    // }
                }
            }
            // 1.4 24/06/2019 RDAVID-tasks/24515914 
            else if(supportRequestRTName == 'Communication - NOD - Request a Call'){
                caseInstance.Latest_Mortgage_Team_Comment__c = 'PB: SR Call Request to NVM: Sent to NVM';
                caseInstance.Call_Request_Scheduled__c = FALSE;
                caseInstance.Last_Call_Request_SR_ID__c = supportRequest.Id;
                caseInstance.Call_Request_Last_Dialed_Time__c = Datetime.now();
                caseInstance.h_Updated_from_SR__c = TRUE;
                SupportRequestHandler.updatedCaseStageinSR = caseInstance.h_Updated_from_SR__c;
            }
        }
    }

    public static Sector__c getSectorInstance (Support_Request__c supportRequest, String sectorFieldStr){
		Sector__c secIns = new Sector__c(	SR_Number__c = supportRequest.Id, 
											Sector_Entry__c = System.now(), 
											Channel__c = supportRequest.Channel__c, 
											Partition__c = supportRequest.Partition1__c, 
											Entry_Owner__c = UserInfo.getUserId(),
											Sector1__c = sectorFieldStr,
                                            Case_Number__c = (sectorFieldStr.containsIgnoreCase('SLA')) ? supportRequest.Case_Number__c : null);
		return secIns;
	}

    /** 
    * @MethodName: populateParentCaseForCallRequest
    * @Description: Populates SR field (Ready_To_Dial_Time__c) Case field (Ready_For_Call_NOD_SR_Request_a_Call__c, Ready_To_Dial_Time__c) from SR - Before Insert
    * @author: Rexie Aaron A. David
    * @Modification Log =============================================================== 
    * Ver Date Author Modification
    * 1.0 23/05/2019 RDAVID-tasks/24515914 : Logic for NVM Routing of "Communication - NOD - Request a Call" SR
    * 1.1 19/06/2019 RDAVID-tasks/24515914 : Continue working on Request a Call routing to NVM 
    **/ 
    public static Time currentLocalTime;// = Datetime.now().Time();
    public static void setReadyToDialTime (Support_Request__c sr, Case cse){
        // If Urgent is requested before 8am - Call now
        // They Can if URGENT is requested before 9am otherwise no 
        Time eightAm = Time.newInstance(8, 0, 0, 0);
        Time twoThirtyPm = Time.newInstance(14, 30, 59, 0);//Time.newInstance(14, 30, 59, 0);
        Time fourThirtyPm = Time.newInstance(16, 30, 59, 0);
        Time nineAm = Time.newInstance(9, 00, 0, 0);
        Time midnight = Time.newInstance(23, 59, 59, 0); //11:59 midnight
        Time midnight12 = Time.newInstance(24, 00, 0, 0);  

        if(!Test.isRunningTest()){
            currentLocalTime = Datetime.now().Time();//.addHours(-3);
        }
        
        System.debug('setReadyToDialTime > CURRENT LOCAL TIME + 2 HOURS FOR TESTING > '+currentLocalTime);
        //currentLocalTime = Time.newInstance(0, 1, 0, 0);
        System.debug('setReadyToDialTime > currentLocalTime > '+currentLocalTime);
        System.debug('setReadyToDialTime > eightAm > '+eightAm);
        System.debug('setReadyToDialTime > twoThirtyPm > '+twoThirtyPm);
        System.debug('setReadyToDialTime > fourThirtyPm > '+fourThirtyPm);
        System.debug('setReadyToDialTime > nineAm > '+nineAm);
        System.debug('setReadyToDialTime > midnight > '+midnight);
        System.debug('setReadyToDialTime > midnight12 > '+midnight12);
        
        // IF request a call is created between 8am-2:30pm 
        if(checkCurrentTimeWithinGivenRange(currentLocalTime,eightAm,twoThirtyPm)){
            sr.Call_Request_Log__c = 'Request a call is created between 8:00 am - 2:30 pm ';
            System.debug('setReadyToDialTime > Request a call is created between 8am-2:30pm  > '+currentLocalTime);
            if(sr.Urgency__c == 'Next 2 Hours'){
                System.debug('setReadyToDialTime > set Ready_To_Dial_Time__c after 2 hours > ' + DateTime.now().addHours(2));
                sr.Ready_To_Dial_Time__c = DateTime.now().addHours(2);// //DateTime.now().addHours(2);
                // cse.Ready_For_Call_NOD_SR_Request_a_Call__c = false;
                cse.Call_Request_Scheduled__c = true;
            }
            else if(sr.Urgency__c == 'Before EOD'){
                System.debug('setReadyToDialTime > set Ready_To_Dial_Time__c Before EOD > ' + fourThirtyPm);
                sr.Ready_To_Dial_Time__c = DateTime.newInstance(Date.today(), fourThirtyPm);
                // cse.Ready_For_Call_NOD_SR_Request_a_Call__c = false;
                cse.Call_Request_Scheduled__c = true;
            }
            else if(sr.Urgency__c == 'Urgent'){
                System.debug('setReadyToDialTime > set Ready_To_Dial_Time__c Urgent > ' + Datetime.now());
                sr.Ready_To_Dial_Time__c = Datetime.now();//.addMinutes(2);
                // cse.Ready_For_Call_NOD_SR_Request_a_Call__c = true;
                cse.Call_Request_Scheduled__c = false;
            }
            
        }
        // ELSE IF request a call is created between 2:30pm-4:30pm 
        else if(checkCurrentTimeWithinGivenRange(currentLocalTime,twoThirtyPm,fourThirtyPm)){    
            sr.Call_Request_Log__c = 'Request a call is created between 2:30 pm - 4:30 pm ';
            System.debug('setReadyToDialTime > Request a call is created between 2:30pm and 4:30pm  > '+currentLocalTime);
            if(sr.Urgency__c == 'Before EOD' || sr.Urgency__c == 'Next 2 Hours'){
                System.debug('setReadyToDialTime > set Ready_To_Dial_Time__c BEFORE EOD || Next 2 Hours > Set to fourThirtyPm > ' + DateTime.newInstance(Date.today(), fourThirtyPm));
                // sr.Ready_To_Dial_Time__c = DateTime.now().addMinutes(2);//
                sr.Ready_To_Dial_Time__c =  DateTime.newInstance(Date.today(), fourThirtyPm);
                // cse.Ready_For_Call_NOD_SR_Request_a_Call__c = false;
                cse.Call_Request_Scheduled__c = true;
            }
            else if(sr.Urgency__c == 'Urgent'){
                System.debug('setReadyToDialTime > set Ready_To_Dial_Time__c Urgent > ' + Datetime.now());
                sr.Ready_To_Dial_Time__c = Datetime.now();//.addMinutes(2);
                // cse.Ready_For_Call_NOD_SR_Request_a_Call__c = true;
                cse.Call_Request_Scheduled__c = false;
            }
        }

        // ELSE IF request a call is created outside 4:30pm 
        else if(!checkCurrentTimeWithinGivenRange(currentLocalTime,twoThirtyPm,fourThirtyPm)){
            sr.Call_Request_Log__c = 'Request a call is created after 4:30 pm ';
            System.debug('setReadyToDialTime > Request a call is created after 4:30pm  > '+currentLocalTime);

            if(checkCurrentTimeWithinGivenRange(currentLocalTime,fourThirtyPm,midnight)){
                System.debug('setReadyToDialTime > Request a call is created between 4:30pm and 11:59pm  > Set to 9am Next Day > '+DateTime.newInstance(Date.today().addDays(1), nineAm));
                sr.Ready_To_Dial_Time__c = DateTime.newInstance(Date.today().addDays(1), nineAm);
                // cse.Ready_For_Call_NOD_SR_Request_a_Call__c = false;
                cse.Call_Request_Scheduled__c = true;
            }

            else if(checkCurrentTimeWithinGivenRange(currentLocalTime,midnight12,eightAm)){
                System.debug('setReadyToDialTime > Request a call is created between 12:00am and 8:00am  > Set to 9am today > '+DateTime.newInstance(Date.today(), nineAm));
                sr.Ready_To_Dial_Time__c = DateTime.newInstance(Date.today(), nineAm);
                // cse.Ready_For_Call_NOD_SR_Request_a_Call__c = false;
                cse.Call_Request_Scheduled__c = true;
            }
        }
        // cse.Ready_To_Dial_Time__c = sr.Ready_To_Dial_Time__c;
	}

    public static Boolean checkCurrentTimeWithinGivenRange (Time timeInstance, Time startTime, Time endTime){
        // System.debug('timeInstance = '+timeInstance);
        if(timeInstance >= startTime && timeInstance <= endTime){
            //System.debug(timeInstance + ' is within '+ startTime +' AND '+endTime);
            return true;
        }
        return false;
    } 

    /** 
    * @MethodName: populateParentCaseForCallRequest
    * @Description: Populates Case fields (Requester_Phone__c, Call_Request_Reason__c) from SR - Before Insert
    * @author: Rexie Aaron A. David
    * @Modification Log =============================================================== 
    * Ver Date Author Modification
    * 1.0 19/06/2019 RDAVID-tasks/24515914 
    **/ 
    public static void populateParentCaseForCallRequest (Support_Request__c sr, Case cse, Map<Id,Case> caseMapToUpdate){
        Boolean updateCase = false;
        if(sr.Callback_Number__c != NULL){
            cse.Requester_Phone__c = sr.Callback_Number__c;
            updateCase = true;
        }
        if(sr.Call_Request_Reason__c != NULL){
            cse.Call_Request_Reason__c = sr.Call_Request_Reason__c;
            updateCase = true;
        }
        if(updateCase) caseMapToUpdate.put(cse.Id,cse);
    } 

    /** 
    * @MethodName: populateNVMFields
    * @Description: Populates Case NVM fields (NVMContactWorld__NVMAccountOverride__c, NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedIn__c, NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedOut__c, NVMContactWorld__RoutePlanIdentifier__c) from SR - Before Insert
    * @author: Rexie Aaron A. David
    * @Modification Log =============================================================== 
    * Ver Date Author Modification
    * 1.0 19/06/2019 RDAVID-tasks/24515914
    **/ 
    public static void populateNVMFields(Case cse, General_Settings1__c gensettings) {
        if(cse.Partition__c == 'Nodifi') { //RDAVID 24/05/2019 tasks/24515914 Populate NVM Fields
			cse.NVMContactWorld__NVMAccountOverride__c = gensettings.NVM_AccountOverride__c;
			cse.NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedIn__c = 120;
			cse.NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedOut__c = 5;
			cse.NVMContactWorld__RoutePlanIdentifier__c = gensettings.NVM_RoutePlanIdentifier__c;
		}
    }

    /** 
	* @FileName: initSRCallRequest
	* @Description: Generate Call Request SR Object
                    - 17/09/2019 RDAVID extra/task25953355-NODFlowV3SFAutomations 
                        This will invoke:
                        - "Support Request - Call Request" Process Builder
    * @Arguments:	String rTName, Id parentCaseId, String reason, Datetime readyToDialTime, String partition
	* @Returns:		Support_Request__c initSRCallRequest               
	**/ 
    public static Support_Request__c initSRCallRequest (String rTName, Id parentCaseId, String reason, Datetime readyToDialTime, String partition){
        Support_Request__c srIns = new Support_Request__c();
        srIns.RecordTypeId = Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get(rTName).getRecordTypeId();
        srIns.Case_Number__c = parentCaseId;
        srIns.Stage__c = 'New';
        srIns.Status__c = 'New';
        // srIns.Urgency__c = 'Urgent';
        srIns.Ready_To_Dial_Time__c = readyToDialTime;//.addMinutes(1);
        srIns.Partition__c = partition;
        srIns.Call_Request_Reason__c = reason;
        return srIns;
    }
}
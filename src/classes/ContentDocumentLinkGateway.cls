/**
 * @File Name          : ContentDocumentLinkGateway.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 08/01/2020, 7:38:36 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    07/01/2020   jesfer.baculod@positivelendingsolutions.com.au     Initial Version - SFBAU - 161
 * 1.1    08/01/2020   jesfer.baculod@positivelendingsolutions.com.au     Added Population of Latest Comment on Partner
**/
public without sharing class ContentDocumentLinkGateway {

    public static Lead updateLatestCommentonLead(ContentVersion cversion, Id leadId){
        Lead upLead = new Lead(Id = leadId);
        upLead.Latest_Comment__c = getLatestNoteAsLatestComment(cversion);
        return upLead;
    }

    public static Partner__c updateLatestCommentonPartner(ContentVersion cversion, Id partnerId){
        Partner__c upPartner = new Partner__c(Id = partnerId);
        upPartner.Latest_Comment__c = getLatestNoteAsLatestComment(cversion);
        return upPartner;
    }

    public static string getLatestNoteAsLatestComment(ContentVersion cversion){
        DateTime now = DateTime.now();
        String curTime = now.format('dd/MM/yyyy hh:mm a'); //hh for 12-hour format, HH for 24-hour format
        Blob nbodyB = cversion.VersionData;
        string nbodyS = nbodyB.toString();
        nbodyS.unescapeunicode();
        nbodyS = nbodyS.unescapeHtml4();
        string noteToLC = curTime + ' : ' + cversion.Title + ' - ' + nbodyS;
        if (noteToLC.length() > 255) noteToLC = noteToLC.substring(0,250);
        system.debug('@@noteToLC: '+noteToLC);
        return noteToLC;
    }

}
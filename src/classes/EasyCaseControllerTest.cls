/*
    @Description: test class of EasyCaseController
    @Author: Jesfer Baculod - Positive Group
    @History:
        03/27/18 - JBACULOD, Created test class; testCaseRoleScreen, testRoleAccScreen, testSummaryScreen
        04/04/18 - JBACULOD, Modified testCaseRoleScreen, testSummaryScreen - Primary Applicant Display and validations
*/
@isTest
private class EasyCaseControllerTest {

    @testsetup static void setup(){

        //Create Test Account
		List<Account> accList = TestDataFactory.createGenericAccount('Test Comp Account ',CommonConstants.ACC_RT_COMPANY, 6);
		Database.insert(accList);

    }

    static testmethod void testCaseRoleScreen(){

        //Load EasyCase page
        PageReference ecpref = Page.EasyCase;
        Test.setCurrentPage(ecpref);

        //Load controller
        EasyCaseController eccon = new EasyCaseController();

        //Load Case Record Type selection
        eccon.getCaseRTs();

        Test.startTest();

            //Simulate selecting of Commercial Case Record Type
            eccon.cse.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_N_COMM_BF).getRecordTypeId();
            eccon.defaultCaseLoanType();  
            eccon.cse.Application_Record_Type__c = CommonConstants.APP_RT_COMM_C;          
            //Load Application Record Type Type selection
            eccon.getAppRTs();
            //Simulate clicking of Continue button in Case screen
            eccon.Pcontinue();

            //Verify page display is now in Role screen (Curpage = 2)
            system.assertEquals(eccon.curPage,2);            

            //Simulate clicking of Back button in Role screen
            eccon.Pback();

            //Verify page display is back in Case screen (Curpage = 1)
            system.assertEquals(eccon.curPage,1);            

            //Commercial Partnership
            eccon.cse.Application_Record_Type__c = CommonConstants.APP_RT_COMM_P;
            eccon.Pcontinue();
            eccon.Pback();

            //Commercial Sole Trader
            eccon.cse.Application_Record_Type__c = CommonConstants.APP_RT_COMM_ST;
            eccon.Pcontinue();
            eccon.Pback();

            //Commercial Trust (Company)
            eccon.cse.Application_Record_Type__c = CommonConstants.APP_RT_COMM_TC;
            eccon.Pcontinue();
            eccon.Pback();

            //Commercial Trust (Individual)
            eccon.cse.Application_Record_Type__c = CommonConstants.APP_RT_COMM_TI;
            eccon.Pcontinue();
            eccon.Pback();

            //Simulate selecting of Consumer Case Record Type
            eccon.cse.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_P_CONS_AF).getRecordTypeId();          
            //Load Applicant Type selection
            eccon.getAppRTs();
            //Simulate clicking of Continue button in Case screen
            eccon.Pcontinue();
            //Simulate clicking of Back button in Role screen
            eccon.Pback();

            //Simulate selecting of Other Case Record Type
            eccon.cse.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_P_CONS_AF).getRecordTypeId(); //Consumer - Asset Finance
            //Load Application Record Type selection
            eccon.getAppRTs();
            eccon.cse.Application_Record_Type__c = CommonConstants.APP_RT_CONS_I; //Consumer - Individual
            //Simulate clicking of Continue button in Case screen
            eccon.Pcontinue();
            eccon.Pback();

            eccon.cse.Application_Record_Type__c = CommonConstants.APP_RT_CONS_M; //Consumer - Multi
            //Simulate clicking of Continue button in Case screen
            eccon.Pcontinue();

            //Simulate clicking of Add New Role button in Role screen
            //eccon.addNewRole();

            //Verify that list of Role increased its size
            //system.assertEquals(eccon.roleWrlist.size(),0);

        Test.stopTest();

    }

    static testmethod void testRoleAccScreen(){

        //Load EasyCase page
        PageReference ecpref = Page.EasyCase;
        Test.setCurrentPage(ecpref);

        //Load controller
        EasyCaseController eccon = new EasyCaseController();

        //Load Case Record Type selection
        eccon.getCaseRTs();

        //Simulate selecting of Other Case Record Type
        eccon.cse.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_Z_SG).getRecordTypeId();
        eccon.cse.Application_Record_Type__c = CommonConstants.APP_RT_CONS_J; //'Consumer - Joint';

        //Simulate clicking of Continue button in Case screen
        eccon.Pcontinue();

        Test.startTest();

            eccon.curRoleCtr = 1; //Role #1
            //Simulate clicking of New Account button in Role
            eccon.defaultRTshowAcc();        

            //Verify current default Account is Business Account due to selected Role RT
            system.assertEquals(eccon.roleWrlist[0].isPerson, true);

            //Simulate clicking of Add New Role button in Role screen
            //eccon.addNewRole();

            eccon.curRoleCtr = 2; //Role #2
            //Simulate clicking of New Account button in Role
            eccon.defaultRTshowAcc();

            //Simulate selecting of Account record type
            eccon.roleWrlist[1].acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CommonConstants.PACC_RT_I).getRecordTypeId();            
            eccon.checkAccIfPerson();   
            //Verify current Account is Person Account base on selected Account RT         
            system.assertEquals(eccon.roleWrlist[1].isPerson, true);
            
        Test.stopTest();

    }

    static testmethod void testSummaryScreen(){

        //Retrieve Account test data
        list <Account> acclist = [Select Id, Name From Account];

        //Load EasyCase page
        PageReference ecpref = Page.EasyCase;
        Test.setCurrentPage(ecpref);

        //Load controller
        EasyCaseController eccon = new EasyCaseController();

        //Load Case Record Type selection
        eccon.getCaseRTs();

        //Simulate selecting of Other Case Record Type
        eccon.cse.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_Z_SG).getRecordTypeId();
        eccon.cse.Application_Record_Type__c = CommonConstants.APP_RT_CONS_J; //Consumer - Joint

        //Simulate clicking of Continue button in Case screen
        eccon.Pcontinue();

        //Simulate clicking of Add New Role button in Role screen 
        /*eccon.addNewRole(); //Role #2
        eccon.addNewRole(); //Role #3
        eccon.addNewRole(); //Role #4
        eccon.addNewRole(); //Role #5 */

        //Verify that current number of Roles is correct
        system.assertEquals(eccon.roleWrlist.size(),2);

        Id roleRTId = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get(CommonConstants.ROLE_RT_BEN_O_BUSI).getRecordTypeId();
        Id paccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CommonConstants.PACC_RT_I).getRecordTypeId();      
        Id baccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CommonConstants.ACC_RT_COMPANY).getRecordTypeId();      
        for (EasyCaseController.roleWrapper rwr : eccon.roleWrlist){
            rwr.role.RecordTypeId = roleRTId;
            rwr.role.Primary_Contact_for_Application__c = false;
            if (rwr.roleCtr > 2){
                rwr.role.Account__c = acclist[rwr.roleCtr].Id;
                rwr.isCreateAcc = false;
            }
            else if (rwr.roleCtr == 1){
                rwr.con.FirstName = 'FirstT';
                rwr.con.MiddleName = 'MiddleT';
                rwr.con.LastName = 'LastT';
                rwr.isCreateAcc = true;
                rwr.acc.RecordTypeId = paccRTId;
                rwr.isPerson = true;
            }
            else if (rwr.roleCtr == 2){
                rwr.isPerson = false;
                rwr.acc.Name = 'TestAccName';
                rwr.acc.RecordTypeId = baccRTId;
                rwr.isCreateAcc = true;
            }
        }

        Test.startTest();
            //Simulate clicking of Continue button in Role screen
            eccon.Pcontinue();
            //Verify page display is in Summary screen (Curpage = 3)
            system.assertEquals(eccon.curPage,2);   

            //Verify page has errors due to no Primary Contact has been selected in Roles
            system.assertEquals(eccon.hasErrors,true);  

            for (EasyCaseController.roleWrapper rwr : eccon.roleWrlist){
                rwr.role.Primary_Contact_for_Application__c = true;
            }

            //Simulate clicking of Continue button in Role screen
            eccon.Pcontinue();
            //Verify page has errors due to multiple Primary Contacts have been selected in Roles
            system.assertEquals(eccon.hasErrors,true); 

            for (EasyCaseController.roleWrapper rwr : eccon.roleWrlist){
                if (rwr.roleCtr == 1) rwr.role.Primary_Contact_for_Application__c = true;
                else rwr.role.Primary_Contact_for_Application__c = false;
                rwr.role.Role_Type__c = CommonConstants.EASYCASE_VAL_ROLE_PA; //Test multiple Primary Applicant
            } 

            //Simulate clicking of Continue button in Role screen
            eccon.Pcontinue();
            //Verify page has still errors due to multiple Primary Applicants have been selected in Roles
            system.assertEquals(eccon.hasErrors,true); 

            for (EasyCaseController.roleWrapper rwr : eccon.roleWrlist){
                if (rwr.roleCtr == 1) rwr.role.Role_Type__c = CommonConstants.EASYCASE_VAL_ROLE_PA;
                else rwr.role.Role_Type__c = null;
            } 
            eccon.Pcontinue();
            //Verify page has no more errors
            //Verify page display is in Summary screen (Curpage = 3)
            system.assertEquals(eccon.curPage,3);
            system.assertEquals(eccon.hasErrors,false); 

            eccon.roleWrlist[0].acc.FirstName = 'longtestlongtestlongtestlongtestlongtestlongtestlongtestlongtestlongtestlongtestlongtestlongtestlongtestlongtestlongtestlongtest';
            eccon.saveEasyCase();
            //Verify page has errors (Test exception)
            system.assertEquals(eccon.hasErrors,false); 

        Test.stopTest();

    }

}
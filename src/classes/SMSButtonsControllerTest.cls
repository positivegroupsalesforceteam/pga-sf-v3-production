/**
 * @File Name          : SMSButtonsControllerTest.cls
 * @Description        : Test class of SMSButtonsController
 * @Author             : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Group              : Positive Group Australia
 * @Last Modified By   : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Last Modified On   : 04/07/2019, 6:52:35 am
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    26/06/2019, 6:11:29 am   Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Initial Version
 * 1.1    04/07/2019, 6:47:44 am   Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Added testCompose
**/
@isTest
private class SMSButtonsControllerTest {

    @testSetup static void setupData(){

        //Create Test Account
        Account acc = new Account(
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CommonConstants.PACC_RT_I).getRecordTypeId(), 
            FirstName = 'TestF',
            LastName = 'TestL',
            PersonEmail = 'test@test.com',
            PersonMobilePhone = '04123456789'
        );
        insert acc;

        //Create Test Case
        Case cse = new Case(
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_P_CONS_AF).getRecordTypeId(), 
            Status = 'New', Stage__c = 'Open', Partition__c = 'Positive', Channel__c = 'PLS',
            Primary_Contact_Name_MC__c = acc.Id,
            Primary_Contact_Phone_MC__c = acc.PersonMobilePhone,
            AccountId = acc.Id
        );
        insert cse;

        //Create Test Role
        Role__c role = new Role__c(
            Case__c = cse.Id,
            Account__c = acc.Id,
            Mobile_Phone__c = acc.PersonMobilePhone,
            Email__c = acc.PersonEmail
        );
        insert role;

        //Create Test SMS Template
        smagicinteract__SMS_Template__c smstemplate = new smagicinteract__SMS_Template__c(
            smagicinteract__Description__c = 'test',
            smagicinteract__Name__c = 'TestTemplate',
            smagicinteract__ObjectName__c = 'Role__c',
            smagicinteract__Text__c = 'content'
        );
        insert smstemplate;

        //Create Test Converse App
        smagicinteract__Converse_App__c converseApp = new smagicinteract__Converse_App__c(
            Name = 'Test Converse App',
            smagicinteract__Object__c = 'Role__c',
            smagicinteract__PrimaryType__c = 'Automation',
            smagicinteract__Status__c = 'Active',
            smagicinteract__Purpose__c = 'test'
        );  
        insert converseApp;

        //Create Test Converse App Action
        smagicinteract__Converse_App_Action__c converseAppAction = new smagicinteract__Converse_App_Action__c(
            Name = 'TestAction1',
            smagicinteract__Converse_App__c = converseApp.Id,
            smagicinteract__Status__c = 'Active',
            smagicinteract__Mobile_Phone_Field__c = 'Mobile_Phone__c',
            smagicinteract__Type__c = 'Automation',
            smagicinteract__Template_Selection_Type__c = 'Fixed',
            smagicinteract__SMS_Template__c = smstemplate.Id
        );
        insert converseAppAction;
        list <smagicinteract__Converse_App_Action__c> caa = [Select Id, smagicinteract__Automation_Key__c From smagicinteract__Converse_App_Action__c Where Id = : converseAppAction.Id ];
        system.debug('@@caa:'+ caa); 

    }

    
    /**
    * @description test displaying of SMS Buttons base from SMS Button Settings
    * @author Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au) | 26/06/2019
    * @return testmethod
    */
    static testmethod void testDisplayButtons(){

        list <Role__c> rolelist = [Select Id, Case__c, Case_Partition__c From Role__c];
        list <smagicinteract__Converse_App_Action__c> smsActionList = [Select Id, smagicinteract__Automation_Key__c From smagicinteract__Converse_App_Action__c Order By CreatedDate DESC limit 1];
        SMSButtonsController smsbtnCC = new SMSButtonsController();

        Test.startTest();
            SMSButtonsController.wrappedAllowedSMSPerObject waspo = SMSButtonsController.getSMSBUttonSettingForObject('Role__c', rolelist[0].Case_Partition__c, rolelist[0].Id);

            smagicinteract__Converse_App_Task__c cAppTask = new smagicinteract__Converse_App_Task__c(
                Role__c = rolelist[0].Id
            );

            try{ //Save Converse App Task with Error
                string toCreatecAppTask1 = SMSButtonsController.createSMSNotification(cAppTask, rolelist[0].Id);
            }
            catch(Exception e){
                //Send SMS Template
                cApPTask.smagicinteract__Automation_Key_Reference__c = smsActionList[0].smagicinteract__Automation_Key__c;
                string toCreatecAppTask2 = SMSButtonsController.createSMSNotification(cAppTask, rolelist[0].Id);
            }

        Test.stopTest();

    }

    static testmethod void testCompose(){

        list <Role__c> rolelist = [Select Id, Case__c, Case_Partition__c From Role__c];
        list <smagicinteract__Converse_App_Action__c> smsActionList = [Select Id, smagicinteract__Automation_Key__c From smagicinteract__Converse_App_Action__c Order By CreatedDate DESC limit 1];
        SMSButtonsController smsbtnCC = new SMSButtonsController();

        SMSButtonsController.wrappedAllowedSMSPerObject waspo = SMSButtonsController.getSMSBUttonSettingForObject('Role__c', rolelist[0].Case_Partition__c, rolelist[0].Id);

        Test.startTest();

            smagicinteract__smsMagic__c freeBodySms = new smagicinteract__smsMagic__c(
                smagicinteract__SenderId__c  = 'POSITIVE',
                smagicinteract__SMSText__c  = 'test sms',
                smagicinteract__disableSMSOnTrigger__c = 0,
                smagicinteract__ObjectType__c = 'Role__c',
                smagicinteract__Name__c = 'Test Recipient',
                Role__c = rolelist[0].Id
            );

            try{ //Save SMS History with Error
                string toCreatecSMSHistory = SMSButtonsController.sendComposedSMS(freeBodySms);
            }
            catch(Exception e){
                //Send Compose SMS
                freeBodySms.smagicinteract__PhoneNumber__c = '04123456789';
                string toCreatecSMSHistory = SMSButtonsController.sendComposedSMS(freeBodySms);
            }

        Test.stopTest();

    }



}
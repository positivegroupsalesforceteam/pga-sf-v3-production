<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <tab>standard-Chatter</tab>
    <tab>standard-Workspace</tab>
    <tab>standard-ContentSearch</tab>
    <tab>standard-ContentSubscriptions</tab>
    <tab>Commission__c</tab>
    <tab>Tasks</tab>
    <tab>smagicworkflow__Error_Log__c</tab>
    <tab>API_Lenders__c</tab>
    <tab>Applicant_2__c</tab>
    <tab>Company_Information__c</tab>
    <tab>smagicinteract__License__c</tab>
</CustomApplication>
